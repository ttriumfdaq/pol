[/System/Clients/22708]
Name = STRING : [32] ODBEdit
Host = STRING : [256] isdaq01.triumf.ca
Hardware type = INT : 42
Server Port = INT : 44541
Transition Mask = DWORD : 0
Deferred Transition Mask = DWORD : 0

[/System/Tmp/19472O]
State = INT : 1
Online Mode = INT : 1
Run number = INT : 52
Transition in progress = INT : 0
Requested transition = INT : 0
Start time = STRING : [32] Fri Aug  8 11:31:51 2003
Start time binary = DWORD : 1060367511
Stop time = STRING : [32] Fri Aug  8 11:39:02 2003
Stop time binary = DWORD : 1060367942

[/System]
Client Notify = INT : 0
Prompt = STRING : [256] [%h:%e:%s]%p>

[/Programs/ODBEdit]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 0

[/Programs/rf_config]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/rf_config  -e pol -D
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : y
Alarm class = STRING : [32] Caution
First failed = DWORD : 1090950389

[/Programs]
Execute on start run = STRING : [256] 

[/Programs/mheader]
Required = BOOL : n
Watchdog timeout = INT : 60000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/bin/start-daq-tasks
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 1088463679

[/Programs/Mdarc]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/bin/start-daq-tasks
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 1088463703

[/Programs/mhttpd]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090950389

[/Programs]
Execute on stop run = STRING : [256] odb -c 'set  "/Equipment/fifo_acq/client flags/client alarm" 0'

[/Programs/Logger]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090950389

[/Programs/fePOL]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : y
Alarm class = STRING : [32] Caution
First failed = DWORD : 1090950389

[/Programs/mdump]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090535828

[/Programs/Lcrplot]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1059687843

[/Programs/Analyzer]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090950389

[/Programs/fe_runlog]
Required = BOOL : n
Watchdog timeout = INT : 60000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090950389

[/Programs/test_dvm]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090370364

[/Programs/feDVM]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 1090950389

[/Programs/test_dvm_scan]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1090370364

[/Experiment]
Name = STRING : [32] pol

[/Experiment/Edit on start]
run_title = STRING : [88] Test
experiment number = DWORD : 920
experimenter = STRING : [32] Suz
sample = STRING : [15] none
orientation = STRING : [15] none
temperature = STRING : [15] none
field = STRING : [15] none
Element = STRING : [24] Lanthanum
Mass = INT : 139
DC offset(V) = INT : 300
Ion source (kV) = DOUBLE : 14
Laser wavelength (nm) = DOUBLE : 18587.3
write data = LINK : [35] /Logger/Channels/0/Settings/Active
Number of scans = LINK : [47] /Equipment/FIFO_acq/sis mcs/hardware/num scans
Edit run number = BOOL : y

[/Experiment/Lock when running]
dis_rn_check = LINK : [51] /Equipment/FIFO_acq/mdarc/disable run number check
SIS test mode = LINK : [42] /Equipment/FIFO_acq/sis mcs/sis test mode
PPGinput = LINK : [34] /Equipment/FIFO_acq/sis mcs/Input
SIS ref A = LINK : [65] /Equipment/FIFO_acq/sis mcs/Hardware/Enable SIS ref ch1 scaler A
SIS ref B = LINK : [65] /Equipment/FIFO_acq/sis mcs/Hardware/Enable SIS ref ch1 scaler B

[/Experiment/Parameter Comments]
Active = STRING : [36] <i>Enter y to save data to disk</i>
Num scans = STRING : [80] <i>Stop run after num scans is reached. Enter 0 to disable (free running)</i>
Num cycles = STRING : [80] <i>Stop run after num cycles is reached. Enter 0 to disable (free running)</i>

[/Experiment/Security]
Web Password = STRING : [32] mim2P4OCV.PW3

[/Runinfo]
State = INT : 1
Online Mode = INT : 1
Run number = INT : 185
Transition in progress = INT : 0
Requested transition = INT : 0
Start time = STRING : [32] Tue Jul 27 10:46:02 2004
Start time binary = DWORD : 1090950362
Stop time = STRING : [32] Mon Jul 26 13:35:13 2004
Stop time binary = DWORD : 0

[/Alarms]
Alarm system active = BOOL : n

[/Alarms/Alarms/Demo ODB]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Runinfo/Run number > 100
Alarm Class = STRING : [32] Alarm
Alarm Message = STRING : [80] Run number became too large

[/Alarms/Alarms/Demo periodic]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 4
Check interval = INT : 28800
Checked last = DWORD : 1058817867
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] 
Alarm Class = STRING : [32] Warning
Alarm Message = STRING : [80] Please do your shift checks

[/Alarms/Alarms/Epics]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Warning
Alarm Message = STRING : [80] Program Epics is not running

[/Alarms/Alarms/client alarm]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 20
Checked last = DWORD : 1090950379
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Equipment/fifo_acq/client flags/client alarm = 1
Alarm Class = STRING : [32] Alarm
Alarm Message = STRING : [80] Stop the run and restart (error or event limit reached)

[/Alarms/Alarms/Mdarc]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program Mdarc is not running

[/Alarms/Alarms/fePOL]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program fePOL is not running

[/Alarms/Alarms/Run number]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 60
Checked last = DWORD : 1060106030
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /runinfo/run number < 29999
Alarm Class = STRING : [32] Alarm
Alarm Message = STRING : [80] Run number became too small

[/Alarms/Alarms/rf_config]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program rf_config is not running

[/Alarms/Alarms/mheader]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program mheader is not running

[/Alarms/Alarms/feDVM]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] caution
Alarm Message = STRING : [80] Program feDVM is not running

[/Alarms/Classes/Alarm]
Write system message = BOOL : y
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : n
Display BGColor = STRING : [32] red
Display FGColor = STRING : [32] black

[/Alarms/Classes/Warning]
Write system message = BOOL : y
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : n
Display BGColor = STRING : [32] red
Display FGColor = STRING : [32] black

[/Alarms/Classes/Caution]
Write system message = BOOL : n
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : y
Display BGColor = STRING : [32] blue
Display FGColor = STRING : [32] red

[/alias]
Expert& = LINK : [18] /alias/Expert_dir
Hardware& = LINK : [26] /alias/User_hardware_dir/
Run_info& = LINK : [27] /Experiment/Edit on start/

[/alias/Expert_dir]
PPGinput = LINK : [34] /Equipment/FIFO_acq/sis mcs/Input
PPGoutput = LINK : [35] /Equipment/FIFO_acq/sis mcs/Output
Mdarc = LINK : [26] /Equipment/FIFO_acq/mdarc
Midbnmr = LINK : [45] /equipment/FIFO_acq/mdarc/histograms/midbnmr
Hardware = LINK : [37] /Equipment/FIFO_acq/sis mcs/Hardware
CampSweepDev = LINK : [39] /Equipment/FIFO_acq/camp sweep devices
ClientFlags = LINK : [33] /Equipment/FIFO_acq/client flags

[/alias/User_hardware_dir]
Fluor mon  thr; 0 means disable = LINK : [55] /Equipment/FIFO_acq/sis mcs/Hardware/Fluor monitor thr
Cycle thr (%); 0 means disable = LINK : [51] /Equipment/FIFO_acq/sis mcs/Hardware/Cycle thr (%)
N good cycles skip extra = LINK : [60] /Equipment/FIFO_acq/sis mcs/Hardware/Diagnostic channel num
Re-reference = LINK : [50] /Equipment/FIFO_acq/sis mcs/Hardware/Re-reference
Disable NaCell = LINK : [52] /Equipment/FIFO_acq/sis mcs/Hardware/Disable NaCell
Disable Laser = LINK : [51] /Equipment/FIFO_acq/sis mcs/Hardware/Disable Laser
Disable Field = LINK : [51] /Equipment/FIFO_acq/sis mcs/Hardware/Disable Field

[/alias]
PPG1h& = LINK : [11] /ppg/PPG1h

[/Equipment/FIFO_acq/Common]
Event ID = WORD : 1
Trigger mask = WORD : 1
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 1
Period = INT : 10
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/FIFO_acq/mdarc/histograms]
number defined = INT : 6
num bins = INT : 1
dwell time (ms) = FLOAT : 1000
resolution code = INT : -1
titles = STRING[16] :
[32] Const
[32] FluM2
[32] L+
[32] R+
[32] L-
[32] R-
[32] NBMB+
[32] NBMF+
[32] NBMB-
[32] NBMF-
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
bin zero = INT[16] :
[0] 1
[1] 1
[2] 1
[3] 1
[4] 1
[5] 1
[6] 1
[7] 1
[8] 1
[9] 1
[10] 1
[11] 1
[12] 1
[13] 1
[14] 1
[15] 1
first good bin = INT[16] :
[0] 1
[1] 1
[2] 1
[3] 1
[4] 1
[5] 1
[6] 0
[7] 0
[8] 0
[9] 0
[10] 20
[11] 20
[12] 20
[13] 20
[14] 1
[15] 1
last good bin = INT[16] :
[0] 1
[1] 1
[2] 1
[3] 1
[4] 1
[5] 1
[6] 1100
[7] 1100
[8] 1100
[9] 1100
[10] 60
[11] 60
[12] 60
[13] 60
[14] 800
[15] 800
first background bin = INT[16] :
[0] 0
[1] 0
[2] 0
[3] 0
[4] 0
[5] 0
[6] 1
[7] 1
[8] 1
[9] 1
[10] 1
[11] 1
[12] 1
[13] 1
[14] 0
[15] 0
last background bin = INT[16] :
[0] 0
[1] 0
[2] 0
[3] 0
[4] 0
[5] 0
[6] 50
[7] 50
[8] 50
[9] 50
[10] 10
[11] 10
[12] 10
[13] 10
[14] 0
[15] 0

[/Equipment/FIFO_acq/mdarc/histograms/output]
histogram totals = FLOAT[16] :
[0] 2.74982e+08
[1] 0
[2] 0
[3] 0
[4] 28
[5] 62
[6] 0
[7] 0
[8] 199
[9] 288
[10] 0
[11] 0
[12] 0
[13] 0
[14] 0
[15] 0
total saved = FLOAT : 2.74983e+08

[/Equipment/FIFO_acq/mdarc/histograms/midbnmr]
label = STRING : [16] not used
active = BOOL : n
first time bin = INT : 0
last time bin = INT : 1
start freq (Hz) = INT : 1000
end freq (Hz) = INT : 2000
last file written = STRING : [128] not used
time file written = STRING : [32] not used

[/Equipment/FIFO_acq/mdarc]
time_of_last_save = STRING : [32] Thu Jul 31 15:51:41 2003
last_saved_filename = STRING : [128] /is01_data/pol/dlog/2003/030201.msr
saved_data_directory = STRING : [128] /is01_data/pol/dlog/2003
num_versions_before_purge = DWORD : 4
end_of_run_purge_rename = BOOL : y
archived_data_directory = STRING : [128] /musr/dlog/bnmr
archiver task = STRING : [80] /home/musrdaq/musr/mdarc/bin/cpbnmr
save_interval(sec) = DWORD : 15
toggle = BOOL : n
disable run number check = BOOL : n
run type = STRING : [5] test
perlscript path = STRING : [50] /home/pol/online/perl
enable mdarc logging = BOOL : n

[/Equipment/FIFO_acq/mdarc/camp]
camp hostname = STRING : [128] polvw
temperature variable = STRING : [128] /Test/dac_set
field variable = STRING : [128] /Test/dac_set

[/Equipment/FIFO_acq/mdarc]
enable poststop rn check = BOOL : n

[/Equipment/FIFO_acq/sis mcs/Output]
PPG_loadfile = STRING : [128] 1n.ppg
PPG template = STRING : [20] 1n_.ppg
beam on time (ms) = FLOAT : 0
frequency stop (Hz) = FLOAT : 0
num frequency steps = DWORD : 0
dwell time (ms) = FLOAT : 1000
num dwell times = DWORD : 1
RF on time (ms) = FLOAT : 0
RF off time (ms) = FLOAT : 0
compiled file time (binary) = DWORD : 1090949745
compiled file time = STRING : [32] Tue Jul 27 10:35:45 2004
e1c Camp Path = STRING : [32] 
e1c Camp IfMod = STRING : [10] 
e1c Camp Instr Type = STRING : [32] 
e1n Epics device = STRING : [32] none
e1h scan device = STRING : [10] DAC

[/Equipment/FIFO_acq/sis mcs/Hardware]
num cycles = DWORD : 0
Fluor monitor thr = DWORD : 0
Cycle thr (%) = FLOAT : 0
Diagnostic channel num = INT : 100
Re-reference = BOOL : n
num polarization cycles = DWORD : 0
polarization switch delay = DWORD : 0
Enable SIS ref ch1 scaler A = BOOL : n
Enable SIS ref ch1 scaler B = BOOL : y
Enable helicity flipping = BOOL : n
helicity flip sleep (ms) = INT : 0
PPG acq cycle control = BOOL : y
Check RF trip = BOOL : y
Num scans = INT : 0
Disable NaCell = BOOL : y
Disable Laser = BOOL : y
Disable Field = BOOL : y

[/Equipment/FIFO_acq/sis mcs/sis test mode]
test mode = BOOL : n
num bins = DWORD : 202
Dwell time (ms) = FLOAT : 1
beam time (ms) = FLOAT : 150

[/Equipment/FIFO_acq/sis mcs/flags]
hold = BOOL : n

[/Equipment/FIFO_acq/sis mcs/Input]
Experiment name = STRING : [32] 1h
CFG path = STRING : [128] /home/pol/online/pol/ppcobj
PPG path = STRING : [128] /home/pol/online/ppg-templates
beam off time (ms) = FLOAT : 0
num beam PreCycles = DWORD : 1
num_beam_acq_cycles = DWORD : 100000
frequency start (Hz) = DWORD : 1000
frequency stop (Hz) = DWORD : 2000
frequency increment (Hz) = DWORD : 100
RF on time (ms) = FLOAT : 100
RF off time (ms) = FLOAT : 100
num RF on delays (dwell times) = DWORD : 10
MCS enable delay (ms) = FLOAT : 1
MCS enable gate (ms) = FLOAT : 1000
MCS max delay (ms) = FLOAT : 1
DAQ service time (ms) = FLOAT : 3000
Minimal delay (ms) = FLOAT : 0.0005
Time slice (ms) = FLOAT : 1e-04
E1B Dwell time (ms) = FLOAT : 100
RF delay (ms) = FLOAT : 30000
Bg delay (ms) = FLOAT : 1
Num spectra per freq = DWORD : 1
Num RF cycles = DWORD : 50
E2B Num beam on dwell times = INT : 1000
Check recent compiled file = BOOL : y
freq single slice width (Hz) = DWORD : 10
Num freq slices = DWORD : 1
prebeam on time (ms) = FLOAT : 1
flip 360 delay (ms) = FLOAT : 10
flip 180 delay (ms) = FLOAT : 40
f slice internal delay (ms) = FLOAT : 10
beam_mode = CHAR : P
counting_mode = CHAR : 1
f select pulselength (ms) = FLOAT : 10
e00 prebeam dwelltimes = DWORD : 50
RFon dwelltime = DWORD : 0
e00 beam on dwelltimes = DWORD : 50
e00 beam off dwelltimes = DWORD : 1000
e00 rf frequency (Hz) = DWORD : 22064585
rfon duration (dwelltimes) = INT : 0
num type1 frontend histograms = INT : 6
num type2 frontend histograms = INT : 10
e1a&1b pulse pairs = STRING : [4] 000
e1a&1b freq mode = STRING : [3] F0
e1c Camp Device = STRING : [5] DAC
e2c beam on time (ms) = FLOAT : 0
num cycles per supercycle = INT : 0
NaVolt start = FLOAT : 190
NaVolt stop = FLOAT : 290
NaVolt inc = FLOAT : 2
Laser start = FLOAT : -2
Laser stop = FLOAT : 2
Laser inc = FLOAT : -0.005
e1c Camp Start = FLOAT : 0
e1c Camp Stop = FLOAT : 9.95
e1c Camp Inc = FLOAT : 0.05
Field start = FLOAT : 95
Field stop = FLOAT : 100
Field inc = FLOAT : 1.1
e1h DAC Start = FLOAT : 0
e1h DAC Stop = FLOAT : 10
e1h DAC Inc = FLOAT : 0.05

[/Equipment/FIFO_acq/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/FIFO_acq/camp sweep devices/frequency generator]
Sweep Device code = STRING : [5] FG
Camp path = STRING : [32] /frq
GPIB port or rs232 portname = STRING : [10] 7
Instrument Type = STRING : [32] bnc625a
Camp scan path = STRING : [80] /frq/frequency
Scan units = STRING : [10] Hz
maximum = FLOAT : 500000
minimum = FLOAT : 1000
Camp device dependent path = STRING : [80] 
integer conversion factor = INT : 1

[/Equipment/FIFO_acq/camp sweep devices/magnet]
Sweep Device code = STRING : [5] MG
Camp path = STRING : [32] /Magnet
GPIB port or rs232 portname = STRING : [10] /tyCo/5
Instrument Type = STRING : [32] oxford_ips120
Camp scan path = STRING : [80] /Magnet/mag_field
Scan units = STRING : [10] T
maximum = FLOAT : 7
minimum = FLOAT : -7
Camp device dependent path = STRING : [80] /Magnet/ramp_status
integer conversion factor = INT : 10000

[/Equipment/FIFO_acq/camp sweep devices/dac]
Sweep Device code = STRING : [5] DAC
Camp path = STRING : [32] /v_scan
GPIB port or rs232 portname = STRING : [10] 0
Instrument Type = STRING : [32] tip850_dac
Camp scan path = STRING : [80] /v_scan/dac_set
Scan units = STRING : [10] V
maximum = FLOAT : 10
minimum = FLOAT : -10
Camp device dependent path = STRING : [80] /frq/amplitude
integer conversion factor = INT : 1000

[/Equipment/FIFO_acq/client flags]
mdarc = BOOL : n
rf_config = BOOL : n
mheader = BOOL : n
frontend = BOOL : n
fe_epics = BOOL : n
enable client check = BOOL : y
client alarm = INT : 0

[/Equipment/FIFO_acq/Pol params]
Settling time (ms) = INT : 10

[/Equipment/Cycle_Scalers/Settings]
Names = STRING[16] :
[32] Scaler_B%SIS Ref pulse
[32] Scaler_B%Fluor. mon 1
[32] Scaler_B%Polariz Left
[32] Scaler_B%Polariz Right
[32] Scaler_B%Neutral Beam B1
[32] Scaler_B%Neutral Beam B2
[32] Scaler_B%Neutral Beam B3
[32] Scaler_B%Neutral Beam B4
[32] Scaler_B%Neutral Beam F1
[32] Scaler_B%Neutral Beam F2
[32] Scaler_B%Neutral Beam F3
[32] Scaler_B%Neutral Beam F4
[32] General%Pol Cycle Sum
[32] General%Pol Cycle Asym
[32] General%NeutBm Cycle Sum
[32] General%NeutBm Cycle Asym

[/Equipment/Cycle_Scalers/Common]
Event ID = WORD : 3
Trigger mask = WORD : 1
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 257
Period = INT : 100
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Cycle_Scalers/Variables]
HSCL = DOUBLE : 0
CYCI = DWORD : 0

[/Equipment/Cycle_Scalers/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Histo/Common]
Event ID = WORD : 2
Trigger mask = WORD : 1
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : n
Read on = INT : 17
Period = INT : 100
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Histo/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Hdiagnosis/Common]
Event ID = WORD : 4
Trigger mask = WORD : 1
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : n
Read on = INT : 1
Period = INT : 100
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Hdiagnosis/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Info ODB/Common]
Event ID = WORD : 10
Trigger mask = WORD : 0
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] FIXED
Enabled = BOOL : y
Read on = INT : 257
Period = INT : 500
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Info ODB/Variables]
helicity = DWORD : 0
current cycle = DWORD : 7
cancelled cycle = DWORD : 1
current scan = DWORD : 1
Ref HelUp thr = DOUBLE : 33
Ref HelDown thr = DOUBLE : 33
Current HelUp thr = DOUBLE : 0
Current HelDown thr = DOUBLE : 55
RF state = DWORD : 0
Fluor monitor counts = DWORD : 0
EpicsDev Set(V) = FLOAT : 0
EpicsDev Read(V) = FLOAT : 0
Campdev set = FLOAT : 0
Campdev read = FLOAT : 0

[/Equipment/Info ODB/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/DVM/Common]
Event ID = WORD : 6
Trigger mask = WORD : 0
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 511
Period = INT : 60000
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 60
Frontend host = STRING : [32] midtis07.triumf.ca
Frontend name = STRING : [32] feDVM
Frontend file name = STRING : [256] feDVM.c

[/Equipment/DVM/Settings]
Address = INT : 8
Modules = INT[3] :
[0] 34901
[1] 34907
[2] 0
ScanList = STRING : [64] 101,102
Slot1Config = STRING[20] :
[64] SENS:FUNC "VOLT:DC";SENS:VOLT:DC:RANG 10;SENS:VOLT:DC:NPLC 1
[64] SENS:FUNC "TEMP";SENS:TEMP:TRAN:TC:TYPE J;SENS:TEMP:NPLC 1
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
Slot1Names = STRING[20] :
[32] Readback (Volts)
[32] Temp
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 

[/Equipment/DVM/Settings/Slot2DAC1]
Address = STRING : [32] 204
Demand = FLOAT : 0
Handshake = INT : 1

[/Equipment/DVM/Settings/Slot2DAC2]
Address = STRING : [32] 205
Demand = FLOAT : 0
Handshake = INT : 0

[/Equipment/DVM/Variables]
Measured = FLOAT[2] :
[0] -1.3e-05
[1] 26.813

[/Equipment/DVM/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/PPG/PPGcommon]
Experiment name = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
CFG path = LINK : [43] /Equipment/FIFO_acq/sis mcs/Input/CFG path
PPG path = LINK : [43] /Equipment/FIFO_acq/sis mcs/Input/PPG path
Time slice (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Time slice (ms)
Minimal delay (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/Minimal delay (ms)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1a]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
beam mode (C or P) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/beam_mode
pulse pairs (000 or 180) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b pulse pairs
frequency mode (FR or F0) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b freq mode
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
Bg delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Bg delay (ms)
RF delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/RF delay (ms)
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num RF cycles = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Num RF cycles
Num events per freq = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
reference frequency (Hz) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/e00 rf frequency (Hz)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1b]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
beam mode (C or P) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/beam_mode
pulse pairs (000 or 180) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b pulse pairs
frequency mode (FR or F0) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b freq mode
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
Bg delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Bg delay (ms)
RF delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/RF delay (ms)
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num RF cycles = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Num RF cycles
Num events per freq = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
E1B Dwell time (ms) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/E1B Dwell time (ms)
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
reference frequency (Hz) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/e00 rf frequency (Hz)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1n]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
NaVolt start (volts) = LINK : [47] /Equipment/FIFO_acq/sis mcs/Input/NaVolt start
NaVolt stop (volts) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/NaVolt stop
NaVolt inc (volts) = LINK : [45] /Equipment/FIFO_acq/sis mcs/Input/NaVolt inc
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG1f]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG1d]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Laser start (volts) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/Laser start
Laser stop (volts) = LINK : [45] /Equipment/FIFO_acq/sis mcs/Input/Laser stop
Laser inc (volts) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/Laser inc
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG1c]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Camp Sweep Device = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Device
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Sleep time (ms) = LINK : [50] /Equipment/FIFO_acq/Pol params/Settling time (ms)
scan start = LINK : [49] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Start
scan stop = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Stop
scan increment = LINK : [47] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Inc

[/PPG/PPG10]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG20]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Dwell time (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
e20 prebeam dwelltimes = LINK : [57] /Equipment/FIFO_acq/sis mcs/Input/e00 prebeam dwelltimes
e20 beam on dwelltimes = LINK : [57] /Equipment/FIFO_acq/sis mcs/Input/e00 beam on dwelltimes
e20 beam off dwelltimes = LINK : [58] /Equipment/FIFO_acq/sis mcs/Input/e00 beam off dwelltimes
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG2a]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
num RF on delays (dwell times) = LINK : [65] /Equipment/FIFO_acq/sis mcs/Input/num RF on delays (dwell times)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)
beam off time (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/beam off time (ms)
num beam PreCycles = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/num beam PreCycles
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)

[/PPG/PPG2b]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
num RF on delays (dwell times) = LINK : [65] /Equipment/FIFO_acq/sis mcs/Input/num RF on delays (dwell times)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)
beam off time (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/beam off time (ms)
num beam PreCycles = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/num beam PreCycles
Num beam on dwell times = LINK : [62] /Equipment/FIFO_acq/sis mcs/Input/E2B Num beam on dwell times
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)

[/PPG/PPG2c]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
prebeam on time (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/prebeam on time (ms)
beam_mode = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/beam_mode
num RF on delays (dwell times) = LINK : [65] /Equipment/FIFO_acq/sis mcs/Input/num RF on delays (dwell times)
beam on time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/e2c beam on time (ms)
beam off time (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/beam off time (ms)
RF on time e2c (ms) = LINK : [60] /Equipment/FIFO_acq/sis mcs/Input/f select pulselength (ms)
Num freq slices = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Num freq slices
freq single slice width (Hz) = LINK : [63] /Equipment/FIFO_acq/sis mcs/Input/freq single slice width (Hz)
f slice internal delay (ms) = LINK : [62] /Equipment/FIFO_acq/sis mcs/Input/f slice internal delay (ms)
flip 180 delay (ms) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/flip 180 delay (ms)
flip 360 delay (ms) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/flip 360 delay (ms)
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
counting_mode = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/counting_mode
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1e]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Field start (Gauss) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/Field start
Field stop (Gauss) = LINK : [45] /Equipment/FIFO_acq/sis mcs/Input/Field stop
Field inc (Gauss) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/Field inc
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG1h]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
DAC start (V) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/e1h DAC Start
DAC stop (V) = LINK : [47] /Equipment/FIFO_acq/sis mcs/Input/e1h DAC Stop
DAC inc (V) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/e1h DAC Inc
Settling time (ms) = LINK : [50] /Equipment/FIFO_acq/Pol params/Settling time (ms)

[/History/Display/Fluor]
Factor = FLOAT : 1
Offset = FLOAT : 0
Timescale = STRING : [32] 1d
Zero ylow = BOOL : y
Show run markers = BOOL : y
Log axis = BOOL : n
Variables = STRING : [64] Epics:Fluoresc. Monit 1 Demand
Buttons = STRING[7] :
[32] 10m
[32] 1h
[32] 3h
[32] 12h
[32] 24h
[32] 3d
[32] 7d

[/Script/POL Hold]
cmd = STRING : [128] /home/pol/online/perl/hold.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
hold flag = LINK : [39] /Equipment/FIFO_acq/sis mcs/flags/hold
toggle flag = LINK : [33] /Equipment/FIFO_acq/mdarc/toggle
beamline = STRING : [5] pol
ppg mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name

[/Script/Continue]
cmd = STRING : [128] /home/pol/online/perl/continue.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
hold flag = LINK : [39] /Equipment/FIFO_acq/sis mcs/flags/hold
beamline = STRING : [5] pol

[/Script/Scalers]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 10

[/Script/DAC]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 1h

[/Elog]
Display run number = BOOL : y
Allow delete = BOOL : n
Buttons = STRING[3] :
[32] 8h
[32] 24h
[32] 7d
Types = STRING[20] :
[32] Routine
[32] Shift summary
[32] Minor error
[32] Severe error
[32] Fix
[32] Question
[32] Info
[32] Modification
[32] Reply
[32] Alarm
[32] Test
[32] Other
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
Systems = STRING[20] :
[32] General
[32] DAQ
[32] Detector
[32] Electronics
[32] Target
[32] Beamline
[32] Laser
[32] RF
[32] Cryogenics
[32] UHV
[32] Magnet
[32] Deceleration
[32] Beam Tuning
[32] Documentation
[32] BNMRFIT Program
[32] 
[32] 
[32] 
[32] 
[32] 
Host name = STRING : [256] isdaq01.triumf.ca
Email DAQ = STRING : [45] suz@triumf.ca,renee@triumf.ca,asnd@triumf.ca
Email Documentation = STRING : [32] suz@triumf.ca
SMTP host = STRING : [32] trmail.triumf.ca

[/Analyzer/Output]
Filename = STRING : [256] 124.root
RWNT = BOOL : n
Histo Dump = BOOL : y
Histo Dump Filename = STRING : [256] his%05d.root
Clear histos = BOOL : y
Last Histo Filename = STRING : [256] last.root
Events to ODB = BOOL : y
Global Memory Name = STRING : [8] ONLN

[/Analyzer]
Book N-tuples = BOOL : y

[/Analyzer/CAMP/Common]
Event ID = INT : 13
Trigger mask = INT : -1
Sampling type = INT : 2
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/CAMP/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer/DARC/Common]
Event ID = INT : 14
Trigger mask = INT : -1
Sampling type = INT : 1
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/DARC/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer/INFO/Common]
Event ID = INT : 15
Trigger mask = INT : -1
Sampling type = INT : 1
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/INFO/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer/Bank switches]
CYCI = DWORD : 0
HSCL = DWORD : 0

[/Analyzer/Module switches]
Scaler hist = BOOL : y

[/Analyzer/Cycle_Scalers/Common]
Event ID = INT : 3
Trigger mask = INT : -1
Sampling type = INT : 2
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01.triumf.ca

[/Analyzer/Cycle_Scalers/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer]
Book TTree = BOOL : y
ODB Load = BOOL : y

[/Logger]
Data dir = STRING : [256] /is01_data/pol/data/current
Message file = STRING : [256] midas.log
Write data = BOOL : y
ODB Dump = BOOL : y
ODB Dump File = STRING : [256] 
Auto restart = BOOL : n
Tape message = BOOL : y

[/Logger/Channels/0/Settings]
Active = BOOL : y
Type = STRING : [8] Disk
Filename = STRING : [256] %06d.mid
Format = STRING : [8] MIDAS
Compression = INT : 0
ODB dump = BOOL : y
Log messages = DWORD : 0
Buffer = STRING : [32] SYSTEM
Event ID = INT : -1
Trigger mask = INT : -1
Event limit = DWORD : 0
Byte limit = DOUBLE : 0
Tape capacity = DOUBLE : 0
Subdir format = STRING : [32] 
Current filename = STRING : [256] 000185.mid

[/Logger/Channels/0/Statistics]
Events written = DOUBLE : 0
Bytes written = DOUBLE : 0
Bytes written total = DOUBLE : 2.29008e+07
Files written = INT : 93

[/script_all/NaCell]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 1n

