/*  NaTest.c

Tests NaCell routines used in febnmr.c

NaTest()   tests getting ID, reading/setting
NaCycle()  tests NaSet and NaSet_step

Used with routines in conn.c (use callbacks)


CVS log information:
$Log: NaTest.c,v $
Revision 1.2  2004/11/24 02:01:01  suz
typical_offset param added to EpicsSet and EpicsStep

Revision 1.1  2003/05/02 20:21:29  suz
initial bnmr version; identical to bnmr1 Rev 1.3

Revision 1.3  2002/06/07 18:29:44  suz
replace conn.h by connect.h

Revision 1.2  2002/06/05 17:29:43  suz
use with connect.c and EpicsStep.c

Revision 1.1  2001/11/22 21:15:41  suz
NaCell test program for use with conn.c


*/
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "EpicsStep.h"

#ifndef LOCAL
#define LOCAL static
#endif

void NaTest(void);
void NaCycle(void);
int test_cycle(int Rchan_id, int Wchan_id);

/* globals */
int ddd=0;
int Rchan_id;
int Wchan_id;
char Rname[]="ILE2:BIAS15:RDVOL";
char Wname[]="ILE2:BIAS15:VOL";

/* globals used in test_cycle */
float NaVolt_inc=5;
float NaVolt_start=400;
float NaVolt_stop=450;
int nscans=1;
float typical_stability = 0.3; /* needed by EpicsSet */
float maximum_stability= 0.5;
float minimum_stability =0.05;
float maximum_offset = 0.9;
/* globals used in test_cycle */
float minimum_inc = 0.2; /* 1mV */
float minimum_voltage=0.5;
float maximum_voltage=999.5;


/* Note : NaVolt_diff is calculated from hard-coded parameters */

/* constants for EpicsStep.c (structure defined in EpicsStep.h)  */
 EPICS_CONSTANTS epics_constants = { -1, -1, 0, 0, 0, 0 };
/* NaTest */

float NaCell_get_typical_offset (  float set_point );

void NaTest(void)
{
  int status;
  float value, wvalue=25.1;
  float offset, check_offset, stability;
  float step;
  float typical_offset;
  
  status=caGetId (Rname, Wname, &Rchan_id, &Wchan_id);
  if(status==-1)
    {
      printf("NaTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("caGetId returns Rchan_id = %d and Wchan_id = %d\n",Rchan_id, Wchan_id);
  
  if (caCheck(Rchan_id))
    printf("caCheck says channel %s is connected\n",Rname);
  else
    printf("caCheck says channel %s is NOT connected\n",Rname);
  if (caCheck(Wchan_id))
    printf("caCheck says channel %s is connected\n",Wname);
  else
    printf("caCheck says channel %s is NOT connected\n",Wname);


  /* Read voltage */
  status=caRead(Rchan_id,&value);
  if(status==-1)
    printf("NaTest: Bad status after caRead\n");
  else
    printf("NaTest: Read value as %8.3f\n",value);

  /* write voltage */
  status=caWrite(Wchan_id,&wvalue);
  if(status==-1)
    {
    printf("NaTest: Bad status after caWrite\n");
    caExit();
    return;
    }  
  else  
    printf("Set value to %8.3f\n",wvalue);
    

  printf("INFO: it may take NaCell some time to change voltage if large change\n");



  /* read again */
   status=caRead(Rchan_id,&value);
   if(status==-1)
     printf("NaTest: Bad status after caRead\n");
   else
     printf("NaTest: Read back value as %8.3f\n",value);



     /* test EpicsSet */
  
   /* EpicsSet & EpicsSet_Step go together */
 

   /* load up some conatants needed by EpicsSet and EpicsSet_Step */
   epics_constants.Rchan_id = Rchan_id ;
   epics_constants.Wchan_id = Wchan_id ;
   epics_constants.max_offset = maximum_offset;
   epics_constants.min_set_value = minimum_voltage;
   epics_constants.max_set_value=  maximum_voltage;
   epics_constants.stability  =  typical_stability; 
   epics_constants.debug=ddd;
   printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
   printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	  epics_constants.max_offset,
	  epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	  epics_constants.debug);


     wvalue=20.50; /* set value */
     value=-1;
     typical_offset = NaCell_get_typical_offset (wvalue); /* variable offset */
     printf(" and wvalue=%.2f, typical_offset=%2f\n",wvalue,typical_offset);

     printf("\n\n =======  Now calling EpicsSet to set value to %.2f  V\n",wvalue);
     status = EpicsSet(wvalue, typical_offset, &value, &offset, epics_constants);
     printf("EpicsSet returns with status =%d\n",status);
     printf("      and Read value=%.2f, offset = %.2f\n",value,offset);
     
          
     /* now test EpicsSet_step */
     
     printf("\n\n =========== Now calling EpicsSet_step \n");
     /* test EpicsSet_step */
     
     step=1;
     wvalue = wvalue + 1; /* move by one step from last set value */
     stability = 0.2 * step;
     if(stability < minimum_stability) stability = minimum_stability;
     if(stability > maximum_stability ) stability = maximum_stability;
     check_offset=0.5 + stability;

     typical_offset = NaCell_get_typical_offset (wvalue); /* variable offset */

     printf("Calling EpicsSet_step to step to %.2f V\n",wvalue);
     printf(" typical offset=%.2f, check_offset=%.2f, step=%.2f, stability=%.2f\n"
	    ,typical_offset,check_offset,step,stability);

     status = EpicsSet_step(wvalue, check_offset, typical_offset, step, stability, &value, &offset,  epics_constants);
     printf("EpicsSet_step returns with status =%d\n",status);
     printf("      and Read value=%.2f, offset = %.2f",value,offset);
   
     printf("\nUse NaCycle() to call test_cycle\n");
      caExit(); /* clear any channels that are open */

      /* try testing again if channels are open */

  if (caCheck(Rchan_id))
    printf("caCheck says channel %s is connected\n",Rname);
  else
    printf("caCheck says channel %s is NOT connected\n",Rname);
  if (caCheck(Wchan_id))
    printf("caCheck says channel %s is connected\n",Wname);
  else
    printf("caCheck says channel %s is NOT connected\n",Wname);
}



/* NaLoop */
void NaCycle(void)
{
  int status;

  status=caGetId (Rname, Wname, &Rchan_id, &Wchan_id);
  if(status==-1)
    {
      printf("NaTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("caGetId returns Rchan_id = %d and Wchan_id = %d\n",Rchan_id, Wchan_id);
  
 
  printf("\n\n ========= Now calling test_cycle \n");
   /* test stepping in a loop */
  status=test_cycle(Rchan_id,Wchan_id);
     
 caExit();
   
}


int test_cycle(int Rchan_id, int Wchan_id)
{
  float NaVolt_diff, NaVolt_read, NaVolt_val;
  int NaVolt_ninc;
  int status,icount,i;
  float offset,check_offset,stability;
  float typical_offset=0;
  /* test stepping in a loop */
  
  /* Steps from NaVolt_start to NaVolt_stop with an increment of NaVolt_inc */
  
  printf("test_cycle: starting\n");

   /* load up some conatants needed by EpicsSet and EpicsSet_Step */
   epics_constants.Rchan_id = Rchan_id ;
   epics_constants.Wchan_id = Wchan_id ;
   epics_constants.max_offset = typical_offset + 2 * maximum_stability;
   epics_constants.min_set_value = minimum_voltage;
   epics_constants.max_set_value=  maximum_voltage;
   epics_constants.stability   = typical_stability;
   epics_constants.debug=ddd;
   printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
   printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	  epics_constants.max_offset,
	  epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	  epics_constants.debug);


  
  if (fabs (NaVolt_inc) < minimum_inc )  /* use a minimum increment ( 0.2V) */
    {
      printf("test_cycle: NaVolt increment (%.2f) must be at least %.2fV\n",NaVolt_inc,minimum_inc);
      return(-1) ;
    }
  if   (NaVolt_start < minimum_voltage || NaVolt_stop < minimum_voltage
     || NaVolt_start > maximum_voltage || NaVolt_stop > maximum_voltage)
    {
      printf("test_cycle: NaVolt start or stop parameter(s) out of range. Allowed range is between %.2f and %.2fV\n",
	     minimum_voltage,maximum_voltage);
      return(-1);
    }
  
  if( ((NaVolt_inc > 0) && (NaVolt_stop < NaVolt_start)) ||
      ((NaVolt_inc < 0) && (NaVolt_start < NaVolt_stop)) )
    NaVolt_inc *= -1;
  NaVolt_ninc = ((NaVolt_stop - NaVolt_start)/NaVolt_inc + 1.); 
  if( NaVolt_ninc < 1)
    {
      printf("test_cycle: Invalid number of NaVolt steps: %d\n", NaVolt_ninc);
      return(-1) ;
    }
  printf("=== test_cycle: Scanning NaCell Voltage from %.2f to %.2f by steps of %.2fV\n",
	 NaVolt_start,NaVolt_stop,NaVolt_inc);
  printf("        Number of sweeps = %d; Number of steps = %d\n", nscans+1, NaVolt_ninc);
  NaVolt_val = NaVolt_start;
  
  
       typical_offset = NaCell_get_typical_offset (NaVolt_val); /* variable offset */

  /* set the first value to give it time to stabilize, test NaCell etc (begin run) */
  if(ddd)printf("Test_cycle: calling EpicsSet to set voltage to %.2f\n",NaVolt_val);
  
  status = EpicsSet( NaVolt_val, typical_offset, &NaVolt_read, &offset,  epics_constants);
  if(ddd)
    {
      printf("test_cycle: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.2f, offset = %.2f\n",NaVolt_read,offset);
    }
  
  switch(status)
    {
    case(0):
      printf("** Test_cycle: Success after EpicsSet at setpoint = %.2f \n",NaVolt_start);
      break;
    case(-4):
      printf("Test_cycle: NaCell voltage not responding (could be switched off)\n");
    default:
      return(-1);
    }
  
  
  
  /* stability: see if voltage has changed */
  stability = 0.2 * NaVolt_inc;
  if(stability < minimum_stability) stability = minimum_stability;
  if(stability > maximum_stability ) stability = maximum_stability;
  
  /* NaVolt_diff - used for comparison of read_inc and write_inc */
  if  (fabs (NaVolt_inc) < 10.0 )
    NaVolt_diff = 0.05;
  else
    NaVolt_diff = 0.2; /* default value of 0.2V for comparison */
  
  printf("\nTest_cycle: begin-of-run procedure completed\n\n");
  
  for (i=0; i< nscans ; i++)
    {
      printf("\n      N E W  C Y C L E    (%d)  \n\n",i);  
      icount = 0;
      /* new scan: set point one incr. below NaVolt_start */
      
      NaVolt_val = NaVolt_start - NaVolt_inc;
      if(NaVolt_val <  minimum_voltage || NaVolt_val > maximum_voltage ) NaVolt_val = NaVolt_start + NaVolt_inc;
      typical_offset = NaCell_get_typical_offset (NaVolt_val); /* variable offset */

      if(ddd)
	{
	  printf("Test_cycle: calling EpicsSet to set voltage to %.2f (step %d)\n",NaVolt_val,icount);
	  printf("          i.e. 1 increment away from start voltage\n");
	}
      status = EpicsSet(NaVolt_val,typical_offset, &NaVolt_read, &offset,  epics_constants);
      if(ddd)
	{
	  printf("Test_cycle: EpicsSet returns with status =%d\n",status);
	  printf("      and Read value=%.2f, offset = %.2f\n",NaVolt_read,offset);
	}
      switch(status)
	{
	case(0):
	  printf("** Test_cycle: New cycle - success after EpicsSet at setpoint = %.2f\n",NaVolt_val);
	  break;
	case(-4):
	  printf("Test_cycle: New cycle - NaCell voltage not responding (could be switched off)\n");
	default:
	  return(-1);
	}
  
      while( icount < NaVolt_ninc  )
	{  
	  if(ddd)printf("\n\n ++++++++  Test_cycle:  Scan=%d Step %d, NaVolt_val %.2f ++++++++\n",i,icount,NaVolt_val);
	  /* calculate new NaVolt value */
	  NaVolt_val += NaVolt_inc;
	
	  typical_offset = NaCell_get_typical_offset (NaVolt_val); /* variable offset */
	  
	  check_offset = offset + stability;
	  if(ddd)printf("Test_cycle: calling EpicsSet_step to set voltage to %.2f\n",NaVolt_val);
	  status = EpicsSet_step(NaVolt_val, check_offset, typical_offset, NaVolt_inc, NaVolt_diff, &NaVolt_read, &offset,  epics_constants);
	  if(ddd)
	    {
	      printf("Test_cycle: EpicsSet_step returns with status =%d\n",status);
	      printf("      and Read value=%.2f, offset = %.2f\n",NaVolt_read,offset);
	    }
	  switch(status)
	    {
	    case(0):
	      if(ddd)
		printf("*** Test_cycle: Success - Accepting set point %.2f (read %.2f)\n",NaVolt_val,NaVolt_read);
	      else
		printf("\rTest_cycle: Success - Accepting set point %.2f (read %.2f)",NaVolt_val,NaVolt_read);
	      break;
	    case (-3):
	      if(ddd)
		printf("**** Test_cycle: Accepting set point  %.2f (read %.2f) since read/write values are within allowed offset\n",NaVolt_val,NaVolt_read );
	    case(-4):
	      printf("Test_cycle: NaCell not responding at set point  %.2f (read %.2f) \n", NaVolt_val,NaVolt_read );
	    default:
	      return(-1);
	    }
	  
	  icount++;
	} /* end of while loop */
      
    } /* end of nscans loop */
  
      return(0);
}



float NaCell_get_typical_offset (  float set_point )
{
  float typical_offset;
  /* The NaCell readback offset varies according
     to the voltage. 
     All offsets are in Volts.
  */
  
  if(set_point < 50)       typical_offset=0.3;
  else if(set_point < 75)  typical_offset=0.45;
  else if(set_point < 100) typical_offset=0.4;
  else if(set_point < 175) typical_offset=0.45;
  else if(set_point < 250) typical_offset=0.5;
  else if(set_point < 325) typical_offset=0.55;
  else if(set_point < 400) typical_offset=0.6;
  else if(set_point < 500) typical_offset=0.65;
  else if(set_point < 600) typical_offset=0.7;
  else if(set_point < 700) typical_offset=0.75;
  else                     typical_offset=0.8;
  if(ddd)
    printf("get_typical_offset: typical offset at set_point=%.2f is %.2f\n",
	   set_point,typical_offset);
  return(typical_offset);
}









