/*  pol_dac.h 

parameters for POL's  DAC

*/
#ifdef POL
const float min_DAC = -10.0;
const float max_DAC =  10.0;
const float min_DAC_incr = 0.006;
#endif
