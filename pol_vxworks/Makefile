#####################################################################
#
#  Name:         Makefile
#  Based on Makefile for the example frontend by:   Stefan Ritt
#
#  Contents:     Makefile for MIDAS experiment BNMR or BNQR or POL
#		 Builds object modules needed by frontend 
#
#  Revision history
#
# CVS log information:
# $Log: Makefile,v $
# Revision 1.16  2005/02/23 01:34:42  suz
# fix clean for POL so it works for BNQR
#
# Revision 1.15  2005/02/22 18:08:11  suz
# Build fe_runlog for POL as well
#
# Revision 1.14  2005/02/21 18:29:40  suz
# BNQR now has the PSM module installed in the crate
#
# Revision 1.13  2005/02/18 20:21:58  suz
# dasdevpc test system now has FSC not PSM
#
# Revision 1.12  2004/11/17 00:51:00  suz
# change name of jump.c to scan_jump.c
#
# Revision 1.11  2004/11/17 00:09:01  suz
# add POL's jump scan
#
# Revision 1.10  2004/10/25 19:10:28  suz
# add PSM
#
# Revision 1.7  2004/07/08 17:26:38  renee
# add option listings to get a pre-processor output listing
#
# Revision 1.6  2004/05/20 18:48:19  suz
# now works if EPICS_ACCESS not defined
#
# Revision 1.5  2004/03/31 00:12:47  suz
# no longer need camacnul for Midas 1.9.3
#
# Revision 1.4  2004/02/12 21:35:59  suz
# Makefile now compiles ppg,fsc,sis,vmeio code automatically & executes make_connect; links with camp libs in standard musrdaq/musr/camp area
#
# Revision 1.3  2003/08/14 20:14:40  suz
# add  POL expt
#
# Revision 1.2  2003/05/01 21:55:47  suz
# remove bnmr_common
#
# Revision 1.1  2003/05/01 21:49:28  suz
# initial version supporting all expt types
#
# 
#
#####################################################################

#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the 
# lines below.

#-------------------------------------------------------------------
#####################################################################
#     NOTE:
# BEAMLINE is an environment variable;  must be defined (e.g. in .cshrc)
# It should be  bnmr or bnqr or pol
#####################################################################
#
# The following lines define directories. Adjust if necessary
#                 
# System directories
SYSINC_DIR      = /vw-ppc/include
OBJ_DIR 	= ppcobj
INC_DIR 	= /usr/local/include
LIB_DIR 	= /usr/local/lib
DRV_DIR         = $(MIDASSYS)/drivers/camac
SYSOBJ_DIR 	= $(MIDASSYS)/vxworks/ppcobj
SRC_DIR         = .
BNMR_DIR        = ..
UFE_SOURCE = febnmr.c
JUMP_SOURCE =

# other drivers
SIS_DIR         = $(BNMR_DIR)/sis
VMEIO_DIR       = $(MIDASSYS)/drivers/divers
PPG_DIR         = $(BNMR_DIR)/ppg
FSC_DIR         = $(BNMR_DIR)/fsc
PSM_DIR         = $(BNMR_DIR)/psm
DRV_INC =  -I$(PPG_DIR) -I$(VMEIO_DIR)  -I$(SIS_DIR)
DRV_OBJS =  $(OBJ_DIR)/sis3801.o $(OBJ_DIR)/trPPG.o
CVSPOL = $(BNMR_DIR)/cvspol

# EPICS_OBJ and EPICS_CONNECT must be set blank if EPICS_ACCESS flag is undefined
EPICS_OBJ      =   $(OBJ_DIR)/bnmr_epics.o
EPICS_CONNECT = connect
# CAMP_OBJ must be set blank if CAMP_ACCESS flag is undefined
CAMP_OBJ    =   $(OBJ_DIR)/camp_acq.o
# default DRIVER_INCLUES: no FSC or PSM 
DRIVER_INCLUDES    =  $(DRV_INC)
# FE_RUNLOG must be set blank except for POL
FE_RUNLOG =

ifeq ($(BEAMLINE),)
BEAMLINE = not defined; BNMR is assumed
endif
# default is bnmr on isdaq01 (with FSC)
UFE=febnmr
HARDWARE_FLAGS  = -D BNMR -D TWO_SCALERS -D FSC -D EPICS_ACCESS -D CAMP_ACCESS -D VXWORKS -D VMEIO
DRIVER_INCLUDES    = -I$(FSC_DIR)  $(DRV_INC)
DRIVER_OBJ     =  $(OBJ_DIR)/trFSC.o


## bnqr on isdaq01  now uses PSM  ##
ifeq ($(BEAMLINE),bnqr)
UFE=febnqr
HARDWARE_FLAGS  = -D BNQR -D EPICS_ACCESS -D CAMP_ACCESS -D VXWORKS -D PSM 
DRIVER_INCLUDES   = -I$(PSM_DIR)  $(DRV_INC)
DRIVER_OBJ     =  $(OBJ_DIR)/trPSM.o
endif



## POL expt did not define EPICS_ACCESS, but Phil's expt uses ##
## POL with the NaCell, so POL now needs Epics defined        ##
## Currently POL expt does not use CAMP, FSC or PSM           ##

#pol on isdaq01
ifeq  ($(BEAMLINE),pol)
UFE=fepol
# HARDWARE_FLAGS  = -D POL -D EPICS_ACCESS  -D VXWORKS
HARDWARE_FLAGS  = -D POL   -D VXWORKS
DRIVER_INCLUDES  =  $(DRV_INC)
# DRIVER_OBJ must be set blank as PSM or FSC are not defined 
DRIVER_OBJ     =
# CAMP_OBJ must be set blank as CAMP_ACCESS flag is undefined
CAMP_OBJ    =
FE_RUNLOG = fe_runlog
endif

## TEST system: bnmr on dasdevpc      ##
ifeq ($(HOSTNAME),dasdevpc.triumf.ca)
# test system... bnmr with one scaler and psm, no epics access, small memory PPC
#HARDWARE_FLAGS  = -D BNMR  -D CAMP_ACCESS -D VXWORKS -D PSM -D SMALL_MEMORY -D TEST_SYSTEM
#DRIVER_INCLUDES    = -I$(PSM_DIR)  $(DRV_INC) 
#DRIVER_OBJ     =  $(OBJ_DIR)/trPSM.o

#bnmr on dasdevpc with 1 scaler and FSC
HARDWARE_FLAGS  = -D BNMR  -D CAMP_ACCESS -D VXWORKS -D FSC -D SMALL_MEMORY -D TEST_SYSTEM
DRIVER_INCLUDES    = -I$(FSC_DIR)  $(DRV_INC) 
DRIVER_OBJ     =  $(OBJ_DIR)/trFSC.o

JUMP_SOURCE  =   $(SRC_DIR)/scan_jump.c
# EPICS_OBJ and EPICS_CONNECT must be set blank as EPICS_ACCESS flag is undefined
EPICS_OBJ      =
EPICS_CONNECT =
endif

# experim.h is in current directory
EXPERIM_INC     = .

#-------------------------------------------------------------------------
# ****                              NOTE                             ****
# **** If CAMP_ACCESS flag is defined, VXWORKS must also be defined  ****
# ****                                                               ****
# 
# camp or mud specific for ppc
# for now, ppc version only in bnmr/musr rather than bnmr/online
#
# The following are the directories for the header files needed
MUSR_DIR    = /home/musrdaq/musr
LIBC_DIR  = $(MUSR_DIR)/libc_tw/lib
TCL_DIR =   $(MUSR_DIR)/mvme162/tcl7.3
CAMP_INC    = $(MUSR_DIR)/camp/src
LIBC_TW_INC = $(MUSR_DIR)/libc_tw/src
CAMP_INCLUDES = -I$(CAMP_INC) -I$(LIBC_TW_INC) -I$(TCL_DIR)
#-----------------------------------------
# This is for Linux
#OSFLAGS = -DOS_LINUX -Dextname
#CFLAGS = 
# 
# for f2c, uncomment following:
#LIBS = -lbsd -lm -lutil /usr/lib/libf2c.a
#FF = cc
# for egcs g77, use this
#FF = g77
#LIBS = -lm -lz -lutil -lnsl
# compiler
#CC = cc
#CFLAGS += -g -I$(INC_DIR) $(HARDWARE_FLAGS)
#LDFLAGS +=
#-------------------------------------------------------------------------
# This is for VxWorks
# Common flags
# -save-temps
CC_COMPILER	 = -ansi -fstrength-reduce -fkeep-inline-functions
CC_DEBUGGING     = -g
CC_WARNINGS_ALL  = -Wall 
CC_WARNINGS_NORMAL = 
CC_WARNINGS_NONE =
CC_OPTIM_NORMAL  = -O -finline-functions -fkeep-inline-functions
CC_OPTIM_TARGET  = -O -fvolatile

CC = /vxworks-ppc/bin/ccppc
LD = /vxworks-ppc/bin/ldppc

# internally defined by X-compile
# CPU= -DPPC604

# Define Current Configuration
CC_OPTIM	 = $(CC_OPTIM_NORMAL)
CC_INCLUDES      = -I. -I$(INC_DIR) -I$(SYSINC_DIR)
CC_WARNINGS	 = $(CC_WARNINGS_NORMAL)
CC_DEFINES	 = -DOS_VXWORKS $(CPU) -DPPCxxx

CFLAGS = $(CC_OPTIM) $(CC_WARNINGS) $(CC_INCLUDES) $(CC_COMPILER) $(CC_DEFINES) $(HARDWARE_FLAGS) 
#        
#-----------------------
# Overwrite MAX_EVENT_SIZE with environment variable
#
# Make sure PPC frontend used MIDAS_MAX_EVENT_SIZE of  1000000 
#       (same as we have set in $(MIDASSYS)/vxworks/makefile.ppc_tri )
MIDAS_MAX_EVENT_SIZE = 1000000
CFLAGS += -DMAX_EVENT_SIZE=$(MIDAS_MAX_EVENT_SIZE)
         
####################################################################
# Lines below here should not be edited (really?)
####################################################################

LIB = $(LIB_DIR)/libmidas.a
INCLUDES = -I$(EXPERIM_INC) -I$(INC_DIR) $(CAMP_INCLUDES) $(DRIVER_INCLUDES)

all: $(EPICS_CONNECT) info $(DRV_OBJS) $(DRIVER_OBJ)  $(OBJ_DIR)/$(UFE) $(FE_RUNLOG)

#For Linux
#$(OBJ_DIR)/$(UFE): $(LIB)  $(LIB_DIR)/mfe.o \
#	$(OBJ_DIR)/bnmr_epics.o $(OBJ_DIR)/bnmr_init.o $(OBJ_DIR)/vxdummy.o
#	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/$(UFE).o $(UFE_SOURCE)
#	$(CC) -o $(OBJ_DIR)/$(UFE)  $(LIB_DIR)/mfe.o  $(OBJ_DIR)/$(UFE).o \
#	$(OBJ_DIR)/bnmr_epics.o $(OBJ_DIR)/bnmr_init.o $(OBJ_DIR)/vxdummy.o \
#	$(LIB) $(LDFLAGS) $(LIBS)
#$(OBJ_DIR)/vxdummy.o: vxdummy.c
#	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/vxdummy.o vxdummy.c


#For VxWorks
$(OBJ_DIR)/$(UFE): $(LIB)  $(SYSOBJ_DIR)/mfe.o  $(OBJ_DIR)/$(UFE).o $(CAMP_OBJ)\
	$(EPICS_OBJ) $(OBJ_DIR)/bnmr_init.o $(OBJ_DIR)/vmeio.o 

	$(CC) -o $(OBJ_DIR)/mfe.o  $(SYSOBJ_DIR)/mfe.o  $(OBJ_DIR)/$(UFE).o \
	$(EPICS_OBJ) $(OBJ_DIR)/bnmr_init.o $(CAMP_OBJ) $(OBJ_DIR)/vmeio.o
# $(DRIVER_OBJ)


#For both VxWorks and Linux
$(OBJ_DIR)/$(UFE).o : $(UFE_SOURCE)  $(JUMP_SOURCE)
	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/$(UFE).o $(UFE_SOURCE)

$(EPICS_OBJ): bnmr_epics.c
	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(EPICS_OBJ) bnmr_epics.c

$(OBJ_DIR)/bnmr_init.o: bnmr_init.c
	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/bnmr_init.o bnmr_init.c

$(OBJ_DIR)/camp_acq.o: camp_acq.c
	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/camp_acq.o camp_acq.c


# hardware drivers 
$(OBJ_DIR)/vmeio.o: $(VMEIO_DIR)/vmeio.c
	$(CC) $(CFLAGS) $(INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/vmeio.o $(VMEIO_DIR)/vmeio.c


$(OBJ_DIR)/sis3801.o: $(SIS_DIR)/sis3801.c
	$(CC) $(CFLAGS) $(DRV_INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/sis3801.o $(SIS_DIR)/sis3801.c

$(OBJ_DIR)/trPPG.o: $(PPG_DIR)/trPPG.c
	$(CC) $(CFLAGS) $(DRV_INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/trPPG.o $(PPG_DIR)/trPPG.c

$(OBJ_DIR)/trFSC.o: $(FSC_DIR)/trFSC.c
	$(CC) $(CFLAGS) $(DRV_INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/trFSC.o $(FSC_DIR)/trFSC.c

$(OBJ_DIR)/trPSM.o: $(PSM_DIR)/trPSM.c
	$(CC) $(CFLAGS) $(DRV_INCLUDES) $(OSFLAGS) -c -o $(OBJ_DIR)/trPSM.o $(PSM_DIR)/trPSM.c

$(OBJ_DIR):
	@if [ ! -d  $(OBJ_DIR) ] ; then \
           echo "Making directory $(OBJ_DIR)" ; \
           mkdir $(OBJ_DIR); \
        fi;
connect :
	make  -w -f make_connect

fe_runlog :
	@echo " "
	@echo " Now building $(FE_RUNLOG)   ";
	make -f $(CVSPOL)/Makefile
info :
	@echo " *************************************************************************************";
	@echo " *   Building frontend  \"$(UFE)\" for beamline \"$(BEAMLINE)\"  *" ;
	@echo " *   using current directory for experim.h and frontend source file \"$(UFE_SOURCE)\"     *";
	@echo " *   with hardware flags:                                                            *"; 
	@echo " *     \"$(HARDWARE_FLAGS)\"            * ";
	@echo " *                                                                                   *";
	@echo " *                                                                                   *";
	@echo " *   If hardware flags are changed, \"make touch\" first, then \"make\"                  *";
	@echo " +   For a listing, use \"make listing\"                                              *";
#	@echo " *   Frontend also uses code generated by : \"make -f Make_connect\"                   *";
	@echo " *************************************************************************************";
touch:
	touch $(SRC_DIR)/*.c $(SIS_DIR)/sis3801.c $(PSM_DIR)/trPSM.c $(FSC_DIR)/trFSC.c $(PPG_DIR)/trPPG.c 

psm:
	@echo "...touching $(PSM_DIR)/trPSM.c /"
	touch $(PSM_DIR)/trPSM.c
	make all

clean:
	@echo "...deleting all object modules in $(OBJ_DIR)/"
	rm -f $(OBJ_DIR)/*.o *~ \#*
	@echo "...now copying camp library $(MUSR_DIR)/camp/ppc-vxworks/libcamp.o to $(OBJ_DIR)/ "
	cp $(MUSR_DIR)/camp/ppc-vxworks/libcamp.o $(OBJ_DIR)
	@echo " "
ifeq  ($(BEAMLINE),pol)
	 make -f  $(CVSPOL)/Makefile clean
endif
#
#	rm -f $(OBJ_DIR)/bnmr_epics.o $(OBJ_DIR)/bnmr_init.o \
#$(OBJ_DIR)/camp_acq.o $(OBJ_DIR)/$(UFE).o *~ \#*

listing:
	$(CC) -E  $(CFLAGS) $(INCLUDES) $(UFE_SOURCE) >$(UFE).l







