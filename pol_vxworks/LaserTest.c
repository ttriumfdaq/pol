/*  LaserTest.c

Tests Dye Laser routines used in febnmr.c

LaserTest()   tests getting ID, reading/setting
LaserCycle()  tests EpicsSet and EpicsSet_step

Used with routines in conn.c (use callbacks)


CVS log information:
$Log: LaserTest.c,v $
Revision 1.2  2004/11/24 02:12:11  suz
typical_offset param added to EpicsSet and EpicsStep

Revision 1.1  2003/05/02 20:24:15  suz
initial bnmr version; identical to bnmr1 v1.2

Revision 1.2  2002/06/07 18:30:30  suz
replace conn.h by connect.h

Revision 1.1  2002/06/05 17:30:43  suz
Dye Laser (epics device) test program

Revision 1.1  2001/11/22 21:15:41  suz
Dye Laser test program for use with conn.c


*/
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "EpicsStep.h"

#ifndef LOCAL
#define LOCAL static
#endif

void LaserTest(void);
void LaserCycle(void);
int test_cycle(int Rchan_id, int Wchan_id);

/* globals */
int dd=0;
int Rchan_id;
int Wchan_id;
char Rname[]="ILE2:RING:FREQ:RDVOL";
char Wname[]="ILE2:RING:FREQ:VOL";


/* Make values easy to change as the hardware for the
   dye laser is not yet available */

/* parameters used in LaserTest: */

/* for Dye Laser, minimum step size is supposed to be 1mV */
float typical_offset  ; /* constant, set to 70% of maximum */
const float maximum_offset = 0.004;
const float minimum_stability = 0.0005;     /* Dye Laser */
const float maximum_stability = 0.005;   /*  Dye Laser */
float typical_stability = 0.001; /* needed for EpicsSet */
float wvalue1 = -3.0;
float   wvalue2 = 2.0; /* set a second value */
float step1 = 0.05;

/* globals used in test_cycle */
const float minimum_inc = 0.001; /* 1mV */
const float minimum_voltage=-5;
const float maximum_voltage=+5;

/* parameters for LaserCycle */
float LaVolt_inc=0.1;
float LaVolt_start=-3;
float LaVolt_stop=+3;
int nscans=2;

/* Note : LaVolt_diff is calculated from hard-coded parameters */

/* constants for EpicsStep.c (structure defined in EpicsStep.h)  */
EPICS_CONSTANTS epics_constants = { -1, -1, 0, 0, 0, 0 };


/* LaserTest */
void LaserTest(void)
{
  int status;
  float value;
  float offset, check_offset, stability;
  
  
  typical_offset = 0.7 * maximum_offset; /* constant offset, 70% of maximum */
  status=caGetId (Rname, Wname, &Rchan_id, &Wchan_id);
  if(status==-1)
    {
      printf("LaserTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("\ncaGetId returns Rchan_id = %d and Wchan_id = %d\n\n",Rchan_id, Wchan_id);
  
  if (caCheck(Rchan_id))
    printf("caCheck says channel %s is connected\n",Rname);
  else
    printf("caCheck says channel %s is NOT connected\n",Rname);
  if (caCheck(Wchan_id))
    printf("caCheck says channel %s is connected\n",Wname);
  else
    printf("caCheck says channel %s is NOT connected\n",Wname);
  
  
  /* Read voltage */
  printf("\n   Now reading voltage \n");
  status=caRead(Rchan_id,&value);
  if(status==-1)
    {
      printf("LaserTest: Bad status after caRead\n");
      caExit();
      return;
    }


  printf("LaserTest: Read value as %8.3f\n",value);
          
  /* write voltage */
  printf("\n   Now about to set voltage to  %f \n",wvalue1);
  status=caWrite(Wchan_id,&wvalue1);	
  if(status==-1)
    {
      printf("LaserTest: Bad status after caWrite\n");
      caExit();
      return;
    }    
  printf("Set value to %8.3f\n",wvalue1);    

  printf("INFO: it may take Dye Laser some time to change voltage if large change\n");



  /* read again */
  status=caRead(Rchan_id,&value);
  if(status==-1)
    printf("LaserTest: Bad status after caRead\n");
  else
    printf("LaserTest: Read back value as %8.3f\n",value);


  /* test EpicsSet */
  /* EpicsSet & EpicsSet_Step go together */
  
  /* load up some conatants needed by EpicsSet and EpicsSet_Step */
  epics_constants.Rchan_id = Rchan_id ;
  epics_constants.Wchan_id = Wchan_id ;
  epics_constants.max_offset = maximum_offset ;
  epics_constants.min_set_value = minimum_voltage;
  epics_constants.max_set_value=  maximum_voltage;
  epics_constants.stability = typical_stability;
  epics_constants.debug=dd;
  if(dd)
    {
      printf("Typical stability: %.4f\n",typical_stability);
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	      epics_constants.debug);
    }
  /* set a second value (wvalue2) */
  value=-1;
  printf("\n\n =======  Now calling EpicsSet to set value to %.4f  V\n",wvalue2);
  status = EpicsSet(wvalue2, typical_offset, &value, &offset, epics_constants);
  printf("EpicsSet returns with status =%d\n",status);
  printf("      and Read value=%.4f, offset = %.4f\n",value,offset);
  
  
  /* now test EpicsSet_step */
  
  printf("\n\n =========== Now calling EpicsSet_step \n");
  /* test EpicsSet_step */
  
  
  step1=1;
  wvalue2 = wvalue2 + step1; /* move by one step from last set value */
  stability = 0.2 * step1;
  if(stability < minimum_stability) stability = minimum_stability;
  if(stability > maximum_stability ) stability = maximum_stability;
  check_offset=typical_offset + stability;
  
  
  printf("Calling EpicsSet_step to step to %.4f V\n",wvalue2);
  status = EpicsSet_step(wvalue2, check_offset, typical_offset, step1, stability, &value, 
			 &offset, epics_constants);
  printf("EpicsSet_step returns with status =%d\n",status);
  printf("      and Read value=%.4f, offset = %.4f",value,offset);
  
  printf("\nYou can use LaserCycle() command to call test_cycle\n");
  
  
  printf("\n\nNow calling caExit to clear any open channels\n");
  caExit(); /* clear any channels that are open */
  
  /* try testing again if channels are open */
  
  printf("Now testing to see if channels are open:\n");
  if (caCheck(Rchan_id))
    printf("caCheck says channel %s is connected\n",Rname);
  else
    printf("caCheck says channel %s is NOT connected\n",Rname);
  if (caCheck(Wchan_id))
    printf("caCheck says channel %s is connected\n",Wname);
  else
    printf("caCheck says channel %s is NOT connected\n",Wname);
}



/* LaserLoop */
void LaserCycle(void)
{
  int status;
  

  status=caGetId (Rname, Wname, &Rchan_id, &Wchan_id);
  if(status==-1)
    {
      printf("LaserTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("caGetId returns Rchan_id = %d and Wchan_id = %d\n",Rchan_id, Wchan_id);
  
 
  printf("\n\n ========= Now calling test_cycle \n");
  /* test stepping in a loop */
  status=test_cycle(Rchan_id,Wchan_id);
     
  caExit();
   
}


int test_cycle(int Rchan_id, int Wchan_id)
{
  float LaVolt_diff, LaVolt_read, LaVolt_val;
  int LaVolt_ninc;
  int status,icount,i;
  float offset,check_offset,stability;

  typical_offset = 0.7 * maximum_offset; /* constant offset, 70% of maximum */
  typical_stability = 0.005;

  /* test stepping in a loop */
  printf("Typical stability: %.4f\n",typical_stability);

  /* load up some constants needed by EpicsSet and EpicsSet_Step */
  epics_constants.Rchan_id = Rchan_id ;
  epics_constants.Wchan_id = Wchan_id ;
  epics_constants.max_offset = maximum_offset;
  epics_constants.min_set_value = minimum_voltage;
  epics_constants.max_set_value=  maximum_voltage;
  epics_constants.stability = typical_stability;
  epics_constants.debug=dd;
  
  printf("Typical stability: %.4f\n",typical_stability);
  printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
  printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	 epics_constants.max_offset,
	 epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	 epics_constants.debug);
  
  
  /* Steps from LaVolt_start to LaVolt_stop with an increment of LaVolt_inc */
  
  printf("test_cycle: starting... stepping from %.4fV to %.4fV in steps of %.4fV\n",
	 LaVolt_start,LaVolt_stop,LaVolt_inc);
  
  if (fabs (LaVolt_inc) < minimum_inc )  /* use a minimum increment  */
    {
      printf("test_cycle: Laser Voltage increment (%.4f) must be at least %.4fV\n",LaVolt_inc,minimum_inc);
      return(-1) ;
    }
  if(LaVolt_start < minimum_voltage  || LaVolt_stop <  minimum_voltage || 
     LaVolt_start > maximum_voltage  || LaVolt_stop >  maximum_voltage)
    {
      printf("test_cycle: Laser Voltage start or stop parameter(s) out of range. Allowed range is between %.4f and %.4fV\n",
	     minimum_voltage, maximum_voltage);
      return(-1);
    }
  
  if( ((LaVolt_inc > 0) && (LaVolt_stop < LaVolt_start)) ||
      ((LaVolt_inc < 0) && (LaVolt_start < LaVolt_stop)) )
    LaVolt_inc *= -1;
  LaVolt_ninc = ((LaVolt_stop - LaVolt_start)/LaVolt_inc + 1.); 
  if( LaVolt_ninc < 1)
    {
      printf("test_cycle: Invalid number of Laser Voltage steps: %d\n", LaVolt_ninc);
      return(-1) ;
    }
  printf("=== test_cycle: Selected %d Laser Voltage cycles starting at %.4f v by steps of %.4f v\n",
	 LaVolt_ninc,LaVolt_start,LaVolt_inc);
  LaVolt_val = LaVolt_start;
  
  
  
  /* set the first value to give it time to stabilize, test Dye Laser etc (begin run) */
  if(dd)printf("Test_cycle: calling EpicsSet to set voltage to %.4f\n",LaVolt_val);
  
  status = EpicsSet( LaVolt_start, typical_offset, &LaVolt_read, &offset,epics_constants);
  if(dd)
    {
      printf("test_cycle: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.4f, offset = %.4f\n",LaVolt_read,offset);
    }
  
  switch(status)
    {
    case(0):
      printf("** Test_cycle: Success after EpicsSet at setpoint = %.4f \n",LaVolt_start);
      break;
    case(-4):
      printf("Test_cycle: Dye Laser voltage not responding (could be switched off)\n");
    default:
      return(-1);
    }
  
  
  
  /* stability: to see if voltage has changed */
  stability = 0.2 * LaVolt_inc;
  if(stability < minimum_stability) stability =  minimum_stability;
  if(stability >  maximum_stability ) stability = maximum_stability ;
  
  /* LaVolt_diff - used for comparison of read_inc and write_inc
     for now divided everything by 1000 cf NaCell
   */
  if  (fabs (LaVolt_inc) < 0.01 )
    LaVolt_diff = 0.0005;
  else
    LaVolt_diff = 0.002; /* default value of 0.2V for comparison */
  
  printf("\nTest_cycle: begin-of-run procedure completed\n\n");
  
  for (i=0; i< nscans ; i++)
    {
      printf("\n      N E W  C Y C L E  (%d)    \n\n",i);  
      icount = 0;
      /* new scan: set point one incr. below LaVolt_start */
      
      LaVolt_val = LaVolt_start - LaVolt_inc;
      if(LaVolt_val < minimum_voltage || LaVolt_val > maximum_voltage ) 
	LaVolt_val = LaVolt_start + LaVolt_inc;
      
      if(dd)printf("Test_cycle: calling EpicsSet to set voltage to %.4f (step %d)\n",LaVolt_val,icount);
      status = EpicsSet( LaVolt_val, typical_offset, &LaVolt_read, &offset,epics_constants);
      if(dd)
	{
	  printf("Test_cycle: EpicsSet returns with status =%d\n",status);
	  printf("      and Read value=%.4f, offset = %.4f\n",LaVolt_read,offset);
	}
      switch(status)
	{
	case(0):
	  printf("** Test_cycle: New cycle - success after EpicsSet at setpoint = %.4f\n",LaVolt_val);
	  break;
	case(-4):
	  printf("Test_cycle: New cycle - Dye Laser voltage not responding (could be switched off)\n");
	default:
	  return(-1);
	}
  
      while( icount < LaVolt_ninc  )
	{  
	  if(dd)printf("\n\n ++++++++  Test_cycle:  Scan=%d Step %d, LaVolt_val %.4f ++++++++\n",i,icount,LaVolt_val);
	  /* calculate new LaVolt value */
	  LaVolt_val += LaVolt_inc;
	
	  
	  check_offset = offset + stability;
	  if(dd)printf("Test_cycle: calling EpicsSet_step to set voltage to %.4f\n",LaVolt_val);
	  status = EpicsSet_step( LaVolt_val, check_offset, typical_offset, LaVolt_inc, LaVolt_diff, 
				  &LaVolt_read, &offset,epics_constants);
	  if(dd)
	    {
	      printf("Test_cycle: EpicsSet_step returns with status =%d\n",status);
	      printf("      and Read value=%.4f, offset = %.4f\n",LaVolt_read,offset);
	    }
	  switch(status)
	    {
	    case(0):
	      if(dd)
		printf("*** Test_cycle: Success - Accepting set point %.4f (read %.4f)\n",LaVolt_val,LaVolt_read);
	      else
		printf("\rTest_cycle: Success - Accepting set point %.4f (read %.4f)      ",LaVolt_val,LaVolt_read);
	      break;
	    case (-3):
	      if(dd)
		printf("**** Test_cycle: Accepting set point  %.4f (read %.4f) since read/write values are within allowed offset\n",LaVolt_val,LaVolt_read );
	    case(-4):
	      printf("Test_cycle: Dye Laser not responding at set point  %.4f (read %.4f) \n", LaVolt_val,LaVolt_read );
	    default:
	      return(-1);
	    }
	  
	  icount++;
	} /* end of while loop */
      
    } /* end of nscans loop */
  
      return(0);
}













