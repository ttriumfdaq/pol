#ifdef POL
/* POL only */
/*********************************************************************************/
INT BiasReconnect(void)
/*********************************************************************************/
{
  INT status;
  
#ifdef EPICS_ACCESS
  INT max_err=1;
  INT icount;
  BOOL connected;
  
  printf("\nBiasReconnect: ***  reconnecting to Epics device to read Source HV Bias (%s).....\n",Rbias);
  
  icount=0;
  
  connected=FALSE;
  
  while ( ! connected && icount < max_err )
    {
      /* Clear old channel if open */
      if(Rchid_Bias != -1)
	ca_clear_channel(Rchid_Bias);
      status =caGetSingleId(Rbias, &Rchid_Bias );
      if(status==0)
	connected=TRUE;
      
      if(connected) 
	break;
      icount++;
      printf("BiasReconnect: could not connect to Epics HV bias (%s).....waiting 15s and retrying (%d)\n",
	     Rbias,icount);
      if(icount < max_err)
	iwait(15); /* wait 15s before retrying */
    }
  if(connected)
    {
      printf("BiasReconnect: successfully reconnected to HV bias\n");
      return SUCCESS;
    }
  
  cm_msg(MERROR,"BiasReconnect",
	 "Could not reconnect to Epics HV bias (%s). Epics system may not be available ",Rbias);
  return (-1);

#else
  printf("BiasReconnect: Epics Access is not defined -> DUMMY access to Epics in this code\n");
  return SUCCESS;
#endif 
}


/* Note: Bias watchdog should not be needed, Bias read often enough to keep channel open */


/*********************************************************************************/
void BiasDisconnect(void)
/*********************************************************************************/
{
 if(Rchid_Bias != -1) 
   ca_clear_channel(Rchid_Bias);
 Rchid_Bias=-1;
 return;
}

/*********************************************************************************/
INT Bias_init(INT code)
/*********************************************************************************/
{
  INT status,i;
  float value;
  /* Set ID for Bias, depending on code
     code = 0  OLIS
            1  ISAC-W
            2  ISAC-E
 
       Set the Midas Watchdog to a long time before calling !   */

#ifdef EPICS_ACCESS
  switch (code)
    {
    case 0:
      sprintf(Rbias,"%s",R_IOS);
      break;
    case 1:
      sprintf(Rbias,"%s",R_ITW);
      break;
    case 2:
      sprintf(Rbias,"%s",R_ITE);
      break;
    default:
      cm_msg(MERROR,"Bias_init","Invalid HV source code (%d)",code);
      return (DB_INVALID_PARAM);
    }
  printf("Bias_init: Epics Channel \"%s\" will be used for HV Bias\n",RBias);

  if(Rchid_Bias > 0)
    {
      /* Should have been cleared at end of last run */
      printf("Bias_init: channel ID for HV Bias is already defined; clearing the channel...\n");
      ca_clear_channel(Rchid_Bias);
      Rchid_Bias=-1;
    }
  /* get the ID for HV Bias */
 
  Rchid_Bias=-1;
  status = caGetSingleId(Rbias, &Rchid_Bias );
  if(status==-1)
    {
      printf("Bias_init: Bad status after caGetID for HV Bias (\"%s\") \n",Rbias);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }




  if(d7)printf("Calling read_Epics_chan for HV Bias\n");
  status = read_Epics_chan(Rchid_Bias,  &value);
  i=(INT)value;
  if(status != SUCCESS)
    printf("Bias_init: failure from read_Epics_chan for HV Bias\n");
  else
    if(d7)printf("Bias_init: HV Bias reads %d\n",i);
#else
  printf("Bias_init: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return SUCCESS;
}
#endif /* POL */
