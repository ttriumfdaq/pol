[/System/Clients/1267]
Name = STRING : [32] Logger
Host = STRING : [256] isdaq01
Hardware type = INT : 42
Server Port = INT : 46170
Transition START = INT : 200
Transition STOP = INT : 800
Transition PAUSE = INT : 800
Transition RESUME = INT : 200

[/System/Clients/1267/RPC]
14000 = BOOL : y

[/System/Clients/1271]
Name = STRING : [32] mhttpd
Host = STRING : [256] isdaq01
Hardware type = INT : 42
Server Port = INT : 46171

[/System/Clients/1276]
Name = STRING : [32] Lazy_Disk
Host = STRING : [256] isdaq01
Hardware type = INT : 42
Server Port = INT : 46174

[/System/Clients/1280]
Name = STRING : [32] rf_config
Host = STRING : [256] isdaq01
Hardware type = INT : 42
Server Port = INT : 46176
Transition START = INT[2] :
[0] 350
[1] 400

[/System/Clients/1309]
Name = STRING : [32] fe_runlog
Host = STRING : [256] isdaq01
Hardware type = INT : 42
Server Port = INT : 46189
Transition START = INT[2] :
[0] 500
[1] 450
Transition STOP = INT[3] :
[0] 500
[1] 600
[2] 500
Transition PAUSE = INT : 500
Transition RESUME = INT : 500

[/System/Clients/1470]
Name = STRING : [32] fePOL
Host = STRING : [256] vwisac2
Hardware type = INT : 50
Server Port = INT : 1028
Link timeout = INT : 10000
Transition START = INT : 500
Transition STOP = INT[2] :
[0] 500
[1] 750
Transition PAUSE = INT : 500
Transition RESUME = INT : 500

[/System/Clients/8029]
Name = STRING : [32] ODBEdit
Host = STRING : [256] isdaq01
Hardware type = INT : 42
Server Port = INT : 46375

[/System/Clients/8405]
Name = STRING : [32] feDVM
Host = STRING : [256] midtis07
Hardware type = INT : 42
Server Port = INT : 33725
Link timeout = INT : 10000
Transition START = INT : 500
Transition STOP = INT : 500
Transition PAUSE = INT : 500
Transition RESUME = INT : 500

[/System/Tmp/11871O]
Tape Data Append = BOOL : y

[/System/Tmp/25645O]
Tape Data Append = BOOL : y

[/System]
Client Notify = INT : 0
Prompt = STRING : [256] [%h:%e:%s]%p>

[/Programs/ODBEdit]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 0

[/Programs/rf_config]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/rf_config  -e pol -D
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : y
Alarm class = STRING : [32] Caution
First failed = DWORD : 0

[/Programs]
Execute on start run = STRING : [256] 

[/Programs/mheader]
Required = BOOL : n
Watchdog timeout = INT : 60000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/bin/start-daq-tasks
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 1088463679

[/Programs/mhttpd]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 0

[/Programs]
Execute on stop run = STRING : [256] odb -c 'set  "/Equipment/fifo_acq/client flags/client alarm" 0'

[/Programs/Logger]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] mlogger -D
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : y
Alarm class = STRING : [32] Caution
First failed = DWORD : 0

[/Programs/fePOL]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 0

[/Programs/mdump]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1256349273

[/Programs/Lcrplot]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1059687843

[/Programs/Analyzer]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/bin/start_analyzer
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 1242840641

[/Programs/fe_runlog]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] /home/pol/online/pol/bin/start_fe_runlog
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : y
Alarm class = STRING : [32] Caution
First failed = DWORD : 0

[/Programs/test_dvm]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1099594608

[/Programs/feDVM]
Required = BOOL : y
Watchdog timeout = INT : 10000
Check interval = DWORD : 10000
Start command = STRING : [256] ssh pol@midtis07.triumf.ca "cd slow/dvm; ./start_feDVM"
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] Caution
First failed = DWORD : 0

[/Programs/test_dvm_scan]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1099594608

[/Programs/feDVMtest]
Required = BOOL : n
Watchdog timeout = INT : 60000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1254255565

[/Programs/Lazy_Disk]
Required = BOOL : n
Watchdog timeout = INT : 60000
Check interval = DWORD : 180000
Start command = STRING : [256] lazylogger -c Disk -D
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : y
Alarm class = STRING : [32] Caution
First failed = DWORD : 0

[/Programs/rootana]
Required = BOOL : n
Watchdog timeout = INT : 60000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1256347715

[/Programs/mtransition]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1267657656

[/Programs/Speaker]
Required = BOOL : n
Watchdog timeout = INT : 10000
Check interval = DWORD : 180000
Start command = STRING : [256] 
Auto start = BOOL : n
Auto stop = BOOL : n
Auto restart = BOOL : n
Alarm class = STRING : [32] 
First failed = DWORD : 1259182763

[/Experiment]
Name = STRING : [32] pol

[/Experiment/Buffer sizes]
SYSMSG = DWORD : 100000
SYSTEM = DWORD : 8388608

[/Experiment/Edit on start]
run_title = STRING : [88] test
experiment number = DWORD : 145401856
experimenter = STRING : [32] 
sample = STRING : [15] 
orientation = STRING : [15] 
temperature = STRING : [15] 
field = STRING : [15] 
Element = STRING : [24] 
Mass = INT : 0
DC offset(V) = INT : 0
Ion source (kV) = DOUBLE : 0
Laser wavelength (nm) = DOUBLE : 0
write data = LINK : [35] /Logger/Channels/0/Settings/Active
Number of scans = LINK : [47] /Equipment/FIFO_acq/sis mcs/hardware/num scans
Source HV Bias = STRING : [12] ISAC-WEST
Edit run number = BOOL : n
Pedestals run = BOOL : n

[/Experiment/Lock when running]
dis_rn_check = LINK : [51] /Equipment/FIFO_acq/mdarc/disable run number check
SIS test mode = LINK : [42] /Equipment/FIFO_acq/sis mcs/sis test mode
PPGinput = LINK : [34] /Equipment/FIFO_acq/sis mcs/Input
SIS ref A = LINK : [65] /Equipment/FIFO_acq/sis mcs/Hardware/Enable SIS ref ch1 scaler A
SIS ref B = LINK : [65] /Equipment/FIFO_acq/sis mcs/Hardware/Enable SIS ref ch1 scaler B

[/Experiment/Parameter Comments]
Active = STRING : [36] <i>Enter y to save data to disk</i>
Num scans = STRING : [80] <i>Stop run after num scans is reached. Enter 0 to disable (free running)</i>
Num cycles = STRING : [80] <i>Stop run after num cycles is reached. Enter 0 to disable (free running)</i>
Source HV Bias = STRING : [80] <i>Enter one of 'OLIS', 'ISAC-WEST' or 'ISAC-EAST' </i>

[/Experiment]
Transition debug flag = INT : 0
Transition connect timeout = INT : 10000
Transition timeout = INT : 120000

[/Experiment/security]
Web Password = STRING : [32] mim2P4OCV.PW3

[/Logger]
Data dir = STRING : [256] /data1/pol/data/current
Message file = STRING : [256] midas.log
Write data = BOOL : y
ODB Dump = BOOL : y
ODB Dump File = STRING : [256] 
Auto restart = BOOL : y
Tape message = BOOL : y

[/Logger/Channels/0/Settings]
Active = BOOL : y
Type = STRING : [8] Disk
Filename = STRING : [256] %06d.mid
Format = STRING : [8] MIDAS
Compression = INT : 0
ODB dump = BOOL : y
Log messages = DWORD : 0
Buffer = STRING : [32] SYSTEM
Event ID = INT : -1
Trigger mask = INT : -1
Event limit = DOUBLE : 0
Byte limit = DOUBLE : 0
Subrun Byte limit = DOUBLE : 0
Tape capacity = DOUBLE : 0
Subdir format = STRING : [32] 
Current filename = STRING : [256] 000005.mid

[/Logger/Channels/0/Statistics]
Events written = DOUBLE : 17
Bytes written = DOUBLE : 222284
Bytes written uncompressed = DOUBLE : 222284
Bytes written total = DOUBLE : 143081108
Bytes written subrun = DOUBLE : 138412032
Files written = DOUBLE : 246

[/Logger]
History dir = STRING : [32] /data1/pol/log
Elog dir = STRING : [32] /data1/pol/elog
Auto restart delay = INT : 0

[/Logger/SQL]
Create database = BOOL : n
Write data = BOOL : n
Hostname = STRING : [80] localhost
Username = STRING : [80] root
Password = STRING : [80] 
Database = STRING : [32] pol
Table = STRING : [80] Runlog
Logfile = STRING : [80] sql.log

[/Logger/SQL/Links BOR]
Run number = LINK : [20] /Runinfo/Run number
Start time = LINK : [20] /Runinfo/Start time

[/Logger/SQL/Links EOR]
Stop time = LINK : [19] /Runinfo/Stop time

[/Logger]
ODBC_Debug = INT : 0
ODBC_DSN = STRING : [256] 

[/Runinfo]
State = INT : 1
Online Mode = INT : 1
Run number = INT : 5
Transition in progress = INT : 0
Start abort = INT : 0
Requested transition = INT : 0
Start time = STRING : [32] Wed Mar  3 15:08:32 2010
Start time binary = DWORD : 1267657712
Stop time = STRING : [32] Wed Mar  3 15:08:49 2010
Stop time binary = DWORD : 1267657729

[/Alarms]
Alarm system active = BOOL : y

[/Alarms/Alarms/Demo ODB]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 60
Checked last = DWORD : 1267657928
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Runinfo/Run number > 100
Alarm Class = STRING : [32] Alarm
Alarm Message = STRING : [80] Run number became too large

[/Alarms/Alarms/Demo periodic]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 4
Check interval = INT : 28800
Checked last = DWORD : 1058817867
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] 
Alarm Class = STRING : [32] Warning
Alarm Message = STRING : [80] Please do your shift checks

[/Alarms/Alarms/Epics]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Warning
Alarm Message = STRING : [80] Program Epics is not running

[/Alarms/Alarms/client alarm]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 20
Checked last = DWORD : 1267657964
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Equipment/fifo_acq/client flags/client alarm = 1
Alarm Class = STRING : [32] Alarm
Alarm Message = STRING : [80] Stop the run and restart (error or event limit reached)

[/Alarms/Alarms/mdarc]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program Speaker is not running

[/Alarms/Alarms/fePOL]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program fePOL is not running

[/Alarms/Alarms/Run number]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 60
Checked last = DWORD : 1060106030
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /runinfo/run number < 29999
Alarm Class = STRING : [32] Alarm
Alarm Message = STRING : [80] Run number became too small

[/Alarms/Alarms/rf_config]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program rf_config is not running

[/Alarms/Alarms/mheader]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program mheader is not running

[/Alarms/Alarms/feDVM]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] caution
Alarm Message = STRING : [80] Program feDVM is not running

[/Alarms/Alarms/fe_runlog]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program fe_runlog is not running

[/Alarms/Alarms/Analyzer]
Active = BOOL : n
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program Analyzer is not running

[/Alarms/Alarms/Logger]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program Logger is not running

[/Alarms/Alarms/Lazy_Disk]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program Lazy_Disk is not running

[/Alarms/Alarms/thr1 trip]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 15
Checked last = DWORD : 1267657950
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Equipment/Info ODB/Variables/last failed thr test = 1
Alarm Class = STRING : [32] Threshold
Alarm Message = STRING : [80] P+ threshold check failed

[/Alarms/Alarms/thr2 trip]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 15
Checked last = DWORD : 1267657950
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Equipment/Info ODB/Variables/last failed thr test = 2
Alarm Class = STRING : [32] Threshold
Alarm Message = STRING : [80] Laser threshold check failed

[/Alarms/Alarms/thr3 trip]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 3
Check interval = INT : 15
Checked last = DWORD : 1267657950
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] /Equipment/Info ODB/Variables/last failed thr test = 3
Alarm Class = STRING : [32] threshold
Alarm Message = STRING : [80] FC15 threshold check failed

[/Alarms/Alarms/Speaker]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program Speaker is not running

[/Alarms/Alarms/ODBEdit]
Active = BOOL : y
Triggered = INT : 0
Type = INT : 2
Check interval = INT : 60
Checked last = DWORD : 0
Time triggered first = STRING : [32] 
Time triggered last = STRING : [32] 
Condition = STRING : [256] Program not running
Alarm Class = STRING : [32] Caution
Alarm Message = STRING : [80] Program ODBEdit is not running

[/Alarms/Classes/Alarm]
Write system message = BOOL : y
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : n
Display BGColor = STRING : [32] red
Display FGColor = STRING : [32] black

[/Alarms/Classes/Warning]
Write system message = BOOL : y
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : n
Display BGColor = STRING : [32] red
Display FGColor = STRING : [32] black

[/Alarms/Classes/Caution]
Write system message = BOOL : y
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : y
Display BGColor = STRING : [32] blue
Display FGColor = STRING : [32] red

[/Alarms/Classes/Threshold]
Write system message = BOOL : n
Write Elog message = BOOL : n
System message interval = INT : 60
System message last = DWORD : 0
Execute command = STRING : [256] 
Execute interval = INT : 0
Execute last = DWORD : 0
Stop run = BOOL : n
Display BGColor = STRING : [32] yellow
Display FGColor = STRING : [32] black

[/alias]
Expert& = LINK : [18] /alias/Expert_dir
Hardware& = LINK : [26] /alias/User_hardware_dir/
Run_info& = LINK : [27] /Experiment/Edit on start/

[/alias/Expert_dir]
PPGinput = LINK : [34] /Equipment/FIFO_acq/sis mcs/Input
PPGoutput = LINK : [35] /Equipment/FIFO_acq/sis mcs/Output
Mdarc = LINK : [26] /Equipment/FIFO_acq/mdarc
Midbnmr = LINK : [45] /equipment/FIFO_acq/mdarc/histograms/midbnmr
Hardware = LINK : [37] /Equipment/FIFO_acq/sis mcs/Hardware
CampSweepDev = LINK : [39] /Equipment/FIFO_acq/camp sweep devices
ClientFlags = LINK : [33] /Equipment/FIFO_acq/client flags

[/alias/User_hardware_dir]
P+ threshold (0 disables) = LINK : [52] /Equipment/FIFO_acq/sis mcs/Hardware/Cycle thr1
Laser threshold (0 disables) = LINK : [52] /Equipment/FIFO_acq/sis mcs/Hardware/Cycle thr2
Fcup threshold (0 disables) = LINK : [52] /Equipment/FIFO_acq/sis mcs/Hardware/Cycle thr3
repeat scan = LINK : [60] /Equipment/FIFO_acq/sis mcs/Hardware/out-of-tol repeat scan
skip ncycles after out-of-tol = LINK : [61] /Equipment/FIFO_acq/sis mcs/Hardware/skip ncycles out-of-tol
disable Epics bias read = LINK : [58] /Equipment/FIFO_acq/sis mcs/Hardware/disable Epics checks

[/alias]
PPG& = LINK : [11] /ppg/ppg1h

[/Equipment/FIFO_acq/Common]
Event ID = WORD : 1
Trigger mask = WORD : 1
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 1
Period = INT : 10
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/FIFO_acq/mdarc/histograms]
number defined = INT : 7
num bins = INT : 1
dwell time (ms) = FLOAT : 1000
resolution code = INT : -1
titles = STRING[16] :
[32] Const
[32] FluM2
[32] L+
[32] R+
[32] L-
[32] R-
[32] NBMB+
[32] NBMF+
[32] NBMB-
[32] NBMF-
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
bin zero = INT[16] :
[0] 1
[1] 1
[2] 1
[3] 1
[4] 1
[5] 1
[6] 1
[7] 1
[8] 1
[9] 1
[10] 1
[11] 1
[12] 1
[13] 1
[14] 1
[15] 1
first good bin = INT[16] :
[0] 1
[1] 1
[2] 1
[3] 1
[4] 1
[5] 1
[6] 1
[7] 0
[8] 0
[9] 0
[10] 20
[11] 20
[12] 20
[13] 20
[14] 1
[15] 1
last good bin = INT[16] :
[0] 1
[1] 1
[2] 1
[3] 1
[4] 1
[5] 1
[6] 1
[7] 1100
[8] 1100
[9] 1100
[10] 60
[11] 60
[12] 60
[13] 60
[14] 800
[15] 800
first background bin = INT[16] :
[0] 0
[1] 0
[2] 0
[3] 0
[4] 0
[5] 0
[6] 0
[7] 1
[8] 1
[9] 1
[10] 1
[11] 1
[12] 1
[13] 1
[14] 0
[15] 0
last background bin = INT[16] :
[0] 0
[1] 0
[2] 0
[3] 0
[4] 0
[5] 0
[6] 0
[7] 50
[8] 50
[9] 50
[10] 10
[11] 10
[12] 10
[13] 10
[14] 0
[15] 0

[/Equipment/FIFO_acq/mdarc/histograms/output]
histogram totals = FLOAT[16] :
[0] 2.74982e+08
[1] 0
[2] 0
[3] 0
[4] 28
[5] 62
[6] 0
[7] 0
[8] 199
[9] 288
[10] 0
[11] 0
[12] 0
[13] 0
[14] 0
[15] 0
total saved = FLOAT : 2.74983e+08

[/Equipment/FIFO_acq/mdarc/histograms/midbnmr]
label = STRING : [16] not used
active = BOOL : n
first time bin = INT : 0
last time bin = INT : 1
start freq (Hz) = INT : 1000
end freq (Hz) = INT : 2000
last file written = STRING : [128] not used
time file written = STRING : [32] not used

[/Equipment/FIFO_acq/mdarc]
time_of_last_save = STRING : [32] Thu Jul 31 15:51:41 2003
last_saved_filename = STRING : [128] /is01_data/pol/dlog/2003/030201.msr
saved_data_directory = STRING : [128] /is01_data/pol/dlog/2003
num_versions_before_purge = DWORD : 4
end_of_run_purge_rename = BOOL : y
archived_data_directory = STRING : [128] /musr/dlog/bnmr
archiver task = STRING : [80] /home/musrdaq/musr/mdarc/bin/cpbnmr
save_interval(sec) = DWORD : 15
toggle = BOOL : n
disable run number check = BOOL : n
run type = STRING : [5] test
perlscript path = STRING : [50] /home/pol/online/perl
enable mdarc logging = BOOL : n

[/Equipment/FIFO_acq/mdarc/camp]
camp hostname = STRING : [128] polvw
temperature variable = STRING : [128] /Test/dac_set
field variable = STRING : [128] /Test/dac_set

[/Equipment/FIFO_acq/mdarc]
enable poststop rn check = BOOL : y

[/Equipment/FIFO_acq/sis mcs/Output]
PPG_loadfile = STRING : [128] 1n.ppg
PPG template = STRING : [20] 1n_.ppg
beam on time (ms) = FLOAT : 0
frequency stop (Hz) = FLOAT : 0
num frequency steps = DWORD : 0
dwell time (ms) = FLOAT : 1000
num dwell times = DWORD : 1
RF on time (ms) = FLOAT : 0
RF off time (ms) = FLOAT : 0
compiled file time (binary) = DWORD : 1267657712
compiled file time = STRING : [32] Wed Mar  3 15:08:32 2010
e1c Camp Path = STRING : [32] 
e1c Camp IfMod = STRING : [10] 
e1c Camp Instr Type = STRING : [32] 
e1n Epics device = STRING : [32] none
e1h scan device = STRING : [10] DAC
scan jump inc1 = INT : 109
scan jump inc2 = INT : 231
scan jump value1 = FLOAT : 1.79
scan jump value2 = FLOAT : 3.01
VME beam control = BOOL : y
Bias source code = INT : 1

[/Equipment/FIFO_acq/sis mcs/sis test mode]
test mode = BOOL : n
num bins = DWORD : 202
Dwell time (ms) = FLOAT : 1
beam time (ms) = FLOAT : 150

[/Equipment/FIFO_acq/sis mcs/flags]
hold = BOOL : n

[/Equipment/FIFO_acq/sis mcs/Input]
Experiment name = STRING : [32] 1h
CFG path = STRING : [128] /home/pol/online/pol/ppcobj
PPG path = STRING : [128] /home/pol/online/ppg-templates
beam off time (ms) = FLOAT : 0
num beam PreCycles = DWORD : 1
num_beam_acq_cycles = DWORD : 100000
frequency start (Hz) = DWORD : 1000
frequency stop (Hz) = DWORD : 2000
frequency increment (Hz) = DWORD : 100
RF on time (ms) = FLOAT : 100
RF off time (ms) = FLOAT : 100
num RF on delays (dwell times) = DWORD : 10
MCS enable delay (ms) = FLOAT : 1
MCS enable gate (ms) = FLOAT : 1000
MCS max delay (ms) = FLOAT : 1
DAQ service time (ms) = FLOAT : 3000
Minimal delay (ms) = FLOAT : 0.0005
Time slice (ms) = FLOAT : 1e-04
E1B Dwell time (ms) = FLOAT : 100
RF delay (ms) = FLOAT : 30000
Bg delay (ms) = FLOAT : 1
e1f num dwell times = DWORD : 1
Num RF cycles = DWORD : 50
E2B Num beam on dwell times = INT : 1000
Check recent compiled file = BOOL : y
freq single slice width (Hz) = DWORD : 10
Num freq slices = DWORD : 1
prebeam on time (ms) = FLOAT : 1
flip 360 delay (ms) = FLOAT : 10
flip 180 delay (ms) = FLOAT : 40
f slice internal delay (ms) = FLOAT : 10
beam_mode = CHAR : P
counting_mode = CHAR : 1
f select pulselength (ms) = FLOAT : 10
e00 prebeam dwelltimes = DWORD : 50
RFon dwelltime = DWORD : 0
e00 beam on dwelltimes = DWORD : 50
e00 beam off dwelltimes = DWORD : 1000
e00 rf frequency (Hz) = DWORD : 22064585
rfon duration (dwelltimes) = INT : 0
num type1 frontend histograms = INT : 7
num type2 frontend histograms = INT : 10
e1a&1b pulse pairs = STRING : [4] 000
e1a&1b freq mode = STRING : [3] F0
e1c Camp Device = STRING : [5] DAC
e2c beam on time (ms) = FLOAT : 0
num cycles per supercycle = INT : 0
NaVolt start = FLOAT : 180
NaVolt stop = FLOAT : 690
NaVolt inc = FLOAT : 1
Laser start = FLOAT : -5
Laser stop = FLOAT : 0
Laser inc = FLOAT : 0.05
e1c Camp Start = FLOAT : 0
e1c Camp Stop = FLOAT : 9.95
e1c Camp Inc = FLOAT : 0.05
Field start = FLOAT : 95
Field stop = FLOAT : 100
Field inc = FLOAT : 1.1
e1h DAC Start = FLOAT : -9.4
e1h DAC Stop = FLOAT : 0
e1h DAC Inc = FLOAT : 0.01
scan jump start = FLOAT : 1.8
scan jump stop = FLOAT : 3
enable scan jump = BOOL : n

[/Equipment/FIFO_acq/sis mcs/Hardware]
num cycles = DWORD : 9410
Fluor monitor thr = DWORD : 0
Cycle thr1 = FLOAT : 0
Cycle thr2 = FLOAT : 0
Cycle thr3 = FLOAT : 0
Diagnostic channel num = INT : 100
Re-reference = BOOL : n
re-ref_P+ = BOOL : n
re-ref_Laser = BOOL : n
re-ref_Fcup = BOOL : n
num polarization cycles = DWORD : 0
polarization switch delay = DWORD : 0
Enable SIS ref ch1 scaler A = BOOL : n
Enable SIS ref ch1 scaler B = BOOL : y
Enable helicity flipping = BOOL : y
helicity flip sleep (ms) = INT : 0
Check RF trip = BOOL : y
Num scans = INT : 5
out-of-tol repeat scan = BOOL : n
skip ncycles out-of-tol = DWORD : 3
disable helicity checking = BOOL : n
disable Epics checks = BOOL : n
flag_bad_scan = BOOL : n

[/Equipment/FIFO_acq/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/FIFO_acq/camp sweep devices/frequency generator]
Sweep Device code = STRING : [5] FG
Camp path = STRING : [32] /frq
GPIB port or rs232 portname = STRING : [10] 7
Instrument Type = STRING : [32] bnc625a
Camp scan path = STRING : [80] /frq/frequency
Scan units = STRING : [10] Hz
maximum = FLOAT : 500000
minimum = FLOAT : 1000
Camp device dependent path = STRING : [80] 
integer conversion factor = INT : 1

[/Equipment/FIFO_acq/camp sweep devices/magnet]
Sweep Device code = STRING : [5] MG
Camp path = STRING : [32] /Magnet
GPIB port or rs232 portname = STRING : [10] /tyCo/5
Instrument Type = STRING : [32] oxford_ips120
Camp scan path = STRING : [80] /Magnet/mag_field
Scan units = STRING : [10] T
maximum = FLOAT : 7
minimum = FLOAT : -7
Camp device dependent path = STRING : [80] /Magnet/ramp_status
integer conversion factor = INT : 10000

[/Equipment/FIFO_acq/camp sweep devices/dac]
Sweep Device code = STRING : [5] DAC
Camp path = STRING : [32] /v_scan
GPIB port or rs232 portname = STRING : [10] 0
Instrument Type = STRING : [32] tip850_dac
Camp scan path = STRING : [80] /v_scan/dac_set
Scan units = STRING : [10] V
maximum = FLOAT : 10
minimum = FLOAT : -10
Camp device dependent path = STRING : [80] /frq/amplitude
integer conversion factor = INT : 1000

[/Equipment/FIFO_acq/client flags]
mdarc = BOOL : y
rf_config = BOOL : y
mheader = BOOL : n
frontend = BOOL : y
fe_epics = BOOL : n
enable client check = BOOL : y
client alarm = INT : 0

[/Equipment/FIFO_acq/Pol params]
Settling time (ms) = INT : 10
flag bad scan = BOOL : n

[/Equipment/Cycle_Scalers/Common]
Event ID = WORD : 3
Trigger mask = WORD : 1
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 257
Period = INT : 100
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Cycle_Scalers/Statistics]
Events sent = DOUBLE : 14
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Cycle_Scalers/Settings]
Names = STRING[6] :
[32] Scaler_B%SIS Ref pulse
[32] Scaler_B%Fluor. mon
[32] Scaler_B%P+ beam
[32] Scaler_B%Laser power
[32] Scaler_B%Faraday Cup 15
[32] Scaler_B%Locking Feedback

[/Equipment/Cycle_Scalers/Variables]
CYCI = DWORD[9] :
[0] 14
[1] 14
[2] 1
[3] 0
[4] 8
[5] 0
[6] 0
[7] 4294958027
[8] 4294883903
HSCL = DOUBLE[6] :
[0] 8222500
[1] 0
[2] 2
[3] 32
[4] 0
[5] 0

[/Equipment/Histo/Common]
Event ID = WORD : 2
Trigger mask = WORD : 1
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : n
Read on = INT : 17
Period = INT : 100
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Histo/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Hdiagnosis/Common]
Event ID = WORD : 4
Trigger mask = WORD : 1
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : n
Read on = INT : 1
Period = INT : 100
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Hdiagnosis/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Info ODB/Common]
Event ID = WORD : 10
Trigger mask = WORD : 0
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] FIXED
Enabled = BOOL : y
Read on = INT : 273
Period = INT : 500
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Info ODB/Variables]
helicity = DWORD : 0
current cycle = DWORD : 15
cancelled cycle = DWORD : 1
current scan = DWORD : 1
Ref P+ thr = DOUBLE : 2
Ref Laser thr = DOUBLE : 12
Ref Fcup thr = DOUBLE : 0
Current P+ thr = DOUBLE : 2
Current Laser thr = DOUBLE : 32
Current Fcup thr = DOUBLE : 0
RF state = DWORD : 0
Fluor monitor counts = DWORD : 0
EpicsDev Set(V) = FLOAT : 0
EpicsDev Read(V) = FLOAT : 0
Campdev set = FLOAT : 0
Campdev read = FLOAT : 0
Pol DAC set = DOUBLE : 0
Pol DAC read = DOUBLE : 0
last failed thr test = DWORD : 0
cycle when last failed thr = DWORD : 0

[/Equipment/Info ODB/Statistics]
Events sent = DOUBLE : 32
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/DVM/Common]
Event ID = WORD : 6
Trigger mask = WORD : 0
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 511
Period = INT : 60000
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 60
Frontend host = STRING : [32] midtis07
Frontend name = STRING : [32] feDVM
Frontend file name = STRING : [256] feDVM.c

[/Equipment/DVM/Settings]
Address = INT : 8
Modules = INT[3] :
[0] 34901
[1] 34907
[2] 0
ScanList = STRING : [64] 101,102,103,104
Slot1Config = STRING[20] :
[64] SENS:FUNC "VOLT:DC";SENS:VOLT:DC:RANG 10;SENS:VOLT:DC:NPLC 10
[64] SENS:FUNC "TEMP":SENS:TEMP:TRAN:TC:TYPE J:SENS:TEMP:NPLC 1
[64] SENS:FUNC "VOLT:DC";SENS:VOLT:DC:RANG 10;SENS:VOLT:DC:NPLC 10
[64] SENS:FUNC "TEMP":SENS:TEMP:TRAN:TC:TYPE T:SENS:FUNC:NPLC 1
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
[64] 
Slot1Names = STRING[20] :
[32] Readback (Volts)
[32] Temp Electronic Rack
[32] Readback2 (Volts)
[32] Temp Hut
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 

[/Equipment/DVM/Settings/Slot2DAC1]
Address = STRING : [32] 204
Demand = FLOAT : 0
Handshake = INT : 1

[/Equipment/DVM/Settings/Slot2DAC2]
Address = STRING : [32] 205
Demand = FLOAT : -9.7
Handshake = INT : 1

[/Equipment/DVM/Settings]
Slot2Config = FLOAT[5] :
[0] 0
[1] 0
[2] 0
[3] 0
[4] 0
Slot2Names = STRING[5] :
[32] 
[32] 
[32] 
[32] PS1 Setpoint
[32] PS2 Setpoint

[/Equipment/DVM/Settings/Channels]
DVM = INT : 1

[/Equipment/DVM/Variables]
Measured = FLOAT[4] :
[0] 0.0101283
[1] -0.000326
[2] -0.0001212
[3] -0.000143008

[/Equipment/DVM/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Wavemeter/Common]
Event ID = WORD : 7
Trigger mask = WORD : 0
Buffer = STRING : [32] 
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 511
Period = INT : 60000
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 60
Frontend host = STRING : [32] midtis07.triumf.ca
Frontend name = STRING : [32] feDVM
Frontend file name = STRING : [256] feDVM.c

[/Equipment/Wavemeter/Settings]
Address = INT : 6
Names = STRING[3] :
[32] Frequency
[32] Update
[32] Error

[/Equipment/Wavemeter/Settings/Channels]
Wavemeter = INT : 3

[/Equipment/Wavemeter/Variables]
Measured = FLOAT[3] :
[0] 15798
[1] 0
[2] 1

[/Equipment/Wavemeter/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/DVMT/Common]
Event ID = WORD : 5
Trigger mask = WORD : 0
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 377
Period = INT : 1000
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] midtis07
Frontend name = STRING : [32] feDVMtest
Frontend file name = STRING : [256] feDVMtest.c

[/Equipment/DVMT/Statistics]
Events sent = DOUBLE : 0
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/Equipment/Monitor/Common]
Event ID = WORD : 11
Trigger mask = WORD : 0
Buffer = STRING : [32] SYSTEM
Type = INT : 1
Source = INT : 0
Format = STRING : [8] MIDAS
Enabled = BOOL : y
Read on = INT : 265
Period = INT : 500
Event limit = DOUBLE : 0
Num subevents = DWORD : 0
Log history = INT : 0
Frontend host = STRING : [32] vwisac2
Frontend name = STRING : [32] fePOL
Frontend file name = STRING : [256] febnmr.c

[/Equipment/Monitor/Variables]
MONI = DOUBLE[3] :
[0] 0
[1] -99
[2] 15798

[/Equipment/Monitor/Statistics]
Events sent = DOUBLE : 1
Events per sec. = DOUBLE : 0
kBytes per sec. = DOUBLE : 0

[/PPG/PPGcommon]
Experiment name = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
CFG path = LINK : [43] /Equipment/FIFO_acq/sis mcs/Input/CFG path
PPG path = LINK : [43] /Equipment/FIFO_acq/sis mcs/Input/PPG path
Time slice (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Time slice (ms)
Minimal delay (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/Minimal delay (ms)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1a]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
beam mode (C or P) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/beam_mode
pulse pairs (000 or 180) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b pulse pairs
frequency mode (FR or F0) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b freq mode
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
Bg delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Bg delay (ms)
RF delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/RF delay (ms)
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num RF cycles = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Num RF cycles
Num events per freq = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
reference frequency (Hz) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/e00 rf frequency (Hz)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1b]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
beam mode (C or P) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/beam_mode
pulse pairs (000 or 180) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b pulse pairs
frequency mode (FR or F0) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/e1a&1b freq mode
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
Bg delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Bg delay (ms)
RF delay (ms) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/RF delay (ms)
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num RF cycles = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/Num RF cycles
Num events per freq = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
E1B Dwell time (ms) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/E1B Dwell time (ms)
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
reference frequency (Hz) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/e00 rf frequency (Hz)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1n]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
NaVolt start (volts) = LINK : [47] /Equipment/FIFO_acq/sis mcs/Input/NaVolt start
NaVolt stop (volts) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/NaVolt stop
NaVolt inc (volts) = LINK : [45] /Equipment/FIFO_acq/sis mcs/Input/NaVolt inc
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/e1f num dwell times
Enable up down scan = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping
enable jump in scan = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/enable scan jump
start scan jump = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/scan jump start
end scan jump = LINK : [49] /Equipment/FIFO_acq/sis mcs/Input/scan jump stop

[/PPG/PPG1f]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/e1f num dwell times
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG1d]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Laser start (volts) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/Laser start
Laser stop (volts) = LINK : [45] /Equipment/FIFO_acq/sis mcs/Input/Laser stop
Laser inc (volts) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/Laser inc
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/e1f num dwell times
Enable up down scan = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping
Enable jump in scan = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/enable scan jump
start scan jump = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/scan jump start
end scan jump = LINK : [49] /Equipment/FIFO_acq/sis mcs/Input/scan jump stop

[/PPG/PPG1c]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/e1f num dwell times
Camp Sweep Device = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Device
Sleep time (ms) = LINK : [50] /Equipment/FIFO_acq/Pol params/Settling time (ms)
scan start = LINK : [49] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Start
scan stop = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Stop
scan increment = LINK : [47] /Equipment/FIFO_acq/sis mcs/Input/e1c Camp Inc

[/PPG/PPG10]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/e1f num dwell times

[/PPG/PPG20]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Dwell time (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
e20 prebeam dwelltimes = LINK : [57] /Equipment/FIFO_acq/sis mcs/Input/e00 prebeam dwelltimes
e20 beam on dwelltimes = LINK : [57] /Equipment/FIFO_acq/sis mcs/Input/e00 beam on dwelltimes
e20 beam off dwelltimes = LINK : [58] /Equipment/FIFO_acq/sis mcs/Input/e00 beam off dwelltimes
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG2a]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
num RF on delays (dwell times) = LINK : [65] /Equipment/FIFO_acq/sis mcs/Input/num RF on delays (dwell times)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)
beam off time (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/beam off time (ms)
num beam PreCycles = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/num beam PreCycles
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)

[/PPG/PPG2b]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
RF on time (ms) = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/RF on time (ms)
RF off time (ms) = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/RF off time (ms)
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
num RF on delays (dwell times) = LINK : [65] /Equipment/FIFO_acq/sis mcs/Input/num RF on delays (dwell times)
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)
beam off time (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/beam off time (ms)
num beam PreCycles = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/num beam PreCycles
Num beam on dwell times = LINK : [62] /Equipment/FIFO_acq/sis mcs/Input/E2B Num beam on dwell times
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)

[/PPG/PPG2c]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
frequency start (Hz) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)
frequency stop (Hz) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)
frequency increment (Hz) = LINK : [59] /Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)
prebeam on time (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/prebeam on time (ms)
beam_mode = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/beam_mode
num RF on delays (dwell times) = LINK : [65] /Equipment/FIFO_acq/sis mcs/Input/num RF on delays (dwell times)
beam on time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/e2c beam on time (ms)
beam off time (ms) = LINK : [53] /Equipment/FIFO_acq/sis mcs/Input/beam off time (ms)
RF on time e2c (ms) = LINK : [60] /Equipment/FIFO_acq/sis mcs/Input/f select pulselength (ms)
Num freq slices = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Num freq slices
freq single slice width (Hz) = LINK : [63] /Equipment/FIFO_acq/sis mcs/Input/freq single slice width (Hz)
f slice internal delay (ms) = LINK : [62] /Equipment/FIFO_acq/sis mcs/Input/f slice internal delay (ms)
flip 180 delay (ms) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/flip 180 delay (ms)
flip 360 delay (ms) = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/flip 360 delay (ms)
MCS enable delay (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/MCS enable delay (ms)
MCS enable gate (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
counting_mode = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/counting_mode
DAQ service time (ms) = LINK : [56] /Equipment/FIFO_acq/sis mcs/Input/DAQ service time (ms)

[/PPG/PPG1e]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Field start (Gauss) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/Field start
Field stop (Gauss) = LINK : [45] /Equipment/FIFO_acq/sis mcs/Input/Field stop
Field inc (Gauss) = LINK : [44] /Equipment/FIFO_acq/sis mcs/Input/Field inc
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
Num bins = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/Num spectra per freq
Enable helicity flipping = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping

[/PPG/PPG1h]
PPG mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name
Bin width (ms) = LINK : [55] /Equipment/FIFO_acq/sis mcs/Input/MCS enable gate (ms)
DAC start (V) = LINK : [48] /Equipment/FIFO_acq/sis mcs/Input/e1h DAC Start
DAC stop (V) = LINK : [47] /Equipment/FIFO_acq/sis mcs/Input/e1h DAC Stop
DAC inc (V) = LINK : [46] /Equipment/FIFO_acq/sis mcs/Input/e1h DAC Inc
Settling time (ms) = LINK : [50] /Equipment/FIFO_acq/Pol params/Settling time (ms)
Enable up down scan = LINK : [62] /Equipment/FIFO_acq/sis mcs/Hardware/Enable helicity flipping
enable jump in scan = LINK : [51] /Equipment/FIFO_acq/sis mcs/Input/enable scan jump
start scan jump = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/scan jump start
stop scan jump = LINK : [49] /Equipment/FIFO_acq/sis mcs/Input/scan jump stop
Num bins = LINK : [54] /Equipment/FIFO_acq/sis mcs/Input/e1f num dwell times

[/History/Display/Fluor]
Factor = FLOAT : 1
Offset = FLOAT : 0
Timescale = STRING : [32] 1d
Zero ylow = BOOL : y
Show run markers = BOOL : y
Log axis = BOOL : n
Variables = STRING : [64] Epics:Fluoresc. Monit 1 Demand
Buttons = STRING[7] :
[32] 10m
[32] 1h
[32] 3h
[32] 12h
[32] 24h
[32] 3d
[32] 7d

[/History]
PerVariableHistory = INT : 0
DisableTags = BOOL : n

[/History/Tags]
6 = STRING[2] :
[96] DVM
[96] 9[4] Measured
7 = STRING[4] :
[96] Wavemeter
[96] 9[1] Frequency Measured
[96] 9[1] Update Measured
[96] 9[1] Error Measured

[/Script/POL Hold]
cmd = STRING : [128] /home/pol/online/perl/hold.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
hold flag = LINK : [39] /Equipment/FIFO_acq/sis mcs/flags/hold
toggle flag = LINK : [33] /Equipment/FIFO_acq/mdarc/toggle
beamline = STRING : [5] pol
ppg mode = LINK : [50] /Equipment/FIFO_acq/sis mcs/Input/Experiment name

[/Script/Continue]
cmd = STRING : [128] /home/pol/online/perl/continue.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
hold flag = LINK : [39] /Equipment/FIFO_acq/sis mcs/flags/hold
beamline = STRING : [5] pol

[/Script/Bad Scan]
cmd = STRING : [128] /home/pol/online/perl/reref.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
scan flag value = LINK : [51] /Equipment/FIFO_acq/sis mcs/Hardware/flag_bad_scan
beamline = STRING : [5] pol
flag name = STRING : [50] flag_bad_scan

[/Script/Reref all]
cmd = STRING : [128] /home/pol/online/perl/reref.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
flag value = LINK : [50] /Equipment/FIFO_acq/sis mcs/Hardware/re-reference
beamline = STRING : [5] pol
flag name = STRING : [50] re-reference

[/Script/Reref P+]
cmd = STRING : [128] /home/pol/online/perl/reref.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
flag value = LINK : [47] /Equipment/FIFO_acq/sis mcs/Hardware/re-ref_P+
beamline = STRING : [5] pol
flag name = STRING : [50] re-ref_P+

[/Script/Reref Lsr]
cmd = STRING : [128] /home/pol/online/perl/reref.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
flag value = LINK : [50] /Equipment/FIFO_acq/sis mcs/Hardware/re-ref_Laser
beamline = STRING : [5] pol
flag name = STRING : [50] re-ref_Laser

[/Script/Reref Fcup]
cmd = STRING : [128] /home/pol/online/perl/reref.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
flag value = LINK : [49] /Equipment/FIFO_acq/sis mcs/Hardware/re-ref_Fcup
beamline = STRING : [5] pol
flag name = STRING : [50] re-ref_Fcup

[/Script/Scalers]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 10

[/Script/DAC]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 1h
mode file tag = STRING : [10] none

[/Script/NaCell]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 1n
mode file tag = STRING : [10] none

[/Script/Laser]
cmd = STRING : [128] /home/pol/online/perl/change_mode.pl
include path = STRING : [64] /home/pol/online/perl
experiment name = LINK : [17] /experiment/name
select mode = STRING : [5] 1d
mode file tag = STRING : [10] none

[/Elog]
Display run number = BOOL : y
Allow delete = BOOL : n
Buttons = STRING[3] :
[32] 8h
[32] 24h
[32] 7d
Types = STRING[20] :
[32] Routine
[32] Shift summary
[32] Minor error
[32] Severe error
[32] Fix
[32] Question
[32] Info
[32] Modification
[32] Reply
[32] Alarm
[32] Test
[32] Other
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
[32] 
Systems = STRING[20] :
[32] General
[32] DAQ
[32] Detector
[32] Electronics
[32] Target
[32] Beamline
[32] Laser
[32] RF
[32] Cryogenics
[32] UHV
[32] Magnet
[32] Deceleration
[32] Beam Tuning
[32] Documentation
[32] BNMRFIT Program
[32] 
[32] 
[32] 
[32] 
[32] 
Host name = STRING : [256] isdaq01.triumf.ca
Email DAQ = STRING : [45] suz@triumf.ca,renee@triumf.ca,asnd@triumf.ca
Email Documentation = STRING : [32] suz@triumf.ca
SMTP host = STRING : [32] trmail.triumf.ca

[/Analyzer/Output]
Filename = STRING : [256] r00319.root
RWNT = BOOL : n
Histo Dump = BOOL : y
Histo Dump Filename = STRING : [256] his%05d.root
Clear histos = BOOL : y
Last Histo Filename = STRING : [256] last.root
Events to ODB = BOOL : y
Global Memory Name = STRING : [8] ONLN

[/Analyzer]
Book N-tuples = BOOL : y

[/Analyzer/CAMP/Common]
Event ID = INT : 13
Trigger mask = INT : -1
Sampling type = INT : 2
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/CAMP/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer/DARC/Common]
Event ID = INT : 14
Trigger mask = INT : -1
Sampling type = INT : 1
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/DARC/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer/INFO/Common]
Event ID = INT : 15
Trigger mask = INT : -1
Sampling type = INT : 1
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/INFO/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer/Bank switches]
CYCI = DWORD : 0
HSCL = DWORD : 0
DVMT = DWORD : 0

[/Analyzer/Module switches]
Scaler hist = BOOL : y
DMV Hist = BOOL : y

[/Analyzer/Cycle_Scalers/Common]
Event ID = INT : 3
Trigger mask = INT : -1
Sampling type = INT : 2
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01

[/Analyzer/Cycle_Scalers/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Analyzer]
Book TTree = BOOL : y
ODB Load = BOOL : y

[/Analyzer/DVMT/Common]
Event ID = INT : 5
Trigger mask = INT : -1
Sampling type = INT : 2
Buffer = STRING : [32] SYSTEM
Enabled = BOOL : y
Client name = STRING : [32] Analyzer
Host = STRING : [32] isdaq01.triumf.ca

[/Analyzer/DVMT/Statistics]
Events received = DOUBLE : 0
Events per sec. = DOUBLE : 0
Events written = DOUBLE : 0

[/Lazy/Disk/Settings]
Maintain free space (%) = INT : 0
Stay behind = INT : -1
Alarm Class = STRING : [32] 
Running condition = STRING : [128] ALWAYS
Data dir = STRING : [256] /is01_data/is01_data1/pol/data/current
Data format = STRING : [8] MIDAS
Filename format = STRING : [128] %06d.mid
Backup type = STRING : [8] Disk
Execute after rewind = STRING : [64] 
Path = STRING : [128] /isdaq/data21/pol/data/current
Capacity (Bytes) = FLOAT : 5e+09
List label = STRING : [128] archive
Execute before writing file = STRING : [64] 
Execute after writing file = STRING : [64] 
Modulo.Position = STRING : [8] 
Tape Data Append = BOOL : y

[/Lazy/Disk/Statistics]
Backup file = STRING : [128] 000321.mid
File size (Bytes) = DOUBLE : 0
KBytes copied = DOUBLE : 0
Total Bytes copied = DOUBLE : 0
Copy progress (%) = DOUBLE : 0
Copy Rate (Bytes per s) = DOUBLE : 0
Backup status (%) = DOUBLE : 0
Number of Files = INT : 173
Current Lazy run = INT : 321

[/Lazy/Disk/List]
archive = INT[173] :
[0] 30127
[1] 30128
[2] 30129
[3] 30130
[4] 30131
[5] 30132
[6] 30133
[7] 30134
[8] 30135
[9] 30136
[10] 30137
[11] 30138
[12] 30139
[13] 30140
[14] 30141
[15] 30142
[16] 30143
[17] 30144
[18] 30145
[19] 30146
[20] 30147
[21] 30148
[22] 30149
[23] 30150
[24] 30151
[25] 30152
[26] 30153
[27] 30154
[28] 30155
[29] 30156
[30] 30157
[31] 30158
[32] 30159
[33] 30160
[34] 30161
[35] 30162
[36] 30163
[37] 30164
[38] 30165
[39] 30166
[40] 30167
[41] 30168
[42] 30169
[43] 30170
[44] 30171
[45] 104
[46] 105
[47] 106
[48] 107
[49] 108
[50] 110
[51] 111
[52] 112
[53] 113
[54] 114
[55] 115
[56] 116
[57] 117
[58] 118
[59] 119
[60] 120
[61] 121
[62] 122
[63] 123
[64] 124
[65] 125
[66] 126
[67] 127
[68] 128
[69] 129
[70] 130
[71] 131
[72] 132
[73] 133
[74] 134
[75] 135
[76] 136
[77] 137
[78] 138
[79] 139
[80] 140
[81] 141
[82] 142
[83] 143
[84] 144
[85] 145
[86] 146
[87] 147
[88] 148
[89] 200
[90] 201
[91] 202
[92] 203
[93] 204
[94] 205
[95] 30100
[96] 206
[97] 207
[98] 208
[99] 209
[100] 210
[101] 211
[102] 212
[103] 214
[104] 215
[105] 216
[106] 217
[107] 218
[108] 219
[109] 220
[110] 221
[111] 222
[112] 223
[113] 224
[114] 225
[115] 226
[116] 227
[117] 228
[118] 229
[119] 230
[120] 231
[121] 232
[122] 233
[123] 234
[124] 235
[125] 236
[126] 237
[127] 238
[128] 239
[129] 240
[130] 241
[131] 242
[132] 243
[133] 244
[134] 245
[135] 246
[136] 247
[137] 248
[138] 249
[139] 250
[140] 30101
[141] 30102
[142] 161
[143] 162
[144] 163
[145] 164
[146] 165
[147] 166
[148] 167
[149] 30001
[150] 30002
[151] 300
[152] 301
[153] 302
[154] 303
[155] 304
[156] 305
[157] 306
[158] 307
[159] 308
[160] 309
[161] 310
[162] 311
[163] 312
[164] 313
[165] 314
[166] 315
[167] 316
[168] 317
[169] 318
[170] 319
[171] 320
[172] 321

[/custom]
try = STRING : [64] /home/pol/online/custom/try.html

