/*  BiasTest.c

Tests HV Bias routines used in febnmr.c

BiasTest()   tests getting ID, reading voltage

Used with routines in connect.c (use callbacks)


CVS log information:
$Log: BiasTest.c,v $
Revision 1.1  2005/08/12 18:53:00  suz
original for Pol


*/
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "EpicsStep.h"

#ifndef LOCAL
#define LOCAL static
#endif
char * ca_name (int chan);

void BiasTest(void);


/* globals */
int ddd=0;
int Rchid_Bias;


/* Epics names of BIAS variables */ 

char R_ITW[]="ITW:BIAS:RDVOL";
char R_ITE[]="ITE:BIAS:RDVOL";
char R_IOS[]="IOS:BIAS:RDVOL";

void BiasTest(void)
{
  int status;
  float value;


  /* HV Bias on ISAC West */
  status=caGetSingleId (R_ITW, &Rchid_Bias );
  if(status==-1)
    {
      printf("BiasTest: Bad status after caGetSingleID for %s\n",R_ITW);
    }
  else
    {
      printf("caGetSingleId returns Rchid_Bias = %d \n",Rchid_Bias);
  
      if (caCheck(Rchid_Bias))
	printf("caCheck says channel %s is connected\n",R_ITW);
      else
	printf("caCheck says channel %s is NOT connected\n",R_ITW);
  
      printf("\nBiasTest: Now calling caRead for %s  with chan_id=%d\n",R_ITW,Rchid_Bias);
      status=caRead(Rchid_Bias,&value);
      if(status==-1)
	{
	  printf("BiasTest: Bad status after caRead for %s\n", ca_name(Rchid_Bias));
	}
      else
	printf("BiasTest: Read value from %s as %8.3f\n", ca_name(Rchid_Bias),value);
    }
  /* close this channel */
  caExit();
  
  /* HV Bias on OLIS */
  status=caGetSingleId (R_IOS, &Rchid_Bias );
  if(status==-1)
    {
      printf("BiasTest: Bad status after caGetSingleID for %s\n",R_IOS);
    }
  else
    {
      printf("caGetSingleId returns Rchid_Bias = %d \n",Rchid_Bias);
  
      if (caCheck(Rchid_Bias))
	printf("caCheck says channel %s is connected\n",R_IOS);
      else
	printf("caCheck says channel %s is NOT connected\n",R_IOS);
  



      printf("\nBiasTest: Now calling caRead for %s  with chan_id=%d\n",R_IOS,Rchid_Bias);
      status=caRead(Rchid_Bias,&value);
      if(status==-1)
	{
	  printf("BiasTest: Bad status after caRead for %s\n", ca_name(Rchid_Bias));
	}
      else
	printf("BiasTest: Read value from %s as %8.3f\n", ca_name(Rchid_Bias),value);
    }
  /* close this channel */
  caExit();
  
  /* HV Bias on ISAC East */
  status=caGetSingleId (R_ITE, &Rchid_Bias );
  if(status==-1)
    {
      printf("BiasTest: Bad status after caGetSingleID for %s\n",R_ITE);
    }
  else
    {
      printf("caGetSingleId returns Rchid_Bias = %d \n",Rchid_Bias);
      
      if (caCheck(Rchid_Bias))
	printf("caCheck says channel %s is connected\n",R_ITE);
      else
	printf("caCheck says channel %s is NOT connected\n",R_ITE);
      



      printf("\nBiasTest: Now calling caRead for %s  with chan_id=%d\n",R_ITE,Rchid_Bias);
      status=caRead(Rchid_Bias,&value);
      if(status==-1)
	{
	  printf("BiasTest: Bad status after caRead for %s\n", ca_name(Rchid_Bias));
	}
      else
	printf("BiasTest: Read value from %s as %8.3f\n", ca_name(Rchid_Bias),value);
    }
  /* close this channel */
  caExit();
 
  return;
}


