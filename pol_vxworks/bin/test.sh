#!/bin/sh
echo MIDASSYS $MIDASSYS
ODB=$MIDASSYS/linux/bin/odbedit


# Start the PPG/FSC control program rf_config
$ODB -c scl | grep  rf_config
if [ "$?" != "0" ] ; then
    echo "Starting rf_config"
  #  ~/online/pol/rf_config -e pol -D
  else
 echo rf_config is already running
fi

$MIDASSYS/linux/bin/odbedit -c scl | grep  krud
if [ "$?" != "0" ] ; then
    echo "Starting krud"
  #  ~/online/pol/rf_config -e pol -D
  else
 echo krud is already running
fi
