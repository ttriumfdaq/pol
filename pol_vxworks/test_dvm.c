/* test_dvm 
   Simple program to exercise the DAC/DVM combination when the PPC is not available

   Initialize values, connect to experiment
   Loop over
      Read DAC request value from keyboard
      Set DAC 
      Wait 200 ms
      Read DAC stuff
   Endloop

*/
#include <stdio.h>
#include <stdlib.h>
#include "midas.h"

int main (void)
{  
  HNDLE hDB;
  char str[128]={'\0'};
  char* sptr;
  float request;
  INT d10 = 1; /* for debugging */
  INT gbl_scan_flag = 4;
  size_t il;

  INT size;
  float rdata;
  HNDLE hVar;
  HNDLE hHand,hReq;
  INT icount, icount_max;
  INT status, ihand;
  DWORD start;
  
  DWORD  dvm_val=0;
  DWORD  dac_val=0;  /* dac value to set */
 
  /* Connect to experiment */
  /* get basic handle for experiment ODB */
  status = cm_connect_experiment("isdaq01","pol", "test_dvm", 0);
  if(status != CM_SUCCESS) return 1;
  status=cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS) return 1;
  status = db_find_key(hDB, 0, "/Equipment/DVM/Variables/Measured", &hVar);
  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC1/Handshake", &hHand);
  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC1/Demand", &hReq);
    

  while (1) 
    {
      printf("dvmtest> DAC ? :");
      ss_gets(str,128);
      if (str[0] == 'q')
      {
        printf("\n");
        break;
      }
      il = strlen(str);
      // printf("Read %i ch: %s\n",il,str);
      sptr = str;
      request = strtod(sptr,&sptr);
      printf("Requesting DAC set to %f\n",request);

      /* Set DAC value */
      /* Set the handshake */
      start = ss_millitime();      
      ihand = 0;
      db_set_data(hDB,hHand,&ihand,sizeof(ihand),1,TID_INT);
      db_set_data(hDB,hReq,&request,sizeof(request),1,TID_FLOAT);
      //ss_sleep(50);

      /* Get DVM value now */
      size = sizeof(ihand);
      icount = 0;
      icount_max = 1000;
      do {
	db_get_data(hDB, hHand,&ihand,&size,TID_INT);
	icount +=1;
      }
      while ( (ihand == 0) && (icount != icount_max) );
      if( icount == icount_max) {
	//	*pwdata++ = -1; /* word 9 flags an error */
	printf("Timeout waiting for DAC handshake\n");
	/* !!!! MUST SET THE MECHANISM TO STOP THE RUN ~~~~~ */
      }
      else {
	size = sizeof(rdata);
	status = db_get_data_index(hDB,hVar,&rdata,&size,0,TID_FLOAT);
	dac_val = (DWORD)(1000.*request);
	dvm_val = (DWORD)(100000.*rdata);
	//	      *pwdata++ = dvm_val; /* word 9 is DVM value */
	if(d10) {
          printf("Process took %ld ms\n",ss_millitime() - start);
	  printf("Got ihand %i , data %f; icount = %i\n",ihand,rdata,icount);
	  printf("\n +++ DAC +++gbl_scan_flag=%d,dac_val=%d, dvm_val=%d(%f)\n",
		 gbl_scan_flag,dac_val,dvm_val,rdata);
	}
      }
    }
  cm_disconnect_experiment();
}
