/* test_dvm_scan 
   Simple program to scan the DAC/DVM combination when the PPC is not available

   Initialize values, connect to experiment
   Loop over
      Read DAC request value from keyboard
      Set DAC 
      Wait 200 ms
      Read DAC stuff
   Endloop

*/
#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "experim.h"

int main (void)
{  
  HNDLE hDB;
  char str[128]={'\0'};
  char* sptr;
  INT d10 = 1; /* for debugging */
  INT gbl_scan_flag = 4;
  size_t il;

  INT size;
  float rdata;
  HNDLE hVar;
  HNDLE hHand,hReq;
  INT icount, icount_max;
  INT status, ihand;
  DWORD start;
  
  DWORD  dvm_val=0;
  DWORD  dac_val_set=0;  /* dac value to set */
  INT psleep=0;

  HNDLE hFS;
  FIFO_ACQ_SIS_MCS fs;
  INT     gbl_FREQ_n;                /* current cycle # in freq sweep */
  INT     gbl_SCAN_N = 0;
  float   dac_val;  /* dac value to set */
  float   dac_start;      /* start value for dac sweep */
  float   dac_stop;       /* stop value for dac sweep */
  float   dac_inc;        /* increment value for dac at each sweep */
  float ftmp;
  INT     dac_ninc;       /* number of dac increments to do */
  INT     dac_iinc;       /* current increment num */
  INT d11 = 0;

  /* Connect to experiment */
  /* get basic handle for experiment ODB */
  status = cm_connect_experiment("isdaq01","pol", "test_dvm_scan", 0);
  if(status != CM_SUCCESS) return 1;
  status=cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS) return 1;
  status = db_find_key(hDB, 0, "/Equipment/DVM/Variables/Measured", &hVar);
  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC1/Handshake", &hHand);
  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC1/Demand", &hReq);

  /* create record "/Equipment/FIFO_acq/sis mcs" to make sure it exists  */
  sprintf(str,"/Equipment/FIFO_acq/sis mcs"); 
  
  /* find the key for sis mcs */
  printf("Looking for key %s\n",str);
  status = db_find_key(hDB, 0, str, &hFS);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin-of-run", "cannot find key FIFO_ACQ/Settings");
      return DB_NO_ACCESS;
    }
    
  /* Get current FIFO_ACQ settings */
  size = sizeof(fs);
  status = db_get_record(hDB, hFS, &fs, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin-of-run", "cannot retrieve FIFO_ACQ/Settings");
      return DB_NO_ACCESS;
    }
    
  if(!d11)printf("begin_of_run: DAC scan detected\n");
  dac_start = fs.input.e1h_dac_start; 
  dac_stop = fs.input.e1h_dac_stop;
  dac_inc = fs.input.e1h_dac_inc;
  
  printf("Params: %f %f %f \n",dac_start,dac_stop,dac_inc);
  if( dac_inc  == 0 )
    {
      cm_msg(MERROR,"begin_of_run","Invalid DAC scan increment value: %f",
	     dac_inc);
      return FE_ERR_HW;
    }
  
  
  if ( fabs(dac_inc) > fabs(dac_stop - dac_start)) 
    {
      cm_msg(MERROR,"begin_of_run","Invalid DAC scan increment value: %f",
	     dac_inc);
      return FE_ERR_HW;
    }
  
  if( ((dac_inc > 0) && (dac_stop < dac_start)) ||
      ((dac_inc < 0) && (dac_start < dac_stop)) )
    dac_inc *= -1;
  ftmp =  ( fabs(dac_stop - dac_start)/ fabs(dac_inc) ) +1;
  dac_ninc = (INT)ftmp;
  
  if(!d11)printf("ninc %f -> integer %d  diff %f dac_inc = %f\n",ftmp,dac_ninc,(dac_stop-dac_start),dac_inc);
  if( dac_ninc < 1)
    {
      cm_msg(MERROR,"begin_of_run","Invalid number of sweep steps: %d",
	     dac_ninc);
      return FE_ERR_HW;
    }
  gbl_FREQ_n = dac_ninc; /* set to max to satisfy cycle_start routine */
  dac_val = dac_start;
  printf("begin_of_run: DAC device will be set to =%f\n",dac_val);
  printf(" Selected %d DAC scan increments starting with %f by steps of %f\n",
	 dac_ninc, dac_start, dac_inc);

  psleep=0;  /* default value */
  size = sizeof( psleep);
  sprintf(str,"/Equipment/fifo_acq/Pol params/settling time (ms)");
  status = db_get_value(hDB, 0, str, &psleep , &size, 
			TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MINFO,"begin_of_run","cannot read %s; setting sleep time to 0 (%d)",str,status);
      psleep=0;
    }	
  printf("begin_of_run: Pol DAC scan settling time = %d\n",psleep);


  while (gbl_SCAN_N < 2) 
    {
      if( gbl_FREQ_n ==  dac_ninc) 
	{ /* N E W  S C A N  FOR FREQ */
	  gbl_FREQ_n = 0;
	  gbl_SCAN_N ++;
	  dac_val = dac_start; /* set to start value */		 
	} /* end of new scan */
      
      else  /* not a new scan */
	dac_val += dac_inc;   /* calculate new value */
      
      dac_val =  dac_start + (gbl_FREQ_n * dac_inc);
      gbl_FREQ_n ++;
      
      
      /*** Set the DAC ***/
      /* Set ODB handshake word = 0
       * Set ODB DAC value 
       * Wait for ODB handshake word = 1 
       * Potentially wait for SLLEP time
       * 
       * At end of scan, check for ODB handshake word =2 or more
       * read the DVM from ODB, etc
       */
      
      ihand = 0;
      db_set_data(hDB,hHand,&ihand,sizeof(ihand),1,TID_INT);
      db_set_data(hDB,hReq,&dac_val,sizeof(dac_val),1,TID_FLOAT);
      /*  ss_sleep(500); */ 
      ss_sleep (psleep);
      
      /* Now get the results */
      
      /* Get DVM value now */
      size = sizeof(ihand);
      icount = 0;
      icount_max = 1000;
      do {
	db_get_data(hDB, hHand,&ihand,&size,TID_INT);
	icount +=1;
      }
      while ( (ihand == 0) && (icount != icount_max) );
      if( icount == icount_max) {
	printf("Timeout waiting for DAC handshake\n");
	/* !!!! MUST SET THE MECHANISM TO STOP THE RUN ~~~~~ */
      }
      else {
	size = sizeof(rdata);
	status = db_get_data_index(hDB,hVar,&rdata,&size,0,TID_FLOAT);
	dac_val_set = (DWORD)((1000.*dac_val)+0.5);
	dvm_val = (INT)((1000000.*rdata) + 0.5);
	if(d10)
	  printf("\n +++ DAC +++ dac_val %d(%f), dvm_val=%d(%f)\n",
		 dac_val_set,dac_val,dvm_val,rdata);
      }
    }
  
  cm_disconnect_experiment();
}

