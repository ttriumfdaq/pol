/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Wed Jul 14 17:17:58 2004

\********************************************************************/

#define EXP_EDIT_DEFINED

typedef struct {
  char      run_title[88];
  DWORD     experiment_number;
  char      experimenter[32];
  char      sample[15];
  char      orientation[15];
  char      temperature[15];
  char      field[15];
  char      element[24];
  INT       mass;
  INT       dc_offset_v_;
  double    ion_source__kv_;
  double    laser_wavelength__nm_;
  BOOL      active;
  INT       num_scans;
  BOOL      edit_run_number;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"run_title = STRING : [88] test",\
"experiment number = DWORD : 1",\
"experimenter = STRING : [32] renee",\
"sample = STRING : [15] none",\
"orientation = STRING : [15] none",\
"temperature = STRING : [15] none",\
"field = STRING : [15] none",\
"Element = STRING : [24] Lanthanum",\
"Mass = INT : 139",\
"DC offset(V) = INT : 2078",\
"Ion source (kV) = DOUBLE : 14",\
"Laser wavelength (nm) = DOUBLE : 18587.6",\
"write data = LINK : [35] /Logger/Channels/0/Settings/Active",\
"Number of scans = LINK : [47] /Equipment/FIFO_acq/sis mcs/hardware/num scans",\
"Edit run number = BOOL : n",\
"",\
NULL }

#ifndef EXCL_FIFO_ACQ

#define FIFO_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} FIFO_ACQ_COMMON;

#define FIFO_ACQ_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 10",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] vwisac2",\
"Frontend name = STRING : [32] fePOL",\
"Frontend file name = STRING : [256] febnmr.c",\
"",\
NULL }

#define FIFO_ACQ_MDARC_DEFINED

typedef struct {
  struct {
    INT       number_defined;
    INT       num_bins;
    float     dwell_time__ms_;
    INT       resolution_code;
    char      titles[16][32];
    INT       bin_zero[16];
    INT       first_good_bin[16];
    INT       last_good_bin[16];
    INT       first_background_bin[16];
    INT       last_background_bin[16];
    struct {
      float     histogram_totals[16];
      float     total_saved;
    } output;
    struct {
      char      label[16];
      BOOL      active;
      INT       first_time_bin;
      INT       last_time_bin;
      INT       start_freq__hz_;
      INT       end_freq__hz_;
      char      last_file_written[128];
      char      time_file_written[32];
    } midbnmr;
  } histograms;
  char      time_of_last_save[32];
  char      last_saved_filename[128];
  char      saved_data_directory[128];
  DWORD     num_versions_before_purge;
  BOOL      end_of_run_purge_rename;
  char      archived_data_directory[128];
  char      archiver_task[80];
  DWORD     save_interval_sec_;
  BOOL      toggle;
  BOOL      disable_run_number_check;
  char      run_type[5];
  char      perlscript_path[50];
  BOOL      enable_mdarc_logging;
  struct {
    char      camp_hostname[128];
    char      temperature_variable[128];
    char      field_variable[128];
  } camp;
  BOOL      enable_poststop_rn_check;
} FIFO_ACQ_MDARC;

#define FIFO_ACQ_MDARC_STR(_name) char *_name[] = {\
"[histograms]",\
"number defined = INT : 6",\
"num bins = INT : 1",\
"dwell time (ms) = FLOAT : 2000",\
"resolution code = INT : -1",\
"titles = STRING[16] :",\
"[32] Const",\
"[32] FluM2",\
"[32] L+",\
"[32] R+",\
"[32] L-",\
"[32] R-",\
"[32] NBMB+",\
"[32] NBMF+",\
"[32] NBMB-",\
"[32] NBMF-",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"bin zero = INT[16] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"first good bin = INT[16] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 20",\
"[11] 20",\
"[12] 20",\
"[13] 20",\
"[14] 1",\
"[15] 1",\
"last good bin = INT[16] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1100",\
"[7] 1100",\
"[8] 1100",\
"[9] 1100",\
"[10] 60",\
"[11] 60",\
"[12] 60",\
"[13] 60",\
"[14] 800",\
"[15] 800",\
"first background bin = INT[16] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 0",\
"[15] 0",\
"last background bin = INT[16] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 50",\
"[7] 50",\
"[8] 50",\
"[9] 50",\
"[10] 10",\
"[11] 10",\
"[12] 10",\
"[13] 10",\
"[14] 0",\
"[15] 0",\
"",\
"[histograms/output]",\
"histogram totals = FLOAT[16] :",\
"[0] 2.74982e+08",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 28",\
"[5] 62",\
"[6] 0",\
"[7] 0",\
"[8] 199",\
"[9] 288",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"total saved = FLOAT : 2.74983e+08",\
"",\
"[histograms/midbnmr]",\
"label = STRING : [16] not used",\
"active = BOOL : n",\
"first time bin = INT : 0",\
"last time bin = INT : 1",\
"start freq (Hz) = INT : 1000",\
"end freq (Hz) = INT : 2000",\
"last file written = STRING : [128] not used",\
"time file written = STRING : [32] not used",\
"",\
"[.]",\
"time_of_last_save = STRING : [32] Thu Jul 31 15:51:41 2003",\
"last_saved_filename = STRING : [128] /is01_data/pol/dlog/2003/030201.msr",\
"saved_data_directory = STRING : [128] /is01_data/pol/dlog/2003",\
"num_versions_before_purge = DWORD : 4",\
"end_of_run_purge_rename = BOOL : y",\
"archived_data_directory = STRING : [128] /musr/dlog/bnmr",\
"archiver task = STRING : [80] /home/musrdaq/musr/mdarc/bin/cpbnmr",\
"save_interval(sec) = DWORD : 15",\
"toggle = BOOL : n",\
"disable run number check = BOOL : n",\
"run type = STRING : [5] test",\
"perlscript path = STRING : [50] /home/pol/online/perl",\
"enable mdarc logging = BOOL : n",\
"",\
"[camp]",\
"camp hostname = STRING : [128] polvw",\
"temperature variable = STRING : [128] /Test/dac_set",\
"field variable = STRING : [128] /Test/dac_set",\
"",\
"[.]",\
"enable poststop rn check = BOOL : y",\
"",\
NULL }

#define FIFO_ACQ_SIS_MCS_DEFINED

typedef struct {
  struct {
    char      ppg_loadfile[128];
    char      ppg_template[20];
    float     beam_on_time__ms_;
    float     frequency_stop__hz_;
    DWORD     num_frequency_steps;
    float     dwell_time__ms_;
    DWORD     num_dwell_times;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    DWORD     compiled_file_time__binary_;
    char      compiled_file_time[32];
    char      e1c_camp_path[32];
    char      e1c_camp_ifmod[10];
    char      e1c_camp_instr_type[32];
    char      e1n_epics_device[32];
    char      e1h_scan_device[10];
  } output;
  struct {
    DWORD     num_cycles;
    DWORD     fluor_monitor_thr;
    float     cycle_thr____;
    INT       diagnostic_channel_num;
    BOOL      re_reference;
    DWORD     num_polarization_cycles;
    DWORD     polarization_switch_delay;
    BOOL      enable_sis_ref_ch1_scaler_a;
    BOOL      enable_sis_ref_ch1_scaler_b;
    BOOL      enable_helicity_flipping;
    INT       helicity_flip_sleep__ms_;
    BOOL      ppg_acq_cycle_control;
    BOOL      check_rf_trip;
    INT       num_scans;
    BOOL      disable_nacell;
    BOOL      disable_laser;
    BOOL      disable_field;
  } hardware;
  struct {
    BOOL      test_mode;
    DWORD     num_bins;
    float     dwell_time__ms_;
    float     beam_time__ms_;
  } sis_test_mode;
  struct {
    BOOL      hold;
  } flags;
  struct {
    char      experiment_name[32];
    char      cfg_path[128];
    char      ppg_path[128];
    float     beam_off_time__ms_;
    DWORD     num_beam_precycles;
    DWORD     num_beam_acq_cycles;
    DWORD     frequency_start__hz_;
    DWORD     frequency_stop__hz_;
    DWORD     frequency_increment__hz_;
    float     rf_on_time__ms_;
    float     rf_off_time__ms_;
    DWORD     num_rf_on_delays__dwell_times_;
    float     mcs_enable_delay__ms_;
    float     mcs_enable_gate__ms_;
    float     mcs_max_delay__ms_;
    float     daq_service_time__ms_;
    float     minimal_delay__ms_;
    float     time_slice__ms_;
    float     e1b_dwell_time__ms_;
    float     rf_delay__ms_;
    float     bg_delay__ms_;
    DWORD     num_spectra_per_freq;
    DWORD     num_rf_cycles;
    INT       e2b_num_beam_on_dwell_times;
    BOOL      check_recent_compiled_file;
    DWORD     freq_single_slice_width__hz_;
    DWORD     num_freq_slices;
    float     prebeam_on_time__ms_;
    float     flip_360_delay__ms_;
    float     flip_180_delay__ms_;
    float     f_slice_internal_delay__ms_;
    char      beam_mode;
    char      counting_mode;
    float     f_select_pulselength__ms_;
    DWORD     e00_prebeam_dwelltimes;
    DWORD     rfon_dwelltime;
    DWORD     e00_beam_on_dwelltimes;
    DWORD     e00_beam_off_dwelltimes;
    DWORD     e00_rf_frequency__hz_;
    INT       rfon_duration__dwelltimes_;
    INT       num_type1_frontend_histograms;
    INT       num_type2_frontend_histograms;
    char      e1a_1b_pulse_pairs[4];
    char      e1a_1b_freq_mode[3];
    char      e1c_camp_device[5];
    float     e2c_beam_on_time__ms_;
    INT       num_cycles_per_supercycle;
    float     navolt_start;
    float     navolt_stop;
    float     navolt_inc;
    float     laser_start;
    float     laser_stop;
    float     laser_inc;
    float     e1c_camp_start;
    float     e1c_camp_stop;
    float     e1c_camp_inc;
    float     field_start;
    float     field_stop;
    float     field_inc;
    float     e1h_dac_start;
    float     e1h_dac_stop;
    float     e1h_dac_inc;
  } input;
} FIFO_ACQ_SIS_MCS;

#define FIFO_ACQ_SIS_MCS_STR(_name) char *_name[] = {\
"[Output]",\
"PPG_loadfile = STRING : [128] 1n.ppg",\
"PPG template = STRING : [20] 1n_.ppg",\
"beam on time (ms) = FLOAT : 0",\
"frequency stop (Hz) = FLOAT : 0",\
"num frequency steps = DWORD : 0",\
"dwell time (ms) = FLOAT : 2000",\
"num dwell times = DWORD : 1",\
"RF on time (ms) = FLOAT : 0",\
"RF off time (ms) = FLOAT : 0",\
"compiled file time (binary) = DWORD : 1089850365",\
"compiled file time = STRING : [32] Wed Jul 14 17:12:45 2004",\
"e1c Camp Path = STRING : [32] ",\
"e1c Camp IfMod = STRING : [10] ",\
"e1c Camp Instr Type = STRING : [32] ",\
"e1n Epics device = STRING : [32] none",\
"e1h scan device = STRING : [10] DAC",\
"",\
"[Hardware]",\
"num cycles = DWORD : 0",\
"Fluor monitor thr = DWORD : 0",\
"Cycle thr (%) = FLOAT : 0",\
"Diagnostic channel num = INT : 100",\
"Re-reference = BOOL : n",\
"num polarization cycles = DWORD : 0",\
"polarization switch delay = DWORD : 0",\
"Enable SIS ref ch1 scaler A = BOOL : n",\
"Enable SIS ref ch1 scaler B = BOOL : y",\
"Enable helicity flipping = BOOL : n",\
"helicity flip sleep (ms) = INT : 0",\
"PPG acq cycle control = BOOL : y",\
"Check RF trip = BOOL : y",\
"Num scans = INT : 0",\
"Disable NaCell = BOOL : y",\
"Disable Laser = BOOL : y",\
"Disable Field = BOOL : y",\
"",\
"[sis test mode]",\
"test mode = BOOL : n",\
"num bins = DWORD : 202",\
"Dwell time (ms) = FLOAT : 1",\
"beam time (ms) = FLOAT : 150",\
"",\
"[flags]",\
"hold = BOOL : n",\
"",\
"[Input]",\
"Experiment name = STRING : [32] 1h",\
"CFG path = STRING : [128] /home/pol/online/pol/ppcobj",\
"PPG path = STRING : [128] /home/pol/online/ppg-templates",\
"beam off time (ms) = FLOAT : 0",\
"num beam PreCycles = DWORD : 1",\
"num_beam_acq_cycles = DWORD : 100000",\
"frequency start (Hz) = DWORD : 1000",\
"frequency stop (Hz) = DWORD : 2000",\
"frequency increment (Hz) = DWORD : 100",\
"RF on time (ms) = FLOAT : 100",\
"RF off time (ms) = FLOAT : 100",\
"num RF on delays (dwell times) = DWORD : 10",\
"MCS enable delay (ms) = FLOAT : 1",\
"MCS enable gate (ms) = FLOAT : 2000",\
"MCS max delay (ms) = FLOAT : 1",\
"DAQ service time (ms) = FLOAT : 3000",\
"Minimal delay (ms) = FLOAT : 0.0005",\
"Time slice (ms) = FLOAT : 1e-04",\
"E1B Dwell time (ms) = FLOAT : 100",\
"RF delay (ms) = FLOAT : 30000",\
"Bg delay (ms) = FLOAT : 1",\
"Num spectra per freq = DWORD : 1",\
"Num RF cycles = DWORD : 50",\
"E2B Num beam on dwell times = INT : 1000",\
"Check recent compiled file = BOOL : y",\
"freq single slice width (Hz) = DWORD : 10",\
"Num freq slices = DWORD : 1",\
"prebeam on time (ms) = FLOAT : 1",\
"flip 360 delay (ms) = FLOAT : 10",\
"flip 180 delay (ms) = FLOAT : 40",\
"f slice internal delay (ms) = FLOAT : 10",\
"beam_mode = CHAR : P",\
"counting_mode = CHAR : 1",\
"f select pulselength (ms) = FLOAT : 10",\
"e00 prebeam dwelltimes = DWORD : 50",\
"RFon dwelltime = DWORD : 0",\
"e00 beam on dwelltimes = DWORD : 50",\
"e00 beam off dwelltimes = DWORD : 1000",\
"e00 rf frequency (Hz) = DWORD : 22064585",\
"rfon duration (dwelltimes) = INT : 0",\
"num type1 frontend histograms = INT : 6",\
"num type2 frontend histograms = INT : 10",\
"e1a&1b pulse pairs = STRING : [4] 000",\
"e1a&1b freq mode = STRING : [3] F0",\
"e1c Camp Device = STRING : [5] DAC",\
"e2c beam on time (ms) = FLOAT : 0",\
"num cycles per supercycle = INT : 0",\
"NaVolt start = FLOAT : 190",\
"NaVolt stop = FLOAT : 290",\
"NaVolt inc = FLOAT : 2",\
"Laser start = FLOAT : -2",\
"Laser stop = FLOAT : 2",\
"Laser inc = FLOAT : -0.005",\
"e1c Camp Start = FLOAT : 0",\
"e1c Camp Stop = FLOAT : 9.95",\
"e1c Camp Inc = FLOAT : 0.05",\
"Field start = FLOAT : 95",\
"Field stop = FLOAT : 100",\
"Field inc = FLOAT : 1.1",\
"e1h DAC Start = FLOAT : 0",\
"e1h DAC Stop = FLOAT : 5",\
"e1h DAC Inc = FLOAT : 0.05",\
"",\
NULL }

#define FIFO_ACQ_CAMP_SWEEP_DEVICES_DEFINED

typedef struct {
  struct {
    char      sweep_device_code[5];
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    char      camp_device_dependent_path[80];
    INT       integer_conversion_factor;
  } frequency_generator;
  struct {
    char      sweep_device_code[5];
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    char      camp_device_dependent_path[80];
    INT       integer_conversion_factor;
  } magnet;
  struct {
    char      sweep_device_code[5];
    char      camp_path[32];
    char      gpib_port_or_rs232_portname[10];
    char      instrument_type[32];
    char      camp_scan_path[80];
    char      scan_units[10];
    float     maximum;
    float     minimum;
    char      camp_device_dependent_path[80];
    INT       integer_conversion_factor;
  } dac;
} FIFO_ACQ_CAMP_SWEEP_DEVICES;

#define FIFO_ACQ_CAMP_SWEEP_DEVICES_STR(_name) char *_name[] = {\
"[frequency generator]",\
"Sweep Device code = STRING : [5] FG",\
"Camp path = STRING : [32] /frq",\
"GPIB port or rs232 portname = STRING : [10] 7",\
"Instrument Type = STRING : [32] bnc625a",\
"Camp scan path = STRING : [80] /frq/frequency",\
"Scan units = STRING : [10] Hz",\
"maximum = FLOAT : 500000",\
"minimum = FLOAT : 1000",\
"Camp device dependent path = STRING : [80] ",\
"integer conversion factor = INT : 1",\
"",\
"[magnet]",\
"Sweep Device code = STRING : [5] MG",\
"Camp path = STRING : [32] /Magnet",\
"GPIB port or rs232 portname = STRING : [10] /tyCo/5",\
"Instrument Type = STRING : [32] oxford_ips120",\
"Camp scan path = STRING : [80] /Magnet/mag_field",\
"Scan units = STRING : [10] T",\
"maximum = FLOAT : 7",\
"minimum = FLOAT : -7",\
"Camp device dependent path = STRING : [80] /Magnet/ramp_status",\
"integer conversion factor = INT : 10000",\
"",\
"[dac]",\
"Sweep Device code = STRING : [5] DAC",\
"Camp path = STRING : [32] /v_scan",\
"GPIB port or rs232 portname = STRING : [10] 0",\
"Instrument Type = STRING : [32] tip850_dac",\
"Camp scan path = STRING : [80] /v_scan/dac_set",\
"Scan units = STRING : [10] V",\
"maximum = FLOAT : 10",\
"minimum = FLOAT : -10",\
"Camp device dependent path = STRING : [80] /frq/amplitude",\
"integer conversion factor = INT : 1000",\
"",\
NULL }

#define FIFO_ACQ_CLIENT_FLAGS_DEFINED

typedef struct {
  BOOL      mdarc;
  BOOL      rf_config;
  BOOL      mheader;
  BOOL      frontend;
  BOOL      fe_epics;
  BOOL      enable_client_check;
  INT       client_alarm;
} FIFO_ACQ_CLIENT_FLAGS;

#define FIFO_ACQ_CLIENT_FLAGS_STR(_name) char *_name[] = {\
"[.]",\
"mdarc = BOOL : n",\
"rf_config = BOOL : y",\
"mheader = BOOL : n",\
"frontend = BOOL : y",\
"fe_epics = BOOL : n",\
"enable client check = BOOL : n",\
"client alarm = INT : 0",\
"",\
NULL }

#define FIFO_ACQ_POL_PARAMS_DEFINED

typedef struct {
  INT       settling_time__ms_;
} FIFO_ACQ_POL_PARAMS;

#define FIFO_ACQ_POL_PARAMS_STR(_name) char *_name[] = {\
"[.]",\
"Settling time (ms) = INT : 10",\
"",\
NULL }

#endif

#ifndef EXCL_CYCLE_SCALERS

#define CYCLE_SCALERS_SETTINGS_DEFINED

typedef struct {
  char      names[16][32];
} CYCLE_SCALERS_SETTINGS;

#define CYCLE_SCALERS_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[16] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 1",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#define CYCLE_SCALERS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} CYCLE_SCALERS_COMMON;

#define CYCLE_SCALERS_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] vwisac2",\
"Frontend name = STRING : [32] fePOL",\
"Frontend file name = STRING : [256] febnmr.c",\
"",\
NULL }

#endif

#ifndef EXCL_HISTO

#define HISTO_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} HISTO_COMMON;

#define HISTO_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 17",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] vwisac2",\
"Frontend name = STRING : [32] fePOL",\
"Frontend file name = STRING : [256] febnmr.c",\
"",\
NULL }

#endif

#ifndef EXCL_HDIAGNOSIS

#define HDIAGNOSIS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} HDIAGNOSIS_COMMON;

#define HDIAGNOSIS_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 4",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] vwisac2",\
"Frontend name = STRING : [32] fePOL",\
"Frontend file name = STRING : [256] febnmr.c",\
"",\
NULL }

#endif

#ifndef EXCL_INFO_ODB

#define INFO_ODB_EVENT_DEFINED

typedef struct {
  DWORD     helicity;
  DWORD     current_cycle;
  DWORD     cancelled_cycle;
  DWORD     current_scan;
  double    ref_helup_thr;
  double    ref_heldown_thr;
  double    current_helup_thr;
  double    current_heldown_thr;
  DWORD     rf_state;
  DWORD     fluor_monitor_counts;
  float     epicsdev_set_v_;
  float     epicsdev_read_v_;
  float     campdev_set;
  float     campdev_read;
} INFO_ODB_EVENT;

#define INFO_ODB_EVENT_STR(_name) char *_name[] = {\
"[.]",\
"helicity = DWORD : 0",\
"current cycle = DWORD : 68",\
"cancelled cycle = DWORD : 1",\
"current scan = DWORD : 1",\
"Ref HelUp thr = DOUBLE : 64",\
"Ref HelDown thr = DOUBLE : 64",\
"Current HelUp thr = DOUBLE : 0",\
"Current HelDown thr = DOUBLE : 56",\
"RF state = DWORD : 0",\
"Fluor monitor counts = DWORD : 32",\
"EpicsDev Set(V) = FLOAT : 0",\
"EpicsDev Read(V) = FLOAT : 0",\
"Campdev set = FLOAT : 0",\
"Campdev read = FLOAT : 0",\
"",\
NULL }

#define INFO_ODB_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} INFO_ODB_COMMON;

#define INFO_ODB_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] vwisac2",\
"Frontend name = STRING : [32] fePOL",\
"Frontend file name = STRING : [256] febnmr.c",\
"",\
NULL }

#endif

#ifndef EXCL_DVM

#define DVM_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} DVM_COMMON;

#define DVM_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 6",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] ",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 60",\
"Frontend host = STRING : [32] midtis07.triumf.ca",\
"Frontend name = STRING : [32] feDVM",\
"Frontend file name = STRING : [256] feDVM.c",\
"",\
NULL }

#define DVM_SETTINGS_DEFINED

typedef struct {
  INT       address;
  INT       modules[3];
  char      scanlist[64];
  char      slot1config[20][64];
  char      slot1names[20][32];
  struct {
    char      address[32];
    float     demand;
    INT       handshake;
  } slot2dac1;
  struct {
    char      address[32];
    float     demand;
    INT       handshake;
  } slot2dac2;
  struct {
    struct {
    } dvm;
  } devices;
} DVM_SETTINGS;

#define DVM_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Address = INT : 8",\
"Modules = INT[3] :",\
"[0] 34901",\
"[1] 34907",\
"[2] 0",\
"ScanList = STRING : [64] 101,102",\
"Slot1Config = STRING[20] :",\
"[64] SENS:FUNC "VOLT:DC";SENS:VOLT:DC:RANG 10;SENS:VOLT:DC:NPLC 1",\
"[64] SENS:FUNC "TEMP";SENS:TEMP:TRAN:TC:TYPE J;SENS:TEMP:NPLC 1",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"[64] ",\
"Slot1Names = STRING[20] :",\
"[32] Readback (Volts)",\
"[32] Temp",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"[32] ",\
"",\
"[Slot2DAC1]",\
"Address = STRING : [32] 204",\
"Demand = FLOAT : 3.35",\
"Handshake = INT : 1",\
"",\
"[Slot2DAC2]",\
"Address = STRING : [32] 205",\
"Demand = FLOAT : 0",\
"Handshake = INT : 0",\
"",\
NULL }

#endif

