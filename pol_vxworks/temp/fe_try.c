/*******************************************************************\

  Name:         fe_runlog.c
  Created by:   R. Poutissou 
                based on fedaq.c from TWIST, written by Peter Green

  Contents:     Fill runlog file for logging purposes
		This one only responds to Start/Stop run commands.

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "midas.h"
#include "mcstd.h"

#include "experim.h"
#include "client_flags.h"  // prototypes

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fe_runlog";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
//INT display_period = 3000;
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 1000;
INT max_event_size_frag = 5*1024; /* unused */

/* buffer size to hold events */
INT event_buffer_size = 10*1000;

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

HNDLE hDB;
INT gbl_run_number;

/* Root of Equipment/Settings tree */
HNDLE hSettingRoot;

/* Root of Equipment/Variables tree */
HNDLE hVarRoot;

FILE* open_runlog (void);

  // client flags
  char    eqp_name[]="fifo_acq";
FIFO_ACQ_CLIENT_FLAGS fcf;
char client_str[132];
INT debug_cf=1;
INT status,size;
HNDLE hCF;
BOOL tr_flag;
INT run_state;
INT run_number;
INT start_time;

  // function declarations
INT my_start(INT rn, char *error);
INT my_stop(INT rn, char *error);
INT my_prestart(INT rn, char *error);
void write_message1(INT status, char *name);


/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {
  { "" }
};


#ifdef __cplusplus
}
#endif

#include "client_flags.c"  // code for client_flags handling
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  HNDLE hKey;
  int status;
  char buff[256];
  char *s;
  INT fe_post_stop ();
  
  /* Set root of directory tree
   * (Note: hDB comes from the caller - mfe.c)
   */

  //cm_set_watchdog_params (FALSE, 0);


  /* Renee would like me to get the run number correct right from the start
   */
  status = my_find_and_load (0, "/Runinfo/Run number", NULL, 
			     &gbl_run_number, 1, TID_INT);

  status = cm_register_transition (TR_POSTSTOP, fe_post_stop);

  status = cm_register_transition (TR_PRESTART, my_prestart);
  if(status != SUCCESS)
    printf("could not register for prestart transition\n");

  status = cm_register_transition (TR_START, my_start);
  if(status != SUCCESS)
    printf("could not register for start transition\n");

  status = cm_register_transition (TR_STOP,  my_stop);
  if(status != SUCCESS)
    printf("could not register for stop transition\n");
 
  //  set_global_status (ST_NORMAL);

  /* Get handles to various points in the ODB tree

  sprintf (buff, "/Equipment/%s/Settings", equipment[0].name);
  status = my_find (0, buff, &hSettingRoot);
  if (status != SUCCESS) {
    return (status);
  }

  sprintf (buff, "/Equipment/%s/Variables", equipment[0].name);
  status = my_find (0, buff, &hVarRoot);
  if (status != SUCCESS) {
    return (status);
  }
  */

  status = setup_clientflags();
  return (status);
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  return SUCCESS;
}

FILE *open_runlog (void)
{
  int status;
  char buff [256];
  int size;
  HNDLE hKey;
  FILE *fp;
  
  status = my_find (0, "/Logger/Data dir", &hKey);
  if (status != SUCCESS) {
    return NULL;
  }
  size = 256;
  status = db_get_data (hDB, hKey, buff, &size, TID_STRING);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "Can't get value for /Logger/Data dir - runlog not updated");
    return NULL;
  }
  size = strlen (buff);
  if (buff [size - 1] != '/') {
    strcat (buff, "/");
  }
  strcat (buff, "runlog.txt");
  fp = fopen (buff, "a");
  return (fp);
}

/*-- Begin of Run --------------------------------------------------*/

#define MAXBUFF 256
/* These are really positions within the buffer where things start
 */
#define TIMELEN 42
#define OPERATORLEN 32
#define COMMENTLEN 88
#define PURPOSELEN 80
#define ENABLELEN 8
#define EVCOUNTLEN 20
#define NCYCLELEN 25
#define TRIGGERLEN 24
#define MASSLEN 8
#define DCOFFLEN 8


static BOOL write_data;

int padblanks (char *s, int n)
{
    /* Pad a string with n blanks at the end
     */

    s += strlen (s);
    while (n--) *s++ = ' ';
    *s = '\0';
    return (0);
}

INT begin_of_run(INT run_number, char *error)
{
  int status;
  HNDLE hKey;
  char buff1 [MAXBUFF], buff2 [MAXBUFF];
  INT runnum;
  INT data_int;
  int size;
  FILE *fp;
  time_t now;
  char *s;
  
  /* put here clear scalers etc. */
  gbl_run_number = run_number;

  /* Set Running status
   */
  //  set_run_status (ST_RUNNING);
  time (&now);

/* Generate message for runlog
 * Note: On errors here I just give up - but still return success.
 * I still want the run to start.
 */
  s = buff1;
  status = my_find_and_load (0, "/Runinfo/Run Number", NULL, &runnum, 1, TID_INT);
  if (status != SUCCESS)
    {
       printf("begin_of_run: failure after my_find_and_load for \"/Runinfo/Run Number\"  (%d)\n",status);
       return (SUCCESS);
    }
  sprintf (s, "Start Run %5d %s", runnum, ctime (&now));

/* ctime puts a c/r on the end of things
 * We'll fix that!
 */
  size = strlen (s);
  s [size-1] = '\0';
  padblanks (s, TIMELEN);

  status = my_find (0, "/Experiment/Edit on start", &hKey);
  if (status != SUCCESS)
    {
      printf("begin_of_run: failure after my_find for \"/Experiment/Edit on start\"  (%d)\n",status);
      return (SUCCESS);
    }
  if(!hKey)
    {
      printf("begin_of_run: invalid kKey after my_find  (%d)\n");
      return (SUCCESS);
    }

  s += TIMELEN;
  sprintf(s,"E:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hKey, "Experimenter", NULL, s, OPERATORLEN, TID_STRING);
  if (status != SUCCESS)
    return (SUCCESS);
  padblanks (s, OPERATORLEN);
  s += OPERATORLEN;

  sprintf(s,"T:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hKey, "run_title", NULL, s, COMMENTLEN, TID_STRING);
  if (status != SUCCESS)
    return (SUCCESS);
/* Just in case
 */
  padblanks (s,COMMENTLEN );
  s += COMMENTLEN;
  *s = '\0';

/* Do it again for other info and Logger status
 */
  strncpy (buff2, buff1, TIMELEN);
  s = buff2;
  s += TIMELEN;

  sprintf(s,"E:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hKey, "Element", NULL, s, TRIGGERLEN, TID_STRING);
  if (status != SUCCESS)
    return (SUCCESS);
  padblanks (s, TRIGGERLEN);
  s += TRIGGERLEN;

  sprintf(s,"M:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hKey, "Mass", NULL, &data_int, 1, TID_INT);
  if (status != SUCCESS)
    return (SUCCESS);
  sprintf (s," %i6 ",data_int);
  padblanks (s, MASSLEN);
  s += MASSLEN;

  sprintf(s,"D:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hKey, "DC offset(V)", NULL, &data_int, 1, TID_INT);
  if (status != SUCCESS)
    return (SUCCESS);
  sprintf (s," %i6 ",data_int);
  padblanks (s, DCOFFLEN);
  s += DCOFFLEN;

  sprintf(s,"L:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hKey, "Write Data", NULL, &write_data, 1, TID_BOOL);
  if (status != SUCCESS)
    return (SUCCESS);
  if (write_data) {
      sprintf (s, "Enabled ");
  } else {
      sprintf (s, "Disabled");
  }
  padblanks (s, ENABLELEN);
  s += ENABLELEN;

  *s = '\0';
  
  printf ("%s\n", buff1);
  printf ("%s\n", buff2);

  fp = open_runlog ();
  if (fp == NULL) return (SUCCESS);
  
  fprintf (fp, "%s\n", buff1);
  fprintf (fp, "%s\n", buff2);
  fclose (fp);

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  /* Set Stopped status
   */
  //  set_run_status (ST_STOPPED);
  return SUCCESS;
}

INT fe_post_stop(INT run_number, char *error)
{
  int status;
  char buff [256];
  INT runnum;
  int size;
  FILE *fp;
  DWORD ncycle;
  double evcount;
  time_t now;
  char *s;

  time (&now);
  
  printf("fe_post_stop: starting with rn=%d\n",run_number);
  //  status = my_find_and_load (0, "/Runinfo/Run Number", NULL, &runnum, 1, TID_INT);
  size=sizeof(runnum);
  status = db_get_value(hDB,0,"/Runinfo/Run Number",&runnum, &size, TID_INT, FALSE);
  if (status != SUCCESS)
    {
      printf("error getting value of run number (%d)\n",status);
      return (SUCCESS);
    }
  else
    printf("run number is %d\n",runnum);
  s = buff;
  sprintf (s, "End   Run %5d %s", runnum, ctime (&now));

/* ctime puts a c/r on the end of things
 */
  size = strlen (s);
  s [size-1] = '\0';
  strcat (s, "                                ");

  /* Modified to put out statistics 
   */
  s += TIMELEN;
  status = my_find_and_load (0, "/Equipment/Info ODB/Variables/current cycle", NULL,
			     &ncycle, 1, TID_DWORD);
  if (status != SUCCESS)
    return (SUCCESS);
  sprintf (s, "Cycles:  %10i,", ncycle);
  strcat (buff, "               ");
  s += NCYCLELEN;
  
  if (write_data) {
      status = my_find_and_load (0, "/Logger/Channels/0/Statistics/Events written", NULL,
				 &evcount, 1, TID_DOUBLE);
      if (status != SUCCESS)
	  return (SUCCESS);
      sprintf (s, "Logger %10.0f ", evcount);
  } else {
      sprintf (s, "Logger Disabled");
  }
  strcat (buff, "               ");
  s += EVCOUNTLEN;
  *s = '\0';

  printf ("%s\n", buff);

  fp = open_runlog ();
  if (fp == NULL) return (SUCCESS);
  fprintf (fp, "%s\n", buff);
  fclose (fp);

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  /* Set Paused status
   */
  //  DB0PRINT("Pause run %d\n",gbl_run_number);
  //  set_run_status (ST_PAUSED);
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  /* Set Running status
   */
  //DB0PRINT("Resume run %d\n",gbl_run_number);
  //  set_run_status (ST_RUNNING);
  return SUCCESS;
}

INT my_prestart(INT rn, char *error)
{

  BOOL enable_client_check;
  char str[256];

 //  DO NOT CLEAR CLIENT FLAGS HERE ( in case any clients also use a prestart)
  //  they are cleared after a stop ready for next time.

  start_time =  ss_time(); // set a timer when run was started
  if(debug_cf)
    printf("my_prestart: starting, setting start_time to %d\n",start_time);
  tr_flag = FALSE; // and a flag for check_client_flags

  // get the latest value
  size = sizeof(fcf.enable_client_check);
  sprintf(str,"%s/enable client check",client_str);
  status = db_get_value(hDB, 0, str, &fcf.enable_client_check, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"tr_prestart","could not get value for \"%s\"  (%d)",str,status);
      return(status);
    }
  if(debug_cf)
    printf("my_prestart: ending, disable Client flag check = %d\n",fcf.enable_client_check);

  return SUCCESS;
}


 /*----- start  ----------------------------------------------*/
INT my_start(INT rn, char *error)
{
  printf("\nRun %d starting\n",rn);

  if(debug_cf)
    printf("my_start: Calling setup_hot_clients\n");

  status =  setup_hot_clients();
  if (status != DB_SUCCESS)
      return(status); // failure from hot links
    
  /* set mdarc's client flag to SUCCESS now */
  if(debug_cf)
    printf("my_start: setting mdarc client flag TRUE\n");
  fcf.mdarc=TRUE;
  size = sizeof(fcf.mdarc);
  status = db_set_value(hDB, hCF, "mdarc", &fcf.mdarc, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "my_start", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
      return status;
    } 
  return status;
}


INT my_stop(INT rn, char *error)
{
  /* remove hot link on client flags while the run is off */
  if(debug_cf)printf("my_stop: starting\n");
  status = close_client_hotlinks();
  if(status != DB_SUCCESS)
    printf("my_stop: error closing client hotlinks\n");

  start_time = 0; // make sure check_client_flags is not called if run stopped soon after starting.  
  /* clear client success flags ready for next time */
  status = clear_client_flags();
  if(status != DB_SUCCESS)
    printf("my_stop: error clearing client success flags\n");
  return status;
}


INT setup_clientflags(void)
{
  /* called from frontend_init */
  run_number = gbl_run_number; // "run_number" needed by client flag routines
  status = create_ClFlgs_record(); // create the record for the client flags
  if (status != DB_SUCCESS)
    {
      printf("setup_clientflags: error return from create_ClFlgs_record (%d)\n",status);
      return(status);
    }

  // initialize a few things for client_flags
  start_time = 0; // dummy value


  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_clientflags","key not found /Runinfo/State (%d)",status);
    return(status);
  }

  if(run_state == STATE_STOPPED)
    {
      printf("setup_clientflags: run is STOPPED, clearing client flags\n");
      status = clear_client_flags();
      if(status != DB_SUCCESS)
	return status;
    }
  else if (run_state == STATE_RUNNING)
    {  /* run is already in progress; setup hotlinks */
      if(debug_cf)printf("setup_clientflags: running already so calling setup_hot_clients\n");
      status =  setup_hot_clients();
      if (status != DB_SUCCESS)
      {
	return(status); // failure from hot links
      }
    }
  
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */

  if(start_time<= 0)return;

  

  if(start_time > 0) // run has recently started 
    {
      if(ss_time() - start_time > 20) // allow 20s for everything to start
	{
	  if(debug_cf)
	    printf("frontend_loop: ***  about to call check_client_flag, start_time=%d, ss_time=%d, tr_flag=%d\n",
		   start_time,ss_time(),tr_flag);
	  /* if !fcf.enable_client_check, then check_client_flag will issue a warning message
	     but will not stop the run */
	  status = check_client_flags(&tr_flag); // may stop the run
	  if(status == DB_SUCCESS)
	    {
	      if(debug_cf)printf("main:check_client_flags returns status=%d and tr_flag=%d\n",status,tr_flag);
	      if(tr_flag) // run is in transition; leave the flag set for next time
		printf("frontend_loop:run is still in transition... waiting to recheck client flags\n");
	      else
		{
		  if(debug_cf)printf("frontend_loop:resetting start_time to 0 to prevent check_client_flags being called again\n");
		  start_time = 0; // reset so we don't come here again
		}
	    }	    
	}
    } // start_time > 0
  
  
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\
  
  Readout routines for different events

\********************************************************************/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  if (test) {
    ss_sleep (count);
  }
  return (0);
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch(cmd)
    {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_ATTACH:
      break;
    case CMD_INTERRUPT_DETACH:
      break;
    }
  return SUCCESS;
}

INT my_find_and_load (HNDLE hRoot, const char *name, HNDLE *hKey, void *data,
		   int numvalues, int type)
{
  /* function to locate the Key for data and load it from the odb
   * hKey may be null if the key is not required
   * Returns SUCCESS if successful
   */
  HNDLE hTemp;
  int size;
  int status;
  char my_name[128];
  return SUCCESS;


  sprintf(my_name,"%s",name);
  printf("my_find_and_load: name = %s and my_name=%s\n",name,my_name);
  status = db_find_key (hDB, hRoot, my_name, &hTemp);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't find Key for %s", my_name);
    return (status);
  }
  if (type == TID_BYTE || type == TID_SBYTE || type == TID_CHAR || type == TID_STRING)
    size = numvalues;
  else if (type == TID_WORD || type == TID_SHORT)
    size = numvalues * sizeof (short);
  else if (type == TID_DWORD || type == TID_INT
	   || type == TID_BOOL  || type == TID_FLOAT)
    size = numvalues * sizeof (DWORD);
  else if (type == TID_DOUBLE)
    size = numvalues * sizeof (double);
  else {
    cm_msg (MERROR, frontend_name, "unrecognized type %d", type);
    return (FE_ERR_ODB);
  }
  status = db_get_data (hDB, hTemp, data, &size, type);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't get value for %s", my_name);
    return (status);
  }
  if (hKey != NULL)
    *hKey = hTemp;
  return (SUCCESS);
}

INT my_find (HNDLE hRoot, const char *name, HNDLE *hKey)
{
  /* Find key in the ODB
   */
  int status;
  char my_name[128];
 
  sprintf(my_name,"%s",name);
  printf("my_find: name = %s and my_name=%s\n",name,my_name);
  hKey=NULL;
  status = db_find_key (hDB, hRoot, my_name, hKey);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't find Key for %s (%d)", my_name,status);
 }
 printf("my_find: returning hKey=%d\n",hKey);
 return (status);
}


void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}
