/*  HelTest.c

Tests helicity routines used in febnmr.c

HelTest()   tests getting ID, reading

Used with routines in conn.c (use callbacks)


CVS log information:
$Log: HelTest.c,v $
Revision 1.1  2005/02/04 22:16:29  suz
initial version; direct access to Helicity Epics channels




*/
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "EpicsStep.h"

#ifndef LOCAL
#define LOCAL static
#endif
char * ca_name (int chan);

void HelTest(void);

int test_cycle(int Rchan_id, int Wchan_id);

/* globals */
int ddd=0;
int Rchid_helSwitch;
int Rchid_helOn;
int Rchid_helOff;


/* Epics names of Helicity variables */ 

char RhelOn[]="ILE2:POLSW2:STATON";
char RhelOff[]="ILE2:POLSW2:STATOFF";
char RhelSwitch[]="ILE2:POLSW2:STATLOC";/* 0=DAQ has control 1=Epics has control */

void HelTest(void)
{
  int status;
  float value;

  status=caGetId (RhelOn, RhelOff, &Rchid_helOn, &Rchid_helOff);
  if(status==-1)
    {
      printf("HelTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }

  printf("caGetId returns Rchid_helOn = %d and Rchid_helOff = %d\n",Rchid_helOn, Rchid_helOff);
  
  if (caCheck(Rchid_helOn))
    printf("caCheck says channel %s is connected\n",RhelOn);
  else
    printf("caCheck says channel %s is NOT connected\n",RhelOn);
  if (caCheck(Rchid_helOff))
    printf("caCheck says channel %s is connected\n",RhelOff);
  else
   printf("caCheck says channel %s is NOT connected\n",RhelOff);
 



  status=caGetSingleId (RhelSwitch, &Rchid_helSwitch );
  if(status==-1)
    {
      printf("HelTest: Bad status after caGetSingleID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("caGetSingleId returns Rchid_helSwitch = %d \n",Rchid_helSwitch);
  
  if (caCheck(Rchid_helSwitch))
    printf("caCheck says channel %s is connected\n",RhelSwitch);
  else
    printf("caCheck says channel %s is NOT connected\n",RhelSwitch);
  



  printf("\nHelTest: Now calling caRead for %s  with chan_id=%d\n",RhelSwitch,Rchid_helSwitch);
  status=caRead(Rchid_helSwitch,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_helSwitch));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_helSwitch),value);




  printf("\nHelTest: Now calling caRead for %s  with chan_id=%d\n",RhelOn,Rchid_helOn);
  status=caRead(Rchid_helOn,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_helOn));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_helOn),value);



  printf("HelTest: Now calling caRead for %s  with chan_id=%d\n",RhelOff,Rchid_helOff);
  status=caRead(Rchid_helOff,&value);
  if(status==-1)
    {
      printf("HelTest: Bad status after caRead for %s\n", ca_name(Rchid_helOff));
      return;
    }
  else
    printf("HelTest: Read value from %s as %8.3f\n", ca_name(Rchid_helOff),value);
  caExit();

  return;
}


