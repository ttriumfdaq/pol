#include <stdio.h>
int main(void)
{
  float dac_start=5;
  float dac_stop=-3;


  printf("DAC_set_scan_params: DAC start=%f stop=%f \n",dac_start,dac_stop);
  if ((dac_start > 0 && dac_stop < 0) || (dac_stop > 0 && dac_start < 0))
    printf("fail \n");

  dac_start = 0; dac_stop = -3; 
  printf("DAC_set_scan_params: DAC start=%f stop=%f \n",dac_start,dac_stop);
  if ((dac_start > 0 && dac_stop < 0) || (dac_stop > 0 && dac_start < 0))
    printf("fail \n");

  dac_start = -2; dac_stop = +3; 
  printf("DAC_set_scan_params: DAC start=%f stop=%f \n",dac_start,dac_stop);
  if ((dac_start > 0 && dac_stop < 0) || (dac_stop > 0 && dac_start < 0))
    printf("fail \n");

  dac_start = 2; dac_stop = 3; 
  printf("DAC_set_scan_params: DAC start=%f stop=%f \n",dac_start,dac_stop);
  if ((dac_start > 0 && dac_stop < 0) || (dac_stop > 0 && dac_start < 0))
    printf("fail \n");

  dac_start = -2; dac_stop = -3; 
  printf("DAC_set_scan_params: DAC start=%f stop=%f \n",dac_start,dac_stop);
  if ((dac_start > 0 && dac_stop < 0) || (dac_stop > 0 && dac_start < 0))
    printf("fail \n");

  return 1;
}
