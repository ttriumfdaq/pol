
#ifdef GONE  
#ifndef POL  /* POL expt doesn't actually use hot_rereference or flip the helicity */  
  /* hotlink rereference */
  if (lhot_rereference)    
    {
      if(exp_mode != 1) 
	{
	  if(gbl_ppg_hel==HEL_DOWN) 
	    { /*  use set value; readback is unreliable */
	      hot_rereference = TRUE;
	      lhot_rereference = FALSE;
	    }
	  else
	    cm_msg(MINFO,"cycle_start",
		   "can only rereference on helicity DOWN");
	} 
      else 
	{
	  hot_rereference = TRUE;
	  lhot_rereference = FALSE;
	}
    
      if(d3)printf("\ncycle_start: detected hot rereference...\n");
    }
  

  
  if(!fs.hardware.enable_dual_channel_mode)
    {
      /* read helicity if it has flipped earlier (single channel mode)
	 in dual channel mode - presently no reliable readback */
      
      if(!fs.hardware.disable_helicity_checking)
	{ 
	  if(dh)printf("flip=%d gbl_hel_flipped=%d\n",flip,gbl_hel_flipped);
	  if(flip && !gbl_hel_flipped)
	    { /* flip is true but helicity didn't flip */
	      gbl_hel_flipped=TRUE; /* make sure we only get these messages once per attempted flip */
	      status = helicity_read(); /* updated gbl_ppg_hel and gbl_HEL */
	      if(status == FE_ERR_HW)
		{
		  cm_msg(MERROR,"cycle_start","cannot read helicity state from EPICS (%d), stopping run",status);
		  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag)*/
		  return status; /* error return */
		}
	      if(dh) 
		printf("\n cycle_start: after helicity_read, gbl_ppg_hel=%d  gbl_HEL = %d\n", gbl_ppg_hel, gbl_HEL);
	      
	      if(gbl_HEL != gbl_ppg_hel) /* or status is mismatch */
		{ 
		  /* For now, continue as readback may be slow  */
		  if(!dhw)
		    {
		      printf("\ncycle_start: WARNING According to the readback, helicity still has not flipped\n");
		      printf("  you can disable helicity checking  \"..hardware/disable helicity checking\" in odb & restart run\n");
		    }
		}
	    } /* end of failed helicity flip */
	} /* end of helicity checking */
    }


#endif
#endif /* GONE */
