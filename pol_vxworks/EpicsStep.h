/* EpicsStep.h

  Header file for EpicsStep.c

 $Log: EpicsStep.h,v $
 Revision 1.2  2004/11/24 01:42:40  suz
 added parameter device_typical_offset to subroutines to allow NaCell to have a variable offset

 Revision 1.1  2003/05/01 21:32:06  suz
 initial bnmr version; identical to bnmr1 Rev1.2

 Revision 1.2  2003/01/09 21:37:32  suz
 add EpicsStep2

 Revision 1.1  2002/06/05 17:27:08  suz
 header file for EpicsStep.c


*/

typedef struct {
  int Rchan_id;      /* read channel ID */ 
  int Wchan_id;      /* write channel ID */ 
  float max_offset;    /* maximum allowable difference between values written and read back */
  float typical_offset; /* typical offset at this setpoint */
  float min_set_value; /* minimum allowable value for set point */
  float max_set_value; /* maximum allowable value for set point */
  float stability;    /* values read back should be stable to within this value     Used by EpicsSet only */
  int   debug;        /* debug */
} EPICS_CONSTANTS;

/* prototypes */
int EpicsSet (  float val, float device_typical_offset, float *pRvalue,  float *pOffset, 
		EPICS_CONSTANTS epics_constants) ; 
int EpicsSet_step ( float val,float offset, float device_typical_offset, float step, float diff,
		    float *pRvalue, float *pOffset, EPICS_CONSTANTS epics_constants);
int EpicsSet2 (  float val, float last_offset, float device_typical_offset, float *pRvalue,  
		 float *pOffset, EPICS_CONSTANTS epics_constants) ; 

/* global */
int d4; /* debug ca routines and event handlers etc. */ 
int d6; /* debug  in LaserSet and LaserSet_step */





