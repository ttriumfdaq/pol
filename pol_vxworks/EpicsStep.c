/*   EpicsStep.c

Routines to step an Epics device - uses routines in conn.c 
Based on NaStep.c
 *
 * Modification Log:
 * -----------------
CVS log information:
$Log: EpicsStep.c,v $
Revision 1.6  2004/11/24 20:29:10  suz
Add parameter typical_offset to EpicsStep and EpicsSet(2) to allow variable offset for NaCell; add wrote_msg

Revision 1.5  2004/05/25 22:40:05  suz
change a comment

Revision 1.4  2003/12/01 19:46:56  suz
changes for Epics reconnect

Revision 1.3  2003/06/26 21:33:36  suz
add a debug

Revision 1.2  2003/06/24 23:20:26  suz
change Volt to value to accomodate MagField (current)

Revision 1.1  2003/05/02 20:25:49  suz
initial bnmr version; identical to bnmr1 v1.4

Revision 1.4  2003/01/09 21:36:24  suz
change algorithm for repeat; add EpicsStep2

Revision 1.3  2003/01/08 19:24:46  suz
change typical_offset calc.

Revision 1.2  2002/06/07 18:32:08  suz
replace conn.h by connect.h

Revision 1.1  2002/06/05 17:25:31  suz
NaStep routines from conn.c generalized for any epics device



 */
#include <string.h>
#include <stdio.h>
#include <math.h>



#ifndef LOCAL
#define LOCAL static
#endif

#include <alarmString.h> /* EPICS header files */
#include "cadef.h"
#include "epicsTime.h" 
#include "connect.h"
#include "EpicsStep.h"  /* defines EPICS_constants */
/* global */
int d6=0; /* debug */

 /* Flag used to communicate between main() and event_handlers() */

/* All constants are now in epics_constants (defined in EpicsStep.h)  */

/* PARAMETERS: */

int nloops=10; /* we don't want to run out of loops & have to send another setpoint */
int nloops_step=6; /* we don't want to run out of loops & have to send another setpoint */

/*  
    EpicsSet
*/ 
int EpicsSet ( float val,         /* set value */
	       float device_typical_offset, /* typical offset at this value */
	   float *pRvalue,    /*  output: value read back */
	   float *pOffset,    /*  output: set/read offset value */
	  EPICS_CONSTANTS epics_constants)  
{

  /* assumes chan_id is set up by a call to caGetId

     sets the set value  & checks it is within epics_constants.max_offset of set value 

     returns   0  SUCCESS : value changed & is within limits

               -1 hardware failure returned  from setting/reading Epics 
	       -2 invalid input parameters
      
	       -4  offset between read and set values is outside input parameter (offset)

    Assumes:
    Maximum offset between read/set values is epics_constants.max_offset
    callback on set values is actually working!

    Tries for an offset <  device_typical_offset (an input parameter)
    After 5 loops, accepts epics_constants.max_offset

  */


     /* Read value */
  float EpicsVal_read , EpicsValue, EpicsVal_prev ;
  int status;
  int  i,loop_cnt,ltemp;
  float my_offset=0.0;
  float typical_offset;
  int wrote_msg;

  wrote_msg=0;
  if( epics_constants.debug) 
    d6=1; /* global debug */
  else
    d6=0;
  
  if(d6)
    {
      printf(" EpicsSet starting with constants :  Rchan_id = %d, Wchan_id = %d, \n",     
	     epics_constants.Rchan_id, epics_constants.Wchan_id);
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f, debug=%d\n",
	      epics_constants.max_offset, epics_constants.min_set_value, epics_constants.max_set_value,  
	     epics_constants.stability, epics_constants.debug);
      printf("  typical_offset = %.4f and val = %f\n",device_typical_offset,val);
    }
  typical_offset = device_typical_offset;
  /* get some values from parameter list */
  EpicsValue = val;

  if(EpicsValue < epics_constants.min_set_value || EpicsValue > epics_constants.max_set_value )
    {
      printf("EpicsSet: Invalid set value (%.4f) for Epics device\n",val);
      return (-2);
    }
  

  /* check for valid connection */
  if(epics_constants.Rchan_id < 0 || epics_constants.Wchan_id < 0 )
    return -1;
  else if  ( ca_state((chid)epics_constants.Rchan_id)!= cs_conn)
    return -1;
  else  if  ( ca_state((chid)epics_constants.Wchan_id)!= cs_conn)
    return -1;

  /* 
     
     now write the value 
   
  */
  if(d6)printf("Calling caWrite with parameters %d and EpicsValue=%f\n",epics_constants.Wchan_id,EpicsValue);
  status=caWrite(epics_constants.Wchan_id,&EpicsValue);
  if(status==-1)
    {
      printf("EpicsSet: Bad status after caWrite for set point %.4f\n",EpicsValue);
      
      return (status);
    }  
  else  
    if(d6)printf("Set value to %8.3f\n",EpicsValue);
  
  
  
  /*
    
    Is the value now close to the setpoint ?
    
  */
  
  loop_cnt = 0;
  while (loop_cnt < nloops)
    {   /* nloops is 10 */
      if(loop_cnt == (nloops-2) )
	typical_offset=epics_constants.max_offset;  /* widen the offset range after 3 loops (or on the last loop) */
      
      /* print a message if we have a long wait */
      if(loop_cnt == 4)
	{
	  printf("EpicsSet: Trying to set Epics device to %.4f... ",EpicsValue);
	  wrote_msg=1; /* flag to indicate we wrote this message */
	}
      if(loop_cnt%4==0 && loop_cnt > 3 )printf(".");

      /* wait for 500ms (& make sure no IO is pending) */
      if(d6)printf("EpicsSet: Waiting  0.5s for value to settle...\n");
      for (i = 0; i < 50; i++)
	  status=ca_pend_event(0.01); /* wait 50 * 0.01 = 0.5 s  */
    
      if (status != ECA_TIMEOUT && status != ECA_NORMAL)
	SEVCHK(status, ca_message(status)); 
     
      /* 
	 read Epics device value  
      */
      status=caRead(epics_constants.Rchan_id,&EpicsVal_read);
      if(status==-1)
	{
	  printf("EpicsSet: Bad status after caRead\n");
	  return(status);
	}
      
      my_offset = EpicsVal_read-EpicsValue;  /* calculate offset for this measurement */
      /* printf("my_offset=%.4f, fabs(my_offset)=%.4f, typical_offset=%.4f, fabs(typical_offset)=%.4f\n",
	 my_offset,fabs(my_offset),typical_offset,fabs(typical_offset));*/
      
      if (fabs(my_offset) < fabs(typical_offset))
	{
	if(d6)printf("EpicsSet: Set %.4f Read  %.4f fabs(offset)  %.4f <  %.4f loop %d\n",
	       EpicsValue, EpicsVal_read,fabs(my_offset),fabs(typical_offset),loop_cnt);
	break;
	}
      else 
	if(d6)printf("EpicsSet: Set %.4f Read  %.4f fabs(offset)  %.4f >  %.4f loop %d\n",
	       EpicsValue, EpicsVal_read, fabs(my_offset),fabs(typical_offset),loop_cnt);
      
      loop_cnt ++;

    }
  *pOffset = my_offset;  /* return value */
  *pRvalue = EpicsVal_read;  /* return value */

  if (loop_cnt >= nloops)
    {
      printf("\nEpicsSet: Failure at set value  %.4f, Read %.4f (offset %.4f > %.4f)\n",
	     EpicsValue,EpicsVal_read,fabs(my_offset),fabs(typical_offset));
      return(-4);
    }
  ltemp=loop_cnt; /* remember for message later */
  

  /* check stability */
  
  loop_cnt= 0 ;
  while (loop_cnt < nloops)
    {
      EpicsVal_prev = EpicsVal_read;
      
      /* wait for 0.5s  & make sure no IO is pending */
      if(d6)printf("EpicsSet: Waiting  1 sec for stablility check...\n");
      for(i = 0; i < 100; i++)
	status=ca_pend_event(0.01); /* wait 100 * 0.01 = 1sec  */
      if (status != ECA_TIMEOUT && status != ECA_NORMAL)
	SEVCHK(status, ca_message(status)); 
      

      /* read Epics device value  */
      status=caRead(epics_constants.Rchan_id,&EpicsVal_read);
      if(status==-1)
	{
	  printf("EpicsSet:Bad status after caRead\n");
	  return(status);
	}
      if ( fabs (EpicsVal_read - EpicsVal_prev) < fabs(epics_constants.stability) )
	  break;
      else
	if(d6)printf("Set %.4f Read  %.4f diff > stability  %.4f loop %d\n",
		     EpicsValue, EpicsVal_read, epics_constants.stability,loop_cnt);

      loop_cnt ++;

    }

  *pRvalue = EpicsVal_read;  /* return value */

  if (loop_cnt >= nloops)
    {
      printf("EpicsSet: Read values are not stable to within %.4f \n",epics_constants.stability);
      return(-3);
    }
  if(d6)printf("EpicsSet: successfully set  Epics device value to %.4f (loops=%d,%d)\n",EpicsValue,ltemp,loop_cnt);
  if(wrote_msg)printf("...success\n"); /* tell user after previous message */
  return(0); /* success */
}




/*  
    EpicsSet_step
*/ 
int EpicsSet_step ( float val,         /* set value */
		    float offset,      /* checks offset between read/set values (offset depends on value set)
					  is less than this (absolute) value */
		    float device_typical_offset, /* typical offset at this set point */
		    float step,        /* step size */
		    float diff,        /* typically 0.05 to 0.5V depending on step size
					  used for checking if value has actually changed  */
		    float *pRvalue,    /*  output: value read back */
		    float *pOffset,    /*  output: set/read offset value */
		    EPICS_CONSTANTS epics_constants)
{
  
  /* assumes chan_id is set up by a call to caGetId
     
  sets value & checks it has changed 
  
  returns   0  SUCCESS : value changed & is within limits
  
  -1 failure returned  from setting/reading Epics device or getting a connection 
  -2 invalid input parameters
  
  -3  read increment and write increment do not agree
  -4  offset between read and set values is outside allowed  range
  -5  the voltage has not changed after completion of nloops_step 
  
  Assumes:
  Maximum offset between read/set values is epics_constants.max_offset
  
  
  Assumes callback on set values is actually working!
  
  */
  
  
  /* Read value */
  float EpicsVal_read , EpicsValue, EpicsVal_prev, EpicsVolt_inc ;
  int status,code;
  int loop_cnt,i;
  float read_inc=900; /* initialize to a silly value */
  float b, check_diff, check_offset, my_offset=0;
  int wrote_msg;
  
  wrote_msg=0;
  if( epics_constants.debug) 
    d6=1; /* global debug */
  else
    d6=0;
  if(d6)
    {
      printf(" EpicsSet_step starting with constants :  Rchan_id = %d, Wchan_id = %d, \n",
	     epics_constants.Rchan_id, epics_constants.Wchan_id);
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f, debug=%d\n",
	     epics_constants.max_offset, epics_constants.min_set_value, epics_constants.max_set_value,  
	     epics_constants.stability, epics_constants.debug);
      printf(" and val=%.4f, offset=%.4f, step=%.4f, diff=%.4f\n",val, offset,step,diff);
    }
  
  
  if(val < epics_constants.min_set_value || val > epics_constants.max_set_value)
    {
      printf("EpicsSet_step: Invalid set value (%.4f) for Epics device\n",val);
      return (-2);
    }
  
  
  /* get some values from parameter list */
  EpicsValue = val;
  EpicsVolt_inc = step;
  check_diff = fabs(diff);
  
  check_offset = fabs( offset) ;
  if(check_offset > fabs(device_typical_offset)) 
    check_offset =  fabs(device_typical_offset);
  
  if(d6)
    printf("EpicsSet_step: check_diff: %.4f, check_offset: %.4f (absolute values)\n", check_diff, check_offset); 
  
  
  if(epics_constants.Rchan_id < 0 || epics_constants.Wchan_id < 0 )
    return -1;
  else  if (ca_state((chid)epics_constants.Rchan_id) != cs_conn)
    return -1;
  else  if (ca_state((chid)epics_constants.Wchan_id) != cs_conn)
    return -1;
  
  
  status=caRead(epics_constants.Rchan_id,&EpicsVal_prev);
  if(status==-1)
    {
      printf("EpicsSet_step: Bad status after caRead\n");
      return(status);
    }
  else
    if(d6)printf("EpicsSet_step: Read present value as %.4f\n",EpicsVal_prev);
  
  
  /* write value */
  status=caWrite(epics_constants.Wchan_id,&EpicsValue);
  if(status==-1)
    {
      printf("EpicsSet_step: Bad status after caWrite for set point %.4f\n",EpicsValue);
      return (status);
    }  
  else  
    if(d6)printf("EpicsSet_step: set value to %.4f\n",EpicsValue);
  
  /*
    
  MAIN LOOP
  
  */
  
  code =-4; /* set code for failure */
  loop_cnt = 0;
  while (loop_cnt < nloops_step)  /* 6 at present */
    {
      /* print a message if we have a long wait */
      if(loop_cnt == 2 )
	{
	  printf("\nEpicsSet_step: Waiting for Epics device to respond at setpoint= %.4f",EpicsValue);
	  wrote_msg=1;
	  check_offset = device_typical_offset; /* widen the offset */
	}      
      else if(loop_cnt%2==0 && loop_cnt > 3) 
	printf(".");
      if(loop_cnt == (nloops_step -2) )
	 check_offset= epics_constants.max_offset; /* widen again so we at least check the read/write increments */
      
      /* wait for 0.5s & make sure no IO is pending */
      if(d6)printf("EpicsSet_step: Waiting for 0.5s for value to settle...\n");
      for(i=0; i<50; i++)
	status=ca_pend_event(0.01); /* wait 50 * .01 = .5s  */
      if (status != ECA_TIMEOUT && status != ECA_NORMAL)
	SEVCHK(status, ca_message(status)); 
      
      /* read Epics device value again  */
      status=caRead(epics_constants.Rchan_id,&EpicsVal_read);
      if(status==-1)
	{
	  printf("EpicsSet_step: Bad status after caRead\n");
	  return(status);
	}
      
      my_offset = EpicsVal_read-EpicsValue;  /* calculate offset for this measurement */
      read_inc = EpicsVal_read -  EpicsVal_prev ;  /* calculate read_inc */
      
      /* make sure offset is reasonable */
      if (fabs(my_offset) < fabs(check_offset) )
	{
	  code=-3;
	  if (d6) 
	    printf("EpicsSet_step: Read EpicsVolt %.4f; prev %.4f; set %.4f; offset %.4f, read_inc=%.4f, set_inc=%.4f, loops=[%d]\n",
		   EpicsVal_read, EpicsVal_prev, EpicsValue, my_offset, read_inc, EpicsVolt_inc, loop_cnt);
	  
	  /* check read/write increments are within limits */
	  b= fabs(read_inc) - fabs(EpicsVolt_inc);
	  b= fabs(b);  
	  if( b < check_diff)
	    {
	      if(d6)
		printf("EpicsSet_step: Setpoint/readback ACCEPTED (diff. %.4f < %.4fV) after %d loops \n",b,check_diff,loop_cnt);
	      break;
	    }
	  else  
	    if(d6)
	      {
		printf("\nEpicsSet_step: Setpoint/readback NOT ACCEPTED at setpoint %.4f (diff. %.4f > %.4fV) after %d loops \n",
		       EpicsValue,b,check_diff,loop_cnt);
		printf("   Read EpicsVolt %.4f; prev %.4f; offset %.4f, read_inc=%.4f, set_inc=%.4f, loops=[%d]\n",
		       EpicsVal_read, EpicsVal_prev, my_offset, read_inc, EpicsVolt_inc, loop_cnt);
	      }
	} /* offset out of range; value may not have changed yet */
      else
	{
	  if (d6) 
	    printf("EpicsSet_step: Read EpicsVolt %.4f; prev %.4f; set %.4f, read/write offset=%.4f > %.4f, loops=[%d]\n",
			 EpicsVal_read, EpicsVal_prev, EpicsValue, fabs(my_offset), fabs(check_offset), loop_cnt);
	}      
      loop_cnt ++;
      
    }
  *pOffset = fabs(my_offset); /* return the latest offset value */
  *pRvalue = EpicsVal_read; /* and the read value */
  
  if(loop_cnt < nloops_step)	  
    {          /*   S U C C E S S  */
      /* Epics value has been set successfully */
      if(loop_cnt>0)
	{
	  if(d6)
	    printf("Success: Read EpicsVolt %.4f; prev %.4f; set %.4f, read/write offset=%.4f < %.4f, loops=[%d]\n",
		   EpicsVal_read, EpicsVal_prev, EpicsValue, fabs(my_offset), fabs(check_offset), loop_cnt);
	  else
	    {
	      if(wrote_msg)
		printf("..Success at setpoint %.4f (read %.4f) offset=%.4f\n",
		       EpicsValue,EpicsVal_read,fabs(my_offset)); /* tell user after previous message */
	    }
	}
      
      return(0); /* success */
    }
  else
    { 
      printf("\nEpicsSet_step: Loop count exhausted; read EpicsVolt %.4f; prev %.4f; set %.4f; offset %.4f, check_offset = %.4f, read_inc=%.4f, set_inc=%.4f, diff=%.4f,loops=[%d]\n",
	     EpicsVal_read, EpicsVal_prev, EpicsValue, my_offset, check_offset, read_inc, EpicsVolt_inc, diff,loop_cnt); 
      if(read_inc <  epics_constants.stability) 
	return (-5); /* value did not change */
      return(code);
    }
}




int EpicsSet2 ( float val,         /* set value */
		float last_offset, /* offset value to check against  */
		float device_typical_offset, /* typical offset at this voltage */
	   float *pRvalue,    /*  output: value read back */
	   float *pOffset,    /*  output: set/read offset value */
	  EPICS_CONSTANTS epics_constants)  
{

  /* assumes chan_id is set up by a call to caGetId

     sets value & checks it is within last_offset (or device_typical_offset of set value 

     returns   0  SUCCESS : value changed & is within limits

               -1 failure returned  from accessing  Epics 
	       -2 invalid input parameters
      
	       -4  offset between read and set values is outside input parameter (offset)

    Assumes:
    Maximum offset between read/set values is epics_constants.max_offset 
    typical offset for this device at this set_point is device_typical_offset
    Epics_Constants.Stability of read values : 0.05V
    Assumes callback on set values is actually working!

    Tries for an offset <  last_offset 
    After half the loops, tries for offset < device_typical_offset
    Finally it accepts epics_constants.max_offset

  */


     /* Read value */
  float EpicsVal_read , EpicsValue, EpicsVal_prev ;
  int status;
  int  i,loop_cnt,ltemp;
  float my_offset=0.0;
  float typical_offset;
  int wrote_msg;

  wrote_msg=0;
  if( epics_constants.debug) 
    d6=1; /* global debug */
  else
    d6=0;
  
  if(d6)
    {
      printf(" EpicsSet2 starting with constants :  Rchan_id = %d, Wchan_id = %d, \n",     
	     epics_constants.Rchan_id, epics_constants.Wchan_id);
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f, debug=%d\n",
	      epics_constants.max_offset, epics_constants.min_set_value, epics_constants.max_set_value,  
	     epics_constants.stability, epics_constants.debug);
      printf("  and val = %.4f last_offset = %.4f\n",val,last_offset);
    }
  
  typical_offset = last_offset;
  /* get some values from parameter list */
  EpicsValue = val;
  
 
  if(EpicsValue < epics_constants.min_set_value || EpicsValue > epics_constants.max_set_value )
    {
      printf("EpicsSet2: Invalid set value (%.4f) for Epics device\n",val);
      return (-2);
    }


  if(epics_constants.Rchan_id < 0 || epics_constants.Wchan_id < 0 )
    return -1;
  
  /* 
     
     write the value 
     
  */
  status=caWrite(epics_constants.Wchan_id,&EpicsValue);
  if(status==-1)
    {
      printf("EpicsSet2: Bad status after caWrite for set point %.4f\n",EpicsValue);
      return (status);
    }  
  else      /* if(d6) */
      printf("EpicsSet2: called caWrite to set value to %8.3f\n",EpicsValue);
  
  
  
  /*
    
    Is the value now close to the setpoint ?
    
  */
  
  loop_cnt = 0;
  while (loop_cnt < nloops*2) /* nloops is 10 */
    { 
      if(loop_cnt == nloops )
	  typical_offset = device_typical_offset;/* widen the offset range after half the loops) */
      if(loop_cnt == nloops-5)
	typical_offset =  epics_constants.max_offset; /* try the maximum offset */
      
      /* print a message if we have a long wait */
      if(loop_cnt == 4)
	{
	  wrote_msg=1;
	  printf("EpicsSet2: Trying to set Epics device to %.4f",EpicsValue);
	}
      if(loop_cnt%4==0 && loop_cnt > 3 )printf(".");

      /* wait for 500ms (& make sure no IO is pending) */
      if(d6)printf("EpicsSet2: Waiting  0.5s for value to settle...\n");
      for (i = 0; i < 50; i++)
	  status=ca_pend_event(0.01); /* wait 50 * 0.01 = 0.5 s  */
    
      if (status != ECA_TIMEOUT && status != ECA_NORMAL)
	SEVCHK(status, ca_message(status)); 
     
      /* 
	 read Epics device value  
      */
      status=caRead(epics_constants.Rchan_id,&EpicsVal_read);
      if(status==-1)
	{
	  printf("EpicsSet2: Bad status after caRead\n");
	  return(status);
	}
      
      my_offset = EpicsVal_read-EpicsValue;  /* calculate offset for this measurement */
      /* printf("my_offset=%.4f, fabs(my_offset)=%.4f, typical_offset=%.4f, fabs(typical_offset)=%.4f\n",
	 my_offset,fabs(my_offset),typical_offset,fabs(typical_offset));*/
      
      if (fabs(my_offset) < fabs(typical_offset))
	{
	if(d6)printf("EpicsSet2: Set %.4f Read  %.4f fabs(offset)  %.4f <  %.4f loop %d\n",
	       EpicsValue, EpicsVal_read,fabs(my_offset),fabs(typical_offset),loop_cnt);
	break;
	}
      else 
	if(d6)printf("EpicsSet2: Set %.4f Read  %.4f fabs(offset)  %.4f >  %.4f loop %d\n",
	       EpicsValue, EpicsVal_read, fabs(my_offset),fabs(typical_offset),loop_cnt);
      
      loop_cnt ++;

    }
  *pOffset = my_offset;  /* return value */
  *pRvalue = EpicsVal_read;  /* return value */

  if (loop_cnt >= nloops)
    {
      printf("\nEpicsSet2: Failure at set value  %.4f, Read %.4f (offset %.4f > %.4f)\n",
	     EpicsValue,EpicsVal_read,my_offset,typical_offset);
      return(-4);
    }
  ltemp=loop_cnt; /* remember for message later */
  

  /* check stability */
  
  loop_cnt= 0 ;
  while (loop_cnt < nloops)
    {
      EpicsVal_prev = EpicsVal_read;
      
      /* wait for 0.5s  & make sure no IO is pending */
      if(d6)printf("EpicsSet2: Waiting  1 sec for stablility check...\n");
      for(i = 0; i < 100; i++)
	status=ca_pend_event(0.01); /* wait 100 * 0.01 = 1sec  */
      if (status != ECA_TIMEOUT && status != ECA_NORMAL)
	SEVCHK(status, ca_message(status)); 
      

      /* read Epics device value  */
      status=caRead(epics_constants.Rchan_id,&EpicsVal_read);
      if(status==-1)
	{
	  printf("EpicsSet2:Bad status after caRead\n");
	  return(status);
	}
      if ( fabs (EpicsVal_read - EpicsVal_prev) < fabs(epics_constants.stability) )
	  break;
      else
	if(d6)printf("Set %.4f Read  %.4f diff > stability  %.4f loop %d\n",
		     EpicsValue, EpicsVal_read, epics_constants.stability,loop_cnt);

      loop_cnt ++;

    }

  *pRvalue = EpicsVal_read;  /* return value */

  if (loop_cnt >= nloops)
    {
      printf("EpicsSet2: Read values are not stable to within %.4f \n",epics_constants.stability);
      return(-3);
    }
  if(d6)
    printf("EpicsSet2: successfully set  Epics device value to %.4f (loops=%d,%d)\n",EpicsValue,ltemp,loop_cnt);
  else
    {
      if(wrote_msg)
	printf("..Success at setpoint %.4f\n",EpicsValue);
    }
  return(0); /* success */
}



















