/* scan_jump.c

Implement POL's desire to have a jump in the Type 1 scan

CVS log information:
$Log: scan_jump.c,v $
Revision 1.2  2004/11/24 20:29:56  suz
fix a bug

Revision 1.1  2004/11/17 00:50:41  suz
original: implement for pol


*/
INT find_jump(float start, float stop, float inc, DWORD ninc, 
	      float jstart, float jstop, INT debug, DWORD *pjinc1, DWORD *pjinc2)
{
  float temp;
  DWORD jinc1,jinc2;
  BOOL gotstart,gotstop,i;
  INT size,status;
  float value1, value2;
  char str_set[128];
  HNDLE hOP;

  *pjinc1=*pjinc2=0; /* initialize */
  value1=value2=0;

  if ( fabs(jstop - jstart) < fabs(inc))
    {
      cm_msg(MERROR,"find_jump","jump stop and jump start must be at least 1 increment apart");
      return -1;
    }
  
  if(debug)printf("find_jump starting with start=%.2f stop=%.2f jstart=%.2f jstop=%.2f inc=%.2f\n",
	 start,stop,jstart,jstop,inc);
  
  
  /* make sure jump value are consistent with start/stop */
  if(start < stop)
    {
      if(debug)printf("find_jump: start is less than stop\n");
      if (jstart > jstop)
	{  
	  temp=jstart;
	  jstart = jstop;
	  jstop = temp;
	  if(debug)printf("find_jump: swapped so jstart=%d jstop=%d\n",jstart,jstop);
	} 
      if (jstart < start || jstop > stop)
	{
	  cm_msg(MERROR,"find_jump","illegal jump parameter(s) (%.2f,%.2f)  ( outside start/stop (%.2f,%.2f) )",
		 jstart,jstop,start,stop);
	  return -1;
	}
          

    }
  else
    {
      if(debug)printf("find_jump: start is greater than stop\n");
      
      if (jstart < jstop)
	{  
	  temp=jstart;
	  jstart = jstop;
	  jstop = temp;
	  if(debug)printf("find_jump: swapped so jstart=%.2f jstop=%.2f\n",jstart,jstop);
	}
      if (jstart > start || jstop < stop)
	{
	  cm_msg(MERROR,"find_jump","illegal jump parameter(s) (%.2f,%.2f)  ( outside start/stop (%.2f,%.2f) )",
		 jstart,jstop,start,stop);
	  return -1;
	}
    }

 if ( fabs(start - jstart) < fabs(inc))
    {
      cm_msg(MERROR,"find_jump","start and jump start must be at least 1 inc apart");
      return -1;
    }

 if ( fabs(stop - jstop) < fabs(inc))
    {
      cm_msg(MERROR,"find_jump","stop and jump stop must be at least 1 inc apart");
      return -1;
    }

  gotstart=gotstop=0;

  if(debug)printf("start=%f, jstart=%f, jstop=%f, ninc=%d, inc=%d\n",
		  start,jstart,jstop,ninc,inc);
  /* find the jump in terms of increments */
  for (i=0; i<ninc; i++)
    {
      temp = start + i * inc;
      if(debug)printf("find_jump:  i=%d, calculated sweep value = %f \n",i,temp);
     
      
      if(inc > 0)  /* +ve increment */
	{
	  if( (temp >= jstart) && !gotstart)
	    {
	      jinc1 = i-1;
	      gotstart = 1;
	      value1 = start + (jinc1*inc);
	      if(debug) /* sweep value is within jump */
		printf("find_jump:  ..  setting last good incr (jinc1) to %d ->sweep value=%f\n",jinc1,value1);
	    }
	  if( (temp > jstop) && !gotstop)
	    {
	      jinc2 = i;
	      value2 = temp;
	      if(debug) /* sweep value is outside jump */
		printf("find_jump: .. setting next good incr (jinc2) to %d ->sweep value=%f\n",jinc2,value2);
	      gotstop = 1;
	    }
	}
      else  /* negative increment */
	{
	  if( (temp <= jstart) && !gotstart)
	    
	    {
	      jinc1 = i-1;
	      gotstart = 1;
	      value1 =  start + (jinc1*inc);
	      if(debug)/*  sweep value is within jump */
		printf("find_jump: ..setting last good incr (jinc1) to %d ->sweep value =%f\n",
		       jinc1,value1);
	    }
	  if( (temp < jstop) && !gotstop)
	    {
	      jinc2 = i;
	      value2 = temp;
	      if(debug) /*  sweep value is outside jump */
		printf("find_jump:.., setting next good increment (jinc2) to %d ->sweep value=%f\n",
		       jinc2,value2);
	      gotstop = 1;
	    }
	}
    }

  if(gotstop & gotstart)
    {
      if(jinc1 < 0 || jinc2 < 0)
	{
	  cm_msg(MERROR,"find_jump",
		 "error in calculation... jump increments (%d and %d) cannot be negative",jinc1,jinc2);
	  return -1;
	}
      
      if(jinc2 <= jinc1)
	{
	  cm_msg(MERROR,"find_jump","Error in calculation... jinc2(%d) <= jinc1(%d); expect jinc2>jinc1",
		 jinc1,jinc2);
	  return -1;
	}
            
      *pjinc1=jinc1;
      *pjinc2=jinc2;
    }
  
  /* write these to the output area */
  fs.output.scan_jump_inc1 = jinc1;
  fs.output.scan_jump_inc2 = jinc2;
  fs.output.scan_jump_value1=value1;
  fs.output.scan_jump_value2=value2;
  
  printf("find_jump: jump will be between increments %d and %d\n",jinc1,jinc2);
  printf("           i.e. between values %f and %f \n",value1,value2);
 
  sprintf(str_set,"/Equipment/%s/sis mcs/output",equipment[FIFO].name); 
  
  /* find the key for output */
  status = db_find_key(hDB, 0, str_set, &hOP);
  if (status != DB_SUCCESS)
    {
      printf( "find_jump: Cannot find key %s (%d) ",str_set,status);
    }
  else
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hOP, 0, &size);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "find_jump", "error during get_record_size (%d)",status);
      else
	{
	  if (sizeof(fs.output) != size) 
	    {
	      printf("size of saved structure:%d size of output record: %d\n", sizeof(fs.output),size);
	    }
	  else
	    {
	      status = db_set_record(hDB,hOP, &fs.output,size,0); 
	      if (status != DB_SUCCESS)
		cm_msg(MERROR, "find_jump", "could not set record for output (%d)",status);
	    }
	}
    }
  
  
  if(gotstop & gotstart)  
    return SUCCESS;
  else
    {
      cm_msg(MERROR,"find_jump","could not assign jump stop/start increments");
      return -1; /* failure */
    }
  
}


INT check_jump(INT incr_cntr, BOOL debug)
{
  /* uses globals gbl_fix_inc_cntr gbl_jinc1, gbl_jinc2 gbl_jump_now 

  Check if it's time to jump; 
  if so, returns the new scan counter, & sets gbl_jump_now true;
  otherwise incr_cntr is unchanged, gbl_jump_now is false.
  
  */

  INT i,icntr,kcntr;


  if(debug)
    printf("check_jump: starting with incr_cntr=%d, jump at increments %d and %d, gbl_fix_inc_cntr=%d \n",
	   incr_cntr,gbl_jinc1,gbl_jinc2,gbl_fix_inc_cntr);


  gbl_jump_now = FALSE;
  i=incr_cntr;

  icntr = i - gbl_fix_inc_cntr;   
  
  if( icntr > 0) /* positive scan */
    {
      if (icntr == gbl_jinc1 ) /* jinc1 = last good sweep value */
	{
	  i = gbl_jinc2-1; /* will be increased by loop counter */	  
	  if(debug)printf("\n check_jump: (+ve scan)jumping.... to increment %d\n",i);
	  gbl_jump_now = TRUE;
	}
    }
  else if (icntr < 0)  /* negative scan, gbl_fix_inc_cntr is negative */
    {
      if ( icntr == -gbl_jinc2)
	{
	  if(debug)printf("got icntr = -jinc2, icntr=%d, i=%d\n",icntr,i);
	  i=-gbl_jinc1+gbl_fix_inc_cntr -1; /* will be increased by loop counter */
	  if(debug)
	    {
	      printf("setting i = %d\n",i);
	      printf("\ncheck_jump: (-ve scan)jumping... to i=%d  increment %d\n",i, abs( (i)-gbl_fix_inc_cntr));
	    }
	  gbl_jump_now = TRUE;	  
	}
    }
  return i;
}  	    





