/* header file for random number generator etc
Included by febnmr.c

CVS log information:
$Log: trandom.h,v $
Revision 1.2  2005/05/17 23:38:36  suz
add support for randomized 2a

Revision 1.1  2005/01/24 17:58:32  suz
original: header file for random no. generator needed by 1a/1b with randomized frequencies


*/

# include "stdio.h"
#include "math.h"
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876

#define MAX_FREQ 25

INT  *pfreq=NULL;  /* pointer to initial frequency array (ordered)*/
INT *prandom_freq=NULL; /* pointer to randomized frequency array */
INT *pindex=NULL; /* pointer to index array */
INT *pseqf=NULL;
DWORD seed;
INT dr=0;

float ran0(long *idum);
INT get_random_index(INT nval);
INT read_freq_file(char *ppg_mode, INT num_val);
INT randomize_freq_values(char *ppg_mode, INT num_val);
void free_freq_pntrs(void);
