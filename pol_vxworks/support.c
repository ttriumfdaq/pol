/* Various support routines for E614 Slow controls
 *
 * P. W. Green - April 2001
 */

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

#include "midas.h"
#include "msystem.h"
#include <ybos.h>

#include "queue.h"
#include "heartbeat.h"
#include "e614.err"
#include "e614proto.h"

extern char *frontend_name;
extern int slowdebug;
extern HNDLE hDB;
DWORD gbl_status;
INT gbl_run_number;
/* Not actually used, but must be defined
 */
INT max_event_size_frag = 5*1024*1024;


/* For HeartBeat functions
 */
HNDLE hHeartBeat;

/* General Initialization for SC FrontEnd
 */
INT init_scfe (void)
{
  INT cm_disconnect_experiment ();
  char buff [256];
  INT status;

  /* Need Handle to the Status for HeartBeat function
   */
  sprintf (buff, "/Status/SlowControl/%s", frontend_name);
  status = db_find_key (hDB, 0, buff, &hHeartBeat);
  if (status != SUCCESS) {
    cm_msg (MERROR, "init_scfe", "can't find %s", buff);
    return (status);
  }
  /* When I die I should disconnect cleantly
   */
  atexit ((void *)cm_disconnect_experiment);

  /* For some slow control clients, the drfault 10 second timeout is too short
   */
  cm_set_watchdog_params (TRUE, 30000);

  /* Set initial status to OK (until I discover differently)
   */
  gbl_status = ST_OK;
  return (SUCCESS);
}

/* General Init for all SC Events
 */
INT init_scevent (char *pevent)
{
  DWORD *pdw;

  /* create EVID bank
   * on INTEL PC, the trigger mask comes first, event_type second
   * on PPC, the event_type comes first, the trigger mask second 
   */

  ybk_create((DWORD *) pevent, "EVID", I4_BKTYPE, (DWORD *)(&pdw));
  
  *((WORD *)pdw) = 0; /*trigger mask*/   ((WORD *)pdw)++;
  *((WORD *)pdw) = EVENT_ID(pevent);   ((WORD *)pdw)++;
  *(pdw)++ = SERIAL_NUMBER(pevent);    /* Serial number */
  *(pdw)++ = TIME_STAMP(pevent);       /* Time Stamp */
  *(pdw)++ = gbl_run_number;                         /* run number */
  *(pdw)++ = *((DWORD *)frontend_name);              /* frontend name */
  ybk_close((DWORD *) pevent, pdw);

  return SUCCESS;
}

/* Set Event ID from Queue entry
 */

void set_eventid (char *pevent, struct quentry *pe)
{
  /* The value 0 means leave it alone
   */

  if (pe->eventid != 0)
    EVENT_ID (pevent) = pe->eventid;
  if (pe->triggermask != 0)
    TRIGGER_MASK(pevent) = pe->triggermask;
}

/* always use the fast queue element allocator */
#define FAKE_QMALLOC 1

/* malloc for which I don't have to check success
 * Note: calloc clears the memory to zero
 */
#ifdef FAKE_QMALLOC

#define QUEUESIZE 10000
static struct quentry qlist [QUEUESIZE];
static char qlstatus [QUEUESIZE];

int qecheck (int *s)
{
  int size = sizeof (struct quentry) / sizeof (int);
  int i;

  for (i=0; i<size; i++) {
    if (s[i] != 0) {
      return (1);
    }
  }
  return (0);
}

void qmcheck (void)
{
  int i, j;
  int *s;
  int size = sizeof (struct quentry) / sizeof (int);

  
  for (i=0;i<QUEUESIZE; i+=2) {
    s = (int *) &qlist [i];
    if (qecheck (s)) {
      DB7PRINT ("Overwrite error in malloc entry %d\n", i);
      for (j=0; j<size; j++)
	DB7PRINT ("%d %d\n", j, s[j]);
    }
  }
}

#endif

struct quentry *qmalloc (void)
{
  struct quentry *pe = NULL;

#ifdef FAKE_QMALLOC
  int i;

  for (i=1;i<QUEUESIZE; i+=2) {
    if (qlstatus [i] == 0) {
      qlstatus [i] = 1;
      pe = &qlist [i];
      DB7PRINT ("Allocate block %d\n", i);
      break;
    }
  }
  if (pe == NULL) {
    printf ("No more queue blocks left\n");
    exit (1);
  }
#else
  pe = (struct quentry *) mymalloc (sizeof (struct quentry));
#endif
  return (pe);
}

void qfree (struct quentry *pe)
{
#ifdef FAKE_QMALLOC

  int i;

  qmcheck ();
  for (i=1;i<QUEUESIZE; i+=2) {
    if (pe == &qlist [i]) {
      qlstatus [i] = 0;
      memset (&qlist[i], sizeof (struct quentry), 0);
      DB7PRINT ("Free block %d\n", i);
      break;
    }
  }
#else

  myfree (pe);

#endif
}

char *mymalloc (int nbytes)
{
  char *s;

  s = calloc (nbytes, 1);
  if (s == NULL) {
    cm_msg (MERROR, frontend_name, "malloc failure - fatal");
    exit (1);
  }
  return (s);
}

void *myfree (void *s)
{
  
  if (s) free (s);
  return (NULL);
}

/* Set global status, and send it to the odb
 * (if it is different than what is there already)
 */
#define ERRORFILE "/home/twistonl/slow/include/e614.err"

struct StatusTextStr
{
  DWORD status;
  const char* text;
  struct StatusTextStr *next;
};

const char* getStatusText(DWORD status)
{
  static struct StatusTextStr * gList = NULL;
  struct StatusTextStr *lptr;

  if (gList == NULL)
    {
      FILE *fp;
      /* printf("Reading the error list %s\n",ERRORFILE); */
      fp = fopen(ERRORFILE,"r");
      while (fp != NULL)
	{
	  char  buf[256], *s, *p;
	  char  ename[256];
	  char  emsg[256];
	  long  errorcode;
	  int rd;

	  s = fgets (buf, sizeof(buf)-1, fp);
	  if (s == NULL)
	    {
	      fclose(fp);
	      fp = NULL;
	      break;
	    }

	  buf[sizeof(buf)-1] = 0;

	  rd = sscanf (buf, "#define %s %ld /* %s",ename, &errorcode, emsg);
	  if (rd != 3) continue;

	  s = strstr(buf,"/*");
	  if (s == NULL) continue;

	  s += 2;
	  while (*s == ' ')
	    s++;

	  if (*s == '"')
	    s++;

	  p = strstr(s,"*/");
	  if (p == NULL) continue;
	  *p = 0;
	  while (p[-1] == ' ')
	    p--;
	  if (p[-1] == '"')
	    p--;
	  *p = 0;

	  lptr = calloc(sizeof(*lptr),1);
	  lptr->next = gList;
	  gList = lptr;

	  lptr->status = errorcode;
	  lptr->text   = strdup(s);
	}
    }

  lptr = gList;
  while (lptr != NULL)
    {
      if (lptr->status == status)
	return lptr->text;
      lptr = lptr->next;
    }

  {
    static char tmpbuf[256];
    sprintf(tmpbuf,"Unknown status %ld",(long)status);
    return tmpbuf;
  }
}

void set_global_status (DWORD currentstatus)
{
  FILE *fp;
  char buff [256];
  char ename[100];
  char emsg[100];
  int  errorcode;
  char *s;
  int i;

  if (currentstatus != gbl_status) {
    gbl_status = currentstatus;
    /* Message to log file / Operator?
     * Talk if LSB in error is set
     */
    if (currentstatus & 3) {
      fp = fopen (ERRORFILE, "r");
      while (1) {
	s = fgets (buff, 256, fp);
	if (s == NULL) break;
	i = sscanf (buff, "#define %s %d /* %s",ename, &errorcode, emsg);
	if (i != 3) continue;
	if (errorcode == currentstatus) break;
      }
      if (s) {
	if (currentstatus & 1)
	  cm_msg (MTALK,frontend_name, emsg);
	if (currentstatus >= 4000000)
	  cm_msg (MERROR,frontend_name, emsg);
	else
	  cm_msg (MINFO,frontend_name, emsg);
      }
    }
    send_global_status ();
  }
}

/* Send global status to odb (regardless of what it already is)
 * This is used also as a "HeartBeat" function - allstatus expects
 * something from every piece of software every so often so that
 * it knows that is still lives
 */

INT send_global_status ()
{
  int status;

  status = db_set_data (hDB, hHeartBeat, &gbl_status, sizeof (DWORD),
			1, TID_DWORD);
  return (status);
}

/* This function calculates the total size of all entries in a sub-tree.
 */

int trace_tree (HNDLE hKey)
{
  /* Trace a sub-tree (and all of its subtrees) and calculate the total size
   * of all of the entries in it.
   */

  int size;
  int i;
  int totalsize;
  KEY keyinfo;
  HNDLE hTempKey;
  int status;

  totalsize = 0;
  for (i=0; ; ++i) {
    status = db_enum_key (hDB, hKey, i, &hTempKey);
    if (status == DB_NO_MORE_SUBKEYS) break;
    if (status != DB_SUCCESS) {
      cm_msg (MERROR, "trace_tree", "db_enum_key %d", status);
      exit (1);
    }
    status = db_get_key (hDB, hTempKey, &keyinfo);
    if (status != DB_SUCCESS) {
      cm_msg (MERROR, "trace_tree", "db_get_key %d", status);
      exit (1);
    }
    if (keyinfo.type == TID_KEY) {
      size = trace_tree (hTempKey);
      totalsize += size;
    } else if (keyinfo.type == TID_DWORD) {
      totalsize += keyinfo.total_size;
    } else {
      cm_msg (MERROR, "trace_tree", "invalid data type %d", keyinfo.type);
      exit (1);
    }
  }
  return (totalsize);
}

void e614_send_event (EQUIPMENT *eq, EVENT_HEADER *pevent)
{

  /* Explicitly send an event (done normally be mfe.c, but we need our own
   * for those events which we generate at Begin-of-Run)
   */
  eq->bytes_sent += pevent->data_size + sizeof(EVENT_HEADER);
  eq->events_sent++;
  
  eq->stats.events_sent += eq->events_sent;
  eq->events_sent = 0;
  
  /* send event to buffer */
  if (eq->buffer_handle)
    {
#ifdef USE_EVENT_CHANNEL
      dm_pointer_increment(eq->buffer_handle, 
			   pevent->data_size + sizeof(EVENT_HEADER));
#else
      rpc_flush_event();
      bm_send_event(eq->buffer_handle, pevent,
		    pevent->data_size + sizeof(EVENT_HEADER), SYNC);
      bm_flush_cache(eq->buffer_handle, SYNC);
#endif
    }
  DB1PRINT ("e614 send event %d %d\n", pevent->event_id, pevent->trigger_mask);
}

int set_if_worse (int status, int newstatus)
{
  /* Return the worse of the two status values passed to me
   * (except if the current status is DISABLED or NOT_IMPLEMENTED)
   */
  if (status == ST_DISABLE || status == ST_NOTIMP || status > newstatus)
    return (status);
  else
    return (newstatus);
}

INT my_find_and_load (HNDLE hRoot, const char *name, HNDLE *hKey, void *data,
		   int numvalues, int type)
{
  /* function to locate the Key for data and load it from the odb
   * hKey may be null if the key is not required
   * Returns SUCCESS if successful
   */
  HNDLE hTemp;
  int size;
  int status;
  
  status = db_find_key (hDB, hRoot, name, &hTemp);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't find Key for %s", name);
    set_global_status (ST_BADSOFT);
    return (status);
  }
  if (type == TID_BYTE || type == TID_SBYTE || type == TID_CHAR || type == TID_STRING)
    size = numvalues;
  else if (type == TID_WORD || type == TID_SHORT)
    size = numvalues * sizeof (short);
  else if (type == TID_DWORD || type == TID_INT
	   || type == TID_BOOL  || type == TID_FLOAT)
    size = numvalues * sizeof (DWORD);
  else if (type == TID_DOUBLE)
    size = numvalues * sizeof (double);
  else {
    cm_msg (MERROR, frontend_name, "unrecognized type %d", type);
    set_global_status (ST_BADSOFT);
    return (FE_ERR_ODB);
  }
  status = db_get_data (hDB, hTemp, data, &size, type);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't get value for %s", name);
    set_global_status (ST_BADSOFT);
    return (status);
  }
  if (hKey != NULL)
    *hKey = hTemp;
  return (SUCCESS);
}

INT my_find (HNDLE hRoot, const char *name, HNDLE *hKey)
{
  /* Find key in the ODB
   */
  int status;

  status = db_find_key (hDB, hRoot, name, hKey);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't find Key for %s", name);
    set_global_status (ST_BADSOFT);
  }
  return (status);
}

/* Set debug level.
 * s is a string like
 * 'debug 1'
 * passed (usually) via the command hotlink.
 */

void set_debug (const char *s)
{
  int i;
  int status;

  /* Flush past the command wordc
   */
  while (*s) {
    if (isspace (*s)) break;
    ++s;
  }

  if (*s) {
    status = sscanf (s, "%d", &i);
    if (status == 1)
      slowdebug = i;
  } else {
    slowdebug = 0;
  }
  return;
}

FILE *fp = NULL;

INT debugprintf (const char *format, ...)
{
  va_list      argptr;
  time_t now;
  char buff[1024];
  char *s;
  int i;

  /* Include Time with all output
   */

  time (&now);
  strncpy (buff, ctime (&now), 50);
  
  i = strlen (buff) - 1;
  s = &buff [i];
  *s++ = ' ';
  *s = '\0';
  /* Now what he gave me
   */
  va_start(argptr, format);
  vsprintf(s, (char *) format, argptr);
  va_end(argptr);
  printf ("%s", buff);
  fflush (stdout);
  return SUCCESS;
}

int myatoi (const char *s)
{

  int value = 0;

  if (*s == '0') {
    if (tolower (* ++s) == 'x') {
      sscanf (++s, "%x", &value);
      return (value);
    }
  }
  return (atoi (s));
}
