/*  FieldTest.c

Tests  Field routines used in febnmr.c

FieldTest()   tests getting ID, reading/setting
FieldCycle()  tests EpicsSet and EpicsSet_step

Used with routines in conn.c (use callbacks)


CVS log information:
$Log: FieldTest.c,v $
Revision 1.2  2004/11/24 02:12:22  suz
typical_offset param added to EpicsSet and EpicsStep

Revision 1.1  2003/08/01 18:25:44  suz
original


*/
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "EpicsStep.h"

#ifndef LOCAL
#define LOCAL static
#endif

#define WRITE 1

void FieldTest(void);
void FieldCycle(void);
int test_cycle(int Rchan_id, int Wchan_id);

/* globals */
int dd=0;
int Rchan_id;
int Wchan_id;
char Rname[]="ILE2A1:HH:RDCUR";
char Wname[]="ILE2A1:HH:CUR";


/* Make values easy to change */

/* parameters used in FieldTest: */

/* for  Field, minimum step size is supposed to be 0.4 A */
const float maximum_offset = 0.7;
float typical_offset;
const float minimum_stability = 0.4;     /*  Field */
const float maximum_stability = 0.75;   /*   Field */
float typical_stability = 0.6; /* needed for EpicsSet */
float wvalue1 = 5.0;
float   wvalue2 = 20.0; /* set a second value */
float step1 = 1.0;

/* globals used in test_cycle */
const float minimum_inc = 0.5; /* .1A */
const float minimum_value=0; /* these are currents */
const float maximum_value=60;

/* parameters for FieldCycle */
float FieldVal_inc=10.0;
float FieldVal_start=0;
float FieldVal_stop=50.0;
int nscans=2;

/* Note : FieldVal_diff is calculated from hard-coded parameters */

/* constants for EpicsStep.c (structure defined in EpicsStep.h)  */
EPICS_CONSTANTS epics_constants = { -1, -1, 0, 0, 0, 0 };


/* FieldTest */
void FieldTest(void)
{
  int status;
  float value;
  float offset, check_offset, stability;
  
  typical_offset = 0.7 * maximum_offset; /* 70% of maximum; constant offset */


  status=caGetId (Rname, Wname, &Rchan_id, &Wchan_id);
  if(status==-1)
    {
      printf("FieldTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("\ncaGetId returns Rchan_id = %d and Wchan_id = %d\n\n",Rchan_id, Wchan_id);
  
  if (caCheck(Rchan_id))
    printf("caCheck says channel %s is connected\n",Rname);
  else
    printf("caCheck says channel %s is NOT connected\n",Rname);
  if (caCheck(Wchan_id))
    printf("caCheck says channel %s is connected\n",Wname);
  else
    printf("caCheck says channel %s is NOT connected\n",Wname);
  
  
  /* Read current */
  printf("\n   Now reading current \n");
  status=caRead(Rchan_id,&value);
  if(status==-1)
    {
      printf("FieldTest: Bad status after caRead\n");
      caExit();
      return;
    }


  printf("FieldTest: Read value as %8.3f A\n",value);

#ifdef WRITE          
  /* write current */
  printf("\n   Now about to set current to  %f A \n",wvalue1);
  status=caWrite(Wchan_id,&wvalue1);	
  if(status==-1)
    {
      printf("FieldTest: Bad status after caWrite\n");
      caExit();
      return;
    }    
  printf("Set value to %8.3f A\n",wvalue1);    

  printf("INFO: it may take  Field some time to change current if large change\n");
#else
  printf("INFO: for now writing to Field is disabled \n");
#endif


  /* read again */
  status=caRead(Rchan_id,&value);
  if(status==-1)
    printf("FieldTest: Bad status after caRead\n");
  else
    printf("FieldTest: Read back value as %8.3f A\n",value);

#ifdef WRITE

  /* test EpicsSet */
  /* EpicsSet & EpicsSet_Step go together */
  
  /* load up some conatants needed by EpicsSet and EpicsSet_Step */
  epics_constants.Rchan_id = Rchan_id ;
  epics_constants.Wchan_id = Wchan_id ;
  epics_constants.max_offset = typical_offset ;
  epics_constants.min_set_value = minimum_value;
  epics_constants.max_set_value=  maximum_value;
  epics_constants.stability = typical_stability;
  epics_constants.debug=dd;
  if(dd)
    {
      printf("Typical stability: %.2f\n",typical_stability);
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	      epics_constants.debug);
    }
  /* set a second value (wvalue2) */
  value=-1;
  printf("\n\n =======  Now calling EpicsSet to set value to %.2f  A\n",wvalue2);
  status = EpicsSet(wvalue2, typical_offset,&value, &offset, epics_constants);
  printf("EpicsSet returns with status =%d\n",status);
  printf("      and Read value=%.2f A, offset = %.2f A\n",value,offset);
  
  
  /* now test EpicsSet_step */
  
  printf("\n\n =========== Now calling EpicsSet_step \n");
  /* test EpicsSet_step */
  
  
  step1=1;
  wvalue2 = wvalue2 + step1; /* move by one step from last set value */
  stability = 0.2 * step1;
  if(stability < minimum_stability) stability = minimum_stability;
  if(stability > maximum_stability ) stability = maximum_stability;
  check_offset=typical_offset + stability;
  
  
  printf("Calling EpicsSet_step to step to %.2f A\n",wvalue2);
  status = EpicsSet_step(wvalue2, check_offset, typical_offset, step1, stability, &value, &offset, epics_constants);
  printf("EpicsSet_step returns with status =%d\n",status);
  printf("      and Read value=%.2f, offset = %.2f",value,offset);
  
  printf("\nYou can use FieldCycle() command to call test_cycle\n");

#else
  printf("End of tests as Writing to Field is disabled\n");  
#endif  
  printf("\n\nNow calling caExit to clear any open channels\n");
  caExit(); /* clear any channels that are open */
  
  /* try testing again if channels are open */
  
  printf("Now testing to see if channels are open:\n");
  if (caCheck(Rchan_id))
    printf("caCheck says channel %s is connected\n",Rname);
  else
    printf("caCheck says channel %s is NOT connected\n",Rname);
  if (caCheck(Wchan_id))
    printf("caCheck says channel %s is connected\n",Wname);
  else
    printf("caCheck says channel %s is NOT connected\n",Wname);
}



/* FieldLoop */
void FieldCycle(void)
{
  int status;

  typical_offset = 0.7 * maximum_offset; /* 70% of maximum; constant offset */

  status=caGetId (Rname, Wname, &Rchan_id, &Wchan_id);
  if(status==-1)
    {
      printf("FieldTest: Bad status after caGetID\n");
      caExit(); /* clear any channels that are open */
      return;
    }
  printf("caGetId returns Rchan_id = %d and Wchan_id = %d\n",Rchan_id, Wchan_id);
  
 
  printf("\n\n ========= Now calling test_cycle \n");
  /* test stepping in a loop */
  status=test_cycle(Rchan_id,Wchan_id);
     
  caExit();
   
}


int test_cycle(int Rchan_id, int Wchan_id)
{
  float FieldVal_diff, FieldVal_read, FieldValue;
  int FieldVal_ninc;
  int status,icount,i;
  float offset,check_offset,stability;
  /* test stepping in a loop */
  printf("Typical stability: %.2f\n",typical_stability);
  typical_stability = 0.3;
  /* load up some constants needed by EpicsSet and EpicsSet_Step */
  epics_constants.Rchan_id = Rchan_id ;
  epics_constants.Wchan_id = Wchan_id ;
  epics_constants.max_offset = maximum_offset ;
  epics_constants.min_set_value = minimum_value;
  epics_constants.max_set_value=  maximum_value;
  epics_constants.stability = typical_stability;
  epics_constants.debug=dd;
  
  printf("Typical stability: %.2f\n",typical_stability);
  printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
  printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	 epics_constants.max_offset,
	 epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	 epics_constants.debug);
  
  
  /* Steps from FieldVal_start to FieldVal_stop with an increment of FieldVal_inc */
  
  printf("test_cycle: starting... stepping from %.2fV to %.2fV in steps of %.2fV\n",
	 FieldVal_start,FieldVal_stop,FieldVal_inc);
  
  if (fabs (FieldVal_inc) < minimum_inc )  /* use a minimum increment  */
    {
      printf("test_cycle: Field Current increment (%.2f) must be at least %.2f A\n",FieldVal_inc,minimum_inc);
      return(-1) ;
    }
  if(FieldVal_start < minimum_value  || FieldVal_stop <  minimum_value || 
     FieldVal_start > maximum_value  || FieldVal_stop >  maximum_value)
    {
      printf("test_cycle: Field Current start or stop parameter(s) out of range. Allowed range is between %.2f and %.2fV\n",
	     minimum_value, maximum_value);
      return(-1);
    }
  
  if( ((FieldVal_inc > 0) && (FieldVal_stop < FieldVal_start)) ||
      ((FieldVal_inc < 0) && (FieldVal_start < FieldVal_stop)) )
    FieldVal_inc *= -1;
  FieldVal_ninc = ((FieldVal_stop - FieldVal_start)/FieldVal_inc + 1.); 
  if( FieldVal_ninc < 1)
    {
      printf("test_cycle: Invalid number of Field Current steps: %d\n", FieldVal_ninc);
      return(-1) ;
    }
  printf("=== test_cycle: Selected %d Field Current cycles starting at %.2f A by steps of %.2f A\n",
	 FieldVal_ninc,FieldVal_start,FieldVal_inc);
  FieldValue = FieldVal_start;
  
  
  
  /* set the first value to give it time to stabilize, test  Field etc (begin run) */
  if(dd)printf("Test_cycle: calling EpicsSet to set current to %.2f\n",FieldValue);
  
  status = EpicsSet( FieldVal_start, typical_offset, &FieldVal_read, &offset,epics_constants);
  if(dd)
    {
      printf("test_cycle: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.2f, offset = %.2f\n",FieldVal_read,offset);
    }
  
  switch(status)
    {
    case(0):
      printf("** Test_cycle: Success after EpicsSet at setpoint = %.2f \n",FieldVal_start);
      break;
    case(-4):
      printf("Test_cycle:  Field current not responding (could be switched off)\n");
    default:
      return(-1);
    }
  
  
  
  /* stability: to see if current has changed */
  stability = 0.2 * FieldVal_inc;
  if(stability < minimum_stability) stability =  minimum_stability;
  if(stability >  maximum_stability ) stability = maximum_stability ;
  
  /* FieldVal_diff - used for comparison of read_inc and write_inc
     for now divided everything by 1000 cf NaCell
   */
  if  (fabs (FieldVal_inc) < 0.01 )
    FieldVal_diff = 0.0005;
  else
    FieldVal_diff = 0.002; /* default value of 0.2V for comparison */
  
  printf("\nTest_cycle: begin-of-run procedure completed\n\n");
  
  for (i=0; i< nscans ; i++)
    {
      printf("\n      N E W  C Y C L E  (%d)    \n\n",i);  
      icount = 0;
      /* new scan: set point one incr. below FieldVal_start */
      
      FieldValue = FieldVal_start - FieldVal_inc;
      if(FieldValue < minimum_value || FieldValue > maximum_value ) 
	FieldValue = FieldVal_start + FieldVal_inc;
      
      if(dd)printf("Test_cycle: calling EpicsSet to set current to %.2fV (step %d)\n",FieldValue,icount);
      status = EpicsSet( FieldValue, typical_offset, &FieldVal_read, &offset,epics_constants);
      if(dd)
	{
	  printf("Test_cycle: EpicsSet returns with status =%d\n",status);
	  printf("      and Read value=%.2f, offset = %.2f\n",FieldVal_read,offset);
	}
      switch(status)
	{
	case(0):
	  printf("** Test_cycle: New cycle - success after EpicsSet at setpoint = %.2f\n",FieldValue);
	  break;
	case(-4):
	  printf("Test_cycle: New cycle -  Field current not responding (could be switched off)\n");
	default:
	  return(-1);
	}
  
      while( icount < FieldVal_ninc  )
	{  
	  if(dd)printf("\n\n ++++++++  Test_cycle:  Scan=%d Step %d, FieldValue %.2f ++++++++\n",i,icount,FieldValue);
	  /* calculate new FieldVal value */
	  FieldValue += FieldVal_inc;
	
	  
	  check_offset = offset + stability;
	  if(dd)printf("Test_cycle: calling EpicsSet_step to set current to %.2f\n",FieldValue);
	  status = EpicsSet_step( FieldValue, check_offset, typical_offset, FieldVal_inc, FieldVal_diff, 
				  &FieldVal_read, &offset,epics_constants);
	  if(dd)
	    {
	      printf("Test_cycle: EpicsSet_step returns with status =%d\n",status);
	      printf("      and Read value=%.2f, offset = %.2f\n",FieldVal_read,offset);
	    }
	  switch(status)
	    {
	    case(0):
	      if(dd)
		printf("*** Test_cycle: Success - Accepting set point %.2f (read %.2f)\n",FieldValue,FieldVal_read);
	      else
		printf("\rTest_cycle: Success - Accepting set point %.2f (read %.2f)      ",FieldValue,FieldVal_read);
	      break;
	    case (-3):
	      if(dd)
		printf("**** Test_cycle: Accepting set point  %.2f (read %.2f) since read/write values are within allowed offset\n",FieldValue,FieldVal_read );
	    case(-4):
	      printf("Test_cycle:  Field not responding at set point  %.2f (read %.2f) \n", FieldValue,FieldVal_read );
	    default:
	      return(-1);
	    }
	  
	  icount++;
	} /* end of while loop */
      
    } /* end of nscans loop */
  
      return(0);
}








