// Header section

Clock Frequency = 10 Mhz;
ISA Card Address = 8000;   // VME address of the pulse programmer board (in hex)
Number of Flags = 24;      // Number of output bits

//************************************************************************************************************************************//

//************************************************************************************************************************************//

// !! USER SECTION !! //

  // Delay declarations

  d_scalerpulse=0;  	// pulse-length for the multiscaler-next pulse
  d_freqpulse=0;   	// pulse-length for the next freq pulse
  d_pulse=0;   	// pulse-length, for pulsing simultaneously

  d_pre_beam_on=0;  	// beam on  cycle during beam build up
  d_pre_beam_off=0;  	// beam off cycle during beam build up

  d_ms_predelai=0;   	// delay between scaler next pulse and following enable counting-gate during the buildup    (RF on delay cycle)
  d_ms_postdelai=0;   // delay between disable counting-gate and following scaler next pulse during the buildup   (RF on delay cycle)

  d_countingtime=0;   	// counting enabled time                                                                    (case one, two or RF on delay cycle)

  d_ms_predelay=0;   	// delay between scaler next pulse (or rf-off in case 2) and following enable counting-gate (case one, two or three)
  d_ms_postdelay=0;   	// delay between disable counting-gate and following disable RF or pulse                    (case one, two or three)
  d_rf_off=0;   	// RF off delay between two distinct RF frequencies                                         (only for case one)
  d_rf_on=0;   		// RF on time                                                                               (only for case two)

  d_ctime_on=0;   	// counting time during RF-on                                                               (only for case three)
  d_ctime_off=0;   	// counting time during RF-off                                                              (only for case three)
  d_dacs=0; 		// DAC Servicetime
  d_beam_off=0;   	// beam-off time between the Acquisition cycles (identical with d_pre_beam_off !?)

// !! END OF USER SECTION !! //

//************************************************************************************************************************************//

// Bit patterns ATTENTION!!: The bits are adressed from high to low, the position in the sum gives the number of the bit affected.

f_rf0_on       = 1,1;   // bit   1
f_rf0_off      = 0,1;
f_rf90_on      = 1,1;   // bit   2
f_rf90_off     = 0,1;
f_rf180_on     = 1,1;   // bit   3
f_rf180_off    = 0,1;
f_rf270_on     = 1,1;   // bit   4
f_rf270_off    = 0,1;
f_rfgate_on    = 1,1;   // bit   5
f_rfgate_off   = 0,1;

f_rf_on        = 1,1;   // bit   6
f_rf_off       = 0,1;

f_msnp_hi  = 1,1;   // bit   7 next dwelltime multiscaler pulse
f_msnp_lo  = 0,1;
f_counter_e    = 1,1;   // bit   8 multiscaler counter-gate
f_counter_d    = 0,1;

f_userbit1_on  = 1,1;   // bit   9 -> Userbit1
f_userbit1_off = 0,1;

f_userbit2_on  = 1,1;   // bit  10 -> Userbit2
f_userbit2_off = 0,1;

f_freqp_hi     = 1,1;   // bit  11 next FSC freq. ext. strobe
f_freqp_lo     = 0,1;

f_udctrl_u     = 1,1;   // bit  12 Strobe up down control
f_udctrl_d     = 0,1;

f_dacservp_hi  = 1,1;   // bit  13 Dac service pulse
f_dacservp_lo  = 0,1;

f_beam_on      = 1,1;   // bit 14
f_beam_off     = 0,1;

f_pol_pos      = 1,1;   // bit 15
f_pol_neg      = 0,1;

f_fsc_0        = 0,9;
f_fsc_1        = 1,9;

f_fsc_dummy  = 0,9;   // mask for channel 16 - 24 FSC memory select channels

//************************************************************************************************************************************//

// Program //--------------------------------------------------------------------------------------------------------------// Program //

// Please choose the order of scaler and frequency pulses and the case for the counting gate timing //

// For aquisition with changing polarisation, use the polarisation loop and the second half of the script //

// Loop Polarisation n_pol;
   	d_pulse          			f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
  // pre-Acquisition beam on/off periods to reach steady state condition
  // Loop Prebeam n_precycles;
  //
  //   	d_pre_beam_on   		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
  //   	d_pre_beam_off 		f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
  // 
  // End Loop Prebeam;

 // Main Acquisition loop



 Loop Acquisition n_acq;
          	   d_pulse           	f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	  // delay after beam on, aquisition cycles without rf

	//  Loop rf_delay n_rfdelay;

	//     d_scalerpulse     	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	//     d_ms_predelai   	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	//    d_countingtime    	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	//     d_ms_postdelai  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;

	//  End Loop rf_delay;

	  // rf cycles loop, allows different time structure for the counting time. please add all delay-times in one cycle to check the time structure

	  Loop rf_on n_rf_on;

	
	     d_pulse         		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	     // use this to pulse fsc and multiscaler simultaneously. Decide and switch rf_on or off

	     // case one: counting cycle entirely in the rf_on period
	     d_ms_predelay   		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     d_countingtime  		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     d_ms_postdelay  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     // d_rf_off        		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

   	     d_pulse        			f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_hi + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
	     d_ms_predelay   		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
	     d_countingtime  		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_e + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
	     d_ms_postdelay  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;


	     // case two: counting cycle entirely in the rf_off period
	     // d_rf_on         		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     // d_ms_predelay   	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	     // d_countingtime  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	     // d_ms_postdelay  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

	     // case three: counting cycle starts in rf on period and stops in rf off period
	     // d_ms_predelay   	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     // d_ctime_on      		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     // d_ctime_off     		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	     // d_ms_postdelay  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

	  End Loop rf_on;
	  
	  //last acq cycle, userbit 2 on
	     d_pulse         		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_hi + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     d_ms_predelay   		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     d_countingtime  		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
	     d_ms_postdelay  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;

        	     d_pulse        			f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off  + f_counter_d + f_msnp_hi + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
	     d_ms_predelay   		f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
	     d_countingtime	  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off  + f_counter_e + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;
	     d_ms_postdelay  	f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_off + f_rf270_off + f_rf180_on  + f_rf90_off + f_rf0_off;

	     d_pulse         		f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	     d_dacs          		f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
  	     d_beam_off      		f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;

        End Loop Acquisition;


