/*   type2_compute.c 

Type 2 Compute routines included into rf_config.c
   i.e. 00 2a 2b 2c


  $Log: type2_compute.c,v $
  Revision 1.17  2006/06/21 19:11:12  suz
  add mode 1j (combination of 1c (CAMP) and 20 (SLR) )

  Revision 1.16  2005/08/17 23:17:53  suz
  fix error message

  Revision 1.15  2005/08/15 23:31:11  suz
  remove warning message about e2e_num_rf_dwelltimes_per_freq must be 1

  Revision 1.14  2005/08/04 00:56:01  suz
  add e2e_compute

  Revision 1.13  2005/07/19 21:36:20  suz
  fix for POL; doesn't use modes 2a,2b,2c

  Revision 1.12  2005/05/16 19:12:46  suz
  continuous/pulsed beam: fill new output param vme_beam_control

  Revision 1.11  2005/04/30 22:59:12  suz
  Added userbit action for pulse pairs (2a). Add 2d

  Revision 1.10  2005/04/13 01:15:35  suz
  syd fixes bug in pulse pair mode

  Revision 1.9  2005/04/12 23:10:23  suz
  Syd adds new 2a modes including pulse pairs

  
  Revision 1.7  2005/03/07 19:35:25  suz
  daq service time must be at least minimal delay; likely to be set small since PPG for Type 2 no longer free running (preparing for dual channel mode)

  Revision 1.6  2004/10/26 21:26:12  suz
  add PSM support

  Revision 1.5  2004/05/28 18:03:21  suz
  changes for 1g (FAST)

  Revision 1.4  2003/12/01 19:56:26  suz
  add support for mode 1g (FAST)

  Revision 1.3  2003/06/12 18:30:01  suz
  separate numbers of type 1 and type2 frontend histos

  Revision 1.2  2002/08/16 23:08:31  suz
  add new variable e2c_beam_on_time to avoid confusion

  Revision 1.1  2002/06/05 00:14:28  suz
  Type2 compute routines included into rf_config.c


/*------------------------------------------------------------------*/
INT e00_compute(char *ppg_mode)
{
  /* Mode SLR (00 or 20)
     Mode FAST (1g) also uses this routine 
  */
  char *modus;
  char m1,m2;
  FILE *rulef;
  char  str [256];
  char  str2[256];
  char  rulefile_name[256];

  double d_pulse       ;
  double d_countingtime;
  double d_dacs        ;
  double d_minimal     ;    
  int  n_acq         ;
  int  n_RFbeam      ;
  int  n_precycles   ;    /*Beam precycles*/
  int  n_beam        ;
  int  n_nobeam      ;
  int  n_noRFnobeam  ;
  int  n_nobeamPostRF ;
  int  RFduration;
  int  RFstart,beamon,beamoff;
  double time_slice;
  int i,j;
  div_t n;
  double d;
  int num_his;

  // FAST or 1g : called from e1g_compute
  // LCR  or 1j ; called from e1j_compute
  //    both use 1g ppg mode template file (same as 20/00 except no userbits) 
  
  BOOL mode1g_flag, mode1j_flag;
  
  mode1g_flag= mode1j_flag= FALSE;

  if( strcmp (ppg_mode,"1g")==0) 
    mode1g_flag=TRUE;
  else if ( strcmp (ppg_mode,"1j")==0  )
    mode1j_flag=TRUE;

  if(debug)printf("e00_compute is starting with ppg mode %s, mode1g_flag=%d  mode1j_flag=%d\n",
		  ppg_mode,mode1g_flag,mode1j_flag);
  
#ifdef PSM
  if( !mode1j_flag )/* 1j (CAMP scan and 20 combination) does not use RF */
    {
      if(check_psm_gate() != 0)return -1;
      if(check_psm_quad(ppg_mode)!= 0) // calls build_iq_table_psm for quad mode 
	{
	  cm_msg(MERROR,"e00_compute","Error return from check_psm_quad ");
	  return -1;
	}
    }
#endif

  /* --No frequency table in this mode  -- */

  /*Assign the ODB input values to the delays*/                 
  time_slice       =    ppg.input.time_slice__ms_;
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_pulse          =    d_minimal;
                /*All delays must be defined, even if they are not used in the selected acq.mode,*/
                /*because they are defined in each template. The perl compiler requires each delay to be */
                        /*in the allowed range, otherwise the compilation will stop with an error message*/
            /*When a delay = 0 is encountered, the corresponding mode is selected and the delay is set to*/
            /*minimum delaytime*/   


  d_countingtime=   ppg.input.mcs_enable_gate__ms_ - d_pulse;

  d_dacs    =   ppg.input.daq_service_time__ms_;
  
  n_acq     =   ppg.input.num_beam_acq_cycles;
  n_precycles   =       ppg.input.e00_prebeam_dwelltimes;
  RFstart       =       ppg.input.rfon_dwelltime;
  RFduration    =       ppg.input.rfon_duration__dwelltimes_;
  beamon    =   ppg.input.e00_beam_on_dwelltimes;
  beamoff   =       ppg.input.e00_beam_off_dwelltimes;

  /* make sure daq service time is at least the minimal delay
     (now that ppg is started by software for Type 2 we may not need the daq service time) */
  if(d_dacs <=0)
    d_dacs = d_minimal;

  /* in the selection of the numerical mode parameter
      n_precycles is not yet modified for proper ppg    
      ppg output (i.e. -1ed)
  */                  
  if (RFduration <= 0 )
    {
      m1       = '0';    /* NO RF*/
      n_beam   = beamon  -1;
      n_RFbeam = 0;
      n_nobeam = beamoff -2;
      n_noRFnobeam = 0;
    }
  else if (RFstart < n_precycles) 
    {
      cm_msg(MINFO,"e00_compute","RF irradiation in prebeam cycle is not implemented");
      cm_msg(MERROR,"e00_compute","Please set RFon delay > e00_prebeam_dwelltimes");
      return -1;
    }   
/*mode selection m1: RF off -> on transition */

  else if (RFstart == n_precycles )
    {
      m1       = '1';    /*transition with beam on */
      n_beam   = beamon  -1;
      n_RFbeam = 0;
      n_nobeam = beamoff -2;
      n_noRFnobeam = 0;
      if (RFduration > 0) 
    cm_msg(MINFO,"e00_compute","Finite values of RFon duration ignored unless RFon delay >= prebeam + beam on");
    }
  else if (RFstart < n_precycles + beamon)
    {
      m1       = '2';    /*transition in beam on time*/
      n_beam   = RFstart - n_precycles - 1 ;
      n_RFbeam = beamon  - (RFstart - n_precycles) - 1 ;
      if(debug)
       {printf("mode 3 n_nbeam %d\n",n_beam);
       printf("mode 3 n_RFbeam %d\n",n_RFbeam);
       }
      n_nobeam = beamoff -2;
      n_noRFnobeam = 0;
      if (RFduration > 0) 
    cm_msg(MINFO,"e00_compute","Finite values of RFon duration ignored in unless RFon delay >= prebeam + beam on");
    }
  else if (RFstart == n_precycles + beamon )
    {
      m1       = '3';    /*transition with beam on -> off transition*/
      n_beam   = beamon  -1;
      n_RFbeam = 0;
      n_noRFnobeam = 0;
      n_nobeam = RFduration - 1;
      n_nobeamPostRF = beamoff - RFduration - 2;
      if(debug)
    printf("mode 3 n_nobeamPostRF %d\n",n_nobeamPostRF);
      if (n_nobeamPostRF < 0)
    {cm_msg(MERROR,"e00_compute","Insufficient beam off time to accomodate RFon delay + Rfon duration ");
    return -1;
    }
    }
  else if (RFstart > n_precycles + beamon  )
    {
      m1       = '4';    /*transition after beam on -> off transition*/
      n_beam   = beamon  -1;
      n_RFbeam = 0;
      n_noRFnobeam = RFstart - (n_precycles + beamon) - 1 ;
      n_nobeam = RFduration -1;
      n_nobeamPostRF = beamoff - (RFstart - (n_precycles + beamon)) - RFduration - 2 ;
      if(debug) 
    printf("n_nobeam %d\n",n_nobeam);
      if(debug)
    printf("mode 4 n_nobeamPostRF %d\n",n_nobeamPostRF);
      if (n_nobeamPostRF < 0)
    {
      cm_msg(MERROR,"e00_compute","Insufficient beam off time to accomodate RFon delay + Rfon duration ");
      return -1;
    }
    }
  else
    {
      cm_msg(MERROR,"e00_compute","Impossible mode case - see source code ");
      return -1;
    }

  if (  n_beam < 0)
    {
      cm_msg(MINFO,"e00_compute","Loop count for beam_on < 1");
      cm_msg(MERROR,"e00_compute","Please set beam on dwelltimes to a larger value");
      return -1;
        }
  if (  n_nobeam < 0)
    {
      cm_msg(MINFO,"e00_compute","Loop count for beam_off < 1");
      cm_msg(MERROR,"e00_compute","Please set beam off dwelltimes to a larger value");
      return -1;
        }


  ppg.output.dwell_time__ms_     =  d_countingtime + d_pulse;
  ppg.output.num_dwell_times     =  beamon + beamoff + n_precycles;
  ppg.output.beam_on_time__ms_   =  beamon * (d_countingtime + d_pulse);
  ppg.output.vme_beam_control    =  FALSE; /* pulsed beam, PPG controls beam */

/*mode selection m2: pre beam cycles */
  n_precycles = n_precycles -1;
  m2 = '0';
  if (n_precycles >= 0)
    m2       = 'P';
  /*    n_precycles = n_precycles -1;  srk comments this out as an error */


  if( mode1g_flag ||  mode1j_flag)
    {
      num_his =  ppg.input.num_type1_frontend_histograms;    
      if(debug)printf("e00_compute: Mode %s no. histograms=%d\n",ppg_mode, num_his);
    }
  else
    {
      num_his =  ppg.input.num_type2_frontend_histograms;    
      if(debug)printf("e00_compute: Mpde 00 no. histograms=%d\n",num_his);
    }

  /* -- Writing the histogram information to the ODB (e00) -- */
  
  i             = RFstart;  /*since RFstart markes the first bin with RF on */
  for (j=0; j <  num_his ; j ++)
     {
       fmdarc.histograms.bin_zero [j]       = 1;
       fmdarc.histograms.first_good_bin [j] = i;
     }


  i                 = ppg.output.num_dwell_times;
  for (j=0; j <  num_his; j ++)
    fmdarc.histograms.last_good_bin [j] = i;

  if (m2 == 'P')
    {
      i = n_precycles +1;
      for (j=0; j < num_his ; j ++)
    {
      fmdarc.histograms.first_background_bin [j] = 1;
          fmdarc.histograms.last_background_bin  [j] = i;
    }
    }
  else
    {
      for (j=0; j <  num_his ; j ++)
    {
      fmdarc.histograms.first_background_bin [j] = 0;
          fmdarc.histograms.last_background_bin  [j] = 0;
    }
    }
 /* write some information to mdarc area for e00 or e1g/e1j */
  fmdarc.histograms.number_defined = num_his ; /*  number of  histograms as written by frontend */
  fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

  if( ! ppg.sis_test_mode.test_mode)  /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
    {
      if(debug)printf("SIS test mode is disabled\n");
      fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
      fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
    }
  else   /* sis internal  test mode  */  
    {
      if(debug)printf("SIS test mode is enabled\n");
      fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.dwell_time__ms_;
      fmdarc.histograms.num_bins = ppg.sis_test_mode.num_bins;
      
      for (j=0; j <  num_his; j ++)
    {
      fmdarc.histograms.last_good_bin   [j] =  fmdarc.histograms.num_bins ;
      fmdarc.histograms.last_background_bin  [j] = 0;
    }
    }
  if(debug)
    {
      printf("Writing values to mdarc area :\n");
      printf("  Number frontend histograms : %d\n", fmdarc.histograms.number_defined);
      printf("  Number of bins             : %d\n", fmdarc.histograms.num_bins);
      printf("  Dwell time (ms)            : %f\n", fmdarc.histograms.dwell_time__ms_);
    }
  
  
  /* -- Writing the rulefile for the substitution from template-file to the pulse-script -- */
  cm_msg(MINFO,"e00_compute","Computing values and writing to the ODB done, writing rulefile.");
  
  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e00_compute: about to open rulefile: %s\n",rulefile_name);
  
  rulef = fopen(rulefile_name, "w");
  if (rulef) 
    {
      int len,size;
      sprintf(str,"%s_%c%c%s%s",ppg_mode,m1,m2,".ppg","\n"); /*  1g/1j use 1g/1j ppg files (identical) */

      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
      if (debug) printf("e00_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);
      
      fputs(str,rulef);
      sprintf(str,"%s%s",ppg_mode,".ppg\n");
      fputs(str,rulef);
      
      
      sprintf(str,"%s%s%s%f%s","d_pulse=0","%","d_pulse=",d_pulse,"ms\n");
      fputs(str,rulef);
      
      sprintf(str,"%s%s%s%f%s","d_countingtime=0","%","d_countingtime=",d_countingtime,"ms\n");
      fputs(str,rulef);
      
      sprintf(str,"%s%s%s%f%s","d_dacs=0","%","d_dacs=",d_dacs,"ms\n");
      fputs(str,rulef);
      


      sprintf(str,"%s%s%d%s","n_precycles","%",n_precycles,"\n");
      fputs(str,rulef);
      sprintf(str,"%s%s%d%s","n_acq","%",n_acq,"\n");
      fputs(str,rulef);
      sprintf(str,"%s%s%i%s","n_beam","%",n_beam,"\n");
      fputs(str,rulef);
      sprintf(str,"%s%s%i%s","n_postRF","%",n_nobeamPostRF,"\n"); 
      fputs(str,rulef);
      sprintf(str,"%s%s%i%s","n_nobeam","%",n_nobeam,"\n");
      fputs(str,rulef); 
      sprintf(str,"%s%s%d%s","n_RFbeam","%",n_RFbeam,"\n");
      fputs(str,rulef);
      sprintf(str,"%s%s%i%s","n_noRFnobeam","%",n_noRFnobeam,"\n");
      fputs(str,rulef);
      fclose (rulef);
      
    }
  else 
    {
      cm_msg(MERROR,"e00_compute","can't open rulefile %s", rulefile_name);
      return -1;
    }

  cm_msg(MINFO,"e00_compute","Rulefile %s written.",rulefile_name);
                                    
  return 1;
}

/*------------------------------------------------------------------*/
INT e2a_compute(char *ppg_mode)
{
  char *modus;
  char m1,m2,m3,m4,m5;
  FILE *rulef;
  char  str [256];
  char  str2[256];
  char  rulefile_name[256];

  double d_freqpulse      ;
  double    d_scalerpulse    ;
  double d_pulse          ;
  double    d_ms_postdelai   ;  /*MCS gate off time during RF on delay dwell times*/ 
  double    d_ms_predelay    ;  /*MCS gate delay during regular dwell times*/
  double    d_ms_predelai    ;  /*MCS gate delay during RF on delay dwell times*/
  double    d_pre_beam_off   ;
  double    d_pre_beam_on    ;
  double    d_ms_postdelay   ; /*MCS gate off time during regular dwell times*/
  double    d_countingtime   ;
  double    d_rf_on          ;
  double d_rf_off         ;
  double d_ctime_on       ;    /*MCS counting time with RF on, mode 3*/
  double d_ctime_off      ;    /*MCS counting time with RF off, mode 3*/
  double d_beam_off       ;
  double d_dacs           ;
  double d_minimal   ;
  DWORD n_acq            ;
  DWORD n_rf_on          ;
  DWORD n_precycles      ;    /*Beam precycles*/
  DWORD n_rfdelay        ;    /*RF on delay in dwelltimes*/
  DWORD n_pol            ;
  DWORD f_inc            ;    
  int  freq_num_factor   ;    /* =2 if Pulse Pair mode, =1 otherwise */ 
  double time_slice;
  int i,j,ad_check;
  div_t n;
  double d;

#ifdef POL
  /* POL does not need 2a */
  printf("e2a_compute: ppg mode %s is not supported for POL expt\n",ppg_mode);
  return -1;
#else
  if(debug)printf("e2a_compute is starting with ppg mode %s\n",ppg_mode);
  

#ifdef PSM
  /* check only one gate parameter is selected */
  printf("calling check_psm_gate()\n");
  if(check_psm_gate() != 0)
    {
      cm_msg(MERROR,"e2a_compute","Error return from check_psm_gate ");
      return -1;
    }
  if(check_psm_quad(ppg_mode)!= 0) // calls build_iq_table_psm for quad mode 
    {
      cm_msg(MERROR,"e2a_compute","Error return from check_psm_quad ");
      return -1;
    }


  /* -- Calculate the frequency table (PSM) -- */
  i = build_f_table_psm(ppg_mode);
  if ( i == -1 )
    {
      cm_msg(MERROR,"e2a_compute","Error return from build_f_table_psm ");
      return -1;
    }
#else
  /* -- Calculate the frequency table (FSC)-- */
  i = build_f_table(ppg_mode);
  if ( i == -1 )
    {cm_msg(MERROR,"e2a_compute","Invalid number frequency steps %i ",i);
    return -1;
    }
#endif

ppg.output.num_frequency_steps =      i; 


/*Assign the ODB input values to the delays*/                 
  time_slice       =    ppg.input.time_slice__ms_;
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_pulse          =    d_minimal;
  d_scalerpulse    =    d_pulse;
  d_freqpulse      =    d_pulse;
  d_ctime_on       =    d_minimal;         /*All delays must be defined, even if they are not used in the selected acq.mode,*/
  d_ctime_off      =    d_minimal;         /*because they are defined in each template. The perl compiler requires each delay to be */
                                           /*in the allowed range, otherwise the compilation will stop with an error message*/
                       /*When a delay = 0 is encountered, the corresponding mode is selected and the delay is set to*/
                       /*minimum delaytime*/    

  d            =    (ppg.output.num_frequency_steps +
                ppg.input.num_rf_on_delays__dwell_times_); 		/* not use as of 2005/04/08 srk */
                
  d_dacs       =    ppg.input.daq_service_time__ms_;

  ppg.output.e2a_pulse_pairs_mode=9; /* initialize to a large value */
  if (ppg.input.e2a_pulse_pairs)
    {
      freq_num_factor =  2;   					/* 2005/04/08 srk */
      /* make sure it's in lower case */
      if (ppg.input.e2a_ubit1_action[0] == 0)
	{
	  cm_msg(MERROR,"e2a_compute",
		 "Null string supplied for pulse pair userbit action. Must be one of: pairs,1st,2nd or diff ");
	  return -1;
	}
      
      /* fill e2a_pulse_pairs_mode to make it easier for frontend or mdarc */
      sprintf(str,"%s",ppg.input.e2a_ubit1_action);
      str[2]='\0'; /* terminate after two characters */
      for  (j=0; j< strlen(str) ; j++) 
	str[j] = tolower (str[j]); /* convert to lower case */  
      
      if( strncmp(str,"pairs",2) == 0) 
	ppg.output.e2a_pulse_pairs_mode=0;
      else if (strncmp(str,"1st",2) == 0)
	ppg.output.e2a_pulse_pairs_mode=1;
      else if  (strncmp(str,"2nd",2) == 0)
	ppg.output.e2a_pulse_pairs_mode=2;
      else if  (strncmp(str,"diff",2) == 0)
	ppg.output.e2a_pulse_pairs_mode=3;
      else
	{	    
	  cm_msg(MERROR,"e2a_compute",
		 "Invalid pulse pair userbit action \"%s\". Must be one of: pairs,1st,2nd or diff ",
		 ppg.input.e2a_ubit1_action);
	  return -1;
	}
      printf("e2a_compute: pulse pair userbit action is: %s\n",ppg.input.e2a_ubit1_action);
    }
  else
    freq_num_factor =  1;  /* default 2005/04/08 srk */
  
  printf("freq_num_factor = %d\n",freq_num_factor); 
  
  /* -- Write computed output values to the ODB -- */
  ppg.output.dwell_time__ms_     =      ppg.input.rf_on_time__ms_
                    + ppg.input.rf_off_time__ms_;
  ppg.output.num_dwell_times     =    ppg.input.num_rf_on_delays__dwell_times_
                    + ppg.output.num_frequency_steps * freq_num_factor ;  	/* 2005/04/08 srk */
  ppg.output.beam_on_time__ms_   =  ppg.output.num_dwell_times
                    * (ppg.output.dwell_time__ms_ + d_pulse) ;			/* 2005/04/08 srk */

    /* make sure daq service time is at least the minimal delay
     (now that ppg is started by software for Type 2 we may not need the daq service time) */
  if(d_dacs <=0)
    d_dacs = d_minimal;
  
  d_beam_off       =    ppg.input.beam_off_time__ms_;
  d_pre_beam_on    =    ppg.output.beam_on_time__ms_;			/* redefined 2005/04/08 srk 
  											but will eventually be removed 
  											since prebeam cycles removed */

  d_pre_beam_off   =    ppg.input.beam_off_time__ms_ + d_dacs;
  d_countingtime   =    ppg.input.mcs_enable_gate__ms_;



  d_ms_predelai  =  ppg.input.mcs_enable_delay__ms_ - d_pulse;
                        if (!rangecheck(d_ms_predelai,d_minimal,time_slice))
                                {cm_msg(MINFO,"e2a_compute","Rangecheck-Error for d_ms_predelai = %f ",d_ms_predelai);
                                 cm_msg(MERROR,"e2a_compute","MCS enable delay (ms) must be > 2* Minimal delay (ms) ");
                                 return -1;
                                }

  d_ms_postdelai =    ppg.output.dwell_time__ms_ -
            (ppg.input.mcs_enable_delay__ms_+d_countingtime);       
                         if (!rangecheck(d_ms_postdelai,d_minimal,time_slice))
                                {cm_msg(MINFO,"e2a_compute","Rangecheck-Error for d_ms_postdelai = %f ",d_ms_postdelai);
                 cm_msg(MERROR,"e2a_compute","MCS enable delay + MCS enable gate  must be < RF on + RF off");
                                 return -1;
                                }

  d_ms_predelay    =    ppg.input.mcs_enable_delay__ms_-d_pulse;
                        if (!rangecheck(d_ms_predelay,d_minimal,time_slice))
                                {cm_msg(MINFO,"e2a_compute","Rangecheck-Error for d_ms_predelay = %f ",d_ms_predelay);
                                 cm_msg(MERROR,"e2a_compute","MCS enable delay (ms) must be > 2* Minimal delay (ms) ");
                                 return -1;
                                }

  d_rf_on          =    ppg.input.rf_on_time__ms_;

  /*The following part of the code prepares the check for the adiabacity. see below.*/
  n =   div (ppg.input.frequency_start__hz_, f_switch_boundary);
  ad_check = n.quot;
  n = div (ppg.input.frequency_stop__hz_ , f_switch_boundary);
  ad_check = ad_check - n.quot;

  d_rf_off         =    ppg.input.rf_off_time__ms_;
  /*With RF off = 0, the program assumes that the user wants to run adiabatic frequency sweeps   */
  /*Adiabacity is destroyed, when the frequency crosses any 100 kHz boundary (see PTS 160 manual)*/
  /* August 2001 - Syd requested for this test to be removed - Hardware has been fixed he says 
     if ((d_rf_off == 0) && (ad_check < 0))
     {cm_msg(MERROR,"e2a_compute","Phase coherence error: Crossing a 100 kHz boundary with RF off time = 0");
     return -1;
     }
  */

  /* -- Selection of the matching template file -- 2004/04/08 srk */

    /* Filename: ppg_mode      +m1        +m2        +m3       +m4                       +m5        .ppg 
       like      2a_           C/P        C/P        0/P       N/P/Q                     1/2/3      .ppg 
                               beam       RF         RFdelay   N:Single Pulse/Freq       MCS gate
                               CW/Pulsed  CW/Pulsed  off/on    P:0deg  Pulse Pairs/Freq  Counting   
                                                               Q:0/180 Pulse Pairs/Freq  Mode
   
    Available modes are:    2a_CC0N1.ppg    2a_CC0P1.ppg    2a_CC0Q1.ppg  
                            2a_PC0N1.ppg    2a_PC0P1.ppg    2a_PC0Q1.ppg
                            2a_PCPN1.ppg                                
                            2a_CP0N1.ppg    2a_CP0P1.ppg    2a_CP0Q1.ppg
                            2a_CP0N2.ppg    2a_CP0P2.ppg    2a_CP0Q2.ppg
                            2a_CP0N3.ppg    2a_CP0P3.ppg    2a_CP0Q3.ppg
                            2a_CPPN1.ppg                                
                            2a_CPPN2.ppg                                
                            2a_CPPN3.ppg                                
                            2a_PP0N1.ppg    2a_PP0P1.ppg    2a_PP0Q1.ppg
                            2a_PP0N2.ppg    2a_PP0P2.ppg    2a_PP0Q2.ppg
                            2a_PP0N3.ppg    2a_PP0P3.ppg    2a_PP0Q3.ppg
                            2a_PPPN1.ppg
                            2a_PPPN2.ppg
                            2a_PPPN3.ppg
    */                           

  /*Counting gate mode selection, m5*/

  if (ppg.input.mcs_enable_delay__ms_ > d_rf_on)
    {               
       /*Counting mode 2, counting gate in RF off */
     d_ms_predelay  = ppg.input.mcs_enable_delay__ms_ - d_rf_on;
     if (!rangecheck(d_ms_predelay,d_minimal,time_slice))
       {cm_msg(MINFO,"e2a_compute","Counting in RF off: Rangecheck-Error for d_ms_predelay = %f ",d_ms_predelay);
       cm_msg(MERROR,"e2a_compute","MCS enable delay (ms) must be >= RF on time + Minimal delay ");
       return -1;
       }

     d_ms_postdelay = d_rf_off - (d_ms_predelay+ d_countingtime);
     if (!rangecheck(d_ms_postdelay,d_minimal,time_slice))
       {cm_msg(MINFO,"e2a_compute","Rangecheck-Error for d_ms_postdelay = %f ",d_ms_postdelay);
       cm_msg(MERROR,"e2a_compute","MCS enable delay + counting gate must be < RF on + RF off + Minimal delay");
       return -1;
       }

     m5 = '2';
    }
  else {
    if (ppg.input.mcs_enable_delay__ms_+d_countingtime > d_rf_on)
      {                 
    /*Counting mode 3, counting gate over RF on-off transition*/
    d_ms_postdelay =  d_ms_postdelai;
    
    /*Split counting gate in RF on and RF off part*/
    d_ctime_on     =  d_rf_on - ppg.input.mcs_enable_delay__ms_;
    if (!rangecheck(d_ctime_on,d_minimal,time_slice))
      {cm_msg(MINFO,"e2a_compute","Counting during the RF on->off transition: Rangecheck-Error for d_ctime_on = %f ",d_ctime_on);
      cm_msg(MERROR,"e2a_compute","MCS enable delay must be < RF on - Minimal delay");
      return -1;
      }

    d_ctime_off    =  d_rf_off - d_ms_postdelay;
    if (!rangecheck(d_ctime_off,d_minimal,time_slice))
      {cm_msg(MINFO,"e2a_compute","Counting during the RF on->off transition: Rangecheck-Error for d_ctime_off = %f ",d_ctime_off);
      cm_msg(MERROR,"e2a_compute","MCS enable delay + counting gate must be >= RF on + Minimal delay");
      return -1;
      }

    m5 = '3';
      }
    else            
      {
    /*Counting mode 1, counting gate in RF on */
    d_ms_postdelay = d_rf_on - d_countingtime - ppg.input.mcs_enable_delay__ms_;
    if (!rangecheck(d_ms_postdelay,d_minimal,time_slice))
      {cm_msg(MINFO,"e2a_compute","Counting in RF on: Rangecheck-Error for d_ms_postdelay = %f ",d_ms_postdelay);
      cm_msg(MERROR,"e2a_compute","MCS enable delay + counting gate must be <= RF on - Minimal delay");
      return -1;
      }
    
    m5 = '1';
      }
    
  }

   
  /* -- RF on delay selection, m3 -- */

  n_rfdelay = ppg.input.num_rf_on_delays__dwell_times_ -1;

  if (n_rfdelay == -1)
    m3 = '0';     /*No RF on delay*/
   else
    m3 = 'P';     /*RF on delay present*/

  /* -- RF mode selection, m2 -- */

  if (d_rf_off == 0)
    {
     m2 = 'C';                   /*Continous irradiation*/
     d_rf_off = d_minimal;       /*Must be defined within the allowed range, see above*/

     if (m5 != '1' )             /*no RF off time*/
                            {cm_msg(MERROR,"e2a_compute","Counting gate longer than RF on time, but no RF off time specified");
                             return -1;
                            }
                                 /* -> Counting in RF off is not possible without RF off time*/
    }

    else
     m2 = 'P';                   /*Pulsed irradiation*/

  /* -- Beam mode selection, m1 -- */

  if (d_beam_off == 0)
    {
     m1 = 'C';                   /*Continuous beam mode*/
     d_beam_off = d_minimal;
     d_pre_beam_on = d_pre_beam_on - d_pulse; /* Since there can be no loop with only one delay
                                                 Use a  two delay loop in the pre_beam, with the 
                         second loop length d_pulse */
     ppg.output.vme_beam_control=TRUE;

     if (m3 != '0' && m2 == 'C')
       {cm_msg(MERROR,"e2a_compute","Continuous (beam and RF) mode -> RF on delay must be 0 but is  %i dwelltimes",n_rfdelay + 1);
       return -1;
       }     /*No RF on delay allowed in continous (beam and RF)  mode*/
    }
  else
    {
      m1 = 'P';                  /*Pulsed beam mode*/
      ppg.output.vme_beam_control = FALSE;
   /* if (m3 == '0') 
    {cm_msg(MERROR,"e2a_compute","Running in pulsed beam mode. RF on delay must be > 0 dwelltimes, but is 0 ");
    return -1;
    }     /*RF delay must be present in pulsed beam mode*/         /* added 2a_Px0xx modes 2005/04/08 SRK */  
    }
  
 /* -- Pulse Pair Mode Selection, m4 -- */						/* 2005/04/09 SRK */
  
      m4 = 'N';
   if (ppg.input.e2a_pulse_pairs && (m3 != '0'))
   	{cm_msg(MERROR,"e2a_compute","Running in pulse-pair mode with no zero RF on delay is prohibited");
    	return -1;
    	}     /*RF delay must be O in pulse-pair mode*/
    	
   if (ppg.input.e2a_pulse_pairs && !ppg.input.e2a_180)
      { 
	m4 ='P';
      } /* all pulses on 0 degree phase channel */
      
   if (ppg.input.e2a_pulse_pairs && ppg.input.e2a_180)
      {
	m4 ='Q';
      } /* odd pulses on 180 degree phase channel */


  
  /*The number of beam precycles must be zero */					/* 2005/04/09 SRK */

  
  n_precycles   = ppg.input.num_beam_precycles-1;
  if (n_precycles != -1)
    {
      cm_msg(MINFO,"e2a_compute","Loop count non-zero for beam precycles, set to zero");
      n_precycles = -1;
    }
  
  /*The number of acquisition cycles must be one or more*/
  
  n_acq     = ppg.input.num_beam_acq_cycles-1;
  if (n_acq == -1)
    {
      cm_msg(MINFO,"e2a_compute","Loop count zero for acquisition cycles, set to one");
      n_acq = 0;
    }
  
  n_rf_on   = ppg.output.num_frequency_steps-2; 
  /* -2 accounts for last acq cycle with ub2 on and +1 in pulseblaster loop execution */
  if (n_rf_on <= 0)
    {
      cm_msg(MERROR,"e2a_compute","Loop count < zero in n_rf_on, number of frequencies must be >= 3.");
      return -1;
    }
  

  ppg.output.rf_on_time__ms_  = d_rf_on;
  ppg.output.rf_off_time__ms_ = d_rf_off;

  /* -- Writing the histogram information to the ODB (2a) -- */

  i             = n_rfdelay + 2;  /*since n_rfdelay is the number of dwelltimes in the delay -1 */
  for (j=0; j < ppg.input.num_type2_frontend_histograms   ; j ++)
    {fmdarc.histograms.bin_zero [j]       = 1;
    fmdarc.histograms.first_good_bin [j] = i;
    }


  i                 = fmdarc.histograms.first_good_bin [1] + ppg.output.num_frequency_steps -1;
  for (j=0; j < ppg.input.num_type2_frontend_histograms  ; j ++)
    fmdarc.histograms.last_good_bin [j] = i;
  
  if (m3 == 'P')
    {
      i = n_rfdelay +1;
      for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.first_background_bin [j] = 1;
    fmdarc.histograms.last_background_bin  [j] = i;
    }
    }
  else
    {
      for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.first_background_bin [j] = 0;
    fmdarc.histograms.last_background_bin  [j] = 0;
    }
    }
  /* write some information to mdarc area for type 2a */
  fmdarc.histograms.number_defined = ppg.input.num_type2_frontend_histograms ; /*  number of  histograms as written by frontend */
  fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */
  
  if( ! ppg.sis_test_mode.test_mode)  /* real mode (front panel signals, uses ppg and fsc */
    /* The number of bins as calculated by rf_config */
    {
     if(debug)printf("SIS test mode is disabled\n");
      fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;

      if((ppg.output.e2a_pulse_pairs_mode > 0) &&  (ppg.output.e2a_pulse_pairs_mode <=3)  )
	{ /* pulse_pairs compaction modes -> userbit1 action : 0=pairs 1=first 2=second 3=diff */
	  fmdarc.histograms.num_bins  =  ppg.output.num_dwell_times -  
	    ppg.output.num_frequency_steps; /* end up with less bins */
	}
      else
	fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
    }
  else   /* sis internal  test mode  */  
   {
     if(debug)printf("SIS test mode is enabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.num_bins;
     for (j=0; j <  ppg.input.num_type2_frontend_histograms; j ++)
       {
     fmdarc.histograms.last_good_bin    [j] =  fmdarc.histograms.num_bins ;
     fmdarc.histograms.last_background_bin  [j] = 0;
       }
   }
  if(debug)
    {
      printf("Writing values to mdarc area:\n");
      printf("  Number frontend histograms: %d\n", fmdarc.histograms.number_defined);
      printf("  Number of histo bins      : %d\n", fmdarc.histograms.num_bins);
      printf("  Dwell time (ms)           : %f\n", fmdarc.histograms.dwell_time__ms_);
    }
  
  
  /* -- Writing the rulefile for the substitution from template-file to the pulse-script -- */
  if (d_pre_beam_off < d_minimal) d_pre_beam_off = d_minimal;
  cm_msg(MINFO,"e2a_compute","Computing values and writing to the ODB done, writing rulefile.");
  
  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e2a_compute: about to open rulefile: %s\n",rulefile_name);
  
  rulef = fopen(rulefile_name, "w");
  if (rulef) {
    
    sprintf(str,"%s_%c%c%c%c%c%s%s",ppg_mode,m1,m2,m3,m4,m5,".ppg","\n"); /* add an underscore after ppg_mode */
    {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
    }
    if (debug) printf("e2a_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);


    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);
    
    sprintf(str,"%s%s%s%f%s","d_scalerpulse=0","%","d_scalerpulse=",d_scalerpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_freqpulse=0","%","d_freqpulse=",d_freqpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pulse=0","%","d_pulse=",d_pulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pre_beam_on=0","%","d_pre_beam_on=",d_pre_beam_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pre_beam_off=0","%","d_pre_beam_off=",d_pre_beam_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_predelai=0","%","d_ms_predelai=",d_ms_predelai,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_predelay=0","%","d_ms_predelay=",d_ms_predelay,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_postdelai=0","%","d_ms_postdelai=",d_ms_postdelai,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_postdelay=0","%","d_ms_postdelay=",d_ms_postdelay,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_countingtime=0","%","d_countingtime=",d_countingtime,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_rf_on=0","%","d_rf_on=",d_rf_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_rf_off=0","%","d_rf_off=",d_rf_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ctime_on=0","%","d_ctime_on=",d_ctime_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ctime_off=0","%","d_ctime_off=",d_ctime_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dacs=0","%","d_dacs=",d_dacs,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_beam_off=0","%","d_beam_off=",d_beam_off,"ms\n");
    fputs(str,rulef);
    
    
    sprintf(str,"%s%s%d%s","n_pol","%",n_pol,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_acq","%",n_acq,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%i%s","n_rf_on","%",n_rf_on,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%i%s","n_precycles","%",n_precycles,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%i%s","n_rfdelay","%",n_rfdelay,"\n");
    fputs(str,rulef);
    
    fclose (rulef);
    
  }
  else {
    cm_msg(MERROR,"e2a_compute","can't open rule file %s", rulefile_name);
    return -1;
  }
  cm_msg(MINFO,"e2a_compute","Rulefile %s written.",rulefile_name);    
  return 1;
#endif
}


/*------------------------------------------------------------------*/
INT e2b_compute(char *ppg_mode)
{
  char *modus;
  char m1,m2,m3,m4;
  FILE *rulef;
  char  str [256];
  char  str2[256];
  char  rulefile_name[256];

  double d_freqpulse      ;
  double    d_scalerpulse    ;
  double d_pulse          ;
  double    d_ms_postdelai ;
  double    d_ms_predelay    ;
  double    d_ms_predelai    ;
  double    d_pre_beam_off   ;
  double    d_pre_beam_on    ;
  double    d_ms_postdelay   ;
  double    d_countingtime   ;
  double    d_rf_on          ;
  double d_rf_off         ;
  double d_ctime_on       ;
  double d_ctime_off      ;
  double d_beam_off       ;
  double d_dacs           ;
  double d_minimal   ;
  DWORD n_acq            ;
  DWORD n_beam_on        ;
  DWORD n_precycles      ;    /*Beam precycles*/
  DWORD n_rfdelay        ;    /*RF on delay in dwelltimes*/
  DWORD n_beam_off       ;
  DWORD n_rfd_nobeam     ;
  DWORD f_inc            ;
  double time_slice;
  int i,j,ad_check;
  div_t n;
  double f;
#ifdef POL
  /* POL does not need 2b  */
  printf("e2b_compute: ppg mode %s is not supported for POL expt\n",ppg_mode);
  return -1;
#else

  if(debug)printf("e2b_compute is starting with ppg_mode %s\n",ppg_mode);

  /* -- Calculate the frequency table -- */
#ifdef PSM
  if(check_psm_gate() != 0)return -1;
  if(check_psm_quad(ppg_mode)!= 0) // calls build_iq_table_psm for quad mode 
    {
      cm_msg(MERROR,"e2b_compute","Error return from check_psm_quad ");
      return -1;
    }
  /* -- Calculate the PSM frequency table -- */
  i = build_f_table_psm(ppg_mode);
  if ( i == -1 )
    {
      cm_msg(MERROR,"e2b_compute","Error return from build_f_table_psm ");
      return -1;
    }
#else
  /* -- Calculate the FSC frequency table -- */
  i = build_f_table(ppg_mode);
  if ( i == -1 )
    {
      cm_msg(MERROR,"e2b_compute","Invalid number frequency steps %i ",i);
      return -1;
    }
#endif
  
  /* -- Write computet output values to the ODB -- */
  ppg.output.num_frequency_steps =      i;
  ppg.output.dwell_time__ms_     =      ppg.input.rf_on_time__ms_
    + ppg.input.rf_off_time__ms_;
  ppg.output.num_dwell_times     =    ppg.input.num_rf_on_delays__dwell_times_
    + ppg.output.num_frequency_steps ;
  
  
  /*Assign the ODB input values to the delays*/                 
  time_slice       =    ppg.input.time_slice__ms_;
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_pulse          =    d_minimal;
  d_scalerpulse    =    d_pulse;
  d_freqpulse      =    d_pulse;
  d_ctime_on       =    d_minimal;         /*All delays must be defined, even if they are not used in the selected acq.mode,*/
  d_ctime_off      =    d_minimal;         /*because they are defined in each template. The perl compiler requires each delay to be */
  /*in the allowed range, otherwise the compilation will stop with an error message*/
  /*When a delay = 0 is encountered, the corresponding mode is selected and the delay is set to*/
  /*minimum delaytime*/ 
  
  
  d_countingtime   =    ppg.input.mcs_enable_gate__ms_;
  
  d_ms_predelai  =  ppg.input.mcs_enable_delay__ms_ - d_pulse;
  if (!rangecheck(d_ms_predelai,d_minimal,time_slice))
    {cm_msg(MERROR,"e2b_compute","Rangecheck-Error for d_ms_predelai = %f ",d_ms_predelai);
    cm_msg(MERROR,"e2b_compute","MCS enable delay (ms) must be > 2* Minimal delay (ms) ");
    return -1;
    }
  
  d_ms_postdelai =    ppg.output.dwell_time__ms_ -
    (ppg.input.mcs_enable_delay__ms_+d_countingtime);       
  if (!rangecheck(d_ms_postdelai,d_minimal,time_slice))
    {cm_msg(MERROR,"e2b_compute","Rangecheck-Error for d_ms_postdelai = %f ",d_ms_postdelai);
    cm_msg(MERROR,"e2b_compute","MCS enable delay + MCS enable gate  must be < RF on + RF off");
    return -1;
    }
  
  d_ms_predelay    =    ppg.input.mcs_enable_delay__ms_-d_pulse;
  if (!rangecheck(d_ms_predelay,d_minimal,time_slice))
    {cm_msg(MERROR,"e2b_compute","Rangecheck-Error for d_ms_predelay = %f ",d_ms_predelay);
    cm_msg(MERROR,"e2b_compute","MCS enable delay (ms) must be > 2* Minimal delay (ms) ");
    return -1;
    }
  
  d_rf_on          =    ppg.input.rf_on_time__ms_;
  
  /*The following part of the code prepares the check for the adiabacity. see below.*/
  n =   div (ppg.input.frequency_start__hz_, f_switch_boundary);
  ad_check = n.quot;
  n = div (ppg.input.frequency_stop__hz_ , f_switch_boundary);
  ad_check = ad_check - n.quot;
  
  d_rf_off         =    ppg.input.rf_off_time__ms_;
  /*With RF off = 0, the program assumes that the user wants to run adiabatic frequency sweeps   */
  /*Adiabacity is destroyed, when the frequency crosses any 100 kHz boundary (see PTS 160 manual)*/
  
  if ((d_rf_off == 0) && (ad_check < 0))
    {cm_msg(MERROR,"e2b_compute","Phase coherenz error: Crossing 100 kHz boundary with RF off time = 0  ");
    return -1;
    }
  
  d_dacs       =    ppg.input.daq_service_time__ms_;
  /* make sure daq service time is at least the minimal delay
     (now that ppg is started by software for Type 2 we may not need the daq service time) */
  if(d_dacs <=0)
    d_dacs = d_minimal;

  d_beam_off       =    ppg.input.beam_off_time__ms_ - d_dacs;
  if (d_beam_off < d_minimal)
    {
      d_beam_off = d_minimal;
      f = d_beam_off + d_dacs;
      cm_msg(MINFO,"e2b_compute","Time between two cycles (DAQ Service time + beam off time) = %f ms",f);
    }
  
  
  
  /* -- Selection of the matching template file -- */
  
  /* Filename: Experimentname + m1 +m2 +m3 +m4  .ppg */
  /* like      2b_              C   C   0   N   .ppg */
  
  
  /*Counting gate mode selection, m4*/
  
  if (ppg.input.mcs_enable_delay__ms_ > d_rf_on)
    {               
      /*Counting mode 2, counting gate in RF off */
      d_ms_predelay  = ppg.input.mcs_enable_delay__ms_ - d_rf_on;
      if (!rangecheck(d_ms_predelay,d_minimal,time_slice))
    {cm_msg(MERROR,"e2b_compute","Counting in RF off: Rangecheck-Error for d_ms_predelay = %f ",d_ms_predelay);
    cm_msg(MERROR,"e2b_compute","MCS enable delay (ms) must be >= RF on time + Minimal delay ");
    return -1;
    }
      
      d_ms_postdelay = d_rf_off - (d_ms_predelay+ d_countingtime);
      if (!rangecheck(d_ms_postdelay,d_minimal,time_slice))
    {cm_msg(MERROR,"e2b_compute","Rangecheck-Error for d_ms_postdelay = %f ",d_ms_postdelay);
    cm_msg(MERROR,"e2b_compute","MCS enable delay + counting gate must be < RF on + RF off + Minimal delay");
    return -1;
    }
      
      m4 = '2';
    }
  else {
    if (ppg.input.mcs_enable_delay__ms_+d_countingtime > d_rf_on)
      {                 
    /*Counting mode 3, counting gate over RF on-off transition*/
    d_ms_postdelay =  d_ms_postdelai;
    
    /*Split counting gate in RF on and RF off part*/
    d_ctime_on     =  d_rf_on - ppg.input.mcs_enable_delay__ms_;
    if (!rangecheck(d_ctime_on,d_minimal,time_slice))
      {cm_msg(MERROR,"e2b_compute","Counting during the RF on->off transition: Rangecheck-Error for d_ctime_on = %f ",d_ctime_on);
      cm_msg(MERROR,"e2b_compute","MCS enable delay must be < RF on - Minimal delay");
      return -1;
      }
    
    d_ctime_off    =  d_rf_off - d_ms_postdelay;
    if (!rangecheck(d_ctime_off,d_minimal,time_slice))
      {cm_msg(MERROR,"e2b_compute","Counting during the RF on->off transition: Rangecheck-Error for d_ctime_off = %f ",d_ctime_off);
      cm_msg(MERROR,"e2b_compute","MCS enable delay + counting gate must be >= RF on + Minimal delay");
      return -1;
      }
    
    m4 = '3';
      }
    else            
      {
    /*Counting mode 1, counting gate in RF on */
    d_ms_postdelay = d_rf_on - d_countingtime - ppg.input.mcs_enable_delay__ms_;
    if (!rangecheck(d_ms_postdelay,d_minimal,time_slice))
      {cm_msg(MERROR,"e2b_compute","Counting in RF on: Rangecheck-Error for d_ms_postdelay = %f ",d_ms_postdelay);
      cm_msg(MERROR,"e2b_compute","MCS enable delay + counting gate must be <= RF on - Minimal delay");
      return -1;
      }
    
    m4 = '1';
      }
    
  }
  
  /* -- Beam on off transition mode, m3 -- */
  if (ppg.input.e2b_num_beam_on_dwell_times > ppg.input.num_rf_on_delays__dwell_times_ +
      ppg.output.num_frequency_steps -1 )
    {
      cm_msg(MERROR,"e2b_compute","Problem with input parameters 'E2B Num beam on dwell times' & 'num RF on delays (dwell times)' ");
      cm_msg(MERROR,"e2b_compute","Num beam-on dwelltimes (%i) > num dwelltimes.",ppg.input.e2b_num_beam_on_dwell_times );
      cm_msg(MERROR,"e2b_compute","Num dwelltimes = Num frequencies (%i) + num RF on delays (%i)",ppg.output.num_frequency_steps,ppg.input.num_rf_on_delays__dwell_times_);
      return -1;
    }
  if (ppg.input.e2b_num_beam_on_dwell_times > ppg.input.num_rf_on_delays__dwell_times_)
    {
      /*Beam on off transition in the irradiation cycle*/
      m3 = '3';
      
      n_beam_on  = ppg.input.e2b_num_beam_on_dwell_times - ppg.input.num_rf_on_delays__dwell_times_ -1;
      if (n_beam_on < 0)
    {
      cm_msg(MERROR,"e2b_compute","Loop count zero in n_beam_on.");
      return -1;
    }
      
      n_beam_off = ppg.output.num_frequency_steps - n_beam_on -2;
      if (n_beam_off < 0)
    {
      cm_msg(MERROR,"e2b_compute","Loop count zero in n_beam_off.");
      cm_msg(MERROR,"e2b_compute","All dwelltimes in beam on mode. Please choose Experiment type 2a_.");
      return -1;
    }
    }
  if (ppg.input.e2b_num_beam_on_dwell_times == ppg.input.num_rf_on_delays__dwell_times_)
    {
      /*Beam on off transition at the end of the RF on delay*/
      m3 = '2';
      
      n_beam_on  = -1;                      /*Not used in this mode*/
      
      n_beam_off = ppg.output.num_frequency_steps -1;
      if (n_beam_off < 0)
    {
      cm_msg(MERROR,"e2b_compute","Loop count zero in n_beam_off.");
      cm_msg(MERROR,"e2b_compute","All dwelltimes in beam on mode. Please choose Experiment type 2a_.");
      return -1;
    }
    }
  if (ppg.input.e2b_num_beam_on_dwell_times < ppg.input.num_rf_on_delays__dwell_times_)
    {
      /*Beam on off transition in the RF on delay*/
      m3 = '1';
      n_beam_on  = -1;                      /*Not used in this mode*/
      
      n_beam_off = ppg.output.num_frequency_steps -1;
      if (n_beam_off < 0)
    {
      cm_msg(MERROR,"e2b_compute","Loop count zero in n_beam_off.");
      cm_msg(MERROR,"e2b_compute","All dwelltimes in beam on mode. Please choose Experiment type 2a_.");
      return -1;
    }
    }
  
  
  /* -- RF on delay selection, m2 -- */

  n_rfdelay    = ppg.input.num_rf_on_delays__dwell_times_ -1;
  n_rfd_nobeam = -1;
  
  if (n_rfdelay == -1)
    m2 = '0';     /*No RF on delay*/
  else
    m2 = 'P';     /*RF on delay present*/
  
  if (m3 == '1')
    {
      n_rfdelay    = ppg.input.e2b_num_beam_on_dwell_times - 1;
      if (n_rfdelay < 0)
    {cm_msg(MERROR,"e2b_compute","No beam on dwelltime, please define 'E2B Num beam on dwell times' > 0");
    return -1;
    }
      n_rfd_nobeam =   ppg.input.num_rf_on_delays__dwell_times_
    - ppg.input.e2b_num_beam_on_dwell_times - 1;
      if (n_rfd_nobeam < 0) n_rfd_nobeam = 0;
    }
  
  /* -- RF mode selection, m1 -- */
  
  if (d_rf_off == 0)
    {
      m1 = 'C';                   /*Continous irradiation*/
      d_rf_off = d_minimal;       /*Must be defined within the allowed range, see above*/
      ppg.output.vme_beam_control=TRUE;

      if (m4 != '1' )             /*no RF off time*/
    {cm_msg(MERROR,"e2b_compute","MCS Counting gate > RF on time, but no RF off time specified");
    return -1;
    }
      /* -> Counting in RF off is not possible without RF off time*/
    }
  
  else
    {
      m1 = 'P';                   /*Pulsed irradiation*/
      ppg.output.vme_beam_control = FALSE;
    }
  /*The number of beam precycles must be one or more*/
  
  n_precycles   = ppg.input.num_beam_precycles-1;
  if (n_precycles == -1)
    {
      cm_msg(MINFO,"e2b_compute","Loop count zero for beam precycles, set to one");
      n_precycles = 0;
    }
  
  /*The number of acquisition cycles must be one or more*/
  
  n_acq     = ppg.input.num_beam_acq_cycles-1;
  if (n_acq == -1)
    {
      cm_msg(MINFO,"e2b_compute","Loop count zero for acquisition cycles, set to one");
      n_acq = 0;
    }
  
  
  ppg.output.rf_on_time__ms_  = d_rf_on;
  ppg.output.rf_off_time__ms_ = d_rf_off;
 
  /* -- Writing the histogram information to the ODB (e2b) -- */
  
  i             = ppg.input.num_rf_on_delays__dwell_times_ + 1;
  for (j=0; j < ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.bin_zero [j]       = 1;
    fmdarc.histograms.first_good_bin [j] = i;
    }
  
  
  i                 = fmdarc.histograms.first_good_bin [1] + ppg.output.num_frequency_steps -1;
  for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
    fmdarc.histograms.last_good_bin [j] = i;
  
  if (m2 == 'P')
    {
      i = n_rfdelay +1;
      for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.first_background_bin [j] = 1;
    fmdarc.histograms.last_background_bin  [j] = i;
    }
    }
  else
    {
      for (j=0; j < ppg.input.num_type2_frontend_histograms  ; j ++)
    {fmdarc.histograms.first_background_bin [j] = 0;
    fmdarc.histograms.last_background_bin  [j] = 0;
    }
    }
  
  d_pre_beam_on    = (n_beam_on +1    + n_rfdelay  +1)*(d_rf_on+d_rf_off);
  d_pre_beam_off   = (n_rfd_nobeam +1 + n_beam_off +1)*(d_rf_on+d_rf_off);
  ppg.output.beam_on_time__ms_   =  d_pre_beam_on;
  
  
 /* write some information to mdarc area for e2b */
 fmdarc.histograms.number_defined = ppg.input.num_type2_frontend_histograms ; /*  number of  histograms as written by frontend */
 fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

 if( ! ppg.sis_test_mode.test_mode)  /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
   {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
     if(debug)printf("SIS test mode is enabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.num_bins;
     for (j=0; j <  ppg.input.num_type2_frontend_histograms; j ++)
       {
     fmdarc.histograms.last_good_bin    [j] =  fmdarc.histograms.num_bins ;
     fmdarc.histograms.last_background_bin  [j] = 0;
       }
   }
 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number frontend histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins            : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)           : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
  
  /* -- Writing the rulefile for the substitution from template-file to the pulse-script -- */
  cm_msg(MINFO,"e2b_compute","Computing values and writing to the ODB done, writing rulefile.");
  
  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e2b_compute: about to open rulefile: %s\n",rulefile_name);

  rulef = fopen(rulefile_name, "w");
  if (rulef) {
    sprintf(str,"%s_%c%c%c%c%s%s",ppg_mode,m1,m2,m3,m4,".ppg","\n"); /* add an underscore after ppg_mode */
     {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
    }
    if (debug) printf("e2b_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);

    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);
    
    sprintf(str,"%s%s%s%f%s","d_scalerpulse=0","%","d_scalerpulse=",d_scalerpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_freqpulse=0","%","d_freqpulse=",d_freqpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pulse=0","%","d_pulse=",d_pulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pre_beam_on=0","%","d_pre_beam_on=",d_pre_beam_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pre_beam_off=0","%","d_pre_beam_off=",d_pre_beam_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_predelai=0","%","d_ms_predelai=",d_ms_predelai,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_predelay=0","%","d_ms_predelay=",d_ms_predelay,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_postdelai=0","%","d_ms_postdelai=",d_ms_postdelai,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_postdelay=0","%","d_ms_postdelay=",d_ms_postdelay,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_countingtime=0","%","d_countingtime=",d_countingtime,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_rf_on=0","%","d_rf_on=",d_rf_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_rf_off=0","%","d_rf_off=",d_rf_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ctime_on=0","%","d_ctime_on=",d_ctime_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ctime_off=0","%","d_ctime_off=",d_ctime_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dacs=0","%","d_dacs=",d_dacs,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_beam_off=0","%","d_beam_off=",d_beam_off,"ms\n");
    fputs(str,rulef);
    
    
    sprintf(str,"%s%s%d%s","n_rfd_nobeam","%",n_rfd_nobeam,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_acq","%",n_acq,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_beam_on","%",n_beam_on,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_beam_off","%",n_beam_off,"\n");
    fputs(str,rulef);   
    sprintf(str,"%s%s%d%s","n_precycles","%",n_precycles,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_rfdelay","%",n_rfdelay,"\n");
    fputs(str,rulef);
    
    fclose (rulef);
    
  }
  else  {
    cm_msg(MERROR,"e2b_compute","can't open rule file %s",rulefile_name);
    return -1;
  }
  cm_msg(MINFO,"e2b_compute","Rulefile %s written.",rulefile_name);
  
  return 1;
#endif
}

/*------------------------------------------------------------------------------*/
/*mode 2c multislice freq table builder for rf_config by Christoph Bommas 11-2000*/
int build_f_table2c(char *ppg_mode,int n_f_slices)
{
  FILE  *freqfile;
  char  str[256];
  char  fstr[12];

  DWORD freq;
  DWORD f_inc   ;
  DWORD f_slice_width;
  WORD  i,j,n_half;
  char  * f_bsd;
  div_t n;
  WORD  ad_test;


  /*Create frequency table */
  f_slice_width = ppg.input.freq_single_slice_width__hz_;
  n         = div (n_f_slices, 2);
  n_half    = n.quot;

  i = 0;

  /*Multiple slices frequencies start with lowest freq.*/
  
  /*Even number of small freq. slices, center freq. of the */
  /*large slice between two small slices */
  if (n.rem == 0)
    {freq      =  ppg.input.frequency_start__hz_
       - (n.quot - .5)*f_slice_width;
    
    }
  /*Odd number of small freq. slices, center freq. of the */
  /*large slice on middle small slice */
  else
    {freq  =  ppg.input.frequency_start__hz_
       - (n.quot)*f_slice_width;
    }
  
  /*Freq. spacing of the large slices*/
  f_inc =  ppg.input.frequency_increment__hz_;
  
  /*Selecting pathname for the freq. output file*/
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,".fsc");
  
  /*Main part, writing of the frequency table*/
  
  if (freqfile = fopen(str,"w"))
    
    {
      do   /*Steps the start freq of the large slices up*/
    {
      j = 0;
      
      do    /*Steps the freq within the large slices up -> 180 pulse*/
        {
          
          sprintf(str,"%u%s",freq,"\n");
          fputs(str,freqfile);
          freq = freq + f_slice_width;
          j ++;
        }
      while (j < n_f_slices);
      
      do    /*Steps the freq within the large slices down -> 360 pulse*/
        {
          freq = freq - f_slice_width;
          sprintf(str,"%u%s",freq,"\n");
          fputs(str,freqfile);
          
          j --;
        }
      while (j > 0);
      
      freq = freq + f_inc;
    
          i ++;  
      
    }
      while (freq <= ppg.input.frequency_stop__hz_);
      
      /*Close the table*/
      sprintf(str,"%u%s",i,"\n");
      if (!(1))fputs(str,freqfile);
      fclose(freqfile);
      
      /*Write output to ODB, recalculate slicecentre*/
      ppg.output.num_frequency_steps =  i;
      ppg.output.frequency_stop__hz_ =  freq - f_inc
    + (f_slice_width * (n.quot - .5 + n.rem));
      
      return i;
    }
  else
    {cm_msg(MERROR,"Build_f_table2c","Error opening frequency file %s ",str);
    return -1;
    }
}


/*mode 2c selection function for rf_config by Christoph Bommas 11-2000*/
/*--------------------------------------------------------------------*/
INT e2c_compute(char *ppg_mode)
{
  
  char    *modus;
  char   m1,m2,m3,m4;
  FILE   *rulef;
  char   str [256];
  char   str2[256];
  char   ctemp[5];
  char  rulefile_name[256];

  double d_nextfpulse     ;
  double d_scalerpulse    ;
  double d_pulse          ;
  double d_RFpulse        ;
  double d_ms_postdelai ;
  double d_ms_predelay    ;
  double d_ms_predelai  ;
  double d_prebeam_off    ;
  double d_prebeam_on     ;
  double d_ms_postdelay   ;
  double d_countingtime   ;
  double d_beam_off       ;
  double d_beam_on        ;
  double d_dacs           ;
  double d_minimal    ;
  double d_flip180        ;
  double d_flip360        ;
  double d_f_slice        ;
  double d_slice_internal ;

  DWORD  n_acq            ;
  DWORD  n_rf_on          ;
  DWORD  n_f_slices       ;
  DWORD  n_rfdelay        ;    /*RF on delay in dwelltimes*/

  DWORD  f_inc            ;
  double time_slice;
  int i,j;

  double f;

#ifdef POL
  /* POL does not need 2c*/
  printf("e2c_compute: ppg mode %s is not supported for POL expt\n",ppg_mode);
  return -1;
#else
  if(debug)printf("e2c_compute is starting with ppg_mode %s\n",ppg_mode);

  /* -- Calculate the frequency table -- */
  n_f_slices     =    ppg.input.num_freq_slices;
  if(n_f_slices < 1)
    { cm_msg(MERROR,"e2c_compute","Invalid number frequency slices %i ",n_f_slices);
    return -1;
    }
#ifdef PSM

  /* check only one gate parameter is selected */
  //  if(check_psm_gate() != 0)return -1;  2c not supported for PSM
  // if(check_psm_quad(ppg_mode)!= 0) // calls build_iq_table_psm for quad mode 
  //{
  //  cm_msg(MERROR,"e2c_compute","Error return from check_psm_quad ");
  //  return -1;
  //}
  printf("e2c_compute: build_f_table2c for PSM does not exist yet\n");
  return (-1);

#else
  if(debug)printf("Calling build_f_table2c\n");
  i = build_f_table2c(ppg_mode,n_f_slices);
#endif
  /*The build_f_table2c should work for single slice mode too. */
  /*The table includes pairs of the same frequency (11-22-33..)*/

  
  if ( i == -1 )
    {cm_msg(MERROR,"e2c_compute","Invalid number frequency steps %i ",i);
    return -1;
    }  
  
  /*Assign the ODB input values to the delays*/                 
  /*All delays must be defined, even if they are not used in the selected acq.mode,*/
  /*because they are defined in each template. The perl compiler requires each delay to be */
  /*in the allowed range, otherwise the compilation will stop with an error message*/
  /*When a delay = 0 is encountered, the corresponding mode is selected and the delay is set to*/
  /*minimum delaytime*/
  time_slice       =    ppg.input.time_slice__ms_;  
  d_minimal        =    ppg.input.minimal_delay__ms_;
  d_pulse          =    d_minimal;
  d_RFpulse        =    ppg.input.f_select_pulselength__ms_;
  d_scalerpulse    =    d_minimal;
  d_nextfpulse     =    d_minimal;
  
  d_prebeam_off    =    d_minimal;
  d_prebeam_on     =    ppg.input.prebeam_on_time__ms_;
  /**!!!!! Following variable is borrowed from E2B...  (RP) !!!!*/
  /* too confusing - this variable is in dwell times but expecting ms so make a new variable */  
  /* d_beam_on     =    ppg.input.e2b_num_beam_on_dwell_times; */
  d_beam_on        =    ppg.input.e2c_beam_on_time__ms_;
  /* too confusing - make a new variable */  
  d_beam_off       =    ppg.input.beam_off_time__ms_;
  d_flip360        =    ppg.input.flip_360_delay__ms_;
  d_flip180        =    ppg.input.flip_180_delay__ms_;
  d_f_slice        =    ppg.input.f_slice_internal_delay__ms_ - d_nextfpulse;
  d_countingtime   =    ppg.input.mcs_enable_gate__ms_;
  d_ms_predelay    =    ppg.input.mcs_enable_delay__ms_;
  d_dacs       =    ppg.input.daq_service_time__ms_;
  d_slice_internal =    ppg.input.f_slice_internal_delay__ms_;

  /* make sure daq service time is at least the minimal delay
     (now that ppg is started by software for Type 2 we may not need the daq service time) */
  if(d_dacs <=0)
    d_dacs = d_minimal;
  
  if(debug)printf("Reading all parameters\n");  
  if (d_slice_internal < d_minimal)
    {cm_msg(MINFO,"e2c_compute","Rangecheck-Error for f_slice_internal_delay = %f ",d_slice_internal);
    cm_msg(MINFO,"e2c_compute","Set to minimal delay = %f",d_minimal);
    d_slice_internal = d_minimal;
    }
  if (!rangecheck(d_ms_predelay,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_ms_predelay = %f ",d_ms_predelay);
    cm_msg(MERROR,"e2c_compute","MCS enable delay (ms) must be >  Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_RFpulse,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_RFpulse = %f ",d_RFpulse);
    cm_msg(MERROR,"e2c_compute","f_select_pulselength (ms) must be >  Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_countingtime,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_countingtime = %f ",d_countingtime);
    cm_msg(MERROR,"e2c_compute","MCS enable gate (ms) must be >  Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_f_slice,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_f_slice = %f ",d_f_slice);
    cm_msg(MERROR,"e2c_compute","f_slice_internal_delay must be >  2 * Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_flip180,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_flip180 = %f ",d_flip180);
    cm_msg(MERROR,"e2c_compute","flip_180_delay must be > Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_flip360,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_flip360 = %f ",d_flip360);
    cm_msg(MERROR,"e2c_compute","flip_360_delay must be > Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_prebeam_on,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_prebeam_on = %f ",d_prebeam_on);
    cm_msg(MERROR,"e2c_compute","prebeam_on_time must be > Minimal delay (ms) ");
    return -1;
    }
  
    if(  ppg.input.num_rf_on_delays__dwell_times_ < 0)
    {cm_msg(MERROR,"e2c_compute","Invalid RF on delays (dwell times) (%d) must be >= 0 ",ppg.input.num_rf_on_delays__dwell_times_);
    return -1;
    }
  
  
  n_rfdelay        =    ppg.input.num_rf_on_delays__dwell_times_ - 1;
  
  /* f_slice_width    =    ppg.input.freq_single_slice_width__hz_; */
  
  /* -- Write computet output values to the ODB -- */
  ppg.output.num_frequency_steps =      i;
  ppg.output.dwell_time__ms_     =      d_flip180 + d_flip360 + 2 * (d_RFpulse * n_f_slices
                                     + d_slice_internal * (n_f_slices-1));
  ppg.output.num_dwell_times     =      ppg.input.num_rf_on_delays__dwell_times_
    + ppg.output.num_frequency_steps ;
  
  /* -- Selection of the matching template file -- */
  
  
  /* Filename: Experimentname + m1 +m2 +m3 +m4  .ppg */
  /* like      2c_              C   M   0   1   .ppg */
  /*                            P   S   P   2        */
  /*                            x   x   x   3        */
  

  
  /* -- Beam mode, m1 -- */
  m1 = ppg.input.beam_mode;
  if  (m1 == 'P')
    {
      if (!rangecheck(d_beam_on,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for beam_on_time = %f ",d_beam_on);
    cm_msg(MERROR,"e2c_compute","Must be > minimal delay");
    return -1;
    }
      if (!rangecheck(d_beam_off,d_minimal,time_slice))
    {cm_msg(MINFO,"e2c_compute","Rangecheck-Error for beam off time = %f ",d_beam_off);
    cm_msg(MINFO,"e2c_compute","Set to minimal delay = %f",d_minimal);
    return -1;
    }
      d_beam_off = d_minimal;
      ppg.output.vme_beam_control = FALSE;
    }
   
  else if  (m1 == 'C') 
    {
      d_beam_off = d_minimal;  /*Delays in the template-header must be defined within the allowed range,*/
      d_beam_on  = d_minimal;  /* even when they are not used in the script*/      
      ppg.output.vme_beam_control = TRUE;
    }
  
  else
    {
      cm_msg(MERROR,"e2c_compute","Invalid beam mode (%c) ",ppg.input.beam_mode);
      cm_msg(MERROR,"e2c_compute","Please use C (Continous beam) or P (Pulsed beam) ");
      return -1;
    }
  
  /* -- Number of freq slices, m2 -- */
   
   if (n_f_slices == 1)
    m2 = 'S';     /*Single slice mode*/
  else
    m2 = 'M';     /*Multiple slice mode*/
  
  /* -- RF on delay, m3 -- */
  
  if (n_rfdelay == -1)
    m3 = '0';     /*No RF on delay*/
  else
    m3 = 'P';     /*RF on delay present*/
  
  /* -- Counting gate mode, m4 -- */
  /* The delays for the rf_on_delay time (Counting without RF) are calculated dependant               */
  /* of the position of the counting gate - after 180 or after 360 puls(es)to keep timing consistent  */
  
  m4 = ppg.input.counting_mode;
  
  if (m4 == '2' )
    {
      sprintf(ctemp,"360");
      d_ms_postdelay   = d_flip360 - d_ms_predelay - d_countingtime - d_scalerpulse ;
    
      d_ms_predelai  = d_ms_predelay + 2 *(d_RFpulse * n_f_slices + d_slice_internal * (n_f_slices-1))
    + d_flip180 + d_scalerpulse ;
      d_ms_postdelai = d_ms_postdelay;
    }
  else if  ( m4 == '1')
    {
      sprintf(ctemp,"180");
      d_ms_postdelay   = d_flip180 - d_ms_predelay - d_countingtime;
      d_ms_predelai  = d_ms_predelay  + (d_RFpulse * n_f_slices + d_slice_internal * (n_f_slices-1));
      d_ms_postdelai = d_ms_postdelay + (d_RFpulse * n_f_slices + d_slice_internal * (n_f_slices-1))
    + d_flip360 ;
    }
   else if  ( m4 == '3')
    {
      sprintf(ctemp,"180");
      d_ms_postdelay   = d_flip180 - d_ms_predelay - d_countingtime - d_scalerpulse ;
      d_ms_predelai  = d_ms_predelay  + (d_RFpulse * n_f_slices + d_slice_internal * (n_f_slices-1));
      d_ms_postdelai = d_ms_postdelay + (d_RFpulse * n_f_slices + d_slice_internal * (n_f_slices-1))
    + d_flip180 ;
      ppg.output.num_dwell_times     =    2* ( ppg.input.num_rf_on_delays__dwell_times_
                           + ppg.output.num_frequency_steps) ;
      ppg.output.dwell_time__ms_     =      d_flip180  +  (d_RFpulse * n_f_slices
                                         + d_slice_internal * (n_f_slices-1));    
    }
  else
    {
      cm_msg(MERROR,"e2c_compute","Invalid counting mode ",ppg.input.counting_mode);
      cm_msg(MERROR,"e2c_compute","Please use 1 (Count after first 180) or 2 (after second 180) or 3 (after both)");
      return -1;
    }
  
  if (!rangecheck(d_ms_postdelay,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Counting after flip_%s_delay: Rangecheck-Error for d_ms_postdelay = %f ",ctemp,d_ms_postdelay);
    cm_msg(MERROR,"e2c_compute","Counting Time + ms_predelay must be <= flip_%s_delay -2 * d_minimal",ctemp);
    return -1;
    }
  if (!rangecheck(d_ms_predelai,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_ms_predelai = %f ",d_ms_predelai);
    cm_msg(MERROR,"e2c_compute","MCS enable delay (ms) must be > 2* Minimal delay (ms) ");
    return -1;
    }
  if (!rangecheck(d_ms_postdelai,d_minimal,time_slice))
    {cm_msg(MERROR,"e2c_compute","Rangecheck-Error for d_ms_postdelai = %f ",d_ms_postdelai);
    cm_msg(MERROR,"e2c_compute","d_ms_postdelai must be > Minimal delay (ms) ");
    return -1;
    }
  
  
  
  /*The number of acquisition cycles must be one or more*/
  
  n_acq     = ppg.input.num_beam_acq_cycles-1;
  if (n_acq < 0)
    {
      cm_msg(MINFO,"e2c_compute","Loop count zero for acquisition cycles, set to one");
      n_acq = 0;
    }
  
  n_rf_on   = ppg.output.num_frequency_steps-2;
  if (n_rf_on == -1)
    {
      cm_msg(MERROR,"e2c_compute","Low Loop count for n_rf_on, number of frequecies should be at least 2.");
      return -1;
    }
  
  /* -- Writing the histogram information to the ODB (2c) -- */
  
  i             = ppg.input.num_rf_on_delays__dwell_times_ + 1;
  for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.bin_zero [j]       = 1;
    fmdarc.histograms.first_good_bin [j] = i;
    }
  
  
  i                 = fmdarc.histograms.first_good_bin [1] + ppg.output.num_frequency_steps -1;
  for (j=0; j < ppg.input.num_type2_frontend_histograms  ; j ++)
    fmdarc.histograms.last_good_bin [j] = i;
  
  if (m3 == 'P')
    {
      
      i = n_rfdelay + 1;
      for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.first_background_bin [j] = 1;
    fmdarc.histograms.last_background_bin  [j] = i;
    }
    }
  else
    {
      for (j=0; j < ppg.input.num_type2_frontend_histograms ; j ++)
    {fmdarc.histograms.first_background_bin [j] = 0;
    fmdarc.histograms.last_background_bin  [j] = 0;
    }
    }
  /*The scripts do loopcount + 2 frequency slices, so the number must be reduced by 2 */
  n_f_slices       =    ppg.input.num_freq_slices - 2;
  if (!(n_f_slices+2))
    {cm_msg(MERROR,"e2c_compute","Error for num_freq_slices = %i ",n_f_slices+1);
    cm_msg(MERROR,"e2c_compute","num_freq_slices must be > 0, integer");
    return -1;
    }
  
  ppg.output.rf_on_time__ms_  = 0; /* not used */
  ppg.output.rf_off_time__ms_ = 0;

 /* write some information to mdarc area for e2c */
 fmdarc.histograms.number_defined = ppg.input.num_type2_frontend_histograms ; /*  number of  histograms as written by frontend */
 fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */

 if( ! ppg.sis_test_mode.test_mode)  /* real mode (front panel signals, uses ppg and fsc */
   /* The number of bins as calculated by rf_config */
   {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.output.num_dwell_times ;
   }
 else   /* sis internal  test mode  */  
   {
     if(debug)printf("SIS test mode is enabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.dwell_time__ms_;
     fmdarc.histograms.num_bins = ppg.sis_test_mode.num_bins;
     for (j=0; j <  ppg.input.num_type2_frontend_histograms; j ++)
       {
     fmdarc.histograms.last_good_bin    [j] =  fmdarc.histograms.num_bins ;
     fmdarc.histograms.last_background_bin  [j] = 0;
       }
   }
 if(debug)
   {
     printf("Writing values to mdarc area:\n");
     printf("  Number frontend histograms: %d\n", fmdarc.histograms.number_defined);
     printf("  Number of bins            : %d\n", fmdarc.histograms.num_bins);
     printf("  Dwell time (ms)           : %f\n", fmdarc.histograms.dwell_time__ms_);
   }
  

  /* -- Writing the rulefile for the substitution from template-file to the pulse-script -- */
  cm_msg(MINFO,"e2c_compute","Computing values and writing to the ODB done, writing rulefile.");
  
  sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");
  if(debug)printf("e2c_compute: about to open rulefile: %s\n",rulefile_name);
  
  rulef = fopen(rulefile_name, "w");
  if (rulef) {
    
    
    sprintf(str,"%s_%c%c%c%c%s%s",ppg_mode,m1,m2,m3,m4,".ppg","\n"); /* add an underscore after ppg_mode */
    {
      int len,size;
     
      len=strlen(str);
      size=sizeof(ppg.output.ppg_template);
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file
    }
    if (debug) printf("e2c_compute: writing ppg template file name (%s) into rulefile\n", ppg.output.ppg_template);

    fputs(str,rulef);
    sprintf(str,"%s%s",ppg_mode,".ppg\n");
    fputs(str,rulef);
    
    sprintf(str,"%s%s%s%f%s","d_scalerpulse=0","%","d_scalerpulse=",d_scalerpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_nxtfpulse=0","%","d_nxtfpulse=",d_nextfpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pulse=0","%","d_pulse=",d_pulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_RFpulse=0","%","d_RFpulse=",d_RFpulse,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pre_beam_on=0","%","d_pre_beam_on=",d_prebeam_on,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_pre_beam_off=0","%","d_pre_beam_off=",d_prebeam_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_predelai=0","%","d_ms_predelai=",d_ms_predelai,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_predelay=0","%","d_ms_predelay=",d_ms_predelay,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_postdelai=0","%","d_ms_postdelai=",d_ms_postdelai,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_ms_postdelay=0","%","d_ms_postdelay=",d_ms_postdelay,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_countingtime=0","%","d_countingtime=",d_countingtime,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_flip180=0","%","d_flip180=",d_flip180,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_flip360=0","%","d_flip360=",d_flip360,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_slice_internal=0","%","d_slice_internal=",d_slice_internal,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_dacs=0","%","d_dacs=",d_dacs,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_beam_off=0","%","d_beam_off=",d_beam_off,"ms\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%s%f%s","d_beam_on=0","%","d_beam_on=",d_beam_on,"ms\n");
    fputs(str,rulef);
    
    sprintf(str,"%s%s%d%s","n_rf_on","%",n_rf_on,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_acq","%",n_acq,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_slices","%",n_f_slices,"\n");
    fputs(str,rulef);
    sprintf(str,"%s%s%d%s","n_rfdelay","%",n_rfdelay,"\n");
    fputs(str,rulef);
    
    fclose (rulef);
    
  }
  else  {
    cm_msg(MERROR,"e2c_compute","can't open rule file %s",rulefile_name);
    return -1;
  }
  cm_msg(MINFO,"e2c_compute","Rulefile %s written.",rulefile_name);
  
  return 1;
#endif
}

INT e2d_compute(char *ppg_mode)
{
  INT i,status;
#ifdef POL
  /* POL does not need 2d */
  printf("e2d_compute: ppg mode %s is not supported for POL expt\n",ppg_mode);
  return -1;
#else
  printf("Mode 2d : using 1b_compute \n");
  status = e1b_compute(ppg_mode); // identical to e1b
  if (status <= 0)
    {
      printf("e2d_compute: error returned from e1b_compute\n");
      return(status);
    }
  return 1;
#endif
}

/* e2e_compute */                                                                                                    
INT e2e_compute(char *ppg_mode)                                                                                      
{                                                                                                                    
  /*                                                                                                                 
Scaler data:                                                                                                         
nprebins |  ndepth nRF  ndepth nRF  ndepth nRF.........nRF  ndepth | npostbins                                       
bins     |  bins   bins  bins  bins  bins  bins        bins bins   | bins                                            
                                                                                                                     
Histo data:                                                                                                          
nprebins | {ndepth nRF ndepth} | {ndepth nRF  ndepth} |... {ndepth nRF ndepth} | npostbins                           
bins     | {bins   bins bins } | {bins   bins  bins } |... {bins   bins bins } | bins                                
                                                                                                                     
Scaler array structure...                                                                                            
ch 0 bin 0, ch 1 bin 0, ch 2 bin 0 ....... ch N bin 0,                                                               
ch 0 bin 1, ch 1 bin 1, ch 2 bin 1.........ch N bin 1,                                                               
ch 0 bin 2, ch 1 bin 2, ch 2 bin 2.........ch N bin 2,                                                               
.........................                                                                                            
ch 0 bin M, ch 1 bin M, ch 2 bin M.........ch N bin M                                                                
                                                                                                                     
Histo structure:                                                                                                     
ch 0 bin 0-N                                                                                                         
ch 1 bin 0-N                                                                                                         
etc.                                                                                                                 
(except there are more histo channels than scaler due to repeated ndepth data)                                       
  */ 
  char m1,m2;
  FILE *rulef;                                                                                                                                                           
  char  str [256];                                                                                                                                                                                                                                                                                                              
  char  rulefile_name[256];                                                                                                                 
  double    d_mcs_pulse       ;// mimnimal pulse used for multichannel scalar next pulse 
  double    d_fsc_pulse       ;// mimnimal pulse used for fsc strobe                                                                                                                                           
  double    d_pulse          ;// mimnimal pulse used for multichannel scalar next & fsc strobe                                                                                                                                              
  /* the _ppgs means that it is the parameter for the ppg output script file.
     Loop parameters like n_** will usually be one less than the input values becasue the
     ppg always goes through a single loop before checking the End Loop condition.
     Delay values that require an mcs next pulse and/or a fcd strobe pulse will be reduced
     by d_minimal, since these actions + the delay correspond to two delays in the ppg script
     the first of which is d_minimal
  */
  double    d_dwell_ppgs     ; //corrected dwell time = rf on time for beam on measurements  
  double    d_dwell_t1_ppgs  ; //dwell time for beam off T1 measurements                                                                                                                                                                                                                
  double    d_rfon_ppgs     ; //corrected rfon time
  double    d_dacs           ; //service request time
  double    d_minimal   ;   //minimal pulse width for ppg
  double    d_beam_off  ;   //used to calculate/select the ppg mode
  DWORD     n_rf_delay_ppgs  ; 
  DWORD     n_freqs_ppgs     ;
  DWORD     n_pp_dts_ppgs    ;
  DWORD     n_beam_on_postF_ppgs ;
  DWORD     n_beam_off_ppgs  ; 
  double    time_slice  ;                                                                     
  INT num_sbins_ch;
  INT i,j,status;                                                                                                      
#ifdef POL                                                                                                           
  /* POL does not need 2e */                                                                                         
  printf("e2e_compute: ppg mode %s is not supported for POL expt\n",ppg_mode);                                       
  return -1;                                                                                                         
#else
                                                                                                               
#ifdef PSM                                                             
  /* check only one gate parameter is selected */                                                                    
  printf("calling check_psm_gate()\n");                                                                              
  if(check_psm_gate() != 0)                                                                                          
    {                                                                                                                
      cm_msg(MERROR,"e2e_compute","Error return from check_psm_gate ");                                              
      return -1;                                                                                                     
    }                                                                                                                
  if(check_psm_quad(ppg_mode)!= 0) // calls build_iq_table_psm for quad mode                                         
    {                                                                                                                
      cm_msg(MERROR,"e2e_compute","Error return from check_psm_quad ");                                              
      return -1;                                                                                                     
    }                                                                                                                
                                                                                                                     
                                                                                                                     
  /* -- Calculate the frequency table (PSM) -- */                                                                    
  i = build_f_table_psm(ppg_mode);                                                                                   
  if ( i == -1 )                                                                                                     
    {                                                                                                                
      cm_msg(MERROR,"e2e_compute","Error return from build_f_table_psm ");                                           
      return -1;                                                                                                     
    }                                                                                                                
#else                                                                                                                
  /* -- Calculate the frequency table (FSC)-- */                                                                     
  i = build_f_table(ppg_mode);                                                                                       
  if ( i == -1 )                                                                                                     
    {cm_msg(MERROR,"e2e_compute","Invalid number frequency steps %i ",i);                                            
    return -1;                                                                                                       
    }                                                                                                                
#endif                                                                                                               
                                                                                                                     
ppg.output.num_frequency_steps =      i;                                                                             
n_freqs_ppgs = ppg.output.num_frequency_steps - 2; 

/*  n_freqs for the ppg script must the the number of freqs -1, must check this statement ??? */ 

 /*  input parameters from user interface & odb                                                                                      
     ppg.input.e2e_num_dwelltimes_per_freq                                                                           
     ppg.input.e2e_num_rf_dwelltimes_per_freq   (fixed at 1)                                                         
     ppg.input.e2e_num_postrfbeamon dwelltimes                                                                      
     ppg.input.beam_off_time__ms_  
     ppg.input.num_rf_on_delays__dwell_times_                                                                                
 
     output parameters  (calculated or otherwise)                                                                                                              
     ppg.output.e2e_num_beam_off_dwelltimes     
     ppg.output.num_dwell_times
     ppg.output.dwell_time__ms_
     ppg.output.beam_on_time__ms_ 
     ppg.output.num_frequency_steps
     ppg.output.rf_on_time__ms
     ppg.output.rf_off_time__ms_
  */
                                                               
/* This value is fixed at 1 for e2e */
 if(ppg.input.e2e_num_rf_dwelltimes_per_freq != 1)                                                                   
   {                                                                     
     ppg.input.e2e_num_rf_dwelltimes_per_freq=1;                              
   }                                                                                                                 
                                                                                                                                                         
 /*Assign the ODB input values to the delays*/                                                                                                                            
  time_slice       =    ppg.input.time_slice__ms_;                                                                                                                       
  d_minimal        =    ppg.input.minimal_delay__ms_;                                                                                                                    
  d_pulse          =    d_minimal;                                                                                                                                       
  d_mcs_pulse      =    d_pulse;                                                                                                                                         
  d_fsc_pulse      =    d_pulse;  
  d_rfon_ppgs      =    ppg.input.rf_on_time__ms_ - d_minimal;
  d_dwell_ppgs     =    d_rfon_ppgs;
  d_dwell_t1_ppgs  =    d_dwell_ppgs;
  d_dacs           =    ppg.input.daq_service_time__ms_;
  d_beam_off       =    ppg.input.beam_off_time__ms_;                                                                                                                                      
                                                               
  /* check the integrity of the rf_on time, it must be at least 2xdm_minimal, since delays like
  d_dwell and d_rf_on are corrected (reduced by d_minimal) before being sent to the ppg since they
  are comprised of a minimal delay (that also contains an mcs next and/or fsc pulse) pluse a corrected 
  (i.e. shortened) delay to complete the dedlay interval. */
  
  if (d_rfon_ppgs < d_minimal)
     {
      cm_msg(MERROR,"e2e_compute","rf_on time must be at least 2*d_minimal, i.e. .001ms ");                                                                  
      return -1;                                                                                                                                                                  
     }  
  if ( ppg.input.num_rf_on_delays__dwell_times_ < 2)
    {
      cm_msg(MERROR,"e2e_compute","number of rf on delays (%d) must be at least 2 dwelltimes ",
	     ppg.input.num_rf_on_delays__dwell_times_ );                                                                  
      return -1;                                                                                                                                                                  
    }  

     n_rf_delay_ppgs = ppg.input.num_rf_on_delays__dwell_times_ - 2;
  if (n_rf_delay_ppgs < 0)
     { 
      cm_msg(MERROR,"e2e_compute","number of rf delay background bins must be at least 2");                                                                  
      return -1;
      }
                                                                                                                                                                             d_dacs       =    ppg.input.daq_service_time__ms_;                                                                                                                     
     n_beam_on_postF_ppgs = ppg.input.e2e_num_postrfbeamon_dwelltimes -2;
  if ( n_beam_on_postF_ppgs < 0)
     { 
      cm_msg(MERROR,"e2e_compute","number of post freq scan beam on dwell times must be at least 2 ");                                                                  
      return -1;
      }
      
  if ( ppg.input.e2e_num_dwelltimes_per_freq < 2 )
     { 
      cm_msg(MERROR,"e2e_compute","number of ppg dwell times/freq (including the RF pulse) must be at least 2 ");                                                                  
      return -1;
      }
     
  if (d_dacs <= 0) d_dacs == d_minimal;
  
  /* calculate the ppg mode/script; 2e_&m1&m2: there are four of them 
  
                         beam off time 
                      =0               >=0
     
  2 ppg dwts/freq    2e_C1            2e_P1
 >2 ppg dwts.freq    2e_C2            2e_P2     
  */                                                             
                                                                                                   
      
      m1 = 'C' ;
      ppg.output.e2e_num_beam_off_dwelltimes = 0;
      n_beam_off_ppgs = 0;
      ppg.output.rf_off_time__ms_ = 0;
      ppg.output.vme_beam_control = TRUE;

  if (d_beam_off > 0) 
      {
      m1 = 'P';
      if (d_beam_off > 2*ppg.input.rf_on_time__ms_)n_beam_off_ppgs = (d_beam_off/ppg.input.rf_on_time__ms_) - 1 ;  
         ppg.output.e2e_num_beam_off_dwelltimes =  n_beam_off_ppgs + 2 ;
         ppg.output.rf_off_time__ms_ = ppg.output.e2e_num_beam_off_dwelltimes * (d_dwell_t1_ppgs + d_minimal);
	 ppg.output.vme_beam_control = FALSE;
      }
      
      m2 = '1' ;   
      n_pp_dts_ppgs = 0;
  if (ppg.input.e2e_num_dwelltimes_per_freq > 2)
      { m2 = '2';
        n_pp_dts_ppgs = ppg.input.e2e_num_dwelltimes_per_freq -3 ;
        if (n_pp_dts_ppgs < 0 ) 
            { cm_msg(MERROR,"e2e_compute","n_pp_dts_ppgs count inconsistent with mode, check code ");                                                                  
               return -1;
            }
      }
  /*  assign the remainder of the output parameters  */
  
     /* ppg.output.e2e_num_beam_off_dwelltimes , assigned in m1 selection above */ 
     /* ppg.output.num_frequency_steps, assigned after freq table calculation */   
     ppg.output.num_dwell_times = ppg.input.num_rf_on_delays__dwell_times_
                                 + ppg.output.num_frequency_steps * ppg.input.e2e_num_dwelltimes_per_freq
                                 +  ppg.input.e2e_num_dwelltimes_per_freq - 1            
                                 + ppg.input.e2e_num_postrfbeamon_dwelltimes
                                 + ppg.output.e2e_num_beam_off_dwelltimes ;
     ppg.output.dwell_time__ms_ = ppg.input.rf_on_time__ms_;
     ppg.output.beam_on_time__ms_ = (ppg.input.num_rf_on_delays__dwell_times_
                                    + ppg.output.num_frequency_steps * ppg.input.e2e_num_dwelltimes_per_freq
                                    +  ppg.input.e2e_num_dwelltimes_per_freq - 1 
                                    + ppg.input.e2e_num_postrfbeamon_dwelltimes)* ppg.output.dwell_time__ms_ ;
     ppg.output.rf_on_time__ms_ = ppg.input.rf_on_time__ms_;
     /*`ppg.output.rf_off_time__ms_, assigned in m1 selection above */
                                                                                                                
                                               
     /* calculate output params used by frontend (PPC) code
	
     note that e2e_num_rf_dwelltimes_per_freq=1 always for e2e
  
     /* nRFbins: number of bins in the ntuple where the RF is on */

     /* ndepthbins: number of bins preceding and following the RFbin in the ntuple
	(needed by ppc pgm) */
     ppg.output.e2e_ntuple_depth__bins_ = ppg.input.e2e_num_dwelltimes_per_freq - 
					   ppg.input.e2e_num_rf_dwelltimes_per_freq;
  
     /* npostbins: number of bins after the last ntuple 
	(needed by ppc pgm) */
     ppg.output.e2e_num_post_ntuple_bins = ppg.input.e2e_num_postrfbeamon_dwelltimes +
       ppg.output.e2e_num_beam_off_dwelltimes;
     
     /* nprebins: number of bins before the first ntuple 
	(needed by ppc pgm)  */
     /* nprebins =  fs.input.num_rf_on_delays__dwell_times_ ;  */
     
     
     /* ntuple_width_h: width of each ntuple in the output histogram data */
     ppg.output.e2e_histo_ntuple_width__bins_ = 
       (2* ppg.output.e2e_ntuple_depth__bins_ + ppg.input.e2e_num_rf_dwelltimes_per_freq ); /* for histogram */
  
     /* ntuple_width_s: width of each ntuple in the input scaler data 
	(needed by ppc pgm)  */
     ppg.output.e2e_scaler_ntuple_width__bins_  =
       ppg.output.e2e_ntuple_depth__bins_ + ppg.input.e2e_num_rf_dwelltimes_per_freq ; /* for scaler */
  

     printf("nfreqsteps=%d nprebins=%d ndepthbins=%d npostbins=%d nRFbins=%d\n",
	    ppg.output.num_frequency_steps, ppg.input.num_rf_on_delays__dwell_times_,
	    ppg.output.e2e_ntuple_depth__bins_,  ppg.output.e2e_num_post_ntuple_bins,
	    ppg.input.e2e_num_rf_dwelltimes_per_freq);
 
     printf("ntuple_width_h=%d ntuple_width_s=%d\n",
	   ppg.output.e2e_histo_ntuple_width__bins_,
	    ppg.output.e2e_scaler_ntuple_width__bins_  );
 
     
     /* num_sbins_ch : number of scaler bins for each channel 
	(needed by ppc pgm)  ... check value against dwell times
     */
     num_sbins_ch = ppg.input.num_rf_on_delays__dwell_times_ +  
       ppg.output.e2e_ntuple_depth__bins_ +  ppg.output.e2e_scaler_ntuple_width__bins_  * ppg.output.num_frequency_steps 
       +  ppg.output.e2e_num_post_ntuple_bins; /* n scaler bins per channel */
     if (ppg.output.num_dwell_times !=num_sbins_ch)
       {     
	 printf("Error in calculation for num_sbins_ch (%d), num dwell times=%d; should be equal\n",
		num_sbins_ch, ppg.output.num_dwell_times);
	 return -1;
       }
     /* n_histo_bins : number of histogram bins for each channel 
	(needed by ppc pgm)
     */
     ppg.output.e2e_num_histo_bins_per_ch  =  ppg.input.num_rf_on_delays__dwell_times_ +  
       ppg.output.e2e_histo_ntuple_width__bins_ * ppg.output.num_frequency_steps + 
       ppg.output.e2e_num_post_ntuple_bins ; 
     

     printf("num_sbins_ch=%d num_hbins_ch=%d \n",
	    ppg.output.num_dwell_times,	 ppg.output.e2e_num_histo_bins_per_ch);
     
     

 /*  Parameters for Syd to use:
     ppg.input.e2e_num_dwelltimes_per_freq  
     ppg.input.e2e_num_rf_dwelltimes_per_freq   (fixed at 1)
     ppg.input.e2e_num_postrfbeamon dwelltimes
     ppg.input.beam_off_time__ms_
     
     ppg.output.e2e_num_beam_off_dwelltimes  (to be calculate and output) 
 */

     /* -- Writing the histogram information to the ODB (2e) -- */

     /* write some information to mdarc area for e2e */
     fmdarc.histograms.number_defined = ppg.input.num_type2_frontend_histograms ; /*  number of  histograms as written by frontend */
     fmdarc.histograms.resolution_code = -1 ; /* not used (MUSR only). Dwell_time__ms_ is used instead */
  

     if(! ppg.sis_test_mode.test_mode)  /* real mode (front panel signals, uses ppg and fsc */
       /* The number of bins as calculated by rf_config */
       {
     if(debug)printf("SIS test mode is disabled\n");
     fmdarc.histograms.dwell_time__ms_ =  ppg.output.dwell_time__ms_;
     fmdarc.histograms.num_bins =   ppg.output.e2e_num_histo_bins_per_ch  ;
   }
     else   /* sis internal  test mode  */  
       {
	 if(debug)printf("SIS test mode is enabled\n");
	 fmdarc.histograms.dwell_time__ms_ =  ppg.sis_test_mode.dwell_time__ms_;
	 fmdarc.histograms.num_bins = ppg.sis_test_mode.num_bins;
	 for (j=0; j <  ppg.input.num_type2_frontend_histograms; j ++)
	   {
	     fmdarc.histograms.last_good_bin    [j] =  fmdarc.histograms.num_bins ;
	     fmdarc.histograms.last_background_bin  [j] = 0;
	   }
       }
     if(debug)
       {
	 printf("Writing values to mdarc area:\n");
	 printf("  Number frontend histograms: %d\n", fmdarc.histograms.number_defined);
	 printf("  Number of bins            : %d\n", fmdarc.histograms.num_bins);
	 printf("  Dwell time (ms)           : %f\n", fmdarc.histograms.dwell_time__ms_);
       }
  
     
   
     i             =   ppg.input.num_rf_on_delays__dwell_times_  ;  /* first bin of first ntuple */
     for (j=0; j < ppg.input.num_type2_frontend_histograms   ; j ++)
       {
	 fmdarc.histograms.bin_zero [j]       = 1;
	 fmdarc.histograms.first_good_bin [j] = i;
       }
     
     /* last bin of last ntuple =  histo ntuple width * num freq steps + prebins  -1 */
     i                 =  ( ppg.output.e2e_histo_ntuple_width__bins_ * ppg.output.num_frequency_steps )
       +  ppg.input.num_rf_on_delays__dwell_times_ -1 ;
     for (j=0; j < ppg.input.num_type2_frontend_histograms  ; j ++)
       fmdarc.histograms.last_good_bin [j] = i;
     
     if( ppg.input.e2e_num_postrfbeamon_dwelltimes > 0)
       {  /* mark the region of post rf beam on */
	 for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
	   {
	     fmdarc.histograms.first_background_bin [j] = i+1;
	     fmdarc.histograms.last_background_bin  [j] = i + ppg.input.e2e_num_postrfbeamon_dwelltimes ;
	   }
       }
     else
       {
	 for (j=0; j <  ppg.input.num_type2_frontend_histograms ; j ++)
	   {
	     fmdarc.histograms.first_background_bin [j] = 0;
	     fmdarc.histograms.last_background_bin  [j] = 0;
	   }
       }
     cm_msg(MINFO,"e2e_compute","Computing values and writing to the ODB done, writing rulefile.");                                                                         
     
     sprintf(rulefile_name,"%s%s%s",ppg.input.cfg_path,ppg_mode,".rule");                                                                                                   
     if(debug)printf("e2e_compute: about to open rulefile: %s\n",rulefile_name);                                                                                            

     rulef = fopen(rulefile_name, "w");                                                                                                                                     
     if (rulef) {                                                                                                                                                           
       
    sprintf(str,"%s_%c%c%s%s",ppg_mode,m1,m2,".ppg","\n"); /* add an underscore after ppg_mode */                                                         
    {                                                                                                                                                                    
      int len,size;                                                                                                                                                      
                                                                                                                                                                         
      len=strlen(str);                                                                                                                                                   
      size=sizeof(ppg.output.ppg_template);                                                                                                                              
      strncpy(ppg.output.ppg_template, str,size); /* put the name in the output list */                                                                                  
      ppg.output.ppg_template[len-1] = '\0'; // remove the carriage return needed for the file                                                                           
    }                                                                                                                                                                    
    if (debug) printf("e2e_compute: writing ppg template file name (%s) into rulefile\n",
 ppg.output.ppg_template);                                                      
                                                                                                                                                                         
                                                                                                                                                                         
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s",ppg_mode,".ppg\n");                                                                                                                               
    fputs(str,rulef);                                                                                                                                                    
                                                                                                                                                                         
    sprintf(str,"%s%s%s%f%s","d_mcs_pulse=0","%","d_mcs_pulse=",d_mcs_pulse,"ms\n");                                                                               
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%s%f%s","d_fsc_pulse=0","%","d_fsc_pulse=",d_fsc_pulse,"ms\n");                                                                                     
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%s%f%s","d_pulse=0","%","d_pulse=",d_pulse,"ms\n");                                                                                                 
    fputs(str,rulef);                                                                                                                                                                                                                             
    sprintf(str,"%s%s%s%f%s","d_rfon=0","%","d_rfon=",d_rfon_ppgs,"ms\n");                                                                                                 
    fputs(str,rulef);                                                                                                                                                                                                                                                                                                   
    sprintf(str,"%s%s%s%f%s","d_dwell=0","%","d_dwell=",d_dwell_ppgs,"ms\n");                                                                                        
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%s%f%s","d_dwell_t1=0","%","d_dwell_t1=",d_dwell_t1_ppgs,"ms\n");                                                                                     
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%s%f%s","d_dacs=0","%","d_dacs=",d_dacs,"ms\n");                                                                                                    
    fputs(str,rulef);                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                             
                                                                                                                                                                         
    sprintf(str,"%s%s%d%s","n_beam_on_postF","%",n_beam_on_postF_ppgs,"\n");                                                                                                                      
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%d%s","n_pp_dts","%",n_pp_dts_ppgs,"\n");                                                                                                                      
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%i%s","n_freqs","%",n_freqs_ppgs,"\n");                                                                                                                  
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%i%s","n_beam_off","%",n_beam_off_ppgs,"\n");                                                                                                          
    fputs(str,rulef);                                                                                                                                                    
    sprintf(str,"%s%s%i%s","n_rf_delay","%",n_rf_delay_ppgs,"\n");                                                                                                              
    fputs(str,rulef);                                                                                                                                                    
                                                                                                                                                                         
    fclose (rulef);                                                                                                                                                      
                                                                                                                                                                         
  }                                                                                                                                                                      
  else {                                                                                                                                                                 
    cm_msg(MERROR,"e2e_compute","can't open rule file %s", rulefile_name);                                                                                               
    return -1;                                                                                                                                                           
  }                                                                                                                                                                      
  cm_msg(MINFO,"e2e_compute","Rulefile %s written.",rulefile_name);                                                                                                      
  return 1;                                                                                                                                                              
#endif                                                                                                                                                                   
}                                                                                                                                                                        
                                                                                                                                
                                                                                                                     
                                                                                                                    
                                                                                                                     
                                                                                                                     
                                                                                                                     
                                                                                                                  
  
