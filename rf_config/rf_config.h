// rf_config.h

#define MAX_BNMR_EXPT      13
#define N_SUBSTRINGS       64
#define MAX_MIDBNMR_REGIONS 5

typedef struct {
    char s[64];
    INT  sl;
    char r[64];
    INT  rl;
    char *p;
  } SUBSTRING;

SUBSTRING c[N_SUBSTRINGS];

typedef struct {
    double quot;
    double rem;
  } div_double;

typedef struct {
    char name[3];
    INT (*op_func)(char *);
} BNMR_EXPT;


/* prototypes */
div_double div_f (double num, double denom,const double timeslice);
int rangecheck(double delay, double min_delay, double time_slice);
int build_f_table(char *ppg_mode);
double mult (double a,DWORD b);
INT first_match(SUBSTRING *c, INT n);
INT substitute(char *rulefile, char *inpath, char *outpath, char *outfile);
INT settings_rec_get(void);
INT check_params(char *ppg_mode);
INT expt_search(char *expt_name);
INT file_wait(char *path, char *file);
INT tr_prestart(INT run_number, char *error);
int main(unsigned int argc,char **argv);
#ifdef PSM
INT check_psm_gate(void);
INT get_IQ_params(INT Ntiqtemp, INT *Nc, INT *Ncic);
int build_f_table_psm(char *ppg_mode);
INT check_psm_quad(char *ppg_mode);
INT check_psm_profile(void);
int build_iq_table_psm(char *ppg_mode);
#endif


#ifdef BOTH
INT e1a_compute(char *);
INT e1b_compute(char *);
INT e1c_compute(char *);
INT e1n_compute(char *);
INT e1g_compute(char *);
INT e1j_compute(char *);
INT e1f_compute(char *);
INT e00_compute(char *);
INT e2a_compute(char *);
INT e2b_compute(char *);
INT e2c_compute(char *);
INT e2d_compute(char *);
INT e2e_compute(char *);
#else
#ifdef TYPE1
INT e1a_compute(char *);
INT e1b_compute(char *);
INT e1c_compute(char *);
INT e1n_compute(char *);
INT e1f_compute(char *);
#else
INT e00_compute(char *);
INT e2a_compute(char *);
INT e2b_compute(char *);
INT e2c_compute(char *);
#endif
#endif

#ifdef CAMP
INT camp_get_rec(void);
INT camp_create_rec(void);
INT ping_camp(void);
void check_camp(void);
#endif


