/*
 EPICS IDs

  globals */

int Wchid_polOn;
int Wchid_polOff;
int Wchid_helOn;
int Wchid_helOff;
int Wchid_shutOn;
int Wchid_shutOff;

int Rchid_polOn;
int Rchid_polOff;
int Rchid_helOn;
int Rchid_helOff;
int Rchid_shutOn;
int Rchid_shutOff;

int Rchid_helSwitch;
int Rchid_BnmrPeriod;
int Rchid_BnmrDelay;
int Rchid_BnqrDelay;
int Rchid_BnqrPeriod;

char WpolOn[]="ILE2:POLSW1:DRVON";
char WpolOff[]="ILE2:POLSW1:DRVOFF";
char WhelOn[]="ILE2:POLSW2:DRVON";
char WhelOff[]="ILE2:POLSW2:DRVOFF";
char WshutOn[]="ILE2:SHUT:DRVON";
char WshutOff[]="ILE2:SHUT:DRVOFF";

char RpolOn[]="ILE2:POLSW1:STATON";
char RpolOff[]="ILE2:POLSW1:STATOFF";
char RhelOn[]="ILE2:POLSW2:STATON";
char RhelOff[]="ILE2:POLSW2:STATOFF";
char RshutOn[]="ILE2:SHUT:STATON";
char RshutOff[]="ILE2:SHUT:STATOFF";
char RhelSwitch[]="ILE2:POLSW2:STATLOC";/* 0=DAQ has control 1=Epics has control */

/* dual channel switching modes */
char RbnmrPeriod[]="BNMR:BNQRSW:BNMRPERIOD";
char RbnqrPeriod[]="BNMR:BNQRSW:BNQRPERIOD";
char RbnmrDelay[]="BNMR:BNQRSW:BNMRDELAY";
char RbnqrDelay[]="BNMR:BNQRSW:BNQRDELAY";
char REnableSwitching[]="BNMR:BNQRSW:ENBSWITCHING";

/* Epics names of HV bias variables */ 
char R_ITW[]="ITW:BIAS:RDVOL";
char R_ITE[]="ITE:BIAS:RDVOL";
char R_IOS[]="IOS:BIAS:RDVOL";

