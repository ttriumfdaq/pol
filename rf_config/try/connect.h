/* connect.h

Prototypes for routines in connect.c 

CVS log information:
$Log: connect.h,v $
Revision 1.1  2002/06/07 18:21:12  suz
replaces conn.h


*/

/* caRead, caWrite, caGetId, caExit */
int caSingleGetId(char *pname, int *pchan_id);
int caGetId(char *pRname, char *pWname, int *pchan_Rid, int *pchan_Wid);
int caRead(int chan_id, float *pval);
int caWrite(int chan_id, float *pval);
void caExit(void);
int caCheck(int chan_id);
