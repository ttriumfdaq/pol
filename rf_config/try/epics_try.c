/* test program  epics_try.c

Read helicity

 */


#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "cadef.h"


#include "epics_IDs.h"
#ifndef LOCAL
#define LOCAL static
#endif

INT read_Epics_chan(INT chan_id,  float * pvalue);
INT write_Epics_chan(INT chan_id,  float *pvalue);


int main(void)
{
  INT status;
  float value=-1.0;
  
  Rchid_helOn=Rchid_helOff=-1;

  // Get ID for  STATUS  HEL ON
  status=caSingleGetId (RhelOn, &Rchid_helOn );
  if(status==-1)
    {
      printf("HelTest: Bad status after caGetID for STATUS HEL ON \n");
      caExit(); /* clear any channels that are open */
      return 0;
    }

  
  printf("caGetId returns chan_id for STATUS HEL ON (%s) as %d \n",
	 RhelOn,Rchid_helOn);

  if (caCheck(Rchid_helOn))
    printf("caCheck says channel %s is connected\n",RhelOn);
  else
    printf("caCheck says channel %s is NOT connected\n",RhelOn);

  
  // Get ID for STATUS HEL OFF
  status=caSingleGetId (RhelOff, &Rchid_helOff );
  if(status==-1)
    {
      printf("HelTest: Bad status after caGetID for Hel Off \n");
      caExit(); /* clear any channels that are open */
      return 0;
    }
  printf("caGetId returns chan_id for STATUS HEL OFF (%s) as %d \n",
	 RhelOn,Rchid_helOff);
  
  if (caCheck(Rchid_helOff))
    printf("caCheck says channel %s is connected\n",RhelOff);
  else
    printf("caCheck says channel %s is NOT connected\n",RhelOff);

  
  // Get ID for SET HEL OFF
  status=caSingleGetId (WhelOff, &Wchid_helOff );
  if(status==-1)
    {
      printf("HelTest: Bad status after caGetID for SET HEL OFF \n");
      caExit(); /* clear any channels that are open */
      return (FE_ERR_HW);
    }
  printf("caGetId returns chan_id for SET HEL OFF (%s) as %d \n",
	 WhelOff,Wchid_helOff);

 
  /* now read the channels */

  value = -1;
  status = read_Epics_chan(Rchid_helOn,  &value);
  printf("After call for helOn, value = %f, status = %d\n",value,status);

  value = -1;
   status = read_Epics_chan(Rchid_helOff, &value);
  printf("After call for helOff, value = %f, status = %d\n",value,status);

  /* now write 0 to a SET HEL OFF channel (doesn't change anything) */
  value = 0;
  printf("\n Writing 0 to SET HEL OFF (0 doesn't change anything)\n");
  
  status = write_Epics_chan(Wchid_helOff, &value);
  printf("After call for SET  helOff,  status = %d\n",status);

  
  caExit();
}


INT read_Epics_chan(INT chan_id, float *pvalue)
{
  INT status;
  float fval;
  
  if(chan_id == -1)
  {
    printf("read_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caRead(chan_id,&fval);
  if(status==-1)
  {
    printf("read_Epics_chan: Bad status after caRead for %s\n", ca_name((chid)chan_id));
    return(FE_ERR_HW);
  }
  else
    printf("read_Epics_chan: Read value for %s as %8.3f\n", ca_name((chid)chan_id),fval);

  *pvalue=fval;
  printf("read_Epics_chan for %s returning value = %f \n",ca_name((chid)chan_id),*pvalue);
  return(DB_SUCCESS);
}


/*********************************************************************/
INT write_Epics_chan(INT chan_id,  float *pvalue)
/*********************************************************************/  
{
  INT status;
  INT d7=1;


  if(d7)printf("Write_Epics_chan starting with chan_id=%d and value = %f\n",
	       chan_id ,*pvalue);
 
  if(chan_id == -1)
  {
    printf("write_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caWrite(chan_id,pvalue);
  if(d7)printf("caWrite returns status (%d)\n",status);

  if(status==-1)
  {
    printf("write_Epics_chan: Bad status after caWrite for %s\n",ca_name( (chid)chan_id) );
    return(FE_ERR_HW);
  }
  
  return(DB_SUCCESS);
}
