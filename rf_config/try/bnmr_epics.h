/* bnmr_epics.h

   $Log: bnmr_epics.h,v $
   Revision 1.1  2002/06/05 00:48:32  suz
   defines structure for globals


 */

/* Define structure EPICS_PARAMS

     contains globals needed by Main program  AND  
     epics routines in bnmr_epics.c  

*/
typedef struct {
  BOOL    NaCell_flag; /* True if we scanning NaCell voltage (i.e. ppg_mode=1n) */
  BOOL    Laser_flag;
  /* Hardware Disable Flags   in odb  ../hardware/ 
     Must reboot frontend after resetting flage to re-enable access */
  BOOL hardware_disable_nacell; /* global flag; user can disable NaCell access in odb */
  BOOL hardware_disable_laser ; /* global flag; user can disable Laser access in odb */

  /* EPICS channel IDs */
  int NaRchid; /* ID for read;  set to -1 if no access */
  int NaWchid; /* ID for write;  set to -1 if no access */
  int LaRchid; /* ID for read;  set to -1 if no access */
  int LaWchid; /* ID for write;  set to -1 if no access */

  /* other globals */
  float   Epics_stability; /* value used for stability check (histo process) & NaSet */
  float   Epics_val;      /* Na cell voltage to request */
  float   Epics_inc;
  DWORD   Epics_ninc;     /* number of increments */
  float   Epics_read;     /* value read back */
  INT     Epics_bad_flag; /* counter increments if cannot set NaCell voltage */
  BOOL    Epics_repeat_flag; 
  float   Epics_last_value;  /* value as read and accepted by histo_process */
  float   Epics_start;
} EPICS_PARAMS;


/* function prototypes */

/* Functions for Epics devices used for scanning
   i.e. NaCell, Laser
*/

INT EpicsWatchdog  (EPICS_PARAMS *pEpics_params);
INT EpicsIncr      (EPICS_PARAMS *pEpics_params);
void EpicsGetIds   (EPICS_PARAMS *pEpics_params);
INT  EpicsInit     (BOOL flip,  EPICS_PARAMS *pEpics_params);
INT  EpicsNewCycle (EPICS_PARAMS *pEpics_params);
INT EpicsRead      (float* value, EPICS_PARAMS *pEpics_params);





