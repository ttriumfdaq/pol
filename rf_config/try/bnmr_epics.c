/* bnmr_epics,c

Frontend code to scan the epics devives (NaCell,Laser)

   $Log: bnmr_epics.c,v $
   Revision 1.3  2002/06/07 18:30:57  suz
   replace conn.h by connect.h

   Revision 1.2  2002/06/05 00:45:41  suz
   add cvs header

*/

#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "experim.h"
#include "connect.h"
#include "bnmr_epics.h"
#include "EpicsStep.h"
extern TRIGGER_SETTINGS ts;
//extern INT d5,d7;  /* debugs */

static char epics_device[10];
/*
------------------------------------------------------------------------------

   Sodium Cell :     CONSTANTS
  
------------------------------------------------------------------------------
*/
/* Epics names of NaCell variables */ 
char NaRead_name[]="ILE2:BIAS15:RDVOL";
char NaWrite_name[]="ILE2:BIAS15:VOL";
/* parameters - used at begin run to calculate NaVolt_diff and stability  */
const float   Nadiff_min=0.05; /* NaVolt_diff set to  Nadiff_min if voltage increment (fabs (Epics_inc) < Nainc_limit */
const float   Nadiff_max=0.2;  /*      else   set to  Nadiff_max */
const float   Nainc_limit=1.5;/* below this value, NaVolt_diff = Nadiff_min */
const float   Nainc_min=0.3;   /* minimum increment user is allowed to specify */
/* note also that stability is calculated using above values */
const float   NaVolt_max=999; /* maximum voltage that user can set on NaCell (not reliable higher) */
const float   NaVolt_min=0.5; /* minimum voltage that user can set on NaCell (not reliable lower)  */
const float   Na_max_offset = 0.9; /* maximum offset for laser (more accurate offset value will be calculated */
const float   Na_typical_stability = 0.4;
/*      end of NaCell constants        */
/*
------------------------------------------------------------------------------

   LASER  :     CONSTANTS

------------------------------------------------------------------------------
*/
/* Epics names of Dye Laser  variables */ 
char LaRead_name[]="ILE2:RING:FREQ:RDVOL";
char LaWrite_name[]="ILE2:RING:FREQ:VOL";

/* parameters - used at begin run to calculate NaVolt_diff and stability  */
const float   Ladiff_min= 0.001; /* NaVolt_diff set to  Nadiff_min if voltage increment (fabs (Epics_inc) < Nainc_limit */
const float   Ladiff_max= 0.005;  /*      else   set to  Nadiff_max */
const float   Lainc_limit=0.010;/* below this value, NaVolt_diff = Nadiff_min */
const float   Lainc_min=  0.003;   /* minimum increment user is allowed to specify */
/* note also that stability is calculated using above values */
const float   LaVolt_max=5.0; /* maximum voltage that user can set on NaCell (not reliable higher) */
const float   LaVolt_min=-5.0; /* minimum voltage that user can set on NaCell (not reliable lower)  */
const float   La_max_offset = 0.004; /* maximum offset for laser (more accurate offset value will be calculated */
const float   La_typical_stability = 0.002;
/*      end of Laser constants        */
/*-------------------------------------------------------------------------------------

/* globals within this file 

    Note: offset = difference between read and write values
          diff   = difference between read and write increments, i.e.
          increment = difference between value now and value at last set point
*/

float   Volt_diff;     /* voltage has changed if (abs(difference between read & write increments) < Volt_diff)*/
float   Volt_offset;   /* current offset value  */
float   Epics_stop;

/* structure defined in EpicsStep.h */
EPICS_CONSTANTS epics_constants = { -1, -1, 0, 0, 0, 0 };

/* Function prototypes */
INT  NaCell_init(BOOL flip, EPICS_PARAMS *p_epics_params) ;
void NaGetIds(EPICS_PARAMS *p_epics_params);
INT  Laser_init(BOOL flip, EPICS_PARAMS *p_epics_params);
void LaGetIds(EPICS_PARAMS *p_epics_params);
int d5=0;
int d7=0;


/************************************************************************************/
INT EpicsInit(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /* Initialize Epics device - calls NaCell_init or Laser_init */
  INT status;

  if(d5)printf("EpicsInit starting\n");
  if(p_epics_params->NaCell_flag) 
      status = NaCell_init(flip, p_epics_params );
  else if (p_epics_params->Laser_flag)
    status = Laser_init(flip, p_epics_params );
  else
    {
      printf("Epics_init: Invalid Epics device \n");
      return ( FE_ERR_HW );
    }
  if(d5)printf("EpicsInit returning with status = %d\n",status);
  return (status);
}




/************************************************************************************/
INT EpicsRead(float* pvalue, EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{

  /* Calls caRead to read appropriate epics device 

     caRead returns -1 for failure 
  
         prototype is   int caRead(int chan_id, float *pval);
*/

  if(p_epics_params->NaCell_flag)
    return( caRead(p_epics_params->NaRchid , pvalue));
  else if (p_epics_params->Laser_flag)
    return ( caRead(p_epics_params->LaRchid , pvalue));
  else
    {
      printf("EpicsRead: invalid epics device \n");
      return (  FE_ERR_HW);
    }
}


/************************************************************************************/
INT EpicsWatchdog(EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  INT status, i=0;
  float read_val; /* local variable */
  BOOL flag;
  
  
  /* Maintain EPICS live */
  /* check that channel(s) have not disconnected */
  
  if(d7)printf("EpicsWatchdog starting\n");
  /* just make sure we can access ts */
  if (d7)printf("Checking access to ts; Epics scan enabled = \"%d\"\n",ts.scan_enabled);

 
  flag=FALSE;
  /* 
     check the NaCell channels 
  */
  if (p_epics_params->NaRchid != -1 && p_epics_params->NaWchid != -1 ) /* both or neither should be connected */ 
    {
      /* check whether channels have disconnected */  

      if (caCheck(p_epics_params->NaRchid) && caCheck(p_epics_params->NaWchid) )  /* prototype is  : int caCheck(int chan_id); */
	{   /* we are still connected */
	  status=caRead(p_epics_params->NaRchid , &read_val);
	  if (d7) printf("Loop NaCell:%f [%d]\n", read_val, status);
	  if(status)
	    {
	      cm_msg(MERROR, "EpicsWatchdog", "error  reading NaCell voltage (%d)",status);
	      printf("EpicsWatchdog: ERROR: bad status after calling caRead (%d)\n",status);
	      p_epics_params->NaRchid = p_epics_params->NaWchid = -1; /* indicate failure */
	      flag=TRUE;
	    }
	}		
      else
	{ /* channels have disconnected */
	  printf("EpicsWatchdog: Info: NaCell EPICS channels are disconnected\n");		   
	  p_epics_params->NaRchid = p_epics_params->NaWchid = -1; /* indicate failure */
	  flag=TRUE;
	}
    }
  
  /* 
     now check the Laser channels 
  */
  if (p_epics_params->LaRchid != -1 && p_epics_params->LaWchid != -1 ) /* both or neither should be connected */ 
    {
      /* check whether channels have disconnected */
      if (caCheck(p_epics_params->LaRchid) && caCheck(p_epics_params->LaWchid) )
	{   /* we are still connected */
	  status=caRead(p_epics_params->LaRchid , &read_val);
	  if (d7) printf("Loop Laser:%f [%d]\n", read_val, status);
	  if(status)
	    {
	      cm_msg(MERROR, "EpicsWatchdog", "error  reading LaCell voltage (%d)",status);
	      printf("EpicsWatchdog: ERROR: bad status after calling caRead (%d)\n",status);
	      p_epics_params->LaRchid = p_epics_params->LaWchid = -1; /* indicate failure */
	      flag=TRUE;
	    }
	}		
      else
	{ /* channels have disconnected */
	  printf("EpicsWatchdog: Info: Laser EPICS channels are disconnected\n");		   
	  p_epics_params->LaRchid = p_epics_params->LaWchid = -1; /* indicate failure */
	  flag=TRUE;
	}
    }

  if(flag)
    {   /* flag TRUE indicates state has changed and want to disconnect */
      if ( p_epics_params->LaRchid == -1 &&  p_epics_params->LaWchid  == -1  &&  p_epics_params->NaRchid == -1 &&  p_epics_params->NaWchid == -1)
	{ /* all have lost connection */
	  printf("EpicsWatchdog: disconnecting as flag is true\n");
	  caExit();   /* disconnect */
	  return FE_ERR_HW;
	} 
    }

  return(SUCCESS);

}

/*---------------------------------------------------------------------------*/
void EpicsGetIds( EPICS_PARAMS *p_epics_params )
/*---------------------------------------------------------------------------*/
{
  /* Get Epics IDs */

  NaGetIds( p_epics_params);
  LaGetIds( p_epics_params);

  return;
}
/*---------------------------------------------------------------------------*/
void LaGetIds( EPICS_PARAMS *p_epics_params )
/*---------------------------------------------------------------------------*/
{
  /* Get Epics IDs for the Dye Laser */

  INT status;

  printf("LaGetIds: getting epics channel ID for access to Dye Laser ....\n");
  p_epics_params->LaRchid=p_epics_params->LaWchid= -1; /* initialize to a bad value */
  /*printf("Calling caGetId with LaRead_name %s LaWrite_name %s\n",LaRead_name,LaWrite_name); */
  status=caGetId (LaRead_name, 
		  LaWrite_name, 
		  &p_epics_params->LaRchid, 
		  &p_epics_params->LaWchid);
  if(status==-1)
  {
    printf("LaGetIds: Cannot get epics channel ID; access to Dye Laser is not currently available\n");
    printf("LaGetIds: If a type 1d run is started, will retry for access at BOR \n");
    cm_msg(MERROR,"LaGetIds","Cannot get epics channel ID; access to Dye Laser is not available at present");
    caExit(); /* clear any channels that are open */
  }
  else
  {
    printf("LaGetIds: caGetId returns channel ID for read: %d and write: %d\n",p_epics_params->LaRchid,p_epics_params->LaWchid);
    if (caCheck(p_epics_params->LaRchid) && caCheck(p_epics_params->LaWchid))
      printf("LaGetIds: Info: Laser channels are  connected \n");
  }
  return;
}

/*---------------------------------------------------------------------------*/
void NaGetIds( EPICS_PARAMS *p_epics_params )
/*---------------------------------------------------------------------------*/
{
  /* Get Epics IDs for the   Nacell */
  INT status;

  printf("NaGetIds: getting epics channel ID for access to   Nacell ....\n");
  p_epics_params->NaRchid=p_epics_params->NaWchid= -1; /* initialize to a bad value */
  /*printf("Calling caGetId with NaRead_name %s NaWrite_name %s\n",NaRead_name,NaWrite_name); */
  status=caGetId (NaRead_name, 
		  NaWrite_name, 
		  &p_epics_params->NaRchid, 
		  &p_epics_params->NaWchid);
  if(status==-1)
  {
    printf("NaGetIds: Cannot get epics channel ID; access to Nacell is not currently available\n");
    printf("NaGetIds: If a type 1d run is started, will retry for access at BOR \n");
    cm_msg(MERROR,"NaGetIds","Cannot get epics channel ID; access to Nacell is not available at present");
    caExit(); /* clear any channels that are open */
  }
  else
  {
    printf("NaGetIds: caGetId returns channel ID for read: %d and write: %d\n",
	   p_epics_params->NaRchid, p_epics_params->NaWchid);
    if (caCheck(p_epics_params->NaRchid) && caCheck(p_epics_params->NaWchid))
      printf("NaGetIds: Info: Nacell channels are  connected \n");
  }
  return;
}







/************************************************************************************/
INT EpicsIncr( EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  /* called by cycle_start to increment Epics voltage  */

  INT status;
  float Volt_tmp;
  float check_offset;

 
  if( epics_constants.debug)  /* set to d5 in Laser_init or NaCell_init */
    {
      printf("\nEpicsIncr starting\n");
      
      /* Epics constants should have been set up by Laser_init or NaCell_init */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }

  /*
    call EpicsSet_step to set the next setpoint
  */
  if(! p_epics_params->Epics_repeat_flag)
    {
      /* just check if voltage has changed since last read in histo_process (set points out of order?) */
      status=caRead( epics_constants.Rchan_id , &p_epics_params->Epics_read);
      if(status==-1)
	{
	  printf("EpicsIncr: Bad status returned from caRead (%d)\n",status);
	  cm_msg(MERROR, "EpicsIncr", "error  reading %s voltage to check last_read (%d)",epics_device,status);
	  return DB_NO_ACCESS;
	}
      if(fabs (p_epics_params->Epics_last_value - p_epics_params->Epics_read) > p_epics_params->Epics_stability )
	{
	  printf("\nEpicsIncr: WARNING Unstable %s Voltage or set points out of order\n",epics_device);
	  printf("     Set value=%.4f,just read %.4fV,expected %.4fV, diff > %.4f\n",
		 p_epics_params->Epics_val, p_epics_params->Epics_read, p_epics_params->Epics_last_value, p_epics_params->Epics_stability);
	}
      
      if(d5)printf("EpicsIncr: calling EpicsSet_step to set %s voltage to %.4f\n",epics_device,p_epics_params->Epics_val);
      check_offset = Volt_offset + p_epics_params->Epics_stability; /* derive offset limit from last offset value */
      
      status = EpicsSet_step(  
			  p_epics_params->Epics_val, 
			  check_offset, 
			  p_epics_params->Epics_inc, 
			  Volt_diff, 
			  &p_epics_params->Epics_read, 
			  &Volt_offset,
			  epics_constants);
      
      if(d5)
	{
	  printf("EpicsIncr: EpicsSet_step returns with status =%d\n",status);
	  printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Volt_offset);
	}
      switch(status)
	{
	case(0):
	  if(d5)
	    printf("EpicsIncr: Success - Accepting set point %.4f (read %.4f)\n",p_epics_params->Epics_val,p_epics_params->Epics_read);
	  else
	    printf("Set %.4fV (Read back %.4fV)                    ",p_epics_params->Epics_val,p_epics_params->Epics_read );
	  break;
	case (-3):
	  printf("EpicsIncr: Info: accepting set point  %.4f (read %.4f) since read/write values within allowed offset (%.4f)\n",
		 p_epics_params->Epics_val,p_epics_params->Epics_read,check_offset );
	  break;
	case(-4):
	  printf("EpicsIncr: %s not responding at set point  %.4f (read %.4f) offset=%.4f \n", 
		 epics_device, p_epics_params->Epics_val, p_epics_params->Epics_read, Volt_offset );
	  /* if callbacks are working, we should not need to repeat ..... 
	     /*   -  if callbacks are NOT working, repeating just queues up set values
		  /*  we will try repeating once only after a long delay */
	  printf("EpicsIncr: Repeating at this set point (%.4f)  .... first wait for 5 seconds\n",p_epics_params->Epics_val);
//	  ss_sleep(2000);
	  p_epics_params->Epics_repeat_flag = TRUE ;
	  break;
	default:
	  cm_msg(MERROR,"EpicsIncr","Cannot change %s voltage at set voltage = %.4f (read %.4f)",
		 epics_device, p_epics_params->Epics_val, p_epics_params->Epics_read);
	  cm_msg(MERROR,"EpicsIncr","due to hardware error. Check status of %s then restart",epics_device);
	  return FE_ERR_HW; /* don't continue the run */
	} /* end of switch for EpicsSet_step */
      
    }  /* end of if !p_epics_params->Epics_repeat_flag*/
  
  
  /* now check for repeat flag - it may have just been set above  */
  if(p_epics_params->Epics_repeat_flag)
    {
      /* use EpicsSet to repeat - just checks to offset, does not worry about previous value */
      /* EpicsSet sets and reads back Epics voltage to check access */
      status=EpicsSet(
		   p_epics_params->Epics_val,
		   &p_epics_params->Epics_read,
		   &Volt_offset,
		   epics_constants);
      if(d5)
	{
	  printf("EpicsIncr: EpicsSet returns with status =%d\n",status);
	  printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Volt_offset);
	}
      switch(status)
	{
	case(0):
	  printf("EpicsIncr: Success after EpicsSet; repeating at setpoint = %.4f, read back %.4f, offset %.4f \n",
		 Volt_tmp,p_epics_params->Epics_read, Volt_offset);
	  p_epics_params->Epics_repeat_flag = FALSE;
	  p_epics_params->Epics_bad_flag=0;  /* initialize global counter */
	  break;
	case(-2):
	  printf("EpicsIncr: EpicsSet detects invalid parameters\n");
	case(-4):
	  printf("EpicsIncr: %s voltage not responding to repeat at setpoint %.4f (read %.4f)\n",
		 epics_device, p_epics_params->Epics_val,p_epics_params->Epics_read);
	default:
	  printf("EpicsIncr: Failure from EpicsSet (repeat) at set point %.4f (%d)\n",p_epics_params->Epics_val, status);
	  printf("EpicsIncr: Check if %s is ON\n",epics_device, p_epics_params->Epics_val, status);      
	  cm_msg(MERROR,"EpicsIncr","Cannot change %s voltage at set voltage = %.4f (read %.4f)",
		 epics_device, p_epics_params->Epics_val, p_epics_params->Epics_read);
	  cm_msg(MERROR,"EpicsIncr","Check status of %s then restart",epics_device);
	  cm_msg(MTALK,"EpicsIncr","Can't set %s device voltage",epics_device);
	  p_epics_params->Epics_bad_flag++; /* used to stop trying to set the Epics after Epics_bad_flag = 5  */
	  return FE_ERR_HW; /* don't continue the run */
	  
	}	/* end of switch for EpicsSet */
      
    } /* end of if Epics_Repeat_Flag */
  
  /*   S U C C E S S  */
  /* Epics voltage has been set successfully */
  return(SUCCESS);
}  /* end of EpicsIncr */
  




/************************************************************************************/
INT  EpicsNewCycle(EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /* called by cycle_start at each new cycle 

     - sets voltage to one increment below start voltage
  */

  INT status;
  float Volt_tmp;


  if( epics_constants.debug)
    {
      printf("\nEpicsNewCycle starting\n");

      /* epics_constants was filled in Laser_init or NaCell_init */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
	  
  
  /* set Epics voltage to this value (1 incr. away from start value) now for two reasons:
     1. So that the read_inc will be within range for the check later on
     2. To give the Epics device a chance to reach the start value if there is a large sweep */
  
  Volt_tmp = p_epics_params->Epics_val;
  /* make sure we are within range - if not get within range by adding/subtracting 2 increments */
  if(Volt_tmp > epics_constants.max_set_value ) Volt_tmp = p_epics_params->Epics_val - 2* fabs(p_epics_params->Epics_inc) ;
  if(Volt_tmp < epics_constants.min_set_value ) Volt_tmp = p_epics_params->Epics_val + 2* fabs(p_epics_params->Epics_inc)  ;
  
  /* EpicsSet sets & reads back  & checks  Epics voltage & returns the offset */
  if(d5)printf("\n EpicsNewCycle: NEW CYCLE -  calling EpicsSet with pre-start value of %.4f\n",Volt_tmp);
  status=EpicsSet( Volt_tmp,
	           &p_epics_params->Epics_read,
	           &Volt_offset,
		   epics_constants);
  if(d5)
    {
      printf("EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Volt_offset);
    }
      switch(status)
	{
	case(0):
	  if(d5)printf("Success after EpicsSet at setpoint = %.4f, read back %.4f (new cycle)\n",Volt_tmp,p_epics_params->Epics_read);
	  break;
	case(-2):
	  printf("EpicsNewCycle: EpicsSet detects invalid parameters at setpoint %.4f\n",Volt_tmp);
	case(-4):
	  printf("EpicsNewCycle: %s voltage not responding at setpoint %.4f (read back %.4f)\n",
		 epics_device, Volt_tmp,p_epics_params->Epics_read);
	default:
	  printf("EpicsNewCycle: Failure from EpicsSet (new cycle) at setpoint %.4f (%d)\n",Volt_tmp,status);
	  cm_msg(MERROR, "EpicsNewCycle", "cannot set %s voltage to %.4f (read %.4f)",
		 epics_device,Volt_tmp,p_epics_params->Epics_read);
	  cm_msg(MTALK,"EpicsNewCycle","can't set %s voltage",epics_device);
	  return DB_NO_ACCESS;
	}  
      p_epics_params->Epics_last_value = p_epics_params->Epics_read;  /* new cycle:  set this for Epics_stability check later */

  return(SUCCESS); /* end of new cycle */
}



/*****************************************************************************************/
INT  NaCell_init(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /*   Initialize NaCell
       Called from begin of run 
   */ 

  float read_inc;
  float NaVolt_tmp;
  

  INT status;

      /* Check we have channel IDs for NaCell access */

  if (p_epics_params->NaRchid != -1)
      if (!caCheck(p_epics_params->NaRchid)) p_epics_params->NaRchid=-1;

  if (p_epics_params->NaWchid != -1)
      if (!caCheck(p_epics_params->NaWchid)) p_epics_params->NaWchid=-1;


  if (p_epics_params->NaRchid == -1 || p_epics_params->NaWchid == -1)  /* no access */
    {   /* try again for access to NaCell */      
      printf("NaCell_init: attempting to get epics channel ID for access to NaCell ....\n");
      p_epics_params->NaRchid=p_epics_params->NaWchid= -1; /* initialize both channels to a bad value */
      /*printf("Calling caGetId with NaRead_name %s NaWrite_name %s\n",NaRead_name,NaWrite_name); */
      status=caGetId (NaRead_name, 
		      NaWrite_name, 
		      &p_epics_params->NaRchid, 
		      &p_epics_params->NaWchid);
      if(status==-1)
	{
	  printf("NaCell_init: Cannot get epics channel ID; access to NaCell is not possible\n");
	  cm_msg(MERROR,"NaCell_init","Cannot get epics channel ID; access to NaCell is not possible");
	  caExit(); /* clear any channels that are open */
	  return (DB_NO_ACCESS);
	}
      printf("NaCell_init: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->NaRchid,p_epics_params->NaWchid);
    }

  sprintf(epics_device,"NaCell");
  
  p_epics_params->Epics_start = ts.vol_start;
  Epics_stop = ts.vol_stop;
  p_epics_params->Epics_inc = ts.vol_step;
  if( p_epics_params->Epics_inc == 0 )
    {
      cm_msg(MERROR,"NaCell_init","Invalid increment value: %f volt(s)",
	     p_epics_params->Epics_inc);
      return FE_ERR_HW;
    }
  
  if(    p_epics_params->Epics_start < NaVolt_min  || Epics_stop < NaVolt_min || p_epics_params->Epics_start > NaVolt_max || Epics_stop > NaVolt_max)
    {
      cm_msg(MERROR,"NaCell_init","NaVolt start (%.2f) or stop (%.2f) parameter(s) out of range. Allowed range is between %.2f and %.2f volts",
	     p_epics_params->Epics_start, Epics_stop,  NaVolt_min, NaVolt_max);
      return FE_ERR_HW;
    }
  
  if( ((p_epics_params->Epics_inc > 0) && (Epics_stop < p_epics_params->Epics_start)) ||
      ((p_epics_params->Epics_inc < 0) && (p_epics_params->Epics_start < Epics_stop)) )
    p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_ninc = ((Epics_stop - p_epics_params->Epics_start)/p_epics_params->Epics_inc + 1.); 
  if( p_epics_params->Epics_ninc < 1)
    {
      cm_msg(MERROR,"NaCell_init","Invalid number of Voltage steps: %d",
	     p_epics_params->Epics_ninc);
      return FE_ERR_HW;
    }
  printf("NaCell_init: Selected %d increments starting at %.2f volt(s) by steps of %.2f volt(s)\n",
	 p_epics_params->Epics_ninc,p_epics_params->Epics_start,p_epics_params->Epics_inc);
  printf("NaCell_init: flip = %d\n",flip);
  if(flip) p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_val = p_epics_params->Epics_start;
  
  
  /* 
     calculate Volt_diff
     in EpicsSet_step:
     NaCell voltage has changed if (abs(difference between read & write increments) < Volt_diff) 
  */
  if (fabs (p_epics_params->Epics_inc) < Nainc_min )  /* use a minimum increment of currently 0.3V */
    {
      cm_msg(MERROR,"NaCell_init","NaVolt increment (%.2f) must be at least %.2f",
	     p_epics_params->Epics_inc,Nainc_min);
      return FE_ERR_HW;
    }
  else if  (fabs (p_epics_params->Epics_inc) < Nainc_limit )
    {
      Volt_diff = Nadiff_min;
      p_epics_params->Epics_stability = Nadiff_min *2 ;
    } 
  else
    {
      Volt_diff = Nadiff_max; /* use default constant  value for comparison */
      p_epics_params->Epics_stability = Nadiff_min + 0.1 ;
    }
  if(d5)printf("NaCell_init: Calculated Volt_diff = %.2fV & Epics_stability = %.2f V\n",Volt_diff,p_epics_params->Epics_stability);
  
  /* 
     check whether we can access the NaCell 
  */
  NaVolt_tmp = p_epics_params->Epics_start - p_epics_params->Epics_inc; /* set a value one incr. away from Epics_start */
  /* make sure this is within range  */
  if(NaVolt_tmp > NaVolt_max || NaVolt_tmp < NaVolt_min ) 
    NaVolt_tmp =  p_epics_params->Epics_start + p_epics_params->Epics_inc ; 
  
  printf("NaCell_init: Initial setpoint (%.2f volts) will be 1 increment away from start value\n",
	 NaVolt_tmp);
  /* If NaWchid or NaRchid are -1, we have no access */
  if(p_epics_params->NaWchid < 0 || p_epics_params->NaRchid < 0 )
    {
      printf("Invalid channel ID for NaCell direct access Write=%d, Read=%d\n",p_epics_params->NaWchid,p_epics_params->NaRchid);
      cm_msg(MERROR, "NaCell_init", "Invalid chID for NaCell - cannot access Na Cell");
      return DB_NO_ACCESS;
    }
  
  /* load up some constants needed by EpicsSet and EpicsSet_Step; these are now independent of device (NaCell or NaCell) */
  epics_constants.Rchan_id = p_epics_params->NaRchid ;
  epics_constants.Wchan_id = p_epics_params->NaWchid ;
  epics_constants.max_offset = Na_max_offset;  /* use an initial large value */
  epics_constants.min_set_value = NaVolt_min;
  epics_constants.max_set_value=  NaVolt_max;
  epics_constants.stability = Na_typical_stability;
  epics_constants.debug=0; // may be d5



  if(d5)
    {
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }


  /* EpicsSet sets and reads back NaCell voltage to check access */
  status=EpicsSet(	NaVolt_tmp,
		&p_epics_params->Epics_read,
		&Volt_offset,
		epics_constants);
  if(d5)
    {
      printf("NaCell_init: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.2f, offset = %.2f\n",p_epics_params->Epics_read,Volt_offset);
    }
  switch(status)
    {
    case(0):
      printf("NaCell_init: Success after EpicsSet at setpoint = %.2f, read back %.2f\n",
	     NaVolt_tmp, p_epics_params->Epics_read);
      break;
    case(-2):
      printf("NaCell_init: EpicsSet detects invalid parameters\n");
    case(-4):
      printf("NaCell_init: NaCell voltage not responding (could be switched off)\n");
      printf("              at setpoint = %.2f, read back %.2f\n",NaVolt_tmp,p_epics_params->Epics_read);
    default: 
      printf("NaCell_init: Failure from EpicsSet (%d)\n",status);
      cm_msg(MERROR, "NaCell_init", "cannot set NaCell voltage (NaCell may be switched off)");
      return DB_NO_ACCESS;
    }

  return(SUCCESS);

} /* end of NaCell_init */







/*****************************************************************************************/
INT  Laser_init(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /*   Initialize Laser
       Called from begin of run 
   */ 

  float read_inc;
  float LaVolt_tmp;
  

  INT status;

      /* Check we have channel IDs for Laser access */

  if (p_epics_params->LaRchid != -1)
      if (!caCheck(p_epics_params->LaRchid)) p_epics_params->LaRchid=-1;

  if (p_epics_params->LaWchid != -1)
      if (!caCheck(p_epics_params->LaWchid)) p_epics_params->LaWchid=-1;


  if (p_epics_params->LaRchid == -1 || p_epics_params->LaWchid == -1)  /* no access */
    {   /* try again for access to Laser */      
      printf("Laser_init: attempting to get epics channel ID for access to Laser ....\n");
      p_epics_params->LaRchid=p_epics_params->LaWchid= -1; /* initialize both channels to a bad value */
      printf("Calling caGetId with LaRead_name %s LaWrite_name %s\n",LaRead_name,LaWrite_name); 
      status=caGetId (LaRead_name, 
		      LaWrite_name, 
		      &p_epics_params->LaRchid, 
		      &p_epics_params->LaWchid);
      if(status==-1)
	{
	  printf("Laser_init: Cannot get epics channel ID; access to Laser is not possible\n");
	  cm_msg(MERROR,"Laser_init","Cannot get epics channel ID; access to Laser is not possible");
	  caExit(); /* clear any channels that are open */
	  return (DB_NO_ACCESS);
	}
      printf("Laser_init: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->LaRchid,p_epics_params->LaWchid);
    }
    sprintf(epics_device,"Laser");


  p_epics_params->Epics_start = ts.vol_start;
  Epics_stop = ts.vol_stop;
  p_epics_params->Epics_inc = ts.vol_step;
  if( p_epics_params->Epics_inc == 0 )
    {
      cm_msg(MERROR,"Laser_init","Invalid increment value: %f volt(s)",
	     p_epics_params->Epics_inc);
      return FE_ERR_HW;
    }
  
  if(    p_epics_params->Epics_start < LaVolt_min  || Epics_stop < LaVolt_min || p_epics_params->Epics_start > LaVolt_max || Epics_stop > LaVolt_max)
    {
      cm_msg(MERROR,"Laser_init","LaVolt start (%.4f) or stop (%.4f) parameter(s) out of range. Allowed range is between %.4f and %.4f volts",
	     p_epics_params->Epics_start, Epics_stop,  LaVolt_min, LaVolt_max);
      return FE_ERR_HW;
    }
  
  if( ((p_epics_params->Epics_inc > 0) && (Epics_stop < p_epics_params->Epics_start)) ||
      ((p_epics_params->Epics_inc < 0) && (p_epics_params->Epics_start < Epics_stop)) )
    p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_ninc = ((Epics_stop - p_epics_params->Epics_start)/p_epics_params->Epics_inc + 1.); 
  if( p_epics_params->Epics_ninc < 1)
    {
      cm_msg(MERROR,"Laser_init","Invalid number of Voltage steps: %d",
	     p_epics_params->Epics_ninc);
      return FE_ERR_HW;
    }
  printf("Laser_init: Selected %d increments starting at %.4f volt(s) by steps of %.4f volt(s)\n",
	 p_epics_params->Epics_ninc,p_epics_params->Epics_start,p_epics_params->Epics_inc);
  printf("Laser_init: flip = %d\n",flip);
  if(flip) p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_val = p_epics_params->Epics_start;
  
  
  /* 
     calculate Volt_diff
     in EpicsSet_step:
     Laser voltage has changed if (abs(difference between read & write increments) < Volt_diff) 
  */
  if (fabs (p_epics_params->Epics_inc) < Lainc_min )  /* use a minimum increment of currently 0.3V */
    {
      cm_msg(MERROR,"Laser_init","LaVolt increment (%.4f) must be at least %.4f",
	     p_epics_params->Epics_inc,Lainc_min);
      return FE_ERR_HW;
    }
  else if  (fabs (p_epics_params->Epics_inc) < Lainc_limit )
    {
      Volt_diff = Ladiff_min;
      p_epics_params->Epics_stability = Ladiff_min *2 ;
    } 
  else
    {
      Volt_diff = Ladiff_max; /* use default constant  value for comparison */
      p_epics_params->Epics_stability = Ladiff_min + 0.1 ;
    }
  if(d5)printf("Laser_init: Calculated Volt_diff = %.4fV & Epics_stability = %.4f V\n",Volt_diff,p_epics_params->Epics_stability);
  
  /* 
     check whether we can access the Laser 
  */
  LaVolt_tmp = p_epics_params->Epics_start - p_epics_params->Epics_inc; /* set a value one incr. away from Epics_start */
  /* make sure this is within range  */
  if(LaVolt_tmp > LaVolt_max || LaVolt_tmp < LaVolt_min ) 
    LaVolt_tmp =  p_epics_params->Epics_start + p_epics_params->Epics_inc ; 
  
  printf("Laser_init: Initial setpoint (%.4f volts) will be 1 increment away from start value\n",
	 LaVolt_tmp);
  /* If LaWchid or LaRchid are -1, we have no access */
  if(p_epics_params->LaWchid < 0 || p_epics_params->LaRchid < 0 )
    {
      printf("Invalid channel ID for Laser direct access Write=%d, Read=%d\n",p_epics_params->LaWchid,p_epics_params->LaRchid);
      cm_msg(MERROR, "Laser_init", "Invalid chID for Laser - cannot access La Cell");
      return DB_NO_ACCESS;
    }
  
  /* load up some constants needed by EpicsSet and EpicsSet_Step; these are now independent of device (Laser or NaCell) */
  epics_constants.Rchan_id = p_epics_params->LaRchid ;
  epics_constants.Wchan_id = p_epics_params->LaWchid ;
  epics_constants.max_offset = La_max_offset;  /* use an initial large value */
  epics_constants.min_set_value = LaVolt_min;
  epics_constants.max_set_value=  LaVolt_max;
  epics_constants.stability = La_typical_stability;
  epics_constants.debug=d5;



  if(d5)
    {
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }


  /* EpicsSet sets and reads back Laser voltage to check access */
  status=EpicsSet(	LaVolt_tmp,
		&p_epics_params->Epics_read,
		&Volt_offset,epics_constants );
  if(d5)
    {
      printf("Laser_init: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Volt_offset);
    }
  switch(status)
    {
    case(0):
      printf("Laser_init: Success after EpicsSet at setpoint = %.4f, read back %.4f\n",
	     LaVolt_tmp, p_epics_params->Epics_read);
      break;
    case(-2):
      printf("Laser_init: EpicsSet detects invalid parameters\n");
    case(-4):
      printf("Laser_init: Laser voltage not responding (could be switched off)\n");
      printf("              at setpoint = %.4f, read back %.4f\n",LaVolt_tmp,p_epics_params->Epics_read);
    default: 
      printf("Laser_init: Failure from EpicsSet (%d)\n",status);
      cm_msg(MERROR, "Laser_init", "cannot set Laser voltage (Laser may be switched off)");
      return DB_NO_ACCESS;
    }
return SUCCESS;
} /* end of Laser_init */



















