/* test program  epics_check.c

Read helicity

 */


#include <string.h>
#include <stdio.h>
#include <math.h>
#include "midas.h"
#include "connect.h" /* prototypes */
#include "cadef.h"


#include "epics_IDs.h"
#ifndef LOCAL
#define LOCAL static
#endif

INT read_Epics_chan(INT chan_id,  float * pvalue);
/* INT write_Epics_chan(INT chan_id,  float *pvalue); */


int main(void)
{
  INT status;
  float value=-1.0;
  

  // Get ID for  Helicity Switch
  Rchid_helSwitch=-1;
  status=caSingleGetId (RhelSwitch, &Rchid_helSwitch );
  if(status==-1)
    {
      printf("epics_check: Bad status after caGetID for status of Helicity Switch \n");
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }

  
  printf("caGetId returns chan_id for  status HEL SWITCH (%s) as %d \n",
	 RhelSwitch,Rchid_helSwitch);

 
  if(d7)printf("Calling read_Epics_chan for helSwitch\n");
  status = read_Epics_chan(Rchid_helSwitch,  &value);
  i=(INT)value;
  printf("Helicity_init: helicity switch value is %d (switch control: 1 EPICS 0: DAQ)\n",i);
  if(flip)
    {
      if(i != 0)
	{
	  cm_msg(MINFO,"Helicity_init",
		 "ERROR - DAQ does not have control of helicity switch; cannot flip helicity");
	  caExit(); /* clear any channels that are open */
	  return FE_ERR_HW;
	}
    }
  if(d7)printf("Helicity_init: disconnecting Epics channel for Switch \n");
  ca_clear_channel(Rchid_helSwitch); /* we don't need this channel any more */
  Rchid_helSwitch = -1;


  /* Read the Epics Enable Switching param for dual channel mode */

  if(Rchid_EnableSwitching > 0)
    {
      /* Should have been cleared at end of last run */
      printf("GetEpicsParams: channel ID for EnableSwitching is already defined; clearing the channel...\n");
      ca_clear_channel(Rchid_EnableSwitching);
    }
  Rchid_EnableSwitching = -1;

  status = caGetSingleId(REnableSwitching, &Rchid_EnableSwitching );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" to Enable Switching \n", REnableSwitching);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }



  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"Enable Switching\" (\"%s\") as %d \n",
	 REnableSwitching,Rchid_EnableSwitching);
 
  if(d7)printf("Calling read_Epics_chan for Dual channel EnableSwitching\n");
  status = read_Epics_chan(Rchid_EnableSwitching,  &value);
  i=(INT)value;
  printf("GetEpicsParams: dual channel switch value is %d (switch control: 1 Dual Channel Mode 0: Single Channel Mode)\n",i);

  fs.output.epics_dual_channel_switch=i;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for Enable Switching \n");
  ca_clear_channel(Rchid_EnableSwitching); /* we don't need this channel any more */
  Rchid_EnableSwitching = -1;

  if(i==0)
    {
      cm_msg(MINFO,"GetEpicsParams","Single Channel Mode is enabled in Epics");
      return SUCCESS;  /* Single Channel Mode: other values are irrelevent */
    }

/* 
          Dual Channel Mode: 

    read the rest of the parameters 
*/
  if(Rchid_BnqrPeriod > 0)
    {
      /* Should have been cleared at end of last run */
      printf("GetEpicsParams: channel ID for BnqrPeriod is already defined; clearing the channel...\n");
      ca_clear_channel(Rchid_BnqrPeriod);
    }
    
  if(Rchid_BnqrDelay > 0)
    {
      /* Should have been cleared at end of last run */
      printf("GetEpicsParams: channel ID for BnqrDelay is already defined; clearing the channel...\n");
      ca_clear_channel(Rchid_BnqrDelay);
    }    
    
  if(Rchid_BnmrPeriod > 0)
    {
      /* Should have been cleared at end of last run */
      printf("GetEpicsParams: channel ID for BnmrPeriod is already defined; clearing the channel...\n");
      ca_clear_channel(Rchid_BnmrPeriod);
    }    

  if(Rchid_BnmrDelay > 0)
    {
      /* Should have been cleared at end of last run */
      printf("GetEpicsParams: channel ID for BnmrDelay is already defined; clearing the channel...\n");
      ca_clear_channel(Rchid_BnmrDelay);
    }
    
  Rchid_BnmrDelay=Rchid_BnmrPeriod=Rchid_BnqrDelay=Rchid_BnqrPeriod=-1;
 

  /* get parameters and write them into odb output area */

  /*  BNMR Period */
  status = caGetSingleId(RbnmrPeriod, &Rchid_BnmrPeriod );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" to Enable Switching \n", RbnmrPeriod);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNMR Period\" (\"%s\") as %d \n",
	 RbnmrPeriod,Rchid_BnmrPeriod);
 
  if(d7)printf("Calling read_Epics_chan for BNMR Period\n");
  status = read_Epics_chan(Rchid_BnmrPeriod,  &value);
  printf("GetEpicsParams: BNMR Period is %f \n",value);

  fs.output.epics_bnmr_period=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNMR Period \n");
  ca_clear_channel(Rchid_BnmrPeriod); /* we don't need this channel any more */
  Rchid_BnmrPeriod = -1;


  /*  BNMR Delay */
  status = caGetSingleId(RbnmrDelay, &Rchid_BnmrDelay );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" to Enable Switching \n", RbnmrDelay);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNMR Delay\" (\"%s\") as %d \n",
	 RbnmrDelay,Rchid_BnmrDelay);
 
  if(d7)printf("Calling read_Epics_chan for BNMR Delay\n");
  status = read_Epics_chan(Rchid_BnmrDelay,  &value);
  printf("GetEpicsParams: BNMR Delay is %f \n",value);

  fs.output.epics_bnmr_delay=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNMR Delay \n");
  ca_clear_channel(Rchid_BnmrDelay); /* we don't need this channel any more */
  Rchid_BnmrDelay = -1;


 

  /*  BNQR Period */
  status = caGetSingleId(RbnqrPeriod, &Rchid_BnqrPeriod );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" to Enable Switching \n", RbnqrPeriod);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNQR Period\" (\"%s\") as %d \n",
	 RbnqrPeriod,Rchid_BnqrPeriod);
 
  if(d7)printf("Calling read_Epics_chan for BNQR Period\n");
  status = read_Epics_chan(Rchid_BnqrPeriod,  &value);
  printf("GetEpicsParams: BNQR Period is %f \n",value);

  fs.output.epics_bnqr_period=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNQR Period \n");
  ca_clear_channel(Rchid_BnqrPeriod); /* we don't need this channel any more */
  Rchid_BnqrPeriod = -1;


  /*  BNQR Delay */
  status = caGetSingleId(RbnqrDelay, &Rchid_BnqrDelay );
  if(status==-1)
    {
      printf("GetEpicsParams: Bad status after caGetID for status of \"%s\" to Enable Switching \n", RbnqrDelay);
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }


  if(d7)printf("GetEpicsParams: caGetId returns chan_id for \"BNQR Delay\" (\"%s\") as %d \n",
	 RbnqrDelay,Rchid_BnqrDelay);
 
  if(d7)printf("Calling read_Epics_chan for BNQR Delay\n");
  status = read_Epics_chan(Rchid_BnqrDelay,  &value);
  printf("GetEpicsParams: BNQR Delay is %f \n",value);

  fs.output.epics_bnqr_delay=value;
  if(d7)printf("GetEpicsParams: disconnecting Epics channel for BNQR Delay \n");
  ca_clear_channel(Rchid_BnqrDelay); /* we don't need this channel any more */
  Rchid_BnqrDelay = -1;
 
  caExit();
}


INT read_Epics_chan(INT chan_id, float *pvalue)
{
  INT status;
  float fval;
  
  if(chan_id == -1)
  {
    printf("read_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caRead(chan_id,&fval);
  if(status==-1)
  {
    printf("read_Epics_chan: Bad status after caRead for %s\n", ca_name((chid)chan_id));
    return(FE_ERR_HW);
  }
  else
    printf("read_Epics_chan: Read value for %s as %8.3f\n", ca_name((chid)chan_id),fval);

  *pvalue=fval;
  printf("read_Epics_chan for %s returning value = %f \n",ca_name((chid)chan_id),*pvalue);
  return(DB_SUCCESS);
}


/*********************************************************************/
INT write_Epics_chan(INT chan_id,  float *pvalue)
/*********************************************************************/  
{
  INT status;
  INT d7=1;


  if(d7)printf("Write_Epics_chan starting with chan_id=%d and value = %f\n",
	       chan_id ,*pvalue);
 
  if(chan_id == -1)
  {
    printf("write_Epics_chan: channel has disconnected\n");
    return(FE_ERR_HW);
  }

  
  status=caWrite(chan_id,pvalue);
  if(d7)printf("caWrite returns status (%d)\n",status);

  if(status==-1)
  {
    printf("write_Epics_chan: Bad status after caWrite for %s\n",ca_name( (chid)chan_id) );
    return(FE_ERR_HW);
  }
  
  return(DB_SUCCESS);
}
