/* rf_config.c 

Author: Pierre Amaudruz
Modifications by: C. Bommas, R. Poutissou, S. Kreitzman, S. Daviel
  
 $Log: rf_config.c,v $
 Revision 1.58  2006/06/21 19:10:50  suz
 add mode 1j (combination of 1c (CAMP) and 20 (SLR) )

 Revision 1.57  2006/04/28 17:32:24  suz
 PSMII : d_w_min formula corrected according to Syd's new spec.

 Revision 1.56  2005/08/13 01:39:24  suz
 add checks on epics dual ch. mode - split up settings_rec_get

 Revision 1.55  2005/08/04 00:56:14  suz
 add e2e_compute

 Revision 1.54  2005/07/19 21:35:18  suz
 add check on Epics dual-channel params instead of in frontend; for POL write Epics address for bias depending on edit-on-start param

 Revision 1.53  2005/06/08 00:13:27  suz
 fix maximum value allowed for PSMII

 Revision 1.52  2005/05/16 19:11:12  suz
 fix single sweep so it doesn't write PSM params to odb

 Revision 1.51  2005/05/02 18:31:39  suz
 replace BNMR by COPY_ODB otherwise BNMR to support bnmr on dasdevpc as well

 Revision 1.50  2005/05/02 18:16:48  suz
 add a comment

 Revision 1.49  2005/05/02 17:52:27  suz
 add some checking on setting i,q to constant values

 Revision 1.48  2005/04/30 23:01:04  suz
 copy e00_rf_freq (Hz) into psm area (used by fsc). Add 2d

 Revision 1.47  2005/04/26 22:17:06  suz
 add PSMIIB (Ncmx fixed); change odb param modulation_mode; fix bug

 Revision 1.46  2005/04/12 23:08:08  suz
 add Hermite function for PSM

 Revision 1.45  2005/04/08 21:21:15  suz
 add support for PSMII

 Revision 1.44  2005/03/10 22:13:10  suz
 use complete path for comp_init.pl

 Revision 1.43  2005/03/03 19:12:41  suz
 use comp_int.pl from rf_config directory where it is in cvs, rather than /home/bnmr/bin

 Revision 1.42  2005/02/16 21:01:41  suz
 fix bug: add ifdef PSM around Ncmx for FSC case

 Revision 1.41  2005/02/14 20:50:32  suz
 do not overwrite Ncmx; get the record for output

 Revision 1.40  2005/02/04 18:44:49  suz
 freq table now works with start<stop; Type 2 no longer free-running, set loop counter to 0

 Revision 1.39  2005/01/27 19:28:28  suz
 cm_register_transition fixed for Midas 1.9.5

 Revision 1.38  2005/01/12 01:20:11  suz
 build_f_table and build_f_table_psm modified to handle negative increments

 Revision 1.37  2004/10/26 23:10:30  suz
 add support for PSM

 Revision 1.36  2004/06/15 19:14:56  suz
 POL defined only for pol (not bnqr). Add pol support. Remove TYPE1 and TYPE2(obsolete) - supports BOTH only

 Revision 1.35  2004/03/09 21:53:04  suz
 changes for Midas 1.9.3

 Revision 1.34  2003/12/01 19:56:00  suz
 add support for mode 1g (FAST)

 Revision 1.33  2003/07/29 18:50:31  suz
 add support for client status flag

 Revision 1.32  2003/06/25 01:12:07  suz
 add support for Epics device: Field

 Revision 1.31  2003/06/12 18:24:14  suz
 separate numbers of type1 and type2 histograms

 Revision 1.30  2003/01/20 22:36:19  suz
 add mode 10 (dummy frequency scan mode)

 Revision 1.29  2003/01/08 19:34:58  suz
 add polb

 Revision 1.28  2002/06/24 19:35:04  suz
 Added extra parameter to ss_daemon_init for midas 1.9.1

 Revision 1.27  2002/06/05 20:28:02  suz
 add ifndef POL for polarimeter

 Revision 1.26  2002/06/05 00:05:16  suz
 separate Type 1 and Type2 with conditional compile; camp support

 Revision 1.25  2001/11/20 21:58:30  suz
 add counting mode 3 to e2c

 Revision 1.24  2001/11/05 17:44:25  suz
 Fix bug 1a/b frequency mode param: F0 not FO

 Revision 1.23  2001/10/25 21:47:44  suz
 create record only if sizes have changed

 Revision 1.22  2001/10/24 21:26:42  suz
 remove underscore after mode in filenames

 Revision 1.21  2001/10/11 19:28:42  suz
 update rf on and off time in odb output tree

 Revision 1.20  2001/09/14 18:50:48  suz
 autoselect type 1 template files

 Revision 1.19  2001/08/16 20:15:22  suz
 adiabacity check removed

 Revision 1.18  2001/08/08 19:42:50  suz
 add mode 1f. Fix bug in writing mdarc params

 Revision 1.17  2001/08/02 18:36:49  suz
 changed delay_i to delai to agree with type 2 scripts

 Revision 1.16  2001/08/01 19:30:40  suz
 frontend histos now read from mdarc area

 Revision 1.15  2001/05/23 17:34:45  suz
 write default values to midbnmr area (BNMR type 1 only)

 Revision 1.14  2001/05/04 21:31:32  suz
 fix small bug

 Revision 1.13  2001/05/04 21:20:09  suz
 Write mdarc area info to odb for 1n

 Revision 1.12  2001/05/04 03:33:34  renee
 add mode 1n

 Revision 1.11  2001/04/24 23:03:07  suz
 renee improves error messages

 Revision 1.10  2001/01/04 23:19:33  syd
 Changes made by Syd on 20 Dec 2000.

 Revision 1.9  2000/12/19 17:36:38  suz
 Syd:  More mode 00 fixes to reflect that the pulseblaster
  will not loop around a code segment consisting of only
  a single delay statement.

 Revision 1.8  2000/12/18 19:32:37  suz
 Mode 00 additions and timing fixes by Syd:
 New RFduration parameter active in modes  _30, _3P, _40, and _4P
 RFduration=0 also now controls turning on _00 and _0P

 Revision 1.7  2000/12/14 18:36:12  suz
 Syd's mods for e2c; writes nbins etc. to mdarc odb area; calls perl
 compiler directly with 2 parameters & prints messages on error.

 Revision 1.6  2000/12/05 03:42:32  renee
 numerous bug fixes to mode 00

 Revision 1.4  2000/11/30 05:54:53  renee
 rf_config writes information needed by mdarc to the mdarc.histogram area,
 i.e.  no. bins, no. histograms, dwell time.

 Revision 1.3  2000/11/15 00:28:01  midas
 remove pid parameter from ss_system for new version of midas

 Revision 1.2  2000/11/10 19:35:50  midas
  "mdarc" & "sis mcs" trees now created independently. The trees moved to
  /equipment/FIFO_acq from /equipment/FIFO_acq/settings. (New version of
  odb make cmd creates independent trees in experim.h)

 Revision 1.1  2000/11/06 20:05:19  midas
 Add time of compiled file to odb, modify odb settings path to include
 "sis mcs". Add to cvs.

 Add to cvs  6Nov2000 SD

 Modified Oct and NOV 2000   SD
 Add time of compiled file to odb, and change odb settings path
 to "/equipment/FIFO_acq/settings/sis mcs"

 Dec 12
 This version 
 1. writes calculated values (nbins,nhis,dwell time) to mdarc area
 2. contains Syd's modifications to e2c_compute.
 3. calls perl compiler comp_int.pl directly with two parameters,
    saves compiler error messages in a file and displays it on
    error 
*/      
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef CAMP
/* camp includes come before midas.h */
#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif
#endif  // CAMP
  
#include "midas.h"
#include "msystem.h"
#include "rf_config.h" // function prototypes and structures defined
#include "experim.h"

#ifdef CAMP
#include "camp_acq.h" // communicate with camp_acq
#endif

#ifdef EPICS_ACCESS
#include "connect.h" /* prototypes */
#include "cadef.h"
#include "epics_IDs.h"
#endif

const int f_switch_boundary = 100000;   /* Phase discontinuity when changing the 100 kHz digit*/
const int f_max_stepsize    = 100000;



char eqp_name[32];
HNDLE  hDB, hSeq, hFiS, hIn, hOut, hHis, hMdarc, hTest, hPSM, hH;

#ifdef CAMP
HNDLE hCamp;
FIFO_ACQ_CAMP_SWEEP_DEVICES fcamp;
/* camp_acq.h */
CAMP_PARAMS camp_params;
#endif
char comp_path[32];
FIFO_ACQ_SIS_MCS ppg;
FIFO_ACQ_MDARC fmdarc;

BOOL single_shot;
BOOL debug=1;

#ifdef BOTH  // BOTH is defined for BNMR/BNQR/POL i.e. all experiments
/*  BOTH includes PPG TYPE1 and TYPE2 modes 

The following modes are DUMMIES that revert to 1f and do not appear in the list:
     Note 1e is taken for FIELD  Epics Field scan
          1d is taken for LASER  Epics Laser scan
     Note 1h is taken for DAC (Pol's DAC scan)

If adding another expt, increase MAX_BNNR_EXPT in rf_config.h
*/

BNMR_EXPT expt[] = {
  {"1a", e1a_compute},
  {"1b", e1b_compute},
#ifdef CAMP
  {"1c", e1c_compute},
  {"1j", e1j_compute},
#endif
  {"1n", e1n_compute},
  {"1g", e1g_compute},
  {"1f", e1f_compute},
  {"00", e00_compute},
  {"2a", e2a_compute},
  {"2b", e2b_compute},
  {"2c", e2c_compute},
  {"2d", e2d_compute},
  {"2e", e2e_compute},
  {""  , NULL}
};
 
#endif // BOTH is now defined for BNMR/BNQR/POL
/*  TYPE1 and TYPE2 are obsolete */

#ifdef CAMP
/* CAMP support */
CAMP_PARAMS camp_params; // structure defined in camp_acq.h

/* needed for check_camp */
int campInit  = FALSE;
int camp_available;
char serverName[LEN_NODENAME+1];
#include "check_camp.c"   // include code shared with fe_camp.c, i.e. check and ping_camp 
#endif

/*------------------------------------------------------------------*/
// Include relevent code
#ifdef BOTH
#include "type1_compute.c"
#include "type2_compute.c"
#endif // BOTH  ; TYPE1 or TYPE2

#ifdef EPICS_ACCESS
#include "epics_check.c"
#endif
/*------------------------------------------------------------------*/
int rangecheck(double delay, double min_delay, double time_slice)
{ /* Check for minimal delay and time_slice*/
  double f;
  char str [32];
  DWORD i=0;

  if (delay < min_delay) return 0;      /*Errorcond.*/
  if (time_slice == 0) return 0;
  f = delay / time_slice;
  cm_msg(MINFO,"Rangecheck","pre calc delay %f ",delay);
  while (i<f) i++;
  f = i * time_slice;
  cm_msg(MINFO,"Rangecheck","post calc f %f ",f);

	 		
return 1;
}
/*------------------------------------------------------------------*/

div_double div_f (double num, double denom,const double timeslice)
{
  div_t     n;
  div_double m;
  int a,b;

  //  printf("div_f:  num=%f, denom=%f, timeslice=%f\n",num,denom,timeslice);
  a = num   / timeslice;
  b = denom / timeslice;
  n = div (a,b);
  m.quot = n.quot;
  m.rem  = n.rem  * timeslice;
  // printf("num=%f;denom=%f;timeslice=%f;  a=%d;b=%d;  n.quot=%d;n.rem=%d; m.rem=%f\n",
  //	  num,denom,timeslice,a,b,n.quot,n.rem,m.rem);
 return m;
}


/*------------------------------------------------------------------*/

int build_f_table(char *ppg_mode)
{
  /* Modify so this works with start < stop value */
  FILE  *freqfile;
  char  str[256];
  char  fstr[12];
  INT freq,f_stop;
  INT f_inc   ;
  INT  i,j,k;
  float fk;
  char  * f_bsd;
  div_t n;
  WORD  ad_test;
  INT f_ninc;

 /*Create frequency table */
  i = 0;
  freq  =  ppg.input.frequency_start__hz_;
  f_inc =  ppg.input.frequency_increment__hz_;
  f_stop = ppg.input.frequency_stop__hz_;
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,".fsc");

  /* printf("build_f_table: freq start = %d stop = %d incr = %d\n",
	ppg.input.frequency_start__hz_ ,
	ppg.input.frequency_stop__hz_, 
	ppg.input.frequency_increment__hz_);
  */

  if(f_inc == 0)
    {
      printf("build_f_table: frequency increment cannot be 0 \n");
      return -1;
    }
 if ( abs(f_inc) > abs(f_stop - freq))
   { 
     printf("build_f_table: frequency increment (%d) is too large \n",f_inc);
      return -1;
    }

 if( ((f_inc > 0) && (f_stop < freq)) ||
	      ((f_inc < 0) && (freq < f_stop)) )
	    f_inc *= -1;

 printf("build_f_table: freq start = %d stop = %d incr = %d\n",
	freq,f_stop,f_inc);

  f_ninc = (  abs(f_stop - freq)/ abs(f_inc) ) +1;

  // printf("no. increments %d diff %d freq_inc = %d\n",f_ninc,(f_stop-freq),f_inc);
  if( f_ninc < 1)
    {      
      printf("build_f_table: too few frequency increments\n");
      return -1;
    }
  
  printf("build_f_table: about to open frequency table file %s\n",str);
 if (freqfile = fopen(str,"w"))
   {
     for(i=0; i<f_ninc; i++)
       {
	 printf("i=%d freq=%d f_inc=%d\n",i,freq,f_inc);
	  
	 if (freq != f_switch_boundary)
	   n = div (freq,f_switch_boundary);
	 else
	   n = div ((freq + 1),f_switch_boundary);
	  
	  
	 sprintf(str,"%u%s",freq,"\n");
	 fputs(str,freqfile);
	 freq = freq + f_inc;
       }

     sprintf(str,"%u%s",f_ninc,"\n");
     if (!(1))fputs(str,freqfile);
     fclose(freqfile);

     printf("final freq: %d\n",freq);
     
     ppg.output.num_frequency_steps =  f_ninc;
     ppg.output.frequency_stop__hz_ = (float) (freq - f_inc);
     printf("final freq = %f,  num steps=%d\n",
	    ppg.output.frequency_stop__hz_,
	     ppg.output.num_frequency_steps);
     
     return f_ninc;
   }
  else
    {cm_msg(MERROR,"Build_f_table","Error opening frequency file %s ",str);
    return -1;
    }
}

#ifdef PSM
/*------------------------------------------------------------------*/
int build_f_table_psm(char *ppg_mode)
{
  /* build a frequency table for the psm 

  note - for Type 2 file is in HEX format (file will be loaded into PSM)
         for Type 1 file is not loaded. 
	       File is used only by Types 1a,1b with randomized values, but freq values are in Hz)
*/
  FILE  *freqfile;
  char  str[256];
  char  fstr[12];
  double freq,fstep,fstop;
  INT  i,j;
  char  * f_bsd;
  int  ad_test;
#ifndef PSMII
  const double finc = 0.009313226; // Hz
#else
  const double finc =  0.04656613;
#endif
  double fmax;

  double ftemp;
  unsigned long int freqx,fstepx,fstopx; 
  int my_debug=TRUE;
  long int freq_inc;
  INT f_ninc;
  BOOL hz;  /*if true, freq values will be in Hz in the file */

#ifdef PSMII
  fmax = 8 * pow(10,7) - finc;
#else
  fmax = 4 * pow(10,7) - finc;
#endif

  if( ppg.input.frequency_stop__hz_ > fmax)
    {
      printf("build_f_table_psm: stop freq (%uMHz) exceeds device maximum (%.1e)\n",
	     (DWORD)(ppg.input.frequency_stop__hz_/pow(10,6)) , fmax);
      return -1;
    }

  freq_inc =  (DWORD)ppg.input.frequency_increment__hz_;
  if(freq_inc == 0)
    {
      printf("build_f_table: frequency increment cannot be 0 \n");
      return -1;
    }

 // use labs for long argument
 if ( labs(freq_inc) > labs( ppg.input.frequency_stop__hz_ - ppg.input.frequency_start__hz_ ))
   { 
     printf("build_f_table: frequency increment (%d) is too large \n",freq_inc);
     return -1;
   }


  /* Modify so this works with start < stop value */
  if( ((freq_inc > 0) && 
       ( ppg.input.frequency_stop__hz_ <  ppg.input.frequency_start__hz_)) ||
      ((freq_inc < 0) && 
       ( ppg.input.frequency_start__hz_ <  ppg.input.frequency_start__hz_)) )
	    freq_inc *= -1;

 printf("build_f_table: freq start = %u stop = %u incr = %d\n",
	(DWORD)ppg.input.frequency_start__hz_, 
	(DWORD)ppg.input.frequency_stop__hz_,
	freq_inc);

 // use labs for long argument
 f_ninc = (  labs( (DWORD)ppg.input.frequency_stop__hz_ -  (DWORD)ppg.input.frequency_start__hz_)
	     / labs(freq_inc) ) +1;

 printf("no. increments %d  freq_inc = %d\n",f_ninc,freq_inc);
 if( f_ninc < 1)
    {      
      printf("build_f_table_psm: too few frequency increments\n");
      return -1;
    }


  /*Create frequency table */
  
  i = 0;
  freq  = (double) ppg.input.frequency_start__hz_;
  ftemp = freq/finc +0.5 ;// round up ftemp is type double
  freqx = (unsigned long int)ftemp; // freq start value in hex

  //  fstep =  (double) ppg.input.frequency_increment__hz_;
  fstep = (double) freq_inc; // direction may have been reversed above
  ftemp = fstep/finc +0.5 ;// round up
  fstepx = (unsigned long int)ftemp; // step value in hex
  
  fstop = (double) ppg.input.frequency_stop__hz_;
  ftemp = fstop/finc +0.5 ;// round up
  fstopx = (long int)ftemp; // stop value in hex
  
  /* calculate the number of steps needed */
  j = 1 + (int)(fstop-freq)/fstep;
  
  printf("start freq=%.1fHz; step by=%.1fHz  stop at %.1fHz\n",freq,fstep,fstop);
  printf("freqx = %u (0x%x) ;  fstepx = %u (0x%x); fstopx = %u (0x%x)\n",
	 freqx,freqx,fstepx,fstepx,fstopx,fstopx);

  //printf("Number of steps needed %d should agree with f_ninc=%d\n",j,f_ninc);
  
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,".psm");
  if(strncmp("1",ppg_mode,1)==0) /* look for type 1 */
    hz=TRUE; /* values in table should be in hz */
  else
    hz = FALSE;

  printf("build_f_table_psm: about to open frequency table file %s\n",str);
  if(hz)
    printf("   freq values will be in Hz\n");
  else 
    printf("   freq values will be in Hex format\n");

  if (freqfile = fopen(str,"w"))
    
    {
      for(i=0; i<j; i++)
	{
	  /* print some extra stuff for debugging including freq in Hz */
	  if(my_debug)
	    {
	      if(i==0)
		{
		  printf(" 0x%x  (%uHz) %d; stop=%dHz; step=%dHz; %dsteps %s",
			 freqx,(unsigned long int)freq,i,
			 ppg.input.frequency_stop__hz_,
			 freq_inc,j,"\n");
		}
	      else
		printf("0x%x  (%uHz) %d%s",freqx,(unsigned long int)freq,i,"\n");
	    }

	  if(hz)
	    sprintf(str,"%u%s",(unsigned long int)freq,"\n");
	  else
	    sprintf(str,"%x%s",freqx,"\n");
	  fputs(str,freqfile);
	  freqx = freqx + fstepx;
	  freq = (double)freqx  * finc +0.5; // debugging, convert to Hz       
	}
      fclose(freqfile);
      ppg.output.num_frequency_steps =  j;
      ftemp = (double)(freqx - fstepx) ; // calculate last value
      freq = ftemp * finc + 0.5 ; // round up
    
      printf("num freq steps = %d\n",j);    
      ppg.output.frequency_stop__hz_ =  (DWORD)freq;
      printf("Freq stop in hz = %d\n",ppg.input.frequency_stop__hz_);
      printf("Actual freq stop will be %f or %u\n",freq,(unsigned long int)freq);
      return j;
    }
  else
    {
      cm_msg(MERROR,"Build_f_table_psm","Error opening frequency file %s ",str);
      printf("Build_f_table_psm: Error opening frequency file %s ",str);
      return -1;
    }
}

/*------------------------------------------------------------------*/
INT check_psm_quad(char *ppg_mode)
{
  INT status,size;
  
  if(check_psm_profile() != 0) return -1; /* checks at least one profile (or all) is enabled */
  
  if(!ppg.hardware.psm.quadrature_modulation_mode)
    return 0;
  
  // quad mode is enabled
  printf("check_psm_quad: quadrature modulation mode is enabled\n");
  
  if (!ppg.hardware.psm.all_profiles_enabled)
    {    
      cm_msg(MINFO,"check_psm_quad","Enabling ALL profiles since Quadrature Mode is selected ");
      ppg.hardware.psm.all_profiles_enabled=TRUE;
      if(!single_shot)
	{
	  size = sizeof(ppg.hardware.psm.all_profiles_enabled);
	  status = db_set_value(hDB, hPSM, "all profiles enabled", 
			    &ppg.hardware.psm.all_profiles_enabled, size, 1, TID_BOOL);
	  if (status != SUCCESS)
	    {
	      cm_msg(MERROR,"check_psm_profile",
		     "Error setting \"all profiles enabled\" to true (%d)\n",status);
	      return -1;
	    }
	}
      if(check_psm_profile() != 0) 
	{
	 cm_msg(MERROR,"check_psm_profile",
		 "Error return from check_psm_profile (%d)\n",status);  
	  return -1; /* redo this check; sets individual profiles false */
	}
    }

  // Quadrature mode: build the I,Q pairs table

  status =  build_iq_table_psm(ppg_mode);// build iq table if quadrature mode is enabled 
  if ( status == -1 )
    {
      cm_msg(MERROR,"check_psm_quad","Error return from build_iq_table_psm ");
      return status;
    }
  return 0;  // SUCCESS
}


/*------------------------------------------------------------------*/

INT check_psm_profile()
{
  INT size,status;

  /* called from check_psm_quad */

  if (ppg.hardware.psm.all_profiles_enabled)
    {
      /* "all" takes precedence over any individual profiles
       */

      printf("check_psm_profile: all profiles are enabled\n");
      if(single_shot)return 0;

      /* make sure individual profiles say disabled if "all" is enabled
       */

      size = sizeof(ppg.hardware.psm.one_f_profile_enabled);
      ppg.hardware.psm.one_f_profile_enabled = 
	ppg.hardware.psm.three_f_profile_enabled =
	ppg.hardware.psm.five_f_profile_enabled =
	ppg.hardware.psm.fref_profile_enabled =0;
    
      status = db_set_value(hDB, hPSM, "one_f profile enabled", 
			    &ppg.hardware.psm.one_f_profile_enabled, size, 1, TID_BOOL);
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"check_psm_profile",
		 "Error setting value for \"one_f profile enabled\"(%d)\n",status);
	  return -1;
	}
      
      
      status = db_set_value(hDB, hPSM, "three_f profile enabled", 
			    &ppg.hardware.psm.three_f_profile_enabled, size, 1, TID_BOOL);
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"check_psm_profile",
		 "Error setting value for \"three_f profile enabled\"(%d)\n",status);
	  return -1;
	}
      
      status = db_set_value(hDB, hPSM, "five_f profile enabled", 
			    &ppg.hardware.psm.five_f_profile_enabled, size, 1, TID_BOOL);
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"check_psm_profile",
		 "Error setting value for \"five_f profile enabled\"(%d)\n",status);
	  return -1;
	}
      
      status = db_set_value(hDB, hPSM, "fREF profile enabled", 
			    &ppg.hardware.psm.fref_profile_enabled, size, 1, TID_BOOL);
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"check_psm_profile",
		 "Error setting value for \"fREF profile enabled\"(%d)",status);
	  return -1;
	}
    }
  else
    {
      /* not "all" profiles are enabled */ 
    if(!(ppg.hardware.psm.one_f_profile_enabled ||ppg.hardware.psm.five_f_profile_enabled
	 || ppg.hardware.psm.three_f_profile_enabled || ppg.hardware.psm.fref_profile_enabled))
      {
	cm_msg(MERROR,"check_psm_profile",
	       "At least one profile must be enabled (or \"all\") ");
	return -1;
      }
    }
  return 0;
}

#ifdef PSMII
//--------------------------------------------------------
int build_iq_table_psm(char *ppg_mode)
//----------------------------------------------------
{
  /* build an iq table for the psmII; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  str[256];
  char  astr[256];
  char  fstr[12];
  BOOL  I_zero; // temp flag
  INT status, size, j,i,q;

  INT my_debug=FALSE;
  INT N, Ncic,Niq;
  double dNcic, dNtiq, dNiq, dn;
  double dNc, dNtiqtemp;

  double d_nu, d_w_req, dw;
  double a,phi,tmp,tmp2;
  INT I,Q;
  double Tp;

  // temp variables
  double x,y,z;
  double d_const;
  int  actual_bandwidth__hz_;

  double d_w_max,d_w_min,d_nu_min,d_nu_max;
  double alpha, A; /* modulation params */
  double d_Ntiqtemp;
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */

  INT Ntiqtemp, Ntiq, Ncmx, Nc;

  printf("\n  build_iq_table_psm: starting (PSM II) \n");

  /* Check the modulation mode (ln-sech or Hermite) */
  sprintf(str,"%s",ppg.hardware.psm.iq_modulation.moduln_mode__ln_sech_hermite_);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen(str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  

  if (strncmp(str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm: detected ln-sech modulation mode\n");
      alpha = ppg.output.psm_amplitude_mod_param__alpha_ = 5;
      A     = ppg.output.psm_linewidth_mod_param__a_     = 0.1;
    }
  else if (strncmp(str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm: detected Hermite modulation mode\n");
      alpha =  ppg.output.psm_amplitude_mod_param__alpha_ = 2.2;
      A =      ppg.output.psm_linewidth_mod_param__a_     = 0.39714;
    }
  else
    {
      printf("build_iq_table_psm: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported\n",
	     ppg.hardware.psm.iq_modulation.moduln_mode__ln_sech_hermite_);
      return -1;
    }

 
  size = sizeof(Ncmx);


  /* Ncmx  is a hardware param read from PSM by frontend_init (febnmr) and written into odb
     in the output directory, so that rf_config can use it.  We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", ppg.output.psm_max_cycles_iq_pairs__ncmx_);
  Ncmx =  ppg.output.psm_max_cycles_iq_pairs__ncmx_;
  
  if(Ncmx < 1 || Ncmx > 128)
    {
      printf("build_iq_table_psm: Illegal value of param Ncmx read from odb (%d), (0<Ncmx<128) \n",Ncmx);
#ifndef PSMIIB
      if(Ncmx != 128)
	{  /* PSMII gives 32 when it should give 128 */
	  printf("Ncmx is supposed to be set to 128 (PSMII) by the frontend reading value from PSM (bnmr_init)\n");
	  printf("Default value of 128 will be used\n");
	  cm_msg(MINFO,"rf_config","Ncmx has not been filled by the frontend...using default Ncmx=128");
	  ppg.output.psm_max_cycles_iq_pairs__ncmx_ =  Ncmx = 128;
	}
#else
      if(Ncmx == 0)
	{  /* PSMIIB has this problem fixed */
	  printf("Ncmx is supposed to be filled by the frontend reading value from PSM (bnmr_init)\n");
	  printf("Default value of 128 will be used\n");
	  cm_msg(MINFO,"rf_config","Ncmx has not been filled by the frontend...using default Ncmx=128");
	  ppg.output.psm_max_cycles_iq_pairs__ncmx_ =  Ncmx = 128;
	}
#endif /* PSMIIB */
      else
	return -1;
    }
  if(my_debug)
    printf("Input params: A=%f ; alpha = %f; Ncmx = %d\n",A,alpha,Ncmx);
 
  /* formulae for PSMII :  */ 
  d_w_max = ( pow(10,8)* alpha ) / (A * 512.0 * 5);  // rad/s
  /* changed from 128 to 2048 27-4-06 */
  d_w_min =  ( pow(10,8)* alpha ) / (A * 2048.0 * 63.0 * 128.0); // rad/s
  d_nu_min = d_w_min/(2*M_PI); // Hz
  d_nu_max = d_w_max/(2*M_PI); // Hz
								   
  //  if (my_debug)
    {
      printf(" d_w_min  = %.3f  d_w_max  = %.3g  rad/s\n",d_w_min,d_w_max);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }

 
  d_nu = (double)  ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_;

  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      printf("build_iq_table_psm: requested bandwidth (%dHz) is out of range\n",
	     ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_);
      printf("        bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      return -1;
    }

  d_w_req= 2 * M_PI * d_nu; 

  printf("build_iq_table_psm: requested bandwidth =%d Hz \n",
	 ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_);
  printf("build_iq_table_psm: requested bandwidth d_nu=%1e Hz;   d_w_req = %.2f rad/s\n",d_nu, d_w_req);


  d_const = ( alpha * pow(10,8)) / A ; // this is 5 * 10**9 for A=0.1 and alpha=5
/* if(my_debug) */
    printf("  d_const = %.2e; expected value = 5*10e9 \n",d_const);

  Ntiqtemp = (INT) ( d_const/d_w_req );   // nearest smallest integer

  if( (Ntiqtemp < 2560)  || ( Ntiqtemp > 16515071 ))
    {
      printf("build_iq_table_psm: preliminary no. of IQ points (Ntiqtemp=%d) is out of range\n",
	     Ntiqtemp);
      printf("      Ntiqtemp must be between 2560 and 16515071 \n");
      return -1;
    }
   
  // select Nc, Ncic from the table of Ntiqtemp values
  Nc=Ncic=0;
  status = get_IQ_params(Ntiqtemp, &Nc, &Ncic); /* for PSMII */
  if(status== -1)
    return -1;


  printf("Build_iq_table_psm: Ntiqtemp=%d, Nc =%d, Ncic=%d\n",Ntiqtemp,Nc,Ncic);
  if(Nc > Ncmx)
    {
      printf("build_iq_table_psm: Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d\n",
	     Ntiqtemp,Nc,Ncmx);
      return -1;
    }

  ppg.output.psm_cic_interpoln_rate__ncic_ = Ncic;
  ppg.output.psm_num_cycles_iq_pairs__nc_ = Nc;


  dNcic = (double) Ncic;
  dNc   = (double) Nc;
  dNtiqtemp = (double) Ntiqtemp;


  /* calculate Niq */
  tmp = dNtiqtemp/(dNcic * dNc);
  Niq = (int) (1+ tmp);  // closest larger integer
  Ntiq= Niq * Nc * Ncic;

  if(my_debug)
    printf("build_iq_table_psm: dNcic = %.1f ; dNc = %.1f;  dNtiqtemp = %.1f; tmp=%.1f  \n",
	   dNcic,dNc,dNtiqtemp,tmp);

  printf("build_iq_table_psm: Calculated Niq = %d ; Ntiq = %d \n",Niq,Ntiq);

  if(Niq < 512 || Niq > 2048)
    {
      printf("Consistency check failure; Niq=%d, must be between 512 and 2048\n",Niq);
      return -1;
    }
  if(Ntiq >= 129024 * Ncmx)
    {
      printf("Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d\n",
	     Ntiq,129024*Ncmx);
      return -1;
    }


  ppg.output.psm_num_iq_pairs__niq_ = Niq;
  dNtiq = (double) Ntiq;
  dNiq = (double) Niq;

  /* d_const = 10**10   */
  dw = d_const /dNtiq  ; /* rad/sec */
  ppg.output.psm_bandwidth__rad_per_sec_ = dw;

  x=dw/(2* M_PI);

  /* no longer output "actual bandwidth" as new calculation
     delivers the requested bandwidth */
  actual_bandwidth__hz_ = (int)(x +0.5); // as a check
  
  Tp = 2*  pow(10,-8) * dNtiq; // pulse width (sec)
  printf("Tp = %.3e sec\n",Tp);
  ppg.output.psm_pulse_width__msec_ =  Tp * pow(10,3); // pulse width (ms)
  
  printf("\n **  IQ Output parameters: *** \n");
  printf("No. IQ pairs Niq= %d;  Ntiq= %d;    Ncic=%d\n",Niq,Ntiq,Ncic);
  printf("output bandwidth dw=%.2f rad/s or %d Hz;   pulse width Tp=%.3e sec or %.3f ms \n\n",
	 dw, actual_bandwidth__hz_, Tp, ppg.output.psm_pulse_width__msec_);



  N=1;
  dn=1.0;
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,"_iq.psm"); // IQ pair file
  sprintf(astr,"%s%s%s",ppg.input.cfg_path,ppg_mode,"_iq.dbg"); // debug file

  if(my_debug)printf("build_iq_table_psm: about to open IQ table DEBUG file %s\n",str);

 

  /* If setting constant values, check they  are reasonable */
  I_zero=FALSE;
  if(ppg.hardware.psm.iq_modulation.set_constant_i_value_in_file)
    {
      if ( ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_ ==0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	      cm_msg(MERROR,"Build_iq_table_psm",
		     "Modulating with constant I=0 will result in no output for Hermite since Q=0 also");
	      printf("Build_iq_table_psm: Constant I=0 not allowed for Hermite since Q=0 (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_);
      
    }
  
  if(ppg.hardware.psm.iq_modulation.set_constant_q_value_in_file)
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	  cm_msg(MINFO,"build_iq_table_psm",
		 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
		 ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_);
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_ ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"Build_iq_table_psm",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("Build_iq_table_psm: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_);
      
    }
  
  if ( dbgfile = fopen(astr,"w"))
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Req bandwidth(Hz)=%d; output bandwidth(Hz) =%d\n",
	      ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_,
	      actual_bandwidth__hz_);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms ; Ncic=%d;\n",
	      Niq,  ppg.output.psm_pulse_width__msec_, Ncic);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);
 

  printf("build_iq_table_psm: about to open IQ table file %s\n",str);
  printf("       these will be the values programmed into 1f profile\n");
  if (iqfile = fopen(str,"w"))    
    {
      do
	{ 
	  double di,dq;
	  /* ln-sech */
	  if(mod_func == 0)
	    {
	      // sech = 1/cosh
	      z=(dw*Tp/10.0)  * ((dn/dNiq) - 0.5);
	      a = 1.0/cosh (z);
	      phi = 5.0 * log(a);
	      di= 511*a*cos(phi) ;    
	      dq= 511*a*sin(phi) ;
	    }
	  else
	    {  /* mod_func = 1, Hermite */
	      z = A * dw * Tp *  ((dn/dNiq) - 0.5) ;
	      a = pow(z,2);
	      di = 511 * (1.0 -0.957 * a) * exp(-a);
	      dq = 0;
	    }

	  if(ppg.hardware.psm.iq_modulation.set_constant_i_value_in_file)
	    I=ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_;
	  else	
	    {    
	      I = (int) (di + 0.5);  // closest integer

	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm: Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	  i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	    
	  if(ppg.hardware.psm.iq_modulation.set_constant_q_value_in_file)
	    Q=ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_;
	  else
	    Q = (int)(dq+0.5); // closest integer
	   

	  q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */


	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g  a=%g  phi=%g \n",
		   (int)dn,I,Q,i,q,di,dq,a,phi);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;
	  dn=(float)N;
	  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"Build_iq_table_psm","Error opening iq file %s ",str);
      printf("Build_iq_table_psm: Error opening iq file %s ",str);
      return -1;
    }
}
/*-----------------------------------------------------------*/
int get_IQ_params(INT Ntiqtemp, INT *pNc, INT *pNcic)
/*----------------------------------------------------------*/
{
  /* version for PSMII module */

  const int ival=4096;
  const int jval=129024;

  *pNc=*pNcic=0;

  if(Ntiqtemp < 2560)
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (min=2560)\n",
	     Ntiqtemp);
      return -1;
    }
  
  if (Ntiqtemp < ival<<1)
    *pNcic=5;
  else if (Ntiqtemp < ival<<2)
    *pNcic=8;
  else if (Ntiqtemp < ival<<3)
    *pNcic=16;
  else if (Ntiqtemp < ival <<4)
    *pNcic=32;
  else
    *pNcic=63;
  
  

  if (Ntiqtemp < jval )
    *pNc = 1 ;
  else if (Ntiqtemp < jval <<1 )
    *pNc = 2 ;
  else if (Ntiqtemp < jval <<2 )
    *pNc = 4 ;
  else if (Ntiqtemp < jval <<3 )
    *pNc = 8 ;
  else if (Ntiqtemp < jval <<4 )
    *pNc = 16 ;
  else if (Ntiqtemp < jval <<5 )
    *pNc = 32 ;
  else if (Ntiqtemp < jval <<6 )
    *pNc = 64 ;
  else if (Ntiqtemp < jval <<7 )
    *pNc = 128 ;

  else
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (max=16515071)\n",
	     Ntiqtemp);
      return -1;
    }
  
  printf("Ntiqtemp=%7.7d    Nc=%2.2d     Ncic=%2.2d\n",Ntiqtemp,*pNc,*pNcic);
  return 1;
}

#else  /* standard PSM */

//--------------------------------------------------------
int build_iq_table_psm(char *ppg_mode)
//----------------------------------------------------
{
  /* build an iq table for the psm; called from check_psm_quad 
   */
#define I_Q_DATA_MASK 0x3FF /* 10 bits */
  FILE  *iqfile;
  FILE  *dbgfile;
  char  str[256];
  char  astr[256];
  char  fstr[12];
  BOOL I_zero;

  INT status, size, j,i,q;

  INT my_debug=FALSE;
  INT N, Ncic,Niq;
  double dNcic, dNtiq, dNiq, dn;
  double dNc, dNtiqtemp;

  double d_nu, d_w_req, dw;
  double a,phi,tmp,tmp2;
  INT I,Q;
  double Tp;

  // temp variables
  double x,y,z;
  double d_const;
  int  actual_bandwidth__hz_;

  double d_w_max,d_w_min,d_nu_min,d_nu_max;
  double alpha, A; /* modulation params */
  double d_Ntiqtemp;
  INT mod_func; /* set to 0 for ln_sech, 1 for Hermite */

  INT Ntiqtemp, Ntiq, Ncmx, Nc;

  printf("\n  build_iq_table_psm: starting \n");

  /* Check the modulation mode (ln-sech or Hermite) */
  sprintf(str,"%s",ppg.hardware.psm.iq_modulation.modulation_mode);
  str[2]='\0'; /* terminate after two characters */
  for  (j=0; j< strlen(str) ; j++) 
    str[j] = tolower (str[j]); /* convert to lower case */ 
  

  if (strncmp(str,"ln-sech",2) == 0)
    {
      mod_func=0 ;
      printf("build_iq_table_psm: detected ln-sech modulation mode\n");
      alpha = ppg.output.psm_amplitude_mod_param__alpha_ = 5;
      A     = ppg.output.psm_linewidth_mod_param__a_     = 0.1;
    }
  else if (strncmp(str,"hermite",2) == 0)
    {
      mod_func = 1;
      printf("build_iq_table_psm: detected Hermite modulation mode\n");
      alpha =  ppg.output.psm_amplitude_mod_param__alpha_ = 2.2;
      A =      ppg.output.psm_linewidth_mod_param__a_     = 0.39714;
    }
  else
    {
      printf("build_iq_table_psm: Illegal psm iq mode \"%s\"; only \"ln-sech\" and \"Hermite\" are presently supported\n",
	     ppg.hardware.psm.iq_modulation.modulation_mode);
      return -1;
    }


  size = sizeof(Ncmx);


  /* Ncmx  is a hardware param read from PSM by frontend_init (febnmr) and written into odb
     in the output directory, so that rf_config can use it.  We got the record for "output" */
 
  if(debug) printf("Ncmx from output record in odb is =%d\n", ppg.output.psm_max_cycles_iq_pairs__ncmx_);
  Ncmx =  ppg.output.psm_max_cycles_iq_pairs__ncmx_;
  
  if(Ncmx < 1 || Ncmx > 32)
    {
      printf("build_iq_table_psm: Illegal value of param Ncmx read from odb (%d), (0<Ncmx<32) \n",Ncmx);
      if(Ncmx == 0)
	{
	  printf("Ncmx is supposed to be filled by the frontend reading value from PSM (bnmr_init)\n");
	  printf("Default value of 32 will be used\n");
	  cm_msg(MINFO,"rf_config","Ncmx has not been filled by the frontend...using default Ncmx=32");
	  ppg.output.psm_max_cycles_iq_pairs__ncmx_ =  Ncmx = 32;
	}
      else
	return -1;
    }
  if(my_debug)
    printf("Input params: A=%f ; alpha = %f; Ncmx = %d\n",A,alpha,Ncmx);
  
  d_w_max = ( pow(10,7)* alpha ) / (A * 512.0);  // rad/s
  if(my_debug)
    { // for checking calc.
      x = d_w_max/128;
      printf(" d_w_min = %f / Ncmx\n", x);
    }
  d_w_min = d_w_max /(128 * Ncmx); // rad/s
  d_nu_min = d_w_min/(2*M_PI); // Hz
  d_nu_max = d_w_max/(2*M_PI); // Hz
								   
  //  if (my_debug)
    {
      printf(" d_w_min  = %.3f  d_w_max  = %.3g  rad/s\n",d_w_min,d_w_max);
      printf(" d_nu_min = %.3f  d_nu_max = %.3f   Hz\n",d_nu_min,d_nu_max);
    }

 
  d_nu = (double)  ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_;

  /* check d_nu is in range */
  if( (d_nu < d_nu_min)  || (d_nu > d_nu_max) )
    {
      printf("build_iq_table_psm: requested bandwidth (%dHz) is out of range\n",
	     ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_);
      printf("        bandwidth must be between %f and %f Hz\n",d_nu_min,d_nu_max);
      return -1;
    }

  d_w_req= 2 * M_PI * d_nu; 

  printf("build_iq_table_psm: requested bandwidth =%d Hz \n",
	 ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_);
  printf("build_iq_table_psm: requested bandwidth d_nu=%1e Hz;   d_w_req = %.2f rad/s\n",d_nu, d_w_req);


  d_const = ( alpha * 2 * pow(10,7)) / A ; // this is 10**9 for A=0.1 and alpha=5
  if(my_debug)
    printf("d_const = %.2e; expected value = 10**9 \n",d_const);

  Ntiqtemp = (INT) ( d_const/d_w_req );   // nearest smallest integer

  if( (Ntiqtemp < 1024)  || ( Ntiqtemp >= 129024 * Ncmx ))
    {
      printf("build_iq_table_psm: preliminary no. of IQ points (Ntiqtemp=%d) is out of range\n",
	     Ntiqtemp);
      printf("      Ntiqtemp must be between 1024 and %d \n",( 129024 * Ncmx));
      return -1;
    }
   
  // select Nc, Ncic from the table of Ntiqtemp values
  Nc=Ncic=0;
  status = get_IQ_params(Ntiqtemp, &Nc, &Ncic);
  if(status== -1)
    return -1;


  printf("Build_iq_table_psm: Ntiqtemp=%d, Nc =%d, Ncic=%d\n",Ntiqtemp,Nc,Ncic);
  if(Nc > Ncmx)
    {
      printf("build_iq_table_psm: Calculated Nc=%d for Ntiqtemp=%d is out of range; must be <= Ncmx=%d\n",
	     Ntiqtemp,Nc,Ncmx);
      return -1;
    }

  ppg.output.psm_cic_interpoln_rate__ncic_ = Ncic;
  ppg.output.psm_num_cycles_iq_pairs__nc_ = Nc;


  dNcic = (double) Ncic;
  dNc   = (double) Nc;
  dNtiqtemp = (double) Ntiqtemp;


  /* calculate Niq */
  tmp = dNtiqtemp/(dNcic * dNc);
  Niq = (int) (1+ tmp);  // closest larger integer
  Ntiq= Niq * Nc * Ncic;

  if(my_debug)
    printf("build_iq_table_psm: dNcic = %.1f ; dNc = %.1f;  dNtiqtemp = %.1f; tmp=%.1f  \n",
	   dNcic,dNc,dNtiqtemp,tmp);

  printf("build_iq_table_psm: Calculated Niq = %d ; Ntiq = %d \n",Niq,Ntiq);

  if(Niq < 512 || Niq > 2048)
    {
      printf("Consistency check failure; Niq=%d, must be between 512 and 2048\n",Niq);
      return -1;
    }
  if(Ntiq >= 129024 * Ncmx)
    {
      printf("Consistency check failure; Ntiq=%d, must be less than 129024*Ncmx = %d\n",
	     Ntiq,129024*Ncmx);
      return -1;
    }


  ppg.output.psm_num_iq_pairs__niq_ = Niq;
  dNtiq = (double) Ntiq;
  dNiq = (double) Niq;

  /* d_const = 10**9   */
  dw = d_const /dNtiq  ; /* rad/sec */
  ppg.output.psm_bandwidth__rad_per_sec_ = dw;

  x=dw/(2* M_PI);

  /* no longer output "actual bandwidth" as new calculation
     delivers the requested bandwidth */
  actual_bandwidth__hz_ = (int)(x +0.5); // as a check
  
  Tp =  pow(10,-7) * dNtiq; // pulse width (sec)
  printf("Tp = %.3e sec\n",Tp);
  ppg.output.psm_pulse_width__msec_ =  Tp * pow(10,3); // pulse width (ms)
  
  printf("\n **  IQ Output parameters: *** \n");
  printf("No. IQ pairs Niq= %d;  Ntiq= %d;    Ncic=%d\n",Niq,Ntiq,Ncic);
  printf("output bandwidth dw=%.2f rad/s or %d Hz;   pulse width Tp=%.3e sec or %.3f ms \n\n",
	 dw, actual_bandwidth__hz_, Tp, ppg.output.psm_pulse_width__msec_);



  N=1;
  dn=1.0;
  sprintf(str,"%s%s%s",ppg.input.cfg_path,ppg_mode,"_iq.psm"); // IQ pair file
  sprintf(astr,"%s%s%s",ppg.input.cfg_path,ppg_mode,"_iq.dbg"); // debug file


  /* If setting constant values, check they  are reasonable 
     For standard PSM,  
     ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_ and
     ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_ are both 0.

*/
  I_zero=FALSE;
  if(ppg.hardware.psm.iq_modulation.set_constant_i_value_in_file)
    {
      if ( ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_ ==0)
	{
	  I_zero=TRUE;
	  if(mod_func ==1) // Hermite: all Q values are zero
	    {
	         cm_msg(MERROR,"Build_iq_table_psm",
		     "Modulating with constant I=0 will result in no output for Hermite since Q=0 also");
	      printf("Build_iq_table_psm: Constant I=0 not allowed for Hermite since Q=0 (no output)\n");
	      return -1;
	    }
	}
      else
	printf("All i values will be set to %d\n",
	       ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_);
      
    }
  if(ppg.hardware.psm.iq_modulation.set_constant_q_value_in_file)
    {    
      if(mod_func == 1  )
	{  // Hermite; all Q values are zero
	    cm_msg(MINFO,"build_iq_table_psm",
	  	 "Modulating with constant Q=%d has no effect since Q=0 always for Hermite ",
	   ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_);
	  printf("Build_iq_table_psm: no effect from modulating with constant Q=%d  since Q=0 always for Hermite\n",	 ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_);
		 
	}
      else
	{ // ln-sech; check for I=Q=0
	  if ( ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_ ==0 )
	    {
	      if(I_zero)
		{
		  cm_msg(MERROR,"Build_iq_table_psm",
			 "Modulating with constant I,Q pair=(0,0) will result in no output");
		  printf("Build_iq_table_psm: Constant I=0 and Q=0 are not allowed (no output)\n");
		  return -1;
		}
	    }
	}
      printf("All q values will be set to %d\n",
	     ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_);
      
    }
  

  if(my_debug)printf("build_iq_table_psm: about to open IQ table DEBUG file %s\n",str);

  if ( dbgfile = fopen(astr,"w"))
    {
      printf("successfully opened debug file: %s\n",astr);
      sprintf(astr,"Req bandwidth(Hz)=%d; output bandwidth(Hz) =%d\n",
	      ppg.hardware.psm.iq_modulation.requested_bandwidth__hz_,
	      actual_bandwidth__hz_);
      fputs(astr,dbgfile);

      sprintf(astr,"Num IQ pairs=%d; pulsewidth Tp =%f ms ; Ncic=%d;\n",
	      Niq,  ppg.output.psm_pulse_width__msec_, Ncic);
      fputs(astr,dbgfile);

      sprintf(astr,"Index: I      Q              i    q (i,q=masked 2's comp) %s","\n"); 
      fputs(astr,dbgfile);
    }
  else
    printf("could not open debug file %s\n",astr);

  printf("build_iq_table_psm: about to open IQ table file %s\n",str);
  if (iqfile = fopen(str,"w"))    
    {
      do
	{ 
	  double di,dq;
	  /* ln-sech */
	  if(mod_func == 0)
	    {
	      // sech = 1/cosh
	      z=(dw*Tp/10.0)  * ((dn/dNiq) - 0.5);
	      a = 1.0/cosh (z);
	      phi = 5.0 * log(a);
	      di= 511*a*cos(phi) ;    
	      dq= 511*a*sin(phi) ;
	    }
	  else
	    {  /* mod_func = 1, Hermite */
	      z = A * dw * Tp *  ((dn/dNiq) - 0.5) ;
	      a = pow(z,2);
	      di = 511 * (1.0 -0.957 * a) * exp(-a);
	      dq = 0;
	    }

	  if(ppg.hardware.psm.iq_modulation.set_constant_i_value_in_file)
	    I=ppg.hardware.psm.iq_modulation.const_i__max_plus_or_minus_511_;
	  else
	    {
	      I = (int) (di + 0.5);  // closest integer
 
	      // Check I value when N=Niq/2
	      if(N==Niq/2)
		{
		  if (I != 510 && I != 511)
		    {
		      printf("build_iq_table_psm: Error I=%d for N=Niq/2=%d; expect 510 or 511\n",I,N);
		      return -1;
		    }
		}
	    }

	  i=(~I+1) & I_Q_DATA_MASK; /* 2's complement and mask to 10 bits */
	    



	  if(ppg.hardware.psm.iq_modulation.set_constant_q_value_in_file)
	    Q=ppg.hardware.psm.iq_modulation.const_q__max_plus_or_minus_511_;
	  else
	    Q = (int)(dq+0.5); // closest integer

	  q=(~Q+1) & I_Q_DATA_MASK;/* 2's complement and mask to 10 bits */

	  if(my_debug)
	    printf("n=%d I,Q pair = %d,%d ->i,q pair =%d,%d; di=%g  dq=%g  a=%g  phi=%g \n",
		   (int)dn,I,Q,i,q,di,dq,a,phi);
 	  if(dbgfile)
	    {
	      sprintf(astr,"%d:  %d %d   %d  %d %s",N,I,Q,i,q,"\n"); 
	      fputs(astr,dbgfile);
	    }

	  sprintf(str,"%d %d%s",i,q,"\n"); // IQ file contains 2s compliment data
	  fputs(str,iqfile);
	  
	  N++;
	  dn=(float)N;
	  
	}
      while ( N <= Niq ); 
  
      fclose(iqfile);
      if(dbgfile)fclose(dbgfile);
      return i;
    }
  else
    {
      cm_msg(MERROR,"Build_iq_table_psm","Error opening iq file %s ",str);
      printf("Build_iq_table_psm: Error opening iq file %s ",str);
      return -1;
    }
}


/*-----------------------------------------------------------*/
int get_IQ_params(INT Ntiqtemp, INT *pNc, INT *pNcic)
/*----------------------------------------------------------*/
{

  const int ival=4096;
  const int jval=129024;

  *pNc=*pNcic=0;

  if(Ntiqtemp < 1024)
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (min=1024)\n",
	     Ntiqtemp);
      return -1;
    }

  if (Ntiqtemp < ival)
    *pNcic=2;
  else if (Ntiqtemp < ival<<1)
    *pNcic=4;
  else if (Ntiqtemp < ival<<2)
    *pNcic=8;
  else if (Ntiqtemp < ival<<3)
    *pNcic=16;
  else if (Ntiqtemp < ival <<4)
    *pNcic=32;
  else
    *pNcic=63;
  
  

  if (Ntiqtemp < jval )
    *pNc = 1 ;
  else if (Ntiqtemp < jval <<1 )
    *pNc = 2 ;
  else if (Ntiqtemp < jval <<2 )
    *pNc = 4 ;
  else if (Ntiqtemp < jval <<3 )
    *pNc = 8 ;
  else if (Ntiqtemp < jval <<4 )
    *pNc = 16 ;
  else if (Ntiqtemp < jval <<5 )
    *pNc = 32 ;

  else
    {
      printf("get_IQ_params: preliminary no. of IQ points (Ntiqtemp=%d) is out of range (max=4128767)\n",
	     Ntiqtemp);
      return -1;
    }
  
  //  printf("Ntiqtemp=%7.7d    Nc=%2.2d     Ncic=%2.2d\n",Ntiqtemp,Nc,Ncic);
  return 1;
}
#endif /* PSM module */
//+++++++++++++++++++++++++++++++++++++
INT check_psm_gate(void)
{
  INT i;
  
  i=0;
  if(ppg.hardware.psm.gate_control.disable_front_panel_gate)
    i++;
  if(ppg.hardware.psm.gate_control.enable_default_mode)
    i++;
  if(ppg.hardware.psm.gate_control.invert_gate_pulse)
    i++;
  if(ppg.hardware.psm.gate_control.internal_gate_always_on)
    i++;

  if(i<=0)
    {
      cm_msg(MERROR,"check_psm_gate","at least one gate mode must be set for PSM");
      return -1;
    }
  else if( i > 1)
    {
      cm_msg(MERROR,"check_psm_gate","only ONE gate mode can be set for PSM (%d)",i);
      return -1;
    }
  else
    {
      if(ppg.hardware.psm.quadrature_modulation_mode)
	{
	  if(!ppg.hardware.psm.gate_control.enable_default_mode)
	    {
	      cm_msg(MERROR,"check_psm_gate",
		     "PSM Gate in quadrature mode must have default mode enabled");
	      return -1;
	    }
	  
	}
    }
  printf("check_psm_gate: success - returning 0\n");
  return 0; // SUCCESS
}
#endif
/*------------------------------------------------------------------*/
double mult (double a,DWORD b)
{
 DWORD  d = 0;
 double x = 0;
 while (d<b)
  {d++;
   x = x + a;
  }
return x;
}



/*------------------------------------------------------------------*/
INT first_match(SUBSTRING *c, INT n)
{
  INT   k, klow = -1;
  char *ptmp;
  
  for(k=0 ; k<n ; k++)
  {
    if (c[k].p != NULL)
    {
      if (klow == -1)     ptmp = c[k].p;
      if (c[k].p <= ptmp) klow = k;
    }
  }
  return klow;
}

/*------------------------------------------------------------------*/
INT substitute(char *rulefile
	       , char *inpath
	       , char *outpath, char *outfile)
{
  INT    n_element, k, j, status = 1;
  char   str[256], *pb, *pp, infile[128];
  FILE   *inf=NULL, *outf=NULL, *rulef=NULL;


  /* init structure */
  memset(c, 0, N_SUBSTRINGS*sizeof(SUBSTRING));

  /* Open config file */
  rulef = fopen(rulefile, "r");
  if (rulef == NULL)
  {
    cm_msg(MERROR,"substitute","can't open configuration file:  %s", rulefile);
    return -4;
  }

  /* get in and out files path (read from rulefile) */
  fgets(infile, 128, rulef);
  infile[strlen(infile)-1] = '\0';
  if (debug) printf("rulef: Input template filename:%s\n",infile);

  fgets(outfile, 128, rulef);
  outfile[strlen(outfile)-1] = '\0';
  if (debug) printf("rulef: Output file:%s\n",outfile);

  /* Open infile  template file */
  sprintf(str,"%s%s", inpath, infile);
  inf = fopen(str, "r");
  if (inf == NULL)
  {
    status = -1;
    cm_msg(MERROR,"substitute","can't open (input) template file: %s", str);  
    cm_msg(MERROR,"substitute","template filename may not exist due to illegal combination of input parameters", str);  
    goto in_error;
  }

  /* Open outfile file */
  sprintf(str,"%s%s", outpath, outfile);
  outf = fopen(str, "w");
  if (outf == NULL)
  {
    status = -1;
    cm_msg(MERROR,"substitute","can't create configuration Sequencer file: %s", str);
    goto in_error;
  }

  /* store substitution strings */
  k = 0;
  while (fgets(str, 128, rulef) != NULL)
  {
    if (debug) printf("rulef:%s",str);
    pb = (char *) str;
    pp = strstr(pb, "%");
    if (pp == NULL)
    {
      status = -1;
      cm_msg(MERROR,"substitute","wrong rule format %s", str);
      goto in_error;
    }
    c[k].sl = pp-pb;
    strncpy(c[k].s, pb, c[k].sl);
    c[k].rl = strlen(pp)-2;
    strncpy(c[k].r, pp+1, c[k].rl);
    k++;
  }
  n_element = k;

  if (debug)
  {
    for (k=0;k<n_element;k++)
      printf("k:%d *p:%p :%d-s:%s / :%d-r:%s\n", k, c[k].p, c[k].sl, c[k].s, c[k].rl, c[k].r);
  }

  fclose(rulef); rulef = NULL;

  while (fgets(str, 128, inf) != NULL)
  {
    pb = (char *) str;

    /* mark string to be replaced */
    for(k=0;k<n_element;k++)
      c[k].p = strstr(str, c[k].s);

    /* replace ALL substring in that line */
    for (k=0;k<n_element;k++)
    {
      /* find first one */
      j = first_match(c, n_element);
      if (j < 0)
      {
	/* No substitution to be done in that line (last part) */
	fputs(pb , outf);
	break;
      }
      else
      {
	/* first part */
	for (; pb < c[j].p ; pb++)
	  fputc(*pb , outf);
	
	/* substitution part */
	fputs(c[j].r , outf);

	/* substitution done */
	c[j].p = NULL;
	
	/* adjust source pointer */
	pb += c[j].sl;
      }
    }
  }

 in_error:
  if (rulef) fclose(rulef);
  if (inf) fclose(inf);
  if (outf) fclose(outf);
  if (status < 1)
    return status;

  if (debug)
    {
      printf("\nRule   file :%s\n", rulefile);
      printf("Input  file :%s%s\n", inpath,infile);
      printf("Output file :%s%s\n", outpath,outfile);
    }
  return SUCCESS;
}


/*------------------------------------------------------------------*/
INT settings_rec_get(void)
{
  /* retrieve the ODB structure
     check if the rule file exists in the given dir
     return the rulefile
  */
  INT size, status,j;
  char str[128];


 
  /* get the record for mdarc area (we already found the key in main) */
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMdarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"settings_rec_get","Failed to retrieve mdarc record  (%d) size=%d ",status,size);
    return(-1); /* error */
  }
  else
    if(debug)printf("Got the record for mdarc\n");

  /* get record for sis test mode */
  size = sizeof(ppg.sis_test_mode);
  status = db_get_record(hDB, hTest, &ppg.sis_test_mode, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for sis test mode  size=%d, struct size=%d (%d))",
	     size, sizeof(ppg.sis_test_mode),status);
      return (-1);
    }
  else
    if(debug)printf("Got the record for sis test mode\n");

 /* get record for hardware */
  size = sizeof(ppg.hardware);
  status = db_get_record(hDB, hH, &ppg.hardware, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for hardware  size=%d, struct size=%d (%d))",
	     size, sizeof(ppg.hardware),status);
      return (-1);
    }
  else
    if(debug)printf("Got the record for hardware\n");



#ifdef PSM
 /* get record for hardware/psm */
  size = sizeof(ppg.hardware.psm);
  status = db_get_record(hDB, hPSM, &ppg.hardware.psm, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for hardware/psm  size=%d, struct size=%d (%d))",
	     size, sizeof(ppg.hardware.psm),status);
      return (-1);
    }
  else
    if(debug)printf("Got the record for hardware/psm\n");
#endif

  /* get the record for hOut */
  size = sizeof(ppg.output);
  status = db_get_record(hDB, hOut, &ppg.output, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for Output  (%d)",status); 
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Output  (%d)",status);
      return (-1);

    }

#ifdef PSM
  if(debug)
    printf("settings_rec_get: successfully got the record for output, Ncmx=%d\n", 
	   ppg.output.psm_max_cycles_iq_pairs__ncmx_);
#endif

  /* get the record for hIn */
  size = sizeof(ppg.input);
  status = db_get_record(hDB, hIn, &ppg.input, &size, 0);
  if (status != DB_SUCCESS)
  {
    if(debug)printf("settings_rec_get:Failed to retrieve record for Input  (%d)",status); 
    cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Input  (%d)",status);
    return (-1);
  }
  return SUCCESS;
}


INT check_params(char *ppg_mode)
{
 INT size, status,j;
  char str[128];

  if(debug)printf("settings_rec_get: num histos=%d (type1) or %d (type2) \n",
		  ppg.input.num_type1_frontend_histograms, ppg.input.num_type2_frontend_histograms );


  if (ppg.input.ppg_path[0] != 0)
    if (ppg.input.ppg_path[strlen(ppg.input.ppg_path)-1] != DIR_SEPARATOR)
      strcat(ppg.input.ppg_path, DIR_SEPARATOR_STR);

  if (ppg.input.cfg_path[0] != 0)
    if (ppg.input.cfg_path[strlen(ppg.input.cfg_path)-1] != DIR_SEPARATOR)
      strcat(ppg.input.cfg_path, DIR_SEPARATOR_STR);

  ppg.output.ppg_loadfile[0] = '\0';
  ppg.output.ppg_template[0] = '\0';

  /* make sure it's in lower case */
  if (ppg.input.experiment_name[0] != 0)
    {
      sprintf(ppg_mode,"%s",ppg.input.experiment_name);
      ppg_mode[2]='\0'; /* terminate after two characters */
      for  (j=0; j< strlen(ppg_mode) ; j++) 
	ppg_mode[j] = tolower (ppg_mode[j]); /* convert to lower case */  
      // ppg_mode is returned to caller
      
      
      // TYPE2  modes
      /* For PSM and BNMR only (odb has to support fsc as well)
	 Modes 20 and 2d:  copy ..input/e00_rf_frequency__hz into PSM idle frequency. 
	 FSC uses e00_rf_frequency__hz.       
      */
#ifdef COPY_ODB
      printf("********* COPY_ODB is defined\n");
#ifdef PSM
      {
	BOOL tflag;
	tflag=FALSE;
#endif
#else
  printf("******* COPY_ODB is NOT defined\n");	
#endif

	/* Detect dummy mode 20 -> reverts to 00 */
	if (strcmp(ppg_mode,"20")==0 )
	  {
	    cm_msg(MINFO,"settings_rec_get","Mode 20: ppg mode is actually 00"); 
	    sprintf(ppg_mode,"00"); /* mode is really 00 */
#ifdef COPY_ODB
#ifdef PSM
	    tflag=TRUE;
	    ppg.hardware.psm.idle_freq__hz_ =  ppg.input.e00_rf_frequency__hz_ ;
#endif
#endif
	  }
	else if (strcmp(ppg_mode,"2d")==0 )
	  {
	    cm_msg(MINFO,"settings_rec_get","Mode 2d: ppg templates for 1b will be used");
#ifdef COPY_ODB
#ifdef PSM 
	    tflag=TRUE;
	    ppg.hardware.psm.idle_freq__hz_ =  ppg.input.e00_rf_frequency__hz_;
#endif 
#endif
	  }
#ifdef COPY_ODB
#ifdef PSM
	if(tflag)
	  {
	    size=sizeof( ppg.hardware.psm.idle_freq__hz_);
	    status = db_set_value(hDB, hPSM, "Idle freq (Hz)", 
				  &ppg.hardware.psm.idle_freq__hz_, size, 1, TID_DWORD);
	    if (status != SUCCESS)
	      {
		cm_msg(MERROR, "settings_rec_get", 
		       "cannot set psm idle freq at path \"..psm/Idle freq (Hz)\" to e00_rf_frequency=%d (%d) ", 
		       ppg.input.e00_rf_frequency__hz_,status);
		return status;
	      }
	    printf("settings_rec_get: copied e00_rf_frequency=%d into PSM idle frequency\n",
		   ppg.input.e00_rf_frequency__hz_);
	  }
      }
#endif
#endif
      
      // TYPE 1 modes
      /* Detect dummy modes 1d/1e/1h -> revert to 1n */
      sprintf(ppg.output.e1n_epics_device,"none") ;   /* initialize for all modes */
      if (strcmp(ppg_mode,"1n")==0 )
	{
	  cm_msg(MINFO,"settings_rec_get","Mode 1n: epics scan device is NaCell"); 
	  sprintf(ppg.output.e1n_epics_device,"NaCell") ; /* write epics device */
	}
      else if (strcmp(ppg_mode,"1d")==0 )
	{
	  cm_msg(MINFO,"settings_rec_get","Mode 1d: setting epics scan device to  Laser & resetting ppg mode to 1n"); 
	  sprintf(ppg_mode,"1n"); /* mode is really 1n */
	  sprintf(ppg.output.e1n_epics_device,"Laser") ; /* write epics device */
	}
      else if (strcmp(ppg_mode,"1e")==0 )
	{
	  cm_msg(MINFO,"settings_rec_get","Mode 1e: setting epics scan device to  Field & resetting ppg mode to 1n"); 
	  sprintf(ppg_mode,"1n"); /* mode is really 1n */
	  sprintf(ppg.output.e1n_epics_device,"Field") ; /* write epics device */
	}
#ifdef POL
      else if (strcmp(ppg_mode,"1h")==0 )
	{
	  cm_msg(MINFO,"settings_rec_get","Mode 1h: setting scan device to DAC & resetting ppg mode to 1n"); 
	  sprintf(ppg_mode,"1n"); /* mode is really 1n */
	  sprintf(ppg.output.e1h_scan_device,"DAC") ; /* write scan device */
	}
#endif
      /* Detect dummy mode 10 (1f with dummy frequency scan) */
      else if (strcmp(ppg_mode,"10")==0 )
	{
	  cm_msg(MINFO,"settings_rec_get","Mode 10: dummy frequency scan; resetting ppg mode to 1f"); 
	  sprintf(ppg_mode,"1f"); /* mode 10 is really 1f */
	}
#ifdef CAMP
      else if (  (strcmp(ppg_mode,"1c")==0) || (strcmp(ppg_mode,"1j")==0 ))
	{
	  printf("CAMP mode %s is selected; getting CAMP record\n",ppg_mode);
	  status=camp_get_rec() ;
	  if(status != SUCCESS)
	    {
	      if (debug) printf("settings_rec_get: failure after camp_get_rec\n");
	      return(status);
	    }
	}
#endif // CAMP
#ifdef COPY_ODB  // BNMR isdaq01 has to also support FSC.
#ifdef PSM
      else  if ((strcmp(ppg_mode,"1b")==0 ) ||(strcmp(ppg_mode,"1a")==0  ))
	{    
	  ppg.hardware.psm.fref_tuning_freq__hz_ =  ppg.input.e00_rf_frequency__hz_;
	  
	  size=sizeof( ppg.hardware.psm.fref_tuning_freq__hz_);
	  status = db_set_value(hDB, hPSM, "Fref tuning freq (Hz)", 
				&ppg.hardware.psm.fref_tuning_freq__hz_, size, 1, TID_DWORD);
	  if (status != SUCCESS)
	    {
	      cm_msg(MERROR, "settings_rec_get", 
		     "cannot set psm tuning freq at path \"..psm/Fref tuning freq (Hz)\" to e00_rf_frequency=%d (%d) ", 
		     ppg.input.e00_rf_frequency__hz_,status);
	      return status;
	    }
	  else
	    printf("settings_rec_get: set \"..psm/Fref tuning freq (Hz)\" to e00_rf_frequency=%d (%d) \n", ppg.input.e00_rf_frequency__hz_);
	}
#endif
#endif
#ifdef POL
      {
	/* Get the Bias Source from the edit-on-start area of odb  */
	char path[]="/Experiment/Edit on start/Source HV Bias";
	int len;
	size = sizeof(str);  
	status = db_get_value(hDB, 0, path, str, &size, TID_STRING, FALSE);
	if(status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"settings_rec_get","cannot get Bias Source at %s (%d)",path,status);
	    return -1;
	  }
	len=(strlen(str));
	if(len ==0 )
	  {
	    cm_msg(MERROR,"settings_rec_get","empty string for Bias Source at %s ",path);
	    return -1;
	  }

	printf("Bias source: %s\n",str);
	ppg.output.bias_source_code= -1;

	for  (j=0; j< len; j++)
	  str[j]  = toupper (str[j] );

	if (strncmp(str,"OLIS",4)==0 ) 
	  ppg.output.bias_source_code=0 ; /* write bias code 0=OLIS */
	else if (strncmp(str,"ISAC",4)==0 )
	  {
	    if (strchr(str,'W'))
	      ppg.output.bias_source_code=1 ; /* write bias code 1 = ISAC-WEST */
	    else if  (strchr(str,'E'))
	      ppg.output.bias_source_code=2 ; /* write bias code 2 = ISAC-EAST */
	  }
	if (  ppg.output.bias_source_code == -1 )
	  {
	    cm_msg(MERROR,"settings_rec_get",
		   "Invalid string for Bias Source (\"%s\"). Enter one of OLIS, ISAC-WEST or ISAC-EAST ",str);
	    return -1;
	  }
      }
#endif
      return 1; // success
    } // ppg.input.experiment_name
  else
    {
      cm_msg(MERROR,"settings_rec_get","Experiment name from ODB is empty");
      return -1;
    }
}
#ifdef CAMP
/* ------------------------------------------------------------------*/
INT camp_create_rec(void)
{
  /* retrieve the ODB structure for CAMP sweep device
  */
  INT size, status,j;
  char str[128];
  FIFO_ACQ_CAMP_SWEEP_DEVICES_STR(fifo_acq_camp_sweep_devices_str); /* for camp (from experim.h) */


  /* get the key hCamp  */
  sprintf(str,"/Equipment/%s/camp sweep devices",eqp_name); 
  status = db_find_key(hDB, 0, str, &hCamp);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("camp_create_rec: Failed to find the key %s ",str);
      
      /* Create record for camp area */     
      if(debug) printf("camp_create_rec:Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_devices_str));
      if (status != DB_SUCCESS)
	{
	  if(debug)printf("camp_create_rec: Failure creating camp record\n");
	  cm_msg(MINFO,"camp_create_rec","Could not create record for %s  (%d)\n", str,status);
	  return(-1);
	}
      else
	if(debug) printf("camp_create_rec: Success from create record for %s\n",str);
      /* try again to get the key hCamp  */
      status = db_find_key(hDB, 0, str, &hCamp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_create_rec", "Failed to get the key %s ",str);
	  return(-1);
	}
    }    
  else  /* key hCamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hCamp, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "camp_create_rec", "error during get_record_size (%d) for camp",status);
	  return status;
	}
      printf("camp_create_rec - Size of camp saved structure: %d, size of camp record: %d\n", 
	     sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICES) ,size);
      if (sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICES) != size) 
	{
	  cm_msg(MINFO,"camp_create_rec","creating record (camp; mismatch between size of structure (%d) & record size (%d)", 
		 sizeof(FIFO_ACQ_CAMP_SWEEP_DEVICES) ,size);
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(fifo_acq_camp_sweep_devices_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"camp_create_rec","Could not create camp record (%d)\n",status);
	      return status;
	    }
	  else
	    if (debug)printf("camp_create_rec: Success from create record for %s\n",str);
	}
    }
  return(SUCCESS);
}

/* ------------------------------------------------------------------*/
INT camp_get_rec(void)
{
  /* retrieve the ODB structure for CAMP sweep device
  */
  INT size, status,j;
  char str[128];

  /* check we have a key hCamp (found in main)  */
  if( hCamp)
    {
 /* get the record for camp area  */
      size = sizeof(fcamp);
      status = db_get_record (hDB, hCamp, &fcamp, &size, 0);/* get the whole record for mdarc */
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"camp_get_record","Failed to retrieve %s record  (%d) size=%d ",str,status,size);
	  return(-1); /* error */
	}
      else
	if(debug)printf("Got the record for camp\n");
    }
  else
    {
      cm_msg(MERROR,"camp_get_record","No key has been found for camp record ");
      return(-1); /* error */
    }
  return(status);
}
#endif // ifdef CAMP

/*------------------------------------------------------------------*/
INT expt_search(char *expt_name)
{
  /* search for a matching ext computation function pointer
     return index in ecode struct
  */

  INT k=0, code = -1;

  while (expt[k].name[0] != 0)
  {
    if (strncmp(expt[k].name, expt_name, 2) == 0)
      break;
    k++;
  }
  return k;
}

/*--db----------------------------------------------------------------*/
INT file_wait(char *path, char *file)
{
  /* wait for compiled file */
  INT    j, nfile;
  char   *list = NULL;

  j = 0;
  do {
    ss_sleep(500);
    nfile = ss_file_find(path, file, &list); 
    j++;
  } while(j<5 && nfile != 1);

  if (nfile != 1)
  {
   cm_msg(MERROR,"file_wait","File not found (%s%s)", path, file);
   return -6;  
  }
  else
    if(debug)printf("file_wait: found file %s%s\n",path,file);
  return 1;
}

/*------------------------------------------------------------------*/
INT tr_precheck(INT run_number, char *error)
{
  INT status;
  BOOL flag;

  /* get settings all records */
  status = settings_rec_get();/* returns -1 for fail, or SUCCESS */
  if (status < 0)
    {
      printf("tr_precheck: Error return after settings_rec_get. See odb messages for details\n");
      return status;
    }

#ifdef EPICS_ACCESS

  printf("tr_precheck: disable epics checks flag = %d\n",ppg.hardware.disable_epics_checks);
  if(ppg.hardware.disable_epics_checks)
    {
      printf("tr_precheck: Epics will not be checked (epics checking disabled in odb)\n");
      return SUCCESS;
    }

  status = epics_check();
  if(status != SUCCESS)
    printf("tr_precheck: error from checking Epics channels\n");
  else
    printf("tr_precheck: success after checking Epics channels\n");
#endif
  return status;
}

/*------------------------------------------------------------------*/
INT tr_prestart(INT run_number, char *error)
{
  INT   n_element, k, size, status;
  INT   expt_code;
  char  rulefile[256], frulefile[256];
  char  outfile[256] , foutfile[256]  , cmd[512];
  char  infile[256]  , finfile[256], fscfile[256];
  char  ppg_mode[30];

  char  infofile[256];
  FILE *errfile;
  char str[256];

  time_t timbuf;
  char timbuf_ascii[30];
  DWORD elapsed;
  char path[80];
  INT pid,i;
  BOOL flag;
  char client_str[128];
  
  struct stat stat_buf;   
 // Note: called with run_number=10 for single loop; why 10? 10 is not a valid rn for bnmr/bnqr fortunately)
 //     using single_shot which is true for single loop
  if(!single_shot)
    {
      printf("tr_prestart: setting client flag for rf_config to false\n"); 
      // except on single loop mode....
      /* first set the client flag to FALSE in case of failure */
      sprintf(client_str,"/equipment/%s/client flags/rf_config",eqp_name );
      flag = FALSE;
      status = db_set_value(hDB, 0, client_str, &flag, sizeof(flag), 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_prestart", "cannot clear status flag at path \"%s\" (%d) ",client_str,status);
	  return status;
	}
    }
  else
    printf("tr_prestart: single loop mode, not setting client flag false\n"); 

  status = check_params(ppg_mode); /* returns ppg_mode = experiment_name in lower case (e.g. 1n_) */
  if(debug)printf("tr_prestart: received %s for ppg_mode from check_params\n",ppg_mode);
  if (status < 0)
    {
      printf("tr_prestart: Error return after check_params. See odb messages for details\n");
      return status;
    }

#ifndef POL
  printf("tr_prestart: MIDBNMR  use_defaults=%d and no. regions=%d\n",
	 fmdarc.histograms.midbnmr.use_defaults, fmdarc.histograms.midbnmr.number_of_regions);

#endif
  /* single and dual channel mode: now PPG for Type 2 is no longer free-running,
     so set num_beam_acq_cycles to 0 so we can use the existing PPG templates
     for SINGLE channel mode. We will need new templates for dual channel mode.
  */
  printf("dual channel mode is enabled... setting loop counter to 0 for PPG files\n");
  ppg.input.num_beam_acq_cycles=0;/* Type 2 now does one cycle only */
    
  printf("ppg.input.num_beam_acq_cycles = %d\n",ppg.input.num_beam_acq_cycles);
  
  
  /* Retrieve expt code */
  status = 0;
  expt_code = expt_search(ppg_mode);
  if(debug)printf("tr_prestart: ppg mode supplied=%s, expt_code=%d\n",ppg_mode,expt_code);
  if ((expt_code >= 0) && (expt_code < MAX_BNMR_EXPT))     
    status = expt[expt_code].op_func(ppg_mode);
  printf("tr_prestart: status returned is %d\n",status); 
  if(status <= 0)
    {
      if(status == 0)
	{
	  cm_msg(MERROR,"tr_prestart","PPG mode not found (%s)", ppg_mode);
	  printf("tr_prestart: PPG mode not found (%s)\n", ppg_mode);
	}
      else
	{
	  cm_msg(MERROR,"tr_prestart","Computation error. Check validity of your input parameters");
	  printf("tr_prestart: error return after calling subroutine e%s_compute; see odb messages for details\n",
		 ppg_mode);
	}
      return -19;
    }
  

  /* Do substitution */
  sprintf(rulefile,"%s%s.rule", ppg.input.cfg_path, ppg_mode);
  printf("tr_prestart: about to call substitute with rulefile: %s\n",rulefile);
  status = substitute(rulefile
		      , ppg.input.ppg_path
		      , ppg.input.cfg_path, outfile);
  if (status < 1)
   goto in_error;

#ifdef PSM
  if(strcmp(ppg_mode,"1j")==0 || strcmp(ppg_mode,"1c")==0 ||  strcmp(ppg_mode,"1n"))
    { 
      if(debug)
	printf("tr_prestart: not building iq table since RF is not used in this mode (%s)\n",
	       ppg_mode);
    }
  else
    {
      // build iq table if quadrature mode is enabled 
      if(ppg.hardware.psm.quadrature_modulation_mode)
	{  // quad mode
	  if (ppg.hardware.psm.all_profiles_enabled)
	    {
	      i=  build_iq_table_psm(ppg_mode);
	      if ( i == -1 )
		{
		  cm_msg(MERROR,"tr_prestart","Error return from build_iq_table_psm ");
		  return -1;
		}
	    }
	  else
	    {
	      cm_msg(MERROR,"tr_prestart","All profiles must be enabled if Quadrature Mode is selected ");
	      return -1;
	    }
	} // end of quad mode
    }
#endif
  /* update internal structure */
  strcpy(ppg.output.ppg_loadfile, outfile);

in_error:
  if (status < 1)
    {
      printf("tr_prestart: Error return from substitute. See odb messages for information\n");
      return status;
    }
  /* remove old bytecode.dat from the CFG path */
  sprintf(foutfile,"%sbytecode.dat", ppg.input.cfg_path);
  //printf("foutfile=%s\n",foutfile);
  status = ss_file_remove(foutfile);
  if (status != 0)
      cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);


  /* update odb parameters */
  ppg.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (ppg.output.compiled_file_time,"file deleted"); 
  
  sprintf(infofile,"%sppgmsg.txt", ppg.input.cfg_path);
  /* compile file */
  /* sprintf(cmd,"%sppgcompile.sh %s %s &>%s", ppg.input.cfg_path, ppg.input.cfg_path, outfile,infofile);
  printf("cmd = \"%s\"\n",cmd);
     ss_system(cmd);
  */
  //  sprintf(cmd,"%sppgcompile.sh %s %s &>%s", ppg.input.cfg_path, ppg.input.cfg_path, outfile,infofile);
  sprintf(cmd,"/usr/bin/perl %s/comp_int.pl %s%s %s &>%s", comp_path, ppg.input.cfg_path, outfile, foutfile ,infofile);
  printf("cmd=\"%s\"\n",cmd);
  if(strlen(cmd) > sizeof(cmd))
    { /* overwriting cmd caused stat to fail due to overwrite of foutfile */
      cm_msg(MERROR,"prestart","Internal programming error. Length of cmd (%d) is too short for command string(%d)",strlen(cmd) , sizeof(cmd));
      printf("tr_prestart: Error return. See odb messages for details\n");
      return -1;
    }
  status = system(cmd);
  if(debug)
    {
      //printf("status after system command = %d\n",status);
      printf("cmd: %s\n",cmd);
    }
  if(status)
    {
      cm_msg(MERROR,"prestart","Error return from ppg compiler\n");
      errfile = fopen(infofile,"r");
      
      if (errfile != NULL)
	{
	  sprintf(str,"PPG COMPILER ERROR MESSAGES:");
	  cm_msg(MERROR,"prestart","%s",str);
	  while(fgets(str,256,errfile) != NULL)
	      cm_msg(MERROR,"prestart","%s",str);
	}
      
      else
	cm_msg(MERROR,"prestart","Couldn't open ppg compiler error file %s",infofile);
      cm_msg(MERROR,"prestart","File bytecode.dat has NOT been produced by ppg compiler");
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
  else
    cm_msg(MINFO,"prestart","Successfully compiled ppg file & written ",foutfile);
  

  /* wait for compiled file */
  if(debug)printf("waiting for compiled file %sbytecode.dat...\n", ppg.input.cfg_path);
  status = file_wait(ppg.input.cfg_path, "bytecode.dat");
  //if(debug)printf("status after file wait = %d\n",status);
  if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %sbytecode.dat not found - Compile error", ppg.input.cfg_path);
      cm_msg(MINFO,"prestart","Compiler messages are in file %s\n",infofile); 
      printf("Compile command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
  else
    cm_msg(MINFO,"prestart","compiled file %s produced successfully\n",foutfile);

//printf("foutfile=%s\n",foutfile);
  if( stat (foutfile,&stat_buf) ==0 )
  {
    ppg.output.compiled_file_time__binary_ = stat_buf.st_ctime; /* binary time */
    strcpy(timbuf_ascii, (char *) (ctime(&stat_buf.st_ctime)) );  
     if(debug)printf("Last change to file %s:  %s\n",foutfile,timbuf_ascii); 
    {
      int j;
      j=strnlen(timbuf_ascii);
      strncpy(ppg.output.compiled_file_time,timbuf_ascii, j);

      /* An extra carriage return appears unless we do this:  */
      ppg.output.compiled_file_time[j-1] ='\0';
    }    
      
  }
  else
    {
    strcpy (ppg.output.compiled_file_time,"no information available");
    cm_msg(MERROR,"prestart","No information available about compile time of file %s",foutfile);
    cm_msg(MINFO,"prestart","Front-end check on compile time of file will fail");
    }
  
   if(debug)printf("Writing %s to odb ( compiled file time)\n",ppg.output.compiled_file_time ); 

  /* Update output settings in ODB */
  size = sizeof(ppg.output);
  status = db_set_record(hDB, hOut, &ppg.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);
  /* Update Histogram settings in ODB */
  size = sizeof(fmdarc.histograms);
  status = db_set_record(hDB, hHis, &fmdarc.histograms, size, 0);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to save mdarc histograms record (size=%d) (%d)",size,status);  

  if(!single_shot)
    {
      printf("prestart: setting client flag for rf_config true (success)\n"); 
      /* Set the client flag to TRUE (success) */
      flag = TRUE;
      size = sizeof(flag);
      status = db_set_value(hDB, 0, client_str, &flag, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "prestart", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
	  return status;
	}
      flag=0;
      status = db_get_value(hDB, 0, client_str, &flag, &size, TID_BOOL, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "prestart", "cannot read client status flag at path \"%s\" (%d) ",client_str,status);
	  return status;
	}
      else
	{
	  printf("prestart: Read rf_config client status flag at path \"%s\" as (%d) \n",client_str,flag);
	  cm_msg(MINFO, "prestart", "Read rf_config client status flag at path \"%s\" as %d ",client_str,flag);
	} 
   }
  else
      printf("prestart: single loop mode, NOT setting client flag for rf_config true (success)\n"); 


  return SUCCESS;
}


/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT    status, stat1; 
  DWORD  j, i;
  HNDLE  hKey;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   svpath[64], info[128];
  INT    fHandle;
  INT    msg, ch;
  BOOL   daemon;
  FIFO_ACQ_MDARC_STR(fifo_acq_mdarc_str); /* for mdarc (from experim.h) */
  FIFO_ACQ_SIS_MCS_STR(fifo_acq_sis_mcs_str); /* for sis mcs (from experim.h) */
  char str[128];
  INT size,run_state;
  char *s;

  daemon = FALSE;

#ifdef POL
  printf("rf_config: This version is for POL");
#else
  printf("rf_config: Info: this version is for BNMR/BNQR");
#endif

#ifdef CAMP
  printf(" with CAMP access\n");
#else // not CAMP
  printf("\n");
#endif // CAMP

  /* set default */
 cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
   /* initialize global variables */
  sprintf(eqp_name,"FIFO_acq"); 
  single_shot=FALSE;
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {
    if (argv[i][0] == '-' && argv[i][1] == 'd')
      debug = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 's')
	single_shot=TRUE;
    else if (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
	goto usage;
      if (strncmp(argv[i],"-f",2) == 0)
	strcpy(svpath, argv[++i]);
      else if (strncmp(argv[i],"-e",2) == 0)
	strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
	strcpy(host_name, argv[++i]);
    }
    else
    {
   usage:
      printf("usage: rf_config -d (debug) -s (single loop) \n");
      printf("             [-h Hostname] [-e Experiment] [-D] \n\n");
      return 0;
    }
  }
  
  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
    }
  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "rf_config", 0);
  if (status != CM_SUCCESS)
    return 1;
  
#ifdef _DEBUG
  cm_set_watchdog_params(TRUE, 0);
#endif
  
  /* turn off message display, turn on message logging */
  cm_set_msg_print(MT_ALL, 0, NULL);
  
  /* register transition callbacks 
     frontend start is 500,  frontend stop is 500 */


 if(cm_register_transition(TR_START, tr_precheck, 350) != CM_SUCCESS)
    {
      cm_msg(MERROR, "main", "cannot register to transition for tr_precheck");
      goto error;
    }

 if(cm_register_transition(TR_START, tr_prestart,400) != CM_SUCCESS)
    {
      cm_msg(MERROR, "main", "cannot register to transition for prestart");
      goto error;
    }

  /* connect to the database */
  cm_get_experiment_database(&hDB, &hKey);
  
  /* Check if the structure "sis mcs"  exists */
  sprintf(str,"/Equipment/%s/sis mcs",eqp_name); 
  status = db_find_key(hDB,0,"equipment/FIFO_Acq/sis mcs", &hFiS);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("main: can't find key Equipment/FIFO_Acq/sis mcs");
      
      /* So try to create record */
      
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_sis_mcs_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Could not create sis mcs record (%d)\n",status);
	}
      else
	if (debug)printf("Success from create record for %s\n",str);
      
      /* Check again if the structure "sis mcs"  exists  */
      status = db_find_key(hDB,0,"equipment/FIFO_Acq/sis mcs", &hFiS);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Equipment/FIFO_Acq/sis mcs  not present");
	  goto error;
	}
    }
  else
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hFiS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "main", "error during get_record_size (%d)",status);
	  return status;
	}
      printf("Size of sis saved structure: %d, size of sis record: %d\n", sizeof(FIFO_ACQ_SIS_MCS) ,size);
      if (sizeof(FIFO_ACQ_SIS_MCS) != size) {
	/* create record */
	cm_msg(MINFO,"main","creating record (sis mcs); mismatch between size of structure (%d) & record size (%d)", sizeof(FIFO_ACQ_SIS_MCS) ,size); 
	status = db_create_record(hDB, 0, str , strcomb(fifo_acq_sis_mcs_str));
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"main","Could not create sis mcs record (%d)\n",status);
	    return status;
	  }
	else
	  if (debug)printf("Success from create record for %s\n",str);
      }
    }

  /* get the key hMdarc  */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  status = db_find_key(hDB, 0, str, &hMdarc);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("main: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(fifo_acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  if(debug)printf("main: Failure creating mdarc record. Checking if mdarc is running...\n");
	  stat1 = cm_exist("mdarc",FALSE) ;
	  if(stat1 == CM_SUCCESS)	  /* mdarc is already running */
	    {
	      /* how come we can't find the mdarc area if mdarc is running? This shouldn't happen */
	      if(debug)printf("mdarc IS running \n");	
	      cm_msg(MINFO,"main","Data archiver mdarc is running; mdarc record could not be created\n");
	    }
	  else
	    cm_msg(MINFO,"main","Could not create record for %s  (%d); (mdarc not running)", str,status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
      /* try again to get the key hMdarc  */
      status = db_find_key(hDB, 0, str, &hMdarc);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Failed to get the key %s ",str);
	  goto error;
	}
    }    
  else  /* key mdarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMdarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "main", "error during get_record_size (%d) for mdarc",status);
	  return status;
	}
      printf("Size of mdarc saved structure: %d, size of mdarc record: %d\n", sizeof(FIFO_ACQ_MDARC) ,size);
      if (sizeof(FIFO_ACQ_MDARC) != size) {
	cm_msg(MINFO,"main","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)", sizeof(FIFO_ACQ_MDARC) ,size);
	/* create record */
	status = db_create_record(hDB, 0, str , strcomb(fifo_acq_mdarc_str));
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"main","Could not create mdarc record (%d)\n",status);
	    return status;
	  }
	else
	  if (debug)printf("Success from create record for %s\n",str);
      }
    }

  sprintf(comp_path,"/home/%s/online/rf_config",expt_name);
  printf("comp path = %s\n",comp_path);

#ifdef CAMP
  /* Create or Get record for the camp tree for BNMR & BNQR  */
  hCamp=0;
  /* now add pol so it can use camp as well */
  if (strcmp(expt_name,"bnmr")==0 || strcmp(expt_name,"bnqr")==0  || strcmp(expt_name,"pol")==0   )
    {
      if(debug)printf("Experiment %s detected; accessing camp record\n",expt_name);
      status = camp_create_rec();
      if(status != SUCCESS) goto error;
    }
  else
    {
      cm_msg(MINFO,"main","experiment \"%s\" does not support CAMP. Camp record not created",expt_name);
    printf("main: *** CAMP is defined but unexpected experiment name found (%s)\n",expt_name);
    printf("          Camp record not created\n");
    }
#endif    
  
  /* check if Input tree is available */
  status = db_find_key(hDB, hFiS, "Input", &hIn);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Input record");
      goto error;
    }
 
 /* check if output tree is available */
  status = db_find_key(hDB, hFiS, "Output", &hOut);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Output record");
      goto error;
    }
 /* check if sis test mode  tree is available */
  status = db_find_key(hDB, hFiS, "sis test mode", &hTest);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find sis test mode record");
      goto error;
    }
 /* check if hardware  tree is available */
  status = db_find_key(hDB, hFiS, "hardware", &hH);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find hardware record");
      goto error;
    }

#ifdef PSM
 /* check if hardware/psm  tree is available */
  status = db_find_key(hDB, hFiS, "hardware/psm", &hPSM);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find hardware/psm record");
      goto error;
    }
#endif

  /* check if mdarc/histograms tree is available */
  status = db_find_key(hDB, hMdarc, "Histograms", &hHis);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Histograms record");
      goto error;
    }

   if (single_shot)    
     {         /* single sweep */

       status = tr_precheck(10, info); 
       if (status != SUCCESS)
	 goto error;

      status = tr_prestart(10, info); 
      if (status) 
	{
	  printf("Thank you\n");
	  goto exit; /* force exit */
	}
      else
	goto error; 
     }

  /* initialize ss_getchar() */
  ss_getchar(0);
  do
    {
      while (ss_kbhit())
	{
	  ch = ss_getchar(0);
      if (ch == -1)
	ch = getchar();
      if (ch == 'R')
	ss_clear_screen();
      if ((char) ch == '!')
	break;
    }
    msg = cm_yield(1000);
  } while (msg != RPC_SHUTDOWN && msg != SS_ABORT && ch != '!');
  

  printf("Thank you\n");
  goto exit;
 
 error:
  printf("\n Error detected. Check odb messages for details\n");
  
 exit:


  /* reset terminal */
  ss_getchar(TRUE);
  
  cm_disconnect_experiment();
  return 1;
}

