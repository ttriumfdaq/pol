/********************************************************************\

 *  Name:         analyzer.h
 * Created by:   Stefan Ritt

 *  Contents:     Analyzer global include file

 * $Log: analyzer.h,v $
 * Revision 1.2  1998/10/12 12:18:58  midas
 * Added Log tag in header


\********************************************************************/
                                                        
/*-- Parameters ----------------------------------------------------*/

/*-- Number of channels --------------------------------------------*/
#define N_CYCSCAL      9 /* Bank CYCI */
#define N_SCLR         6 /* Bank HSCL */
#define N_STEP      0417 /* SUM_array size     */

/*-- Shared functions --------------------------------------------- */

INT cyc_hist_beginrun(void);
INT cyc_hist_endrun(void);
INT cyc_hist_exit(void);


