/********************************************************************\

  Name:         cycscal_hist.c -> adchist.c
  Created by:   Stefan Ritt
  Modified by:  R. Poutissou and Thomas Cocolios

  Contents:     Simple histogramming of SCAL's

  Modification  2005/08/10
  ROOT histograms proper saving
  No more sudden crash

  Modification  2004/10/12
  Delayed summation
  Bad Scan online removal
  DOES NOT WORK YET !!!

  Modification  2004/09/03
  ASCII file for all modes
  Extra channel from DVM

  Modification  2004/07/12
  DAC module implementation

  Modification  2004/06/29
  Root histogramming
  Array of histograms
  Specific to Pol Experiment


\********************************************************************/
                                                        
/*-- Include files -------------------------------------------------*/

/* standard includes */
#include <stdio.h>
#include <math.h>
#include <assert.h>

/*C++ std includes */
#include <string>
#include <sstream>
#include <vector>

/* midas includes */
#include "midas.h"
#include "experim.h"
#include "analyzer.h"

/* cernlib includes */
#ifdef OS_WINNT
#define VISUAL_CPLUSPLUS
#endif

#ifdef __linux__
#define f2cFortran
#endif

#include <cfortran.h>

/* ROOT includes */
#include <TH1.h>
#include <TTree.h>
#include <TDirectory.h>

#ifndef PI
#define PI 3.14159265359
#endif

/*-- Module declaration --------------------------------------------*/

INT cyc_hist(EVENT_HEADER*,void*);
INT cyc_hist_init(void);
INT cyc_hist_exit(void);
INT cyc_hist_beginrun(void);
INT cyc_hist_endrun(void);

ANA_MODULE cyc_hist_module = {
  "Scaler hist",                      /* module name           */  
  "Renee",                            /* author                */
  cyc_hist,                           /* event routine         */
  NULL,                               /* BOR routine           */
  NULL,                               /* EOR routine           */
  cyc_hist_init,                      /* init routine          */
  NULL,                               /* exit routine          */
  NULL, // &scal_hist_param,          /* parameter structure   */
  0, // sizeof(scal_hist_param),      /* structure size        */
  NULL, //scal_hist_param_str,        /* initial parameters    */
};

/*-- Globals -------------------------------------------------------*/
INT event_num=0;
INT dev_ninc;
float dev_start,dev_stop,dev_zero,camp_start, camp_stop, camp_inc;
float factor_readback, camp_readback;
INT   camp_offset;
float *pvhist;
//INT prev_scan;
static INT  factor;
char camp_dev[10];
FILE *fp;
extern HNDLE   hDB;
INT gbl_run_number;
INT n_scan[N_SCLR];
INT dac_flag = 0;
BOOL bad_scan_flag;
INT bad_scan_status = 0;

/*-- Array of histograms -------------------------------------------*/
static TH1D *h_SCAN[N_SCLR];
static TH1D *h_SUM[N_SCLR];
/*float SUM_array[N_SCLR][N_STEP];
double voltage_array[N_STEP];
INT j_max;*/

/* DAC specific histograms          */
static TH1D *h_LINEAR;
/*TH1D *h_REAL;
  TH1D *h_REAL_SUM;*/

/* DAC & NaCell specific histograms */
//static TH1D *h_FC2;
//double FC2_array[N_STEP];

/*-- init routine --------------------------------------------------*/

INT cyc_hist_init(void)
{
  INT    /*i,j,*/ status,size;
 char   device[32];
 char   mode[32];

 // char camp_units[10];
 // char camp_str[128];

 cm_get_experiment_database(&hDB, NULL);
 printf("Entering cyc_hist_init\n");

 /* make sure SUM_array is empty at start up */
 /* for (i=0;i<N_STEP;i++) {
   for (j=0;j<N_SCLR;j++) {
     SUM_array[j][i] = 0;
   }
   voltage_array[i] = 0;
 }
 j_max = N_SCLR;*/

 /* find out the mode */
 size = sizeof(mode);
 status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/Experiment name",
		       &mode, &size, TID_STRING, FALSE);
 printf(" Mode is %s\n",mode);
 /* find out what flag is on */
 size = sizeof(device);
 if (strcmp (mode,"1n")==0){
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Output/e1n Epics device", 
		       &device, &size, TID_STRING, FALSE);
     printf(" Scanning device %s\n",device);
 } else if (strcmp (mode,"1c")==0){
     printf("Must be a camp device\n");
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1c Camp device", 
			   &device, &size, TID_STRING, FALSE);
     printf(" CAMP Scanning device %s\n",device);
     if (strncmp (device,"FG",2)==0)
	 sprintf(camp_dev,"frequency generator");
     else if  (strncmp (device,"DAC",3)==0)
	 sprintf(camp_dev,"dac");
     else if (strncmp (device,"MG",2)==0)
       sprintf(camp_dev,"magnet");
     else
       {
	 sprintf(camp_dev,"unknown");
	 printf("Error: unknown camp device %s, not setting up histograms",device);
       }

   return SUCCESS;
 } else if (strcmp (mode,"1h")==0){
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Output/e1h scan device", 
		       &device, &size, TID_STRING, FALSE);
     printf(" Scanning device %s\n",device);
 }
 return SUCCESS;
}

/*-- begin run routine --------------------------------------------------*/

INT cyc_hist_beginrun(void)
{
  INT    i,/*j,*/ status,size;
 float  laser_start,laser_stop,laser_inc;
 float  nacell_start,nacell_stop,nacell_inc;
 float  dac_start,dac_stop,dac_inc;
 INT    freq_start,freq_stop,freq_inc;
 char   device[32];
 char   mode[32];
 char   title[60];
 char   h_name[32];
 char   label[N_SCLR][32]={" SIS Ref pulse"," Fluo. mon 1"
			    ," proton beam"," FC2 from lock-in"," FC15 current"," Iodine lock"};

 char camp_units[10];
 char camp_str[128];
 char str[32];

 /* for (j=0; j<N_STEP; j++) {
   voltage_array[j] = 0;
 }
 //j_max = 0;*/

 bad_scan_flag = FALSE;

 /* find out the run number */
 size = sizeof(gbl_run_number);
 status = db_get_value(hDB, 0, "/Runinfo/Run number",
		       &gbl_run_number, &size, TID_INT, FALSE);
 printf("Starting run %d\n",gbl_run_number);

 /* find out the mode */
 size = sizeof(mode);
 status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/Experiment name",
		       &mode, &size, TID_STRING, FALSE);
 printf(" Mode is %s\n",mode);
 /* find out what flag is on */
 size = sizeof(device);
 if (strcmp(mode,"1n")==0){
    status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Output/e1n Epics device", 
      &device, &size, TID_STRING, FALSE);
    printf(" Scanning devic %s\n",device);
 } else if (strcmp(mode,"1h")==0){
    status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Output/e1h scan device", 
		       &device, &size, TID_STRING, FALSE);
    printf(" Scanning device %s\n",device);
 } else if (strcmp (mode,"1c")==0){
     printf("Must be a camp device\n");
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1c Camp device", 
			   &device, &size, TID_STRING, FALSE);
     printf(" CAMP Scanning device %s\n",device);
     if (strncmp (device,"FG",2)==0)
	 sprintf(camp_dev,"frequency generator");
     else if  (strncmp (device,"DAC",3)==0)
	 sprintf(camp_dev,"dac");
     else if (strncmp (device,"MG",2)==0)
       sprintf(camp_dev,"magnet");
     else
       {
	 sprintf(camp_dev,"unknown");
	 printf("Error: unknown camp device %s, not setting up histograms",device);
       }
     if(strcmp (camp_dev,"unknown") != 0)
       {
	 sprintf(camp_str, "/Equipment/FIFO_acq/camp sweep devices/%s/scan units",camp_dev);
	 printf("camp string:%s\n",camp_str);
	 size=sizeof(camp_units);
	 status = db_get_value(hDB, 0,camp_str,
			       &camp_units, &size, TID_STRING, FALSE);
	 if(status != DB_SUCCESS)
	   printf("Error getting value from %s (%d)\n",camp_str,status);

	 sprintf(camp_str, "/Equipment/FIFO_acq/camp sweep devices/%s/integer conversion factor",camp_dev);
	 printf("camp string:%s\n",camp_str);
	 size=sizeof(factor);
	 status = db_get_value(hDB, 0,camp_str,
			       &factor, &size, TID_INT, FALSE);
	 if(status == DB_SUCCESS)
	   printf(" Camp %s is being scanned; Units are %s, conversion factor=%d\n",
		  camp_dev,camp_units,factor);
	 else
	   printf("Error getting value from %s (%d)\n",camp_str,status);

	 size = sizeof(camp_start);
	 status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1c Camp Start", 
			       &camp_start, &size, TID_FLOAT, FALSE);
	 size = sizeof(camp_stop);   
	 status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1c Camp Stop", 
			       &camp_stop, &size, TID_FLOAT, FALSE);
	 size = sizeof(camp_inc);
	 status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1c Camp Inc", 
			       &camp_inc, &size, TID_FLOAT, FALSE);
	 if(camp_inc < 0) camp_inc = -1* camp_inc;
	 dev_ninc= (INT) (fabs((camp_stop - camp_start)/camp_inc) + 1.);
	 dev_zero = camp_start;
	 if(camp_start < camp_stop){
	   dev_start = camp_start - 0.5*camp_inc;
	   printf("dev_start %f\n",dev_start);
	 } else {
	   dev_start = camp_start - (camp_inc*dev_ninc) + (0.5*camp_inc);
	 }
	 dev_stop = dev_start + (camp_inc*dev_ninc);
	 printf("Camp scan limits: %f, %f, %f, %d, %f, %f\n", 
		camp_start, camp_stop,camp_inc,dev_ninc,dev_start,dev_stop);
	 if(strcmp (camp_dev,"dac") == 0) {
	   factor_readback = 100.;
	   size=sizeof(camp_offset);
	   status = db_get_value(hDB, 0, "/Experiment/Edit on start/DC offset(V)", 
			       &camp_offset, &size, TID_INT, FALSE);
	   printf("This is a DAC scan, there is an offset added: %d V\n",camp_offset);
           dev_start = (dev_start*factor_readback) + camp_offset;
           dev_stop = (dev_stop*factor_readback) + camp_offset;
	   //dev_ninc = 10000; /* number of bins in histo */
	   printf("The histogram limits are modified:  %d, %f, %f\n",
		 dev_ninc,dev_start,dev_stop); 
	 }

	 for (i=0 ; i<N_SCLR ; i++) {

           n_scan[i] = 0;   // set the scan number at 0 for further scan vs sum

	   strcpy(title,device);
	   sprintf(str,"%s, last scan only",camp_units);
	   strcat(title,str);
	   strcat(title,label[i]);

	   sprintf(h_name,"CAMP_SCAN_%i",i);

	   printf("ROOT Booking %s: %s\n",h_name,title);
	   //h_SCAN[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
	   if (h_SCAN[i])
	     {
	       gManaHistosFolder->Remove(h_SCAN[i]);
	       delete h_SCAN[i];
	     }
	   h_SCAN[i] = new TH1D (h_name,title,102,0,2000);
	   gManaHistosFolder->Add(h_SCAN[i]);

	   strcpy(title,device);
	   sprintf(str,"%s, summing all channels",camp_units);
	   strcat(title,str);
	   strcat(title,label[i]);

	   sprintf(h_name,"CAMP_SUM_%i",i);

	   printf("ROOT Booking %s: %s\n",h_name,title);
	   if (h_SUM[i])
	     {
	       gManaHistosFolder->Remove(h_SUM[i]);
	       delete h_SUM[i];
	     }
	   //h_SUM[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
	   h_SUM[i] = new TH1D (h_name,title,102,0,2000);
	   gManaHistosFolder->Add(h_SUM[i]);
	   /*for (j=0; j<N_STEP; j++) {
	     SUM_array[i][j] = 0;      // set the SUM_array to 0
	     }*/
	 }
       } // end of CAMP known device
     else
       printf("Cannot histogram unknown CAMP device\n");
 }
 if (strcmp (device,"Laser")==0){
     printf(" Laser is being scanned\n");
     size = sizeof(laser_start);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/Laser Start", 
			   &laser_start, &size, TID_FLOAT, FALSE);
     size = sizeof(laser_stop);   
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/Laser Stop", 
			   &laser_stop, &size, TID_FLOAT, FALSE);
     size = sizeof(laser_inc);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/Laser inc", 
			   &laser_inc, &size, TID_FLOAT, FALSE);
     if(laser_inc < 0) laser_inc = -1* laser_inc;
     dev_ninc= (INT) (fabs((laser_stop - laser_start)/laser_inc) + 1.);
     dev_zero = laser_start;
     if(laser_start < laser_stop){
       dev_start = laser_start - 0.5*laser_inc;
       printf("dev_start %f\n",dev_start);
     } else {
       dev_start = laser_start - (laser_inc*dev_ninc) + (0.5*laser_inc);
     }
     dev_stop = dev_start + (laser_inc*dev_ninc);
     printf("Laser scan limits: %f, %f, %f, %d, %f, %f\n", 
	    laser_start, laser_stop,laser_inc,dev_ninc,dev_start,dev_stop);
     for (i=0 ; i<N_SCLR ; i++) {
       
       n_scan[i] = 0;   // set the scan number at 0 for further scan vs sum

       strcpy(title,device);
       strcat(title," volt, last scan only");
       strcat(title,label[i]);

       sprintf(h_name,"LASER_SCAN_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SCAN[i])
	 {
	   gManaHistosFolder->Remove(h_SCAN[i]);
	   delete h_SCAN[i];
	 }
       h_SCAN[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_SCAN[i]);

       strcpy(title,device);
       strcat(title," volt, summing all channels");
       strcat(title,label[i]);

       sprintf(h_name,"LASER_SUM_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SUM[i])
	 {
	   gManaHistosFolder->Remove(h_SUM[i]);
	   delete h_SUM[i];
	 }
       h_SUM[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_SUM[i]);
       /*for (j=0; j<N_STEP; j++) {
	 SUM_array[i][j] = 0;
	 }*/
     }
   } 
 else if (strcmp (device,"NaCell")==0){
     printf(" NaCell is being scanned\n");
     size = sizeof(nacell_start);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/NaVolt start", 
			   &nacell_start, &size, TID_FLOAT, FALSE);
     if (status != DB_SUCCESS)
       printf("Can't get NaCell start value from ODB\n");
     size = sizeof(nacell_stop);   
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/NaVolt stop", 
			   &nacell_stop, &size, TID_FLOAT, FALSE);
     if (status != DB_SUCCESS)
       printf("Can't get NaCell stop value from ODB\n");
     size = sizeof(nacell_inc);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/NaVolt inc", 
			   &nacell_inc, &size, TID_FLOAT, FALSE);
     if (status != DB_SUCCESS)
       printf("Can't get NaCell inc value from ODB\n");
     dev_ninc= (INT) (fabs((nacell_stop - nacell_start)/nacell_inc) + 1.);
     dev_zero = nacell_start;
     if(nacell_start < nacell_stop){
       dev_start = nacell_start;
       dev_stop = nacell_start + fabs((nacell_inc)*dev_ninc);
     } else {
       dev_start = nacell_start - fabs((nacell_inc)*dev_ninc);
       dev_stop = nacell_start;
     }
     printf("NaCell scan limits: %f, %f, %f, %d, %f, %f\n", 
	    nacell_start, nacell_stop,nacell_inc,dev_ninc,dev_start,dev_stop);
     /* book histos */ 
     for (i=0 ; i<N_SCLR ; i++) {

       n_scan[i] = 0;   // set the scan number at 0 for further scan vs sum

       strcpy(title,device);
       strcat(title," volt, last scan only");
       strcat(title,label[i]);

       sprintf(h_name,"NaCELL_SCAN_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SCAN[i])
	 {
	   gManaHistosFolder->Remove(h_SCAN[i]);
	   delete h_SCAN[i];
	 }
       h_SCAN[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_SCAN[i]);

       strcpy(title,device);
       strcat(title," volt,  summing all cycles");
       strcat(title,label[i]);

       sprintf(h_name,"NaCELL_SUM_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SUM[i])
	 {
	   gManaHistosFolder->Remove(h_SUM[i]);
	   delete h_SUM[i];
	 }
       h_SUM[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_SUM[i]);
       /*for (j=0; j<N_STEP; j++) {
	 SUM_array[i][j] = 0;
	 }*/
     }
     /*h_FC2 = new TH1D ("FC2","Search for hfs",dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_FC2);*/
     /*     for (j=0; j<N_STEP; j++) {
       FC2_array[j] = 0;
       }*/
   }
 else if (strcmp (device,"DAC")==0){
     printf(" DAC is being scanned\n");
     size = sizeof(dac_start);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1h DAC Start", 
			   &dac_start, &size, TID_FLOAT, FALSE);
     size = sizeof(dac_stop);   
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1h DAC Stop", 
			   &dac_stop, &size, TID_FLOAT, FALSE);
     size = sizeof(dac_inc);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/e1h DAC Inc", 
			   &dac_inc, &size, TID_FLOAT, FALSE);
     dev_ninc= (INT) (fabs((dac_stop - dac_start)/dac_inc) + 1.);
     dev_zero = dac_start;
     if(dac_start < dac_stop){
       dev_start = dac_start;
       dev_stop = dac_start + fabs((dac_inc)*dev_ninc);
     } else {
       dev_start = dac_start - fabs((dac_inc)*dev_ninc);
       dev_stop = dac_start;
     }
     printf("DAC scan limits: %f, %f, %f, %d, %f, %f\n", 
	    dac_start,dac_stop,dac_inc,dev_ninc,dev_start,dev_stop);

       dev_start = 100 * dev_start;
       dev_stop  = 100 * dev_stop;
       //dev_start = 0;
       //dev_stop = 1001;
       //dev_start = -100000    ;
       //dev_stop =  -45;
       
     /* book histos */ 
     for (i=0 ; i<N_SCLR ; i++) {

       n_scan[i] = 0;   // set the scan number at 0 for further scan vs sum

       strcpy(title,device);
       strcat(title," scan, last scan only");
       strcat(title,label[i]);

       sprintf(h_name,"DAC_SCAN_%i",i);
 
       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SCAN[i])
	 {
	   gManaHistosFolder->Remove(h_SCAN[i]);
	   delete h_SCAN[i];
	 }
       h_SCAN[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_SCAN[i]);

       strcpy(title,device);
       strcat(title," scan, summing all cycles");
       strcat(title,label[i]);

       sprintf(h_name,"DAC_SUM_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SUM[i])
	 {
	   gManaHistosFolder->Remove(h_SUM[i]);
	   delete h_SUM[i];
	 }
       h_SUM[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       gManaHistosFolder->Add(h_SUM[i]);
       /*for (j=0; j<N_STEP; j++) {
	 SUM_array[i][j] = 0;
	 }*/
     }
     if (h_LINEAR)
       {
	 gManaHistosFolder->Remove(h_LINEAR);
	 delete h_LINEAR;
       }
     h_LINEAR = new TH1D ("Linearity","Voltage Ramp",dev_ninc,dev_start,dev_stop);
     gManaHistosFolder->Add(h_LINEAR);
#if 0
     h_REAL = new TH1D ("Real","Fluo. mon. 1 vs Readback, last scan only",(INT)(dev_ninc*40000/(dev_stop-dev_start)),0,4000);
     h_REAL_SUM = new TH1D ("Real_Sum","Fluo. mon. 1 vs Readback, summing all scans",(INT)(dev_ninc*40000/(dev_stop-dev_start)),0,4000);
#endif
     //h_FC2 = new TH1D("FC2","Search for hfs",dev_ninc,dev_start,dev_stop);
     //gManaHistosFolder->Add(h_FC2);
     /*     for (j=0; j<N_STEP; j++) {
       FC2_array[j] = 0;
       }*/
   }
 else if (strcmp (device,"none")==0){
     printf(" Dummy device is being scanned\n");
     size = sizeof(freq_start);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/frequency start (Hz)", 
			   &freq_start, &size, TID_DWORD, FALSE);
     size = sizeof(freq_stop);   
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/frequency stop (Hz)", 
			   &freq_stop, &size, TID_DWORD, FALSE);
     size = sizeof(freq_inc);
     status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/sis mcs/Input/frequency increment (Hz)", 
			   &freq_inc, &size, TID_DWORD, FALSE);
     dev_ninc= (INT) (abs((freq_stop - freq_start)/freq_inc) + 1.);
     dev_zero = (float)freq_start;
     if(freq_start < freq_stop){
       dev_start = (float)freq_start;
       dev_stop = (float)(freq_start + abs((freq_inc)*dev_ninc));
     } else {
       dev_start = (float)(freq_start - abs((freq_inc)*(dev_ninc-1)));
       dev_stop = (float)(freq_start + dev_ninc); 
     }
     printf("Dummy scan limits: %d, %d, %d, %d, %f, %f\n", 
	    freq_start, freq_stop,freq_inc,dev_ninc,dev_start,dev_stop);

     /* book histos */ 
   for (i=0 ; i<N_SCLR ; i++) {

       n_scan[i] = 0;   // set the scan number at 0 for further scan vs sum

       strcpy(title,"Dummy value, last scan only");
       strcat(title,label[i]);

       sprintf(h_name,"DUMMY_SCAN_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SCAN[i])
	 {
	   gManaHistosFolder->Remove(h_SCAN[i]);
	   delete h_SCAN[i];
	 }
       //h_SCAN[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);
       h_SCAN[i] = new TH1D (h_name,title,5001,0,5000);
       gManaHistosFolder->Add(h_SCAN[i]);

       strcpy(title,"Dummy value, summing all cycles");
       strcat(title,label[i]);
       sprintf(h_name,"DUMMY_SUM_%i",i);

       printf("ROOT Booking %s: %s\n",h_name,title);
       if (h_SUM[i])
	 {
	   gManaHistosFolder->Remove(h_SUM[i]);
	   delete h_SUM[i];
	 }
       //h_SUM[i] = new TH1D (h_name,title,dev_ninc,dev_start,dev_stop);  //for dummy variable run
       h_SUM[i] = new TH1D (h_name,title,5001,0,5000);     //for Scalar Run
       gManaHistosFolder->Add(h_SUM[i]);
       /*for (j=0; j<N_STEP; j++) {
	 SUM_array[i][j] = 0;
	 }*/
   }

 }
 /*-- Open an ASCII file for any mode --*/
 printf("Open an ASCII data file for DAC scan\n");
 char buff [256];
 char fname [256];
 size = 256;
 status = db_get_value (hDB, 0, "/Logger/Data dir", buff, &size, TID_STRING,FALSE);
 printf(" Dir is %s\n",buff);
 if (status != SUCCESS) {
   printf("Can't get value for /Logger/Data dir - no ASCII file created\n");
   fp = NULL;
 } else {
   size = strlen (buff);
   if (buff [size - 1] != '/') {
     strcat (buff, "/");
   }
   strcat (buff, "run%06d.txt");
   sprintf(fname,buff,gbl_run_number);
   /*if (fp != NULL)
     fclose(fp);*/
   fp = fopen (fname, "a");
   assert(fp != NULL);
   printf("Opened ASCII data file %s\n",fname);
   //   fprintf(fp,"#Run number %06d\n",gbl_run_number);
   fflush(fp);
 }
 if (strcmp (device,"NaCell")==0){
   //   fprintf(fp,"#NaCell starting voltage = %4f\nNaCell stoping voltage = %4f\n",dev_start,dev_stop);
   //   fprintf(fp,"\n#Event\tScan\tEPICS value\tCounts\tFC2\n");
 } else if (strcmp (device,"DAC")==0){
   dev_start = 100 * dev_start;
   dev_stop  = 100 * dev_stop;
   fprintf(fp,"#DAC starting voltage = %4f\n#DAC stoping  voltage = %4f\n",dev_start,dev_stop);
   fprintf(fp,"\n#Event\tScan\tDAC Chanel\tFM1\tReadback\n");
   fflush(fp);
 }
 return SUCCESS;
}

/*-- event routine -------------------------------------------------*/

INT cyc_hist(EVENT_HEADER *pheader, void *pevent)
{
  /* from bank CYCL */
  INT gbl_CYCLE_N;   /* cycle #,                            */
  INT gbl_SCYCLE_N;  /* Super cycle - same as above for now */
  INT gbl_SCAN_N;    /* Scan number                         */
  INT gbl_POL;       /* Polarization 0 or 1                 */
  INT gbl_scan_flag; /* 1 = NaCell, 2 = Laser, 3 = Other, 8 = DAC, >255 = CAMP */
  INT iepics_read=0,iepics_val=0;
  double epics_read,epics_val;
  INT freq_val=0;
  INT ireadback=0;
  INT dac_val=0;
  INT dvm_readback_0=0, dvm_readback_1=0, /*wavemeter=0,*/ dummy_parameter=0;
  double camp_val=0,camp_readback=0;
  double dac_val_print=0, dvm_readback_print_0=0, dvm_readback_print_1=0;
  INT useable_cycle_N;

  /* internal variables */
  INT   i, /*j, k,*/ n_cyci, n_scal/*, status, size*/;
  DWORD *pdata;
  float cscal;
  //  float dummy=0;
  BOOL fzero;
  //  char  btitle[2]={" "};
  
  event_num++;
  fzero = FALSE;

  /*printf("Entering routine cyc_hist at event %d\n",event_num);*/
  
  /* look for CYCI bank, return if not present */
  n_cyci = bk_locate((DWORD *)pevent, "CYCI", &pdata);
  //printf("Found bank CYCI with %d words\n",n_cyci); 
  if (n_cyci == 0){
    printf("Cannot find bank CYCI at event %d\n",event_num);
    return 1;
  }
  else if(n_cyci != N_CYCSCAL){
    printf("Bank CYCI length is %d, but expected %d\n",
	   n_cyci,N_CYCSCAL);
    return 1;
  }
  gbl_CYCLE_N = *pdata++;
  gbl_SCYCLE_N= *pdata++;
  gbl_SCAN_N= *pdata++;
  gbl_POL= *pdata++;
  gbl_scan_flag= *pdata++;
  if (gbl_scan_flag == 8){
    dummy_parameter = *pdata++;   /* not in use */
    dvm_readback_1  = *pdata++;   /* FC2 current - not in use */
    //printf("%i ", dvm_readback_1);
    dac_val         = *pdata++;   /* DAC set point */
    //printf("%i ", dac_val);
    dvm_readback_0  = *pdata++;   /* DVM Readback of the voltage */
    //printf("%i\n", dvm_readback_0);
    dvm_readback_print_1 = ((double)dvm_readback_1)/100000;
    dac_val_print = ((double)dac_val) / 10;
    dvm_readback_print_0 = ((double)dvm_readback_0)/100;
    //printf("DAC %lf, DVM %lf\n",dac_val_print, dvm_readback_print);
    //printf("Getting dac_flag\t");
    dac_flag = gbl_scan_flag;
    //printf("dac_flag=%i\n",dac_flag);
    useable_cycle_N = (INT)gbl_CYCLE_N;
  } else {
    iepics_read= *pdata++;
    iepics_val = *pdata++;
    freq_val   = *pdata++;  /* dummy value just to skip */
    if (gbl_scan_flag == 1){    
      dvm_readback_1 = *pdata++;  /* FC2 current */
      dvm_readback_print_1 = ((double)dvm_readback_1)/100000;
    } else {
      ireadback = *pdata++;
    }
  }
  //  printf("Pointless computation on epics useless variables\n");
  epics_read= ((double)iepics_read)/1000;
  //  printf("Blah!\n");
  epics_val= ((double)iepics_val)/1000;
  //  printf("Pouf\n");
  
  /* printf("CYCI bank: CYCLE_N=%d,SCYCLE=%d,SCAN=%d,POL=%d,flag=%d\n",
     gbl_CYCLE_N,gbl_SCYCLE_N,gbl_SCAN_N,gbl_POL,gbl_scan_flag);*/
  if(gbl_scan_flag >255){
    camp_val = (double)freq_val/(float)factor;
    camp_readback = (double)ireadback/100.;
    printf("Camp value = %lf ; Camp readback = %lf \n", camp_val,camp_readback);
    if(dev_zero == camp_val) {
      printf("Restarting scan ; zero first set of histo's\n");
      fzero = TRUE;
    }
  }
  else if (gbl_scan_flag == 8) {
    if(dev_zero == dac_val){   // does not work if going both ways. However this does not do much either...
      printf("Restarting scan ; zero first set of histo's\n");
      fzero = TRUE;
    }
  }
  else if (gbl_scan_flag >0 & gbl_scan_flag != 8){
    printf("Epics voltage Read = %.4f(%d), Set = %.4f(%d) \n", 
	   epics_read,iepics_read, epics_val,iepics_val);
    if(dev_zero == epics_val){
      printf("Restarting scan ; zero first set of histo's\n");
      fzero = TRUE;
    }
  }
  else {
    printf("Frequency value = %d\n",freq_val);
    if(dev_zero == (float)freq_val){
      printf("Restarting scan ; zero first set of histo's\n");
      fzero = TRUE;
    }
  }

//printf("Computing j\t");
  /* j = gbl_CYCLE_N - (j_max * (n_scan[1] - 1));*/
//printf("j=%i\n",j);
  
  /* look for HSCL bank, return if not present */
  n_scal = bk_locate((DWORD *)pevent, "HSCL", &pdata);
  if (n_scal == 0){
    printf("Cannot find bank HSCL at event %d\n",event_num);
    return 1;
  }
//printf("Found bank HSCL with %d words\n",n_scal);

  /* if(gbl_scan_flag > 255)*/
    {
      /* camp scan - we are only histogramming the first N_SCLR words */

      /*This has just been changed to always be so as otherwise the arrays are not big enough */
      if(n_scal > N_SCLR) n_scal=N_SCLR;
    }
 

  cscal = 0.f;
  for (i=0 ; i<n_scal  ; i++) {
    if(gbl_scan_flag == 1){
      /* NaCell was varied */
      cscal = (float)*((double *)pdata); 
      if(cscal == -66666.)
	cscal = 0.0 ;
      /*
      printf("Word %d = %g (%f)\n",i,
	     *((double *)pdata),cscal);
      */
      /*printf("Word %d = %g (%f) at cycle %i with NaCELL\n",i,
       *((double *)pdata),cscal,gbl_CYCLE_N);*/
      (*((double *)pdata))++;

      //      if(fzero)
      if(n_scan[i] != gbl_SCAN_N){
	h_SCAN[i]->Reset();
	/*for (k=0; k<j_max; k++) {
	  h_SUM[i] ->Fill(voltage_array[k],SUM_array[i][k]);
	}
	if (n_scan[i] != 0)
	  j_max = gbl_CYCLE_N / n_scan[i]; // set j_max
	  else j_max = N_SCLR;*/
	n_scan[i]++;// = n_scan[i]++;
	if (i == 1) {
	  if(fp != NULL)
	    //	    fprintf(fp,"bad_scan_flag = %i\n",bad_scan_status);
	  /*if(bad_scan_flag == 0)
	    printf("Good scan, adding histograms to the sums\n");
	  else if(bad_scan_flag == 1)
	    printf("Bad scan, discarding last set of histograms from sums\n");
	    else printf("Strange scan flag, discarding last set of histograms from sums\n");*/
	  printf("New scan... scan number %i\n",n_scan[i]);
	}
      }
      h_SCAN[i]->Fill(epics_read,cscal);
      h_SUM[i] ->Fill(epics_read,cscal);
      /*voltage_array[j] = epics_read;
	SUM_array[i][j] = cscal;*/
      if( (fp != NULL) && (i == 1) ){
	//dvm_readback_print = ((double)dvm_readback_0)/1000000;
	/*	fprintf(fp, " %4d\t %2d\t %10lf\t %4.0f\t %10lf\n",
		gbl_CYCLE_N, gbl_SCAN_N, epics_read, cscal, dvm_readback_print_1);*/
      }
    }
    else if(gbl_scan_flag == 2){
      /* Laser was varied */
      cscal = (float)*((double *)pdata); 
      /*
      printf("Word %d = %g (%f)\n",i,
	     *((double *)pdata),cscal);
      */
      printf("Word %d = %g (%f) at cycle %i with LASER\n",i,
	     *((double *)pdata),cscal,gbl_CYCLE_N);
      (*((double *)pdata))++;
      /* fill histo */
      //     if(fzero)
      if(n_scan[i] != gbl_SCAN_N){
	h_SCAN[i]->Reset();
        /*for (k=0; k<j_max; k++) { 
          h_SUM[i] ->Fill(voltage_array[k],SUM_array[i][k]); 
        } 
	if (n_scan[i] != 0)
	  j_max = gbl_CYCLE_N / n_scan[i]; // set j_max 
	  else j_max = N_SCLR;*/
	n_scan[i]++;// = n_scan[i]++;
        if (i == 1) {
          if(fp != NULL) 
            fprintf(fp,"#bad_scan_flag = %i\n",bad_scan_status);
	  /* if(bad_scan_flag == 0) 
            printf("Good scan, adding histograms to the sums\n"); 
          else if(bad_scan_flag == 1) 
            printf("Bad scan, discarding last set of histograms from sums\n"); 
	    else printf("Strange scan flag, discarding last set of histograms from sums\n");*/
          printf("New scan... scan number %i\n",n_scan[i]); 
	} 
      }
      h_SCAN[i]->Fill(epics_read,cscal); 
      h_SUM[i] ->Fill(epics_read,cscal); 
      /*voltage_array[j] = epics_read; 
	SUM_array[i][j] = cscal;*/
    }
    else if(gbl_scan_flag == 8){
      /* DAC was varied */
      cscal = (float)*((double *)pdata); 
      /*
      printf("Word %d = %g (%f)\n",i,
	     *((double *)pdata),cscal);
	     */
      /*      printf("Word %d = %g (%f) at cycle %i with DAC\n",i,
       *((double *)pdata),cscal,gbl_CYCLE_N);*/
      //(*((double *)pdata))++;
      ((double*&)pdata)++;
      //printf("Just incremented pdata\n");
      /* fill histo */
      //     if(fzero)
      if(n_scan[i] != gbl_SCAN_N){
	h_SCAN[i] -> Reset();
	h_LINEAR  -> Reset();
	/*h_REAL    -> Reset();*/
	/*if (bad_scan_flag == 0) {
	  for (k=0; k<j_max; k++) { 
	  h_SUM[i] ->Fill(voltage_array[k],SUM_array[i][k]);
	  }
	  }
	  if (n_scan[i] != 0)
	  j_max = gbl_CYCLE_N / n_scan[i]; // set j_max
	  else j_max =N_SCLR;*/
	n_scan[i]++;// = n_scan[i]++;
        if (i == 1) { 
	  printf("Resetting the SCAN histogram, scan number = %i\n",gbl_SCAN_N);
	/*if(fp != NULL) 
	  fprintf(fp,"#bad_scan_flag = %i\n",bad_scan_status);*/
          /*if(bad_scan_flag == 0) {
            printf("Scan %i = good scan, adding histograms to the sums\n",n_scan[i]);
	    k=0;
	    while (k < j_max) {
	      printf("-volt=%lf-cscal=%lf-",voltage_array[k],SUM_array[i][k]);
	      h_SUM[i] -> Fill(voltage_array[k],SUM_array[i][k]);
	      k++;
	      }
	      }*/
	/*else if(bad_scan_flag == 1) 
            printf("Scan %i = bad scan, discarding last set of histograms from sums\n",n_scan[i]); 
	    else printf("Strange scan flag, discarding last set of histograms from sums\n");*/
	}
      }
      //      printf("Filling SCAN histogram at %lf with %f\n",dac_val_print,cscal);
      //printf("%f %f\n", dac_val_print, cscal);
      h_SCAN[i]->Fill(dac_val_print,cscal);
      //printf("Filling SUM histogram\n");
      //      fflush(stdout);
      h_SUM[i] ->Fill(dac_val_print,cscal); 
      //      printf("Updating the SUM_array[%i][%i]\n",i,j);
      /*SUM_array[i][j] = cscal;*/
      //      printf("DONE !\n");
      if( (fp != NULL) && (i == 1) ){
	//printf("Saving in ASCII file\n");
	//dac_val_print = ((double)dac_val)/10;
	/*voltage_array[j] = dac_val_print;*/
	//dvm_readback_print = ((double)dvm_readback_0)/100;
	//fprintf(fp, " %4d\t %2d\t %10lf\t %4.0f\t %10lf\n",
	//	gbl_CYCLE_N, gbl_SCAN_N, dac_val_print, cscal, dvm_readback_print_0 );
	/*if (gbl_SCAN_N == 2)

         fprintf(fp,"%10lf\t%4.0f\n",dac_val_print,cscal);*/
      }
      if( (fp != NULL) && (i == 4) ){ // for FM2 for Fluorine
		fprintf(fp, " %4d\t %2d\t %10lf\t \t %10lf\t %10lf\t %4.0f\n",
		gbl_CYCLE_N, gbl_SCAN_N, dac_val_print, dvm_readback_print_0, dvm_readback_print_1,cscal);
      }
      /*if (i==1){
	printf("Filling 'REAL' histograms\n");
	h_REAL     -> Fill (dvm_readback_print_0,cscal);
	h_REAL_SUM -> Fill (dvm_readback_print_0,cscal);
	}*/
    }
    else if(gbl_scan_flag > 255){
      /* Camp was varied */
      cscal = (float)*((double *)pdata); 
      /* printf("Word %d = %g (%f) at cyle %i with CAMP\n",i,
       *((double *)pdata),cscal,gbl_CYCLE_N);*/
      (*((double *)pdata))++;
      /* fill histo */
      //     if(fzero)
      if(n_scan[i] != gbl_SCAN_N){
	h_SCAN[i]->Reset();
        /*for (k=0; k<j_max; k++) { 
          h_SUM[i] ->Fill(voltage_array[k],SUM_array[i][k]); 
        } 
	if (n_scan[i] != 0)
	  j_max = gbl_CYCLE_N / n_scan[i]; // set j_max
	  else j_max = N_SCLR;*/
	n_scan[i]++;// = n_scan[i]++;
        if (i == 1) { 
          if(fp != NULL) 
            fprintf(fp,"#bad_scan_flag = %i\n",bad_scan_status);
          /*if(bad_scan_flag == 0) 
            printf("Good scan, adding histograms to the sums\n");
          else if(bad_scan_flag == 1) 
            printf("Bad scan, discarding last set of histograms from sums\n"); 
	    else printf("Strange scan flag, discarding last set of histograms from sums\n");*/
          printf("New scan... scan number %i\n",n_scan[i]);
        } 
      }
      if(strcmp (camp_dev,"dac") == 0) {
        h_SCAN[i]->Fill(camp_readback,cscal);
	h_SUM[i] ->Fill(camp_readback,cscal);
	/*voltage_array[j] = camp_readback;
	  SUM_array[i][j] = cscal;*/
	if( (fp != NULL) && (i == 1) )
	fprintf (fp, " %6d %6d %8.4lf %8.0f\n",
	   gbl_CYCLE_N, gbl_SCAN_N, camp_readback, cscal);
      } else {
	h_SCAN[i]->Fill(camp_val,cscal);
	h_SUM[i] ->Fill(camp_val,cscal);
	/*voltage_array[j] = camp_val;
	  SUM_array[i][j] = cscal;*/
	}
    }
    else if(gbl_scan_flag == 0){
      /* Dummy variable was varied */
      cscal = (float)*((double *)pdata); 
      /* printf("Word %d = %g (%f) at cyle %i with DUMMY\n",i,
       *((double *)pdata),cscal,gbl_CYCLE_N);*/
      (*((double *)pdata))++;
      /* fill histo */
      //     if(fzero)
      if(n_scan[i] != gbl_SCAN_N){
	h_SCAN[i]->Reset();
        /*for (k=0; k<j_max; k++) { 
          h_SUM[i] ->Fill(voltage_array[k],SUM_array[i][k]); 
        } 
	if (n_scan[i] != 0)
	  j_max = gbl_CYCLE_N / n_scan[i]; // set j_max
	  else j_max = N_SCLR;*/
	n_scan[i]++;// = n_scan[i]++;
        if (i == 1) {
          if(fp != NULL) 
            fprintf(fp,"#bad_scan_flag = %i\n",bad_scan_status); 
          /*if(bad_scan_flag == 0) 
            printf("Good scan, adding histograms to the sums\n"); 
          else if(bad_scan_flag == 1) 
            printf("Bad scan, discarding last set of histograms from sums\n"); 
	    else printf("Strange scan flag, discarding last set of histograms from sums\n"); */
          printf("New scan... scan number %i\n",n_scan[i]); 
        } 
      }
      h_SCAN[i]->Fill(freq_val,cscal);
      h_SUM[i] ->Fill(freq_val,cscal);
      /*voltage_array[j] = freq_val;
	SUM_array[i][j] = cscal;*/
    }
    else if(gbl_scan_flag == 4){
      /* Scaler was varied */
      cscal = (float)*((double *)pdata); 
      /* printf("Word %d = %g (%f) at cyle %i with SCALER\n",i,
                   *((double *)pdata),cscal,gbl_CYCLE_N);*/
      (*((double *)pdata))++;
      /* fill histo */
      //     if(fzero)
      if(n_scan[i] != gbl_SCAN_N){
	h_SCAN[i]->Reset();
	/* for (k=0; k<j_max; k++) { 
          h_SUM[i] ->Fill(voltage_array[k],SUM_array[i][k]); 
        } 
	if (n_scan[i] != 0)
	  j_max = gbl_CYCLE_N / n_scan[i]; // set j_max
	  else j_max = N_SCLR;*/
	n_scan[i]++;// = n_scan[i]++;
        if (i == 1) {
	  printf("Resetting SCAN histogram at cycle %i\n",gbl_CYCLE_N);
          if(fp != NULL) 
            fprintf(fp,"#bad_scan_flag = %i\n",bad_scan_status); 
          /*if(bad_scan_flag == 0) 
            printf("Good scan, adding histograms to the sums\n"); 
          else if(bad_scan_flag == 1) 
            printf("Bad scan, discarding last set of histograms from sums\n"); 
	    else printf("Strange scan flag, discarding last set of histograms from sums\n"); */
          printf("New scan... scan number %i\n",n_scan[i]); 
        } 
      }
      //      printf("Filling SCAN histogram at cycle %i\n",gbl_CYCLE_N);
      h_SCAN[i]->Fill(gbl_CYCLE_N,cscal);
      //      printf("Filling SUM histogram at cycle %i\n",gbl_CYCLE_N);
      h_SUM[i] ->Fill(gbl_CYCLE_N,cscal);
      //      printf("%i\t",gbl_SCAN_N);
      /*voltage_array[j] = gbl_CYCLE_N;
      SUM_array[i][j] = cscal;*/
    }
  }
  /*if (gbl_scan_flag == 1){
    //dvm_readback_print = ((double)dvm_readback_0)/1000000;
    h_FC2    -> Fill(epics_read,dvm_readback_print_1);
  }
  else*/ if (gbl_scan_flag == 8){
    //printf("%f, %f\n", dac_val_print, dvm_readback_print_0);
    h_LINEAR -> Fill(dac_val_print,dvm_readback_print_0);
    /*dvm_readback_print = ((double)dvm_readback_1)/1000000; */
    //h_FC2    -> Fill(dac_val_print,dvm_readback_print_1);
  }

  //  prev_scan = gbl_SCAN_N;
  /*
  size = sizeof(bad_scan_flag);
  status = db_get_value(hDB, 0, "/Equipment/FIFO_acq/Pol params/Bad scan flag", 
			&bad_scan_flag, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS){
    printf ("Cannot retrieve bad_scan_flag, setting to STRANGE\n");
    bad_scan_flag = 2;
  }
  bad_scan_status = (INT)bad_scan_flag;
  */
  /*  printf("bad_scan_flag = %i\tvoltage_array[%i]=%lf\tSUM_array[1][%i]=%lf\n",
      bad_scan_status,j,voltage_array[j],j,SUM_array[1][j]);*/
  //j++;

  fflush(fp);

  return SUCCESS;
}
/*-- End of Run -----------------------------------------------------*/

INT cyc_hist_endrun(void) {
  /* Deleting histograms in order not to keep polluting around */
  // printf("Deleting SCAN histograms\n");
  //delete [] h_SCAN;
  //delete [] h_SUM;
  /*if (dac_flag == 1){
    printf("Deleting NaCell histograms\n");
    delete h_FC2;
  }
  else*/ if (dac_flag == 8){
    //printf("Deleting DAC histograms\n");
    //delete h_LINEAR;
    /*delete h_REAL;
      delete h_REAL_SUM;*/
    /* delete h_FC2;*/
  }
  if(fp != NULL) {
    fprintf(fp,"\n#End of run\n");
    fclose (fp);
    printf("Closing ASCII data file\n");
    }
  return SUCCESS;
}

/*-- Analyzer exit ---------------------------------------------------*/

INT cyc_hist_exit(void) {
/*  if(fp != NULL) {
    fprintf(fp,"\n#End of run\n");
    fclose (fp);
    printf("Closing ASCII data file\n");
  }*/
  return SUCCESS;
}
