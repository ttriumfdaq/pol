/********************************************************************\

  Name:         analyzer.c
  Created by:   Stefan Ritt
  Modified by:  Thomas Cocolios

  Contents:     System part of Analyzer code for sample experiment

  $Log: analyzer.c,v $
  Modification  2004/06/29
  Adaptation to the Pol Experiment

  Revision 1.3  1998/10/29 14:18:19  midas
  Used hDB consistently

  Revision 1.2  1998/10/12 12:18:58  midas
  Added Log tag in header


/********************************************************************/
                                                        
/* standard includes */
#include <stdio.h>
#include <time.h>


/* midas includes */
#include "midas.h"
#include "experim.h" 
#include "analyzer.h"

/* cernlib includes */
#ifdef OS_WINNT
#define VISUAL_CPLUSPLUS
#endif
#ifdef __linux__
#define f2cFortran
#endif
#include <cfortran.h>

/*-- Globals -------------------------------------------------------*/

/* The analyzer name (client name) as seen by other MIDAS clients   */
char *analyzer_name = "Analyzer";

/* analyzer_loop is called with this interval in ms (0 to disable)  */
INT  analyzer_loop_period = 0;

/* default ODB size */
INT  odb_size = DEFAULT_ODB_SIZE;

/*-- Module declarations -------------------------------------------*/

extern ANA_MODULE cyc_hist_module;

ANA_MODULE *cycscal_module[] = {
  &cyc_hist_module,
  NULL
};

/*-- Bank definitions ----------------------------------------------*/

BANK_LIST cycscal_bank_list[] = {
  /* online banks */
  { "CYCI", TID_DWORD, 9, NULL },
  { "HSCL", TID_DOUBLE, N_CYCSCAL, NULL },

  { "" },
};

/*-- Event request list --------------------------------------------*/

ANALYZE_REQUEST analyze_request[] = {
  { "Cycle_Scalers",      /* equipment name   */
    3,                    /* event ID         */
    -1,                   /* trigger mask     */
    GET_SOME,             /* get some events  */
    "SYSTEM",             /* event buffer     */
    TRUE,                 /* enabled          */
    "", "", 
    NULL,                 /* analyzer routine */
    cycscal_module,       /* module list      */
    cycscal_bank_list,    /* bank list        */
    50000,                /* RWNT buffer size */
  },
  { "" }
};

/*-- Analyzer Init -------------------------------------------------*/

INT analyzer_init()
{
extern HNDLE hDB;
HNDLE hKey;
char  str[80];

  return SUCCESS;
}

/*-- Analyzer Exit -------------------------------------------------*/

INT analyzer_exit()
{
  cyc_hist_exit();
  return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT ana_begin_of_run(INT run_number, char *error)
{
  cyc_hist_beginrun();
  return CM_SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT ana_end_of_run(INT run_number, char *error)
{
FILE   *f;
time_t now;
char   str[256];
int    size;
DWORD  n;
HNDLE  hDB;
BOOL   flag;

 cyc_hist_endrun();
 return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT ana_pause_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT ana_resume_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- Analyzer Loop -------------------------------------------------*/

INT analyzer_loop()
{
  return CM_SUCCESS;
}

/*------------------------------------------------------------------*/









