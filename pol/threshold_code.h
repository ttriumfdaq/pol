// threshold_code.h

#ifndef _THRCODE_INCLUDE_H_
#define _THRCODE_INCLUDE_H_


#define NTHR 4 // number of defined thresholds
HNDLE hThr, hRef[NTHR+1], hCT[NTHR], hSK, hDT; // one extra reref for "all"
BOOL disable_thresholds[NTHR+1];
BOOL hot_reref[NTHR+1],lhot_reref[NTHR+1];
float ref_value[NTHR]; // set to reference value of scaler[i].sum_cycle
float ref_thr[NTHR];   // reference thresholds ; set to ref_value on rereference
float current_thr[NTHR]; // set to ref_value in test_thresholds
float cycle_thresholds[NTHR];
DWORD thr_fail_cntrs[NTHR];
INT prev_failed_thr; // the last threshold test that failed
INT failed_thr; // 0 if this cycle passed thresholds, otherwise 1-4
INT skip_ncycles_out_of_tol;
INT ncycle_sk_tol;
BOOL enable_thr_check=FALSE;
BOOL  init_thr_flag=FALSE; // reset flag, thresholds not yet initialized

INT get_threshold_keys(void); // odb keys
INT setup_hotthresholds(void); // hot links
void check_hot_rerefs(void);
INT  threshold_new_cycle( int NumSisChannels);
INT hot_reference_clear(INT item);
INT test_thresholds(void); // returns 0 for all passed or the test number that first failed
INT clear_thr_alarm(void);
INT set_thr_alarm(INT alarm_num);
INT close_hotthresholds(void);
void call_back(HNDLE hDB, HNDLE hkey, void * info);
void call_back_thr(HNDLE hDB, HNDLE hkey, void * info);
void clear_thresholds(void);
#endif
