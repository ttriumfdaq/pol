/***************************************************************************/
/*                                                                         */
/*  Filename: VMIVME3801.h                                                 */
/*                                                                         */
/*       headerfile for Pol's ADC                                          */    
/*                                                                         */
/*           VMIVME-3801                                                   */
/*                                                                         */
/***************************************************************************/

#ifndef __VMIVME3801__
#define __VMIVME3801__

/* ADC addresses  (offsets from base) */

#define ADC_V3801_BOARD_ID                              0x0             /* Board ID        ; D8  RO */
#define ADC_V3801_CONFIG                                0x1             /* Configuration   ; D8  RO */
#define ADC_V3801_CONTROL_STATUS                        0x2             /* CSR             ; D8  RW */
#define ADC_V3801_CHAN_PNTR                             0x3             /* Channel Pointer ; D8  RO */
#define ADC_V3801_CHAN_0                                0x40            /* Channel 0 Data  ; D8  RW */
#define ADC_V3801_CHAN_1                                0x42            /* Channel 1 Data  ; D8  RW */
#define ADC_CHAN_OFFSET                                 0x2             /*  Offset of each channel data from previous i.e. 0x40,0x42...0x7e */

#define ADC_BOARDID                      0x44    /* ADC Board ID */
#define ADC_TWOS_COMP_BIT                   4    /* Set 2s complement in  ADC_V3801_CONTROL_STATUS reg */
#define ADC_LED_OFF_BIT                  0x80    /* Set LED OFF in  ADC_V3801_CONTROL_STATUS reg */

#define ADC_CSR_LED_OFF   ADC_TWOS_COMP_BIT  |  ADC_LED_OFF_BIT 
#define ADC_CSR_LED_ON    ADC_TWOS_COMP_BIT 

#endif // __VMIVME3801__
