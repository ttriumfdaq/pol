// utils.cxx

int odbReadAny(const char*name,int index,int tid,void* value,int valueLength = 0)
{
  int status;
  int size = rpc_tid_size(tid);
  HNDLE hdir = 0;
  HNDLE hkey;

  if (valueLength > 0)
    size = valueLength;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status == SUCCESS)
    {
      status = db_get_data_index(hDB, hkey, value, &size, index, tid);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot read \'%s\'[%d] of type %d from odb, db_get_data_index() status %d", name, index, tid, status);
          return -1;
        }
      
      return 0;
    }
  else if (status == DB_NO_KEY)
    {
      cm_msg(MINFO, frontend_name, "Creating \'%s\'[%d] of type %d", name, index, tid);
      
      status = db_create_key(hDB, hdir, (char*)name, tid);
      if (status != SUCCESS)
        {
          cm_msg (MERROR, frontend_name, "Cannot create \'%s\' of type %d, db_create_key() status %d", name, tid, status);
          return -1;
        }

      status = db_find_key (hDB, hdir, (char*)name, &hkey);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot create \'%s\', db_find_key() status %d", name, status);
          return -1;
        }

      status = db_set_data_index(hDB, hkey, value, size, index, tid);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot write \'%s\'[%d] of type %d to odb, db_set_data_index() status %d", name, index, tid, status);
          return -1;
        }

      return 0;
    }
  else
    {
      cm_msg(MERROR, frontend_name, "Cannot read \'%s\'[%d] from odb, db_find_key() status %d", name, index, status);
      return -1;
    }
};

int odbReadInt(const char*name,int index = 0,int defaultValue = 0)
{
  int value = defaultValue;
  if (odbReadAny(name,index,TID_INT,&value) == 0)
    return value;
  else
    return defaultValue;
};

uint32_t odbReadUint32(const char*name,int index = 0,uint32_t defaultValue = 0)
{
  uint32_t value = defaultValue;
  if (odbReadAny(name,index,TID_DWORD,&value) == 0)
    return value;
  else
    return defaultValue;
};

double odbReadDouble(const char*name,int index = 0,double defaultValue = 0)
{
  double value = defaultValue;
  if (odbReadAny(name,index,TID_DOUBLE,&value) == 0)
    return value;
  else
    return defaultValue;
};

bool     odbReadBool(const char*name,int index = 0,bool defaultValue = 0)
{
  bool value = defaultValue;
  if (odbReadAny(name,index,TID_BOOL,&value) == 0)
    return value;
  else
    return defaultValue;
};

const char* odbReadString(const char*name,int index,const char* defaultValue,int stringLength)
{
  static char buf[256];
  strcpy(buf,defaultValue);
  if (odbReadAny(name,index,TID_STRING,buf,stringLength) == 0)
    return buf;
  else
    return defaultValue;
};


int odbWriteAny(const char*name,int index,int tid,void* value,int valueLength = 0)
{
  int status;
  int size = rpc_tid_size(tid);
  HNDLE hdir = 0;
  HNDLE hkey;

  if (valueLength > 0)
    size = valueLength;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status == SUCCESS)
    {
      status = db_set_data_index(hDB, hkey, value, size, index, tid);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot write \'%s\'[%d] of type %d to odb, db_set_data_index() status %d", name, index, tid, status);
          return -1;
        }
    }
  else if (status == DB_NO_KEY)
    {
      cm_msg(MINFO, frontend_name, "Creating \'%s\'[%d] of type %d", name, index, tid);
      
      status = db_create_key(hDB, hdir, (char*)name, tid);
      if (status != SUCCESS)
        {
          cm_msg (MERROR, frontend_name, "Cannot create \'%s\' of type %d, db_create_key() status %d", name, tid, status);
          return -1;
        }
      
      status = db_find_key (hDB, hdir, (char*)name, &hkey);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot create \'%s\', db_find_key() status %d", name, status);
          return -1;
        }
      
      status = db_set_data_index(hDB, hkey, value, size, index, tid);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot write \'%s\'[%d] of type %d to odb, db_set_data_index() status %d", name, index, tid, status);
          return -1;
        }
      
      return 0;
    }
  else
    {
      cm_msg(MERROR, frontend_name, "Cannot write \'%s\'[%d] to odb, db_find_key() status %d", name, index, status);
      return -1;
    }
  return 0;
};



// end
