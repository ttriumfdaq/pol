// globals are in threshold_code.h

INT get_threshold_keys(void)
{
  // called from frontend_init
  char str[80];
  char path[]="/Equipment/Pol_acq/Settings/Cycle Thresholds"; // for error messages
  INT status,i;
  
  sprintf(str,"Cycle Thresholds");
  status = db_find_key(hDB, hTASet, str, &hThr);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_thresholds","Cannot get key for %s (%d)\n",path,status);
      return status;
    }


  // hSK=hDT=hRef[NTHR+1]=0; // initialize handles
hSK=hDT=hRef[NTHR]=0; // initialize handles  - remove warning  array subscript is above array bounds
  for (i=0;i<NTHR;i++)
    {
      hRef[i]=0;
      hCT[i]=0;
    }


  status = setup_hotthresholds();
  if(status != SUCCESS)
    return status;
  
  
  /*  for (i=0; i<NTHR; i++)
    {
      size = sizeof(cycle_thresholds[i]);
      sprintf(str,"Threshold%d",(i+1));
        status = db_get_value(hDB, hThr, str, &cycle_thresholds[i],&size,TID_FLOAT,TRUE );
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_thresholds","Cannot get value for %s/%s (%d)\n",path,str,status);
	  return status;
	  }
}*/

  cycle_thresholds[0]=ps.cycle_thresholds.threshold1;
  cycle_thresholds[1]=ps.cycle_thresholds.threshold2;
  cycle_thresholds[2]=ps.cycle_thresholds.threshold3;
  cycle_thresholds[3]=ps.cycle_thresholds.threshold4;

  for (i=0; i<NTHR; i++)
    printf("Threshold %d  cycle threshold[%d]=%f\n",i+1,i,cycle_thresholds[i]);
  
  /*
  size = sizeof(skip_ncycles_out_of_tol);
  status = db_get_value(hDB, hThr, "num cycles to skip out-of-tol", &skip_ncycles_out_of_tol ,&size,TID_INT,TRUE );
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_thresholds","Cannot get value for \"%s/num cycles to skip out-of-tol\" (%d)\n",path,status);
      return status;
    }
  */  
  skip_ncycles_out_of_tol = ps.cycle_thresholds.num_cycles_to_skip_out_of_tol;
  printf("skip_ncycles_out_of_tol =%d\n",skip_ncycles_out_of_tol);


  /* size = sizeof(enable_thr_check);
  status = db_get_value(hDB, hThr, "Disable threshold checking",&enable_thr_check ,&size,TID_BOOL,TRUE );
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_thresholds","Cannot get value for \"%s/Disable threshold checking\" (%d)\n",path,status);
      return status;
    }  
  */
  enable_thr_check =  ps.cycle_thresholds.enable_threshold_checking;
  printf("enable_thr_check =%d\n",enable_thr_check);

  return status;
}

INT setup_hotthresholds(void)
{
  INT status,i;
  char str[32];
  status=SUCCESS;

  // called from get_threshold_keys and BOR


  for (i=0; i<NTHR+1; i++)
    {
      /* set up hot links on re-references  */
      if(hRef[i] == 0)
	{
	  if(i==0)
	    sprintf(str,"Reref all");
	  else
	    sprintf(str,"Reref%d",i);
	  status = db_find_key(hDB, hThr, str, &hRef[i]);
	  if (status == DB_SUCCESS)
	    {
	      status = db_open_record(hDB, hRef[i], &lhot_reref[i]
				      , sizeof(lhot_reref[i])
				      , MODE_READ, call_back,  (void *)str);
	      if (status != DB_SUCCESS) 
		cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	    }
	  else
	    cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
	}
    

      if (i < NTHR) // one less threshold than reref
	{  /* set up hot links on thresholds  */

	  if(hCT[i] == 0)
	    {
	      sprintf(str,"Threshold%d",(i+1));
	      status = db_find_key(hDB, hThr, str, &hCT[i]);
	      if (status == DB_SUCCESS)
		{
		  status = db_open_record(hDB, hCT[i], &cycle_thresholds[i]
					  , sizeof(cycle_thresholds[i])
					  , MODE_READ, call_back,   NULL);
		  if (status != DB_SUCCESS) 
		    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
		}
	      else
		cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
	    }
	}
    }
  if (hSK==0)
    {
      /* setup hot link on "skip ncycles out-of-tol" */
      sprintf(str,"num cycles to skip out-of-tol");
      status = db_find_key(hDB, hThr, str, &hSK);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hSK, &skip_ncycles_out_of_tol
				  , sizeof(skip_ncycles_out_of_tol)
				  , MODE_READ, call_back,  NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

  if (hDT==0)
    {
      /* setup hot link on "enable threshold checking" */
      sprintf(str,"Enable threshold checking");
      status = db_find_key(hDB, hThr, str, &hDT);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hDT, &enable_thr_check
				  , sizeof(enable_thr_check)
				  , MODE_READ, call_back_thr,  NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }
  return status;
}

void clear_thresholds(void)
{
  // called from begin_of_run
  int i;
  printf("clear_thresholds: starting\n");
  init_thr_flag=FALSE; // reset flag, thresholds not yet initialized
  for (i=0; i<NTHR; i++)
    ref_value[i]=ref_thr[i]=current_thr[i]=0.;
  prev_failed_thr = 0; 
  return;
}

/*-- Call_back  ----------------------------------------------------*/
void call_back(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
}

/*-- Call_back  ----------------------------------------------------*/
void call_back_thr(HNDLE hDB, HNDLE hkey, void * info)
{

  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char*)info);
  if ( enable_thr_check == TRUE )
    init_thr_flag=FALSE; 
  printf("call_back_thr:  set init_thr_flag FALSE; thresholds need to be initialized\n\n");
}

INT close_hotthresholds(void)
{
  INT status,i;
  status=SUCCESS;
  printf("close_hotthresholds: starting\n");
  for (i=0; i<NTHR+1; i++)
    printf("close_hotthresholds:  hRef[%d]=%d\n",i,(int)hRef[i]);


  for (i=0; i<NTHR+1; i++)
    {
      if(hRef[i] != 0)
	{
	  printf("closing record for handle hRef[%d]=%d\n",i,(int)hRef[i]);
	  status = db_close_record(hDB, hRef[i]);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","close record on threshold index %d failed (%d)",i,status);
	  else
	    printf("successfully closed record for  handle hRef[%d]=%d\n",i,(int)hRef[i]);
	}
    }
  printf("returning status=%d\n",status);
  return status;
}



void check_hot_rerefs(void)
{
  int i;
  // called from cycle_start

  if(!enable_thr_check)
    {
      //printf("check_hot_rerefs: returning.. threshold checks are disabled\n");
      return;
    }

  if( lhot_reref[0])
    {    /* re-ref ALL hotlinks */
      hot_reref[0] = TRUE;
      //if(debug)
	printf("\ncycle_start: detected hot rereference (ALL)...\n");
    }
  else
    {     
      for (i=1; i<NTHR+1; i++)
	if(lhot_reref[i])
	  {
	    hot_reref[i] = TRUE;
	    //if(debug)
	    printf("\ncycle_start: detected hot rereference (%d) ...\n",i);
	  }
    }
  for(i=0; i<NTHR+1; i++)
    lhot_reref[i]=0;
  return;
}


INT threshold_new_cycle(int NumSisChannels)
{
  /* called from histo_process
         moves scaler sums to ref_value array; checks for reref
  */
  int status,i;
  int j=0;

  if(NumSisChannels < NTHR)
    {
      cm_msg(MERROR,"check_for_reref","too many thresholds defined for the number of channels");
      return DB_INVALID_PARAM;
    }

  for (i=0; i<NTHR; i++)
    ref_value[i]= scaler[i].sum_cycle;
  
  if(gbl_CYCLE_n == 1 || (!init_thr_flag)) // first valid cycle of the run
    {  
      init_thr_flag = TRUE;
      failed_thr = 0 ; // clear flags
      printf("\nFirst valid cycle...reset reference thresholds to current (scaler.sum_cycle) : \n");
        for (i=0; i<NTHR; i++)
	  {
	    ref_thr[i] = ref_value[i];
            thr_fail_cntrs[i]=0;
	    printf("sum_cycle[%d] = %f ",i,ref_thr[i]);
	  }
	printf("\n");
    }
  else
    {
      /* check for Re-reference of all the thresholds  */
      if (hot_reref[0]) // all channels
	{
	  printf("\nHot rereference ... reset all reference thresholds to current : ");
	  for (i=0; i<NTHR; i++)
	    {
	      ref_thr[i] = ref_value[i];
	      printf("[%d] = %f ",i,ref_thr[i]);
	    }
	  printf("\n");
	  status = hot_reference_clear(0); /* 0 clears all */
	  if(status  != SUCCESS)
	    return status;
	  ncycle_sk_tol = 0; /* counter for skipping cycles out-of-tol */
	}
      else
	{ // clear individual thresholds
	  for (i=0; i<NTHR; i++)
	    {
	      j++; // j goes from 1,NTHR+1
	      if (hot_reref[j])
		{
		  printf("\nRereference.....reset reference threshold %d to current (%f)\n",
			 j,ref_thr[i]);
		  
		  ref_thr[i] = ref_value[i];
		  status = hot_reference_clear(j);
		  if(status != SUCCESS)
		    return status;
		  ncycle_sk_tol = 0; /* counter for skipping cycles out-of-tol */
		}
	    }
	}

    }
  return SUCCESS;
}

/*-- Clear hot link ------------------------------------------------------*/
INT hot_reference_clear(INT item)
/*------------------------------------------------------------------------*/
{
  /* POL version: item = 0 clear all
         otherwise clear appropriate threshold 1-4
  */
  INT status,size,i;
  char str[80];


  if( (item < 0) || (item > (NTHR+1)) )
    {
      cm_msg(MERROR,"hot_reference_clear","error - called with illegal parameter (%d)",item);
      return DB_INVALID_PARAM;
    }
  switch (item)
    {
    case 0: 
      {
	for (i=0; i<NTHR+1; i++)
	    hot_reref[i] = FALSE;

	size = sizeof(hot_reref[0]);
	status = db_set_value(hDB, hThr, "Reref all", &hot_reref[0], size, 1, TID_BOOL);
	if(status != SUCCESS)
	  {
	    cm_msg(MERROR,"hot_reference_clear","Could not clear \"Reref all\" (%d)",status);
	    return status;
	  }
	printf("hot_reference_clear: Cleared  \"Reref all\" in odb\n");

        // clear the others in case they were set also
        for (i=1; i<NTHR+1; i++) // NTHR=4  clear Reref1-4
	  { 
	    sprintf(str,"Reref%d",i);
	    status =db_set_value(hDB, hThr, str, &hot_reref[i+1], size, 1, TID_BOOL);
            printf("cleared   \"%s\"\n",str);   
	  }
	break;
      }
    default:
      {
	hot_reref[item] = FALSE;
	size = sizeof(hot_reref[item]);
	sprintf(str,"Reref%d",item);
	status =db_set_value(hDB, hThr, str, &hot_reref[item], size, 1, TID_BOOL);
	if(status != SUCCESS)
	  {
	    cm_msg(MERROR,"hot_reference_clear","Could not clear \"%s\" (%d)",str,status);
	    return status;
	  }
	printf("hot_reference_clear: Cleared \"%s\" in odb\n",str);
	break;
      }
    }
  return status;
}


INT test_thresholds(void)
{
  int i,j=0; /* thr indices i are 0-NTHR; thr j are numbered 1 - NTHR+1 */
  float ratio;
  BOOL test_failed;

  gbl_in_tol = 1;  // in tol
  test_failed = FALSE; // clear flag

  for(i=0; i<NTHR;i++)
    {
      current_thr[i]=ref_value[i]; // copy present scaler[i].sum_cycle to current_thr[i]
      if(debug)printf("test_thresholds: Updating current thr %d value  to %f \n",i,ref_value[i]);
    }

  if(gbl_CYCLE_n == 1) // no point in checking on very first cycle
    return 0; // SUCCESS 
  
  for(i=0; i<NTHR;i++)
    { 
      j++; // i = index to array, j = threshold test number
      if (cycle_thresholds[i] > 0.f)
	{  // this threshold is enabled
	  
	  if( ps.cycle_thresholds.mode__0___1_level_ == 1)
	    { // discriminator threshold (threshold_mode=1)
	      if( current_thr[i] > ref_thr[i] + cycle_thresholds[i] )
		{
		  /* Test fails; */
		  cm_msg(MINFO,"check_thresholds",
			 "Cycle below thr%d (Current %7.0f >%7.0f (Ref %7.0f + threshold %4.0f); out of tolerance",
			 (i+1), current_thr[i], ref_thr[i], cycle_thresholds[i] );
		  test_failed = TRUE;
		} 
	    }
	  else
	    { // regular threshold 

	      if(ref_thr[i] != 0)
		ratio = fabs(ref_thr[i] - current_thr[i]) / ref_thr[i];
	      else
		ratio = 0.;
	      
	      if(hot_debug)printf("Thr Test %d: Cycle thr level:%.1f%% (%.3f); current %.0f; ref %.0f -> ratio=%.3f",
				  j,
				  cycle_thresholds[i],(cycle_thresholds[i]/100.),
				  current_thr[i],
				  ref_thr[i], ratio);
	      
	      if(ratio > ((double)cycle_thresholds[i] / 100.))
		{/* Test fails; */

		  cm_msg(MINFO,"check_thresholds",
			 "Cycle below thr%d (Ref = %7.0f, Current = %7.0f;  %5.1f % > %5.1f%);  out of tolerance",
			 (i+1),ref_thr[i],  current_thr[i], (ratio*100), cycle_thresholds[i] );
		  
		  //if(debug)printf("Test %d fails, Ref = %7.0f; Current = %7.0f; Ratio (%5.3f) > ref cycle thr/100 (%5.3f)\n",
		  //      i,ref_thr[i],  current_thr[i],ratio, 
		  //      (cycle_thresholds[i] / 100.));
		  test_failed = TRUE;
		}
	    } // end of regular threshold test
	
	  if (test_failed)
	    { // threshold test failed

	      if(hot_debug)printf(" failed\n");
	      gbl_skip_cycle = TRUE;
	      ncycle_sk_tol = skip_ncycles_out_of_tol;
	      prev_failed_thr = failed_thr; // remember the last threshold that failed
	      if( failed_thr > 0 && failed_thr != j ) 
		{
		  clear_thr_alarm();
		  set_thr_alarm(j); /* alarm should go off */
		}
	      failed_thr=j; // threshold that last failed
	      gbl_in_tol=0; // cycle is out of tol
	      thr_fail_cntrs[i]++;
	      return j; /* fail Thr Test */
	    } // threshold test failed
	  else
	    if(hot_debug)printf(" Passed\n");
	}
    }
  
  
  // All tests have passed
  if(failed_thr > 0 )
    {  
      clear_thr_alarm();
      failed_thr=0; // none failed (prev_failed_thr is not cleared).
    }

  if (ncycle_sk_tol > 0)
    {
      gbl_skip_cycle = TRUE;
      printf("Cycle is in tolerance but ncycle_sk_tol still positive (%i)\n",
	     ncycle_sk_tol);
      ncycle_sk_tol--;
    }
  return 0; // all passed
}

INT clear_thr_alarm(void)
{
  INT status, num;
  /* al_reset_alarm returns with status
     AL_INVALID_NAME         Alarm name not defined
     AL_RESET                Alarm was triggered and reset
     AL_SUCCESS              Successful completion
  */

  num=0; 
  char set_alarm_str[]="/Equipment/pol_acq/settings/cycle thresholds/alarm";
  status = db_set_value(hDB, 0, set_alarm_str, &num, sizeof(num), 1, TID_INT);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"set_thr_alarm","error setting ODB key \"%s\" to %d (%d)",set_alarm_str,num,status);
      return status;
    }
  
  status = al_reset_alarm("threshold");
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"clear_thr_alarm","error resetting threshold alarm (%d)",status);
      return status;
    }
  return status;
}

INT  set_thr_alarm(INT alarm_num)
{
  char alarm_str[]="/alarms/alarms/threshold/alarm message";
  char set_alarm_str[]="/Equipment/pol_acq/settings/cycle thresholds/alarm";
  char msg_str[80];
  INT num,status;

  num=alarm_num;
  sprintf(msg_str,"Threshold check %d has failed",alarm_num);

  status = db_set_value(hDB, 0, alarm_str, msg_str, sizeof(msg_str), 1, TID_STRING);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"set_thr_alarm","error setting ODB key \"%s\" to \"%s\" (%d)",alarm_str,msg_str,status);
      return status;
    }

  status = db_set_value(hDB, 0, set_alarm_str, &num, sizeof(num), 1, TID_INT); /* alarm should go off */
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"set_thr_alarm","error setting ODB key \"%s\" to %d (%d)",set_alarm_str,num,status);
      return status;
    }
  return SUCCESS;
}

