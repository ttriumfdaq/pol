// run states from midas.h
var state_stopped=1
var state_paused=2
var state_running=3

// patterns
var pattern_y=/y/
var pattern_n=/n/
var pattern_no_key = /DB_NO_KEY/    // for ODBGET
var pattern_true = /true/;
var pattern_false= /false/;

// paths
var rstate_path= "/runinfo/state";
var tip_path = "/runinfo/transition in progress";
var tr_path = "/runinfo/requested transition";


var pol_path = "/Equipment/pol_acq/";
var input_path = pol_path+"settings/input/";
var output_path = pol_path+"settings/output/";
var sis_path=input_path+"sis3820/";
var thresh_path= pol_path + "settings/cycle thresholds/";
var polarity_path=input_path+"polarity"
var trig_path=input_path+"external trigger"
var watchdog_path = output_path + "watchdog"
var alarms_active_path = "/Alarms/Alarm system active";
var alarm_path1="/Alarms/alarms/pol_fevme/triggered"
var alarm_path2="/Alarms/alarms/fe_init/triggered"
var femsg_path=output_path+ "frontend msg";
var dac_min_step_path=input_path+"galil_rio/dac step check/minimum step (V)"
var dac_dis_step_check_path=input_path+"galil_rio/disable step check"

var dwell_time_ms_path    = input_path+"dwell time (ms)";
var dwell_time_ms;

// these paths need to be global for editing:

var path1 = input_path+"num bins requested";
var path2; // filled in make_stopped_param_table()
var path3 = input_path+"scratch/dwell time units";
var path4 = input_path+"DAC Start";
var path5 = input_path+"Num DAC Incr";
var path6 = input_path+"DAC Inc";
var path11 = input_path+"num cycles per supercycle";
var path16 = input_path+"Discard first bin";
var path28 = input_path+"Discard first cycle";
var path7 = input_path+"PPG external start";
var path8 = input_path+"DAC sleep time (ms)";
var path9 = input_path+"TOF pulse width (ms)";
var path18 = input_path+"hot debug";
var path19 = input_path+"hot timer";

var disable_dac_check;
var jvar16,jvar1; // need to be global to avoid reading twice

var junits=1;  // set to zero if unit change is not working
var path,val;
var ival,jvar;

var stop_path=input_path + 'stop at end-of-sweep'
var nscans_path = input_path+"Number of scans";

var jvar50,jvar51,jvar52,jvar53;
var path50= input_path+"galil_rio/dac step check/minimum step (V)";
var path51= input_path+"galil_rio/dac step check/jitter (V)";

var min_DAC_path = output_path + "DAC_min"  
var max_DAC_path = output_path + "DAC_max"

var min_DAC = ODBGet(min_DAC_path); // values filled by frontend according to range of DAC 
var max_DAC = ODBGet(max_DAC_path);


function unbool(jval)
{
    if(jval)
        return ("y");
    else
        return ("n");
}

function get_bool(jval,path)
{
  var ival=0;


 if(pattern_no_key.test(jval))
 {
     alert('get_bool: no key for path '+path+'  jval= '+jval);
     ival=-1
 }
  else
  {
    if( (pattern_y.test(jval)) ||  (pattern_true.test(jval)))
      ival=1;
    else
      ival=0;


   //  alert('get_bool:  jval= '+jval+ ' returning ival= '+ival)
  }
  return ival;
}

// Open the window for the refresh time
function openWindow()
{
secondWin = open ('TRef!','config','height=250,width=600,scrollbars=no');                
}

// convert dec to hex for output
function d2h(d) {return d.toString(16);}


function calc_scratch_dwell_time()
{  // fill "scratch" using "dwell time (ms)" and "scratch/units"

    //  alert("calc_scratch_dwell_time: starting")

   var path_units = input_path+"scratch/dwell time units";
   var path_value = input_path+"scratch/dwell time";
   var path_format = input_path+"sis3820/data format";
   var  SIS_min_dwell_time_ms;
    u = ODBGet(path_units);

    if(u == "ms")
       factor = 1;
    else if (u == "ns") 
       factor = 1000000; // ms to ns
    else if (u == "us")
       factor = 1000;    // ms to us
    else if  (u == "s")  
       factor = 0.001;      // s to ms
    else
    {  // set units to ms
       u="ms";
       factor =1;
       ODBSet(path_units, u);
   alert ('setting path '+path_units+' to '+u);
    }

    dwell_time_ms = ODBGet(dwell_time_ms_path);
    var numbits = ODBGet(path_format);
    var v_ms = parseFloat(dwell_time_ms);
    if(numbits == 8)
       SIS_min_dwell_time_ms = 0.000224;
    else if (numbits == 16)
       SIS_min_dwell_time_ms = 0.000234;
    else
       SIS_min_dwell_time_ms = 0.000244;
    if (v_ms < SIS_min_dwell_time_ms)
    {
       //document.write('Minimum dwell time is ' +SIS_min_dwell_time_ms+ ' ms for '+numbits+'-bit format data.'<br>'); 
       alert('Minimum dwell time is ' +SIS_min_dwell_time_ms+ ' ms for '+numbits+'-bit format data.'); 
       v_ms =  SIS_min_dwell_time_ms;
       ODBSet(dwell_time_ms_path, v_ms);
    }
    var v = v_ms * factor;
    //alert('calc_scratch_dwell_time:  dwell_time_ms_path = '+ dwell_time_ms_path+' dwell_time_ms= '+dwell_time_ms+' v_ms= '+v_ms);
    //alert('calc_scratch_dwell_time: v_ms = '+v_ms+' wrote v = '+v+' units='+u+' to scratch')
    ODBSet(path_value, v); // fill scratch dwell time value 
}


function dwell_time_convert()
{
   // converts value in  "scratch/dwell time" according to "scratch/units" to  input/dwell_time_ms
   var ivar,u;
   var v_ms,v,factor;
   // alert ('dwell time convert');
   var path_units = input_path+"scratch/dwell time units";
   var path_value = input_path+"scratch/dwell time";
   
   
   document.getElementById('debug').innerHTML +=path_units+'dwell time convert';
  
    u = ODBGet(path_units);
    //   alert ('input path: '+input_path+'units '+u);
  
    factor =1;

    if(u == "ms")
       factor = 1;
    else if (u == "ns") 
       factor = 1000000; // ms to ns
    else if (u == "us")
       factor = 1000;    // ms to us
    else if  (u == "s")  
       factor = 0.001;      // s to ms
    else
    {  // set units to ms
       u="ms";
       factor =1;
       ODBSet(path_units, u);
       //  alert ('setting path '+path_units+' to '+u);
    }

 
    ivar = ODBGet(path_value);
    alert ('dwell_time_convert:  path_value: '+path_value+'ivar '+ivar);
    v = parseFloat(ivar);
    v_ms = v / factor;
    //  alert ('dwell_time_convert: setting '+ dwell_time_ms_path+' to '+v_ms+' ms (factor = '+factor+')');
    ODBSet(dwell_time_ms_path, v_ms); // fill ms value
    //  alert (' dwell_time_ms_path: '+ dwell_time_ms_path+' v_ms= '+v_ms); 
   
}
  
function calculate_all()
{
     alert('calculate_all: starting')

    //  Call if scratch/dwell time (value or units) is changed
      dwell_time_convert(); //  converts value in  "scratch/dwell time" to "input/dwell time (ms)" according to "scratch/units"

    // Call at init if scratch is empty
       calc_scratch_dwell_time(); //   fill "scratch" using "dwell time (ms)" and "scratch/units"
                               //   writes dwell_time_ms_path only if < min

    update_time_bins(); // calculates  num_bins  and other parameters for the image labels


    //   alert('reloading page');
    //  javascript:location.reload(true); // reload to update labels

}

function update_time_bins()
{ // update the image labels if parameters have changed
  // DAC Scan Params is done by check_dac_params()
    var i,j;
    var update=0;

    // recalculate the number of time bins 
    var num_mcs_time_bins_path = output_path+"num bins per cycle";
    var num_mcs_time_bins = ODBGet(num_mcs_time_bins_path);
    var one_cycle_time_path_ms = output_path+"one cycle time (ms)";
    var one_cycle_time_path_sec = output_path+"one cycle time (sec)";
    

    jvar1= ODBGet(path1); // get requested number of time bins again (may have changed)
    i = parseInt(jvar1); // requested number of time bins (input param)
 
    j = parseInt(num_mcs_time_bins);  // actual number of time bins (output param)
    //alert('update_time_bins: requested number of time bins is '+i+' number time bins in output is '+j)
    if(get_bool(jvar16))
	i++; // add 1 to number of time bins
       
    if(i != j)
    {
	ODBSet(num_mcs_time_bins_path, i);
	//	alert('update_time_bins: updated number of time bins to '+i+' in path '+num_mcs_time_bins_path)
        update=1;
     }	

     // get the dwell time in ms
     var kvar = ODBGet( dwell_time_ms_path);
     var k = parseFloat(kvar); // dwell time in ms
     var gate_ms = parseFloat (k * i); // number of bins * dwell time in  ms


      alert('update_time_bins: dwell_time '+k+'ms * '+i+' bins  = '+gate_ms+'ms');
    
      ODBSet( one_cycle_time_path_ms,gate_ms);
       alert ('update_time_bins:  set cycle time path '+one_cycle_time_path_ms+'to gate_ms= '+gate_ms);
      var gate_sec = parseFloat(gate_ms * 1000);
      ODBSet( one_cycle_time_path_sec,gate_ms);
       alert ('update_time_bins:  set cycle time path '+one_cycle_time_path_ms+'to gate_sec= '+gate_sec);
}


function check_dac_params(DAC_start, DAC_inc, DAC_ninc)
{

   disable_dac_check = get_bool(ODBGet(dac_dis_step_check_path));

   //alert('input params: '+DAC_start+ DAC_inc+ DAC_ninc);
   var s = parseFloat(DAC_start);
   var f = parseFloat(DAC_inc);
   var n = parseInt(DAC_ninc);
   var mini;

   if(f==0)
   {
       alert ('DAC increment cannot be '+f+' Volts');
       return;
   }


   if(disable_dac_check)
       mini = 0;
   else
   {
      var min_DAC_incr = ODBGet(dac_min_step_path);
      mini = parseFloat(min_DAC_incr);
   }

   

   var u=f;
   if (u < 0) u*=-1; // u=abs(f)
   if( u < mini)
   {
       alert ('DAC increment is set too small. Minimum is '+mini+' Volts');
       return;
   }
 
  
   if (s > max_DAC || s < min_DAC)
   {
      alert ('DAC start is out of range (DAC range is '+min_DAC+' to '+min_DAC+' Volts)');
      return;
   }
   var st =0.0;
   st = s + (f * n)
   if (st >  max_DAC || st < min_DAC)
   {
      alert ('Calculated DAC stop '+st+' Volts is out of range (DAC range is  '+min_DAC+' to '+min_DAC+' Volts). \n Reduce \"Number of DAC increments\" or \"DAC increment\"');
   }   
  
   var dac_stop_path = output_path+"DAC Stop";
   var dac_stop = ODBGet(dac_stop_path);
   var fstop = parseFloat(dac_stop);
   // alert('start = '+s+' inc = '+f+' num inc = '+n+' stop = '+st+' dac_stop read from odb  = '+dac_stop);
   if(st != fstop)  // stop value has changed
       {  // alert('Setting path '+path10+' to '+st)
       ODBSet(dac_stop_path, st);
       document.location = document.location; // reload page so Stop value shows new value
   }
   
   return;   
}

function s_msg()
{
  var num_scans;

  if (rstate != state_stopped)
  {
    var my_element = document.getElementById('smsg').innerHTML

    num_scans = ODBGet(nscans_path);

  //  alert('s_msg... my_element= '+my_element +' test is '+pattern_y.test(my_stop));

    if(pattern_y.test(my_stop))
    {
      document.getElementById('smsg').innerHTML='Note: run will stop at the end of current DAC scan'
      document.getElementById('smsg').style.color="red";
    }
    else if (num_scans > 0)
    {
      document.getElementById('smsg').innerHTML='Note: run will stop after '+num_scans+' DAC scans'
      document.getElementById('smsg').style.color="fuchsia";
    }
    else // free-running
      document.getElementById('smsg').innerHTML=''
   }
}


function info()
{
  alert ('Threshold mode: enter 1 for threshold mode 1 (c.f. discriminator level -> removes spikes) or 0 for % value')
  return;
}




function get_run_buttons(rstate)
{

   var txt='';
  
   if (rstate == state_stopped) // stopped
      txt+= '<input name="cmd" value="Start" type="submit">'
   else if (rstate == state_paused) // paused
      txt+='<input name="cmd" value="Resume" type="submit">'
  
   else  // running
   {
       // stop works again now
       // txt+= '<input name="customscript"  style="color:red"  value="Stop now" type="submit">'

      txt+='<input name="cmd" value="Stop" type="submit">' 
      txt+='<input name="cmd" value="Pause" type="submit">'
      if(pattern_n.test(my_stop))
         txt+='<input type="button" value="StopAtEndScan"  style="color:firebrick" onclick="ODBSet(stop_path, 1); location.reload(true);">'
 
   }
   return txt;
}

function make_stopped_info_table()
{
    var path,temp;
    var ivar, fvar;
    var txt='';
  // table has absolute size to fit into the image
   txt+='<table style="text-align: left; width:180 ; height:250; background-color: rgb(255, 255, 255);"  border="2" cellpadding="2" cellspacing="2">'
   txt+='<tbody>';
   txt+='<tr>';
 

   txt+='<th class="title" colspan="2" align="center" >';
   txt+='Information';
   txt+='</tr>';




   // Minimum PPG delay
   txt+='<tr>';
   txt+='<th  class="pcitem" colspan="2" align="left" >';
    
   path=output_path+"Minimal Delay (ms)"
   ivar = ODBGet(path);
   fvar = parseFloat(ivar); //ms
   fvar = fvar * 1000000; // ns

   txt+='<small>Min PPG pulsewidth & delays are actually 3 PPG clock cycles, i.e. '+fvar+' ns</small>'
   txt+='</th></tr>'


    // Alarm
    txt+='<tr>';
    txt+='<th  id=alrm  class="param2"  colspan=2  align="left" >';
    jvar52= get_bool( ODBGet(alarms_active_path));
   
    txt+='Alarm system enabled : ';
    //   txt+='</td>';
    //  txt+='<th  class="small" >';
    //  txt+='<a href="#" onclick="ODBEdit(alarms_active_path)" >'
    // txt+=jvar52;
    // txt+='</a>';
    // txt+='' ;
  txt+= '<input name="acb" type="checkbox"  onClick="ODBSet(alarms_active_path, this.checked?\'1\':\'0\'); location.reload(true)">'
    
    
    txt+='</th></tr>'



  // GalilRio DAC
   txt+='<tr>';
   txt+='<th  class="smhdr2" colspan="2" align="left" >';

   txt+='Galil Rio-47120 16bit ADC/DAC';
   txt+='<br>DAC scan range:'+min_DAC+' to '+max_DAC+'V'
   txt+='<br>ADC ch 0-4 are averaged'
   txt+='</th></tr>'

   txt+='<tr>';
   txt+='<th  class="smhdr2" colspan="2" align="left" >';
  
txt+='<a href="/Alias/galil_rio&amp;?"> ADC/DAC Parameters </a>&nbsp;';
 txt+='</th></tr>'

   // Disable checking
     // txt+='<tr>';
     // txt+='<th  id=dachk  class="param2"  colspan=2  align="left" >';
     // disable_dac_check = get_bool(ODBGet(dac_dis_step_check_path));
   
     // txt+='Disable DAC check  : ';
  
     // txt+= '<input name="dacb" type="checkbox"  onClick="ODBSet(dac_dis_step_check_path, this.checked?\'1\':\'0\'); location.reload(true)">'


     // if(disable_dac_check)
     //  txt+= '<br><span class="info2">No checks on DAC</span>'
     // else
     // txt+= '<br><span class="info">Checks if DAC has stepped</span>'

     // txt+='</th></tr>'
   
    // if(!disable_dac_check)
     //  {
	     
     //Dac minimum increment
     //  txt+='<tr align="left">';
     //  txt+='<td class="param2">';

 
     //  jvar50= ODBGet(path50);
     //  var jtemp=jvar50;
     //   if(jtemp <= 0) jtemp=0.01; // 12 bit range +/- 10V
     //  txt+='DAC minimum increment (V) : ';
     //   txt+='</td>';
     //   txt+='<th  class="small" >';
     //   txt+='<a href="#" onclick="ODBEdit(path50)" >'
     //  txt+=jvar50;
     //   txt+='</a>';
     //   txt+='' ;
     //   txt+='</th>';
     //   var t = parseFloat(jvar50);
    //   if (t <= 0)
     //    {
     //     alert('Illegal Minimum increment');
     //   ODBSet(path50,jtemp); // replace value
     //  javascript:location.reload(true);
     //   }


     //  txt+='<tr align="left">';
     //  txt+='<td class="param2">';

    //Dac jitter
     // jvar51= ODBGet(path51);
     // var jtemp=jvar51;
     // if(jtemp <= 0) jtemp=0.01; // 12 bit range +/- 10V
     //  txt+='DAC jitter (V) : ';
     //  txt+='</td>';
     //  txt+='<th  class="small">';
     //  txt+='<a href="#" onclick="ODBEdit(path51)" >'
     //  txt+=jvar51;
     //  txt+='</a>';
     //   txt+='' ;
     //   txt+='</th>';
     //  var t = parseFloat(jvar51);
    //  if (t <= 0)
    //  {
     //   alert('Illegal jitter');
     //   ODBSet(path51,jtemp); // replace value
 //  javascript:location.reload(true);
     //  }
     //   } // disable_dac_check

  txt+='</tr></table>'
      return txt;
}




function make_stopped_param_table()
{
   var txt='';

  // table has absolute size to fit into the image
   txt+='<table style="text-align: left; width:500 ; height:400; background-color: rgb(255, 255, 255);"  border="2" cellpadding="2" cellspacing="2">'
   txt+='<tbody>';
   txt+='<tr>';
 

   txt+='<th class="title" colspan="2" align="center" >';
   txt+='Input Parameters<br>';
   txt+='</tr>';



//<!-- Number of bins  -->
//    var path1 = sis_path+"LNE preset";
   
    txt+='<tr align="left">';
    txt+='<th class="param">';
  
    jvar1 = ODBGet(path1);
    //alert('path : '+path1+' value: '+jvar1);
    txt+='Number of time bins : ';
    txt+='</td>';
    txt+='<th  class="normal">';
    txt+='<a href="#" onclick="ODBEdit(path1);calculate_all()" >'
    txt+=jvar1;
    txt+='</a>';
    txt+='' ;
    txt+='</th>';
    var t = parseInt(jvar1);
    if (t <= 0)
    {
       jvar1=1;
       alert('At least one time bin must be selected');
       ODBSet(path1,jvar1);
       javascript:location.reload(true);
    }

    // temp
    // dwell time
  
    txt+='<tr align="left">';
    txt+='<th class="param">';
  
    dwell_time_ms = ODBGet(dwell_time_ms_path);
    //alert('path : '+path1+' value: '+jvar1);
    txt+='Dwell Time (ms) : ';
    txt+='</td>';
    txt+='<th  class="normal">';
    txt+='<a href="#" onclick="ODBEdit(dwell_time_ms_path)" >'
    txt+= dwell_time_ms
    txt+='</a>';
    txt+='' ;
    txt+='</th>';
   





    if(0)
	{ // temo
if (junits)
{ 
 <!-- Dwell Time value  -->

   txt+='<tr align="left">';
    txt+='<th class="param">';
    path2 = input_path+"scratch/dwell time";
    jvar2 = ODBGet(path2); 
    //alert ('path2: '+path2+' jvar2= '+jvar2);  
    txt+='Dwell time : <br><span class="info">Displayed in choice of units (ms,us,ns or s) </span>';
    txt+='</th>';
    txt+='<th class="normal">';
    txt+='<a href="#" onclick="ODBEdit(path2); calculate_all()  "; >'
    txt+=jvar2;
    txt+='</a>';
   txt+='<span style="color:white">-</span>' ; // invisible -> space between value and units
   
 //<!-- Dwell Time units  -->
   
    jvar3 = ODBGet(path3);   
    txt+='<a href="#" onclick="ODBEdit(path3);  calculate_all() " >'
    txt+=jvar3;
    txt+='</a>';
    txt+='' ;
    txt+='</th>';

}
else
{
 //<!-- Dwell Time value  -->
 txt+='<tr align="left">';
    txt+='<th class="param">';

      path2  = input_path+"dwell time (ms)";
      jvar2 = ODBGet(path2); 
      //alert ('path2: '+path2+' jvar2= '+jvar2); 
      txt+='Dwell time (ms) : ';
      txt+='</td>';
      txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
      txt+='<a href="#" onclick="ODBEdit(path2); calculate_all()" >'
      txt+=jvar2;
      txt+='</a>';
      txt+='' ;
      txt+='</td>';
 
}
	}


//<!-- PPG Trigger  -->


//<!-- DAC Start  -->
    var jvar4 = ODBGet(path4);
    var jvar5 = ODBGet(path5);
    var jvar6 = ODBGet(path6);



    txt+='<tr align="left">';
    txt+='<th class="param">';

    //alert('path: '+path4+' value: '+jvar4);
    txt+='DAC start (V) : ';
    txt+='</td>';
    txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+='<a href="#" onclick="ODBEdit(path4)" >'
    txt+=jvar4;
    txt+='</a>';
    txt+='' ;
    txt+='</td>';


//<!-- Num DAC Increments  -->

    txt+='<tr align="left">';
    txt+='<th class="param">';

    //alert('path: '+path5+' value: '+jvar5);
    txt+='Number of DAC increments : ';
    txt+='</td>';
    txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+='<a href="#" onclick="ODBEdit(path5)" >'
    txt+=jvar5;
    txt+='</a>';
    txt+='' ;
    txt+='</td>';
    //alert('path: '+path5+' value: '+jvar5);
    t = parseInt(jvar5);
    if (t <= 0)
    {
       jvar5=1;
       alert('Minimum number of DAC increments is 1');
       ODBSet(path5,jvar5);
       javascript:location.reload(true);
    }


// 6 <!-- DAC Increment  -->
    
    txt+='<tr align="left">';
    txt+='<th class="param">';
 
   // alert('path: '+path6+' value: '+jvar6);
    txt+='DAC increment (V) : ';
    txt+='</td>';
    txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+='<a href="#" onclick="ODBEdit(path6)" >'
    txt+=jvar6;
    txt+='</a>';
    txt+='' ;
    txt+='</td>';
    //alert('path: '+path6+' value: '+jvar6);

    check_dac_params(jvar4,jvar6,jvar5);




// 8  <!-- DAC sleep time  -->

   
       txt+='<tr align="left">';
       txt+='<th class="param">';
       var jvar8 = ODBGet(path8);
       //alert('path: '+path8+' value: '+jvar8);
       txt+='DAC sleep time (ms) :';
       txt+='</td>';
       txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
       txt+='<a href="#" onclick="ODBEdit(path8)" >'
       txt+=jvar8;
       txt+='</a>';
       txt+='' ;
       txt+='</td>';
   
  



// 9  <!-- DAC/TOF pulse width  -->

   
       txt+='<tr align="left">';
       txt+='<th class="param">';
       var jvar9 = ODBGet(path9);
       //alert('path: '+path9+' value: '+jvar9);
       txt+='TOF pulse width (ms) :';
       txt+='</td>';
       txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
       txt+='<a href="#" onclick="ODBEdit(path9)" >'
       txt+=jvar9;
       txt+='</a>';
       txt+='' ;
       txt+='</td>';
   
  


// 11  <!-- Num cycles to sum   -->
    txt+='<tr align="left">';
    txt+='<th class="param">';
    var jvar11 = ODBGet(path11);
    //alert('path: '+path11+' value: '+jvar11);
    txt+='Num PPG cycles per SuperCycle :<br> <span class="info">DAC increments after every SuperCycle</span>';
    txt+='</td>';
    txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+='<a href="#" onclick="ODBEdit(path11)" >'
    txt+=jvar11;
    txt+='</a>';
    txt+='' ;
    txt+='</td>';

    t = parseInt(jvar11);
    if (t <= 0)
    {
       jvar11=1;
       alert('Minimum value for number of PPG cycles per DAC increment is 1');
       ODBSet(path11,jvar11);
       javascript:location.reload(true);
    }

// 15  <!-- Number of Scans  -->
    txt+='<tr align="left">';
    txt+='<th class="param">';
    var jvar15 = ODBGet(nscans_path);
    //alert('path: '+nscans_path+' value: '+jvar15);
    txt+='Num DAC scans before run stops: <br><span class="info">Set to 0 for free-running</span>';
    txt+='</td>';
    txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+='<a href="#" onclick="ODBEdit(nscans_path)" >'
    txt+=jvar15;
    txt+='</a>';
    txt+='' ;
    txt+='</td>';
    t = parseInt(jvar15);
    if (t < 0)
    {
       jvar15=0;
       alert('Illegal number of scans. Must be positive');
       ODBSet(nscans_path,jvar15);
       javascript:location.reload(true);
    }

   
    txt+='<tr align="center">';
    txt+='<th colspan=2 class="title" >'
    txt+='Frontend Flags'
    txt+='</th></tr>'

    txt+='<tr align="left">';
    txt+='<th colspan=2 >'

    // small table  for Discard 1st bin, Discard 1st cycke, PPG ext start   and toggle hot flags

    txt+='<table style="text-align: left; width: 100%; background-color: rgb(255, 255, 255);"  border="1" cellpadding="1" cellspacing="1">'
// 16  <!-- discard first bin  -->
    txt+='<tr align="left">';
    txt+='<th class="param2">'

    jvar16 = ODBGet(path16);
    
    txt+='Discard first bin of every PPG cycle ';
  //  txt+='</td>';
  //  txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
 // replace by checkbox
 //   txt+='<a href="#" onclick="ODBEdit(path16)" >'
 //   txt+=jvar16;
 //    txt+='</a>';

    txt+= '<input name="dfb" type="checkbox"  onClick="ODBSet(path16, this.checked?\'1\':\'0\')">'



    txt+='' ;
    txt+='</td>';


// 28  <!-- discard first cycle  -->

    txt+='<th class="param2" >';
    var jvar28 = ODBGet(path28);
    //alert('path: '+path28+' value: '+jvar28);
    txt+='Discard first PPG cycle of every SuperCycle ';
  //  txt+='</td>';
  //  txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
   // replace by checkbox
   // txt+='<a href="#" onclick="ODBEdit(path28)" >'
   // txt+=jvar28;
   // txt+='</a>';
  
   txt+= '<input name="dfc" type="checkbox"  onClick="ODBSet(path28, this.checked?\'1\':\'0\')">'
    txt+='</td>';
    

// 7  <!-- External Trigger  -->


    txt+='<th class="param2">';
    var jvar7 = ODBGet(path7);
    //alert('path: '+path7+' value: '+jvar7);
    txt+='PPG external start ';
   // txt+='</td>';
   // txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
// replace by checkbox
 //   txt+='<a href="#" onclick="ODBEdit(path7)" >'
 //   txt+=jvar7;
 //   txt+='</a>';
 //   txt+='' ;
 
    txt+= '<input name="exst" type="checkbox"  onClick="ODBSet(path7, this.checked?\'1\':\'0\')">'
    txt+='</th>'; 
    txt+='</tr>';
    txt+='<tr align="left">';
    txt+='<th colspan=3 class="param2"><span class="info"><center> Set the above three parameters TRUE unless in test mode</span></center></th></tr>'
  

// 18  <!-- Hot debug -->
    txt+='<tr align="left">';
    txt+='<th class="param2">';

    var jvar18 = ODBGet(path18);
    //alert('path: '+path18+' value: '+jvar18);
    txt+='Toggle Hot Debug:';
    //  txt+='</td>';
    // txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+= '<input name="hd" type="checkbox"  onClick="ODBSet(path18, this.checked?\'1\':\'0\')">'

    txt+='' ;
    txt+='</td>';

// 19  <!-- Hot timer -->
    //  txt+='<tr align="left">';
    txt+='<th class="param2" colspan=2>';
    var jvar19 = ODBGet(path19);
    //alert('path: '+path19+' value: '+jvar19);
    txt+='Toggle Hot Timer:';
    // txt+='</td>';
    // txt+='<td  style="vertical-align: top; background-color: white; font-weight: normal; ">';
    txt+= '<input name="ht" type="checkbox"  onClick="ODBSet(path19, this.checked?\'1\':\'0\')">'

    txt+='' ;
    txt+='</td>'


  txt+='<tr><th colspan=3 class="param2"><span class="info"><center> Hot flags should be set FALSE unless debugging</span></center></th></tr>'
   txt+='</table>'; // end of small table






    txt+='</table>';

    return txt;

} // end of function  make_stopped_param_table()




function make_running_param_table()
{ 
  var txt='';

  var path40 = output_path+"num bins per cycle";
  var path41 = input_path+"discard first bin";
  var path42 = input_path+"num cycles per supercycle";
  var path43 = input_path+"discard first cycle";
  var j43 = parseInt(get_bool(ODBGet(path43)));
  var j42 = parseInt(ODBGet(path42));
  var j44 = j42+j43;
  var path_LNE_preset = input_path+"sis3820/LNE preset";
  var j45 =  parseInt(ODBGet( path_LNE_preset));
  var path46 =  output_path+"valid bins per sc";
  var j46 = parseInt(ODBGet(path46));
  var j47 = j45-j46;
  var path_sishis = "/equipment/info/variables/";
  var path48 =  input_path+"num bins requested";

 

  // absolute size to fit into the image
 txt+='<table style="text-align: left; width:500 ; height:400; background-color: rgb(255, 255, 255);"  border="1" cellpadding="2" cellspacing="2">';
  txt+='<tbody>';
  txt+='<tr>';
  txt+='<th class="title" colspan="6" align="center" >';
  txt+='Parameters';
  txt+='</tr><tr>';

  txt+='<th class="pctitle" colspan="2" align="center" >';
  txt+='Per PPG Cycle</th>';
  txt+='<th class="sctitle" colspan="4" align="center" >';
  txt+='Per SuperCycle</th>';
 
  txt+='</tr><tr>';
  txt+='<td  class="pchdr"  colspan="1" >Bins</td>';
  txt+='<td  class="pcitem" colspan="1" >'+ODBGet(path40)+'</td>';
  txt+='<td  class="schdr"  colspan="1" >Cycles</td>';
  txt+='<td  class="scitem" colspan="1" >'+j44+'</td>';
  txt+='<td  class="schdr"  colspan="1" >Totbins</td>';
  txt+='<td  class="scitem" colspan="1" >'+j45+'</td>';
  txt+='</tr><tr>';

  txt+='<td  class="pchdr"  colspan="1">Discard</td>';
  txt+='<td  class="pcitem" colspan="1">'+get_bool(ODBGet(path41))+'</td>';
  txt+='<td  class="schdr"  colspan="1">Discard</td>';
  txt+='<td  class="scitem" colspan="1">'+j43+'</td>';
  txt+='<td  class="schdr"  colspan="1">Discard</td>';
  txt+='<td  class="scitem" colspan="1">'+j47+'</td>';
  txt+='</tr><tr>';

  txt+='<td  class="pchdr"  colspan="1"  style="font-weight: bold;" >Bins</td>';
  txt+='<td  class="pcitem" colspan="1"  style="font-weight: bold;" >'+ODBGet(path48)+'</td>';
  txt+='<td  class="schdr"  colspan="1"  style="font-weight: bold;" >Cycles</td>';
  txt+='<td  class="scitem" colspan="1"  style="font-weight: bold;" >'+j42+'</td>';
  txt+='<td  class="schdr"  colspan="1"  style="font-weight: bold;" >TotBins</td>';
  txt+='<td  class="scitem" colspan="1"  style="font-weight: bold;" >'+j46+'</td>';
  txt+='</tr><tr>';

  txt+='<th class="title" colspan="6" align="center" >';

 // Frontend Statistics Table
 txt+='<table style="text-align: left; width: 100%; background-color: rgb(255, 255, 255);"  border="1" cellpadding="2" cellspacing="2">';
 txt+='<tbody>';
 txt+='<tr>';
 txt+='<th class="title" colspan="6" align="center" >';

  txt+='Front End Statistics<br>';
  txt+='</tr><tr>';
  txt+='<td  rowspan=1  colspan="1"  style="background-color: azure; font-weight: bold;" >Cycle</td>';
  txt+='<td  rowspan=1  colspan="1"  style="background-color: azure; font-weight: bold;" >SCycle</td>';

  txt+='<td  colspan="3"  rowspan ="2" style="background-color: azure; font-weight: bold;" >'
  
  // DAC Table
  txt+='<table style="text-align: left; width: 100%; background-color: rgb(255, 255, 255);"  border="1" cellpadding="2" cellspacing="2">';
  txt+='<tbody>';
  txt+='<tr>';
  
  txt+='<td  colspan="5"  style="background-color: azure; font-weight: bold; text-align:center;" >DAC </td>';
  txt+='</tr><tr>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Scans</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Inc</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold; " >Set(V)</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold; " >Rd(V)</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold; " >Av(V)</td>';
  txt+='</tr><tr>';

 

  var path31 =   path_sishis + "CYCL[*]"; 
  var cycl = new Array();
  var len=14; // read the CYCL array 
  cycl = ODBGet(path31);
  if (cycl.length < len)
    len = cycl.length;
  var j;
 
  //   alert('path31='+path31+' cycl= '+cycl)
  // display indices 1,2,4 (NumCycl, NumSuperCycl, Cycle InSC, Scan num)
  // and     indices 7-10 (DAC Incr num, DAC Set Value, DAC Read Dac Av) 
   txt+='<td  colspan="1"  style="color: teal; background-color: white; font-weight: normal;" >'+cycl[4]+'</td>';
  txt+='<td  colspan="1"  style="color: teal; background-color: white; font-weight: normal;" >'+cycl[7]+'</td>';
  var cf = roundup(cycl[8],3); // round up to 3dp
  txt+='<td  colspan="1"  style="color: teal; background-color: white; font-weight: normal;" >'+cf+'</td>';
  cf = roundup(cycl[9],3);
  txt+='<td  colspan="1"  style="color: teal; background-color: white; font-weight: normal;" >'+cf+'</td>';
  var av = roundup(cycl[10],3);
  txt+='<td  colspan="1"  style="color: teal; background-color: white; font-weight: normal;" >'+av+'</td>';
  txt+='</tr></table>'; 
  // end of DAC table

  txt+='<tr>'
  txt+='<td  colspan="1"  style="color: blue; background-color: white; font-weight: normal;" >'+cycl[1]+'</td>';
  txt+='<td  colspan="1"  style="color: purple; background-color: white; font-weight: normal;" >'+cycl[2]+'</td>';
 
  txt+='</tr>';

//===========================================================================================

  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Buffrd</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >LNE/cyc</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >LNEfromSIS</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >LNEpreset</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Nbins</td>';

  
  txt+='</tr><tr>';
  var path30 =   path_sishis + "DBUG[*]"; 
  var b = new Array();
  len=5; // display first 5 words
  b = ODBGet(path30);

  if (b.length < 4)
    len = b.length;
 
    for (j=0 ; j<len ; j++)
      txt+='<td  colspan="1"  style="background-color: white; font-weight: normal;" >'+b[j]+'</td>';


    txt+='</tr>';

  txt+='<tr>';
  txt+='<th class="title" colspan="6" align="center" >';
  txt+='Scaler Sums<br>';
  txt+='</tr><tr>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Channel</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >0</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >1</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >2</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >3</td>';
  txt+='</tr>';

 
  //   <!-- MSUMS  Scaler Sums  -->
    var path17 = path_sishis + "SUMS[*]";

    txt+='<tr align="left">';
    txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" ><small>SumPerSC</small></td>';
    a = ODBGet(path17);
    
    // alert('a = '+a+ 'path17= '+path17);
 
    for (i=0 ; i<a.length ; i++)
      txt+='<td  colspan="1"  style="background-color: white; font-weight: normal;" >'+a[i]+'</td>';

    txt+='</tr>';

 txt+='<tr>';
  txt+='<th class="title" colspan="6" align="center" >';
  txt+='ADC<br>';
  txt+='</tr><tr>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Channel</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >0</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >1</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >2</td>';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >3</td>';
  txt+='</tr>';

  txt+='<tr align="left">';
  txt+='<td  colspan="1"  style="background-color: azure; font-weight: bold;" >Av(V)</td>';
  // av calc above for ADC[0]
  txt+='<td  colspan="1"  style="background-color: white; font-weight: normal;" >'+av+'</td>'
  av = roundup(cycl[11],3);
  txt+='<td  colspan="1"  style="background-color: white; font-weight: normal;" >'+av+'</td>'
  av = roundup(cycl[12],3);
  txt+='<td  colspan="1"  style="background-color: white; font-weight: normal;" >'+av+'</td>'
  av = roundup(cycl[13],3);
  txt+='<td  colspan="1"  style="background-color: white; font-weight: normal;" >'+av+'</td>'
  txt+='</tr></table>';  // Frontend Stats table

   // txt+='<tr><td id="smsg"  style="color: black ; background-color: yellow; text-align: center"></td>' 
   
    txt+='</tr>';
    txt+='</table>'; // end parameters table

    return txt;

} // end of function  make_running_param_table()



//</script>

function get_message()
{
    var pattern=/ERROR/

    msg =ODBGetMsg(1);

    var wordArray = msg.split("]");
    var len=wordArray.length;
   // for (i=0; i<len; i++)
   //    document.write('<br>wordArray['+i+']= '+wordArray[i]);

     if(len > 2)
     {
        msg=wordArray[0]+"]"+wordArray[len-1]
     }
     if(pattern.test(msg))
        msg = msg.fontcolor("red"); // turn red if error
     
     return msg;
}


function roundup(fval, dec_places)
{
   // fval must be a string
   // rounds up to dec_places places of decimal

   var point_pattern = /./;
   var exp="";
   var a,c,dec,e,f,g,int
   var rounded;
   var zero;
   var factor;


   if(typeof(dec_places) != 'undefined' && dec_places != null && dec_places === parseInt(dec_places))
     factor = Math.pow(10,dec_places);
   else
     factor = 1000;

   dec = fval.indexOf(".") // find if there's a decimal point

   if (dec > 0)
   {
       //  alert('found a decimal point in fval '+fval);
       zero =  fval.indexOf(".0") // find if there's a decimal point followed by a zero

      a = fval.indexOf("e")  // find exponent
      if (a > 0)
         exp = fval.substring(a)


     //  alert('fval= '+fval+'a = '+a+' exp= '+exp)
     c = fval.slice(0,a);
     // alert('fval= '+fval+'c= '+c);
      d=dec;
      dec++;
      int = fval.slice(0,dec); // integer portion

      f=dec+5;
      e = fval.slice(d,f)
      // look for zeroes e.g. .023 .002

      // alert('d= '+d+' e= '+e+' zero = '+zero)
        e=e*factor;
     // e=e*100;  // 256 -> 25.6 -> 26 ->  .26
               // 023 -> 2.3  -> 2 -> .2 so we must add a zero -> .02

      g = Math.round(e);
     // alert ('fval= '+fval+ ' e= '+e+' g= '+g+'  int= '+int)

      if(zero > 0 )
          rounded = int + "0"+ g + exp  // add a zero as a spacer
      else
         rounded = int + g +exp

      // alert ('fval= '+fval+ '   rounded= '+rounded)
   }
   else
  {
     // alert ('no change in fval '+fval)
      rounded=fval;
   }
   return rounded;
}
