// run states from midas.h
var state_stopped=1
var state_paused=2
var state_running=3

// patterns
var pattern_y=/y/
var pattern_n=/n/
var pattern_no_key = /DB_NO_KEY/    // for ODBGET
var pattern_true = /true/;
var pattern_false= /false/;

var  SIS_min_dwell_time_ms;
// On reload page, gather all the former ODBSet calls together
var writePaths=new Array(); var writeValues=new Array(); // for async_odbset
var writeIndex=0; // index of writePaths and writeValues arrays

var progressTimerId;
//  <!-- javascript globals  -->
 var debug=0;
 
// json data from ODB
var EqData;    //  /Equipment/pol_acq/
var RunInfoData; //  /Runinfo
var AlarmData;   //  /Alarms/
var InfoData; //  /equipment/info/variables/
var LoggerData; // /logger
var  CustomHidden; // /custom/hidden
// paths
//var rstate_path= "/runinfo/state";
//var tip_path = "/runinfo/transition in progress";
//var tr_path = "/runinfo/requested transition";


var pol_path = "/Equipment/pol_acq/";
var input_path = pol_path+"settings/input/";
var output_path = pol_path+"settings/output/";
var sis_path=input_path+"sis3820/";
var thresh_path= pol_path + "settings/cycle thresholds/";
var polarity_path=input_path+"polarity"
var trig_path=input_path+"external trigger"
var watchdog_path = output_path + "watchdog"
var alarms_active_path = "/Alarms/Alarm system active";

var femsg_path=output_path+ "frontend msg";
var dac_min_step_path=input_path+"galil_rio/dac step check/minimum step (V)"
var dac_dis_step_check_path=input_path+"galil_rio/disable step check"

var dwell_time_ms_path    = input_path+"dwell time (ms)";
var dwell_time_ms;
var hide_status;

// globals added in get_global_data
var num_bins_req,dac_start,dac_ninc,dac_inc,num_cy_sc,discard_bin1,discard_cycle1,ppg_ext_start,dac_sleep_time,tof_pw
    var hdb,htm,scr_dt,scr_units,num_mcs_time_bins, min_DAC, max_DAC, scaler_format, min_DAC_incr,disable_dac_check,alarms_enable
    var dac_stop,num_scans,min_delay,lne_preset,valid_bins_sc,stop_eos, write_data, gbl_debug, num_dp
    var cycl = new Array(); // for  path31 = /equipment/info/variables/  + "CYCL[*]"; 
var dbug  = new Array(); // for  path30 = /equipment/info/variables/  + "DBUG[*]"; 
var sums =  new Array(); // for  path17 = /equipment/info/variables/  + "SUMS[*]"; 
// these paths need to be global for editing:
var path1 = input_path+"num bins requested";
var path2 = input_path+"scratch/dwell time";
var path3 = input_path+"scratch/dwell time units";
var path4 = input_path+"DAC Start";
var path5 = input_path+"Num DAC Incr";
var path6 = input_path+"DAC Inc";
var path11 = input_path+"num cycles per supercycle";
var path16 = input_path+"Discard first bin";
var path28 = input_path+"Discard first cycle";
var path7 = input_path+"PPG external start";
var path8 = input_path+"DAC sleep time (ms)";
var path9 = input_path+"TOF pulse width (ms)";
var path18 = input_path+"hot debug";
var path19 = input_path+"hot timer";

var path_hidden= "/Custom/Hidden/"
var path_ndp = path_hidden + "decimal places";
var disable_dac_check;
//var jvar16,jvar1; // replaced by discard_bin1, num_bins_req

var junits=1;  // set to zero if unit change is not working
var path,val;
var ival,jvar;

var stop_path=input_path + 'stop at end-of-sweep'
var nscans_path = input_path+"Number of scans";

var jvar50,jvar51,jvar52,jvar53;
var path50= input_path+"galil_rio/dac step check/minimum step (V)";
var path51= input_path+"galil_rio/dac step check/jitter (V)";

var min_DAC_path = output_path + "DAC_min"  
var max_DAC_path = output_path + "DAC_max"

var min_DAC;// get_global       ODBGet(min_DAC_path); // values filled by frontend according to range of DAC 
var max_DAC;  // get_global        ODBGet(max_DAC_path);

var progress_init=0
var progress_callback=1 // waiting for callback  read_data, enable_autorun, async_odbset, try_write_last_message
var progress_got_callback=11 // read_data, enable_autorun, async_odbset
var progress_getdata=2  // assign_data
var progress_gotdata=12 // done
var progress_load_all=5
var progress_load_all_done=15
var progress_load_partial=4
var progress_load_partial_done=14
var progress_update=6;  // function update()
var progress_update_done=16;
var progress_write_callback=7; // waiting for callback from db_paste
var progress_write_got_callback=17; // got  callback from db_paste
var progress_msg=3; // write message callback 
var progress_msg_done=13; // write msg callback done
var progressFlag=progress_init;

var init_flag=0;
var fstop;

function check_alarms()
{ 
   document.getElementById("alarm1").innerHTML=""; // clear alarm banner
   document.getElementById("alarm2").innerHTML=""; // clear alarm banner
   document.getElementById("alarm3").innerHTML=""; // clear alarm banner
   if(!alarms_enable)   //    if( get_bool(ODBGet(alarms_active_path)) == 0)
       return;

   txt="";
   // alert('alarm3='+parseInt(AlarmData.alarms.logger.triggered)+' alarm1='+parseInt(AlarmData.alarms.pol_fevme.triggered)+' alarm2='+parseInt(AlarmData.alarms.fe_init.triggered))
   if(parseInt(AlarmData.alarms.logger.triggered) >= 1)  // ( ODBGet(alarm_path3) >= 1)   alarm_path3="/Alarms/alarms/logger/triggered"
     {
       txt='Program Logger is not running '
       txt+=' <input type="button" value="clear alarm" onclick="clear_alarm(3)">'
       document.getElementById("alarm3").innerHTML=txt;
       document.getElementById('alarm3').style.backgroundColor="blue";
     }

   if(gbl_debug) document.getElementById('gdebug').innerHTML="<br>check_alarms: parseInt(AlarmData.alarms.pol_fevme.triggered)="+parseInt(AlarmData.alarms.pol_fevme.triggered)
     if(parseInt(AlarmData.alarms.pol_fevme.triggered) >= 1)  // ( ODBGet(alarm_path1) >= 1)   alarm_path1="/Alarms/alarms/pol_fevme/triggered"
     {
       txt='Program pol_fevme is not running   '
       txt+=  '  <input type="button" value="clear alarm" onclick="clear_alarm(1)">'
       document.getElementById("alarm1").innerHTML=txt;
       document.getElementById('alarm1').style.backgroundColor="red";
     }    
     else if(parseInt(AlarmData.alarms.fe_init.triggered)  >= 1)  // if( ODBGet(alarm_path2) >= 1)   alarm_path2="/Alarms/alarms/fe_init/triggered"
     { // pol_fevme must be running for this alarm to be valid
        txt='Program pol_fevme cannot run due to '
        txt+= EqData.settings.output["frontend msg"];  // txt+=ODBGet(femsg_path)+'</font></td>'   femsg_path = output_path + "frontend msg";
        txt+=  '&nbsp;&nbsp;<input type="button" value="clear alarm" onclick="clear_alarm(2)">'
   
     document.getElementById("alarm2").innerHTML = txt;
     document.getElementById('alarm2').style.backgroundColor="orange";
     }
   
}

function clear_alarm(j)
{
var alarm_paths=[
		 "/Alarms/alarms/pol_fevme/triggered",
                 "/Alarms/alarms/fe_init/triggered",
                 "/Alarms/alarms/logger/triggered",
  ]
    
    var index=j-1; // j from 1-3

//  alert('alarm_paths.length='+alarm_paths.length+' j='+j+' index='+index)
   if(  index < alarm_paths.length   )
   {
       // alert('calling cs_odbset to clear alarm with path '+alarm_paths[index])
      cs_odbset(alarm_paths[index],0);  // clear alarm
   }

    switch(j)
    {
    case 1:
     document.getElementById("alarm1").innerHTML = "";
    break;

    case 2:
      document.getElementById("alarm2").innerHTML = "";
    break;

    case 3:
      document.getElementById("alarm3").innerHTML = "";
    break;
    
    default:
        alert('clear_alarm:  Unknown alarm ('+j+')')
	    }
 return;
}
function enable_alarm_system(val)
{
     val=parseInt(val);
     // alert('enable alarm system called with val='+val)
     
     cs_odbset(alarms_active_path,val);  // enable/disable alarm system
   
    document.form2.acb.checked=val;
    document.getElementById('alrm').style.backgroundColor=colArray[val];
    if(!val)
    {
        document.getElementById("alarm1").innerHTML = "";
        document.getElementById("alarm2").innerHTML = "";
        document.getElementById("alarm3").innerHTML = "";
    }
    return;
}



function hot_debug(val)
{
      val=parseInt(val)
	  //   	alert('hot_debug val='+val);
      cs_odbset(path18,val);
     document.form2.hdbg.checked=val;
     document.getElementById('hdbgc').style.backgroundColor =  colArray3[val]
	 //	 alert('colArray3['+val+']=  '+colArray3[val])
}

function hot_timer(val)
{
      val=parseInt(val)
	  //   	alert('hot_timer val='+val);
      cs_odbset(path19,val); // hot timer path19
     document.form2.htim.checked=val;
     document.getElementById('htimc').style.backgroundColor =  colArray3[val]
	 //	 alert('colArray3['+val+']=  '+colArray3[val])
}


function check_watchdog()
{
  // watchdog to check if pol_fevme is running
   //  new midas - run will not start without required frontends... however users may not figure out why
   //  also frontend could die
   //watchdog = ODBGet(watchdog_path); // watchdog is a global got in  get_global_data()
   document.getElementById("wddly").innerHTML=wdly;
   if(watchdog != last_watchdog)
   {
       wdly=0;
      last_watchdog= watchdog;
       document.form2.festatus.value=1; // frontend is running
       document.getElementById("wd").innerHTML =' watchdog: '+watchdog;
       document.getElementById("wd").style.backgroundColor ='lime';
       document.getElementById("wddly").style.backgroundColor ='lime';
     
   }
   else
  {
      wdly++;
      document.getElementById("wddly").style.backgroundColor ='pink';  // watchdog delay counter
  }

   if(wdly == 3) // give watchdog time to change before turning red
   {             // only turn red once if program not running
        document.getElementById("wd").innerHTML =' watchdog: '+watchdog;
	document.getElementById('wd').style.backgroundColor="red";
        document.form2.festatus.value=0; // not running
   }	

// disable watchdog
//document.getElementById('wd').innerHTML=""
}


function Main()
{
    // alert('main...')
    
  
      var my_date= new Date;
      var s=my_date.toString();
      s = s.replace(/ GMT.+/,""); // strip off time zone info

     document.getElementById('reload').innerHTML="Page last fully reloaded: "+s

      hide_status=1; 
     document.form2.rws.checked=hide_status;  // default

      gbl_debug=0; // set default to be TRUE to get debug statement below

      if(gbl_debug)
         document.getElementById('gdebug').innerHTML="<br>Main..full reload at " + s

     writeIndex=0; // clear write array
     writeValues.length=0;
     writePaths.length=0;
      read_data(); // read the data
      progress();
}

function update()
 {  // page update every 5s or 10s (default)
    // alert('update: starting')
    // Countdown until full reload
   
    countdown--;
    progressFlag = progress_update;  
    document.getElementById('cntdn').innerHTML=countdown;
    clearTimeout(updateTimerId);
    // if(gbl_debug)document.getElementById('gdebug').innerHTML+="<br><b>Function update()</b> with countdown= "+countdown

      var my_date= new Date;
      var s=my_date.toString();
      s = s.replace(/ GMT.+/,""); // strip off time zone info

      //  document.getElementById('cntdn').innerHTML = "Last partial page update " + s;

      if(document.form2.rws.checked)
      {  // hide status
          document.getElementById('writeStatus').innerHTML=""
          document.getElementById('readStatus').innerHTML=""
          document.getElementById('readStatus').style.Color="black";
          document.getElementById('writeStatus').style.Color="black";
      }
       read_data(); // read the data 
    if (updatePeriod > 0)  // short update period by default
       updateTimerId = setTimeout('update()', updatePeriod);
  progressFlag = progress_update_done;  
 }

function calculate_update_timer()
{
    
   var my_class
   var text="";
  
   my_class="buttons"
   var update_time_s = updatePeriod/1000;  // in seconds 
 
   
    // full page update is every 60s (fixed)
    countdown = parseInt(60 / update_time_s);  // both in seconds
    updateTimerId = setTimeout('update()', updatePeriod);
    document.getElementById('cntdn').innerHTML=countdown

}

function write_run_duration()
{
   var rstart =  RunInfoData["start time"];
   var rstop =   RunInfoData["stop time"];

   document.getElementById('rstart').innerHTML=rstart;
   if (rstate == state_stopped) // not running - display stop time
    {
       document.getElementById('rstop').innerHTML="Stop Time";
       document.getElementById('rtime').innerHTML=rstop
    }
    else
    { // running
       var message=get_elapsed_time();
     
       document.getElementById('rstop').innerHTML="Elapsed Time";
       document.getElementById('rtime').innerHTML=message 
    }
 
    
}
function fill_run_state()
{
 if(rstate == state_running)
  {
     document.getElementById('rs').innerHTML = "Running";
     document.getElementById('rs').style.backgroundColor="lime";
  }
  else if (rstate == state_stopped)
  {
     document.getElementById('rs').innerHTML = "Stopped";
     document.getElementById('rs').style.backgroundColor="red";
  }
  else if (rstate == state_paused)
  {
     document.getElementById('rs').innerHTML = "Paused";
     document.getElementById('rs').style.backgroundColor="yellow";
  }
}
function check_transition()
{
 // look for transition in progress
 
   if(tip == 0) 
       {
       document.getElementById('tip').innerHTML="";
       document.getElementById('tip').style.backgroundColor="white";
       }
  else
  {
      document.getElementById('tip').innerHTML=remember_tip;
     document.getElementById('tip').style.backgroundColor="yellow";
  }
  if(tr == 0)
  {
     document.getElementById('tr').innerHTML ="";
     document.getElementById('tr').style.backgroundColor="white";
  }
  else
  {
     document.getElementById('tr').innerHTML=remember_tr;
     document.getElementById('tr').style.backgroundColor="orange";
  }
}

function Load()
{
   var txt;
   var msg;
   var update_time;
   var rstate_changed;
   var status;

   //cntr++;
    var my_date= new Date;
    var s=my_date.toString();
    s = s.replace(/ GMT.+/,""); // strip off time zone info
    if(gbl_debug)
           document.getElementById('gdebug').innerHTML+="<br>load starting  at " + s
	
	// alert('load')
   // Always (every 5s)
   check_watchdog();
   document.getElementById('alrm').style.backgroundColor=colArray[alarms_enable];
   check_alarms();
   write_last_message();
   s_msg();  // write/clear frontend message
   document.form2.acb.checked=alarms_enable; // get_bool(ODBGet(alarms_active_path));

 
   // On full page reload
   if(!init_flag)
   {
       progressFlag= progress_load_all
        if(gbl_debug) 
            document.getElementById('gdebug').innerHTML+="<br>load: init_flag is true"

      if(num_dp <= 0) // check number decimal places for display
       {
  	   num_dp=3;  // default
           writeValues[writeIndex]=num_dp; // global
           writePaths[writeIndex]=path_ndp;
           writeIndex++;
       }
       document.form2.dacdp.value = num_dp; // needed for check_dac_params, make_param_table, make_running_param_table
  if(gbl_debug) 
       document.getElementById('gdebug').innerHTML+="<br>number of  decimal places num_dp="+  num_dp

       document.form2.pdbg.checked=gbl_debug;
    
       check_transition();
       fill_run_state();
       load_info_table(); // only on full reload
       calculate_all();
       check_dac_params(dac_start,dac_inc,dac_ninc);
       make_param_table(); 
       write_run_duration();

       fill_fe_flags_table();
       fill_hot_debug_table();
       fill_bins_parameters_table();
      
       make_running_param_table();
       txt= get_run_buttons(rstate);
       document.getElementById("rb").innerHTML=txt + remember_run_buttons;
       document.getElementById('rn').innerHTML = RunInfoData["run number"];
       document.getElementById('dd2').innerHTML=LoggerData["data dir"]
       document.getElementById('dl').style.backgroundColor=colArray[write_data];


       if(rstate == state_stopped)
       {  // change running_param_table colour to greyish
	  changeClassBGColor("fenormal","silver" ); // change contents to silver
	  changeClassBGColor("smhdr","#f2eef6" );  // change small header to greyish
          changeClassBGColor("title2","#e0e0eb" );  // change titles back to greyish
       }
       else if (rstate == state_running)
       {     // change input param table colour to greyish
 	     // changeClassBGColor("param","ffffe6" );
	   changeClassBGColor("param","silver" );   // change contents to silver
              changeClassBGColor("title1","#e0e0eb" );  // change titles to greyish
       }

       init_flag=1;
       //  alert('writePaths='+writePaths +' writeValues='+writeValues+' and writeIndex= '+writeIndex)
       progressFlag= progress_load_all_done

    
       if(writeIndex > 0)  async_odbset(writePaths, writeValues)
       return;
   }

   // partial reload
   progressFlag=progress_load_partial;
   if( (tip != last_tip) || (tr != last_tr))
	  check_transition();

     if(gbl_debug)
     {
        document.getElementById('gdebug').innerHTML+="<br>load: init_flag is false"
        if(rstate==state_stopped)
           document.getElementById('gdebug').innerHTML+="<br>load: Stopped"
        else if(rstate==state_running)
           document.getElementById('gdebug').innerHTML+="<br>load: Running"
        else
           document.getElementById('gdebug').innerHTML+="<br>load: run_state="+rstate
     }

// run state
   if(rstate != last_rstate)
    {
       last_rstate = rstate;
       rstate_changed=1; 
     if(gbl_debug)
            document.getElementById('gdebug').innerHTML+=" and  run state HAS changed "
    } 
   else
       {
       rstate_changed=0;
      if(gbl_debug)
          document.getElementById('gdebug').innerHTML+=" and  run state has NOT changed "
     }


   // check whether dacdp has changed (number of decimal places)
   if( document.form2.dacdp.value <= 0 )
       document.form2.dacdp.value= num_dp; // use previous value
      
   else if(num_dp !=  document.form2.dacdp.value)
       { 
          num_dp =  document.form2.dacdp.value;
         document.getElementById('calds').innerHTML=roundup(fstop, num_dp);
         if(rstate==state_running)
              document.getElementById('dacinc').innerHTML=roundup(dac_inc, num_dp);
         cs_odbset(path_ndp, num_dp); // store new value in odb
      }


   if(rstate_changed)
   {
     fill_run_state();
     make_param_table(); // input parameter table
     fill_fe_flags_table();
     make_running_param_table();
     txt= get_run_buttons(rstate); // if rstate has changed
     document.getElementById("rb").innerHTML=txt + remember_run_buttons;
     write_run_duration();
     // rstate has changed; change colours back to default
     if(rstate == state_stopped) 
	 {   // Input Params table
	 changeClassBGColor("param","yellow" ); // change the run parameters back to yellow
         changeClassBGColor("title1","#93aed2" ); // change titles back to close to steel blue
             
       // rstate has changed to stop;  change running table colour to grey
          changeClassBGColor("fenormal","silver" );
	  changeClassBGColor("smhdr","#f2eef6" ); // change small header to greyish
          changeClassBGColor("title2","#e0e0eb" );  // change titles back to greyish
         }
     else if(rstate == state_running)
	 {   // rstate has changed to running;
 	   changeClassBGColor("param","silver" ); // change the run parameters to grey
           changeClassBGColor("title1","#e0e0eb" ); // change titles to greyish
 
	   // change colours to defaults on running table
          changeClassBGColor("fenormal","white" );
	  changeClassBGColor("smhdr","azure" );
          changeClassBGColor("title2","#a5bcd9" ); // // change titles back to close to steel blue
        }
       
   } // end of rstate has changed
   else if (rstate == state_running) 
   {
       make_running_param_table(); // update parameter table
       write_run_duration();
   }

   else if (rstate == state_stopped)
       { // reload table every 10s when run is stopped. Otherwise it is reloaded if edited.
      if(countdown%2 ==0)
      {
	  // alert('countdown%2 is zero')
	  document.getElementById('cntdn').style.color="blue";
        make_param_table();
        fill_fe_flags_table();
        fill_hot_debug_table();
        document.getElementById('dl').style.backgroundColor=colArray[write_data];
      }
      else document.getElementById('cntdn').style.color="black";
   }

      
  progressFlag=progress_load_partial_done;
}


function Init()
{
 //  window.setInterval("Check()",2000);  // timer
    // alert('init')
    
    if(gbl_debug)
             document.getElementById('gdebug').innerHTML+="Init: starting"
    
     
    calculate_update_timer();
 
   remember_run_buttons = document.getElementById('rb').innerHTML;
   
   remember_last_message = document.getElementById('lastmsg').innerHTML;
 
   remember_smsg =  document.getElementById('smsg').innerHTML;
  
   remember_tip =  document.getElementById('tip').innerHTML;
   remember_tr =  document.getElementById('tr').innerHTML;
   remember_hot_db =   document.getElementById('hdbg').innerHTML;
   remember_hot_tim =   document.getElementById('htim').innerHTML;
     
   remember_fef = document.getElementById('fef').innerHTML;

   remember_readstatus = document.getElementById('writeStatus').innerHTML
   remember_writestatus = document.getElementById('readStatus').innerHTML

   //remember_alarm =  document.getElementById('alarm1').innerHTML;

   // timer = window.setInterval("Load()",timerArray[rstate]);  // timer
   last_rstate = rstate;
   last_tip = tip;
   last_tr = tr;
  last_watchdog = watchdog;
   wdly=0;

   
   // dwell_time_ms = ODBGet(dwell_time_ms_path);  // global

  // alert('dwell time= '+dwell_time_ms)
  // dwell_time_convert();
  
 //  var dt =  ODBGet(dwell_time_ms_path);
 //  alert('dt='+dt+' dwell_time_ms= '+dwell_time_ms);
 //  if(dt != dwell_time_ms)
  //    alert('Changed');
       
}


function read_data()
{
  var paths=[ "/Equipment/pol_acq/",
              "/Runinfo",
              "/Alarms/",
              "/Equipment/info/variables/",
              "/Logger",
              "/Custom/hidden/",
           ];
   var my_date= new Date;
    var s=my_date.toString();
    
     // alert(pattern_date.test(s))
     s = s.replace(/ GMT.+/,""); // strip off time zone info
    
    document.getElementById('readdata').innerHTML = "Last update " + s
    progressFlag=progress_callback  // load
    if(!document.form2.rws.checked)
	    { 
          document.getElementById('readStatus').innerHTML ='read_data: getting new data'
          document.getElementById('writeStatus').innerHTML = 'read_data: getting new data'
	    }
 
     mjsonrpc_db_get_values(paths).then(function(rpc) {
     if(!document.form2.rws.checked)
	{
           document.getElementById('readStatus').innerHTML = 'read_data: status='+rpc.result.status
           document.getElementById('writeStatus').innerHTML = 'read_data:processing data'
	}
        progressFlag = progress_got_callback
        var i;
        var len=rpc.result.status.length
        for ( i=0; i<len; i++)
        {  // check individual status
           if(rpc.result.status[i] != 1) 
              document.getElementById('readStatus').innerHTML +='<br>read_data: status error at index ='+i+' path='+paths[i]
              document.getElementById('readStatus').style.Color="red";
        }

      status_key=rpc.result.status[len-1];
      progressFlag=  progress_getdata
	// assign_data(rpc); // success
	EqData=rpc.result.data[0]
        RunInfoData = rpc.result.data[1];
        AlarmData= rpc.result.data[2];
        InfoData= rpc.result.data[3];
        LoggerData= rpc.result.data[4];
        CustomHidden = rpc.result.data[5];
        get_global_data();
      
      progressFlag=  progress_gotdata
      if(!init_flag) // initialize
	  Init();

      var my_date= new Date;
      var s=my_date.toString();
      s = s.replace(/ GMT.+/,""); // strip off time zone info
      if(gbl_debug)
           document.getElementById('gdebug').innerHTML+="<br>read_data calling Load()  at " + s
 	Load();

      }).catch(function(error) {
          document.getElementById('readStatus').innerHTML='callback error from read_data()'
          document.getElementById('readStatus').style.Color="red";
        mjsonrpc_error_alert(error);
       })

}


function get_global_data()
{

    // Fill some globals used elsewhere
    rstate=RunInfoData.state;
    last_tip=tip;
    tip=RunInfoData["transition in progress"];
    last_tr=tr;
    tr = RunInfoData["requested transition"];   // tr_path
    stop_eos =  get_bool(EqData.settings.input["stop at end-of-sweep"]);
    alarms_enable=get_bool(AlarmData["alarm system active"]); // ODBGet(alarms_active_path) jvar52

    dwell_time_ms =  EqData.settings.input["dwell time (ms)"]; //ODBGet(dwell_time_ms_path);
    num_scans =   EqData.settings.input["number of scans"]; //  nscans_path jvar15
    write_data =  get_bool(LoggerData["write data"])
    // odbedit values
    num_bins_req = EqData.settings.input["num bins requested"]; // path1
    dac_start =   EqData.settings.input["dac start"]; // path4
    dac_ninc =   EqData.settings.input["num dac incr"]; // path5
    dac_inc =   EqData.settings.input["dac inc"]; // path6
    num_cy_sc =  parseInt(EqData.settings.input["num cycles per supercycle"]); // path11
    discard_bin1 =  get_bool(EqData.settings.input["discard first bin"]); // path16
    discard_cycle1 =  get_bool(EqData.settings.input["discard first cycle"]); // path28
    ppg_ext_start =  get_bool(EqData.settings.input["ppg external start"]); // path7
    dac_sleep_time = EqData.settings.input["dac sleep time (ms)"]; // path8
    tof_pw =  EqData.settings.input["tof pulse width (ms)"]; // path9
    hdb =  get_bool(EqData.settings.input["hot debug"]); // path18
    htm =  get_bool(EqData.settings.input["hot timer"]); // path19
    // scratch only
    scr_dt = EqData.settings.input.scratch["dwell time"]; // path2
    scr_units = EqData.settings.input.scratch["dwell time units"];  // path3
    // scaler
    scaler_format = parseInt(EqData.settings.input.sis3820["data format"]);
    lne_preset =   parseInt(EqData.settings.input.sis3820["lne preset"]);

    // galil
    min_DAC_incr =   get_bool( EqData.settings.input.galil_rio["dac step check"]["minimum step (V)"]);
    disable_dac_check = get_bool( EqData.settings.input.galil_rio["disable step check"]);  //ODBGet(dac_dis_step_check_path));

    // output
    watchdog=EqData.settings.output.watchdog;   //  watchdog_path = output_path + "watchdog"
    num_mcs_time_bins = EqData.settings.output["num bins per cycle"];
    min_DAC =  EqData.settings.output["dac_min"];
    max_DAC =  EqData.settings.output["dac_max"];
    dac_stop =  EqData.settings.output["dac stop"]; // ODBGet(dac_stop_path);
    min_delay = parseFloat( EqData.settings.output["minimal delay (ms)"]); 
    valid_bins_sc =   parseInt( EqData.settings.output["valid bins per sc"]);

    // /equipment/info/variables/
    var len=14; // read the CYCL array 
    cycl = InfoData.cycl;  // array
  
    dbug = InfoData.dbug; //  DBUG array
    sums =  InfoData.sums; // SUMS array 

    //  gbl_debug=get_bool(CustomHidden[custom page debug]);
    gbl_debug=get_bool(CustomHidden["custom page debug"])
	num_dp = parseInt(CustomHidden["decimal places"]);
    // alert(' gbl_debug='+gbl_debug+' dp='+num_dp)

    // temp
    //  var numb = parseFloat(cycl[9]);
    // var numb2 = numb.toFixed(2);
    // alert('numb='+numb+'numb2='+numb2)

   
}

function unbool(jval)
{
    if(jval)
        return ("y");
    else
        return ("n");
}

function get_bool(jval,path)
{
  var ival=0;


 if(pattern_no_key.test(jval))
 {
     alert('get_bool: no key for path '+path+'  jval= '+jval);
     ival=-1
 }
  else
  {
    if( (pattern_y.test(jval)) ||  (pattern_true.test(jval)))
      ival=1;
    else
      ival=0;


   //  alert('get_bool:  jval= '+jval+ ' returning ival= '+ival)
  }
  return ival;
}

// Open the window for the refresh time
function openWindow()
{
secondWin = open ('TRef!','config','height=250,width=600,scrollbars=no');                
}

// convert dec to hex for output
function d2h(d) {return d.toString(16);}


function calc_min_scaler_dt()
{
    var SIS_min_dt = [  0.000224, 0.000234, 0.000244 ];
    var index = parseInt ((scaler_format/8) -1) ;
    if(index > 2) index=2;
    if(index < 0) index=0;

    SIS_min_dwell_time_ms =parseFloat( SIS_min_dt[index]);
    document.getElementById("minsc").innerHTML='Minimal Scaler dwell time '+ SIS_min_dwell_time_ms+'ms for Scaler ('+scaler_format+' bit format)'
    return;
}

   

function calc_scratch_dwell_time()
{  // fill "scratch" using "dwell time (ms)" and "scratch/units"

   // NOT USED when scratch area is shown on the web page

   var path_units = input_path+"scratch/dwell time units";
   var path_value = input_path+"scratch/dwell time";
   var path_format = input_path+"sis3820/data format";

   var Paths=new Array(); var Values=new Array(); // for async_odbset
   var index=0; // index of Paths and Values arrays

  
   // scr_units  get_global_data   u = ODBGet(path_units);

    if(scr_units == "ms")
       factor = 1;
    else if (scr_units == "ns") 
       factor = 1000000; // ms to ns
    else if (scr_units == "us")
       factor = 1000;    // ms to us
    else if  (scr_units == "s")  
       factor = 0.001;      // s to ms
    else
    {  // set units to ms   (scratch units not assigned)
       scr_units="ms";
       factor =1;
       // ODBSet(path_units, scr_units);
       Paths[index]=path_units;
       Values[index]=scr_units;
       index++;
       //alert ('setting path '+path_units+' to '+scr_units);
    }

    // get_global_data    dwell_time_ms = ODBGet(dwell_time_ms_path);
    // scaler_format  var numbits = ODBGet(path_format);
    var v_ms = parseFloat(dwell_time_ms);
    if( scaler_format == 8)
       SIS_min_dwell_time_ms = 0.000224;
    else if ( scaler_format == 16)
       SIS_min_dwell_time_ms = 0.000234;
    else
       SIS_min_dwell_time_ms = 0.000244;
     
    if (v_ms < SIS_min_dwell_time_ms)
    {

       //document.write('Minimum dwell time is ' +SIS_min_dwell_time_ms+ ' ms for '+ scaler_format+'-bit format data.'<br>'); 
       alert('Minimum dwell time is ' +SIS_min_dwell_time_ms+ ' ms for '+ scaler_format+'-bit format data.'); 
       v_ms =  SIS_min_dwell_time_ms;

       //ODBSet(dwell_time_ms_path, v_ms);
       Paths[index]=dwell_time_ms_path;
       Values[index]=v_ms;
       index++;
    }
    var v = v_ms * factor;
    //alert('calc_scratch_dwell_time:  dwell_time_ms_path = '+ dwell_time_ms_path+' dwell_time_ms= '+dwell_time_ms+' v_ms= '+v_ms);
    //alert('calc_scratch_dwell_time: v_ms = '+v_ms+' wrote v = '+v+' units='+u+' to scratch')
    //ODBSet(path_value, v); // fill scratch dwell time value 
    Paths[index]=path_value;
    Values[index]=v;
       index++;

    async_odbset(Paths,Values)
    return;
}

function init_scratch_dwell_time()
{   // if no information in scratch area
    var path_units = input_path+"scratch/dwell time units";
    var path_value = input_path+"scratch/dwell time";

   
    //  alert('scr_units='+scr_units+' and scr_dt = '+scr_dt);

    // check for illegal values
    if((scr_units != "ms") && (scr_units != "ns") &&  (scr_units != "us") && (scr_units != "s") )
    {
         scr_units="ms";
     
         writePaths[writeIndex]=path_units;
         writeValues[writeIndex]=scr_units;
         writeIndex++;
    }

    if(scr_dt==0)
    {
  	scr_dt=dwell_time_ms;

         writePaths[writeIndex]=path_value;
         writeValues[writeIndex]=dwell_time_ms;
         writeIndex++;
    }
 
    return 
}

function dwell_time_convert()
{    // scrdt
   // converts value in  "scratch/dwell time" according to "scratch/units" to  input/dwell_time_ms
   // called when input values clicked
   var ivar,u;
   var v_ms,v,factor;
  
    var path_units = input_path+"scratch/dwell time units";
    var path_value = input_path+"scratch/dwell time";

 
   // alert ('dwell time convert');

    //  if(gbl_debug)  document.getElementById('gdebug').innerHTML +='<br>dwell time convert  units='+scr_units
  
   // scr_units    u = ODBGet(path_units);
    //   alert ('input path: '+input_path+'units '+scr_units);
  
    factor =1;

    if(scr_units == "ms")
       factor = 1;
    else if (scr_units == "ns") 
       factor = 1000000; // ms to ns
    else if (scr_units == "us")
       factor = 1000;    // ms to us
    else if  (scr_units == "s")  
       factor = 0.001;      // s to ms
    else
    {  // set units to ms
      scr_units ="ms";
       factor =1;
       // ODBSet(path_units,scr_units );
       writePaths[writeIndex]=path;
       writeValues[writeIndex]=factor;
       writeIndex++;
       //  alert ('setting path '+path_units+' to '+scr_units);
    }

 
    //scr_dt   ivar = ODBGet(path_value);
    //   alert ('dwell_time_convert: value scr_dt= '+scr_dt);
    v = parseFloat(scr_dt);
    v_ms = parseFloat( v / factor);
    // alert('scr_dt,v,v_ms='+scr_dt+' '+v+' '+v_ms)
  
    if(v_ms <  SIS_min_dwell_time_ms )
	{
	    v_ms =   SIS_min_dwell_time_ms;
	    scr_dt =v_ms * factor;
	    // alert('scr_dt =v_ms * factor  '+scr_dt+' = '+v_ms+' * '+factor)
            alert('Setting Dwell Time to Scaler minimal dwell time ('+SIS_min_dwell_time_ms+')ms ')
        
		    writePaths[writeIndex]= path_value;
	            writeValues[writeIndex]=scr_dt;
	       	    writeIndex++;
	}

    //   alert ('dwell_time_convert: setting '+ dwell_time_ms_path+' to '+v_ms+' ms (factor = '+factor+')');
 
      // ODBSet(dwell_time_ms_path, v_ms); // fill ms value
    writePaths[writeIndex]=dwell_time_ms_path;
    writeValues[writeIndex]=v_ms;
    writeIndex++;

    //  alert ('setting dwell_time_ms_path: '+ dwell_time_ms_path+' v_ms= '+v_ms); 
    dwell_time_ms = v_ms;  // new value
 
    return;
}
  




function calculate_all()
{
    var status;
    // alert('calculate_all: starting')

    // Called from Load() at full load, i.e. after a value has been changed (OdbEdit reloads page)
    calc_min_scaler_dt(); // fills  SIS_min_dwell_time_ms
    init_scratch_dwell_time(); // only if scratch time/units are not supplied, set default 
                                  // otherwise scratch_dwell_time and units are the "correct" values
    dwell_time_convert(); // writes scratch_dwell_time to dwell_time_ms; checks value against  SIS_min_dwell_time_ms

   document.getElementById('caldt').innerHTML=  dwell_time_ms; // calculated table
    update_time_bins(); // calculates  num_bins  and other parameters for the image labels

}

function update_time_bins()
{ // update the image labels if parameters have changed
  // DAC Scan Params is done by check_dac_params()
    var i,j;
    var update=0;

    // recalculate the number of time bins 
  
    //  var num_mcs_time_bins = ODBGet(num_mcs_time_bins_path);

       //num_bins_req replaces jvar1= ODBGet(path1); // get requested number of time bins again (may have changed)
       //i = parseInt(jvar1); // requested number of time bins (input param)
    i = parseInt(num_bins_req);
    j = parseInt(num_mcs_time_bins);  // actual number of time bins (output param)
    //alert('update_time_bins: requested number of time bins is '+i+' number time bins in output is '+j)
    //  if(get_bool(jvar16))
    if(discard_bin1)
	i++; // add 1 to number of time bins
       
    if(i != j)
    {
	//	ODBSet(num_mcs_time_bins_path, i);
        writePaths[writeIndex]=output_path+"num bins per cycle";
        writeValues[writeIndex]=i;
        writeIndex++;
	//	alert('update_time_bins: updated number of time bins to '+i+' in path '+num_mcs_time_bins_path)
        update=1;
     }	

     // get the dwell time in ms
    // dwell_time_ms = var kvar = ODBGet( dwell_time_ms_path);
     var k = parseFloat(dwell_time_ms); // dwell time in ms
     var gate_ms = parseFloat (k * i); // number of bins * dwell time in  ms


     //  alert('update_time_bins: dwell_time '+k+'ms * '+i+' bins  = '+gate_ms+'ms');
    
     // ODBSet( one_cycle_time_path_ms,gate_ms);
   
     writeValues[writeIndex]=gate_ms; //   
     writePaths[writeIndex]=output_path+"one cycle time (ms)" //  one_cycle_time_path_ms
     writeIndex++;

     document.getElementById('calgw').innerHTML= gate_ms;
   
      //   alert ('update_time_bins:  set cycle time path '+one_cycle_time_path_ms+'to gate_ms= '+gate_ms);
      var gate_sec = parseFloat(gate_ms * 1000);
      //ODBSet( one_cycle_time_path_sec,gate_sec);
 
      writeValues[writeIndex]=gate_sec; 
      writePaths[writeIndex]= output_path+"one cycle time (sec)" //  one_cycle_time_path_sec
      writeIndex++;
      //   alert ('update_time_bins:  set cycle time path '+one_cycle_time_path_ms+'to gate_sec= '+gate_sec);

       document.getElementById('calgw').innerHTML = gate_ms;

   
       return;

}


function check_dac_params(DAC_start, DAC_inc, DAC_ninc)
{

    // get_global_data    disable_dac_check = get_bool(ODBGet(dac_dis_step_check_path));  

   //alert('input params: '+DAC_start+ DAC_inc+ DAC_ninc);
 

   var s = parseFloat(DAC_start);
   var f = parseFloat(DAC_inc);
   var n = parseInt(DAC_ninc);
   var mini;

   if(f==0)
   {
       alert ('DAC increment cannot be '+f+' Volts');
       return;
   }


   if(disable_dac_check)
       mini = 0;
   else
   {
       // get_global_data    var min_DAC_incr = ODBGet(dac_min_step_path);
      mini = parseFloat(min_DAC_incr);
   }

   

   var u=f;
   if (u < 0) u*=-1; // u=abs(f)
   if( u < mini)
   {
       alert ('DAC increment is set too small. Minimum is '+mini+' Volts');
       return;
   }
 
  
   if (s > max_DAC || s < min_DAC)
   {
      alert ('DAC start is out of range (DAC range is '+min_DAC+' to '+min_DAC+' Volts)');
        document.getElementById('dacstrt').style.backgroundColor="red";
      return;
   }
   var st =0.0;
   st = s + (f * n)
   if (st >  max_DAC || st < min_DAC)
   {
       alert ('Calculated DAC stop '+roundup(st, num_dp)+' Volts is out of range (DAC range is  '+min_DAC+' to '+min_DAC+' Volts). \n Reduce \"Number of DAC increments\" or \"DAC increment\"');
   document.getElementById('calds').style.color="red";
  document.getElementById('ndacinc').style.backgroundColor="red";
  document.getElementById('dacinc').style.backgroundColor="red";

   }   
  
   var dac_stop_path = output_path+"DAC Stop";
   //get_global_data  var dac_stop = ODBGet(dac_stop_path);
   fstop = parseFloat(dac_stop);  // global as needed in load()
   // alert('start = '+s+' inc = '+f+' num inc = '+n+' stop = '+st+' dac_stop read from odb  = '+dac_stop);
   if(st != fstop)  // stop value has changed
       {  // alert('Setting path '+path10+' to '+st)
	   // ODBSet(dac_stop_path, st);
           writePaths[writeIndex]=dac_stop_path;
           writeValues[writeIndex]=st;
           writeIndex++;

           fstop=st; // new value
   }
   document.getElementById('calds').innerHTML=roundup(fstop, num_dp);
   
   return;   
}

function s_msg()
{

  if (rstate != state_stopped)
  {
    var my_element = document.getElementById('smsg').innerHTML

	// get_global_data    num_scans = ODBGet(nscans_path);

	//   alert('s_msg... my_element= '+my_element +' test is '+pattern_y.test(stop_eos));

    //	alert('stop_eos='+stop_eos)
    //  if(pattern_y.test(stop_eos))
	if(stop_eos)
    {
      document.getElementById('smsg').innerHTML='Note: run will stop at the end of current DAC scan'
      document.getElementById('smsg').style.color="fuchsia";
    }
    else if (num_scans > 0)
    {
      document.getElementById('smsg').innerHTML='Note: run will stop after '+num_scans+' DAC scans'
      document.getElementById('smsg').style.color="fuchsia";
    }
    else // free-running
      document.getElementById('smsg').innerHTML=''
   }
  else
      { // not running
         document.getElementById('smsg').innerHTML=''
      }
  return;
}


function info()
{
  alert ('Threshold mode: enter 1 for threshold mode 1 (c.f. discriminator level -> removes spikes) or 0 for % value')
  return;
}




function get_run_buttons(rstate)
{

   var txt='';
  
   if (rstate == state_stopped) // stopped
      txt+= '<input name="cmd" value="Start" type="submit">'
   else if (rstate == state_paused) // paused
      txt+='<input name="cmd" value="Resume" type="submit">'
  
   else  // running
   {
       // stop works again now
       // txt+= '<input name="customscript"  style="color:red"  value="Stop now" type="submit">'

      txt+='<input name="cmd" value="Stop" type="submit">' 
      txt+='<input name="cmd" value="Pause" type="submit">'
      if(!stop_eos)
         txt+='<input type="button" value="StopAtEndScan"  style="color:firebrick" onclick="ODBSet(stop_path, 1)">'
 
   }
   return txt;
}

function load_info_table()
{
    var fvar,jvar;
    fvar = min_delay * 1000000; // ns
    //fvar = roundup(fvar,2);
    jvar=parseFloat(fvar);
    fvar=jvar.toFixed(2);

   document.getElementById('minpw').innerHTML='Minimum PPG pulsewidth & delays are actually<br> 3 PPG clock cycles, i.e. '+fvar+' ns'
   document.getElementById('dsr').innerHTML='DAC scan range:'+min_DAC+' to '+max_DAC+'V'



   // Disable checking
     // txt+='<tr>';
     // txt+='<th  id=dachk  class="param2"  colspan=2  align="left" >';
     // disable_dac_check = get_bool(ODBGet(dac_dis_step_check_path));
   
     // txt+='Disable DAC check  : ';
  
     // txt+= '<input name="dacb" type="checkbox"  onClick="ODBSet(dac_dis_step_check_path, this.checked?\'1\':\'0\'); location.reload(true)">'


     // if(disable_dac_check)
     //  txt+= '<br><span class="info2">No checks on DAC</span>'
     // else
     // txt+= '<br><span class="info">Checks if DAC has stepped</span>'

     // txt+='</th></tr>'
   
    // if(!disable_dac_check)
     //  {
	     
     //Dac minimum increment
     //  txt+='<tr align="left">';
     //  txt+='<td class="param2">';

 
     //  jvar50= ODBGet(path50);
     //  var jtemp=jvar50;
     //   if(jtemp <= 0) jtemp=0.01; // 12 bit range +/- 10V
     //  txt+='DAC minimum increment (V) : ';
     //   txt+='</td>';
     //   txt+='<th  class="small" >';
     //   txt+='<a href="#" onclick="ODBEdit(path50)" >'
     //  txt+=jvar50;
     //   txt+='</a>';
     //   txt+='' ;
     //   txt+='</th>';
     //   var t = parseFloat(jvar50);
    //   if (t <= 0)
     //    {
     //     alert('Illegal Minimum increment');
     //   ODBSet(path50,jtemp); // replace value
     //  javascript:location.reload(true);
     //   }


     //  txt+='<tr align="left">';
     //  txt+='<td class="param2">';

    //Dac jitter
     // jvar51= ODBGet(path51);
     // var jtemp=jvar51;
     // if(jtemp <= 0) jtemp=0.01; // 12 bit range +/- 10V
     //  txt+='DAC jitter (V) : ';
     //  txt+='</td>';
     //  txt+='<th  class="small">';
     //  txt+='<a href="#" onclick="ODBEdit(path51)" >'
     //  txt+=jvar51;
     //  txt+='</a>';
     //   txt+='' ;
     //   txt+='</th>';
     //  var t = parseFloat(jvar51);
    //  if (t <= 0)
    //  {
     //   alert('Illegal jitter');
     //   ODBSet(path51,jtemp); // replace value
 //  javascript:location.reload(true);
     //  }
     //   } // disable_dac_check
     
      return;

}




function make_param_table()
{
   var txt='';
 
  if(rstate != state_stopped)  
  {
      document.getElementById('nbinrq').innerHTML=parseInt(num_bins_req);
      document.getElementById('dt').innerHTML= dwell_time_ms;
    document.getElementById('scrdt').innerHTML= scr_dt+'<span style="color:white">-</span>'+scr_units
	//	alert('roundup dac_start by num_dp ('+num_dp+') = '+ roundup(dac_start, num_dp) )
    document.getElementById('dacstrt').innerHTML= roundup(dac_start, num_dp)
    document.getElementById('ndacinc').innerHTML=dac_ninc
    document.getElementById('dacinc').innerHTML=roundup(dac_inc, num_dp);
    document.getElementById('dacslp').innerHTML=dac_sleep_time
    document.getElementById('tofpw').innerHTML=tof_pw
    document.getElementById('ncsc').innerHTML=num_cy_sc
    document.getElementById('nscans').innerHTML=num_scans
    return;
  }
  // alert('make_param_table')
  // run is stopped
    if( parseInt(num_bins_req) <= 0)
    {
       alert('At least one time bin must be selected');
       num_bins_req=1;
       // ODBSet(path1,num_bins_req);
       writePaths[writeIndex]=path1;
       writeValues[writeIndex]=num_bins_req;
       writeIndex++;
    }

//<!-- Number of bins requested  -->
     txt='<a href="#" onclick="ODBEdit(path1);calculate_all()" >'
	 txt+=parseInt(num_bins_req); //jvar1;
     txt+='</a>';
     document.getElementById('nbinrq').innerHTML= txt;
  

 
  // if (junits == 0) // set to zero if junits is not working
	 // {    
     // dwell time  - temp
     // global  dwell_time_ms = ODBGet(dwell_time_ms_path);
     txt='<a href="#" onclick="ODBEdit(dwell_time_ms_path)">'+dwell_time_ms+'</a>';
     //  var elem= document.getElementById('dt').innerHTML;
     //  document.getElementById('dt').innerHTML="";
       document.getElementById('dt').innerHTML=txt;
       //  alert('elem was = '+elem+' now elem= '+ document.getElementById('dt').innerHTML);
     //  }
     //   else
     //  { 
       //<!-- scratch Dwell Time value  -->
    // scr_dt   was  jvar2 = ODBGet(path2); path2 = scratch/dwell time
    txt= '<a href="#" onclick="ODBEdit(path2)"; >'
    txt+=scr_dt;//jvar2;
    txt+='</a>';
    txt+='<span style="color:white">-</span>' ; // invisible -> space between value and units
   
 //<!-- Dwell Time units  -->
   
   //scr_units  was  jvar3 = ODBGet(path3);   
    txt+='<a href="#" onclick="ODBEdit(path3)" >'
    txt+=scr_units; //jvar3;
    txt+='</a>';
    txt+='' ;
    document.getElementById('scrdt').innerHTML= txt;
    //}

//<!-- DAC Start  -->
    // var jvar4 = ODBGet(path4);     dac_start
    //  var jvar5 = ODBGet(path5);    dac_ninc
    // var jvar6 = ODBGet(path6);     dac_inc

    txt='<a href="#" onclick="ODBEdit(path4)" >'
	//  txt+=dac_start;
    txt+=roundup(dac_start, num_dp)
    txt+='</a>';
    txt+='' ;
    document.getElementById('dacstrt').innerHTML= txt;

//<!-- Num DAC Increments  -->


    //alert('path: '+path5+' value: '+dac_ninc);
    if (parseInt(dac_ninc) <= 0)
    {
       dac_ninc=1;
       alert('Minimum number of DAC increments is 1');
       // ODBSet(path5,dac_ninc);
       writePaths[writeIndex]=path5;
       writeValues[writeIndex]=dac_ninc;
       writeIndex++;
    }
    txt='<a href="#" onclick="ODBEdit(path5)" >'
    txt+=dac_ninc;
    txt+='</a>';
    document.getElementById('ndacinc').innerHTML= txt;    




// 6 <!-- DAC Increment  -->
    
   // alert('path: '+path6+' value: '+dac_inc);
    txt='<a href="#" onclick="ODBEdit(path6)" >'
    txt+=roundup(dac_inc, num_dp);
    txt+='</a>';
    //alert('path: '+path6+' value: '+dac_inc);
    document.getElementById('dacinc').innerHTML= txt;    

  




// 8  <!-- DAC sleep time  -->

       //dac_sleep_time     var jvar8 = ODBGet(path8);
       //alert('path: '+path8+' value: '+dac_sleep_time);

       txt='<a href="#" onclick="ODBEdit(path8)" >'
       txt+=dac_sleep_time; //jvar8;
       txt+='</a>';
       document.getElementById('dacslp').innerHTML= txt;      
  



// 9  <!-- DAC/TOF pulse width  -->

       // tof_pw  was   var jvar9 = ODBGet(path9);
       //alert('path: '+path9+' value: '+ tof_pw);

       txt='<a href="#" onclick="ODBEdit(path9)" >'
       txt+= tof_pw;// jvar9;
       txt+='</a>';
       document.getElementById('tofpw').innerHTML= txt;      
   
  


// 11  <!-- Num cycles to sum   -->
    //num_cy_sc was var jvar11 = ODBGet(path11);
    //alert('path: '+path11+' value: '+num_cy_sc);
    if (parseInt(num_cy_sc) <= 0) //  parseInt(jvar11);
    {
       num_cy_sc =1;
       alert('Minimum value for number of PPG cycles per DAC increment is 1');
       //ODBSet(path11,num_cy_sc);
        writePaths[writeIndex]=path11;
       writeValues[writeIndex]=num_cy_sc;
       writeIndex++;
    }
    txt='<a href="#" onclick="ODBEdit(path11)" >'
    txt+=num_cy_sc;//jvar11;
    txt+='</a>';
    document.getElementById('ncsc').innerHTML= txt; 


// 15  <!-- Number of Scans  -->

    //num_scans was  var jvar15 = ODBGet(nscans_path);
    //alert('path: '+nscans_path+' value: '+num_scans);
    if(parseInt(num_scans) < 0) // jvar15
    {
       num_scans=0;
       //ODBSet(nscans_path,num_scans);
       writePaths[writeIndex]=nscans_path;
       writeValues[writeIndex]=num_scans;
       writeIndex++;
    }

    txt='<a href="#" onclick="ODBEdit(nscans_path)" >'
    txt+=num_scans;//jvar15;
    txt+='</a>';
    document.getElementById('nscans').innerHTML= txt;
    return;
}


function fill_hot_debug_table()
{
 
  // init checkboxes

   document.form2.hdbg.checked=hdb; // hot debug path18
   document.form2.htim.checked=htm; // hot timer path19
   
   document.getElementById('hdbgc').style.backgroundColor =  colArray3[hdb]
   document.getElementById('htimc').style.backgroundColor =  colArray3[htm]
 
}
function  fill_fe_flags_table()
{    // small table  for Discard 1st bin, Discard 1st cycle, PPG ext start   and toggle hot flags
     
     if(rstate== state_stopped)
     {  // init checkboxes
	if(  document.form2.dfb != undefined)
	     {
        document.form2.dfb.checked=discard_bin1; // get_bool(ODBGet(path16));
        document.form2.dfc.checked=discard_cycle1; // get_bool(ODBGet(path28));
        document.form2.exst.checked=ppg_ext_start; //get_bool(ODBGet(path7));
	     }
        
     }
     else
     {
         document.getElementById('ffdfb').innerHTML= unbool(discard_bin1);
         document.getElementById('ffdfc').innerHTML= unbool(discard_cycle1);
         document.getElementById('ffexst').innerHTML= unbool(ppg_ext_start);
         changeClassBGColor("param2","silver" );
	 //  alert('changed parm2 to silver')
     }
     // document.getElementById('smsg').innerHTML = remember_smsg;

      document.getElementById('ffdfb').style.backgroundColor = colArray2[discard_bin1]
      document.getElementById('ffdfc').style.backgroundColor = colArray2[discard_cycle1]
      document.getElementById('ffexst').style.backgroundColor = colArray2[ppg_ext_start]
   
} // end of function  

function roundup(value,num)
{
    // value will be rounded to num decimal places
    var fval=parseFloat(value);
    fval.toFixed(num);
    return fval.toFixed(num);
}

function make_running_param_table()
{ 
    var txt="";
    var j,len,cf,av;

    
  document.getElementById('cycl4').innerHTML=  cycl[4];
  // alert('cycl7='+roundup(cycl[7], num_dp));
  document.getElementById('cycl7').innerHTML=  cycl[7]; //  Increment number (integer)
  
  document.getElementById('cycl8').innerHTML=  roundup(cycl[8],  num_dp); //  roundup Set to n  dp

  document.getElementById('cycl9').innerHTML=  roundup(cycl[9],  num_dp); //  roundup Rd to n  dp

  av= roundup(cycl[10], num_dp); //  roundup Av to n  dp
  document.getElementById('cycl10').innerHTML=  av; 
  document.getElementById('cycl1').innerHTML=  cycl[1];
  document.getElementById('cycl2').innerHTML=  cycl[2];

  // dbug array  path 30 0 =   path_sishis + "DBUG[*]";  assigned in get_global_data()
  len=5; // display first 5 words
 

  if (dbug.length < 4)
    len = dbug.length;

  txt="";
    for (j=0 ; j<len ; j++)
      txt+='<td >'+dbug[j]+'</td>';

    document.getElementById('dbg').innerHTML=  txt;

 
 
  //   <!-- MSUMS  Scaler Sums  -->
  //  var path17 = path_sishis + "SUMS[*]";  sums array now got in get_global_data()

    txt='<td  colspan="1"  class=smhdr style="font-size:75%">SumPerSC</td>';
    // a = ODBGet(path17); sums array
 
    // alert('sums (not rounded)='+sums);
    for (i=0 ; i<sums.length ; i++)
      txt+='<td>'+sums[i]+'</td>';
    document.getElementById('ssums').innerHTML=  txt;
  
    // alert('Av cycl (not rounded) = '+cycl[10]+' '+cycl[11]+' '+cycl[12]+' '+cycl[13]);
  txt='<td  colspan="1"  class="smhdr">Av(V)</td>';
  // av calc above for ADC[0]
  txt+='<td>'+av+'</td>'
  av = roundup(cycl[11], num_dp); //  roundup Inc to n  dp);
  txt+='<td>'+av+'</td>'
  av = roundup(cycl[12], num_dp ); //  roundup Inc to n  dp);
  txt+='<td>'+av+'</td>'
      av = roundup(cycl[13],  num_dp); //  roundup Inc to n  dp,);
  txt+='<td>'+av+'</td>'
  txt+='</tr></table>';  // Frontend Stats table
  document.getElementById('adcs').innerHTML=  txt;


    return 
}

function changeClassBGColor(myclass,colour) {
    // var myclass =     document.getElementsByClassName('fenormal');
 var myclass =     document.getElementsByClassName(myclass);
  for(i=0; i<myclass.length; i++) {
    myclass[i].style.backgroundColor =colour;
  }
}

function fill_bins_parameters_table()
{ 
    var j43= parseInt(discard_cycle1);
    var j44 = num_cy_sc + j43;
    var j47 = lne_preset - valid_bins_sc;


  document.getElementById('ptab1').innerHTML= num_mcs_time_bins;
document.getElementById('ptab2').innerHTML=j44
document.getElementById('ptab3').innerHTML=lne_preset
document.getElementById('ptab4').innerHTML=' - '+discard_bin1
document.getElementById('ptab5').innerHTML=' - '+j43
document.getElementById('ptab6').innerHTML=' - '+j47


    document.getElementById('ptab7').innerHTML=' = '+parseInt(num_bins_req)
document.getElementById('ptab8').innerHTML=' = '+num_cy_sc
document.getElementById('ptab9').innerHTML=' = '+valid_bins_sc

} // end of function  make_running_param_table()



//</script>

function write_last_message()
{
  myODBGetMsg("midas",0,1,msg_callback);
}

function progress()
{
    var colour = new Array("black","red",   "blue", "slateblue", "brown", "purple",  "green",  "fuchsia","black","black");
  //                        0        1        2        3          4        5          6         7         8       9
  //                      init     read      assign    write    load      load       update   write        
  //                               waitfor   data      msg      partial   all                 waitfor
  //                               callback           callback                                callback        
  //             
  //                                11       12       13          14      15          16        17
  //                               read got  build    write      done     done       done      write 
  //                               callback  psm      msg got                                  got
  //                                         done     callback                                 callback  
    var rlp=new String();
    var color = new Array("grey","black");
  clearTimeout(progressTimerId);

   if(progressPeriod > 0)
          progressTimerId = setTimeout('progress()', progressPeriod);

  if(progressFlag < 10)
      {
     document.getElementById('myProgress').innerHTML +='<span style="color:'+colour[progressFlag]+'">'+progressFlag+'</span>'
     remember_progress=document.getElementById('myProgress').innerHTML;
      }
     else
	 {
	    
             if(progressFlag==progress_last)
	     {
                 progress_last_index++;
                 if(progress_last_index > 1) progress_last_index=0;
	
                 document.getElementById('myProgress').innerHTML =remember_progress+'<span style="color:'+color[progress_last_index]+'">'+(progressFlag-10) +'</span>'
               
	     }
	     else
	     { 
                remember_progress=document.getElementById('myProgress').innerHTML;
                progress_last_index=0; // black
                document.getElementById('myProgress').innerHTML +='<span style="color:black">'+(progressFlag-10)+'</span>'
	     }
	 }
  progress_last=progressFlag;
}


function myODBGetMsg(facility, start, n, callback)
{
  var url = ODBUrlBase + '?cmd=jmsg&f='+facility+'&t=' + start+'&n=' + n;
  return ODBCall(url, msg_callback);
}

function msg_callback(msg)
{
    var pattern1=/ERROR]/

 // latest MIDAS (August 2015) - clean off unwanted information
    var pattern2=/^\d+ (\d\d:\d\d:\d\d)\.\d+( .+)/
			 //	alert('msg_callback: msg='+msg)
    msg=msg.replace(pattern2,"$1 $2"); 

  if(pattern1.test(msg))
        msg = msg.fontcolor("red"); // turn red if error
   
    document.getElementById('lastmsg').innerHTML = msg;
    progressFlag= progress_msg_done // all done
}


function cs_odbset(one_path, one_value)
{   // cs_odbset   convert single values to array then call async_odbset

    var Paths=new Array(); var Values=new Array();
    
    Paths[0]=one_path;
    Values[0]=one_value;
    //   alert('cs_odbset:  Paths='+Paths[0]+' and Values='+Values[0])
       if(gbl_debug)
           document.getElementById('gdebug').innerHTML='<br>cs_odbset:  Paths='+Paths[0]+' and Values='+Values[0]
   async_odbset(Paths,Values)
   return;  
}



function async_odbset(paths,values)
{
   // expects parameters are ARRAYS
   var len;
  // alert('async_odbset:length of paths array='+paths.length+' and values.length='+values.length);
   
    progressFlag= progress_write_callback
    if(gbl_debug) 
         document.getElementById('gdebug').innerHTML+='<br>async_odbset: writing  paths '+paths+' and values '+values
	     // alert('async_odbset:  writing  arrays paths '+paths+' and values '+values)
    mjsonrpc_db_paste(paths,values).then(function(rpc) {
        var i;
    if(!document.form2.rws.checked)
        document.getElementById('writeStatus').innerHTML = 'async_odbset  status= '+rpc.result.status
        len=rpc.result.status.length // get status array length
	//  alert('async_odbset: length of rpc.result.status='+len+' status='+rpc.result.status)
          if(gbl_debug) document.getElementById('gdebug').innerHTML='async_odbset:   length of rpc.result.status='+len+' status='+rpc.result.status
        for (i=0; i<len; i++)
        {
           if(rpc.result.status[i] != 1) 
	       {
		   // alert(' async_odbset: status error '+rpc.result.status[i]+' at paths['+i+']="'+paths[i]+'"')
                    document.getElementById('writeStatus').innerHTML= '<br>async_odbset: status error '+rpc.result.status[i]+' at paths['+i+']="'+paths[i]+'"'
                    document.getElementById('writeStatus').style.Color="red";
               }
        } 
  if(!document.form2.rws.checked)
        document.getElementById('writeStatus').innerHTML='async_odbset: writing paths '+paths+' and values '+values
  if(gbl_debug)
        document.getElementById('gdebug').innerHTML+='<br>async_odbset: wrote paths '+paths+' and values '+values
      
     progressFlag= progress_write_got_callback
    }).catch(function(error)
          { 
          mjsonrpc_error_alert(error); });
 }


function ODBcs(cmd)
{
   var value, request, url;
   
   var request = XMLHttpRequestGeneric();
    url = ODBUrlBase
   if(redir_path!=undefined)
       //url += ODBUrlBase + '?redir=CustomStatus&&customscript='+ encodeURIComponent(cmd);
    url +=  '?redir='+redir_path
    url+='&customscript='+ encodeURIComponent(cmd);
    //  alert('url='+url)
   request.open('GET', url, true); // async

   request.send(null);
   // alert('url='+url+'request.status='+request.status)
 //      if (request.status != 200 || request.responseText != 'OK') 
//        alert('ODBcs error:\nHTTP Status: '+request.status+'\nMessage: '+request.responseText+'\n'+document.location) ;
//   if (request.status != 200)
//       alert('ODBcs Error calling Custom Script "'+cmd+'" :\nHTTP Status: '+request.status)
}

// NOT USED
function myODBEdit(path, code, value)
{ // from bnqr
   //clear_timer(); // temporarily stop the timer
   stop_page_update =1;
   var new_value = prompt('Please enter new value', value);
   if (new_value != undefined) {
      ODBSet(path, new_value);
      // window.location.reload();  // reread data
      update(code);
   
   }
   stop_page_update=0;
}


function get_elapsed_time(units)
{
    var mydate=new Date()
    var time_now = mydate.getTime() /1000 ; // present time in seconds
    var binary_time =  RunInfoData["start time binary"]
    var elapsed_time = time_now - binary_time // seconds
    var temp = Math.round(elapsed_time)
    var pattern = /seconds/;
    if(pattern.test(units))
	    return temp; // return the number of seconds

    message = temp + ' sec'

    if (elapsed_time > 60)
    {
       temp = elapsed_time % 60  // remainder in seconds
       var elapsed_min = (elapsed_time - temp)/60  // subtract remainder -> integer minutes
       var elapsed_sec = Math.round(temp) // rounded remainder (sec) 

       message = elapsed_min + ' min ' + elapsed_sec + ' sec';

       if(elapsed_min > 60)
       {
          temp = elapsed_min % 60 // remainder in minutes
          var elapsed_hours = (elapsed_min - temp) / 60  // integer hours 
          elapsed_min = Math.round(temp) // round remainder (min)

          message =  elapsed_hours + ' hr ' +  elapsed_min + ' min ' + elapsed_sec + ' sec';

          if (elapsed_hours > 24)
          {
             temp  =  elapsed_hours % 24  // remainder in hours
             var elapsed_days =  (elapsed_hours -  temp) / 24  // integer days 
             elapsed_hours = Math.round(temp) // round remainder (hours)
             message = elapsed_days + ' days ' +  elapsed_hours + ' hr ' +  elapsed_min + ' min ' + elapsed_sec + ' sec';
          }
       }
    }
    return message;
}

function enable_page_debug(val)
{
    val=parseInt(val);
    cs_odbset("/custom/hidden/custom page debug", val)
    document.form2.pdbg.checked=val;
    gbl_debug=val;
    if(gbl_debug)
         document.getElementById('gdebug').innerHTML="<br>global debug is enabled"
    else
        document.getElementById('gdebug').innerHTML=""
}
function hide_rwstatus(val)
{
  val=parseInt(val);
  document.form2.rws.checked=val;
  hide_status=val;
}