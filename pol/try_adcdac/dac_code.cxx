/*  DAC_code.cxx

    This code for new VME hardware 
*/

#ifdef HAVE_DAC
INT set_DAC( MVME_INTERFACE *myvme,  float dac_volts)
{
  INT status;
  
  /*** Set the DAC to dac_volts volts  ***/


  status = convert_to_hex(dac_volts, &wdata);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"set_DAC","DAC value %f Volts is out of range (9.9951 Volts to -10 Volts)  ",dac_volts);
      return status;
    }

  // rdata, rdata_prev are global
  rdata_prev =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 ); // read previous value
  AD_regWrite16(myvme,DAC_BASE, DAC_V4800_CHAN_0, wdata ); // write 2s comp to DAC Chan 0



  if(hot_debug)
    printf("set_DAC: wrote %f volts (0x%x) to DAC \n\n",  dac_volts,wdata);    


  return SUCCESS;
}


float read_DAC( MVME_INTERFACE *myvme)
{
  float dac_read;
  // rdata is global (value read in hex 2s comp)
  rdata =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 ); // read value (hex)
  dac_read = retrieve_from_hex(rdata);  
  
  printf("read_DAC: read DAC value from ADC as %f volts (2s comp = 0x%x)\n",dac_read, rdata);

  return dac_read ;  // return 
} 




int check_DAC_changed( MVME_INTERFACE *myvme)
{
  // call after stepping DAC to a DIFFERENT value

  // rdata, rdata_prev are global (value read in hex 2s comp)
  rdata =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 ); // read value from channel 1 (hex)

  if ( abs(rdata  - rdata_prev)  < 3 )
    {
      printf ("check_DAC_changed:DAC has NOT changed; present readback:0x%4.4x  previous readback:0x%4.4x  set value 0x%4.4x\n",rdata,rdata_prev,wdata);
      return 0;
    }
  printf("check_DAC_changed: DAC HAS changed;  present readback:0x%4.4x  previous readback:0x%4.4x  set value 0x%4.4x\n",rdata,rdata_prev,wdata);
  dac_read = retrieve_from_hex(rdata);  // dac_read is global

  return 1;
}


#else  // DUMMY DAC
INT set_DAC( MVME_INTERFACE *myvme, float dac_volts)
{
  
  printf(" ***  Set_DAC: dummy dac is now set to %f Volts \n\n",dac_volts);
  return SUCCESS;
}

float read_DAC( MVME_INTERFACE *myvme)
{
  
  printf("read_DAC: dummy DAC value read back is set value  %f\n",dac_volts);
  return dac_volts;
}

INT check_DAC_changed( MVME_INTERFACE *myvme)
{
  printf("read_DAC: dummy DAC has changed\n");
  return 1; // true
}

#endif // DUMMY_DAC

