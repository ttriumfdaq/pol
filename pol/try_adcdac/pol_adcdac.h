// Headers for pol_adcdac.c
// POL  VMIC DAC and ADC
//

#ifndef __POLADCDAC__
#define  __POLADCDAC__

 // function prototypes

uint32_t AD_regRead8(MVME_INTERFACE *mvme, DWORD base, int offset);
void AD_regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);
void AD_regWrite16(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);
uint32_t AD_regRead16(MVME_INTERFACE *mvme, DWORD base, int offset);
void setup_DAC(MVME_INTERFACE *mvme,DWORD base);
void setup_ADC(MVME_INTERFACE *mvme,DWORD base);
void  DAC_Status(MVME_INTERFACE *mvme, DWORD base);
void  ADC_Status(MVME_INTERFACE *mvme, DWORD base);
int convert_to_hex(float volt,  uint32_t *data );
float retrieve_from_hex(int data );

#ifdef MAIN_ENABLE
int set_dac( MVME_INTERFACE *myvme,  float volts);
int timeval_subtract (result, x, y);
#endif


#endif //  __POLADCDAC__
