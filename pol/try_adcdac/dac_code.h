/* dac_code.h

   function prototypes for functions in dac_code.cxx

*/

#ifndef __DAC_CODE__
#define __DAC_CODE__

INT set_DAC( MVME_INTERFACE *myvme,  float dac_volts);
float read_DAC( MVME_INTERFACE *myvme);
int check_DAC_changed( MVME_INTERFACE *myvme);

#endif
