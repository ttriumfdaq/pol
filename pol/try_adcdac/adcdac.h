/***************************************************************************/
/*                                                                         */
/*  Filename: sis3820.h                                                    */
/*                                                                         */
/*  Function: headerfile for Pol's ADC and DAC                             */
/*                                                                         */
/*      VMIVME-3801   VMIVME-4800                                          */
/***************************************************************************/


/* DAC addresses  (offsets from base) */

#define DAC_V4800_BOARD_ID                              0x0             /* Board ID  ; D16  RO */
#define DAC_V4800_CONTROL_STATUS                        0x4             /* CSR       ; D16  RW */
#define DAC_V4800_CHAN_0                                0x8             /* Channel 0 ; D16  RW */
#define DAC_V4800_CHAN_2                                0xC             /* Channel 2 ; D16  RW */


/* ADC addresses (offsets from base)  */

#define ADC_V3801_BOARD_ID                              0x0             /* Board ID        ; D8  RO */
#define ADC_V3801_CONFIG                                0x1             /* Configuration   ; D8  RO */
#define ADC_V3801_CONTROL_STATUS                        0x2             /* CSR             ; D8  RW */
#define ADC_V3801_CHAN_PNTR                             0x3             /* Channel Pointer ; D8  RO */
#define ADC_V3801_CHAN_0                                0x40            /* Channel 0 Data  ; D8  RW */
#define ADC_V3801_CHAN_1                                0x40            /* Channel 0 Data  ; D8  RW */
#define ADC_CHAN_OFFSET                                 0x2             /*  Offset of each channel data from previous i.e. 0x40,0x42...0x7e */

// Constants and Masks
#define DAC_BOARDID                    0x4600    /* DAC Board ID */
#define DAC_TWOS_COMP_BIT              0x1000    /* Set 2s complement in DAC_V4800_CONTROL_STATUS reg */
#define DAC_OUTPUT_EN_BIT              0x2000    /* Set Output Enable in  DAC_V4800_CONTROL_STATUS reg */
#define DAC_LED_OFF_BIT                0x8000    /* Set LED OFF in  DAC_V4800_CONTROL_STATUS  reg */

#define ADC_BOARDID                      0x44    /* ADC Board ID */
#define ADC_TWOS_COMP_BIT                   4    /* Set 2s complement in  ADC_V3801_CONTROL_STATUS reg */
#define ADC_LED_OFF_BIT                  0x80    /* Set LED OFF in  ADC_V3801_CONTROL_STATUS reg */

#define ADC_CSR_LED_OFF   ADC_TWOS_COMP_BIT  |  ADC_LED_OFF_BIT 
#define ADC_CSR_LED_ON    ADC_TWOS_COMP_BIT 

#define DAC_CSR_LED_OFF   DAC_OUTPUT_EN_BIT  | DAC_TWOS_COMP_BIT |  DAC_LED_OFF_BIT 
#define DAC_CSR_LED_ON    DAC_OUTPUT_EN_BIT  | DAC_TWOS_COMP_BIT
