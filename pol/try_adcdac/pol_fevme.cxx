/********************************************************************\

  Name:        pol_fevme.cxx
  Created by:   Suzannah Daviel   
                based on code by K.Olchanski

  Contents:     Frontend for the POL VME DAQ

  $Id: pol_fevme.cxx,v 1.5 2013/10/10 19:44:10 suz Exp $

  Supercycle version - Number of LNE for SIS scaler is now set for the supercycle (not the cycle) 
  Threshold and single cycle histos removed

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <math.h>
#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "mvmestd.h"
#include "evid.h"



#define HAVE_SIS3820
#define HAVE_PPG  // Any PPG
#define NEW_PPG  // New PPG;
//#undef  NEW_PPG // OLD PPG
#define HAVE_DAC // real DAC VMIVME4800
#define HAVE_ADC // ADC  VMIVME3801
//#undef  HAVE_DAC // dummy DAC
extern "C" {
#ifdef HAVE_VMEIO
#include "vmeio.h"
#endif

#ifdef HAVE_SIS3820
#include "sis3820drv.h"
#include "my_sis3820.h"
#include "sis_code.h" // prototypes
#endif

#ifdef NEW_PPG 
#include "newppg.h"
#include "ppg_modify_file.h" // prototypes
#else
#include "vppg.h"
#endif
}

#ifdef HAVE_PPG 
  // PPG 
#include <sys/stat.h> // time 
#include "experim.h"
#include "unistd.h" // for sleep 
#include "ppg_code.h" // prototypes
#endif

//DAC
#ifdef HAVE_DAC
#include "VMIVME4800.h"
#include "pol_dac.h"
#include "pol_adcdac.h"
#include "dac_code.h"
#endif
// ADC
#ifdef HAVE_ADC
#include "VMIVME3801.h"
#include "pol_adcdac.h"
#include "dac_code.h"
#endif


//#include "threshold_code.h" // threshold prototypes and globals


INT  process_histo(void);
int stop_PPG(void);

static INT wait_cntr;

#define N_HISTO_MAX 6  // four channels
#define N_SCALER_MAX 6  // presently four channels used for POL
BOOL send_mcs=0;  // if true raw scaler data bank will be sent out (only sums sent otherwise)   bank MCS0


// globals for POL
float gbl_first_word=-101;
// minimum dwell times - otherwise the SIS scaler does not see every LNE from PPG
const float  SIS8_min_dwell_time_ms = 0.000224;  //   minimum dwell time for SIS in ms (8 bits data)
const float  SIS16_min_dwell_time_ms = 0.000234;  //   minimum dwell time for SIS in ms  (16 bits data)
const float  SIS32_min_dwell_time_ms = 0.000244;  //   minimum dwell time for SIS in ms  (16 bits data)

/* Input Parameters to control PPG and SIS (read from ODB) that need to be global:
 */

POL_ACQ_SETTINGS ps;
char  cmd[128]; // holds command to run tri_config
char  data_path[256]; // for /logger/data dir  (to save tri_config output file)

/*int  sismode (ps.input.sis3820.sis_mode)                        PPGMODE

		0 TEST pulses - no PPG   
		1 external LNE with test input pulses   1h
		2 Ext LNE, real inputs;                 1h

*/

//DWORD  requested_bins;  // number of bins requested by user
DWORD  num_bins; // number of bins  required = LNE_preset  (this is the number of bins * number of cycles/supercycle)  
                // NOTE:  if LNE_prescale is > 1, will need more LNE_input pulses to produce each bin
                //        for POL, LNE_prescale will ALWAYS be 1
//DWORD  LNE_prescale_factor; // 1 for no prescale
DWORD LNE_total;
INT  num_cycles_per_supercycle;
BOOL end_of_sweep_flag=FALSE;
INT    data_bytes=2; // number bytes for each data word default= 4 for 32-bit data, 2 for 16-bit, 1 for 8-bit 
                     // decoding only for 16-bit data currently

//BOOL   run_triconfig=TRUE; // run triconfig or not. POL does not run tri_config 
INT    num_scans; // 0= free-running  number of complete DAC scans wanted

// DAC 
float   dac_val;        /* dac value to set in volts */
float   my_dac_val; // dac value for current cycle
float   dac_read;   // dac value read back in volts */

// VMIC DAC read back on ADC Channel 1
int rdata, rdata_prev; // readback from ADC of DAC value in counts (hex)
uint32_t wdata; // data written to DAC in counts
 
INT last_gSumMcsEvents=0;

/* More globals */

// ODB handles
HNDLE hTASet,hOut,hPPG,hfe,hStop;
#ifdef HAVE_DAC
HNDLE hReq,hVar; // write DAC set value, ADC readback (4 values)  1st -> DAC value
INT nadc=4; // number of ADC values to be read in /Equipment/ADC/Variables 
#endif

float adc_readback[4];  // VME adc readback

BOOL  ppg_running=0; // flag
int   gbl_DAC_n=0, gbl_SCAN_n=0, gbl_CYCLE_n=0, gbl_SCYCLE_n=0;// counters
INT   dac_ninc;      /* number of dac increments */
BOOL  flush=FALSE;
char  ppgmode[]="1h"; // 1h for POL
INT   scan_loop_counter; // Set to 1 (PPG not free-running) except for testing
BOOL  bor_flag=0; // TRUE if begin_of_run
BOOL  resume_flag=0; // TRUE if run is being continued
uint32_t acq_count; // readback of number of LNE received
BOOL wrote_stuck_msg=0;
BOOL hot_debug=FALSE, lhot_debug=FALSE,  hot_stop=FALSE;
int NumSisChannels; // actual number of SIS channels enabled (calculated from bitpat)
char ppgfile[128]; // ppg file to load (including path)
BOOL stop_run_flag=FALSE;

INT watchrabbit=0; // checking frontend is still alive
INT watchdog=0; // temporary
INT watchInterval=100;
// The following are needed to program new ppg without running tri_config
DWORD dw_count, lne_count, dac_sleep_count, mcs_gate_count;

#define CYCLE 0  // send of cycle scalers
#define HISTO1 1  // send out histo banks 
#define FIFO 2   // scaler data to be read out
#define MCSC  3  // send raw data (usually disabled)
#define HISTO2 4  // send out histo sums and write to ODB
#define DAC 5  // set DAC and receive correct readback 
#define SUPER 6 // new supercycle
BOOL    waiteqp[7] = {TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE};

INT gbl_IN_CYCLE;
BOOL gbl_skip_cycle;
INT  gbl_skip_cycle_cntr;
INT gbl_his_cntr;
BOOL cycle_start_delay=FALSE;
INT gbl_cycle_cntr;
BOOL gbl_in_tol;
INT gbl_rd;
uint32_t *gbl_data_buffer;
int gbl_buf_index=0;

const int ghbd_len = 6; // size of array gbl_histo_bank_data
float gbl_histo_bank_data[ghbd_len]; // holds cycle number, dac value to go with histograms etc.


/* Individual scaler channel */
typedef struct {
    DWORD   nbins;
    double  sum_cycle;   /* scaler sum over one cycle */
    DWORD   *ps;         /* scaler bin values over one cycle */
} MSCALER;
MSCALER  scaler[N_SCALER_MAX];

/* Histogram struct  */
typedef struct {            
    DWORD   nbins;
    double   sum;          /* sum over N cycles (one supercycle) */
    DWORD   *ph;         /* pointer to histogram */
} MHISTO;
MHISTO   histo[N_SCALER_MAX]; // one histo only, but sums for all the defined scaler channels.

//double CumSum[N_SCALER_MAX]; // cumulative sum over the run for each channel

/* DAC SUM struct  */
typedef struct {
  DWORD ninc;
  double *ptr;
} DACSUM;
DACSUM dacsum[N_SCALER_MAX];

DWORD gbl_histo_bin_cntr=0;
DWORD bin_zero;
struct timeval eoct; // time at end-of-cycle
//INT MCSptr; // pointer to MCS_channel

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   char *frontend_name = "fepol";
/* The frontend file name, don't change it */
   char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 500*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 10*1024*1024;


  INT debug = 1;
  extern INT run_state;
  extern HNDLE hDB;
  extern char exp_name[NAME_LENGTH]; 


/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop(); 
  static double timeDiff(const struct timeval&t1,const struct timeval&t2);
  INT read_histo_event(char *pevent, INT off);
  INT read_sispulser_event(char *pevent, INT off);
  INT read_mcs_event(char *pevent, INT off);
  INT read_vme_event(char *pevent, INT off);
  INT read_adc_event(char *pevent, INT off);
  INT init_vme_modules(void);
  INT set_ppg_alarm(BOOL active);
  void send_scaler_banks(char *pevent);
  INT  prestart(INT run_number, char *error); 
  INT  poststart(INT run_number, char *error); 
  INT  post_stop(INT run_number, char *error); 
  BOOL deferred_stop(INT run_number, BOOL first); 
  void stop_call_back(HNDLE hDB, HNDLE hkey, void * info);
  INT DAC_set_scan_params(void);
  void clear_mcs_counters(void);
  INT get_SIS_dataformat ( INT num_bits );
  INT init_sis3820(void);
  void csr_bits(unsigned int read_data);
  void SIS_readback(unsigned int wrote_mode, unsigned int wrote_csr);
  void opmode_bits(unsigned int read_data);
  void  setup_hotlinks(void);
  void call_back_debug(HNDLE hDB, HNDLE hkey, void * info);
  INT set_dac_sleep_time(void);
  void scaler_clear(INT i);
  void dacsum_clear(INT h);
  INT get_pol_settings(void);
  INT read_hsum_event(char *pevent, INT off);
  INT incr_DAC(void);
  INT find_ADC_keys(void);
  void histo_clear(INT h);
  INT cycle_start(void);
  INT new_supercycle(void);
  INT stop_run(void);
  INT fi_init(void);
  INT decode_scaler_data(void);
  INT set_watchdog(void);
/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {
    
    {"VME",                  /* equipment name */
     {EVID_VME, (1<<EVID_VME), /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_MULTITHREAD,         /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* when to read this event */
      100,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* whether to log history */
      "", "", "",}
     ,
     read_vme_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
    {"SisHistory",            /* equipment name */
     {EVID_MCS, (1<<EVID_MCS),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB|RO_BOR,  /* read only when running and at BOR */
      100,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* whether to log history */
      "", "", "",}
     ,
     read_mcs_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
    {"Histo",                   /* equipment name */
     {EVID_HISTO, (1<<EVID_HISTO),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,            /* read only when running */
      100,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* whether to log history */
      "", "", "",}
     ,
     read_histo_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
    {"SisPulser",               /* equipment name */
     {EVID_PULSER, (1<<EVID_PULSER), /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING|RO_TRANSITIONS, /* when to read */
      10000,                  /* read every 10000 milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_sispulser_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
    {"Hsum",               /* equipment name */
     {EVID_EOS, 0, /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* when to read */
      100,                    /* read every 500 milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* log history */
      "", "", "",}
     ,
     read_hsum_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
    {"ADC",               /* equipment name */
     {EVID_ADC, (1<<EVID_ADC), /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_TRANSITIONS, /* when to read */
      2000,                  /* read every 2 seconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_adc_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },

    {""},
  };
  
#ifdef __cplusplus
}
#endif



// equipment indices
#define VME 0
#define MCS 1
#define ADC 2
#define SCAL 3
#define PULS 4
#define SISP 5
#define PPG  6


/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

MVME_INTERFACE *gVme = 0;


BOOL fe_flag=1;

uint16_t gIoreg = 0;

int gVmeioBase   = 0x780000;
int gIoregBase   = 0x510000;
//int gMcsBase[] = { 0x38000000, 0x39000000, 0 };
int gMcsBase =  0x38000000;
int gTtcBase     = 0x37000000;

#ifdef NEW_PPG
const DWORD gPpgBase     = 0x00100000; // NEWPPG
#else
const DWORD gPpgBase     = 0x00008000; // OLDPPG
#endif

#ifdef HAVE_DAC
uint32_t DAC_BASE  = 0x4000;
#endif

#ifdef HAVE_ADC
uint32_t ADC_BASE  = 0x3100;
#endif

int vmeread16(int addr)
{
  mvme_set_dmode(gVme, MVME_DMODE_D16);
  return mvme_read_value(gVme, addr);
}

int vmeread32(int addr)
{
  mvme_set_dmode(gVme, MVME_DMODE_D32);
  return mvme_read_value(gVme, addr);
}

void vmewrite8(int addr, uint8_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D8);
  mvme_write_value(gVme, addr, data);
}

void vmewrite16(int addr, uint16_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D16);
  mvme_write_value(gVme, addr, data);
}

void vmewrite32(int addr, uint32_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D32);
  mvme_write_value(gVme, addr, data);
}

void encodeU32(char*pdata,uint32_t value)
{
  pdata[0] = (value&0x000000FF)>>0;
  pdata[1] = (value&0x0000FF00)>>8;
  pdata[2] = (value&0x00FF0000)>>16;
  pdata[3] = (value&0xFF000000)>>24;
}

void xusleep(int usec)
{
#if 0
  struct timespec req;
  req.tv_sec = 0;
  req.tv_nsec = usec*1000;
  nanosleep(&req, NULL);
#endif
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = usec;
  select(0, NULL, NULL, NULL, &tv);
}

#include "utils.cxx"
#include "sis_code.cxx"
#include "ppg_code.cxx"
//#include "pol_thresholds.cxx"
#include "dac_code.cxx"

void enable_SIS()
{
#ifdef HAVE_SIS3820
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_KEY_OPERATION_ENABLE,0);
  if(hot_debug)printf ("Enabled SIS3820 module  at address  0x%x \n", gMcsBase);
#endif
}

int stop_PPG()
{
  if(ps.input.sis3820.sis_mode == 0) // test mode - no PPG
    return SUCCESS;


  if(ps.input.ppg_external_start)
    { // real mode
#ifdef NEW_PPG
      TPPGDisableExtTrig(gVme,   gPpgBase); // Disable external trigger to prevent PPG restarting
#else
      VPPGDisableExtTrig(gVme,   gPpgBase); // Disable external trigger to prevent PPG restarting
#endif
    }

  check_ppg_running(); // updates global ppg_running
  if(ppg_running)
    {
      printf("stop_PPG: stopping PPG\n");
      /* Stop the PPG sequencer  */ 
#ifdef NEW_PPG
      TPPGStopSequencer(gVme,   gPpgBase); // Halt pgm and stop
#else
      VPPGStopSequencer(gVme,   gPpgBase); 
#endif

      ppg_running = FALSE; // not running 
      ss_sleep(100);
      check_ppg_running(); // updates global ppg_running
      if(ppg_running)
	{
	  printf("stop_PPG: PPG will not stop\n");
	  return FAILURE;
	}
    }
   else
     //if(debug)
       printf("stop_PPG: PPG is already stopped\n");
  return SUCCESS;
}

int enable_PPG()
{

#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0xFFFF);
#endif

#ifdef HAVE_PPG

  if(ps.input.sis3820.sis_mode == 0) // test mode - no PPG
    return SUCCESS;

  if(ps.input.ppg_external_start)
    { // real mode
#ifdef NEW_PPG
      TPPGEnableExtTrig(gVme,   gPpgBase); // Enable external trigger for subsequent cycles
      TPPGStartSequencer( gVme, gPpgBase); // start PPG
#else
      VPPGEnableExtTrig(gVme,   gPpgBase); // Enable external trigger for subsequent cycles
      VPPGStartSequencer( gVme, gPpgBase);// start PPG
#endif
    
     if(hot_debug)printf("enable_PPG: started PPG by software; subsequent cycles will be triggered externally\n");
     return SUCCESS;
    }
  else
    {
      // test mode - internal start
      check_ppg_running(); // updates global ppg_running
      if(ppg_running)
	{
        
	    printf("enable_PPG: PPG is still running ... \n");
	    if( stop_PPG() != SUCCESS)
	      printf("enable_PPG: PPG would not stop\n");

	}

      /* start the PPG  */ 
    
#ifdef NEW_PPG
      TPPGStartSequencer( gVme, gPpgBase);
#else
      VPPGStartSequencer( gVme, gPpgBase);
#endif      

      if(hot_debug)printf("Restarted PPG by software\n");
    
    }
#endif /* PPG */ 
  
  return SUCCESS;
}


int disable_trigger()
{
  //printf("disable_trigger: starting with sismode=%d\n",ps.input.sis3820.sis_mode);
#ifdef HAVE_SIS3820
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_KEY_OPERATION_DISABLE,0);
#endif
#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0);
#endif

  if(stop_PPG() != SUCCESS)
    return FAILURE;

  return SUCCESS;
}



/*-- VME setup     -------------------------------------------------*/

INT init_vme_modules(void)
{
  INT status;

  //printf("\ninit_vme_modules: starting \n");

#ifdef HAVE_VMEIO
  printf ("\ninit_vme_modules: setting up VMEIO...\n");
  vmeio_OutputSet(gVme,gVmeioBase,0);
  vmeio_StrobeClear(gVme,gVmeioBase);
  printf("VMEIO at 0x%x CSR is 0x%x\n",gVmeioBase,vmeio_CsrRead(gVme,gVmeioBase));
#endif
  
  status = init_ppg();
  if (status != SUCCESS)
    return status;

  status =init_sis3820();
  if (status != SUCCESS)
    return status;
  
  printf ("init_vme_modules: SUCCESS \n");
  return SUCCESS;
}


EQUIPMENT* findEquipment(const char*name)
{
  for (int i=0; equipment[i].name[0] != 0; i++)
    if (strcmp(equipment[i].name, name) == 0)
      return &equipment[i];
  cm_msg(MINFO, frontend_name, "Cannot find equipment %s", name);
  abort();
}


/*-- Global variables ----------------------------------------------*/

static int gRunNumber      = 0;

//static double gPulserPeriod[8];

static EQUIPMENT* gSisPulserEquipmentPtr = 0;

static int gSisBufferOverflow = 0;
INT imax;

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int status;
  INT size;
  int i;
  char str[128];
 
  if (run_state != STATE_STOPPED)
    {
      //  cm_msg(MERROR,"frontend_init","Please stop the run and restart this frontend");
      //  printf("\n!!!  frontend_init: STOP THE RUN and RESTART this Frontend !!! \n");
      cm_msg(MINFO,"frontend_init","Stopping the previous run");
      status = cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), ASYNC, 0);
      if(status != SUCCESS)
	printf("frontend_init: cm_transition returns status=%d\n",status); 
      ss_sleep(1000);
      //return  FE_ERR_HW;
    }


  setbuf(stdout,NULL);
  setbuf(stderr,NULL);
  
  cm_enable_watchdog(0);
  
  /* Init all pointers to potential MALLOC calls to NULL */
  for (i=0 ; i < N_SCALER_MAX ; i++)
      scaler[i].ps = NULL;
  for (i=0 ; i < N_HISTO_MAX ; i++)
      histo[i].ph = NULL;
  for (i=0 ; i < N_HISTO_MAX ; i++)
      dacsum[i].ptr = NULL;

  gbl_data_buffer = NULL;

  gSisPulserEquipmentPtr = findEquipment("SisPulser");
  status = mvme_open(&gVme,0);
  status = mvme_set_am(gVme, MVME_AM_A24_ND);

  status = cm_register_transition(TR_START, prestart, 350); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for prestart"); 
      ss_sleep(1000);
      return status;
    } 

  status = cm_register_transition(TR_START, poststart, 600); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for poststart"); 
      ss_sleep(1000);
      return status;
    } 

  status = cm_register_deferred_transition(TR_STOP, deferred_stop); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for deferred_stop"); 
      ss_sleep(1000);
      return status;
    } 


  status = cm_register_transition(TR_STOP, post_stop, 600); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for post_stop"); 
      ss_sleep(1000);
      return status;
    } 

  
  status = get_pol_settings(); // create records, get ODB keys etc.
  if(status !=SUCCESS)
    {
      ss_sleep(5000);
      return status;
    }



#ifdef HAVE_DAC
  status = find_ADC_keys();
  if(status != SUCCESS)
    {
    return status;
      ss_sleep(5000);
    }
#endif
   
  sprintf(str,"/Logger/data dir"); 
  size=sizeof(data_path);
  status = db_get_value(hDB, 0, str, data_path, &size, TID_STRING, FALSE); 
  if(status != DB_SUCCESS) 
  { 
    cm_msg(MINFO,"frontend_init","cannot get data path at %s (%d)",str,status); 
    data_path[0]='\0'; 
  } 
  printf("data path is :%s\n",data_path); 

  status = init_vme_modules();
  if (status != SUCCESS)
    {
      printf("frontend_init: error from init_vme_modules (%d)\n",status);
      ss_sleep(5000);
      return status;
    }

  disable_trigger();
  setup_hotlinks();
  fe_flag=FALSE; // VME etc now initialized
  printf("frontend_init: SUCCESS\n\n");
  return SUCCESS;

}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  INT i;
  disable_trigger();

  mvme_close(gVme);


  printf("free histo ");
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      if (histo[i].ph != NULL)
	{
	  printf(" %d ", i);
	  free(histo[i].ph);
	  histo[i].ph = NULL;
	}
    }
  /* clear previous booking for dacsum */
  printf("freeing dacsums\n ");
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      if (dacsum[i].ptr != NULL)
	{
	  free(dacsum[i].ptr);
	  dacsum[i].ptr = NULL;
	}
    }
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  INT i;
  INT status;
  char str[80];

  bor_flag = 1;
  gRunNumber = run_number;
  stop_run_flag = FALSE;
  waiteqp[FIFO]=waiteqp[HISTO1]=waiteqp[HISTO2]=waiteqp[CYCLE]=waiteqp[MCSC]=waiteqp[DAC]=waiteqp[SUPER]=FALSE;  
  end_of_sweep_flag=FALSE;
  
  
  // Set DAC right away 
   if (ps.input.sis3820.sis_mode > 0 )  // REAL mode
    {
      status = DAC_set_scan_params(); // sets DAC one incr. away from start value
      if(status != SUCCESS) 
	return status;
    }

  for (i=0 ; i < ghbd_len ; i++)
    gbl_histo_bank_data[i]=0.0; // clear


  // clear hotlink "stop at end-of-sweep"
  sprintf(str,"input/stop at end-of-sweep");
  hot_stop=0;
  status = db_set_value(hDB, hTASet, str, &hot_stop, sizeof(hot_stop), 1, TID_BOOL);  
  if(status !=SUCCESS)
    {
      cm_msg(MERROR,"begin_of_run","cannot set ODB key  ...%s to %d (%d)",str,hot_stop,status); 
      return status;
    };
    
  // num_scans is the number of complete scans required
  num_scans = odbReadInt("/Equipment/POL_ACQ/settings/input/Number of scans",0,0); // 0 is free running  
  printf("number of scans requested: %d\n",num_scans);
  
 
  
  send_mcs = odbReadBool("/Equipment/POL_ACQ/settings/input/send debug banks/send raw data bank",0,0);

  
  al_reset_alarm("sis_overflow");
  gSisBufferOverflow = 0;
  
  
  
  status = init_vme_modules();
  if (status != SUCCESS)
    return status;
  
  // temp .. set preset
  sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_ACQUISITION_PRESET, LNE_total); // select number of LNE       

  if(NumSisChannels > N_SCALER_MAX)
    {
      cm_msg(MERROR,"Begin_of_Run","Number of Scaler channels selected (%d) exceeds maximum permitted for POL (%d)",
	     NumSisChannels,N_SCALER_MAX);
      return DB_INVALID_PARAM;
    }
  for (i=0 ; i < NumSisChannels ; i++)
    {
      printf("scaler %d ", i);     
      if (scaler[i].ps != NULL)
	{
	  printf("free scaler %d\n", i);
	  free(scaler[i].ps);
	  scaler[i].ps = NULL;
	}  
      
      scaler[i].nbins = num_bins ;
      scaler[i].ps =  (DWORD *) malloc(sizeof(DWORD) * num_bins ); 
      printf("malloc scaler %d [%d] (%p)\n", i,  num_bins, scaler[i].ps);
      scaler_clear(i);
      printf("cleared scaler %d (%d)\n",i , scaler[i].nbins); 
    }

  clear_mcs_counters();
  
  
  /* cleanup previous booking
     realloc memory for histogram(s) */
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      // CumSum[i]=0;
      if (histo[i].ph != NULL)
	{
	  printf("free histo %d\n", i);
	  free(histo[i].ph);
	  histo[i].ph = NULL;
	}
      histo[i].nbins = num_bins;
      histo[i].ph = (DWORD *) malloc(sizeof(DWORD) * num_bins);
      if(histo[i].ph == NULL)
	{
	  cm_msg(MERROR,"begin_of_run","could not allocate space for histogram storage");
	  return DB_INVALID_PARAM;
	}
      
      // if(debug)
	printf("malloc histo %d [%d] (%p)\n", i, (num_bins), histo[i].ph);
      histo_clear(i);
      //  if(debug)
	printf("cleared histo %d (%d)\n",i , histo[i].nbins);
    }
  
  /* cleanup previous booking for dacsum */
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      if (dacsum[i].ptr != NULL)
	{
	  printf("free dacsum %d\n", i);
	  free(dacsum[i].ptr);
	  dacsum[i].ptr = NULL;
	}
    }
  
  if (ps.input.sis3820.sis_mode > 0 )  // REAL mode
    {
      
      // allocate space for histo sums at each DAC step (dac_ninc is read in DAC_set_scan_params);
      for (i=0 ; i < N_HISTO_MAX ; i++)
	{
	  dacsum[i].ninc = dac_ninc; // number of DAC increments
	  dacsum[i].ptr = (double *) malloc(sizeof(double) * dac_ninc);
	  if(dacsum[i].ptr == NULL)
	    {
	      cm_msg(MERROR,"begin_of_run","could not allocate space for dacsum storage");
	      return DB_INVALID_PARAM;
	    }
	  // if(debug)
	    printf("malloc dacsum %d [%d] (%p)\n", i, dac_ninc, dacsum[i].ptr);
	  dacsum_clear(i);
	  // if(debug)
	    printf("cleared dacsum %d (%d)\n",i , dacsum[i].ninc);
	}
    } // end of real mode
  else
    dac_ninc = 0;

 
  
  // Clear the counters
  gbl_DAC_n = dac_ninc; // set for incr_DAC to start a new sweep
  gbl_SCAN_n = 0; // incr_DAC will increment this
  gbl_histo_bin_cntr=0; // histogram bin counter
  gbl_CYCLE_n = 0; // incremented by cycle_start
  gbl_SCYCLE_n = 1; // starting supercycle 1
  gbl_his_cntr=gbl_cycle_cntr=0;
  gbl_skip_cycle_cntr=0;
  gbl_skip_cycle=FALSE; 
  gettimeofday(&eoct,NULL);


  if(stop_run_flag)
    {
      printf("begin_of_run: Waiting for run to stop\n");
      return status;
    }

  printf("begin_of_run: calling incr_DAC\n");
  
 
   status = incr_DAC(); // set DAC to first value and sets waiteqp[DAC] true

  if(status != SUCCESS)
    {
      printf("begin_of_run: error from incr_DAC\n");
      return status;
    }
 

  cycle_start_delay = FALSE;
  if (ps.input.sis3820.sis_mode==0)
	cycle_start_delay=TRUE;
  if ( !ps.input.ppg_external_start)
    cycle_start_delay=TRUE;

  bin_zero=num_bins;
  cycle_start(); // start the cycle
  bor_flag = 0; // turn off flag
  gbl_in_tol = 1; // in tolerance by default
  
  watchInterval = 25;
  printf("End of begin-of-run  (debug=%d)\n\n",debug); 
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
 
  printf("end_of_run: starting\n");

  if (fe_flag)  // called from frontend_init; vme not initialized
    return SUCCESS;

  gRunNumber = run_number;

  static bool gInsideEndRun = false;
  watchInterval = 100;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  printf("end run %d\n",run_number);
  disable_trigger(); // stop the PPG


  dac_val=0.0;
  set_DAC(gVme, 0.0); // set DAC to zero
  gInsideEndRun = false;


  fflush(stdout);
  printf("end_of_run: done\n");
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gRunNumber = run_number;
  disable_trigger();
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gRunNumber = run_number;
  resume_flag=1;
  enable_SIS();
  enable_PPG();
  resume_flag=0;
  return SUCCESS;
}

static int gVmeIsIdle = 0;
static unsigned int lacq_count = 0;  // store last value of acq_count
//DWORD my_time;
int stuck;

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  INT status;


#ifdef HAVE_PPG
  watchrabbit++;
  if(watchrabbit % watchInterval == 0)
    set_watchdog();

  if (gVmeIsIdle) 
    {	
      cm_yield(97);
      if (run_state == STATE_RUNNING)
	{ /*      RUNNING  */
	  if(gbl_IN_CYCLE)
	    {
              lacq_count = acq_count; // remmeber previous count
	      // Check whether we have reached num_bins ( num_bins = LNE_preset)
	      acq_count = sis3820_RegisterRead(gVme,gMcsBase, SIS3820_ACQUISITION_COUNT); // global

	      if(hot_debug)
		printf("frontend_loop:  acq_count, prevcount, num_bins, LNE_total  = %d,%d,%d,%d\n",
		      acq_count,lacq_count,(int)num_bins,(int)LNE_total);


	      if ( (acq_count > 0 ) && (acq_count >= (unsigned int) LNE_total))
		{ // At end of PPG supercycle

		  // Stop the PPG from restarting using real fp input
                  disable_trigger();

		  gbl_IN_CYCLE = FALSE; // PPG supercycle is done
		  flush=TRUE; // flush the last data from SIS
		  printf("frontend_loop: supercycle %d  at DAC value %f finished (last wait_cntr %d)\n", gbl_SCYCLE_n, dac_val, wait_cntr );
                  wait_cntr=0;

                  

		  
		  if(!stop_run_flag)
		    {
		      // Set DAC to next value while reading out the data
                      
		      //if(hot_debug) 
			printf("frontend_loop: calling incr_DAC...");
			status = incr_DAC(); // sets waiteqp[DAC]=TRUE;  will stop run if fails
		    }
		
		  gettimeofday(&eoct,NULL);
		  
		  waiteqp[CYCLE]=TRUE; // decode and send out the cycle scalers for this cycle
		  
		  if(hot_debug)
		    printf("\nfrontend_loop: end-of-cycle acq_count=%d; cycle %d supercycle %d  \n",
			   (int)acq_count,gbl_CYCLE_n,gbl_SCYCLE_n);


	       
		  if(wrote_stuck_msg)
		    { // clear message if it has been written
		      cm_msg(MINFO,"frontend_loop","PPG no longer stuck waiting for external trigger");
		      wrote_stuck_msg=0;
		    }
		}
	      else
		{    // not at end of PPG SuperCycle
                     // check for stuck PPG (external trigger) or restart PPG (internal trigger)
		  if ( (acq_count > 0 ) && acq_count == lacq_count)
		    { // PPG stuck
		      if(ps.input.ppg_external_start)
			{      // external start
			  if(!wrote_stuck_msg)
			    {
			      cm_msg(MERROR,"frontend_loop"," PPG may be stuck waiting for external trigger at Cycle %d SC %d (acq_count=%d  LNE_total= %d)",
				     gbl_CYCLE_n,gbl_SCYCLE_n, acq_count, (int)LNE_total);
			      wrote_stuck_msg=1;
			    }
			  
			} // end of external start
		      else
			{ // internal start
			  check_ppg_running(); // updates global ppg_running
			  if(!ppg_running) // PPG is stopped
			    {
			      /* start the PPG  */ 
    
#ifdef NEW_PPG
			      TPPGStartSequencer( gVme, gPpgBase);
#else
			      VPPGStartSequencer( gVme, gPpgBase);
#endif      

			      //if(hot_debug)
			      printf("Frontend_loop: restarted PPG by software for next cycle\n");
			    }

			} // end of internal start
	  
		    } // end of PPG stuck
		  else
		    { // PPG is not stuck
		      if(wrote_stuck_msg)
			{
			  cm_msg(MINFO,"frontend_loop","PPG no longer stuck waiting for external trigger");
			  wrote_stuck_msg=0;
			}
		    }

		} // end of check for stuck PPG
	    }
	  else // not in cycle
	    {
	      /* wait until other relevent  equipment have run */
	   
	      if (waiteqp[CYCLE]  || waiteqp[MCSC] )
		{
		  if(hot_debug)
		    printf("Waiting for CYCLE:%d;  and/or raw MCS0:%d banks to be sent out\n",
			   waiteqp[CYCLE],   waiteqp[MCSC] );

		  return SUCCESS; /* readout is not yet finished i.e. all equipments have not run */
		}
   
		if(stop_run_flag)
		  { // ... but we are waiting to stop
		    if(hot_debug)  
		      printf("NOT restarting cycle... waiting for run to stop\n");
		    return SUCCESS;
		  }

		//	cm_yield[100];
		if (waiteqp[DAC])
		  {
                    if(check_DAC_changed(gVme)==0 ) // not changed
		      {
			wait_cntr++;
                 
			printf("Waiting for DAC:%d counter:%d \n",waiteqp[DAC],wait_cntr);
		   
			if(wait_cntr > 300)
			  {
			    cm_msg(MERROR,"frontend_loop","Cannot set DAC to correct value");
			    stop_run();
			    return SUCCESS;
			  }
			return SUCCESS; // try again
		      }
		    else
		      waiteqp[DAC]=0;  // DAC has changed
		  } // end of waiteqp[DAC]
		
		
		// new supercycle
		gbl_SCYCLE_n++; // increment supercycle counter
		gbl_cycle_cntr = 0; // clear cycle cntr (counts cycles within supercycle i.e. per DAC value)
		enable_SIS(); // enable hardware
		enable_PPG(); // start the PPG by software
		
		// Start a new PPG supercycle 
                if(hot_debug)
		  printf("frontend_loop: starting next supercycle\n");
		cycle_start(); // start a new cycle...
	    }
	} // end of running 
    }
#endif  
  return SUCCESS;
}

INT cycle_start(void)
{
  struct timeval now;
  float ttmp;
  double td;

  // start a new supercycle 
  
  //printf("cycle_start: starting\n");

  gbl_rd=gbl_buf_index=0;
  my_dac_val = dac_val;
  gbl_data_buffer[0]=(int)(dac_val*1000);  // store current DAC value
  gbl_buf_index++; // increment for dac value

  lacq_count = acq_count = 0;
  
  if (!bor_flag  &&  cycle_start_delay)  // FALSE if using external PPG start  (in which case PPG itself delays the start)
    {
      // delay the start of the cycle
      gettimeofday(&now,NULL);
      td = timeDiff(now, eoct);
      //printf("cycle_start: time difference %f\n",td);
      ttmp = ps.input.dac_sleep_time__ms_ - (1000 * td); // we've already waited td seconds for data banks
     
      if(ttmp > 0.0)
	{
	  printf("sleeping %f ms to simulate DAC changing\n",ps.input.dac_sleep_time__ms_);
	  ss_sleep((INT)ttmp);
	}
    }    
 

  if(hot_debug)
    { 
	printf("cycle_start: Cycle %d  SCycle %d  Cyc %d DAC %.2fV Sweep %d (Cycles Skipped : %d) \n", 
	       gbl_CYCLE_n,gbl_SCYCLE_n,gbl_cycle_cntr, dac_val, gbl_SCAN_n,gbl_skip_cycle_cntr);
    }
  flush=FALSE; // reset flush
  
  gbl_IN_CYCLE=TRUE;
  waiteqp[FIFO]=TRUE; // waiting for scaler data to be read
  /* Cycle process sequencer */
  if(bor_flag)enable_SIS();
  enable_PPG(); //  restarts PPG if NOT free-running
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  assert(!"Not implemented");
}

/*-- Event readout -------------------------------------------------*/ 
static const int kMaxNumSisChannels = 32; // index of arrays; 32 channels max
static uint32_t  gSumMcsEvents; // sum the number of events
//static uint32_t  gMaxMcs[kMaxNumSisChannels];  // max value of SIS channels
//static uint64_t  gSumMcs[kMaxNumSisChannels];  // sum SIS channels
//static uint32_t  gSaveMcs[kMaxNumSisChannels]; // sampled SIS data
static uint32_t wc = 0;


static double timeDiff(const struct timeval&t1,const struct timeval&t2)
{
  return (t1.tv_sec - t2.tv_sec) + 0.000001*(t1.tv_usec - t2.tv_usec);
}

static struct timeval gSisLastRead;

//static int lastmsg=0;

static int have_SIS_data(void)
{
  struct timeval now;
  

  int haveData = sis3820_DataReady(gVme,gMcsBase);
  if (haveData == (-1))
    haveData = 0;

  int minread  = 1000*32;

  gettimeofday(&now,NULL);

  double td = timeDiff(now, gSisLastRead);

  //printf("sis: td: %f, haveData: %d\n",td,haveData);

  if(!flush)
    {
      if (td < 1.0 && haveData < minread)
	return 0;
    }
  //int toRead = minread;
  //int toRead = 128*1024/4;
  int toRead = max_event_size/4 - 5000;

  if (toRead > haveData)
    toRead = haveData;
  if (0)
    if (toRead & 0x1F)
      printf("odd available data: %d (%d)\n",toRead,toRead&0x1F);

  if (!flush) // flush last data words
    toRead = toRead & 0xFFFFFFE0;

 
  // if(haveData>0 || toRead>0)  
  //  {
      //  if(hot_debug)
      //	printf("have_SIS_data:  haveData=%d flush=%d toRead=%d\n",haveData,flush,toRead) ;
  //   }
    
  if (toRead == 0)
    return 0;
  
  return toRead;
}

static int read_SIS(char* pevent,  int toRead32)
{
  
  // called from read_vme_event

  //if (toRead32*4 < 20000)
  //  return 0;
  //
  //toRead32 = 32*1;

 
  if(debug)printf("read_SIS: starting with toRead32=%d\n",toRead32);
  int used = bk_size(pevent);
  int free = max_event_size - used;
  if (toRead32*4 > free)
    {
      printf("read_SIS: buffer used %d, free %d, toread %d  (in bytes %d)\n", used, free, toRead32, toRead32*4);
      toRead32 = (free-10*1024)/4;
      printf("read_SIS: 1 toRead32= %d\n",toRead32);
      toRead32 = toRead32 & 0xFFFFFFE0;
      printf("read_SIS: 2 toRead32= %d\n",toRead32);
      if (toRead32 <= 0)
	{
	  printf("read_SIS: nothing to read  - returning\n");
	  return 0;
	}
    }

  //int maxDma = 256*1024;
  int maxDma = 64*1024;
  if (toRead32*4 > maxDma)
    toRead32 = maxDma/4;

  gettimeofday(&gSisLastRead, NULL);


  /* create data bank */
  uint32_t *pdata32;

  char bankname[] = "MCS0";  // raw data
   
  bk_create(pevent, bankname, TID_DWORD, &pdata32);

  if(hot_debug)
    printf("read_SIS: read %d 32-bit words\n",toRead32);

  *pdata32  = (uint32_t)(my_dac_val*1000);  // put current DAC value as first word of every MCS0 bank
  pdata32++;

  int rd = sis3820_FifoRead(gVme,gMcsBase,pdata32,toRead32);
  if(hot_debug)
    printf("read_SIS: to read %d 32-bit words;  number read is rd=%d\n",toRead32,rd);
  gbl_rd+=rd;
  wc += toRead32*4;
  
  if (debug) // dump first four words 
    printf("sis3820 32-bit data (first 4 data words): 0x%x 0x%x 0x%x 0x%x\n",pdata32[0],pdata32[1],pdata32[2],pdata32[3]);

  
  for (int j=0; j<rd; j++)
      gbl_data_buffer [gbl_buf_index+j] = pdata32[j+1];
 
  gbl_buf_index+=rd;
   

  //printf("read_SIS: pevent= %p \n",pevent);
  if(send_mcs)
    bk_close(pevent, pdata32 + rd +1);  // send out MCS0 data bank (raw data)  add 1 for DAC Set Value
  else
    bk_close(pevent,pdata32);  // don't send MCS0

 
  // decoding scaler data was here; moved to read_mcs_event


  if (send_mcs)
    return 1;
  else
    return 0;
}

INT decode_scaler_data(void)
{
  // called from read_mcs_event at end of supercycle
  uint32_t *pdata32;
  int toRead32;
  int i,k;
  uint16_t data[2];
  uint8_t  data8[4];
  int db = data_bytes;
  

  // currently only supports 16-bit data

  int total_event_length = (NumSisChannels * db)/4 * num_bins;
  if(hot_debug)printf("length of data for one cycle is  %d 32-bit words\n",total_event_length);

  if(debug)
      printf("decode_scaler_data: gbl_buf_index=%d  gbl_rd=%d\n",gbl_buf_index,gbl_rd);

  gbl_first_word = (float) (gbl_data_buffer[0]/1000);  // DAC value for this data
  if(hot_debug)
    {
      printf("decode_scaler_data: first word is DAC value= %f; current dac_val is %f\n",((float)gbl_data_buffer[0]/1000),(float)dac_val );
      printf("data length is gbl_rd=%d\n",gbl_rd);
    }


  if(debug)
    {  /* print scaler data */
      printf(" 0001 ->            "); 
      for(i=1; i <= gbl_buf_index; i++)
	{
	  if(i%8 ==0 )
	    printf("\n %4.4d -> ",i);
	  
	  printf(" 0x%8.8x",gbl_data_buffer[i]);
	}
      printf("\n last i=%d \n",i);
    }

  int istart=1; // skip DAC value
  if(ps.input.discard_first_cycle) // in real mode, first cycle probably not valid
    {  
      // ignore the first event in the buffer;
      //printf("index to first valid event is %d; data is 0x%x\n", (total_event_length +1), gbl_data_buffer[total_event_length +1]);
      istart=total_event_length +1;
      gbl_skip_cycle_cntr++; // skipped first cycle
    }

  
  int event_bytes = NumSisChannels * db;   // per time bin
  int numEvents = gbl_rd * 4 /event_bytes; 

 

  if(debug)
    {
      /* 
	 Debug...  print data as cycles  
      */
      int index=1; // skip DAC set value
      int l= (NumSisChannels * db)/4  ;
      printf("Bin  Inputs 1,0 etc.  NumSisChannels=%d  l=%d \n",NumSisChannels,l);
      int j=0;
      int idx=0;
      int cycle=1;
      printf("Cycle %d\n",cycle);
      
      for( i=index; i < gbl_buf_index; i++)
	{
	  if(idx%l ==0 )
	    {
	      if(j == 1 ) 
		printf("... discard bin 0");
	      if(j==num_bins)
		{
		  cycle++;
		  printf("\n Cycle %d  at index %d\n", cycle, (i+1));
		  j=0;
		}
	      printf("\n Time Bin %d  -> ",j);
	      j++;
	    }
	  printf(" 0x%8.8x",gbl_data_buffer[i]);
	  idx++;
	}
      printf("\n");

    }      /*  end of debug */

  uint32_t *mptr = &gbl_data_buffer[istart];  // point to first valid event
  uint32_t *eptr = &gbl_data_buffer[gbl_buf_index];  // pointer to end of buffer
 
  if(debug)printf("decoding starting at index istart=%d contents= 0x%x\n",istart,*mptr);

  gbl_histo_bin_cntr=num_bins+1; // will start at time bin zero
  gbl_cycle_cntr=0;

  // decode the data (16-bit only at present)
  

  while (mptr < eptr)   // for the whole buffer
    {
      gSumMcsEvents++;  // number of LNE Events
      
      if(gbl_histo_bin_cntr >= num_bins)
	{
	  gbl_histo_bin_cntr=0; // bin 0  - next PPG  cycle
          if(hot_debug)printf("gbl_histo_bin_cntr set to 0;  gbl_his_cntr=%d  \n", gbl_his_cntr );
	}
      if(debug)
	printf("\n  Working on time bin %d  of cycle %d within supercycle %d \n",gbl_histo_bin_cntr,gbl_cycle_cntr,  gbl_SCYCLE_n);

      for ( i=0; i<NumSisChannels/2; i++) // use number of channels actually enabled 
	{
	  int j = i*2;
	  
	  uint32_t v = *mptr++;
	  //printf("Working on data word i=%d j=%d v=0x%x  (bin=%d) \n",i,j,v,gbl_histo_bin_cntr);
	  data[0] = v & 0xFFFF;
	  data[1] = (v & 0xFFFF0000) >> 16;
	  for (k=0; k<2; k++)
	    {
	      // check the pointer
	      if(scaler[j+k].ps == NULL)
		{
		  cm_msg(MERROR,"read_SIS","Error - scaler pointer is null for index %d. Cannot histogram",(j+k));
		  return 0;
		}

	      if (gbl_histo_bin_cntr > 0 ||  ps.input.discard_first_bin == FALSE)
		{
		  scaler[j+k].sum_cycle += data[k];
		  // if(data[k] !=0) printf("added data[%d]=%d to scaler[%d].sum_cycle -> %f\n",
		  //			 k,data[k],(j+k), scaler[j+k].sum_cycle);
		}

	    
	      if( ps.input.discard_first_bin)
		{
		  if(gbl_histo_bin_cntr > 0 )
		    {
		      scaler[j+k].ps[gbl_histo_bin_cntr-1] += (DWORD) data[k];
		      //  if(data[k] !=0) printf("added data[%d]=%d to scaler[%d].ps[%d] -> %d\n",
		      //		     k,data[k],(j+k),(gbl_histo_bin_cntr-1), scaler[j+k].ps[gbl_histo_bin_cntr-1]);
		    }
		}
	      else
		{
		  scaler[j+k].ps[gbl_histo_bin_cntr] += (DWORD) data[k];
		  // if(data[k] !=0) printf("added data[%d]=%d to scaler[%d].ps[%d] -> %d\n",
		  //		 k,data[k],(j+k),gbl_histo_bin_cntr, scaler[j+k].ps[gbl_histo_bin_cntr]);
		  
		}
	    } // end for k loop
	} // end of number of SIS channels loop

      gbl_histo_bin_cntr++;  // next time bin

      if(gbl_histo_bin_cntr >= num_bins)   // End of 1 cycle
	{
	  gbl_his_cntr++;   // count number of complete cycles
          gbl_cycle_cntr++; // next cycle within the supercycle
	 
	  if(hot_debug)printf("Histogrammed total of %d cycles ;  %d cycles were skipped; gbl_cycle_cntr=%d \n",
			      gbl_his_cntr, gbl_skip_cycle_cntr, gbl_cycle_cntr);
	  
	  process_histo();  // increments gbl_CYCLE_n
	  clear_mcs_counters(); // clear cycle scalers
	}
    } // while

  if(debug)
    printf(" decode_scaler_data: setting waiteqp[FIFO] false \n");
  waiteqp[FIFO]=FALSE;


 // End of processing supercycle
  gbl_buf_index=0; //  reset index read for next supercycle's data


  if(dac_ninc > 0 ) // real mode
    {
      for(i=0;  i<NumSisChannels; i++)
	{ 
	  dacsum[i].ptr[gbl_DAC_n-1] =  (double)histo[i].sum;
	  //printf("decode_scaler_data: ch %d dacsum at dacstep %d = %f\n",i,(gbl_DAC_n-1), (double)histo[i].sum);
	}
    }

   if(hot_debug)
    printf("decode_scaler_data: setting waiteqp[HISTO1] and [HISTO2] true\n");


  waiteqp[HISTO1]=TRUE; // histo banks (not to ODB)
  waiteqp[HISTO2]=TRUE; // send out histo sum banks  (to ODB as well - run statistics )
  //  histo storage cleared after sending BOTH sets of banks
  
  return SUCCESS;
}




INT  process_histo(void)
{
  INT status,i;
  DWORD j;

  // called from decode_scaler_data which is called at end of supercycle
  // processes scaler data from each PPG cycle into a histo

  // fill histogram

  if( waiteqp[HISTO1])
    printf("process_histo: HISTO1 is true; previous histos not sent\n");
  if( waiteqp[HISTO2])
    printf("process_histo: HISTO2 is true; previous histos not sent\n");
  
  // add the cycle scaler data to the cumulative histogram
  for (i=0; i<NumSisChannels; i++)
    {
      for (j=0; j<num_bins; j++)
	histo[i].ph[j] += scaler[i].ps[j];
	  
      histo[i].sum += scaler[i].sum_cycle;
      if(debug)printf("histo[%d].sum = %f;   scaler[%d].sum_cycle=%f\n",i, histo[i].sum, i, scaler[i].sum_cycle);
    }
    
  gbl_CYCLE_n++;  // next PPG cycle to histogram (starts at 0)

  gbl_histo_bank_data[0] = (float)gbl_CYCLE_n; // cycle number
  gbl_histo_bank_data[1] =  (float)gbl_SCYCLE_n; // supercycle number
  if(debug)
    {
      if(gbl_histo_bank_data[2] != (float)gbl_first_word)
	printf("process_histo: dac val has changed (gbl_cycle_cntr=%d expect 1). gbl_histo_bank_data[2]=%f gbl_first_word=%f\n",
	       gbl_cycle_cntr, (float)gbl_histo_bank_data[2],  (float)gbl_first_word);
    }
  gbl_histo_bank_data[2] = (float)gbl_first_word; // DAC set value when data was taken
  gbl_histo_bank_data[3] = 0 ; //  NOT USED
  gbl_histo_bank_data[4] = float(gbl_DAC_n -1);
  gbl_histo_bank_data[5]++; // increment counter for number of PPG cycles added   

  return SUCCESS;
}


INT new_supercycle(void)
{
  INT status;

  status=SUCCESS; // default

   if(hot_debug)
    printf("New Supercycle... gbl_cycle_cntr=%d gbl_CYCLE_n=%d num_cycles_per_supercycle=%d",
	 gbl_cycle_cntr,gbl_CYCLE_n, num_cycles_per_supercycle);
		   
  gbl_SCYCLE_n++; // increment supercycle counter
  gbl_cycle_cntr = 0; // clear cycle cntr (counts cycles within supercycle i.e. per DAC value)


  if(stop_run_flag)
    {
      printf("new_supercycle: NOT setting DAC... waiting for run to stop\n");
      return status;
    }
 
  waiteqp[SUPER]=FALSE;
  enable_SIS();
  return status;
}


INT read_histo_event(char *pevent, INT off)
{
  DWORD *pdata;
  float *pfloat;
  int i,j;
  DWORD nbins;

  // data NOT sent to ODB

  if(!waiteqp[HISTO1])
    return 0;

  if(debug)
    printf("read_histo_event: starting with waiteqp[HISTO1]=%d\n",waiteqp[HISTO1]);

  /* init bank structure */
  bk_init32(pevent);



 // Add CYCL bank here since banks are no longer sent every cycle
  bk_create(pevent, "CYCL", TID_FLOAT, &pfloat);


  const int size_CYCL = 10;  // increment if add an extra word to this bank
  for (i=0; i<size_CYCL; i++)
    pfloat[i] = 0.0;
     
  pfloat[0] = 1; // code to indicate scan type  1=DAC scan
  pfloat[1] = gbl_CYCLE_n; // Cycle number  i.e. number of PPG cycles
  pfloat[2] = gbl_SCYCLE_n; // Supercycle number
  pfloat[3] = gbl_cycle_cntr; // counts cycles within supercycle (max=num cycles per SC)
  pfloat[4] = gbl_SCAN_n; // Scan number (sweep number)
  pfloat[5] = gbl_skip_cycle_cntr; // Number of cycles skipped  
  pfloat[6] = gbl_his_cntr; // Number of cycles histogrammed 
  pfloat[7] = gbl_DAC_n; // DAC increment number
  pfloat[8] = dac_val; // DAC set value
  pfloat[9] = dac_read; // DAC readback value
  bk_close(pevent, pfloat+size_CYCL);



  bk_create(pevent, "HISI", TID_FLOAT, &pfloat);  // information
  for (i=0; i<ghbd_len; i++)  //gbl_histo_bank_data[ghbd_len]
    pfloat[i] = gbl_histo_bank_data[i];
  bk_close(pevent, pfloat+ghbd_len);
 
  char str[5];
  for (j=0; j<NumSisChannels; j++)
    {
      sprintf(str,"HIS%d",j);
      //printf("creating bank %s \n",str);
      bk_create(pevent, str, TID_DWORD, &pdata);  // Histograms for 1 supercycle
      nbins = histo[j].nbins;
      if( ps.input.discard_first_bin)
	nbins--;

      memcpy(pdata, histo[j].ph, nbins * sizeof(DWORD));
      pdata +=  nbins;
      bk_close(pevent, pdata);
    }


  if(hot_debug  ||  stop_run_flag)
    printf("read_histo_event: sent histo event of size %d\n",
	   bk_size(pevent));
  waiteqp[HISTO1] = FALSE;

  
  // Only clear histograms if histo sums have also been sent
  if ( !waiteqp[HISTO2])
    { // histo sums have also been sent
      for (i=0; i<N_HISTO_MAX; i++)
	histo_clear(i); // clear the histogram and the sums now they have been sent
	
      gbl_histo_bank_data[5]=0.0; //clear counter
    }
  return bk_size(pevent);   
}



INT read_sispulser_event(char *pevent, INT off)
{

#ifdef HAVE_SIS3820
   printf("read_sispulser_event:  Pulse SIS3820 user LED\n");

 
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_CONTROL_STATUS,1<<0);
  // issue a PCI read to flush posted PCI writes
  uint32_t d1 = sis3820_RegisterRead(gVme,gMcsBase,SIS3820_CONTROL_STATUS);
  //usleep(1);
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_CONTROL_STATUS,1<<16);
  // issue a PCI read to flush posted PCI writes
  uint32_t d2 = sis3820_RegisterRead(gVme,gMcsBase,SIS3820_CONTROL_STATUS);
  return 0*(d1+d2);
#else
  return 0;
#endif
}

// New GPIB system ALSO sends a bank GPIB (length 4) with this information



INT read_mcs_event(char *pevent, INT off)
{
  uint32_t count; // readback of number of LNE received
  uint32_t preset; // readback preset reg
  int i;
  INT size,status;
  DWORD *pdata;


  if(gbl_IN_CYCLE) 
    return 0;  // wait for end of supercycle

  if(!waiteqp[CYCLE])
    return 0;

  // HISTOS must have been sent, or the histo sums will not have been cleared
  if(waiteqp[HISTO1] || waiteqp[HISTO2])
    {
      printf("read_mcs_event: waiting for previous histos HISTO1 (%d) or HISTO2 (%d) to be sent; ",waiteqp[HISTO1], waiteqp[HISTO2]);
      return 0;
    }

#ifdef HAVE_SIS3820
 
  int haveData = sis3820_DataReady(gVme,gMcsBase);
  if(hot_debug)printf("read_mcs_event: sis3820_DataReady returns haveData=%d  gbl_IN_CYCLE=%d, %d %d %d\n",
		      haveData,gbl_IN_CYCLE,waiteqp[CYCLE],waiteqp[HISTO1],waiteqp[HISTO2]);
  const int kSisBufferSize = 16000000;
  if (haveData > kSisBufferSize)
    if (!gSisBufferOverflow)
      {
        gSisBufferOverflow = 1;
        printf("read_mcs_event: data (%d) larger than buffer (%d)\n",haveData,kSisBufferSize);
        al_trigger_alarm("sis_overflow", "SIS buffer overflow: LNE rate is too high",
                         "Warning", "", AT_INTERNAL);
      }

 


  if(gbl_histo_bin_cntr != 0 && gbl_histo_bin_cntr != num_bins)
    {
      printf("read_mcs_event: expect gbl_histo_bin_cntr (%d) if non-zero to be equal to num bins (%d)\n",
	     gbl_histo_bin_cntr,num_bins);
      return 0;
    }

  
  decode_scaler_data();  // decodes raw scaler data and fills histograms for SC


  float *pfloat;

 /* init bank structure  (SisHistory event) */
  bk_init32(pevent);
 
  
  count = sis3820_RegisterRead(gVme,gMcsBase, SIS3820_ACQUISITION_COUNT);
  preset = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_ACQUISITION_PRESET); // debug 



  bk_create(pevent, "DBUG", TID_FLOAT, &pfloat);
  const int size_DBUG = 9;  // increment if add an extra word to this bank
  for (i=0; i<size_DBUG; i++)
    pfloat[i] = 0;
  
  pfloat[0] = haveData;         // amount of data still buffered in the SIS
  pfloat[1] = (float)last_gSumMcsEvents;  // number of LNE events from the SIS (before being cleared)
  pfloat[2] = count;  // LNE count from reg SIS380_ACQUISITION_COUNT
  pfloat[3] = preset;  // LNE_preset value from register
  pfloat[4] = num_bins;  // num_bins; actual number of bins sent out (may be 1 more than requested number)
  pfloat[5] = (float)data_bytes;  // 2 for 16-bit data
  pfloat[6] = (float)NumSisChannels; // number of Scaler Channels enabled 
  pfloat[7] = (float)ps.input.discard_first_bin;
  pfloat[8] = (float)ps.input.discard_first_cycle;

  bk_close(pevent, pfloat+size_DBUG);
  

  bk_create(pevent, "CYCL", TID_FLOAT, &pfloat);
  const int size_CYCL = 10;  // increment if add an extra word to this bank
  for (i=0; i<size_CYCL; i++)
    pfloat[i] = 0.0;
  
  pfloat[0] = 1; // code to indicate scan type  
  pfloat[1] = gbl_CYCLE_n; // Cycle number  i.e. number of good cycles
  pfloat[2] = gbl_SCYCLE_n; // Supercycle number
  pfloat[3] = gbl_cycle_cntr; // counts cycles within supercycle (max=num cycles per SC)
  pfloat[4] = gbl_SCAN_n; // Scan number (sweep number)
  pfloat[5] = gbl_skip_cycle_cntr; // Number of cycles skipped  
  pfloat[6] = gbl_his_cntr; // Number of cycles histogrammed (should be same as gbl_CYCLE_n )
  pfloat[7] = gbl_DAC_n; // DAC increment number
  pfloat[8] = dac_val; // DAC set value 
  pfloat[9] = dac_read; // DAC read back value
  bk_close(pevent, pfloat+size_CYCL);

  waiteqp[CYCLE] = FALSE;

  if(debug)printf("read_mcs_event: sent CYCL event of size %d\n",bk_size(pevent));
    return bk_size(pevent);
#else
  return 0; // prevent creation of this event
#endif
}


INT read_hsum_event(char *pevent, INT off)
{
  float *pfloat;
  double *pdouble;
  int i,j;
  char str[5];

  // this could be part of read_histo_event except it is sent to the ODB

  if(!waiteqp[HISTO2])
    return 0;

  if(debug)printf("read_hsum_event: starting\n");
  /* init bank structure */
  bk_init(pevent);

  bk_create(pevent, "HSSI", TID_FLOAT, &pfloat);  // information
  for (i=0; i<ghbd_len; i++)  //gbl_histo_bank_data[ghbd_len]
    pfloat[i] = gbl_histo_bank_data[i];
  bk_close(pevent, pfloat+ghbd_len);

  bk_create(pevent,"HSUM",TID_FLOAT,&pfloat);
  if (debug)printf("Histo sums per channel : ");
  for (i=0; i<NumSisChannels; i++)
    {
      if(debug) printf(" %f ",histo[i].sum);
      pfloat[i] = (float)histo[i].sum;	
    }
  if(debug)printf("\n");
  bk_close(pevent, pfloat+NumSisChannels); // was kMaxNumSisChannels
    
  // sums of histogrammed channel at each DAC step
  for (j=0; j< NumSisChannels; j++)
    {
      sprintf(str,"DSS%d",j);
      bk_create(pevent, str, TID_DOUBLE, &pdouble);
      
      //  memcpy(pdouble, dacsum[j].ptr, (dacsum[j].ninc * sizeof(double)) );
       memcpy(pdouble, dacsum[j].ptr, (gbl_DAC_n * sizeof(double)) ); // copy only the filled values
       // DWORD ii;
       // if(j==3)
       //	{
       //  printf("read_hsum_event:  dacsum[3].ninc = %d\n",dacsum[3].ninc);
       //  for (ii=0; ii<gbl_DAC_n; ii++)
       //   printf("pdouble[%d] = %f\n",ii,pdouble[ii]);
       //	}
      // pdouble += dacsum[j].ninc ; 
       pdouble += gbl_DAC_n ;
      bk_close(pevent, pdouble);
    }
  


  if(hot_debug || stop_run_flag)
    printf("read_hsum_event: sent histo sums event of size %d\n",bk_size(pevent));
  waiteqp[HISTO2] = FALSE;

  if(debug)
    printf("read_hsum_event:waiteqp[HISTO1]=%d waiteqp[HISTO2]=%d\n",waiteqp[HISTO1],waiteqp[HISTO2]);
  
  if ( !waiteqp[HISTO1])
    { // histos have also been sent
      for (i=0; i<N_HISTO_MAX; i++)
	histo_clear(i); // clear the histogram and the sums now they been sent
      gbl_histo_bank_data[5]=0.0; //clear counter
    }
  return bk_size(pevent); 
}


extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  //printf("poll_event %d %d %d!\n",source,count,test);

  for (int i = 0; i < count; i++)
    {
      //udelay(1);
      if (test)
        xusleep(1000);
      else
        return TRUE;
    }
  return 0;

#if 0
  for (int i = 0; i < count; i++)
    {
      //udelay(1);
      int lam = 0;
      lam |= have_ADC_data();
#ifdef HAVE_SIS3820
        lam |= have_SIS_data();
#endif

      if (lam)
        if (!test)
          return lam;
    }
  return 0;
#endif
}

INT read_vme_event(char *pevent, INT off)
{
  static int gCountEmpty = 0;
  int size = 0;

#ifdef HAVE_SIS3820
  int count;


  // SIS has how much data?
  
  count = have_SIS_data();
  
  // printf("read_vme_event:  SIS  count=%d, fifo %d  flush=%d\n",  count, sis3820_DataReady(gVme,gMcsBase),flush);
  
  if (count > 0)
    {
      if (!size)
	bk_init32(pevent);
      size |= read_SIS(pevent, count); 
      // printf("read_vme_event: after read_SIS size=%d\n",size);
    }
      
#endif  // SIS3820
    


  if (size)
    {
      gCountEmpty = 0;
      gVmeIsIdle = 0;
      if(debug)
	printf("read_vme_event %d bytes\n", bk_size(pevent));
      return bk_size(pevent);
    }
  

  gCountEmpty++;

  if (gCountEmpty > 2) {
    gVmeIsIdle = 1;
    xusleep(1000);
  }
  
  return 0;
}



#ifdef HAVE_PPG

INT prestart(INT run_number, char *error) 
{ 
  struct stat stbuf; 
  int size,status; 
  time_t timbuf; 
  char timbuf_ascii[30]; 
  int elapsed_time; 
  char str[80];
  int i;

  gbl_IN_CYCLE=FALSE;

  printf("prestart: starting\n");

  /* Get current  settings */
  size = sizeof(ps);
  status = db_get_record(hDB, hTASet, &ps, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "prestart", "cannot retrieve /Equipment/Pol_Acq/Settings record (size of ps=%d)",size);
      return DB_NO_ACCESS;
    }


  printf("sismode = %d\n",ps.input.sis3820.sis_mode); 
  printf("requested bins = %d\n",ps.input.num_bins_requested); // requested_bins

  // read globals ppgmode sismode, num_bins, min_delay

  num_bins = ps.input.num_bins_requested; 
  if( ps.input.discard_first_bin)
     num_bins++; // add one extra bin so we can discard first bin

 // Write actual number of time bins to output for display
  ps.output.num_bins_per_cycle = num_bins;
  status = db_set_value(hDB, hOut, "num bins per cycle", &ps.output.num_bins_per_cycle, sizeof(INT), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"num bins per cycle\" (%d)",status); 
      return status;
    }
 
  num_cycles_per_supercycle = odbReadInt("/Equipment/POL_ACQ/settings/input/Num cycles per supercycle",0,1);
  printf("number of cycles/supercycle requested: %d\n",num_cycles_per_supercycle);
  if(num_cycles_per_supercycle < 1) 
    num_cycles_per_supercycle =1; // minimum value

  ps.output.num_cycles_per_sc = num_cycles_per_supercycle;
  if(ps.input.discard_first_cycle) // in real mode, first cycle probably not valid
    {
      LNE_total = num_bins  * (num_cycles_per_supercycle +1); // throw away first cycle
      ps.output.num_cycles_per_sc = num_cycles_per_supercycle +1;
    }
  else
    LNE_total = num_bins  * num_cycles_per_supercycle; 

 // Write number of cycles/supercycle to output for display
  status = db_set_value(hDB, hOut, "num cycles per sc", &ps.output.num_cycles_per_sc, sizeof(INT), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"num cycles per sc\" (%d)",status); 
      return status;
    }

  // Write number of valid time bins to output for display
  ps.output.valid_bins_per_sc = ps.input.num_bins_requested * num_cycles_per_supercycle;
  status = db_set_value(hDB, hOut, "valid bins per sc", &ps.output.valid_bins_per_sc, sizeof(INT), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"valid bins per sc\" (%d)",status); 
      return status;
    }
  
  // Write this to LNE_preset 
  sprintf(str,"input/sis3820/LNE preset");
  status = db_set_value(hDB, hTASet, str, &LNE_total, sizeof(LNE_total), 1, TID_DWORD);  
  if(status !=SUCCESS)
    {
      cm_msg(MERROR,"prestart","cannot set ODB key  ...%s to %u (%d)",str,LNE_total,status); 
    }
  ps.input.sis3820.lne_preset = LNE_total;
  
  if(ps.input.sis3820.sis_mode == 0)
    {
      if( num_bins <= 0)
	{
	  cm_msg(MERROR,"prestart","Illegal number of bins (%u)",num_bins);
	  return  DB_INVALID_PARAM;
	}
      return SUCCESS; // no more checks for SIS test mode (no PPG)
    }

  // malloc a buffer to store the data from 1 supercycle
  int db=data_bytes;
  if (db == 3) db = 4;  // 24-bit data uses 32 bits per word
  int size_32 = (LNE_total * NumSisChannels * db/4) + 500 ;  // size in 32-bit words plus 500 for luck
  if(gbl_data_buffer != NULL)
    free (gbl_data_buffer);
  gbl_data_buffer = (uint32_t *) malloc (sizeof(uint32_t) * size_32);
  if(gbl_data_buffer != NULL)
    printf("malloc data buffer of size %d words\n",size_32); 
  else
    {
      cm_msg(MERROR,"prestart","malloc for %d words fails for data buffer", size_32);
      return  DB_INVALID_PARAM;
    }
  gbl_buf_index=0;



  status = check_PPG_input_params(); // check for valid PPG parameters
  if (status !=SUCCESS)
    return  DB_INVALID_PARAM;

  printf("prestart: read DAC sleep time from odb as %f ms\n",ps.input.dac_sleep_time__ms_);
 
  // In real mode, PPG is restarted after each cycle by the end-of-TOF signal 

  // scan_loop_counter = 1; // scan loop count
  // for testing so can set PPG to run in loops
    
  printf("prestart: read scan_loop_counter from ODB as %d\n",ps.input.scan_loop_count);
  if (ps.input.scan_loop_count < 1)
   ps.input.scan_loop_count =1;
  if (ps.input.scan_loop_count != 1)
    cm_msg(MINFO,"prestart","Warning - scan loop counter (%d) should be 1 except for testing PPG",
	   ps.input.scan_loop_count);
       
  
  // set the DAC sleep time - this is included in PPG cycle
  status = set_dac_sleep_time();
  if(status != SUCCESS)
    return status;
  

  if(ps.input.run_tri_config) //  (POL expt does not run tri_config)
    { 
      // Set the scan loop count to  scan_loop_counter
      sprintf(str,"begin_scan/loop count");
      status = db_set_value(hDB, hPPG, str, &scan_loop_counter, sizeof(scan_loop_counter), 1, TID_INT); 
      if(status != DB_SUCCESS) 
	{ 
	  cm_msg(MERROR,"prestart","cannot set ODB key \"%s\" to %d  (%d)",str,scan_loop_counter, status); 
	  return status;
	}
    
      // run tri_config
      printf("prestart: sending system command:\"%s\" \n",cmd); 
      status =  system(cmd); 
 
      stat (ppgfile,&stbuf); 
      printf("prestart: file %s  size %d\n",ppgfile, (int)stbuf.st_size); 
      
      if(stbuf.st_size < 0) 
	{ 
	  cm_msg(MERROR,"prestart","PPG load file %s cannot be found",ppgfile); 
	  //set_equipment_status(equipment[PPG].name, "PPG param check","red");
	  return DB_FILE_ERROR ; 
	} 
 
      strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) ); 
      printf ("prestart: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size); 
 
 
      // get present time 
      time (&timbuf); 
      
      strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) ); 
      printf ("prestart: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf); 
      
      elapsed_time= (int) ( timbuf - stbuf.st_mtime ); 
      
      printf("prestart: time since ppg loadfile last updated: %d seconds \n",elapsed_time); 
      if(elapsed_time > 3) 
	{ 
	  cm_msg(MERROR,"prestart","PPG load file %s is too old",ppgfile); 
	  //set_equipment_status(equipment[PPG].name, "PPG param check","red");
	  return DB_FILE_ERROR ; 
	} 
    }
#ifdef NEW_PPG
  else
    {  // POL experiment modifies the ppg loadfile with the relevent parameters (e.g. number of bins, DAQ sleep time)
      status = modify_ppg_loadfile(ppgmode);
      if(status != SUCCESS)
	return status;
    }
#endif

  return SUCCESS; 
} 

 
 
INT poststart(INT run_number, char *error) 
{ 
  INT status; 
  char copy_cmd[132]; 
 

  if(ps.input.sis3820.sis_mode == 0)  // SIS test mode 
    return SUCCESS;

  if(run_state == STATE_RUNNING) 
    {
      if(ps.input.run_tri_config)
	{
	  //printf("data path: %s\n",data_path);  // data disk not mounted from lxpol
	  sprintf(copy_cmd,"cp %s/ppgload/bytecode.dat %s/ppgload/run%d_bytecode.dat &", 
		  ps.input.ppg_path ,ps.input.ppg_path,run_number); 
	  status =  system(copy_cmd);
	  if(status != 0)
	    cm_msg(MINFO,"poststart","error status after system command \"%s\" is %d",copy_cmd, status);
	}
#ifdef NEW_PPG      
      sprintf(copy_cmd,"cp %s/ppgload/ppgload.dat  %s/ppgload/run%d_ppgload.dat &", 
	      ps.input.ppg_path,ps.input.ppg_path,run_number); 
      status =  system(copy_cmd); 
      if(status != 0)
	cm_msg(MINFO,"poststart","error status after system command \"%s\" is %d",copy_cmd, status); 
    } 
#endif
  return SUCCESS; 
}
#endif // HAVE_PPG

INT post_stop(INT run_number, char *error) 
{
  INT i;
  printf("\n post_stop: fe_flag=%d\n",fe_flag);

  if (fe_flag)  // called from frontend_init; vme not initialized
    return SUCCESS;


  printf("post_stop: free scaler,  NumSisChannels =%d \n", NumSisChannels);
  // for (i=0 ; i <  NumSisChannels ; i++)
 for (i=0 ; i <  N_SCALER_MAX ; i++)
  {
    if (scaler[i].ps != NULL)
      {
	if(debug)	printf("freeing scaler %d [%d] (%p)\n", i, num_bins, scaler[i].ps); 
	free(scaler[i].ps);
	scaler[i].ps = NULL;
      }
    else
      if(debug)printf("scaler[i].ps is NULL, i=%d\n",i);
  }


  printf("free histo, N_HISTO_MAX =%d\n", N_HISTO_MAX);
  for (i=0 ; i <  N_HISTO_MAX ; i++)
  {
    if (histo[i].ph != NULL)
    {
      if(debug) 
	printf("freeing histo %d [%d] (%p)\n", i, num_bins, histo[i].ph);
      free(histo[i].ph);
      histo[i].ph = NULL;
    }
    else
      if(debug)printf("histo[i].ph is NULL, i=%d\n",i);
  }

 
 for (i=0 ; i < N_HISTO_MAX ; i++)
   {
     if(dacsum[i].ptr != NULL)
       {
	 if(debug) 
	 printf("freeing dacsum %d [%d] (%p)\n", i, dac_ninc, dacsum[i].ptr);
	 free(dacsum[i].ptr);
	 dacsum[i].ptr = NULL;
    }
    else
      if(debug)printf("dacsum[i].ptr is NULL, i=%d\n",i);
  }

 if(gbl_data_buffer != NULL)
   {
    if(debug)printf("freeing gbl_data_buffer  (%p)\n", gbl_data_buffer);
    free (gbl_data_buffer);
   }

  fflush(stdout);
  return SUCCESS;
}









INT DAC_set_scan_params(void)
{
  /* called from begin_of_run */
  INT status;
  float   dac_stop;       /* stop value for dac sweep (now calculated) */

  dac_ninc = ps.input.num_dac_incr;
  if( dac_ninc < 1)  // dac_ninc
    {
      cm_msg(MERROR,"DAC_set_scan_params","Invalid number of dac sweep steps: %d",
	     dac_ninc );
      return FE_ERR_HW;
    }



  if( fabs(ps.input.dac_inc) < fabs(min_DAC_incr))
    {
      cm_msg(MERROR,"DAC_set_scan_params","DAC scan increment value (%.2fV) must be greater than %.2fV",
	     ps.input.dac_inc,min_DAC_incr);
      return FE_ERR_HW;
    }

  dac_stop =  ps.input.dac_start + ( ps.input.dac_inc *  dac_ninc);

  if(debug)
    printf("DAC_set_scan_params: DAC start=%f dac_ninc=%d inc=%f; calculated dac_stop=%f\n",
	    ps.input.dac_start, dac_ninc, ps.input.dac_inc, dac_stop);



  if( (dac_stop > max_DAC || dac_stop < min_DAC))
    {
      cm_msg(MERROR,"DAC_set_scan_params",
	     "DAC stop value (%.2fV) is out of range (allowed range is %.2fV to %.2fV)",
	     dac_stop,min_DAC,max_DAC);
      return FE_ERR_HW;
    }
  if( (ps.input.dac_start > max_DAC || ps.input.dac_start < min_DAC))
    {
      cm_msg(MERROR,"DAC_set_scan_params",
	     "DAC start value (%.2fV) is out of range (allowed range is %.2fV to %.2fV)",
	     ps.input.dac_start,min_DAC,max_DAC);
      return FE_ERR_HW;
    }
    
  if ( fabs(ps.input.dac_inc) > fabs(dac_stop - ps.input.dac_start)) 
    {
      cm_msg(MERROR,"DAC_set_scan_params","Invalid DAC scan increment value: %.2f",
	     ps.input.dac_inc);
      return FE_ERR_HW;
    }
  
  status = db_set_value(hDB, hOut, "DAC Stop", &dac_stop, sizeof(dac_stop), 1, TID_FLOAT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"DAC_set_scan_params","cannot write output parameter  \"DAC stop\" (%d)",status); 
      return status;
    }

  
  dac_val = ps.input.dac_start +  ps.input.dac_inc; // set DAC one increment away from start value
  printf("DAC_set_scan_params: DAC device will be set to =%.2f Volts (1 increment away from starting value)\n",dac_val);
  set_DAC(gVme, dac_val); 

  printf(" DAC_set_scan_params: selected %d DAC scan increments starting with %.2f Volts by steps of %.2f Volts\n",
	  dac_ninc, ps.input.dac_start, ps.input.dac_inc);

  return SUCCESS;
}

#ifdef HAVE_PPG
INT set_ppg_alarm(BOOL active)
{
  BOOL activ = active;
  INT size, status;

  size=sizeof(activ);
  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &activ, size, 1, TID_BOOL);
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"set_ppg_alarm","cannot set /Alarms/Alarms/PPG/active to %d  (%d)",activ, status); 
  
  al_reset_alarm("PPG"); // reset alarm
  return status;
}
#endif

INT set_dac_sleep_time(void)
{ 
  INT status,size;
  char str[80];
  double dst;
  
  dst = (double)ps.input.dac_sleep_time__ms_;  // global read from ODB
  
  //printf("set_dac_sleep_time: starting with DAC sleep time = %f ms\n",ps.input.dac_sleep_time__ms_);
  if(ps.input.run_tri_config)
    {
      printf("set_dac_sleep_time: Setting DAC Sleep time to %f  \n",dst);
      sprintf(str,"pulse_dacsv/pulse offset (ms)");
      
      size = sizeof(dst);	  
      status = db_set_value(hDB, hPPG, str, &dst, size, 1, TID_DOUBLE);  
      if(status != DB_SUCCESS) 
	cm_msg(MERROR,"set_dac_sleep_time","cannot set ODB key \"%s\" to %f  (%d)",str,dst, status); 
      return status;   
    }
  
#ifndef NEW_PPG
  else
    {
      cm_msg(MINFO,"set_dac_sleep_time","Cannot change parameter unless tri_config is run");
      return DB_INVALID_PARAM;
    }
#endif

    
#ifdef NEW_PPG
  if( !ps.input.run_tri_config)
    {
      dac_sleep_count  = (DWORD)((dst/ps.output.time_slice__ms_)+0.5); // set delay before daq service pulse to dac_sleep_count
      if(dac_sleep_count > 3)
	dac_sleep_count -=3; // minimum 3 clock cycles included for newppg
      else
	dac_sleep_count = 0;

      printf("set_dac_sleep_time:  dac_sleep_time= %f  dac_sleep_count=%u \n",ps.input.dac_sleep_time__ms_,dac_sleep_count); 
    }    
#endif
  return SUCCESS;

}

void  setup_hotlinks(void)
{
  INT status;
  char str[80];
  
  if (hfe==0)
    {
      sprintf(str,"input/hot debug");
    
      status = db_find_key(hDB, hTASet, str, &hfe);
      if (status == DB_SUCCESS)
	{
        
	  status = db_open_record(hDB, hfe, &lhot_debug
				  , sizeof(lhot_debug)
				  , MODE_READ, call_back_debug, NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

  if (hStop==0)
    {
      sprintf(str,"input/stop at end-of-sweep");
      status = db_find_key(hDB,  hTASet, str, &hStop);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hStop, &hot_stop
				  , sizeof(hot_stop)
				  , MODE_READ, stop_call_back, NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }


  return;

}
/*-- Call_back  ----------------------------------------------------*/
void call_back_debug(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
  //printf("lhot_debug is %d\n",lhot_debug); 
  if (hot_debug)
    hot_debug = 0;
  else
      hot_debug = 1;
  printf("Hot debug has been toggled; hot_debug is now %d\n",hot_debug); 
}
/*-- Call_back  ----------------------------------------------------*/
void stop_call_back(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
   printf("\nstop_call_back: hot_stop=%d \n",hot_stop);

}
/*-- Call_back  ----------------------------------------------------*/



INT stop_run(void)
{
  INT status;
  INT debug_flag=1;
  char str[128];


  if(stop_run_flag)
    {
      printf("stop_run: stop_run_flag is already set. Deferred stop already in progress\n");
      return SUCCESS;
    }

  stop_run_flag = TRUE; // set a flag
  status = cm_transition(TR_STOP, 0, str, sizeof(str), ASYNC, debug_flag);
  printf("stop_run:  after cm_transition, status=%d\n",status);
	  
  if (status == CM_DEFERRED_TRANSITION)
      printf("Deferred transition:  %s\n", str);
  else if (status == CM_TRANSITION_IN_PROGRESS)
    cm_msg(MINFO,"stop_run",
      "Deferred stop already in progress, in an xterm, enter odbedit command \"stop now\" to force stop");
  else if (status != CM_SUCCESS)
    {
      printf("stop_run:   Error: %s\n", str);      
      cm_msg(MERROR,"stop_run","Automatic stop has failed. Please stop the run now"); 
    }

  return status;
}

BOOL deferred_stop(INT run_number, BOOL first) 
{ 
  INT i; 
  char *names[]={"CYCLE","HISTO1","FIFO","MCSC","HISTO2"};


  if(!stop_run_flag)   // set flag in case stop is triggered by user rather than pgm.
    stop_run_flag=TRUE; // user wants to stop the run; prevent next supercycle from starting

  printf("\n***  deferred_stop: stop_run_flag = %d   *** \n",stop_run_flag);
 
  // defer stop until all equipments have run, i.e. decode and write the last data into banks

  for (i=0; i<5; i++)
    {
      if(waiteqp[i] != 0)
	{
	  printf("deferred_stop: waiting for equipment %s to finish\n", names[i]);
	  return FALSE;
	}
    }
 
  printf("\n***    deferred_stop: All equipments have run...stopping run now   *** \n");
  return TRUE;
}


INT incr_DAC(void)
{
  INT status;

  // increment DAC
  //printf("incr_DAC: gbl_DAC_n = %d  dac_ninc= %d\n",gbl_DAC_n, dac_ninc);
  wait_cntr=0;

  waiteqp[DAC]=TRUE;

  if( gbl_DAC_n ==   dac_ninc) 
    { /* Finished SWEEP;   
	 N E W  S W E E P  FOR DAC */
      if(!bor_flag)
	end_of_sweep_flag =  TRUE; // flag to clear (DSS DAC Sum banks) 
      gbl_DAC_n = 0;
      gbl_SCAN_n ++;
      // check whether we have reached num_scans (0 for free-running)
      if (num_scans > 0 && (gbl_SCAN_n > num_scans))
	{
	  cm_msg(MINFO,"incr_DAC","Requested number of scans reached (%d). Attempting to stop the run",num_scans);
	  printf("incr_DAC: stopping the run...(number of scans reached)\n");
          waiteqp[DAC]=FALSE; // clear flag
          status = stop_run();
	  return status;
	}

      if(hot_stop)
	{
	  printf("\n");
	  cm_msg(MINFO,"incr_DAC","Request to stop at end-of-sweep is set (hot-link). Attempting to stop the run",num_scans);
          status = stop_run();
          waiteqp[DAC]=FALSE; // clear flag
	  return status;
	}

      printf("\n incr_DAC: ***  N E W  S W E E P  ...  sweep %d is  starting (gbl_histo_bin_cntr %d) \n",gbl_SCAN_n, gbl_histo_bin_cntr);
    }

  
  dac_val =  ps.input.dac_start + (gbl_DAC_n  *  ps.input.dac_inc);

  if(debug)
     printf("incr_DAC: calling set_DAC with value %f \n",dac_val);

  status = set_DAC(gVme, dac_val);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"incr_DAC","Problem setting DAC value");
      status=stop_run();
      return status;
    }   
  gbl_DAC_n++;
  return SUCCESS;
}


INT read_adc_event(char *pevent, INT off)
{
  float *pfloat;
  int i;
  
  //ADC_CHAN_OFFSET is  Offset of each channel data from previous

  /* init bank structure */
  bk_init32(pevent);

 // Add RADC bank
  bk_create(pevent, "CYCL", TID_FLOAT, &pfloat);

  for (i=0; i<nadc; i++)
    {
      rdata =(int)  AD_regRead16(gVme, ADC_BASE, (ADC_V3801_CHAN_1 + (ADC_CHAN_OFFSET * i)) );
      pfloat[i] = retrieve_from_hex(rdata);
    }
  bk_close(pevent, pfloat+nadc);
  return bk_size(pevent);   
}




/*-- Clear scaler i ------------------------------------------------------*/
void scaler_clear(INT i)
/*------------------------------------------------------------------------*/
{
  /* scaler_clear  */
  DWORD j;
  
  for (j=0;j<scaler[i].nbins;j++)
    scaler[i].ps[j]=0;
  scaler[i].sum_cycle = 0.0;
}


void clear_mcs_counters(void)
{
  int i;
  if(debug)printf("clear_mcs_counters: clearing cycle scalers\n");
  for (i=0; i<NumSisChannels; i++) // use actual max number of SIS channels
    {
      scaler_clear(i); // clear cycle histo
      if(debug)printf("clearing scaler[%d]\n",i);
    }
  // remember gSumMcsEvents for DBUG bank
  last_gSumMcsEvents= (INT)gSumMcsEvents;
  gSumMcsEvents=0;
 
}
/*-- Clear histo h ------------------------------------------------------*/
void histo_clear(INT h)
/*------------------------------------------------------------------------*/
{
  DWORD j;
  /* clear histogram h */
  for (j=0;j<histo[h].nbins;j++)
    histo[h].ph[j]=0;
  histo[h].sum =0.0;
}

/*-- Clear histo h ------------------------------------------------------*/
void dacsum_clear(INT h)
/*------------------------------------------------------------------------*/
{
  DWORD j;
  /* clear  dacsum increment h */
  printf("dacsum_clear: clearing dac sums\n");
  for (j=0;j<dacsum[h].ninc;j++)
    dacsum[h].ptr[j]=0.0;
}

  

INT get_pol_settings(void)
{
  INT status,size;
  char set_str[128];

  /* Book Setting space */ 
  POL_ACQ_SETTINGS_STR(pol_acq_settings_str); 
 
  /* Map /equipment/pol_acq/settings for the sequencer */ 
  sprintf(set_str, "/Equipment/POL_ACQ/Settings"); 
  status = db_create_record(hDB, 0, set_str, strcomb(pol_acq_settings_str)); 
  status = db_find_key (hDB, 0, set_str, &hTASet); 
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"get_pol_settings","Key %s not found", set_str); 
  
  /* check that the record size is as expected */
  status = db_get_record_size(hDB, hTASet, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_pol_settings", "error during get_record_size (%d)",status);
      return status;
    }
  
  printf("Size of sis saved structure: %d, size of sis record: %d\n", sizeof(POL_ACQ_SETTINGS) ,size);
  if (sizeof(POL_ACQ_SETTINGS) != size)
    {
      /* create record */
      cm_msg(MINFO,"get_pol_settings",
	     "creating record for %s; mismatch between size of structure (%d) & record size (%d)", 
	     set_str, sizeof(POL_ACQ_SETTINGS) ,size);
      status = db_create_record(hDB, 0, set_str, strcomb(pol_acq_settings_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_pol_settings","Could not create %s record (%d)",set_str,status);
	  return status;
	}
      else
	printf("\nget_pol_settings: Success from create record for %s\n",set_str);
    }

  /* Get current  settings */
  size = sizeof(ps);
  status = db_get_record(hDB, hTASet, &ps, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_pol_settings", "cannot retrieve %s record (size of ps=%d)", set_str,size);
      return DB_NO_ACCESS;
    }

  // printf("thr2 = %f\n", ps.cycle_thresholds.threshold2);
  printf("get_pol_settings:  PPGpath = %s \n", ps.input.ppg_path); //  e.g. /home/pol/online/pol/ppg  

#ifdef HAVE_PPG
#ifdef NEW_PPG
  sprintf(ppgfile,"%s/ppgload/ppgload.dat", ps.input.ppg_path);
#else 
  sprintf(ppgfile,"%s/ppgload/bytecode.dat", ps.input.ppg_path); 
#endif
  printf("get_pol_settings:  ppgfile:%s\n",ppgfile); 
#endif
 


  /* find key for output */
  sprintf(set_str,"output"); 
  status = db_find_key (hDB, hTASet, set_str, &hOut); 
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_pol_settings","cannot get ODB key \"/Equipment/pol_acq/settings/output\"  (%d)",status); 
      return status;
    }
  //printf("got the key hOut\n");


  /* find key for ppg cycle
     "ppg cycle" is fixed for POL experiment
  */
  hPPG = 0;
  sprintf(set_str,"/equipment/POL_ACQ/ppg cycle"); 
  status = db_find_key (hDB, 0, set_str, &hPPG); 
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_pol_settings","cannot get ODB key at %s (%d)",set_str,status); 
      return status;
    }
  

  printf("PPGpath:%s  Exp name = %s \n", ps.input.ppg_path, exp_name); 
  sprintf(cmd,"%s/tri_config  -e %s -s -d", ps.input.ppg_path,exp_name); 
  //printf("tri_config command is :\"%s\"\n",cmd); 
 
  // initialize hot_debug in ODB to whatever is set in program (usually FALSE)
  status = db_set_value(hDB, hTASet, "input/hot debug", &hot_debug, sizeof(BOOL), 1, TID_BOOL);  
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"frontend_init","cannot set ODB key \"input/hot debug\"  (%d)",status); 
      return status;
    }
  else
    printf("frontend_init: hot_debug initialized to %d\n",hot_debug);

  return SUCCESS;
}


INT set_watchdog()
{
  INT status;
  // counter in ../output indicated frontend is still alive
  watchrabbit=0; // reset
  watchdog++;
  if(watchdog >= 100)
    watchdog = 0;

  //printf("set_watchdog: setting watchdog to %d\n",watchdog);
  status = db_set_value(hDB, hOut, "watchdog", &watchdog, sizeof(INT), 1, TID_INT);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"set_watchdog","cannot set ODB key ..output/watchdog to %d (%d)",watchdog,status); 

  return status;
}


INT find_ADC_keys(void)
{
  INT status;
  KEY key;
  
  hVar=0;
  status = db_find_key(hDB, 0, "/Equipment/POLADC/Variables/", &hVar);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"find_ADC_keys","failure finding key for /Equipment/POLADC/Variables/ (%d)",status);
      return status;
    }

  status = db_get_key(hDB, hVar, &key );
  if (status == DB_SUCCESS)
    {
      nadc = key.num_values;  // nadc is global
      printf("find_ADC_keys: Number of adc values read in \"/Equipment/POLADC/Variables/\" is %d\n",nadc);
    }

  /* check the size of the array adc_readback */
  if (sizeof(adc_readback) <  nadc * sizeof(float))
    {
      printf("find_ADC_keys: adc_readback array is not large enough\n");
     return FAILURE;
    }
  else
    printf("size of adc_readback=%d nadc * sizeof(flaot)=%d\n",sizeof(adc_readback),(  nadc * sizeof(float)) );

  return status;
}

  //end
  
