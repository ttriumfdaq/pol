// Test program for POL  VMIC DAC and ADC

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "mvmestd.h"
#include <unistd.h>
#include <sys/time.h>
#include "VMIVME3801.h"  // ADC
#include "VMIVME4800.h"  // DAC
#include "pol_adcdac.h" // function prototypes

/*****************************************************************/

#ifdef MAIN_ENABLE
  uint32_t DAC_BASE  = 0x4000;
  uint32_t ADC_BASE  = 0x3100;

#endif  //MAIN_ENABLE
/*
Read  register value
*/
uint32_t AD_regRead8(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A16_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/

/*
Write  register value
*/
void AD_regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A16_ND );
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/

/*
Write  register value (16 bit)
*/
void AD_regWrite16(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A16_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/

/*
Read  register value
*/
uint32_t AD_regRead16(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A16_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/

void setup_DAC(MVME_INTERFACE *mvme,DWORD base)
{
 
  AD_regWrite16(mvme, base, DAC_V4800_CONTROL_STATUS, DAC_CSR_LED_OFF ); // setup CSR with LED off
  AD_regWrite16(mvme, base, DAC_V4800_CHAN_0, 0 ); // setup DAC Chan 0 to 0V
}

void setup_ADC(MVME_INTERFACE *mvme,DWORD base)
{
  AD_regWrite8(mvme, base, ADC_V3801_CONTROL_STATUS, ADC_CSR_LED_OFF ); // setup CSR with LED off
}


void  DAC_Status(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t data;
  int i;

  printf("DAC_V4800 at base 0x%x\n", (int)base);
  data =  0xFFFF &  AD_regRead16(mvme,base,DAC_V4800_BOARD_ID); // 16 bits
  printf("ModuleID : 0x%x\n", data);
  if(data != DAC_BOARDID)
    printf("Unexpected DAC board id (0x%x). Expect 0x%x\n",data, DAC_BOARDID);

  data = 0xFFFF & AD_regRead16(mvme,base, DAC_V4800_CONTROL_STATUS); // 16 bits
  printf("DAC CSR: 0x%x\n", data );
  if(data & DAC_LED_OFF_BIT)
    printf ("LED            off\n");
  else
    printf ("LED             on\n");

  if(data &  DAC_OUTPUT_EN_BIT)
    printf ("Output            enabled\n");
  else
    printf ("Output            disabled\n");
  
  if (data &  DAC_TWOS_COMP_BIT )   
    printf ("2s Compliment     enabled\n");
  else
    printf ("2s Compliment     disabled\n");
    
  for (i=0;i<8; i++)
    {
      if(data & 1<<i)
	printf("Channel %d busy\n",i);
      else
	printf("Channel %d not busy\n",i);
    }


  printf("\nChannel 0: 0x%x\n", (0xFF & AD_regRead16(mvme, base,DAC_V4800_CHAN_0 ))); // 16 bits
}

void  ADC_Status(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t data;
  int i;

  printf("ADC_V3801 at base 0x%x\n", (int)base);
  data =    AD_regRead8(mvme,base, ADC_V3801_BOARD_ID); // 8 bits
  printf("ModuleID : 0x%x\n", data);
  if(data != ADC_BOARDID)
    printf("Unexpected ADC board id (0x%x). Expect 0x%x\n",data, ADC_BOARDID);

  data =   0xFF & AD_regRead8(mvme,base, ADC_V3801_CONFIG ); // 8 bits
  printf("Config Reg: 0x%x\n",data);
  if(data != 0)
    printf("Unexpected Config reg value (0x%x). Expect 0\n",data);


  data = 0xFF & AD_regRead8(mvme,base, ADC_V3801_CONTROL_STATUS );
  printf("ADC CSR: 0x%x\n", data);
  
  if(data & ADC_LED_OFF_BIT)
    printf ("LED            off\n");
  else
    printf ("LED             on\n");


  if(data & ADC_TWOS_COMP_BIT)
    printf ("2s Compliment     enabled\n");
  else
    printf ("2s Compliment     disabled\n");


  for (i=0;i<3; i++)
    {
      if(data & 1<<(i+3)) 
	printf("Mode %d is set\n",i);
      else
	printf("Mode %d not set\n",i);
    }


  printf("Channel 1: 0x%x\n", AD_regRead16(mvme, base, ADC_V3801_CHAN_1 )); // 16 bits
}


int convert_to_hex(float volt,  uint32_t *data )
{ // convert Volts to 2s compliment for DAC
    int n,m;
  

    if  (volt > 9.996)
      {
	printf("convert_to_hex: Value %f is out of range. Maximum is 9.9951 V\n",volt);
	return -1;
      }
    else if  (volt < -10 )
      {
	printf("convert_to_hex: Value %f is out of range. Minimum is -10.0 V\n",volt);
	return -1;
      }
    else if (volt >= 9.9951) 
      {    
	m=0x7FF; // set to max to avoid rounding error
      	//printf("Converting %f volts to  2s comp = 0x%x\n",volt,m);
	*data = m;
	return 1;
      }

    else if( volt == -10)
      {
	m=0x800; // set to min to avoid possible rounding error
      	//printf("Converting %f volts to  2s comp = 0x%x\n",volt,m);
	*data = m;
	return 1;
      }
 
    n = volt * 0x800 /10;
    m= n & 0xFFF;
    //printf("Converting %f volts to  2s comp = 0x%x\n",volt,m);
    *data = m;
   
    return 1; // SUCCESS
}


float retrieve_from_hex(int data )
{
  float lsb= 10.0 / (float)0x800; // volts
  float volt;


  //printf("retrieve_from_hex: starting with data=0x%x or %d\n",data,data);

  if(data & 0x800)
      data=data | 0xFFFFF000; // sign extend
   
  volt = (float)data * lsb;
 
  return volt;
}



#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {
  int i,j;

  float maxv =  9.9951;
  float minv = -10.0;
  MVME_INTERFACE *myvme;
  int status;
  uint32_t data, istep,rstep,rtmp;
  float voltage,rvoltage;
  int rdata,rdata_prev,sstep;
  FILE *fout;
  char filename[]="pol.dat";
  float stop,start,step;
  int prev,rprev;
  int nstep;

 



  // if (argc>1) {
  //  sscanf(argv[1],"%x", &SIS3820_BASE);
  // }

  // Test under vmic
  status = mvme_open(&myvme, 0);
  setup_DAC(myvme, DAC_BASE);
  DAC_Status(myvme, DAC_BASE);

  setup_ADC(myvme, DAC_BASE);
  ADC_Status(myvme, DAC_BASE);

#if 0
  // User LED test
  for (;;) {
    AD_regWrite8(myvme, ADC_BASE, ADC_V3801_CONTROL_STATUS , ADC_CSR_LED_ON );
    AD_regWrite16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS , DAC_CSR_LED_OFF );


    data =  (AD_regRead8(myvme, ADC_BASE, ADC_V3801_CONTROL_STATUS )& 0xFF); // 8 bits
    printf("DAC status:%4.4x   ADC status:%2.2x\n", 
	   (AD_regRead16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS  ) & 0xFFFF),
	   data);

    sleep(1);
    AD_regWrite8(myvme, ADC_BASE,  ADC_V3801_CONTROL_STATUS , ADC_CSR_LED_OFF );
    AD_regWrite16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS , DAC_CSR_LED_ON );

    data = (AD_regRead8(myvme, ADC_BASE, ADC_V3801_CONTROL_STATUS)& 0xFF); // 8 bits
    printf("DAC status:%4.4x   ADC status:%2.2x\n",
	   (AD_regRead16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS)  & 0xFFFF ),
	   data);
    

    sleep(1);
  }
#endif

#if 0   // this may not be working
  // step voltage up on DAC Ch 0; read on ADC Ch 1
  
  voltage=-9.995;
  
  fout = fopen(filename,"w");
  if(fout == NULL)
    {
      printf("Could not open output file %s\n",filename);
      return 1;
    }
  
  
  printf("Output file is %s\n",filename);
  j=1;
  
  while (j>0)
    {
      printf("Enter Start value (Volts) : ");
      scanf("%f",&start);
      if((start > maxv) || (start < minv ))
	printf("Start value must be between %f V and %f V\n",minv,maxv);
      else
	j=0;
    }
  j=1;
  while (j>0)
    {  
      printf("Enter Step value (Volts) : ");
      scanf("%f",&step);
      
      printf("Enter Number of steps : ");
      scanf("%d",&nstep);
      
      stop = start + nstep * step;
      
      if((stop > maxv) || (start < minv ))
	printf("Stop value (%f V) must be between %f V and %f V. Reduce step or number of steps.\n",stop,minv,maxv);
      else
	j=0;
    }

  printf("\nStart at %6.3f V, stop at %6.3f V, %2.2d steps of %6.3f V\n\n",start,stop,nstep,step);
  fprintf(fout, "\nStart at %6.3f V, stop at %6.3f V, %2.2d steps of %6.3f V\n\n",start,stop,nstep,step);
  

  
  fprintf(fout, "Step Set(V)    Hex  Diff        Read(V)   Hex     Diff    Diff(V)   Diff Set/Read (V)  Hex (diffs abs values)\n");
  printf("Step Set(V)    Hex  Diff        Read(V)   Hex     Diff    Diff(V)   Diff Set/Read (V)  Hex (diffs abs values)\n");
  
  status = convert_to_hex( (fabs(step)), &istep);
  if(status == -1)
    {
      printf("Error return from convert_to_hex for step %f\n",step);
      sleep( 1);
    }
  printf("istep=0x%x\n",istep); // step converted to hex for comparison
  sstep=istep/2;
  
  

  status = convert_to_hex (voltage, &data);
  if(status == -1)
    {
      printf("Error return from convert_to_hex at voltage %f\n",voltage);
      sleep( 1);
    }

  voltage = start - step;
  j=0;
  prev=rprev=0.0;
  
  
  for (i=0; i< (nstep+1); i++)
    {
      
      voltage = voltage + step ;
      status = convert_to_hex (voltage, &data);
      if(status == -1)
	{
	  printf("Error return from convert_to_hex at voltage %f\n",voltage);
	  sleep( 1);
        }
      else
	{
	  AD_regWrite16(myvme,DAC_BASE, DAC_V4800_CHAN_0, data ); // write 2s comp to DAC Chan 0 
	  sleep(1);
	  rdata =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 );
	  if(j==0) // first step
	    rdata_prev = data - istep; // estimated value of rdata_prev
	  else
	    rdata_prev = rprev;
	  rstep =  abs(rdata-rdata_prev);
          
      
	  // printf("k=%d istep=0x%x rstep=0x%x;  rdata=0x%x rdata_prev=0x%x \n",k,istep, rstep, rdata,rdata_prev);
	  

	  rvoltage=retrieve_from_hex(rdata);
	  //  printf("Wrote %f Volts (0x%4.4x) to DAC Chan 0. Read back ADC Chan 1 as 0x%4.4x or %f Volts\n",
	  //	 voltage,data, rdata, rvoltage ); // 16 bits
	
	  
          if(j==0)
	    {
	      fprintf(fout, "%2.2d  %6.3f or 0x%3.3x            %6.3f or 0x%3.3x                       %6.3f or 0x%3.3x\n",
		      j,voltage,data,rvoltage,rdata, fabs(voltage-rvoltage), abs(data-rdata));  
	      printf("%2.2d  %6.3f or 0x%3.3x            %6.3f or 0x%3.3x                       %6.3f or 0x%3.3x\n",
		     j,voltage,data,rvoltage,rdata, fabs(voltage-rvoltage), abs(data-rdata));  
	      
	    } 
	  else
	    {
	      rtmp = fabs(rdata-rprev);
              
	      fprintf(fout, "%2.2d  %6.3f or 0x%3.3x 0x%3.3x      %6.3f or 0x%3.3x  0x%3.3x or %6.3f      %6.3f or 0x%3.3x\n",
		      j,voltage,data,abs(data-prev),rvoltage,rdata,abs(rdata-rprev), retrieve_from_hex(rtmp), fabs(voltage-rvoltage), abs(data-rdata));
	      printf("%2.2d  %6.3f or 0x%3.3x 0x%3.3x      %6.3f or 0x%3.3x  0x%3.3x or %6.3f      %6.3f or 0x%3.3x\n",
		     j,voltage,data,abs(data-prev),rvoltage,rdata,abs(rdata-rprev), retrieve_from_hex(rtmp), fabs(voltage-rvoltage), abs(data-rdata));
	    }    
	  prev=data;
	  rprev=rdata;
	  j++;
	  sleep (1);
	  
	  
	}
    }
  fclose (fout);
#endif



#if 1

  j=1;
  while (j>0)
    {
      printf("Enter Start value (Volts) : ");
      scanf("%f",&start);
      if((start > maxv) || (start < minv ))
	printf("Start value must be between %f V and %f V\n",minv,maxv);
      else
	j=0;
    }

  j=1;
  while (j>0)
    {  
      printf("Enter Step value (Volts) : ");
      scanf("%f",&step);
      
      printf("Enter Number of steps : ");
      scanf("%d",&nstep);
      
      stop = start + nstep * step;
      
      if((stop > maxv) || (start < minv ))
	printf("Stop value (%f V) must be between %f V and %f V. Reduce step or number of steps.\n",stop,minv,maxv);
      else
	j=0;
    }

  printf("\nStart at %6.3f V, stop at %6.3f V, %2.2d steps of %6.3f V\n\n",start,stop,nstep,step);

  voltage = start - step; // one less
  for (i=0; i< (nstep+1); i++)
    {
      
      voltage = voltage + step ;
      printf("\nStep %d: setting voltage %f volts...\n",i,voltage);
      set_dac(myvme, voltage);
    }


#endif   // if 1
  return 1;
  
}


int set_dac( MVME_INTERFACE *myvme, float volts)
{
  int status;
  uint32_t data;
  int rdata,rdata_prev;
  int k;

  struct timeval starttime,endtime,timediff;



  status = convert_to_hex (volts, &data);
  if(status == -1)
    {
      printf("Error return from convert_to_hex at  voltage %f\n",volts);
      return status;
    }
   rdata_prev =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 );
   AD_regWrite16(myvme,DAC_BASE, DAC_V4800_CHAN_0, data ); // write 2s comp to DAC Chan 0 
   gettimeofday(&starttime,0x0); // get time

   for (k=0; k< 300; k++)
     {
       rdata =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 );
       // printf("set_dac: k=%d wrote 0x%4.4x  read 0x%4.4x   prev read 0x%4.4x\n",k,data,rdata,rdata_prev);
       if( abs(rdata-rdata_prev) > 5)
	 //if(rdata != rdata_prev)
	   {
	     gettimeofday(&endtime,0x0);
	     timeval_subtract(&timediff,&endtime,&starttime);
	     printf("set_dac: count=%d wrote 0x%4.4x  read 0x%4.4x   prev read 0x%4.4x\n",k,data,rdata,rdata_prev);
	     break;
	   }

     }
 
   if(k >=300)
     {
       printf("Timeout... sleeping\n");
       sleep(1);

       rdata =(int)  AD_regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 );
       printf("set_dac: k=%d wrote 0x%4.4x  read 0x%4.4x   prev read 0x%4.4x\n",k+2,data,rdata,rdata_prev);
     }
   else
     printf("elapsed_time = %d sec   %d usec \n",(int)timediff.tv_sec,(int) timediff.tv_usec);

 

   return 1; // success
}




int timeval_subtract (result, x, y)
     struct timeval *result, *x, *y;
{
  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }



  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}


#endif // MAIN ENABLE
