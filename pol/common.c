// common subroutines between newppg.c and ppg_modify_file
#include <stdio.h>
#include <string.h>
#include "common.h"

extern int ddd;
/*------------------------------------------------------------------*/
/** getinsline
    Read line of input instruction file
    @memo read line.
    @param *input char* line buffer to contain line of input file
    @param *input int max  integer maximum characters in line
    @param *input FILE *file file handle
    @return PARAM data structure
*/
/* getinsline */
int getinsline(char *line, int max, FILE *file)
{
  if (fgets(line,max,file) == NULL)
    return 0;
  else
    return strlen(line);
} 
	

/*------------------------------------------------------------------*/
/** lineRead
    Read line of input file
    @memo read line.
    @param *input char* line  line of input file
    @return PARAM data structure
*/
COMMAND lineRead(char *line)
{
  COMMAND data_struct;
  if(ddd)printf ("lineRead: line=\"%s\n",line);
  char *p=strchr(line,'#'); // look for comment (#) following instruction
  if (p)
    *p='\0';
  if(ddd)printf ("lineRead: line=\"%s\n",line);
  sscanf(line,"%u %lx %lx %lx %lx", &(data_struct.pc), &(data_struct.setpat),
	 &(data_struct.clrpat), &(data_struct.delay), &(data_struct.ins_data));
  return(data_struct);
}
