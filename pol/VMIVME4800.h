/***************************************************************************/
/*                                                                         */
/*  Filename: VMIVME4800.h                                                 */
/*                                                                         */
/*       headerfile for Pol's DAC                                          */    
/*                                                                         */
/*           VMIVME-4800                                                   */
/*                                                                         */
/***************************************************************************/

#ifndef __VMIVME4800__
#define __VMIVME4800__

/* DAC addresses  (offsets from base) */

#define DAC_V4800_BOARD_ID                              0x0             /* Board ID  ; D16  RO */
#define DAC_V4800_CONTROL_STATUS                        0x4             /* CSR       ; D16  RW */
#define DAC_V4800_CHAN_0                                0x8             /* Channel 0 ; D16  RW */
#define DAC_V4800_CHAN_2                                0xC             /* Channel 2 ; D16  RW */

// Constants and Masks
#define DAC_BOARDID                    0x4600    /* DAC Board ID */
#define DAC_TWOS_COMP_BIT              0x1000    /* Set 2s complement in DAC_V4800_CONTROL_STATUS reg */
#define DAC_OUTPUT_EN_BIT              0x2000    /* Set Output Enable in  DAC_V4800_CONTROL_STATUS reg */
#define DAC_LED_OFF_BIT                0x8000    /* Set LED OFF in  DAC_V4800_CONTROL_STATUS  reg */

#define DAC_CSR_LED_OFF   DAC_OUTPUT_EN_BIT  | DAC_TWOS_COMP_BIT |  DAC_LED_OFF_BIT 
#define DAC_CSR_LED_ON    DAC_OUTPUT_EN_BIT  | DAC_TWOS_COMP_BIT

#endif // __VMIVME4800__
