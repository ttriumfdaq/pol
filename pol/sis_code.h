// sis_code.cxx
#ifndef SISCODE
#define SISCODE
INT  init_sis3820(void);
INT get_SIS_dataformat(INT nbits );
void SIS_readback(unsigned int wrote_mode, unsigned int wrote_csr);
void opmode_bits(unsigned int data);
void csr_bits(unsigned int data);
int nchan_enabled(unsigned int bp, int en, int nb );
#endif
