#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# $Log: kill_frontend.pl,v $
# Revision 1.2  2014/11/24 21:38:40  suz
# now uses environment variable DAQ_HOST for DAQ hostname
#
# Revision 1.1  2013/04/10 19:44:55  suz
# new to cvs; supports bnmr and bnqr
#
# VMIC version
#
# This version also supports POL experiment
use strict;

# invoke this script with cmd 
#                         Parameter
# perl kill_frontend.pl        0 = kill task first in odb then kill program if still running
#                              1 = just kill the program if running

sub kill_frontend_task($); # prototype
#
#
our $MIDAS_EXPT_NAME = $ENV{'MIDAS_EXPT_NAME'};
our $HOME = $ENV{'HOME'};
our $ONLINE;
my $fe_taskname;

print " Environment variables: selected MIDAS_EXPT_NAME  $MIDAS_EXPT_NAME, HOME is $HOME ";
if($MIDAS_EXPT_NAME=="pol")
{ 
    $ONLINE="online";
    print "\n";
    $fe_taskname="pol_fevme.exe";
}
else
{
    $ONLINE = $ENV{'ONLINE'};
    print "and ONLINE is $ONLINE\n";
    $fe_taskname="fe".$MIDAS_EXPT_NAME."_vmic.exe";
}


#  check-host-lxbn[qr]r looks for host $DAQ_HOST username  "bn[qm]r"  and online "vmic_online"
#  check-host-lxpol looks for host  $DAQ_HOST username  "pol"
my $reply = `$HOME/$ONLINE/$MIDAS_EXPT_NAME/bin/check-host-lx$MIDAS_EXPT_NAME `;
print "$reply\n";
if ( "$?" != "0" ) {
    print "Failure from check-host-$MIDAS_EXPT_NAME\n";
    exit;
}

#

my ($param) = @ARGV;  # need the brackets
print "input parameter =$param\n";

unless ($param)
{  # skip this if param=1 (i.e. called from kill-all where mserver has been shut down)

    my $done=0;
    my $count=0;
    while ( ! $done && $count < 3 )
    {
	$count++;  # if feBN[QM]R_VMIC can't be killed avoid infinite loop
	$reply = `odb -c scl -e $MIDAS_EXPT_NAME` ;
#$reply = $reply . "feBNQR_VMIC      lxbnqr\n";
	print " Active Midas clients are \n $reply \n" ;
	if ($reply =~/(feBN[QM]R_VMIC\d?)/i)
	{  
	    print "found $1 \n";
	    print "shutting down $1\n";
	    `odb -c 'sh $1' `;  
	}
        elsif ($reply =~/(pol_fevme\d?)/i)
	{  
	    print "found $1 \n";
	    print "shutting down $1\n";
	    `odb -c 'sh $1' `;  
	}
	else
	{ 
	    $done=1;
	}
    }
    sleep 2;
} # end of unless $param
kill_frontend_task($fe_taskname);
exit;


sub kill_frontend_task($)
{
# make sure task is stopped
    my $task=shift;
    print "checking for task $task\n";
    `killall $task`;
    return;
}


