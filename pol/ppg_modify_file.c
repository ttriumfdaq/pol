#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "common.h"

//#define MAIN

int start_pc,num_instructions;

FILE  *ppginput,*ppgoutput,*ppgmods;

#ifdef MAIN
double time_slice=0.00001;
// odb parameters
  int scan_lc=10000000;
  int cycle_lc=100;
  double lne_pw=0.05; //ms
  double dtime=0.06; //ms
  double dac_sleep_time=1;
  int debug=0;
int ddd = 0; // needed in common.c
#else
extern int debug;
#endif



#include "ppg_modify_file.h"
/* typedef struct
{
  char            name[80]; // name of variable, e.g. "dwell time"
  int             index; // instruction number in the file
  unsigned long   value;     // value to set instruction to
  char            data_type[15] ; // data type (one of "delay" "loopcount" ...)
} FILE_VAR;  // filled by reading mod file
*/
// prototypes


//int ppg_modify_file(char * infile, char * outfile, char *modfile);
//void  lineWrite (COMMAND *data_struct);
int getinsline(char *line, int max, FILE *file);
COMMAND lineRead(char *line);
//int  get_next_line(char *line);
//int write_modifications(char *modfilename, char *outfile, DWORD array[], char *message);
//int get_mod(char * line, FILE_VAR *mods);
//void load_mod(char *mline, int *index, unsigned int *count, int *type);
//void close_files(void);


#ifdef MAIN

int main(void)
{
  char filename[80];
  char outfile[]="/home/pol/vmetest1/ppgload.dat";
  char modfile[]="/home/pol/vmetest1/ppgmods.dat";
  char modfilename[80];

  int status;
  char mode[]="1h";
  DWORD array[10];
  long int dst_count,dw_count,lne_count;

  sprintf(filename,"/home/pol/vmetest1/ppgtemplate_%s.dat",mode);
  sprintf(modfilename,"/home/pol/vmetest1/mode%s_mods.dat",mode);

  // convert time values to number of cycles
  if(time_slice > 0)
    {
      dw_count = (int)((dtime/time_slice)+0.5);
      lne_count =(int)((lne_pw/time_slice)+0.5);
      dst_count =(int)((dac_sleep_time/time_slice)+0.5);
      if(0)
	{
      if(dw_count > 3)
	dw_count-=3; // 3 cycles minimal time added by device
      else 
	dst_count = 0;
      if(lne_count > 3)
	lne_count-=3;
      else
	lne_count=0;
      if(dst_count > 3)
	dst_count-=3;
      else
	dst_count=0;
	}
    }
  else
    {
      printf("Invalid time slice (%f)\n",time_slice);
      return FAILURE;
    }
  array[0] = 5;
  array[1]= scan_lc;// num scans
  array[2]= cycle_lc; // num cycles
  array[3]= lne_count; // lne width
  array[4] = dw_count; // dwell counts
  array[5]= dst_count;// daq service


  if (write_modifications(modfilename, modfile, array) == 0)
    {
      printf("Could not write modification file %s\n",modfilename);
      return FAILURE;
    }
  else
    printf("\n\nNow applying modifications to ppg template file...\n");
  status = ppg_modify_file(filename, outfile, modfile);
  if (status ==0)
    printf("success from ppg_modify_file\n");
}
#endif

int ppg_modify_file(char * infile, char * outfile, char *modfile)
{
  /*  Local Variables  */

  COMMAND  command_info;
  char  line[100];
  char mline[100];
  //int  cmode, localam;
  int linenum,modnum,len;
  char *p;
  int header_flag;
  int counter;
  unsigned long temp;
  unsigned int count;
  int index,type;

  //  New PPG
  //  SET bits:0-31    |  CLR bits 32-63  | Delay Count:64-95  | Delay Count: 96-115 | Instruction 116-119 |  120-127
  //        32 bits    |        32 bits   |    32 bits         |    32 bits          |     4 bits          |  ignored




  start_pc = num_instructions = 0; // globals

  
  printf("ppg_modify_file: Opening ppg template file: %s   ...  \n",infile);
  if(ppginput != NULL)
    {
      printf("ppg_modify_file: ppginput is not NULL; cannot open %s\n",infile);
      return FAILURE;
    }
  ppginput = fopen(infile,"r");
  if(ppginput == NULL){
    printf("ppg_modify_file: ppg template file %s could not be opened\n", infile);
    return FAILURE;
  }
  else
    printf("opened %s (%p)\n",infile,ppginput);


  printf(" ppg_modify_file: Opening ppg output file: %s   ...  \n",outfile);
  if(ppgoutput != NULL)
    {
      printf("ppg_modify_file: ppgoutput is not NULL; cannot open %s\n",outfile);
      return FAILURE;
    }
  ppgoutput = fopen(outfile,"w");
  if(ppgoutput == NULL){
    printf(" ppg_modify_file:: ppg load file %s could not be opened\n", outfile);
    close_files();
    return FAILURE;
  }
  else
    printf("opened %s (%p)\n",outfile,ppgoutput);




  printf(" ppg_modify_file:Opening ppg mod file: %s   ...  \n",modfile);
  if(ppgmods != NULL)
    {
      printf("ppg_modify_file: ppgmods is not NULL; cannot open %s\n",modfile);
      return FAILURE;
    }
  ppgmods = fopen(modfile,"r");
  if(ppgmods == NULL){
    printf(" ppg_modify_file: ppg mod file %s could not be opened\n", modfile);
    close_files();
    return FAILURE;
  }
  else
    printf("opened %s (%p)\n",modfile,ppgmods);


  linenum=counter=0;
  header_flag=1;
  modnum=0;

  fprintf(ppgoutput,"# PPGLOAD file modified from template %s\n",infile); // write line to output file
  fprintf(ppgoutput,"#   using value in %s\n",modfile);                   // write line to output file
  len = get_next_line(mline); // get the next line from the modfile
  if(len > 0)
    {
      modnum++;
      load_mod(mline, &index, &count, &type);
      printf("Next mod at index %d count %u  type=%d\n",index,count,type);
    }
  else
    printf("No mod could be found in %s \n",modfile);



  // Find number of instructions from the template file
  while (getinsline(line,99,ppginput) > 0)
    {        
      linenum++;
      printf("Template File line %d: %s",linenum,line);
      if(header_flag)
	{
	  fprintf(ppgoutput,"%s",line); // write line to output file
	  if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	    {
	      if ( strncmp(line,"Num Instruction Lines =",23) ==0 ) // first line after comments
		{
		  p=line + 23;  // skip "Instruction Lines "
		  char *q;
		  q =strchr(p,'#');
		  if(q != 0)
		    *q='\0';
		  //printf("Now line is %s\n",p);
		  sscanf(p,"%d",&num_instructions);
		  //printf("instruction lines = %d\n",num_instructions);  
		  //printf("Base Address = %x\n",base_adr);
		  header_flag=0; // we have processed header
		}
	    }
	}
      else
	{ // instructions
	  command_info = lineRead(line);
	  counter++;
	  if (counter==1)
	    start_pc =  command_info.pc ; // remember where program starts (usually 0)
	  if (debug)
	    {
	      printf("pc:%3.3d ",command_info.pc);
	      printf("set bitpat:%8.8lx ",command_info.setpat);
	      printf("clr bitpat:%8.8lx ",command_info.clrpat);
	      printf("delay:%8.8lx\n",command_info.delay);
	      printf("ins/data:%8.8lx\n",command_info.ins_data);
	    }
	  
	  if(command_info.pc == index) 
	    { // this line needs to be modified

	      if (type==0) //loopcount
		{
		  temp = command_info.ins_data &  0xF00000 ;
		  temp = temp |  count;
		  printf("Mod %d: ins/data for instruction number %d changed from 0x%lx to 0x%lx\n",
			 modnum, counter, command_info.ins_data,temp);
		  command_info.ins_data = temp;
		}
	      else if (type == 1) // delay
		{
		  printf("Mod %d: ins/data for instruction number %d changed from 0x%lx to 0x%x\n",
			 modnum,counter, command_info.delay, count);
		  command_info.delay = count;
		}
	      else
		{
		  printf("Mod %d at index %d cannot be done. No such type %d\n",
			 modnum,index,type);
		  close_files();
		  return  FAILURE;
		}
	      lineWrite( &command_info); // write modified line to output file
	      fprintf(ppgoutput,"# Originally: %s",line); // write original line to output file
	      // get the next line from the mod file
	      len = get_next_line(mline); // get the next line from the modfile
	      if(len > 0)
		{
		  modnum++;
		  load_mod(mline, &index, &count, &type);
		  printf("\nppg_modify_file: next mod at index %d count %u  type=%d\n",index,count,type);
		}
	      else
		{
		  printf("No further mod could be found in %s \n",modfile);
		  index=0;
		}
	    }
	  else
	    fprintf(ppgoutput,"%s",line); // write original line to output file
	}
    } // end of while
  
  
  if(header_flag > 0)
    {
      printf(" ppg_modify_file: incorrect format for ppg load file. Did not find number of instruction lines\n");
      close_files();
      return  FAILURE;
    }

  if(counter != num_instructions)
      printf(" ppg_modify_file: Warning - stated number of instructions lines does not agree with actual number of instruction lines\n");
  
  


  printf("ppg_modify_file: returning success\n");
  close_files();
  printf("Programming ended, PPG ready");
  return SUCCESS;
}

void  lineWrite( COMMAND  *command_info)
{
  char line[128];
  if(debug)
    {
      printf("lineWrite input parameters:\n");
      printf("pc:%3.3d\n",command_info->pc);
      printf("set bitpat:%8.8lx\n",command_info->setpat);
      printf("clr bitpat:%8.8lx\n",command_info->clrpat);
      printf("delay:%8.8lx\n",command_info->delay);
      printf("ins/data:%8.8lx\n",command_info->ins_data);	    
    }
  sprintf (line,"%3.3d 0x%8.8lx 0x%8.8lx  0x%8.8lx 0x%6.6lx",
	   command_info->pc, command_info->setpat,
	   command_info->clrpat, command_info->delay, command_info->ins_data);

  if(debug)printf ("lineWrite: line=\"%s\n",line);
  fprintf(ppgoutput,"%s ",line);
  return;
}

int  get_next_line(char *line)
{

  int len,linenum=0;
  len=0;
  while (getinsline(line,99,ppgmods) > 0)
    {     
      linenum++;
      line[(strlen(line)-1)]='\0'; // get rid of <cr>;
      printf("File line %d: %s\n",linenum,line);
      if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	{
	  len=strlen(line);
	  break;
	}
    }
  if(debug && len > 0)
    printf("get_next_line returning len=%d, line=%s\n",len,line);
  return len;
}

void load_mod (char *mline, int *index, unsigned int *count, int *type)
{
  sscanf(mline,"%i %u %i",index,count,type);
  return;
}





int get_mod(char * line, FILE_VAR *mods)
{
  //  FILE_VAR mods;
  char str[80];
  char *p;
  char *q;
  int len=1;
  char c;
	 
  if(debug)printf ("get_mod: line=\"%s\n",line);
  // check format of first item
  sscanf(line, "%c", &c );
  if( ! isdigit(c) )
    {
      len=strlen(line);
      line[len-1]='\0'; // strip off <CR> for message
      printf("Invalid format for line \"%s\"; First item must be a digit\n",line);
      return FAILURE ;
    }

  sscanf(line,"%u %s %s", &(mods->index), (char *) &(mods->data_type), (char *) str);
  p=strstr(line,str);

  //printf("p=%s\n",p);
  q=p;
  while(*p!='#')
    p++;
  p--; // strip off any blanks between last character and #
  while(*p==' ')
    p--;
  *(p+1)='\0'; // terminate string
  strcpy(mods->name,q);
  if(debug)
    {
      printf("\nget_mod: output parameters\n");
      printf("           name:\"%s\"\n",mods->name);
      printf("           pc:%3.3d\n",mods->index);
      printf("           data type:\"%s\"\n",mods->data_type);
    }
    return SUCCESS; // success
}


int write_modifications ( char *modfilename, char *outfile, DWORD array[], char *message)
{
  char  mline[100];

  // parameters from odb are supplied in array
  
  FILE_VAR file_var;
  int i,j,type,n_param;

  printf("write_modifications:  ppginput=%p ppgoutput=%p ppgmods=%p\n",ppginput,ppgoutput,ppgmods);

  printf("write_modifications: Number of input parameters in array =%d\n",(int)array[0]);  
  n_param = array[0];
  for(i=0; i<n_param; i++)
    printf("write_modifications: input param %d : %d or 0x%x\n",(int)i,(int)array[i+1],(unsigned int)array[i+1]);
  
  
  printf("write_modifications: opening ppg mod file: %s   ...  \n",modfilename);
  if(ppgmods != NULL)
    {
      printf("write_modifications: ppgmods is not NULL; cannot open %s\n",modfilename);
      return FAILURE;
    }
  ppgmods = fopen(modfilename,"r");
  if(ppgmods == NULL){
    printf("write_modifications: ppg mod file %s could not be opened\n", modfilename);
    sprintf(message,"ppg mod file %s could not be opened", modfilename);
    close_files();
    return  FAILURE;
  }
  printf("write_modifications: opening output mod file: %s   ...  \n",outfile);
  if(ppgoutput != NULL)
    {
      printf("write_modifications: ppgoutput is not NULL; cannot open %s\n",outfile);
      return FAILURE;
    }
  ppgoutput = fopen(outfile,"w");
  if(ppgoutput == NULL){
    printf("write_modifications: output mod file %s could not be opened.\n", outfile);
    sprintf(message,"output mod file %s could not be opened", outfile);
    close_files();
    return FAILURE ;
  }
  
  
  fprintf(ppgoutput,"# Input file : %s \n",modfilename); // write line to output file
  j=1; // array counter
  while(get_next_line(mline) > 0)
    {
      if (get_mod(mline, &file_var) != SUCCESS)
	{
	  sprintf(message,"Invalid format in %s; First item must be a digit",modfilename);
	  return 0; // error
	}
      //if (debug)
	{
	  printf("\nppg_modify_file: input parameters\n");
	  printf("name:\"%s\"\n",file_var.name);
	  printf("pc:%3.3d\n",file_var.index);
	  printf("data type:\"%s\"\n",file_var.data_type);
	}
      
      if(j > n_param) // exceeds number of values in array
	{
	  printf("write_modifications: Number parameters in file %s exceeds number supplied in array (%d)\n",
		 modfilename, n_param);
	  sprintf(message,"Number of parameters in file %s exceeds number supplied in array (%d)",
		  modfilename, n_param);
	  return  FAILURE;
	}
      file_var.value = array[j];

      if (strcmp(file_var.data_type,"loopcount")==0)
	type=0;
      else if  (strcmp(file_var.data_type,"delay")==0)
	type=1;
      else
	{
	  printf("Unknown data type \"%s\" in modification file %s. Expect \"loopcount\" or \"delay\" \n",
		 file_var.data_type, modfilename);
	  sprintf(message,"Unknown data type \"%s\" in modification file %s. Expect \"loopcount\" or \"delay\" \n",
		  file_var.data_type, modfilename);
	  return  FAILURE;
	}
      printf("%d %d %d # %s\n",
	     file_var.index,(int)file_var.value,type,file_var.name); // write line to output
    
      fprintf(ppgoutput,"%d %d %d # %s\n",
	      file_var.index,(int)file_var.value,type,file_var.name); // write line to output file
      j++;
    } // while

  close_files();
  //printf("write_modifications: ppginput=%p ppgoutput=%p ppgmods=%p\n",ppginput,ppgoutput,ppgmods);
  printf("write_modifications: returning success\n");
  return SUCCESS; // success
}


void close_files(void)
{
  //printf("ppgmods,ppgoutput,ppginput %p,%p,%p\n",ppgmods,ppgoutput,ppginput);
  if( ppgmods != NULL)
    fclose (ppgmods);
  //printf ("close_files: closed ppgmods (%p)\n",ppgmods);
  if( ppgoutput != NULL)
    fclose (ppgoutput);
  //printf ("close_files: closed ppgoutput(%p)\n",ppgoutput);
  if( ppginput != NULL)
    fclose (ppginput);
  //printf ("close_files: closed ppginput(%p)\n",ppgoutput);
  ppgmods=ppgoutput=ppginput=NULL;
  //printf("ppgmods,ppgoutput,ppginput %p,%p,%p\n",ppgmods,ppgoutput,ppginput);
  //printf ("close_files: success\n");
}
