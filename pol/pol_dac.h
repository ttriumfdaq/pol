/*  pol_dac.h 

parameters for POL's GALIL RIO  DAC/ADC

*/
#ifndef __pol_dac
#define __pol_dac


const char rio_ip[] ="192.168.1.100";

// These values are now defined in the odb under /Equipment/pol_acq/settings/input/galil_rio
// They are assigned in subroutine 
INT dac_output_chan;   // dac channel for stepping the voltage
INT dac_monitor_chan;  // dac channel used for monitoring (non-zero when dac_output_chan is stepped).
INT num_dac_chan; // TOTAL number of dac channels to write
  
// The num_adc_chan ADC channels addresses and range code are in the odb arrays  
//        ps.input.galil_rio.adc_params.adc_channels[*] and  ps.input.galil_rio.adc_params.adc_range[*]

// The following 2 channels must first and second in the list of ADC channels
INT adc_chan_a; // ADC channel reserved for direct readback of dac_output_chan
INT adc_chan_b; // ADC channel reserved for users' readback of output voltage

// Other ADC channels the user may want to read (output in CYCL bank)
// These follow the reserved 2 channels above in the list of ADC channels
INT num_adc_chan;  // TOTAL number of adc channels to read

// NOTE At present, these channels must be ADC channels 0-3 because of Donald's averaging pgm

// DAC output chan Range (default =/-10V )
float max_DAC; 
float min_DAC;




#endif //  __pol_dac
