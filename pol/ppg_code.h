/* function prototypes */
#ifndef _PPGCODE_INCLUDE_H_
#define _PPGCODE_INCLUDE_H_

#ifdef HAVE_PPG 
  INT ppg_load(char *ppgfile);
  INT init_ppg(void);
  INT setup_ppg(void);
  INT modify_ppg_loadfile(char *ppgmode);
  INT check_PPG_input_params(void);
  void check_ppg_running(void);
#endif
#endif
