// Test program for POL  VMIC DAC and ADC

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "mvmestd.h"
#include <unistd.h>
#include "pol_adcdac.h"

/*****************************************************************/
/*
Read  register value
*/
static uint32_t regRead8(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A16_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/

/*
Write  register value
*/
static void regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A16_ND );
  mvme_set_dmode(mvme, MVME_DMODE_D8);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/

/*
Write  register value (16 bit)
*/
static void regWrite16(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A16_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/

/*
Read  register value
*/
static uint32_t regRead16(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A16_ND);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/

void setup_DAC(MVME_INTERFACE *mvme,DWORD base)
{
 
  regWrite16(mvme, base, DAC_V4800_CONTROL_STATUS, DAC_CSR_LED_OFF ); // setup CSR with LED off
  regWrite16(mvme, base, DAC_V4800_CHAN_0, 0 ); // setup DAC Chan 0 to 0V
}

void setup_ADC(MVME_INTERFACE *mvme,DWORD base)
{
  regWrite8(mvme, base, ADC_V3801_CONTROL_STATUS, ADC_CSR_LED_OFF ); // setup CSR with LED off
}


void  DAC_Status(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t data;
  int i;

  printf("DAC_V4800 at base 0x%x\n", (int)base);
  data =  0xFFFF &  regRead16(mvme,base,DAC_V4800_BOARD_ID); // 16 bits
  printf("ModuleID : 0x%x\n", data);
  if(data != DAC_BOARDID)
    printf("Unexpected DAC board id (0x%x). Expect 0x%x\n",data, DAC_BOARDID);

  data = 0xFFFF & regRead16(mvme,base, DAC_V4800_CONTROL_STATUS); // 16 bits
  printf("DAC CSR: 0x%x\n", data );
  if(data & DAC_LED_OFF_BIT)
    printf ("LED            off\n");
  else
    printf ("LED             on\n");

  if(data &  DAC_OUTPUT_EN_BIT)
    printf ("Output            enabled\n");
  else
    printf ("Output            disabled\n");
  
  if (data &  DAC_TWOS_COMP_BIT )   
    printf ("2s Compliment     enabled\n");
  else
    printf ("2s Compliment     disabled\n");
    
  for (i=0;i<8; i++)
    {
      if(data & 1<<i)
	printf("Channel %d busy\n",i);
      else
	printf("Channel %d not busy\n",i);
    }


  printf("\nChannel 0: 0x%x\n", (0xFF & regRead16(mvme, base,DAC_V4800_CHAN_0 ))); // 16 bits
}

void  ADC_Status(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t data;
  int i;

  printf("ADC_V3801 at base 0x%x\n", (int)base);
  data =  0xFF &  regRead8(mvme,base, ADC_V3801_BOARD_ID); // 8 bits
  printf("ModuleID : 0x%x\n", data);
  if(data != ADC_BOARDID)
    printf("Unexpected ADC board id (0x%x). Expect 0x%x\n",data, ADC_BOARDID);

  data =   0xFF & regRead8(mvme,base, ADC_V3801_CONFIG ); // 8 bits
  printf("Config Reg: 0x%x\n",data);
  if(data != 0)
    printf("Unexpected Config reg value (0x%x). Expect 0\n",data);


  data = 0xFF & regRead8(mvme,base, ADC_V3801_CONTROL_STATUS );
  printf("ADC CSR: 0x%x\n", data);
  
  if(data & ADC_LED_OFF_BIT)
    printf ("LED            off\n");
  else
    printf ("LED             on\n");


  if(data & ADC_TWOS_COMP_BIT)
    printf ("2s Compliment     enabled\n");
  else
    printf ("2s Compliment     disabled\n");


  for (i=0;i<3; i++)
    {
      if(data & 1<<(i+3)) 
	printf("Mode %d is set\n",i);
      else
	printf("Mode %d not set\n",i);
    }


  printf("Channel 1: 0x%x\n", ( 0xFFFF & regRead16(mvme, base, ADC_V3801_CHAN_1 ))); // 16 bits
}


int convert(float volt,  uint32_t *data )
{ // convert Volts to 2s compliment for DAC
    int n,m;
  

    if  (volt > 9.996)
      {
	printf("convert: Value %f is out of range. Maximum is 9.9951 V\n",volt);
	return -1;
      }
    else if  (volt < -10 )
      {
	printf("convert: Value %f is out of range. Minimum is -10.0 V\n",volt);
	return -1;
      }
    else if (volt >= 9.9951) 
      {    
	m=0x7FF; // set to max to avoid rounding error
      	printf("Converting %f volts to  2s comp = 0x%x\n",volt,m);
	*data = m;
	return 0;
      }

    else if( volt == -10)
      {
	m=0x800; // set to min to avoid possible rounding error
      	printf("Converting %f volts to  2s comp = 0x%x\n",volt,m);
	*data = m;
	return 0;
      }
 
    n = volt * 0x800 /10;
    m= n & 0xFFF;
    printf("Converting %f volts to  2s comp = 0x%x\n",volt,m);
    *data = m;
   
    return 0;
}


float retrieve(int data )
{
  float lsb= 10.0 / (float)0x800; // volts
  float volt;

  printf("retrieve: starting with data=0x%x or %d\n",data,data);

  if(data & 0x800)
      data=data | 0xFFFFF000; // sign extend
   
  volt = (float)data * lsb;
 
  return volt;
}



#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {
  int i;
  uint32_t DAC_BASE  = 0x4000;
  uint32_t ADC_BASE  = 0x3100;
  MVME_INTERFACE *myvme;
  int status;
  uint32_t data;
  float voltage;
  int rdata;

  // if (argc>1) {
  //  sscanf(argv[1],"%x", &SIS3820_BASE);
  // }

  // Test under vmic
  status = mvme_open(&myvme, 0);
  setup_DAC(myvme, DAC_BASE);
  DAC_Status(myvme, DAC_BASE);

  setup_ADC(myvme, DAC_BASE);
  ADC_Status(myvme, DAC_BASE);

#if 0
  // User LED test
  for (;;) {
    regWrite8(myvme, ADC_BASE, ADC_V3801_CONTROL_STATUS , ADC_CSR_LED_ON );
    regWrite16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS , DAC_CSR_LED_OFF );


    data =  (regRead8(myvme, ADC_BASE, ADC_V3801_CONTROL_STATUS )& 0xFF); // 8 bits
    printf("DAC status:%4.4x   ADC status:%2.2x\n", 
	   (regRead16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS  ) & 0xFFFF),
	   data);

    sleep(1);
    regWrite8(myvme, ADC_BASE,  ADC_V3801_CONTROL_STATUS , ADC_CSR_LED_OFF );
    regWrite16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS , DAC_CSR_LED_ON );

    data = (regRead8(myvme, ADC_BASE, ADC_V3801_CONTROL_STATUS)& 0xFF); // 8 bits
    printf("DAC status:%4.4x   ADC status:%2.2x\n",
	   (regRead16(myvme, DAC_BASE, DAC_V4800_CONTROL_STATUS)  & 0xFFFF ),
	   data);
    

    sleep(1);
  }
#endif

#if 1  
  // step voltage up on DAC Ch 0; read on ADC Ch 1

  voltage=-9.995;

  for (i=0; i<20; i++)
    {

      voltage = voltage + 1.0 ;
      status = convert (voltage, &data);
      if(status != 0)
	{
	  printf("Error return from convert at voltage %f\n",voltage);
	  sleep( 1);
        }
      else
	{
	  regWrite16(myvme,DAC_BASE, DAC_V4800_CHAN_0, data ); // write 2s comp to DAC Chan 0 
	  sleep(1);
          rdata =(int)  regRead16(myvme, ADC_BASE, ADC_V3801_CHAN_1 ),
	  printf("Wrote %f Volts (0x%4.4x) to DAC Chan 0. Read back ADC Chan 1 as 0x%4.4x or %f Volts\n",
		 voltage,data, rdata, retrieve(rdata) ); // 16 bits
	  sleep (1);


	}
    }
#endif

  return 1;

}
#endif
