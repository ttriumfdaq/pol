/*   tppg.h
     Author: Suzannah Daviel 

     Include file for VMEIO PPG (Pulse Programmer) for TITAN

  $Id: newppg.h,v 1.1 2011/04/15 19:26:30 suz Exp $
*/
#ifndef _TPPG_INCLUDE_H_
#define _TPPG_INCLUDE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef long unsigned int       DWORD;
typedef long unsigned int  BOOL;
#endif /* MIDAS_TYPE_DEFINED */
#include "vmicvme.h"

#define MINIMAL_DELAY 3 // 3 clock cycles 
// Masks
  

// Registers

#define TPPG_CSR_REG         0x00   /*      DWORD   RW  */
#define TPPG_TEST           0x04   /*      DWORD   RW  */
#define TPPG_ADDR            0x08   /*      DWORD   RW  Address 1-128   */
#define TPPG_DATA_LO         0x0C   /*      DWORD   RW  data bits 0-31  */
#define TPPG_DATA_MED        0x10   /*      DWORD   RW  data bits 32-63 */
#define TPPG_DATA_HI         0x14   /*      DWORD   RW  data bits 64-95 */
#define TPPG_DATA_TOP        0x18   /*      DWORD   RW  data bits 117-96 */
#define TPPG_POL_MASK        0x1C   /*      DWORD   RW  */
#define TPPG_FIRMWARE_ID     0x20   /*      DWORD   R  */

// Masks for CSR Bits in  TPPG_CSR_REG
#define TPPG_RUN_MASK     0x1E
#define TPPG_CLOCK_MASK   0x1D
#define TPPG_TRIG_MASK   0x1B
#define TPPG_SLOW_CLOCK_MASK   0x17
#define TPPG_TEST_MODE_MASK   0x0F
#define TPPG_STATUS_READBACK_MASK 0xFFFFFFE0 /* mask away all but status readback bits (5-31) */
#define TPPG_STATUS_MASK 0x1F /* mask away all but bits 0-4 */
// Instructions
#define HALT 0
#define CONT 1
#define LOOP 2
#define ENDLOOP 3
#define JSR 4
#define RETURN 5
#define BRANCH 6


//  firmware date
#define FWTBUFSIZ 128
#define FWDATESTRLEN 13 /* Length of formatted data string */

#define MAX_DELAY_COUNT 0xFFFFFFFF
#define MAX_LOOP_COUNT  0xFFFFF

// static const char *instructions[]={"HALT","DELAY_SET","LOOP","ENDLOOP","JSR","RETURN","BRANCH"};
// moved to TPPGReadIns in newppg.c to get rid of warning message

void  TPPGInit(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD bitpat );

int   TPPGLoad(MVME_INTERFACE *mvme,const DWORD base_adr, DWORD pc_offset, char *file);

void  TPPGDisable(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGEnable(MVME_INTERFACE *mvme, const DWORD base_adr);
DWORD  TPPGStatusRead(MVME_INTERFACE *mvme, const DWORD base_adr);
//void  byteOutputOrder(PARAM data, char *array);
DWORD  TPPGRegWrite(MVME_INTERFACE *mvme, const DWORD base_adr, 
			 DWORD reg_offset, DWORD value);
DWORD  TPPGRegRead(MVME_INTERFACE *mvme, const DWORD base_adr, 
			 DWORD reg_offset);

int  TPPGDisableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr);
int  TPPGEnableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr);
int TPPGEnableExtClock(MVME_INTERFACE *mvme, const DWORD base_adr);
int TPPGDisableExtClock(MVME_INTERFACE *mvme, const DWORD base_adr);
int TPPGEnableSlowClock(MVME_INTERFACE *mvme, const DWORD base_adr);
int TPPGDisableSlowClock(MVME_INTERFACE *mvme, const DWORD base_adr);
int TPPGEnableTestMode(MVME_INTERFACE *mvme, const DWORD base_adr);
int TPPGDisableTestMode(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGStopSequencer(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGStartSequencer(MVME_INTERFACE *mvme, const DWORD base_adr);
DWORD TPPGPolmskRead(MVME_INTERFACE *mvme, const DWORD base_adr);
DWORD TPPGPolmskWrite(MVME_INTERFACE *mvme, const DWORD base_adr, const DWORD pol);
DWORD TPPGSetIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr, DWORD instruction, 
		 DWORD setbits, DWORD clrbits, DWORD delay);
COMMAND TPPGReadIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr);
char *firmware_date(time_t val);
int getcmd(char *command);
int TPPGCheckIns (MVME_INTERFACE *mvme, const DWORD base_adr, COMMAND *command_info);
#ifdef GONE // not needed now stop works
void TPPGOverwritePgm(MVME_INTERFACE *mvme, const DWORD base_adr,  unsigned int pc, int num);
void TPPGHaltPgm(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGStopSequencerEOC(MVME_INTERFACE *mvme, const DWORD base_adr); // doesn't work
#endif

#endif
