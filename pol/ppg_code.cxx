/* ppg_code.cxx

   included by pol_fevme.cxx

*/

INT  init_ppg(void)
{
  INT size,status;

#ifdef NEW_PPG
  DWORD data;
#else
  BYTE data;
#endif

  printf ("\ninit_ppg: setting up PPG...\n");
  /* Stop the PPG sequencer  */ 
#ifdef NEW_PPG
  TPPGStopSequencer(gVme,   gPpgBase); // Halt pgm and stop
#else
  VPPGStopSequencer(gVme,   gPpgBase); 
#endif
  ppg_running = FALSE; // not running 
  size = sizeof(ppg_running); 
  status = db_get_value(hDB, hOut, "PPG running", &ppg_running, &size, TID_BOOL, TRUE); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"init_ppg","cannot get value of \"PPG running\" flag (%d)",status); 
      return status;
    } 
  status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"init_ppg","cannot clear \"PPG_running\" flag (%d)",status); 
      return status;
    }
  //printf("init_ppg: set the value for PPG running to %d\n",ppg_running);
  
  set_ppg_alarm(FALSE); // disable PPG alarm 
#ifdef NEW_PPG
  TPPGInit( gVme,  gPpgBase, 0); // internal trig,clock set pol mask to zero and PC back to zero 
#else
  VPPGInit( gVme,  gPpgBase); // PPG controls POL output (helicity), disable external trig, beam off (VME control)
  VPPGBeamOff(gVme,  gPpgBase);
#endif

  if(!bor_flag)
    { // NOT at begin run - this routine has been called from frontend_init
      // see if we can read something from the PPG 
#ifdef NEW_PPG
      data = TPPGStatusRead(gVme,gPpgBase); // prints out status
#else 
      data = VPPGStatusRead(gVme,gPpgBase); // prints out status
      data = VPPGExtTrigRegRead(gVme, gPpgBase); // data=0 enabled or 1=disabled
#endif // NEW_PPG 
      printf("init_ppg: success: returning without loading PPG\n");
      return SUCCESS;  // no need to load PPG from frontend_init
    }

  if (ps.input.sis3820.sis_mode == 0)
    return SUCCESS;  //  PPG is not needed for SIS test mode

 
  // Load the PPG file
  if(ppg_load(ppgfile) != SUCCESS) 
    return FE_ERR_HW;
  printf("init_ppg: PPG file loaded successfully\n");

  ss_sleep(1000);
#ifdef NEW_PPG
  data =  TPPGStatusRead( gVme, gPpgBase); // writes status of clock and trigger
#endif

  ss_sleep(1000);

  status = setup_ppg(); // set up int/ext trig, clock
  if(status != SUCCESS)
    return status;
      
#ifdef NEW_PPG
  data =  TPPGStatusRead( gVme, gPpgBase); // writes status of clock and trigger
#else
  data =  VPPGStatusRead( gVme, gPpgBase);
#endif // NEW_PPG
  return SUCCESS;
}


INT setup_ppg(void)
{
  // set up PPG with internal/external trigger and start
  DWORD rpol,status; //pol 

  printf ("setup_ppg : ODB Parameters to control PPG: \n");
  printf("PPG polarity mask:    %d (0x%x)\n",ps.input.polarity, ps.input.polarity);
  printf("PPG external start:   %d\n",ps.input.ppg_external_start);

#ifdef NEW_PPG
  printf("PPG external clock:   %d\n",ps.input.ppg_external_clock );

  DWORD data;
  if(ps.input.ppg_external_start)
    {
      printf("Setting PPG with External Start... ");
      status = TPPGEnableExtTrig(  gVme, gPpgBase);
    }
  else
    {
      printf("Setting PPG with Internal Start... ");
      status = TPPGDisableExtTrig(  gVme, gPpgBase);
    }
  if(status != SUCCESS)
    {
       // might be the readback...
      //cm_msg(MERROR,"setup_ppg","Error setting PPG Internal/External Start mode");
       printf("setup_ppg: readback error setting PPG Internal/External Start mode\n");
       printf("continuing anyway...\n");
       //return status;
    }

  if(ps.input.ppg_external_clock)
    {
      printf("Setting PPG with External Clock... ");
      status = TPPGEnableExtClock(  gVme, gPpgBase);
    }
  else
    {
      printf("Setting PPG with Internal Clock... ");
      status = TPPGDisableExtClock(  gVme, gPpgBase);
    }
  if(status != SUCCESS)
    {
      // might be the readback...
      //cm_msg(MINFO,
      printf("setup_ppg:  readback error setting PPG Internal/External Clock mode\n");
      printf("continuing anyway\n");
      //return status;
    }

  printf("setup_ppg: Reading CSR of PPG...\n");
  data =  TPPGStatusRead( gVme, gPpgBase); // displays status of clock and trigger

  printf("setup_ppg: Writing PPG polarity mask: 0x%x\n",ps.input.polarity);
  rpol = TPPGPolmskWrite( gVme, gPpgBase , ps.input.polarity); 

  
#else  // OLD PPG
  BYTE data;
  if(ppg_external_trig)
    {
      printf("Setting PPG with External Trigger\n");
      data = VPPGEnableExtTrig(  gVme, gPpgBase);
    }
  else
    {
      printf("Setting PPG with Internal Trigger\n");
      data = VPPGDisableExtTrig ( gVme, gPpgBase );
    }
  
  printf("PPG trigger control reg : 0x%x   (0=External trigger 1=Internal)\n",data);
  //printf("setup_ppg: Writing PPG polarity mask: 0x%x\n",pol);
  rpol = VPPGPolmskWrite( gVme, gPpgBase , ps.input.polarity); 
#endif

  //printf("setup_ppg: Read back PPG polarity mask: 0x%x\n",rpol); 
  if (rpol !=ps.input.polarity )
    {
      cm_msg(MERROR,"setup_ppg","PPG Polarity mask Error: wrote 0x%x, read back 0x%x",
	    ps.input.polarity ,rpol);
      return FE_ERR_HW;
    }
  else
    cm_msg(MINFO,"setup_ppg","PPG Outputs Polarity mask: 0x%x",ps.input.polarity);

  return SUCCESS;
  printf("setup_ppg: returning SUCCESS\n\n");
}


INT ppg_load(char *ppgfile) 
{ 
  /* download ppg file
   */ 
 
  /* Stop the PPG sequencer  */ 
#ifdef NEW_PPG
  //TPPGStopSequencer(gVme, gPpgBase);
  if(TPPGLoad(gVme, gPpgBase, 0, ppgfile) != SUCCESS) // TPPGLoad stops sequencer
#else  
  VPPGStopSequencer(gVme,   gPpgBase); 
  /* tr_precheck checked that tri_config has run recently */ 
  if(VPPGLoad(gVme, gPpgBase, ppgfile) != SUCCESS)
#endif 
    { 
      cm_msg(MERROR,"ppg_load","failure loading ppg with file %s",ppgfile); 
      return FE_ERR_HW; 
    } 
  printf("\nppg_load: PPG file %s successfully loaded",ppgfile); 
  //  cm_msg(MINFO,"ppg_load","PPG file %s successfully loaded",ppgfile); 
  return SUCCESS; 
} 

INT modify_ppg_loadfile(char *mode)
{
  DWORD parameters[10];
  char modfilename[80],modfile[80];
  char filename[80];
  int i,status,size;
  char names[10][32];
  char message[256];
  for (i=0; i<10; i++)
    parameters[i]=0;
  
  if(strcmp(mode,"1h")==0)
    {  
      parameters[0] = 6; // number of parameters
      parameters[1] = (DWORD)ps.input.scan_loop_count ; // scan loop counter (normally 1)
      parameters[2] = (DWORD)num_bins; // number of bins
      parameters[3] = lne_count ; // lne count (width of lne pulse in counts)
      parameters[4] = dw_count ; // dw_count (dwell time - lne width)
      parameters[5] = dac_sleep_count ; // daq_service = tof delay
      parameters[6] = tof_width_count ; // daq_service pulse = tof pulse



      // Debugging - write these to the Output area
        size=sizeof(parameters);
	status = db_set_value(hDB, hOut, "newppg/1h_params/parameters", &parameters, size, 10, TID_DWORD);
	if(status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"modify_ppg_loadfile",
		   "cannot set /Equipment/POL_ACQ/Settings/Output/newppg/1h_params/parameters (%d)",status); 
	    return status;
	  }

        // Read the name so they can be output to screen
        size=sizeof(names);
	status = db_get_value(hDB, hOut, "newppg/1h_params/parameter_names", &names, &size, TID_STRING, FALSE);
	if(status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"modify_ppg_loadfile",
		   "cannot get /Equipment/POL_ACQ/Settings/Output/newppg/1h_params/parameter_names (%d)",status); 
	    return status;
	  }
    }

  else
    {
      cm_msg(MINFO,"modify_ppg_loadfile","Unknown PPG mode %s",mode);
      return DB_INVALID_PARAM;
    }

  
  printf(" modify_ppg_loadfile:  PPG Mode %s  Parameters \n",ppgmode);
  printf("     Number of parameters: %d\n",parameters[0]); // e.g. 5 = 5 parameters to follow

  for (i=1; i >= (int)parameters[0]; i++)
    printf("        parameter %d %s :  %d (0x%x)\n",i,names[i],parameters[i],parameters[i]);
  
  sprintf(filename,"%s/ppg-templates/%s.dat",ps.input.ppg_path,mode);
  sprintf(modfile,"%s/ppgload/ppgmods_%s.dat",ps.input.ppg_path,mode); // output file with PC counters and parameter values
  sprintf(modfilename,"%s/ppg-templates/mode%s_mods.dat",ps.input.ppg_path,mode); // input file with PC counters

  status =  write_modifications(modfilename, modfile, parameters, message);
  if (status != SUCCESS)
    {
      printf("modify_ppg_load: error from write_modifications");
      printf("message: %s\n",message);
      cm_msg(MINFO, "modify_ppg_load","%s",message);
      cm_msg(MERROR,"modify_ppg_load","Could not write modification file %s",modfilename);
      return DB_INVALID_PARAM;
    }
  else
    printf("\n\nmodify_ppg_loadfile: Now applying modifications to ppg template file...\n");

    printf("filename: %s \n ppgfile %s  \n modfile %s\n",filename,ppgfile,modfile);
  status = ppg_modify_file(filename, ppgfile, modfile);
  if (status != SUCCESS)
      return DB_INVALID_PARAM;
  else
    printf("success from  modify_ppg_loadfile\n");
  return status; 
}


INT check_PPG_input_params(void)
{

  // called from tr_prestart

  double dwell_time,lne_pw,dtmp;
  INT status,size;
  char str[80];
  INT LNE_freq;
  // Mode 1h
  double pw,dt;
  INT lc;
  float SIS_min_dwell_time_ms; // depends on number of data format bits (and number of channels enabled)

  // ODB output params
  float  fdwt;
  float  one_cycle_time;
  
  // Check parameters common to all ppg modes
  printf("check_PPG_input_params: Common input parameters:\n");
  printf("Run tri_config      %d\n",ps.input.run_tri_config);
  printf("LNE prescale factor %d\n",ps.input.sis3820.lne_prescale_factor);
  printf("Dwell time (ms)     %f\n",ps.input.dwell_time__ms_);
  dwell_time = ps.input.dwell_time__ms_ ;
#ifndef NEW_PPG
  if(!ps.input.run_tri_config)
    {
      cm_msg(MERROR,"check_PPG_input_params",
	     "tri_config must be run when using old PPG");
      return DB_INVALID_PARAM;
    }
#endif


  /* this value written by tri_config (if run)
     Depends on frequency of PPG. If this is changed, tri_config should be run first to update this value.
  */
  printf("Minimal delay (ms)     %f\n",ps.output.minimal_delay__ms_);



  
  // check critical parameters 
  //printf("check_PPG_input_params: min_delay=%f\n", min_delay);
  if (ps.output.minimal_delay__ms_ <= 0)
    {
      cm_msg(MERROR,"check_PPG_input_params",
	     "Invalid minimal delay for PPG (%f ms) read from ODB (filled by tri_config)",ps.output.minimal_delay__ms_);
      return DB_INVALID_PARAM;
    }

  // Check LNE_prescale_factor is > 0
  if(ps.input.sis3820.lne_prescale_factor == 0)
    {
      cm_msg(MINFO,"check_PPG_input_params","value of \"%s\" must be larger than 0 (1=no prescale). Using default value of 1.",str); 
      ps.input.sis3820.lne_prescale_factor = 1;
    }
  // ppgmode for pol is fixed at 1h
  // printf("check_PPG_input_params: ppgmode is %s\n",ppgmode);
  
  
#ifdef NEW_PPG
  if(!ps.input.run_tri_config)
    {     // tri_config won't have changed this
      if(ps.output.time_slice__ms_ < 0) 
	{
	  cm_msg(MERROR,"check_PPG_input_params","Invalid time slice (%f)\n",ps.output.time_slice__ms_);
	  return DB_INVALID_PARAM;
	}
    }
#endif
  
  
  // PPG MODE 1h  PPG supplies LNE pulses in a loop
 
  if(num_bins <= 0)  // num_bins read from odb already
    {
      cm_msg(MERROR,"check_PPG_input_params","Illegal number of bins (%u)",num_bins);
      return  DB_INVALID_PARAM;
    }
  
  // Using the PPG to produce the LNE pulses - LNE_prescale_factor must be 1 (avoids complications)
  if(ps.input.sis3820.lne_prescale_factor > 1)
    {
      cm_msg(MERROR,"check_PPG_input_params","LNE prescale factor (%u) must be 1 in PPG Mode 1h",
	     ps.input.sis3820.lne_prescale_factor);
      return DB_INVALID_PARAM;
    }
  
  


  /* NOTE
     Minimal delay for PPG clocked at 100mHz is 50ns (for old PPG - 5 clock cycles).  1 clock cycle = 10ns 
     Minimum width for SIS LNE is 10ns. Therefore use minimum delay for LNE pulse width.
     Frequency of LNE pulses is restricted by SIS3820 to 5MHz. Min dwell time is  SIS_min_dwell_time_ms (200ns)
     
     Dwell time = LNE pulse width + time between falling and rising edges of the LNE pulses
     (see 1f timing diagram)  
  */

  // get LNE pulse width from PPG area (should be fixed but could have been changed by user)
  sprintf(str,"pulse_lne/pulse width (ms)");
  size = sizeof(lne_pw);
  status = db_get_value(hDB, hPPG, str, &lne_pw, &size, TID_DOUBLE,FALSE); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"check_PPG_input_params","cannot get LNE pulse width at %s (%d)",str,status); 
      return status;
    }      
  printf("LNE pulse width is %f ms\n",lne_pw);
      
      
  /*
            Running tri_config
  */
  if(ps.input.run_tri_config)
    { 
      printf("check_PPG_input_params: triconfig will be run\n");
      /* write the correct number of bins required to PPG cycle loop count
	 if the LNE_prescale_factor > 1, we would have to produce more bins to get the correct number of LNE_pulses
	 so LNE_prescale_factor is fixed to 1 for this mode
      */
      lc = (INT)(num_bins * ps.input.sis3820.lne_prescale_factor); // LNE_prescale_factor = 1 for this mode
      printf("check_PPG_input_params: writing PPG cycle loop count as %d (num_bins=%u, LNE_prescale_factor = %u)\n",
	     lc,num_bins,ps.input.sis3820.lne_prescale_factor );
      // normally write (num_bins) to PPG cycle loop count, but write one extra that will be ignored (just to be safe)

       
      sprintf(str,"begin_cycle/loop count");
      status = db_set_value(hDB, hPPG, str, &lc, sizeof(lc), 1, TID_INT); 
      if(status != DB_SUCCESS) 
	{ 
	  cm_msg(MERROR,"check_PPG_input_params","cannot set ODB key \"%s\" to %d  (%d)",str,lc, status); 
	  return status;
	}
	  
      // Minimal dwell time for 5MHz LNE pulses is 0.0002ms (max LNE freq = 50MHz)
        if(ps.input.sis3820.data_format == 8)
	  SIS_min_dwell_time_ms = SIS8_min_dwell_time_ms;
	else if (ps.input.sis3820.data_format == 16)
	  SIS_min_dwell_time_ms = SIS16_min_dwell_time_ms;
	else
	  SIS_min_dwell_time_ms = SIS32_min_dwell_time_ms;

      if (dwell_time <  SIS_min_dwell_time_ms)
	{
	  cm_msg(MERROR,"check_PPG_input_params","Selected dwell time too short (%f ms). SIS3820 minimum is %f ms", 
		 dwell_time,	 SIS_min_dwell_time_ms); 
	  return DB_INVALID_PARAM;
	}
      if(dwell_time < 0)
        dwell_time  = 0;
      
      if(lne_pw < 0)lne_pw = 0;
      // using dwell_time parameter
      if (lne_pw == 0) // use minimal delay if parameter is 0 
	{
	  printf("LNE pulse width will be set to minimal delay (%f) by tri_config\n",ps.output.minimal_delay__ms_);
	  lne_pw = (double)(ps.output.minimal_delay__ms_);
	}
      //printf("LNE pulse width is %f ms\n",lne_pw);
      
      dtmp = ps.output.minimal_delay__ms_ + lne_pw; // minimum dwell time
      if (dwell_time < dtmp)
	{
	  cm_msg(MERROR,"check_PPG_input_params",
		 "Setting dwell time for PPG to (%f ms). Must be at least (LNE pulse width (%fms) + minimal delay (%f ms) i.e. %fms)",
		 dwell_time,lne_pw,ps.output.minimal_delay__ms_,dtmp);
	  dwell_time = dtmp;
	}
      dwell_time = dwell_time - lne_pw;  // subtract LNE pulse width
      
      // setting values for dwell time; use end_cycle offset for dwell time and min-delay for width see timing diag
      sprintf(str,"end_cycle/time offset (ms)");
      status = db_set_value(hDB, hPPG, str, &dwell_time, sizeof(dwell_time), 1, TID_DOUBLE); 
      if(status != DB_SUCCESS) 
	{ 
	  cm_msg(MERROR,"check_PPG_input_params","cannot set ODB key \"%s\" to %f  (%d)",str,dwell_time, status); 
	  return status;
	}
    } // end of ps.input.run_tri_config = TRUE   


#ifdef NEW_PPG

  /*
            Not running tri_config 
                
                   - parameters will loaded into PPG loadfile
  */


  else
    { // NOT running tri_config

      // use dwell time (ms) parameter
      // convert time values to number of cycles
      printf("check_PPG_input_params: NEW PPG, NOT running tri_config \n");
 
      int dw,lne;
      if(dwell_time < lne_pw) lne_pw = ps.output.minimal_delay__ms_;
      lne =(int)((lne_pw/ps.output.time_slice__ms_)+0.5);  // lne count
      dw = (int)((dwell_time/ps.output.time_slice__ms_)+0.5);  // total dwell time
      printf("after converting to counts, dwell time  = %d and lne pulse width  = %d counts\n",dw,lne);  
      dw-=lne;
      //printf("after subtracting lne pw, dw = %d  counts\n",dw);  	  
      // take 3 counts from both (min delay included)
      lne -= MINIMAL_DELAY;
      dw -= MINIMAL_DELAY;
	  
      if(lne < 0)lne=0;
      if(dw < 0 )dw=0;
      dw_count = (DWORD)dw;
      lne_count = (DWORD)lne;
	  
      //printf("dw_count = %d  lne_count=%d (minimal delay of 3 counts deducted)\n",dw_count,lne_count); 	  
	
      // calculate values for output
      dt = (dw_count + MINIMAL_DELAY) * ps.output.time_slice__ms_; // 3 counts minimal delay added 
      pw = (lne_count+ MINIMAL_DELAY) * ps.output.time_slice__ms_ ;
      dt = dt + pw; // total dwell time 
      
      
      printf("check_PPG_input_params: dw_count=%u lne_count=%u  dt=%f pw=%f \n",
	     dw_count,lne_count,dt,pw);
      
      // Write some output params
      fdwt = (float)dt; // float dwell time for output
      LNE_freq = (int) (1000/dt + 0.5); // Hz
      one_cycle_time =  dt * num_bins /1000; // (seconds)
      
    } // end of NOT running tri_config
#endif // NEW_PPG


  // Write some common parameters to the Output area
  sprintf(str,"/Equipment/POL_ACQ/Settings/Output/");
  printf("Writing output parameters to %s :\n dwell time = %fms one_cycle_time=%fms  LNE_freq=%dHz\n",
	 str,fdwt,one_cycle_time,LNE_freq);


  size=sizeof(fdwt);
  status = db_set_value(hDB, hOut, "dwell time (ms)", &fdwt, size, 1, TID_FLOAT);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"check_PPG_input_params","cannot set \"%sdwell time (ms)\" to %f  (%d)",str,fdwt,status); 
      return status;
    }
  
  
  size=sizeof(one_cycle_time);
  status = db_set_value(hDB, hOut, "one cycle time (sec)", &one_cycle_time, size, 1, TID_FLOAT);
  if(status != DB_SUCCESS) 
    {
      cm_msg(MERROR,"check_PPG_input_params","cannot set \"%sone cycle time (sec)\" to %f  (%d)",str,one_cycle_time,status); 
      return status;
    }

  one_cycle_time *= 1000.0; // convert to ms
  status = db_set_value(hDB, hOut, "one cycle time (ms)", &one_cycle_time, size, 1, TID_FLOAT);
  if(status != DB_SUCCESS) 
    {
      cm_msg(MERROR,"check_PPG_input_params","cannot set \"%sone cycle time (ms)\" to %f  (%d)",str,one_cycle_time,status); 
      return status;
    }
  
  size=sizeof(LNE_freq);
  status = db_set_value(hDB, hOut, "LNE frequency (Hz)", &LNE_freq, size, 1, TID_INT);
  if(status != DB_SUCCESS) 
    {
      cm_msg(MERROR,"check_PPG_input_params","cannot set \"%sLNE frequency (Hz)\" to %d  (%d)",str,LNE_freq,status); 
      return status;
    }
  
  
  //printf("check_PPG_input_params: returning success\n");
  return SUCCESS;

}

void check_ppg_running(void)
{
  // TEMP
  BYTE value;

  if(ps.input.sis3820.sis_mode == 0)  // SIS test mode 
    return;
#ifdef NEW_PPG
  value =  TPPGRegRead(  gVme, gPpgBase, TPPG_CSR_REG );
  if (value & 1)
#else
  value = VPPGRegRead( gVme, gPpgBase, VPPG_VME_READ_STAT_REG ); 
  if  (value & 2)
#endif 
    {
      if(hot_debug)printf("check_ppg_running: Pulse blaster IS running\n"); 
      ppg_running = TRUE;
    }
  else 
    { 
      if(hot_debug)printf("check_ppg_running: Pulse blaster NOT running\n"); 
      ppg_running = FALSE;
    }
  return;
}
