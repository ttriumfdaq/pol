/*  DAC_code.cxx

    This code for new GPIB hardware 
*/

#ifdef HAVE_DAC
INT set_DAC(float dac_val)
{
  INT status;
  
  /*** Set the DAC to the next value ***/

  /* 
   * New version
   *
   * Write required value to "/Equipment/pol_acq/settings/input/dac value"
   * NEW fedvm code has a hot-link on above variable
   *
   * Possibly wait for "psleep" time
   * 
   */
          
  float temp;
  temp=dvm_readback[0];  // remember the readback value
  dvm_callback=FALSE; // flag
  status = db_set_data(hDB,hReq,&dac_val,sizeof(dac_val),1,TID_FLOAT);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"set_DAC","cannot write to ODB key to update DVM (%d)",status);
      return status;
    } 

  if(hot_debug)
    printf("set_DAC: wrote dac_val=%f to hotlink \n\n",  dac_val);    


  return SUCCESS;
}

#ifdef GONE
float read_DAC(void)
{
  
  INT status,size;
  
  // dac_read is a global
  float fDAC;
  
  // return dac_read;
  
  
  //  printf("read_DAC: WARNING cannot read DAC. Using Set Value\n");
  //  dac_read=dac_val;
  size=sizeof(fDAC);
  status = db_get_value(hDB,0,"/equipment/POLDVM/Variables/GPIB[0]",&fDAC,&size,TID_FLOAT,0);
  if(status != DB_SUCCESS)
    {		  
      cm_msg(MERROR,"read_DAC",
	     "failure from db_get_value for /equipment/POLDVM/Variables/GPIB[0](%d)",status);
      return 0;
    }
  printf("read_DAC: DAC set value is %f read value is %f\n", dac_val, fDAC);
  return fDAC;  // return set value
} 
#endif // GONE



INT find_DAC_keys(void)
{
  INT status;
  KEY key;
  

  status = db_find_key(hDB, 0, "/Equipment/pol_acq/Settings/Input/DAC value", &hReq);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"find_DAC_keys","failure finding key for \"/Equipment/pol_acq/Settings/Input/DAC value (%d)\" ",status);
      return status;
    }

  /*  No read of the DAC itself */
  hVar=0;
  status = db_find_key(hDB, 0, "/Equipment/POLDVM/Variables/GPIB", &hVar);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"find_DAC_keys","failure finding key for /Equipment/POLDVM/Variables/GPIB (%d)",status);
      return status;
    }
  
  status = db_get_key(hDB, hVar, &key );
  if (status == DB_SUCCESS) 
    {
      ndvm = key.num_values;  // ndvm is global
      printf("find_DAC_keys: Number of dvm values read in \"/Equipment/POLDVM/Variables/GPIB\" is %d\n",ndvm);
    }

  /* check the size of the array dvm_readback */
  if (sizeof(dvm_readback) <  ndvm * sizeof(float))
    {
      printf("find_DAC_keys: dvm_readback array is not large enough\n");
      return FAILURE;
    }
  else
    printf("size of dvm_readback=%d ndvm* sizeof(flaot)=%d\n",sizeof(dvm_readback),(  ndvm * sizeof(float)) );

  return status;
}

INT open_hot_dvm()
{
  INT status,size;
  char str[80];

  //  opens a hot link on first DVM readback value (DAC value * some factor)
  if(hDVM==0)  // global
    {
      sprintf(str,"/Equipment/POLDVM/Variables/GPIB");
      status = db_find_key(hDB, 0, str, &hDVM);
      if(status != SUCCESS)
	{
          cm_msg(MINFO,"open_hot_dvm","find key on  \"%s\"  failed (%d)",str,status);
          return status;
	}
    }

  size = sizeof(dvm_readback);
  status = db_get_value(hDB, 0, str, &dvm_readback, &size, TID_FLOAT, 0);
  if(status != DB_SUCCESS)
    {
      cm_msg(MINFO,"open_hot_dvm","get value on  \"%s\"  failed (%d)",str,status);
      return status;
    }
  printf("open_hot_dvm: dvm_readback[0] = %f \n",dvm_readback[0]);


  status = db_open_record(hDB, hDVM, &dvm_readback
			  , sizeof(dvm_readback)
			  , MODE_READ, dvm_call_back, NULL);
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"open_hot_dvm","open record on \"%s\"  failed (%d)",str,status);

  return status;
}


#else  // DUMMY DAC
INT set_DAC(float dac_val)
{
  
  //if(hot_debug)
  printf(" ***  Set_dac: dummy dac is now set to %f Volts \n\n",dac_val);
  return SUCCESS;
}

INT read_DAC(void)
{
  dac_read = dac_val;
  printf("read_DAC: dummy DAC value read back is %f ; set value is %f\n",dac_read,dac_val);
  return SUCCESS;
}
INT find_DAC_keys(void)
{
  return SUCCESS;
}
#endif
