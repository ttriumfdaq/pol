INT  init_sis3820(void)
{

#ifdef HAVE_SIS3820
  INT data_format,status;
  DWORD test_pulse_mask=0; // SIS test mode
  DWORD enabled_channels_mask;
  int num_bits;
  unsigned int opbits,csrbits;
  DWORD LNE_prescale_register=0; // register contents
  DWORD dtmp,dbp,modid;
  DWORD sis_control_input_mode=0;
  DWORD sis_control_output_mode=0;
  char str[80];

  opbits=csrbits=0;
  
  //printf("\n init_sis3820: resetting SIS3820\n");
  sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_KEY_RESET, 1); // reset module 
  sis3820_Reset(gVme,gMcsBase);
  dtmp = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_MODID);
  modid = dtmp >> 16;
  printf("SIS3820 Module ID: 0x%x  Firmware Revision Level: 0x%x\n",modid, (dtmp & 0xFFFF));
  if (modid != 0x3820)
    {
      cm_msg(MERROR,"init_sis3820","Unexpected SIS3820 Module ID (0x%x). Expect 0x3820 ",modid); 
      return  DB_INVALID_PARAM;
    }

  sprintf(str,"input/sis3820/Module ID and Rev");
  status = db_set_value(hDB, hTASet, str, &dtmp, sizeof(dtmp), 1, TID_DWORD); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"init_sis3820","cannot write  \"sis3820/Module ID and Rev\" (%d)",status); 
      return status;
    }

  if(! bor_flag)
    { // do not set up the module if called from frontend init as parameters may be in error
      return SUCCESS;
    }

  // get some ODB parameters
  printf("\ninit_sis3820: input parameters\n");
  printf("sismode           = %d\n",ps.input.sis3820.sis_mode);
  printf("num data bits     = %d\n",ps.input.sis3820.data_format);
  printf("enabled chan mask = %d\n",ps.input.sis3820.enable_channels_mask);
  printf("MCS channel       = %d\n",ps.input.sis3820.mcs_channel__1_32_);
  num_bits = ps.input.sis3820.data_format;
  enabled_channels_mask = ps.input.sis3820.enable_channels_mask;

  data_format = get_SIS_dataformat(num_bits);

    dbp = 1<< (ps.input.sis3820.mcs_channel__1_32_ - 1);
  //if(dbp & enabled_channels_mask)
  //  {
  //   cm_msg(MERROR,"init_sis3820","MCS channel (%d) is NOT enabled",ps.input.sis3820.mcs_channel__1_32_);
  //  return DB_INVALID_PARAM;
  // }
  
  if (ps.input.sis3820.sis_mode > 0)
    { // read control modes (only relevent for non-test modes)
       
      if(ps.input.sis3820.input_mode > 6)
	{
	  cm_msg(MERROR,"init_sis_3820","illegal SIS input mode %d. Must be 0-6",
		 ps.input.sis3820.input_mode);
	  return DB_INVALID_PARAM;
	}
      if(ps.input.sis3820.output_mode > 3)
	{
	  cm_msg(MERROR,"init_sis_3820","illegal SIS output mode %d. Must be 0-3",
		 ps.input.sis3820.output_mode);
	  return DB_INVALID_PARAM;
	}
      
      //printf(" sis_control_input_mode = %d  sis_control_output_mode = %d\n",
      //sis_control_input_mode, sis_control_output_mode);
  
      sis_control_input_mode =   ps.input.sis3820.input_mode << 16;
      sis_control_output_mode =  ps.input.sis3820.output_mode << 20;
      printf("Using sis_control_input_mode = 0x%x  sis_control_output_mode = 0x%x\n",
	     sis_control_input_mode, sis_control_output_mode);
    }
  
  LNE_prescale_register =  ps.input.sis3820.lne_prescale_factor -1; // value to be written to the register
  printf("init_sis3820: input parameters...\n");
  printf("sismode = %d number of bits for data word = %d (data format = %d)\n",ps.input.sis3820.sis_mode,num_bits,data_format);  
  printf("number of bins = LNE preset = %u  \n",num_bins); // global 
  printf("LNE prescale factor = %u  \n",ps.input.sis3820.lne_prescale_factor); // global
  
  if(ps.input.sis3820.sis_mode != 2 ) // uses test input pulses
    test_pulse_mask =   odbReadUint32("/Equipment/pol_acq/settings/input/sis3820/Test pulse mask",0,0); // default is 0, no mask
  if(dbp & test_pulse_mask)
    cm_msg(MINFO,"init_sis3820","Warning: MCS channel (%d) IS masked",ps.input.sis3820.mcs_channel__1_32_); // no data from this channel
  
  NumSisChannels  = nchan_enabled(enabled_channels_mask, 1, data_bytes ); // calculate number of enabled SIS channels
  printf("NumSisChannels is %d after call to nchan_enabled with data_bytes=%d\n",NumSisChannels,data_bytes);
  // check validity of input params (not at frontend_init) 
  
  if (ps.input.sis3820.sis_mode == 0)  // SIS TEST mode
    { 
      dtmp = 10000000/ps.input.sis3820.lne_prescale_factor; // calculate actual frequency
      printf("SIS mode %d: test mode, internal LNE, prescale factor %d LNE frequency=%u Hz\n", 
	    ps.input.sis3820.sis_mode ,ps.input.sis3820.lne_prescale_factor,dtmp); // 0x18 prescale LNE pulse rate
      printf("Writing %d (0x%x) to LNE Prescale Register\n",LNE_prescale_register,LNE_prescale_register);
      sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate
      opbits =
	SIS3820_CLEARING_MODE|                           //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_8BIT|                  //   bits 2,3 8 bit data 
	data_format |                                    //   input param
	SIS3820_LNE_SOURCE_INTERNAL_10MHZ|               //   bit 4-6  LNE source 10MHz pulser
	SIS3820_FIFO_MODE|                               //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|            //   bit 28-31  MCS mode
	SIS3820_CONTROL_INPUT_MODE1| SIS3820_CONTROL_OUTPUT_MODE2 ;                   //   bit 16-18 Input mode 1; bit 20,21 Output mode 2
      
	  
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
      sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_OPERATION_MODE, opbits); // 0x100 
	  
    }
      
      
  else if (ps.input.sis3820.sis_mode == 1)  // SIS REAL LNE  with test pulses (Max LNE pulse rate 5MHz unless using prescale)
    {     // "MCS Next" signal from PPG must be plugged into input 1 of SIS scaler (LNE)

      printf("SIS mode %d: test mode with external LNE\n",ps.input.sis3820.sis_mode);
      printf("Writing %d (0x%x) to LNE Prescale Register\n",LNE_prescale_register,LNE_prescale_register);
      sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate reg
      
      opbits =     
	SIS3820_CLEARING_MODE|                    //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_32BIT|            //   bits 2,3 32 bit data 
	data_format | 
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|        //   bit 4-6  external LNE signal 
	SIS3820_FIFO_MODE|                        //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|     //   bit 28-31  MCS mode
	sis_control_input_mode |                  //   bit 16-18 Input mode 1
	sis_control_output_mode      ;            //   bit 20,21 Output mode 2
      
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
      sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_OPERATION_MODE, opbits); // 0x100 
      
      
      
    }
  else if (ps.input.sis3820.sis_mode == 2)  // SIS REAL LNE  with REAL input  pulses
    {     // MCS Next of PPG must be plugged into input 1 of SIS scaler (LNE) and input signals should be plugged into
          // the enabled scaler channels
      printf("SIS mode %d: external LNE\n",ps.input.sis3820.sis_mode);
      printf("Writing %d (0x%x) to LNE Prescale Register\n",LNE_prescale_register,LNE_prescale_register);
      sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_LNE_PRESCALE,LNE_prescale_register); // 0x18  prescale LNE pulse rate reg
      
      opbits = SIS3820_CLEARING_MODE|                    //   bit 0    set clearing mode
	//SIS3820_MCS_DATA_FORMAT_32BIT|            //   bits 2,3 32 bit data 
	data_format | 
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|        //   bit 4-6  external LNE signal 
	SIS3820_FIFO_MODE|                        //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|     //   bit 28-31  MCS mode
	sis_control_input_mode |                  //   bit 16-18 Input mode 1
	sis_control_output_mode     ;             //   bit 20,21 Output mode 1
      
      
      printf("Writing 0x%x to Operation Mode Register\n",opbits);
      sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_OPERATION_MODE, opbits); // 0x100 
      
    }
  else 
    {
      printf ("init_sis3820:  unknown SIS mode %d\n",ps.input.sis3820.sis_mode);
      cm_msg(MERROR,"init_sis3820","Unknown SIS mode");
      return DB_INVALID_PARAM;
    }
  
  if(ps.input.sis3820.sis_mode != 2)  // 2=real
    {          // sis modes with test pulses as input    
      // Enable test pulses
      csrbits =  CTRL_COUNTER_TEST_25MHZ_ENABLE |  CTRL_COUNTER_TEST_MODE_ENABLE;
      sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_CONTROL_STATUS, csrbits );
	  
      printf("Writing 0x%x (test pulse mask) to  SIS3820_TEST_PULSE_MASK (0x%x)\n",
	     test_pulse_mask, SIS3820_TEST_PULSE_MASK );
      sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_TEST_PULSE_MASK, test_pulse_mask); // mask ch(s) from test pulses
      test_pulse_mask = test_pulse_mask & ~enabled_channels_mask; // masks only enabled channels 
      nchan_enabled(test_pulse_mask, 0, data_bytes); // also displays masked test pulse channels


    }
  else
    {
      csrbits = CTRL_REFERENCE_CH1_ENABLE ;
      sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_CONTROL_STATUS, csrbits );
      printf("Ch1 Reference pulses enabled\n"); 
    }
  // All SIS modes
  sis3820_Status(gVme,gMcsBase);
      
  printf("Writing 0x%x (enabled channels mask) to  SIS3820_COPY_DISABLE (0x%x)\n",
	 enabled_channels_mask, SIS3820_COPY_DISABLE );
  sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_COPY_DISABLE, enabled_channels_mask); // enable channels 16-19 
  
  printf("Writing 0x%x (number of bins) to  SIS3820_ACQUISITION_PRESET register (0x%x)\n", 
	 num_bins,SIS3820_ACQUISITION_PRESET);
  
  sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_ACQUISITION_PRESET, num_bins); // select number of LNE       

  printf("\n");
  SIS_readback(opbits,csrbits);

#endif
  return SUCCESS;
}


INT get_SIS_dataformat(INT nbits )
{
  INT data_format;

  printf("Data word = %d bits \n",nbits);

  switch(nbits)
    {
    case 8:
      data_format = SIS3820_MCS_DATA_FORMAT_8BIT;
      data_bytes = 1; // number of bytes for each data word (global)
      if ( (NumSisChannels % 4) != 0)
	{
	  cm_msg(MERROR,"check_SIS_input_params","Enable a multiple of 4 channels for 8-bit format");
	  return DB_INVALID_PARAM;
	}
      break;
    case 16:
      data_format = SIS3820_MCS_DATA_FORMAT_16BIT;
      data_bytes = 2;  // number of bytes for each data word
      if ( (NumSisChannels % 2) != 0)
	{
	  cm_msg(MINFO,"check_SIS_input_params","an even number of channels will always enabled for 16-bit format");
	}
      break;
    case 24:
      data_format = SIS3820_MCS_DATA_FORMAT_24BIT;
      data_bytes = 3;  // number of bytes for each data word (actually 4 with userbit, ch)
      break;
    default:
      printf("Unknown SIS data format (%d bits). Setting default of 32 bits\n",nbits);
      cm_msg(MINFO,"get_SIS_dataformat","Unknown SIS data format (%d bits). Setting default of 32 bits",nbits);
   case 32:
      data_format = SIS3820_MCS_DATA_FORMAT_32BIT;
      data_bytes = 4;  // number of bytes for each data word
      break; 
    }  
  return data_format;
}





void SIS_readback(unsigned int wrote_mode, unsigned int wrote_csr)
{
 static unsigned int my_data = 0;

 printf( "\n=== SIS_readback:  (sismode=%d) ===\n",ps.input.sis3820.sis_mode);
 my_data = sis3820_RegisterRead(gVme,gMcsBase,SIS3820_LNE_PRESCALE); // 0x18  prescale LNE 
 printf( "lne prescale reg = %d or 0x%x; prescale factor = %d\n",my_data,my_data,(my_data+1)); // no prescale, my_data=0
 my_data = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_ACQUISITION_PRESET);
 printf( "lne preset = %d or 0x%x\n",my_data,my_data);
 my_data = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_TEST_PULSE_MASK);
 printf( "Test pulse mask = %d or 0x%x\n",my_data,my_data);
 my_data = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_COPY_DISABLE);
 printf( "Copy disable = %d or 0x%x\n",my_data,my_data);


 my_data = sis3820_RegisterRead(gVme,gMcsBase,SIS3820_OPERATION_MODE);
 if(wrote_mode > 0)
   printf("Wrote 0x%x to OP mode reg; read back 0x%x\n",wrote_mode, my_data);
 else
   printf("Read 0x%x from OP mode reg\n",my_data);

 opmode_bits(my_data);


 my_data = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_CONTROL_STATUS);
 if(wrote_csr > 0)
   printf("\nWrote 0x%x to CSR; read back 0x%x\n",wrote_csr, my_data);
 else
  printf("\nRead 0x%x from CSR\n",my_data);
 
 csr_bits(my_data);


 return;
}
void opmode_bits(unsigned int data)
{
  unsigned int tmp;

  printf("\nSIS Operation Mode bits register 0x%x : 0x%x\n",SIS3820_OPERATION_MODE,data);
  if(data & SIS3820_NON_CLEARING_MODE)
    printf("Clearing mode:              N ");
  else
    printf("Clearing mode:              Y ");

  // data format
  tmp = (data >> 2) & 0x3;
  printf("  Data format:  ");
  if (tmp == 0) 
    printf(" 32-bit Mode ");
  else if (tmp == 1)
    printf(" 24-bit Mode ");	
  else if (tmp == 2)
    printf( "16-bit Mode ");	
  else if (tmp == 3)
    printf("  8-bit Mode ");	
  else 
    printf("       error ");


  // LNE source
  tmp = (data  >> 4) & 7 ;
  printf("  LNE source: "); 
  if (tmp == 0) 
    printf("       Internal ");
  else if (tmp == 1)
    printf("    Front Panel ");
  else if (tmp == 2)
    printf("10MHz testpulse ");
  else if (tmp == 3)
    printf("      Channel N ");
  else if (tmp == 4)
    printf("Preset Scaler N ");
  else 
   printf("           error ");

  printf("\n");

  // ARM/Enable source
  tmp = (data  >> 8) & 3 ;
  printf("Arm/Enable source: ");
  if (tmp == 0) 
    printf(" LNE input ");
  else if (tmp == 1) 
    printf(" Channel N ");
  else
    printf("  Reserved ");

  // SDRAM
  tmp = (data  >> 12) & 3 ;
  printf("  SDRAM add:    ");
  if (tmp == 0) 
    printf("       FIFO ");
  else
    printf("      SDRAM ");

  // HISCAL start
  tmp = (data  >> 14) & 1 ;
  printf("  HISCAL Start: ");
  if (tmp == 0) 
    printf("     internal "); // VME
  else
    printf("     external "); // signal

     printf("\n");
   

  // Control Input mode
  tmp = (data  >> 16) & 7 ;
  printf("Control Input Mode:         %d ",tmp);
  
  // Control Input inverted
  tmp = (data  >> 19) & 1 ;
   printf("  Inverted:               ");
  if(tmp)
    printf("Y ");
  else
    printf("N ");
 
  printf("\n");
 
  // Control Output mode
  tmp = (data  >> 20) & 3 ;
  printf("Control Output Mode:        %d ",tmp);
  
  
  // Control Output inverted
  tmp = (data  >> 23) & 1 ;
  printf("  Inverted:               ");
  if(tmp)
    printf("Y ");
  else
    printf("N ");
  
    printf("\n");
  // Operation mode
  tmp = (data  >> 28) & 7 ;
  printf("Operation Mode:      ");
  if(tmp ==0)
    printf("Scaler Counter/Latching/Preset ");
  else if (tmp ==2)
    printf("MultiChannel Scaler (MCS)      ");
  else if (tmp ==7)
    printf("SDRAM/FIF) VME write test mode ");
  else
    printf("Reserved                       ");

      printf("\n");

}


void csr_bits(unsigned int data)
{
  unsigned int tmp;

  printf("SIS Control Status register 0x%x : 0x%x\n",SIS3820_CONTROL_STATUS,data);

  printf("User LED:               ");
  if(data & 1)
    printf("On  ");
  else
    printf("Off ");

  // 25MHz test pulse
  tmp = (data  >> 4 ) & 1 ;
  printf("  25MHz Test Pulse: ");
  if(tmp & 1)
    printf("On  ");
  else
    printf("Off ");


  // test mode enable
  tmp = (data  >> 5 ) & 1 ;
  printf("  Counter Test Mode: ");
  if(tmp & 1)
    printf("On  ");
  else
    printf("Off ");

  printf("\n");
  // reference pulse
  tmp = (data  >> 6 ) & 1 ;
  printf("Reference Pulser:       ");
  if(tmp & 1)
    printf("On  ");
  else
    printf("Off ");

  // scaler enable
  tmp = (data  >> 16 ) & 1 ;
  printf("Scaler Enable Active:   ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // MCS enable
  tmp = (data  >> 18 ) & 1 ;
  printf("MCS Enable Active:      ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");
  printf("\n");
  // SDRAM/FIFO test enable
  tmp = (data  >> 23 ) & 1 ;
  printf("SDRAM/FIFO Test Enable:   ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // armed
  tmp = (data  >> 24 ) & 1 ;
  printf("  Armed:              ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // HISCAL
  tmp = (data  >> 25 ) & 1 ;
  printf("  HISCAL operation:    ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  printf("\n");
  // Overflow
  tmp = (data  >> 27 ) & 1 ;
  printf("Overflow:                 ");
  if(tmp & 1)
    printf("Y ");
  else
    printf("N ");

  // Status external input bits
  tmp = (data  >> 28 ) & 3 ;
  printf("  Status Input bits:  %d",tmp);
  // Status external latch bits
  tmp = (data  >> 30 ) & 3 ;
  printf("   Status Latch bits:   %d",tmp);
  printf("\n");
  return;
}


int nchan_enabled(unsigned int bp, int en, int db )
{
  /* Input params
     bp  bitpattern   enabled channel mask or test pulse masked channels
     en  enabled/disabled   TRUE for enabled channel mask or FALSE for test pulse mask 
     db  data_bytes = number of bytes for the data format.
            db       data format
            4/3      32-bit
            2        16-bit
            1         8-bit

   Determine which and how many SIS3820 channels are enabled
   or which channels are masked from the test pulse

   NOTE: Individual channels can only be enabled for 32-bit data format
         For 16-bit data format, groups of 2 channels are actually enabled
     and for 8-bit data format, groups of 4 channels are actually enabled
        according to the disable bit of the first channel of the group.


   NOTE: channels are labelled 1-32  rather than 0-31
         This routine uses 0-31 except for output for the user



 

   Also determines the index in the output buffer (global MCS_ptr) of the one MCS channel (ps.input.sis3820.mcs_channel__1_32_) to be histogrammed
  */
  int i,n,j;
  int chan[32];
  int num,cntr,cntr1,cntr2;
  //  char *order[]={"first","second","third"}; 

  for (i=0; i<32; i++)
    chan[i]=1;

  n=0;
  if (bp & 1)
    {
      chan[0]=0; // ch 0 disabled  or ch 0 not masked
      n++;
    }
  for (j=0; j<31; j++)
    {
      bp = bp >> 1;
      if (bp & 1)
	{
	  n++;
	  chan[j+1]=0;
	}
    }

  if(en)
    {
      num = 32-n;
      printf ("nchan_enabled: num channels enabled=%d\n",num);
      printf ("   Enabled channels are (channels are numbered from 1 to 32 ): ");
      printf ("   Note that for 16-bit data format, groups of 2 channels are actually enabled\n");
      printf ("          and for 8-bit data format, groups of 4 channels are actually enabled\n");
    }
  else
    {
      num = n;
      printf ("nchan_enabled: num enabled channels masked from test pulses =%d\n",num);
      printf ("   Masked channels are (channels are numbered from 1 to 32 ): ");
    }

  cntr=0;
  for (j=0; j<32; j++)
    {
      if (en && chan[j] > 0)  // check enabled channels
	{
	  cntr++;
	  printf("%d ",j+1);
	   
	}
      else if (!en && chan[j] == 0) // check masked channels
	printf("%d ",j+1);
    }
  printf("\n");

  
  printf("For the different data formats:\n");
  printf("32-bit      16-bit        8-bit\n");
  cntr=cntr1=cntr2=0;
  int k=0;
  int ch=0;
  for (j=0; j<32; j++)
    {
      //printf("Working on j=%d with chan[%d]=%d   j%4 is %d and j%2 is %d:\n",j,j,chan[j],(j%4),(j%2));
      if (en && chan[j] > 0)  // check enabled channels
	{
          if( (j%4)==0)
	    {
	      k=j; // found channel k
              ch=j+1;
	      printf("%d           %d,%d           %d,%d,%d,%d\n",ch, ch,ch+1, ch,ch+1,ch+2,ch+3 );
	      cntr++; cntr1+=2; cntr2+=4;
	    }
	  else if ( ((j%2)==0 ) &&  (j > k+1))
	    {
	      ch=j+1;
	      printf("%d            %d,%d  \n",ch, ch,ch+1 );
	      cntr++; cntr1+=2;
	    }
	  else
	    {
	      printf("%d \n",j+1);
	      cntr++;
	    }
	}
    }
  printf("\n");
  printf("Number of channels enabled is %d for 32-bit format, %d for 16-bit format or %d for 8-bit data format\n",cntr,cntr1,cntr2);



  //if(en)
  // {
      //if(MCSptr < 3)
      //	printf("MCS_chan %d is the %s channel in ascending order of enabled channels\n",
      //     ps.input.sis3820.mcs_channel__1_32_ ,order[MCSptr]);
      //else
      //	printf("MCS_chan %d is the %dth channel ascending order of enabled channels\n",
      //      ps.input.sis3820.mcs_channel__1_32_  ,MCSptr+1);
      //printf("Index of this channel is %d\n",MCSptr);
  //  }


  if (db==1) // 8-bit format
    {
      printf("Returning number of enabled channels as %d for 8-bit data format\n",cntr2);
      return cntr2;
    }
  else if (db==2) // 16-bit format
    {
      printf("Returning number of enabled channels as %d for 16-bit data format\n",cntr1);
      return cntr1;
    }
  else      
    {
      printf("Returning number of enabled channels as %d for 32-bit data format\n",cntr);
      return cntr;
    }
}
