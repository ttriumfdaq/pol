#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int DAC_set_scan_params(float start, float inc, int ninc);

const float min_DAC_incr = 0.01; // minimum increment (Volts) - must be > DAC_jitter
// DAC range =/-10V
const float max_DAC = 10.0; 
const float min_DAC = -10.0;

// globals
float dac_start,dac_stop,dac_inc,dac_val;
int dac_ninc;
int gbl_DAC_n;
int gbl_SCAN_n;

int main(void)
{
  float start,inc;
  int i, status, ninc;

  status = 0;
  while(status == 0)
    {
      printf("Enter DAC start (Volts) -10V to +10V : ");
      scanf("%f",&start);
	 
      printf("\nEnter DAC increment (Volts) : ");
      scanf("%f",&inc);
		
      printf("\nEnter number of increments  : ");
      scanf("%d",&ninc);
		    
      status=DAC_set_scan_params(start,inc,ninc); // returns 1 for success
    }
 


  gbl_DAC_n= dac_ninc;

  printf("\nStarting to scan \n");
  
  gbl_SCAN_n=0;
   
  
  for (i=0; i< (dac_ninc + 5) ; i++)
     {
       status = incr_DAC();
       sleep(1);
     }
   return 0;
	 
}

int DAC_set_scan_params(float start, float inc, int ninc)
{
  /* called from begin_of_run */
  int status;
  float    stop;       /* stop value for dac sweep (now calculated) */
  float dac_val;
  
  if(  ninc < 1)  //  ninc
    {
      printf("DAC_set_scan_params: Invalid number of dac sweep steps: %d\n", ninc );
      return 0;
    }



  if( fabs( inc) < fabs(min_DAC_incr))
    {
      printf("DAC_set_scan_params: DAC scan increment value (%.2fV) must be greater than %.2fV\n",
	      inc,min_DAC_incr);
      return 0;
    }


   stop =   start + (  inc *   ninc);

  
  printf("DAC_set_scan_params: DAC start=%f dac_ninc=%d inc=%f; calculated dac_stop=%f\n",
	 start,  ninc, inc,  stop);


  if( ( stop > max_DAC ||  stop < min_DAC))
    {
      printf("DAC_set_scan_params: DAC stop value (%.2fV) is out of range (allowed range is %.2fV to %.2fV)\n",
	      stop,min_DAC,max_DAC);
      return 0;
    }
  if( ( start > max_DAC ||  start < min_DAC))
    {
      printf("DAC_set_scan_params: DAC start value (%.2fV) is out of range (allowed range is %.2fV to %.2fV)\n",
	     start,min_DAC,max_DAC);
      return 0;
    }
    
  if ( fabs( inc) > fabs( stop -  start)) 
    {
      printf("DAC_set_scan_params: Invalid DAC scan increment value: %.2f\n",
	     inc);
      return 0;
    }
 
  // Assigne globals
  dac_val =  start;
  dac_start = start;
  dac_inc  = inc;
  dac_ninc = ninc;
  dac_stop = stop;
  
  printf("DAC_set_scan_params: DAC device will be set to =%.2f Volts\n",dac_val);
  printf("DAC_set_scan_params: DAC stop value is %f Volts\n", dac_stop);
  printf(" DAC_set_scan_params: selected %d DAC scan increments starting with %.2f Volts by steps of %.2f Volts\n",
	  dac_ninc, dac_start, dac_inc);

  return 1;
}


int incr_DAC(void)
{
  int status;

  // increment DAC
  //printf("incr_DAC: gbl_DAC_n = %d  dac_ninc= %d\n",gbl_DAC_n, dac_ninc);


  if( gbl_DAC_n ==   (dac_ninc + 1)) 
    { /* Finished SWEEP;   
	 N E W  S W E E P  FOR DAC */
    
      gbl_DAC_n = 0;
      gbl_SCAN_n++;
    
      printf("\n incr_DAC: ***  N E W  S W E E P  ...  sweep %d is  starting with gbl_DAC_n=%d \n ",gbl_SCAN_n,gbl_DAC_n);
    }

  
  dac_val =  dac_start + (gbl_DAC_n  *  dac_inc);


  printf("incr_DAC: set DAC to value %.2f volts  gbl_DAC_n=%d \n",dac_val,gbl_DAC_n);
   
  gbl_DAC_n++;
  return 1;
}
