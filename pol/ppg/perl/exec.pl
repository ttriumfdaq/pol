#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# invoke this script like this:
#           include path          experiment            command                         beamline      
# exec.pl  /home/bnmr/online/perl      bnmr    "/home/bnmr/online/bnmr/rf_config -s"      bnmr
#    NOTE: command can be e.g. "ls -lt" or "/home/bnmr/online/bnmr/rf_config -s "
#
# This perlscript is used to execute a shell command  e.g. from a user-defined button 
#      
#
# $Log: exec.pl,v $
# Revision 1.1  2011/04/15 18:40:58  suz
# new to vmicpol
#
# Revision 1.1  2005/09/26 17:48:36  suz
# original: execute a shell command such as rf_config
#
#
############### G L O B A L S ####################################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
# parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt, $cmd, $beamline ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "exec"; #same as filename
our $parameter_msg = "include_path, experiment, use path, command,   beamline ";
our $outfile = "exec.txt";
our $nparam = 4;  # no. of input parameters

##################################################################

$|=1; # flush output buffers

my ($transition, $run_state, $path, $key, $status);
my $debug=$FALSE;
my @out_array;

# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt;  command=\"$cmd\"  beamline = $beamline \n";

# output from the command will be sent to FOUT
# because this is for use with the browser and STDOUT and STDERR get set to null
#

# Put the output of the command into FOUT like this :
# 
my $last_line;
unless (open (RFC,"$cmd |"))
{
 print_3 ($name,"cannot open filehandle RFC for command: $cmd ($!)", $MERROR,$CONT);
 print_3 ($name,   "    cannot execute $cmd", $MERROR,$DIE);
}

@cmd_array=<RFC>;       # get the output from the command
close (RFC);
print FOUT @cmd_array;  # into output file
if($debug){ print @cmd_array;}

$len=@cmd_array;
$last_line =  @cmd_array[$len-1];
#print "last line: @cmd_array[$len-1]\n";

# command-specific
# if the command is rf_config or tri_config, check for success
if ( $shell_cmd =~ /tri_config/ ||  $shell_cmd =~ /rf_config/)
{
    $_ = $last_line;
    if (/Thank you/i){ print "Success from tri_config \n";}
    else { print "$_\n";} 
}
# for other commands, customize this file to check the relevent output.
print "done\n";
print FOUT "done\n";
exit;
