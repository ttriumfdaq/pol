###!/usr/bin/perl -w
# $Log: comp_int.pl,v $
# Revision 1.1  2011/04/15 18:40:58  suz
# new to vmicpol
#
# Revision 1.3  2005/03/03 19:13:36  suz
# add more info to some error messages so error can be found more easily
#
# Revision 1.2  2000/12/14 18:42:00  suz
# Two input parameters (previously one) & check 2 are supplied.
#
#
# NOTES:  In function "create_output_file":
#         The output widths for all controller fields are defined (i.e. branch,
#         delay, and opcode).  If any of the field widths should change the
#		  output statements must be modified so that the correct length field
#		  is provided.
use strict ;
use FileHandle;
## TEMP
##open (LOG,">>/tmp/$$.log");
open (LOG,">/tmp/titan.log");
    print LOG "hallo\n";
########################### Label Definitions ##################################
my $DEFINED = 1;
my $UNDEFINED = 0;
my $INTERNAL = 0;
my $EXTERNAL = 1;
my $LOCKED = 1;
my $UNLOCKED = 0;
my $TRUE = 1;
my $FALSE = 0;
my $SHORTEST_CLOCK_PERIOD = 12.5;


########################## Conditional Defines #################################
my $CLOCK = $UNDEFINED;
my $MEMORY = $DEFINED;


#################### Global Variable Initializations ###########################

my $DRIVER_FILE = "driver.exe";
my $DEFAULT_CLOCK_FREQUENCY = 10;		# Specified in MHz
my $CLOCK_PERIOD=1000/$DEFAULT_CLOCK_FREQUENCY; # defined in nanoseconds
my $CLOCK_SEMAPHORE = $UNLOCKED;
my $TRIGGER_SEMAPHORE = $UNLOCKED;
my $MEMORY_TYPE = $EXTERNAL;
my $CLOCK_SELECT = $INTERNAL;
my $USER_SPECIFIED_FLAG_COUNT = 24;		# defined in bits
my $FLAG_SEMAPHORE = $UNLOCKED;
my $DELAY_COUNTER_LENGTH = 32;			# defined in bit length
my $LOOP_COUNTER_LENGTH = 20;                   # defined in bit length
my $MIN_LOOP_FOR_LONG_DELAY = 5;
my $LONG_DELAY_LOOP_ADJUSTMENT = 2;		# Used to adjust the loop count
						# value to implement the number
                                                # of loop actually desired.
                                                # Deals with pipelining and
                                                # zero count issues.

my $SETUP_DELAY = 3;				# number of clock cycles it
						# takes to initialize counter
                                                # at beginning of clock cycle.

my $MIN_COUNT = 2;				# minimum value that delay
						# counter must be loaded with.
                        			# 5 for external memory 2 for
                                                # internal memory

my $MAX_LOOP_COUNT = (2**$LOOP_COUNTER_LENGTH)-1;

my $MAX_DELAY = ((2**$DELAY_COUNTER_LENGTH)-1) + $SETUP_DELAY;
						# defined in clock ticks

my $MAX_2_INST_DELAY = ((2**$DELAY_COUNTER_LENGTH) + $SETUP_DELAY - 1) * $MAX_LOOP_COUNT;
						# defined in clock ticks

my $MIN_DELAY = $SETUP_DELAY + $MIN_COUNT;	# defined in clock ticks

my $OUTPUT_FEILD_WIDTH = 32;			# defined in bits;

my $PORT_ADDRESS = 340;
my $PORT_ADDRESS_SEMAPHORE = $UNLOCKED;

my %INSTRUCTION = (
    'next', 0,
	'stop', 1,
    'loop', 2,
    'end loop', 3,
    'jsr', 4,
    'rts', 5,
    'branch', 6,
    'long_delay', 7,
);

my @NAME_SPACES = qw(f_ d_ rx_ \+);

my %DELAY_VARIABLES;		   		# hash containing delay labels and values
my %DELAYS;					# array used to hold delay values for each

my (@FLAGS, @DELAYS) ;
						# instruction line.
############################ End Definitions ###################################
# example command is
#  /usr/bin/perl comp_int.pl /home/bnmr/online/bnmr2/ppcobj/2c.ppg /home/bnmr/online/bnmr2/ppcobj/bytecode.dat 
#
print "Messages from ppg compiler comp_int.pl: \n";
my $path = shift(@ARGV);
my $output_name = shift(@ARGV);
print "input file = $path\n"; #  ---- debug ----s
print "output file = $output_name\n"; #  ---- debug ----s

print LOG "input file = $path\n"; #  ---- debug ----s
print LOG "output file = $output_name\n"; #  ---- debug ----s

unless($path){ die "input file name not supplied\n"};
unless($output_name){ die "output file name not supplied\n"};

open(FILES,$path) || die "Cannot open file: $!\n";
#open(FILES,$path) || die "Can't open file $path: $!\n";

print LOG "2 input file = $path\n"; #  ---- debug ----s
print LOG "2 output file = $output_name\n"; #  ---- debug ----s

my $line = 0;
my $LN = 0;
my $once = 0;
my $temp ;
my @com_array ;
my $command ;
my $DONT_INCREMENT_LN ;
my @array ;
my @dummy ;
my $i ;
my %FLAG_VARIABLES ;
my @temp ;
my $rxcoeff_FH ;
my $flag_bit ;
my @FLOW_CONTROL_LABELS ;
my @OP_CODE ;
my @BRANCH_FIELD ;
my (%BIT_FIELD_WIDTH, %DATA_FILE_HANDLES ) ;

while(<FILES>){
    ##print LOG "in while loop\n";
	$line++;
	chomp($_);
   	$temp = lc($_); 
unless ($temp) { next ; }
unless ($temp) { print "blank 149\n" ; }
   	@com_array = splice_line($temp);
   	if ($com_array[0] == -1) {              # see if syntax error
   		$temp = drop_comments($temp);   # drop comment section of line
       	if ($temp =~ /\w/) {                   	# \w => a-zA-Z0-9_
	       	print "Warning: Line $line does not end with ;\n"
        }
   	}
        else {
           print LOG "Line $line com_array:@com_array\n";
       }


   	foreach $command (@com_array){          # Line has been parsed and
                                                # is ready to be interpreted
unless ($command) { next ;}
	   unless ($command) { print "blank 163\n" ; }
	    ##print LOG "command:$command\n";
        ################  Delay Keyword Present  ################
   		if ($command =~ /\bd_/){
            if ($command =~ /=/){               # TRUE => Delay Assignment
				$temp =~ /(\bd_\w+)\s*=\s*(\d+[.]*\d*)\s*(\w+)/;
                                print LOG "temp:$temp\n";
                			
print("command=$command\n");					# find label,delay value,units
				if ($2 <= 0) {
                    ERROR("Line $line : Delay \"$command\" not correctly specified.\n");
                }
# decode time units, e.g returns conversion factor of  10**9  if sec,  1 if ns etc 
				$temp = decode_time_units($3);
				if ($temp < 0){
                    ERROR("Line $line : Delay units are not specified.\n");
                }
				#print LOG " units=$3  conversion factor to ns =$temp\n";

				if ($CLOCK_SEMAPHORE == $UNLOCKED){
                    # $CLOCK_PERIOD was initialized using the default clock
                    # frequency defined at the beginning of this program.
                	$CLOCK_SEMAPHORE = $LOCKED;
                    print"Warning : Line $line : No clock frequency was specified before delay declarations.\n";
		   print"                       A clock frequncy of $DEFAULT_CLOCK_FREQUENCY MHz is assumed.\n";
                }
			    
				##      print LOG ("\nCLOCK_PERIOD = $CLOCK_PERIOD ns \n");
				##	print LOG ("DELAY_COUNTER_LENGTH = $DELAY_COUNTER_LENGTH\n"); 	
				##      print LOG ("MAX_DELAY =  $MAX_DELAY clock_ticks\n");
				##      print LOG ("MAX_2_INST_DELAY =  $MAX_2_INST_DELAY clock_ticks\n");
				##	print LOG ( "MIN_DELAY = $MIN_DELAY clock_ticks\n\n");
    
       
                $temp = int(0.5+($2 * ($temp/$CLOCK_PERIOD)));
                	   						# round ticks for best accuracy
    			if ($temp < $MIN_DELAY) {
                    ERROR("Line $line : Delay specified is too short.\n");
				}
				if ($temp > $MAX_2_INST_DELAY) {
                    ERROR("Line $line : Delay specified is too long.\n");
                }
		   
				


               
               
                $DELAY_VARIABLES{$1} = $temp;	# store label name and value

               # while(($t1,$t2) = each(%DELAY_VARIABLES)){
                #   print LOG "DELAY_VARIABLES:  label = $t1 and time = $t2\n"
		#	    }
	}
            ################  PROCESS INSTRUCTION LINE  ################
            else {                              				# Delay label being used in
				$DONT_INCREMENT_LN = $FALSE;    		# an instruction line.
                process_instruction($command);
            }
		}

        ################  FLAG DECLARATIONS  ################
		elsif ($command =~ /\bf_/){
		   
            if ($command =~ /=/){               # TRUE => Flag Assignment
               	@array = split(/=/,$command);   # remove '=' sign
               	$i = 0;
               	foreach $temp (@array){
                    $temp = drop_leading_white_spaces($temp);
                    								# Remove leading white spaces
                    @dummy = split(/\s+/,$temp);# Remove trailing white spaces
                    $array[$i++] = $dummy[0];
                     #print "-",$array[$i-1],"-"; 
               	}

               	if ($array[1] =~ />/){						# Flag info in data file
	    			$FLAG_VARIABLES{$array[0]} = -1;    		# set pointer to
						    				# Flag_Files hash
	    			$temp = index($array[1],">");			# drop '>' sign
	    			$temp = substr($array[1],$temp+1);
	    			$temp = drop_leading_white_spaces($temp);

                    @temp = split_flag_fields($temp);
										# Seperate filename and bits
                                                    				# per word count
	    			$BIT_FIELD_WIDTH{$array[0]} = $temp[1];
	    			$DATA_FILE_HANDLES{$array[0]} = init_flag_file($temp[0]);
										# get filehandle for data
										# file to be used.  Get
										# bits per data word also.
	    			#while($q = $DATA_FILE_HANDLES{$array[0]}->getline){
	    			#	print $q;
	    			#}
				}
				else{
	                @temp = split_flag_fields($array[1]);
										# Seperate filename and bits
                                                # per word count
	        		$FLAG_VARIABLES{$array[0]} = $temp[0];
    				$BIT_FIELD_WIDTH{$array[0]} = $temp[1];
                }
    		}
			else{							# this point should never be
				ERROR("Undefined case reached!\n"); 		# reached since delays in
			} #else                             			# command lines should be
										# decided on first

        } #elsif

        ################  RECIEVER FILTER COEFFICIENTS  ################
        elsif ($command =~ /rx_coeffs/){
			@dummy = split(/=/,$command);				# Grab file name
            $temp = drop_leading_white_spaces($dummy[1]);
        	@dummy = split(/\s+/,$temp);        				# drop trailing white spaces
                                                    				# drop leading white spaces
        	$rxcoeff_FH = new FileHandle $dummy[0],"r"; 			# Open coefficient data file
		    if (!defined $rxcoeff_FH){
    			ERROR("Undefined coefficient file: ",$temp, "\n");
    		}
            while(<$rxcoeff_FH>){
			#while($_ = $rxcoeff_FH->getline){
                chomp;
            	#print "coeff = $_\n";				# TESTING   <---------------
            }
		}

        ################  PROGRAM FLOW INSTRUCTIONS  ################
        elsif ($command =~ /branch/){
            $OP_CODE[$LN] = $INSTRUCTION{"branch"};				# Set Op Code to branch value
			$flag_bit = 1;                         			# Set flag indicating OP set
            #print "Branch = $OP_CODE[$LN]\n";
			@_= split(/\s+/,$command);                 			# Grab second part of command
            $FLOW_CONTROL_LABELS[$LN] = $_[1];     				# Store label for processing
		}                                          			# later.
        elsif ( ($command =~ /loop/) & !($command =~ /end loop/) ){
            $OP_CODE[$LN] = $INSTRUCTION{"loop"};  				# Set Op Code to branch value
			$flag_bit = 1;                         			# Set flag indicating OP set
        #    print "Loop = $OP_CODE[$LN]\n";  # XXXXXXX
			@_=split(/\s+/,$command);                 			# Grab second part of command
			$BRANCH_FIELD[$LN] = $_[2];
            $FLOW_CONTROL_LABELS[$LN] = $_[1]."*"; 				# Denote label as address
	#print "branch field=$BRANCH_FIELD[$LN], flow_cntrl_label= $FLOW_CONTROL_LABELS[$LN]\n";
}                                          			# with the *
        elsif ($command =~ /end loop/){
            $OP_CODE[$LN-1] = $INSTRUCTION{"end loop"};
	
            									# Set Op Code to branch value
			@_=split(/\s+/,$command);                 			# Grab second part of command
            $FLOW_CONTROL_LABELS[$LN-1] = $_[2];    				# Store label for processing
          #  print "EndL = $OP_CODE[$LN-1] flow_cntrl_label=$_[2]\n";  # XXXXXXX
		}
        elsif ($command =~ /jsr/){
            $OP_CODE[$LN] = $INSTRUCTION{"jsr"};				# Set Op Code to branch value
			$flag_bit = 1;                         			# Set flag indicating OP set
            #print "JSR = $OP_CODE[$LN]\n";
			@_=split(/\s+/,$command);                 			# Grab second part of command
            $FLOW_CONTROL_LABELS[$LN] = $_[1];     				# Store label for processing
		}
        elsif ($command =~ /rts/){
            $OP_CODE[$LN-1] = $INSTRUCTION{"rts"};
            									# Set Op Code to branch value
										# No label needed since
                                                   				# return address saved in a
                                                   				# FILO stack
            #print "RTS = $OP_CODE[$LN-1]\n";
		}

        ################  CLOCK FREQUENCY  ################
		elsif ($command =~ /\bclock frequency\s*=\s*(\d+[.]*\d*)\s*(\w*)/){
        									# determine clock frequency
			if ($CLOCK_SEMAPHORE == $LOCKED){
            	ERROR("Line $line : Clock frequency must be specified before delay variables are assinged.\n");
            }

			$temp = frequency_scaling($2);		   		# times are assumed in ns,
            if ($temp < 0){                        				# scale clock period to be
            	$temp = 1000;                      				# in nanoseconds
                print "Warning : Line $line : Clock units not properly specified.\n";
                print "Warning : Line $line : Assumed clock frequency in MHz.\n";
            }
            $CLOCK_PERIOD = $temp / $1;
            if ($CLOCK_PERIOD < $SHORTEST_CLOCK_PERIOD) {
            	ERROR("Line $line : Clock frequency must be below 80 MHz\n");
            }
            $CLOCK_SEMAPHORE = $LOCKED;
			#print "Clock Period = $CLOCK_PERIOD\n";
        }

        ################  MEMORY TYPE  ################
	# Used to specify the SETUP_DELAY value required for each instruction.
        # For external memory this value is longer than internal memory.
        # Can also be used in the future to allow for selection of memory through
        # programming instead of physical jumpers on a board.
		elsif ($command =~ /\bmemory type\s*=/){   			# determine whether internal
        	if($command =~ /embedded/){            				# or external memory is being
            	$MEMORY_TYPE = $INTERNAL;      	   				# used.
			}
            elsif($command =~ /external/){
            	$MEMORY_TYPE = $EXTERNAL;
            }
			else{
            	if($MEMORY == $DEFINED){
        	    	print "Warning : Line $line : Memory Type not specified.\n";
	                print "Warning : Line $line : Default memory type is Embedded\n";
    	        }
			}
			#print "Memory type = $MEMORY_TYPE\n";
        }

        ################  TRIGGER TYPE  ################
		elsif ($command =~ /\btrigger type\s*=/){   # determine trigger type
        	if($command =~ /none/){
            	print "The device will not be triggered.\n";
                print "Please ensure jumper XXX is set correctly.\n";
			}
            elsif($command =~ /single/){
            	print "The device will use a single trigger pulse.\n";
                print "Please ensure jumper XXX is set correctly.\n";
            }
            elsif($command =~ /single/){
            	print "The device will use multiple trigger pulses.\n";
                print "Please ensure jumper XXX is set correctly.\n";
            }
			else{
				ERROR("Line $line : Trigger incorrectly specified.\n");
			}
			$TRIGGER_SEMAPHORE = $LOCKED;
        }

        ################  CLOCK SELECT  ################
		# Used in future hardware when clock selection is done through software
        # programming instead of an actual physical jumper on the board.
		elsif ($command =~ /\bclock select\s*=/){  			# determine whether internal
        	if($command =~ /embedded/){            				# or external clock is being
            	$CLOCK_SELECT = $INTERNAL;         				# used.
			}
            elsif($command =~ /external/){
            	$CLOCK_SELECT = $EXTERNAL;
            }
			else{
            	if($CLOCK == $DEFINED){
            		print "Warning : Line $line : Reference clock not specified.\n";
                	print "Warning : Line $line : Default clock used is Embedded\n";
				}
            }
			#print "Clock select = $CLOCK_SELECT\n";
        }

        ################  NUMBER OF FLAGS  ################
		elsif ($command =~ /\bnumber of flags\s*=\s*(\d+)/){
        	$USER_SPECIFIED_FLAG_COUNT = $1;
			#print "Number of flags = $USER_SPECIFIED_FLAG_COUNT\n";
			$FLAG_SEMAPHORE = $LOCKED;
        }

        ################  ISA PORT ADDRESS  ################
		elsif ($command =~ /\bisa card address\s*=\s*(\w+)/){
			$PORT_ADDRESS = $1;
			$PORT_ADDRESS_SEMAPHORE = $LOCKED;
        }

    } # foreach
} #while

@array = create_branch_addr_array(@FLOW_CONTROL_LABELS);
my $len = @array;
for($i=0;$i<$len;$i++){
	if ($BRANCH_FIELD[$i] > 0){
    	#print "BRANCH_FIELD = $i\n";
	}
	else{
     	$BRANCH_FIELD[$i] = $array[$i];
    }
}

## uncomment and send to log file
print LOG "\n --------- Creating output file --------------\n";
#foreach (@DELAYS) {print LOG "$_\n";}

create_output_file($output_name);
my ($t1,$t2) ;

while(($t1,$t2) = each(%DELAY_VARIABLES)){ 			# TESTING   <---------------
	print LOG "Delay Label = $t1 and Time = $t2\n";		# TESTING   <---------------
}                                                  		# TESTING   <---------------
while(($t1,$t2) = each(%FLAG_VARIABLES)){          		# TESTING   <---------------
	print LOG "Flag Label = $t1 and Flags = $t2\n";     	# TESTING   <---------------
}                                                  		# TESTING   <---------------
foreach $temp (@FLAGS){                            		# TESTING   <---------------
	print LOG "Flags = $temp\n";                        	# TESTING   <---------------
}                                                  		# TESTING   <---------------

$temp = @DELAYS;              					# TESTING   <---------------
for ($i=0;$i<$temp;$i++){
  printf LOG ("Delays[%d]=%d or %X\n", $i,$DELAYS[$i],$DELAYS[$i]);    
                                                   		# TESTING   <---------------
#    print LOG "Delays = $DELAYS[$i]\n";                		# TESTING   <---------------
}                                                  		# TESTING   <---------------

$temp = @FLOW_CONTROL_LABELS; 					# TESTING   <---------------
for ($i=0;$i<$temp;$i++){                           		# TESTING   <---------------
    print LOG "Label, Line = $FLOW_CONTROL_LABELS[$i], $i\n";
    								# TESTING   <---------------
}                                                   		# TESTING   <---------------

$len = @BRANCH_FIELD;
print LOG "array length = $len\n"; 				# TESTING   <---------------
for ($i=0;$i<$len;$i++){                            		# TESTING   <---------------
    $temp = sprintf "%X", $array[$i];
    print LOG "$i = $BRANCH_FIELD[$i]\n";				# TESTING   <---------------
}                                                   		# TESTING   <---------------


$temp = `driver.exe $output_name`;
print LOG "driver.exe $output_name\n";

$temp = `driver`;
print LOG "$temp\n";

my @args ;
### @args = ("driver.exe");                                         # commented out by C.Bommas, Triumf
### system(@args) == 0 or die "system @args failed: $?";            # commented out by C.Bommas, Triumf

print @args;
# exec(@args)  or die "system @args failed: $!\n";                # commented out by C.Bommas, Triumf

################################################################################
############					SUBROUTINES        #############
################################################################################
sub frequency_scaling{
	my $a = $_[0];
	if ($a =~ /mhz/){
    	return 1000
	}
    elsif ($a =~ /khz/) {
    	return 10**6;
	}
    elsif ($a =~ /hz/) {
    	return 10**9;
	}
    else {
    	return(-1)            	# could not find a unit identifier;
    }
}

sub decode_time_units{
	my $a = $_[0];
	if ($a =~ /ns/){
    	return 1
    }
    elsif ($a =~ /us/) {
    	return 1000
	}
    elsif ($a =~ /ms/) {
    	return 10**6;
	}
    elsif ($a =~ /sec/) {
    	return 10**9;
	}
    elsif ($a =~ /min/) {
    	return( (10**9)*60 )
	}
    elsif ($a =~ /hrs/) {
    	return( (10**9)*3600 )
    }
    else {
    	return(-1)            	# could not find a unit identifier;
    }
}

sub create_output_file{
# NOTES:  The output widths for all fields are defined.  If the field width
#         should change the output statements must be modified so that the
#         correct length field is provided.
	my $fh;
    my ($len, $temp);
    my $hex_delay;
    my $hex_branch;
    my $hex_opcode;

    $fh = new FileHandle $_[0],"w";			# Open output file
    if (!(defined($fh))){
    ERROR("Output file undefined\n");
    }
    select($fh);					# Makes default output of print
    							# the file specified by $fh

    $len = @DELAYS;
    $temp = @FLAGS;
    if($len != $temp){
    	ERROR("Fatal processing error, instruction arrays differ in length.\n");
    }

	print "Op_Code Size 4\n";
	print "Branch Size 20\n";
	print "Delay Size 32\n";
	print "Flag Size 24\n";
	print "Instruction Lines $temp\n";
	print "Port Address $PORT_ADDRESS\n\n";

    select(STDOUT);					# Makes default output of print
    							# the screen
    if ($PORT_ADDRESS_SEMAPHORE == $UNLOCKED){
    	print("Warning : ISA Port Address was never specified.\n");
        print("          Assuming default address of 0x340.\n");
    }
    select($fh);					# Makes default output of print
    							# the file specified by $fh

	my $OP_CODE_LEN = 4;
	for($temp=0;$temp<$len;$temp++){
		#print "$DELAYS[$temp]\n";
        $hex_delay = sprintf "%32X", $DELAYS[$temp];
        $hex_branch = sprintf "%20X", $BRANCH_FIELD[$temp];
        $hex_opcode = sprintf "%4X", $OP_CODE[$temp];
    	print "$hex_opcode\t$hex_branch\t$hex_delay\t$FLAGS[$temp]\n";
    }

    select(STDOUT);					# Makes default output of print
    							# the screen
	#$temp = sprintf("$DRIVER_FILE $_[0]");
    #system($temp);
	$fh->close;
}
sub create_branch_addr_array{
	my (@array, @b);
    my ($temp, $i, $len);
    my %little_hash;

	$len = @_;
   # print "sub len = $len\n";  # XXXXX
    for($i=0;$i<$len;$i++){
		if($_[$i] =~ /\*/){
    		@b = split(/\*/,$_[$i]);
            $little_hash{$b[0]} = $i
        }
	}
    for($i=0;$i<$len;$i++){
		if( defined($little_hash{$_[$i]}) ){    # Label used as pointer to an
        #	print "defined = $_[$i]\n";    # XXXXX      	# address.  Decode the pointer
            $array[$i] = $little_hash{$_[$i]};  	# address
        }
		elsif (!(defined($_[$i])) ){        	# Never initialized so there is
        #	print "Line $i has no label\n";   # XXXXX   	# no label present on
        	$array[$i] = 0;                     	# corresponding line
        }
        elsif( $_[$i] =~ /\*/ )  {              	# Return Lable
        	$array[$i] = 0;
        }
        elsif( $_[$i] =~ /-1/ )  {			# Part of a long delay
        	$array[$i] = 0;                     	# instruction
        }
        else{
			foreach $temp (@_){
         #   	print "label is = $temp\n"; # XXXXX
            }
        	ERROR ("Label $_[$i] at line $i has been used but not defined\n");
        }
    }
	return (@array)
}



sub find_label_if_present{
	my (@array, @list);
    my ($temp, $len, $i);
    my $flag;
	#print "find_label_if_present working on \n:"; # TESTING s
	#print "$_[0]\n";                              # TESTING s
    @array = split(/\s+/,$_[0]);
    $len = @array;
    for($i=0;$i<$len;$i++){
		$flag = 0;
    	foreach $temp (@NAME_SPACES){
        	#print "temp = $temp\n";  		# TESTING   <---------------
	        if( $array[$i] =~ /$temp/ ){
            	$flag = 1;
            }
        }
		@list = keys(%INSTRUCTION);
    	foreach $temp (@list){
        	if ($array[$i] =~ $temp){
            	ERROR($line," : Misplaced instruction keyword found. Check for ;\n");
            }
        }
        if ($flag == 0){
        	$temp = drop_leading_white_spaces($array[$i]);
			return $temp;
		}
	}
   	return (-1);
}

sub process_instruction{
	my @array;
    my @array2;
    my $i;
    my $command;
    my $temp;
	my $loop_count;
    my $local_max_delay;
    my $LONG_DELAY ;

    $command = $_[0];
    print LOG "process_instruction starting with command: $command\n";
    $temp = find_label_if_present($command);
    if($temp != -1){
 		$temp = $temp . "*";        		# Denote label as the address
	 						# to be used
        $FLOW_CONTROL_LABELS[$LN] = $temp;
        print LOG "temp = $temp\n";     			# TESTING       <---------------
    }

	@array = split(/\s+/,$command); 		# Split instruction line fields
    $i=0;                           			# and extract needed info
    foreach $temp (@array){
    	$array2[$i++] = drop_leading_white_spaces($temp);
    }


## print this once here in case of failure
	if($once == 0)
        {
	    print LOG "DELAY_VARIABLES array:\n";
           while(($t1,$t2) = each(%DELAY_VARIABLES)){
	       print LOG "label = $t1 and time = $t2\n";
	   }
	   $once=1;
       }

	print LOG "\n *** calling get_delay_value with @array2\n";
    $DELAYS[$LN] = get_delay_value(@array2);		# Uses global hash variable
                                            		# Delay_Variables
      
    if ($DELAYS[$LN] <= 0) { 
    	ERROR($line," : Delay label \"$array[0]\" never initialized.\n");
    }

	print LOG "after get_delay value DELAYS[$LN]:$DELAYS[$LN]\n";
    $temp = 0;
	$local_max_delay = $MAX_DELAY;
	if ($DELAYS[$LN] > $MAX_DELAY){
	# In a long delay situation two program instructions are used to implement
        # a single delay instruction.  The first command line will implement a
        # zero overhead loop instruction.  This instruction will carry out most
        # of the delay. The zero overhead loop instruction will delay for a
        # delay that is equal to  LOOP COUNT * (DELAY COUNT + SETUP DELAY).  To
        # avoid drawn out calculations, the TOTAL DELAY will be divided by the
        # Zero Overhead Loop Delay.  This will in general leave a remainder
        # which the second line is used to finish off.  Most non-prime large
        # numbers (delays) could be done in a single line but it would requrie
        # that the desired delay be factored to find values that can be used
        # in the Loop Count and Delay Count registers.

    	while ($temp == 0){
        	$loop_count = int($DELAYS[$LN]/$local_max_delay);
            $i = $DELAYS[$LN]-($local_max_delay*$loop_count);
			if ($i > $MIN_DELAY){ 			# check to see if next instruction
								# after long loop will have at least
 								# the minimum # of ticks

				if ($loop_count < $MIN_LOOP_FOR_LONG_DELAY){
                						# check for minimum loop count
                                            			# if not scale max delay to force
                                            			# more loops
					$local_max_delay = $local_max_delay - $SETUP_DELAY;
					$local_max_delay = $local_max_delay / 4;
					$local_max_delay = $local_max_delay + $SETUP_DELAY;
				}
                else {
					$loop_count = $loop_count - $LONG_DELAY_LOOP_ADJUSTMENT;
	 				#print "finding delay value. Max_Delay = $local_max_delay and i = $i\n";
    	           	$temp = 1;
                }
            }
            else {
            	$local_max_delay = $local_max_delay - 1;
				#print "finding delay value. Max_Delay = $local_max_delay and i = $i\n";
			}
        }

	# Assign zero overhead loop instruction to 1st instruction.  Regular
        # delay is assigned to 2nd instruction.  This is done so that if a JSR,
        # BRANCH, or END LOOP, instruction is encountered the correct Op Code
        # corresponding to an instruction can be used in the 2nd instruction to
        # control program flow.

        if ($flag_bit == 0){
           	$OP_CODE[$LN] = $INSTRUCTION{"long_delay"};
           	$OP_CODE[$LN+1] = $INSTRUCTION{"next"};
	        $BRANCH_FIELD[$LN] = $loop_count;
	        $BRANCH_FIELD[$LN+1] = 0;
			$DELAYS[$LN] = $local_max_delay - $SETUP_DELAY;
    	    $DELAYS[$LN+1] = $i - $SETUP_DELAY;
			$FLAGS[$LN] = process_flags(@array2);	# Reconstruct hex
            							# representation of flags
            $FLAGS[$LN+1] = $FLAGS[$LN];
		} else {
           	$flag_bit = 0;					# OP Code already set; clear flag

        	if ($OP_CODE[$LN] == $INSTRUCTION{"branch"}){
                $OP_CODE[$LN] = $INSTRUCTION{"long_delay"};
                $OP_CODE[$LN+1] = $INSTRUCTION{"branch"};
		        $BRANCH_FIELD[$LN] = $loop_count;
                $FLOW_CONTROL_LABELS[$LN+1] = $FLOW_CONTROL_LABELS[$LN];
                $FLOW_CONTROL_LABELS[$LN] = -1;
				$DELAYS[$LN] = $local_max_delay - $SETUP_DELAY;
    		    $DELAYS[$LN+1] = $i - $SETUP_DELAY;
			}
        	elsif ($OP_CODE[$LN] == $INSTRUCTION{"jsr"}){
            	$OP_CODE[$LN] = $INSTRUCTION{"long_delay"};
                $OP_CODE[$LN+1] = $INSTRUCTION{"jsr"};
		        $BRANCH_FIELD[$LN] = $loop_count;
                $FLOW_CONTROL_LABELS[$LN+1] = $FLOW_CONTROL_LABELS[$LN];
                $FLOW_CONTROL_LABELS[$LN] = -1;
				$DELAYS[$LN] = $local_max_delay - $SETUP_DELAY;
    		    $DELAYS[$LN+1] = $i - $SETUP_DELAY;
			}
        	elsif ($OP_CODE[$LN] == $INSTRUCTION{"rts"}){
            	$OP_CODE[$LN] = $INSTRUCTION{"long_delay"};
                $OP_CODE[$LN+1] = $INSTRUCTION{"rts"};
		        $BRANCH_FIELD[$LN] = $loop_count;
		        $BRANCH_FIELD[$LN] = 0;
				$DELAYS[$LN] = $local_max_delay - $SETUP_DELAY;
    		    $DELAYS[$LN+1] = $i - $SETUP_DELAY;
			}
        	elsif ($OP_CODE[$LN] == $INSTRUCTION{"end loop"}){
            	$OP_CODE[$LN] = $INSTRUCTION{"long_delay"};
                $OP_CODE[$LN+1] = $INSTRUCTION{"end loop"};
		        $BRANCH_FIELD[$LN] = $loop_count;
                $FLOW_CONTROL_LABELS[$LN+1] = $FLOW_CONTROL_LABELS[$LN];
                $FLOW_CONTROL_LABELS[$LN] = -1;
				$DELAYS[$LN] = $local_max_delay - $SETUP_DELAY;
    		    $DELAYS[$LN+1] = $i - $SETUP_DELAY;
			}
        	elsif ($OP_CODE[$LN] == $INSTRUCTION{"loop"}){
				# $FLOW_CONTROL_LABELS[$LN] is already correctly specified
                # Must switch OP Code order so that regular delay instruction
                # is at the top of the loop and the Loop OP Code ca be inserted.
            	$OP_CODE[$LN+1] = $INSTRUCTION{"long_delay"};
                $OP_CODE[$LN] = $INSTRUCTION{"end loop"};
		        $BRANCH_FIELD[$LN+1] = $loop_count;
				$DELAYS[$LN+1] = $local_max_delay - $SETUP_DELAY;
    		    $DELAYS[$LN] = $i - $SETUP_DELAY;
			}
		}

		$FLAGS[$LN] = process_flags(@array2);	# Reconstruct hex
       	$FLAGS[$LN+1] = $FLAGS[$LN];			# representation of flagS
		$LN = $LN + 2;
		$LONG_DELAY = $LOCKED;

    } else {                                    	# This is the case for a regular
    	if ($flag_bit == 0){                    	# Next instruction.
        	$OP_CODE[$LN] = $INSTRUCTION{"next"};
            		  				# OP Code not set by flow
                    					# control, default to next OP C.
            #print "OP_Code = $OP_CODE[$LN]\n";
        }
        else {
        	$flag_bit = 0;				# OP Code already set; clear
        }                               		# flag

		$DELAYS[$LN] = $DELAYS[$LN] - $SETUP_DELAY;
		$FLAGS[$LN] = process_flags(@array2);	# Reconstruct hex
        $LN++;
	}
}

sub process_flags{
	my @array;
    my ($temp, $out_indx);
    my @flags;
    my (@bit_count, $overflow);
	my ($bit_len, $exponent);
    my ($indx, $remainder, $sum);
    my ($i, $j, $k, $lsb, $point);
	my (@fields, @bits);
    my @output_array;
    my @unglued;

	$out_indx = 0;
    $indx = 0;

	if ($FLAG_SEMAPHORE == $UNLOCKED){
    	print "Warning : Number of flags in output has not been specified.\n";
    	print "          It will be assumed design uses $USER_SPECIFIED_FLAG_COUNT. flag outputs.\n";
		$FLAG_SEMAPHORE = $LOCKED;				# Avoids repeated warnings.
	}

    foreach $temp (@_) {
unless ($temp) { next ; }
 unless ($temp) { print "blank 853\n" ; }
    	if ($temp =~ /\bf_/){
			@unglued = split(/\+/,$temp);	# split flag labels
            foreach my $temp (@unglued){           	# foreach label find it hex
            						# value and bit count
        		$flags[$indx] = $FLAG_VARIABLES{$temp};
            	if (!defined($flags[$indx])){
            		ERROR("Flag Label ",$temp," is undefined or not completely defined1.");
            	}

    			$bit_count[$indx] = $BIT_FIELD_WIDTH{$temp};
            	if (!defined($bit_count[$indx])){
            		ERROR("Flag Label ",$temp," is undefined or not completely defined2.");
            	}

				if ($flags[$indx] == -1) { 		# if flag info in file go get it
					$flags[$indx] = $DATA_FILE_HANDLES{$temp}->getline;
					#print "Flags from file = $flags[$indx]";
				}
				$indx++;
			}
        }
        else{
        	next;
        }
    }

   	$bit_len = 0;
    $indx--;						# last defined element in arrays
    $point = 0;
    $remainder = 0;
    $k = 0;
    for($i=$indx;$i>=0;$i--){               		# last element in array should be
    							# least significant bytes of flags.
    	$fields[$k] = hex($flags[$i]);
        $bits[$k] = $bit_count[$i];
        if( (2 ** $bits[$k]) < $fields[$k]){
        	ERROR("Flag field ",$array[$i]," is larger than specified.\n");
        }
        $k++;
        $bit_len = $bit_len + $bit_count[$i];
       	$array[$point++] = $i;

		#$_ = $k-1;								# TESTING   <---------------
		#print "k = $_ : field = $fields[$_] : bit length is = $bit_len\n";
        										# TESTING   ^---------------

		if ( ($bit_len >= $OUTPUT_FEILD_WIDTH) || ($i == 0) ){
        				# segment flags in $OUTPUT_FEILD_WIDTH
                                        # bit patterns or if < $OUTPUT_FEILD_WIDTH
                                        # but no more fields left to process.
			$exponent = 0;
			$sum = 0;
            $k--;			# readjust K pointer to last element
			if ($k > 0){
					# Take care of bit fields except overflow case #
            	for($j=0;$j<$k;$j++){	# Take care of all fields except
					# the one that made count >=
                                        # of next bits
                    if( ($j-1) >= 0){
                    	$exponent = $exponent + $bits[$j-1];
                    }
					$sum = $sum + ($fields[$j] * (2 ** $exponent));
    	        	$output_array[$out_indx] = sprintf "%x", $sum;
                }
      		} # if $k+1

            # Last field must be handled taking into account bit overflows #
            if( ($overflow=$bit_len-$OUTPUT_FEILD_WIDTH) > 0){
				#print "\nOVERFLOWED = $overflow\n"; 	# TESTING  <---------------
                							# overflow will be used in next $OUTPUT_FEILD_WIDTH
                                        				# bit field to specify lsb width

                $lsb = $bits[$k] - $overflow;
                $temp = 2 ** $lsb;
				$remainder = int ($fields[$k] / $temp);
									# stores away the value of the lsbs
                                        				# of next $OUTPUT_FEILD_WIDTH wide field
                $temp = $fields[$k] % $temp;
				$exponent = $exponent + $bits[$k-1];
                   							# bit count is of field previous to
                                        				# last field; shifts last field
	            $sum = $sum + ($temp * (2 ** $exponent));
                                        				# add in value for last field of
                                        				# flags to $OUTPUT_FEILD_WIDTH bit word.

				$k = 1;					# overflow from this $OUTPUT_FEILD_WIDTH
				$fields[0] = $remainder;		# bit word has created the first elements
                $bits[0] = $overflow;   				# of next $OUTPUT_FEILD_WIDTH bit word.
				$bit_len = $overflow;
				#print "new bit_len = $bit_len\n"; 	# TESTING   <---------------

    	        $output_array[$out_indx] = sprintf "%x", $sum;
            }

									# Last field ends exactly on $OUTPUT_FEILD_WIDTH bit word boundary #
            else{
               	if ($k > 0){
                	$exponent = $exponent + $bits[$k-1];
                }  							# bit count is of field previous to
                else{                   				# last field, shifts last field
					$exponent = 0;   		# else case takes care of cases were
                }                       				# only 1 field found when $i => 0;

	            $sum = $sum + ($fields[$k] * (2 ** $exponent));
                                        				# add in value for last field of
                                        				# flags to $OUTPUT_FEILD_WIDTH bit word.

				$k = 0;					# fill bit and fields arrays from
                							# scratch; no remainder
    	        $output_array[$out_indx] = sprintf "%x", $sum;
            }

            $output_array[$out_indx] = sprintf "%X", $sum;
			$out_indx++;					# create $OUTPUT_FEILD_WIDTH bit flag
            								# word and store to array with other
                                        				# flag words

		} #if bit_len
    } # for i
	return ( join(' ',reverse(@output_array)) );
}


sub ERROR { 								# Prints out an error message and kills program.
	my $temp;
    print "\nERROR: ";
    foreach $temp (@_){
	    print $temp;
    }
	die;
}

sub split_flag_fields{
    my ($temp);
    my (@array);

    @array = split(/,/,$_[0]); 		        			# Split filename and
									# bit count field
	if (defined($array[1])){
    	$temp = drop_leading_white_spaces($array[1]);
   	}								# drop WS on bit count
    else{
    	ERROR("Flag field width for ",$array[0]," not specified.\n");
    }
    return @array;							# array with flag data and bit
}                                               			# length.


sub init_flag_file {
    my $fh = new FileHandle $_[0],"r";					# Open data file
    if (!(defined($fh))){
    	ERROR("Undefined file: ",$_[0], "\n");
    }
    return $fh;								# return file handle
}


sub get_delay_value {
    my ($t1,$t2);
    my ($len);
    my ($i);
    my $name;

    $len = @_;

## TEMP
## Originally cycled through all fields of command line array to find delay value.
## But this was crazy as the delay is always at the beginning of the line
## Take out the for loop so we just loop at first item

  print LOG " get_delay_value: looking for delay in supplied delay list....\n"; 

    while( ($t1,$t2) = each(%DELAY_VARIABLES) ){			# Try each element in hash
    ##print LOG "label = $t1 and time = $t2\n";    ## temp
       ### for ($i=0;$i<$len;$i++){                			# Cycle through all field of
    $i=0; 
       ###	    if($i==0) { 
     print LOG "comparing my delay  \"$_[$i]\" with \"$t1 \" \n";
     $name="$_[$i] "; ## fixed bug where it found d_delay_1 in e.g. d_delay_10; added a space at the end
          ###  }
            if ($name =~ /$t1 /) {              			# command line array to find
                                               				# delay value.

                $i = keys %DELAY_VARIABLES;     # DO NOT REMOVE! Resets the
                print LOG "found delay $name as $t1; returning time $t2\n ";
                return $t2;                     # hash pointer to top of hash.
            }                                   # If removed subsequent calls to 
        ##}                                       # hash will not start from 
    }                                           # the beginning
    print "ERROR -could not find delay  $name in supplied delay list\n";
    print LOG "Error - could not find delay  $name in supplied delay list\n";

    return(-1);                                 # Delay Variable not found in
}                                               # hash
sub splice_line {
    my ($input);
    my (@array);
    my ($indx);
    my $GLOBAL_COMMAND_DELIMETER ;
    $input = $_[0];                         	# assign passed variable to name
unless ($input) { next ; }
 unless ($input) { print "blank 1055\n" ; }
    chomp($input);                          	# drop carrige return if it present
    $input = drop_comments($input);         	# drop comment section of line
    if ($input =~ /;/){
        @array = command_splice($input);    	# determine how many commands are on
        $GLOBAL_COMMAND_DELIMETER="TRUE";   	# current line
    }
    else {
        $GLOBAL_COMMAND_DELIMETER = "FALSE";
        return(-1);
    }

    $indx = 0;
    foreach $input (@array){
        $array[$indx] = drop_leading_white_spaces($input);
        #print "array[$indx] = $array[$indx]-\n";
        $indx++;
    }
    return @array
}

sub drop_leading_white_spaces{
    my ($loc);                          	# @_ is predefined variable for
    my (@array);                        	# passing function parameters
    my ($temp);
    my ($point);

    @array = ('a'..'z', 'A'..'Z', '0'..'9', '_', '>');	# create array of valid characters
    $loc = 10000;                       	# initialize pointer far out
    foreach $temp (@array){             	# cycle through all valid characters
        $point = index($_[0],$temp);    	# and find their locatoins
        if (($point < $loc) && ($point >= 0))
        {
            $loc = $point;              	# save the location of the first valid
        }                               	# character
    }
    if ($loc > 0)
    {                                   	# Leading characters NOT valid
        return (substr($_[0],$loc));    	#   return string starting with first
    }                                   	#   valid character
    else
    {                              		# Leading character valid
        return $_[0];                   	#   do nothing to string
    }
}

sub drop_comments {
    my (@array);                    		# @_ is predefined variable for passing
                                    		# function parameters
    @array = split(/\/\//,$_[0]);   		# split on comment marker
    return $array[0];               		# keep only section of string before
}                                   		# comment marker.

sub command_splice {
    my (@array);                    		# @_ is predefined variable for passing
                                    		# function parameters
    @array = split(/;/,$_[0]);      		# find semicolons associated with end of
    return(@array);                 		# command line / instruction
}                                   	


