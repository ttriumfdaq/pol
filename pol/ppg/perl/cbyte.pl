#!/usr/bin/perl -w 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d  cbyte.pl
#
# Translates bytecode.dat (for old PPG) to instructions for new PPG
# If an input params are given,
#       param 1 multiply clock - multiplies the clock of the newppg (old has 10MHz clock)
#       param 2 shift_outputs  - shift left all the outputs of newppg (for testing)
#

my ($multiply, $shift_outputs, $write_scanloop_pc) = @ARGV;
my $num = $#ARGV;
my $clock_freq_MHz = 100; 
my $exp = $ENV{MIDAS_EXPT_NAME}; 
if ($exp eq ""){ die "MIDAS_EXPT_NAME environment variable not defined";}
require "/home/$exp/online/ppg/perl/perlmidas.pl";

my $filename="/home/$exp/online/ppg/ppgload/bytecode.dat"; # old PPG instruction file
my $outfile="/home/$exp/online/ppg/ppgload/ppgpoke.dat";
my $peekfile="/home/$exp/online/ppg/ppgload/ppgpeek.dat";
my $ppgload="/home/$exp/online/ppg/ppgload/ppgload.dat";
my $strpoke1="vme_poke -a VME_A32UD -A";
my $strpoke2="-d VME_D32";
my $strpeek="vme_peek -a VME_A32UD -A";
my $minimal_delay=3; # 3 clock cycles (new ppg)
my $halt = 0; # halt instruction (new ppg)
my $vme_addr = 0x100000;
my $status_reg = $vme_addr;
my $pc_reg = $vme_addr | 0x8;
my $lo_reg = $vme_addr | 0xC;
my $med_reg = $vme_addr | 0x10;
my $hi_reg = $vme_addr | 0x14;
my $top_reg = $vme_addr | 0x18;
my ($cmd,$comment);
my ($ins,$lc,$delay,$bitpat, $clrpat);
my $tmp;
my $numlines;
my @fields;
# instruction list for new and old ppg
my @new_instructions= ("Halt","Continue","Begin Loop","End Loop","Call Subr","Return","Branch");
my @old_instructions=("Continue","Stop","Begin Loop","End Loop","JSR","RSR","Branch","Long Delay");
my @convert_instructions=qw(1 0 2 3 4 5 6); # no "Long Delay"
my $old_ins;
my $len=$#new_instructions;
my $lenc=$#convert_instructions;
my $pc =0; # program counter
my $time_ms;
my $debug=1;
my $freq_MHz = $clock_freq_MHz;
my $numloops = 0;
my $odb_eqpname = "pol_acq";
my $odb_scan_pc = "/equipment/$odb_eqpname/settings/output/scanloop pc";

$num++;
print "Number of parameters supplied: $num\n";
unless ($multiply) { $multiply = 0 };
unless ($shift_outputs) {$shift_outputs = 0 };
unless ($write_scanloop_pc ) {$write_scanloop_pc = 0 };
print "Parameters: multiply=$multiply; shift_outputs=$shift_outputs; write_scanloop_pc=$write_scanloop_pc\n";

if ($write_scanloop_pc)
{ 
    MIDAS_env(); 
    MIDAS_varset($odb_scan_pc, 0); # clear value to start
};

if($shift_outputs > 0)
{ 
    my ($i,$j);
    $i=$shift_outputs+1;
    $j=$shift_outputs+5;
    print "PPG Outputs will all be shifted left by $shift_outputs compared with the input file\n"; 
    print "e.g. PPG outputs 1-5 will be shifted to outputs $i - $j \n";
}

if ($multiply > 0)
{ 
    $freq_MHz = $clock_freq_MHz / $multiply;
    print "PPG delay counts will be multiplied by $multiply compared with the input file\n";
    print "PPG will appear to be clocked at $freq_MHz MHz\n";
}
if($debug){ print "@new_instructions; len=$len\n";}

open (OUT,">$outfile") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    die "FAILURE cannot open file \"$outfile\";$tmp\n";
}
open (PEEK,">$peekfile") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$peekfile\"; $tmp" ) ;
    die "FAILURE cannot open file \"$peekfile\";$tmp\n";
}


open (IN,"$filename") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    die "FAILURE cannot open input file \"$filename\";$tmp\n";
}
open (LDF,">$ppgload") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    die "FAILURE cannot open input file \"$ppgload\";$tmp\n";
}
printf LDF "#Ins 0=halt 1=cont 2=loop 3=endloop   \n"; 
printf LDF "#PC set bitpat clr bitpat       delay  ins/data\n";

my $linenum=0;
while (<IN>)
{
    $linenum++;
    if (/Instruction Lines/)
    {
	s/Instruction Lines//;
	$numlines = $_ + 0;
        print "number of program lines in bytecode.dat : $numlines\n";
        $numlines++; # add one for the halt instruction
        printf LDF ("Num Instruction Lines = %4.0d         #  IDDDDD\n",$numlines);
    }
    if ($linenum >= 8)
    {
	
	if($debug){print "Working on line $linenum: $_";}
	@fields=split;
        $old_ins = hex($fields[0]);  # old PPG instruction
        $lc  = hex($fields[1]);
        $delay  = hex($fields[2]);
        $bitpat  = hex($fields[3]);
	
	if($shift_outputs > 0) 
	{
	    printf "Bitpat 0x%x ", $bitpat;
	    $bitpat = $bitpat << $shift_outputs;
	    printf "shifted by %d ->  0x%x \n",$shift_outputs,$bitpat;
	}
	
	if ($multiply > 0)
	{
	    printf "Delay 0x%x (%d) ", $delay,$delay;
	    $delay*=$multiply;
	    printf "multiplied by %d ->  0x%x (%d) \n",$multiply,$delay,$delay;
	}
	
        if ($old_ins > $lenc) { die "Instruction (\"$ins\")at line $linenum in $filename is not supported by new PPG \n"; } 
        $ins = $convert_instructions[$old_ins]; # convert old instruction to new.
	if($debug)
	{
	    if($ins != $old_ins)
	    { print "Converted old instruction $old_ins to $ins\n"; }
	}
	
	
	if ($ins == 2 ) # begin loop
	{
	    $numloops++;
	    # Check loop count (maximum 20 bits)
	    $lc++; # convert from old ppg loop count (where 0 meant 1 loop)
	    if ($lc >= 0x100000)
	    {
		$lc = 0xFFFFF;
		print "WARNING : data field for loop count is 20 bits maximum. Loop count will be set to $lc\n";
	    }
	}
	elsif ($ins == 4 || $ins == 6)
	{ # check address (max 20 bits)
	    if ($lc >= 0x100000)
	    { die "Illegal address $lc at line $linenum. Address must be 20bits or less";}
	}
	
	if($debug)
	{ 
	    print "fields: @fields\n"; # fields[0..3] code; loop count; clock count; bitpat 	
	    printf ("ins=%d lc=0x%x delay=0x%x bitpat=0x%x \n",$ins,$lc,$delay,$bitpat);
	    print "\nWriting pc = $pc\n";
	}
        
	
	printf OUT "\n#Writing pc=$pc\n";
	printf OUT "%s 0x%x %s 0x%x \#  program address 0x%x \n",
	$strpoke1, $pc_reg, $strpoke2,$pc,$pc;
	printf OUT "%s 0x%x %s  \#   program address 0x%x \n",
	$strpeek, $pc_reg, $strpoke2,$pc;

	printf PEEK "\n#Writing pc=$pc\n";
	printf PEEK "%s 0x%x %s 0x%x \#  program address 0x%x \n",
	$strpoke1, $pc_reg, $strpoke2,$pc,$pc; # strpoke not peek for this one
	
	
	printf OUT "%s 0x%x %s 0x%x \# SET bits \n",
	$strpoke1, $lo_reg, $strpoke2,  $bitpat;
	printf PEEK "%s 0x%x %s \# SET bits (expect 0x%x) \n",
	$strpeek, $lo_reg, $strpoke2,  $bitpat;
	printf OUT "%s 0x%x %s \# SET bits (expect 0x%x) \n",
	$strpeek, $lo_reg, $strpoke2,  $bitpat;
	
	
	printf OUT "%s 0x%x %s 0x%x \# CLR bits \n",
	$strpoke1, $med_reg, $strpoke2,  (~$bitpat & 0xFFFFFFFF) ;
	printf PEEK "%s 0x%x %s \# CLR bits (expect 0x%x) \n",
	$strpeek, $med_reg, $strpoke2,  (~$bitpat & 0xFFFFFFFF) ;
	printf OUT "%s 0x%x %s \# CLR bits (expect 0x%x) \n",
	$strpeek, $med_reg, $strpoke2,  (~$bitpat & 0xFFFFFFFF) ;

	$time_ms =  ($delay + 3)/($freq_MHz * 1000); # add the 3 clock cycles that each instruction takes

  
	printf OUT "%s 0x%x %s 0x%x \# delay=%d time=%g ms\n",
	$strpoke1, $hi_reg, $strpoke2,  $delay, $delay, $time_ms ;
	printf PEEK "%s 0x%x %s  \# (expect 0x%x) delay=%d time=%g ms\n",
	$strpeek, $hi_reg, $strpoke2,  $delay, $delay, $time_ms ;
	printf OUT "%s 0x%x %s  \# (expect 0x%x) delay=%d time=%g ms\n",
	$strpeek, $hi_reg, $strpoke2,  $delay, $delay, $time_ms ;
	
        if($debug)
	{
	    printf  "%s 0x%x %s 0x%x \#  program address 0x%x \n",
	    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;
	    printf  "%s 0x%x %s 0x%x \# SET bits \n",
	    $strpoke1, $lo_reg, $strpoke2,  $bitpat;
	    printf  "%s 0x%x %s 0x%x \# CLR bits \n",
	    $strpoke1, $med_reg, $strpoke2,   (~$bitpat & 0xFFFFFFFF) ;
	    printf  "%s 0x%x %s 0x%x \# delay count \n",
	    $strpoke1, $hi_reg, $strpoke2,  $delay ;
	    printf "%s 0x%x %s 0x%x \# delay=%d time= %g ms\n",
	    $strpoke1, $hi_reg, $strpoke2,  $delay, $delay, $time_ms ;
	}

        $tmp = $ins << 20 ;
        $tmp = $tmp | $lc;

        if  ($ins == 2)  # begin loop
	{
	    printf OUT "%s 0x%x %s 0x%x \# instr/data  %s (loop count %d) \n",
	    $strpoke1, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins], $lc ;
	    printf PEEK "%s 0x%x %s \# (Expect 0x%x) instr/data  %s (loop count %d) \n",
	    $strpeek, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins], $lc ;
	    printf OUT "%s 0x%x %s \# (Expect 0x%x) instr/data  %s (loop count %d) \n",
	    $strpeek, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins], $lc ;

	    if($debug) { printf  "%s 0x%x %s 0x%x \# instr/data  %s (loop count %d) \n",
			 $strpoke1, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins], $lc ; }

	    if( $numloops == 1 &&  $write_scanloop_pc)
	    {
		# record the pc of the scan loop instruction
		printf("($numloops, $write_scanloop_pc)PC for begin_scan instruction is $pc\n");
		MIDAS_varset($odb_scan_pc, $pc);
	    }
	}
	else
	{
	    printf OUT "%s 0x%x %s 0x%x \# instr/data  %s \n",
	    $strpoke1, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins] ;
	    printf PEEK "%s 0x%x %s  \# (expect 0x%x) instr/data  %s \n",
	    $strpeek, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins] ;


	     if($debug) { printf  "%s 0x%x %s 0x%x \# instr/data  %s \n",
			  $strpoke1, $top_reg, $strpoke2,  $tmp, $new_instructions[$ins] ; }
	
	}	 
        
	printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$delay,$tmp;
 
	$pc++;  # next instruction
    }
}
# end of input file


# Add HALT at end of program
$tmp=$ins=0;
printf OUT "\n#End of input file.  Adding HALT at pc=$pc \n";
printf PEEK "\n#End of input file.  Adding HALT at pc=$pc \n";

printf OUT "%s 0x%x %s 0x%x \#  program address 0x%x \n",
    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;
printf PEEK "%s 0x%x %s 0x%x \# program address 0x%x \n",
    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;


printf OUT "%s 0x%x %s 0x%x \# SET bits \n",
    $strpoke1, $lo_reg, $strpoke2,  $bitpat;  # keep the last bit pattern
printf PEEK "%s 0x%x %s \# SET bits (Expect 0x%x)  \n",
    $strpeek, $lo_reg, $strpoke2,  $bitpat;  # keep the last bit pattern


printf OUT "%s 0x%x %s 0x%x \# CLR bits \n",
    $strpoke1, $med_reg, $strpoke2,  (~$bitpat & 0xFFFFFFFF)  ;
printf PEEK "%s 0x%x %s  \#  CLR bits (Expect 0x%x)  \n",
    $strpeek, $med_reg, $strpoke2,  (~$bitpat & 0xFFFFFFFF)  ;

printf OUT "%s 0x%x %s 0x%x \# delay count \n",
    $strpoke1, $hi_reg, $strpoke2,  $minimal_delay ;
printf PEEK "%s 0x%x %s  \#  delay count (Expect 0x%x)  \n",
    $strpeek, $hi_reg, $strpoke2,  $minimal_delay ;

printf OUT "%s 0x%x %s 0x%x \# instr/data  %s \n",
    $strpoke1, $top_reg, $strpoke2,  $tmp,  $new_instructions[$ins] ;
printf PEEK "%s 0x%x %s  \# (Expect 0x%x)  instr/data  %s \n",
    $strpeek, $top_reg, $strpoke2,  $tmp,  $new_instructions[$ins] ;

if($debug) 
{
    printf  "\n#End of input file. Adding HALT at pc=$pc \n";
    printf  "%s 0x%x %s 0x%x \#  program address 0x%x \n",
    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;
    
	
    printf  "%s 0x%x %s 0x%x \# SET bits \n",
    $strpoke1, $lo_reg, $strpoke2,  $bitpat;  # keep the last bit pattern
    
    
    printf  "%s 0x%x %s 0x%x \# CLR bits \n",
    $strpoke1, $med_reg, $strpoke2,   (~$bitpat & 0xFFFFFFFF) ;
    
    printf  "%s 0x%x %s 0x%x \# delay count \n",
    $strpoke1, $hi_reg, $strpoke2,  $minimal_delay ;
    
    printf  "%s 0x%x %s 0x%x \# instr/data  %s \n",
    $strpoke1, $top_reg, $strpoke2,  $tmp,  $new_instructions[$ins] ;
}
# add HALT to end of file
printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
# set pc back to zero
$pc=0;
printf OUT "\n# set pc back to start of program (0)\n";
printf PEEK "\n# set pc back to start of program (0)\n";
printf  "\n# set pc back to start of program (0)\n";
printf OUT "%s 0x%x %s 0x%x \#  program address 0x%x \n",
    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;
printf PEEK "%s 0x%x %s 0x%x \#   program address 0x%x \n",
    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;

printf  "%s 0x%x %s 0x%x \#  program address 0x%x \n",
    $strpoke1, $pc_reg, $strpoke2,$pc,$pc;

# start the pgm
#printf OUT "\n# start the program\n";
#printf  "\n# start the program\n";
#printf OUT "%s 0x%x %s 0x%x \#  status reg 0x%x \n",
#    $strpoke1, $status_reg, $strpoke2,1,1;
#printf  "%s 0x%x %s 0x%x \#  status reg 0x%x \n",
#    $strpoke1, $status_reg, $strpoke2,1,1;

close LDF;
close OUT;
close IN;
