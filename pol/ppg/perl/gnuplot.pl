#!/usr/bin/perl -w
#
# Run this file e.g.
# /home/ebit/online/ppg/perl/gnuplot.pl /home/ebit/online/ppg/ppgload/ebit.dat 5 1490.010000 0 /data/ebit/info/runXX_ebit.png 500 
#
######################################################################
#                                                                        
# gnuplot.pl 
#
#  Input Parameters:
#         filename
#         number of inputs to plot
#         max time range (x axis)
#         loopcount  flag 0 = loops truncated, 1= not truncated (0 gives message on the plot)
#         name of output file (run$rn_ebit.png)
#         run number
use warnings;
use strict;
our ( $file, $nplot, $max_time, $lc, $outfile, $rn ) = @ARGV;# input parameters
our $nparam=6; # need 6 input params
my ($count, $line,  $index,$i,$j,$k);
my (@names,@array,@labels);
my $trmsg="";
my $name="gnuplot.pl";
##################### G L O B A L S ####################################
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $EXPERIMENT="ebit";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
#our $MTALK=32; # talk

#######################################################################
use lib "/home/ebit/online/ppg/perl";
require "send_message.pl";

$file = strip ($file);
$outfile = strip ($outfile);

$|=1; # flush output buffers
$count = @ARGV;
print "gnuplot.pl: starting with num arguments = $count\n";
if($count != $nparam)
{
    send_message($name,$MINFO,"FAILURE: not enough parameters supplied ($nparam); expect $count " ) ;
    die "filename; max_time; number of plots; run number; loop count flag;\n";

}

print   "Starting with parameters:  \n";
print    "  filename = $file;   max_time = $max_time; number of plots=$nplot \n";
print    "  loop count flag = $lc;  output filename = $outfile; run number = $rn  \n";

my ($xmax,$xmin,$xoffset,$yoffset);
# calculate x axis offsets of 5% to get plots away from y axes
$xmin = 0.05 * $max_time;
$xmax = $xmin + $max_time;
#$xoffset = (0.05 * $xmin);
$xmin *= -1;
#$xoffset += $xmin;
$xoffset =0; # these offsets are for the plot labels e.g. TDCBLOCK(ch14)
$yoffset = 0.4;
#printf "label offsets are $xoffset,$yoffset;  xmin = $xmin\n";

# make sure we can open this output file
unless (-e $outfile)
{
   my $tmp;
   print "opening $outfile\n";
   open (RFC,">$outfile") or ($tmp= $!);
   if ($tmp)
   { 
      send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
      die "FAILURE cannot open file \"$file\";$tmp\n";
   }
   close RFC;
   print "closed $outfile\n";
}


unless (-e $file)
{
  send_message($name,$MINFO,"FAILURE file \"$file\" does not exist" ) ;
  die "gnuplot.pl: no such file as $file\n";
}
open (RFC,$file) or die $!;
$line=<RFC>;
print $line;
$line =~tr/ / /s;
chomp $line;
@names = split (/ /,$line);
shift @names; # remove #

# make sure the file contains at least one PPG input
$i=@names;
unless ($i)
{
    send_message($name,$MINFO,"FAILURE file \"$file\" first line does not contain any PPG signal names" ) ;
    die "gnuplot_awg.pl: file \"$file\" first line does not contain any PPG signal names  \n";
}
foreach $index (@names)
{ 
    print "$index\n";  
}



$j=2;$k=0;$i=0;
foreach $index (@names)
{
    #my @fields = split /\(/, $index; # this if we want to remove the e.g. "(ch14)" from the label
  
    unless($k)	
    { # first valid time 
#	$array[$i]="\  '$file\' using 1:(\$$j+$k) with steps title \"$index $k\""; # for the key
	$array[$i]="\  '$file\' using 1:(\$$j+$k) with steps notitle "; # notitle-> no key
        $labels[$i]="\"$index\" at $xoffset,$k-$yoffset\n"; # instead of a key, label each plot
    }

    else
    {
#	$array[$i]=", \'$file\' using 1:(\$$j+$k) with steps title \"$index $k\""; # for the key	
	$array[$i]=", \'$file\' using 1:(\$$j+$k) with steps notitle"; # notitle-> no key
        $labels[$i]="set label \"$index\" at $xoffset,$k-$yoffset\n"; # instead of a key, label each plot
    }
    $j++;$i++;$k+=2;
  
}

$nplot*=2; # double because plots are separated

if ($lc) 
{ # include truncated message if loop(s) have been truncated; otherwise left blank
    $trmsg ="\"Note: number of loops shown on plot has exceeded limit -> truncated\" at 0,$nplot";
}
print("plot \n");
$i=0;
foreach $index (@array) {
    print "$index\n"; 
}

print "labels:\n";
foreach $index(@labels) {
    print " $index\n";
}
foreach $index (@labels) {
    print "$index\n"; }

open (OUT,"|gnuplot") or die $!;
print OUT<<eot;
set xlabel "time"
set title "Transitions in one PPG cycle   Run $rn"
set time
#set label "Note: max number of loops shown expanded on plot is 25" at 0,$nplot
set label $trmsg
set label @labels
set yrange [-1:$nplot + 1]
#set xrange [0:$max_time]
set xrange [$xmin:$xmax]
#plot @array

#pause 10;
set term png colour 
set out "$outfile"
#replot
plot  @array
eot

exit;



