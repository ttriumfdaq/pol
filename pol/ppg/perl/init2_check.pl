# required by perlscripts get_next_run_number.pl & change_mode.pl
#
# Checks that only one copy of this perlscript ($name) is running
# and that a perlscript ($other_name) that may interfere is NOT running
# and that enough input parameters are supplied
#
# $Log: init2_check.pl,v $
# Revision 1.1  2011/04/15 18:40:58  suz
# new to vmicpol
#
# Revision 1.6  2005/09/26 17:51:43  suz
# change message to indicate too few or too many parameters supplied
#
# Revision 1.5  2004/10/04 21:54:17  suz
# change recipe for match so it handles m20 & m15
#
# Revision 1.4  2004/06/15 20:03:45  suz
# add support for POL not checking other_name (no MUSR run numbering)
#
# Revision 1.3  2004/06/10 18:26:43  suz
# check for valid beamline after check on expt
#
# Revision 1.2  2004/06/09 21:34:37  suz
# add check on beamline
#
# Revision 1.1  2004/06/09 00:30:54  suz
# initial version - common code to check interfering perlscripts are not both running
#
#
use strict;
our ($TRUE,$EXPERIMENT,$FALSE,$TRUE,$MERROR,$MINFO,$DIE,$CONT);
#
our ($inc_dir, $expt, $beamline ) ; # no other parameters are needed
our $len;
our $name ; # same as filename
our $other_name; # name of other perlscript that may interfere
our $parameter_msg;
our $nparam;  # no. of input parameters
our $outfile;
our $status;

my $process_id;
my $debug=$TRUE;
my ($loop,$flag);

$nparam = $nparam + 0;  #make sure it's an integer
# can't check if script is running already unless there's an experiment; can't msg either
unless ($expt) {    die  "$name:  FAILURE -  No experiment supplied \n"; }
# send msg only if $EXPERIMENT is defined
if ($expt) { $EXPERIMENT = $expt; } # for msg

# we must have a beamline or we can't check for second copy running
unless ($beamline) 
{
# we can print to screen. Can't print to file yet since outfile not open
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 

# msg as there's an experiment
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: no beamline is supplied") ; 
    die  "$name:  FAILURE -  No beamline supplied \n";
}

if($debug){ print "init2_check.pl: starting\n"; }
# check if another copy of this perlscript is running
# or a copy of $other_perlscript
#
LOOP: for ($loop=0; $loop<3; $loop++)
{
    $flag = $FALSE;
    open PS, "ps -efw |";
    while (<PS>) 
    {
	if ($debug){ print "$_";}
	if($loop == 0)
	{ # do this once only
	    if (/[\s]([\d]+)[\s].*perl.*$name.pl.*$beamline/) # look for space +1 or more digits
                                          # followed by space etc
	    {
		$process_id = $1;
		print "$name: $expt is running  with PID $process_id\n" ;
		if ($$ == $process_id)
		{ 
		    print " .... this is my process\n";
		}
		else 
		{ 
		    # there is no file open yet
		    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: a copy of $name is already running with PID: $$") ;
		    unless($status) { print "$name: Failure from odb_cmd (msg)";} # no file yet
		    die  "$name:  FAILURE -   a copy of $name is already running with PID: $$\n";
		}
	    }
	}
	if($other_name)
	{
# now check if $other_perlscript is running... they interfere
    if (/[\s]([\d]+)[\s].*perl.*$other_name.pl.*$beamline/) # look for space +1 or more digits
                                          # followed by space etc
	    {
		$process_id = $1;
		print "$expt is running $other_name.pl with PID $process_id;loop $loop, sleeping 3s...\n" ;
		sleep 3; # sleep for 3s
		$flag = $TRUE;
	    }
	} # $other_name defined
    } # end of while(PS)
    close(PS) ;
    unless ($flag)
    {    # FOUT not yet defined
	print "$name: $other_name.pl is not running for this experiment\n";
	last LOOP;
    }
} # end if for loop
if($loop >= 3)
{
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: timeout waiting for $other_name.pl to finish; cannot proceed " ) ;
    unless($status) { print  "$name: Failure from odb_cmd (msg)";} #no file yet
    die  "$name:  FAILURE - timeout waiting for $other_name.pl to finish; cannot proceed ";
}


$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and msg, then exit. Cannot print to file (not open). 
    print "$name:   Supplied parameters: @ARGV \n";
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
   
#  msg as there's an experiment
    if ($len < $nparam)
    {
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
    }
    else
    {
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too many input parameters supplied " ) ;
    }
    unless($status) { print "$name: Failure from odb_cmd (msg)";}
    die  "$name:  FAILURE -  Incorrect number of parameters supplied ";
  }

unless ($beamline)
{
  odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
  die  "FAILURE:  beamline  not supplied \n";
}
if ( ( $expt =~ /bn[qm]r/i)  ||  ($expt =~ /pol/i) ||   ($expt =~ /mpet/i) ||  ($expt =~ /ebit/i)  )
{
    # beamline and experiment name should be the same for bnmr/bnqr/pol and titan experiments
    if ($expt ne $beamline)
    {
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: illegal beamline $beamline for experiment $expt") ;
	unless($status) { print "$name: Failure from odb_cmd (msg)";}
	die  "$name:  FAILURE - illegal beamline \"$beamline\" for experiment $expt\n ";
    }
}
elsif ($expt =~ /musr/i)
{
    unless ( ($beamline =~ /m9b/i) ||  ($beamline =~/dev/i) || ($beamline =~ /m20/i) ||
	    ($beamline =~ /m15/i) ) 
    {
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: illegal beamline $beamline for experiment $expt") ;
	unless($status) { print "$name: Failure from odb_cmd (msg)";}
	die  "$name:  FAILURE - illegal beamline \"$beamline\" for experiment \"$expt\" \n ";
 
    }
}
else
{
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: unknown experiment $expt ") ;
    die  "$name:  FAILURE - unknown experiment \"$expt\" \n ";
 
}

# now open the output file since $beamline is defined
$outfile=sprintf("%s/%s",$beamline,$outfile);
open_output_file($name, $outfile, $process_id); # FOUT
#
