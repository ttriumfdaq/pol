#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# invoke this script like this:
#                  include path            experiment   command     beamline      
# mode_check.pl /home/bnmr/online/ppg/perl  ebit       filename     ebit
#  e.g. mode_check.pl  /home/ebit/online/ppg/perl ebit /home/bnmr/online/tunes/1e/defaults.odb ebit
#
# This perlscript opens and reads a saved modefile, then runs odb cmd "ls" on all the keys
# to make sure they exist. 
# Needed because odb cmd "load" creates keys if they don't exist  
#
# Users run this file by running check_file.pl <expt> <filename> in the modefile directory
#  i.e. /home/<expt>/online/modefiles.  See AAA_README in that area for more info.
#
# $Log: mode_check.pl,v $
# Revision 1.1  2011/04/15 18:40:58  suz
# new to vmicpol
#
# Revision 1.1  2005/09/27 20:04:41  suz
# original: checks modefiles
#
#
############### G L O B A L S ####################################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
# parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt, $filename, $beamline ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "mode_check"; #same as filename
our $parameter_msg = "include_path, experiment, use path, command,   beamline ";
our $outfile = "mode_check.txt";
our $nparam = 4;  # no. of input parameters

##################################################################

$|=1; # flush output buffers

my ($transition, $run_state, $path, $key, $status);
my $debug=$FALSE;
my @out_array;
my ($lineno,$line);
my ($my_path, $my_key, $tmp);
my $error_count=0;
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt;  filename=\"$filename\"  beamline = $beamline \n";
print   "$name starting with parameters:  \n";
print   "Experiment = $expt;  filename=\"$filename\"  beamline = $beamline \n";


# output from the command will be sent to FOUT
# because this is for use with the browser and STDOUT and STDERR get set to null
#

unless (open (FILE, $filename))
{
    print_2($name, "can't open file $filename ($!)",$MERROR);
}
$lineno = 1;
while (<FILE>) 
{
    if($debug){print $lineno++;}
    $line=$_;
    if($debug){ print ":$line"; }
    chomp $line;
    if ( /^\[/)
    { # found a directory
	$line=~ s/[\[\]]//g;  # remove square brackets
	$my_path = $line;     # it is the path
	if($debug){ print "my path: $my_path\n"; }   
    }
    else
    { # found a key
	($my_key)=split (/=/,$line); # take the string prior to "="
	if($debug)
            { print "my path: $my_path\n"; 
              print "my key: $my_key\n"; }
	($status) = odb_cmd ( "ls",$my_path,$my_key );
	unless  ($status) 
	{ 
	    print "\n *****  Failure on ls \"$my_path\ $my_key\" from odb_cmd ****\n"; 
	    $error_count++;	   
	    print FOUT "\n *****  Failure on ls \"$my_path\ $my_key\" from odb_cmd ****\n"; 
	}
	if($debug)
	{
	    print_2 ($name,"after odb_cmd, ANSWER=$ANSWER ",$CONT);
	    print_2 ($name,
"\n===========================================================",$CONT);
	}
    }
    
}
close FILE;
#
unless($error_count)
{
    print "\n   SUCCESS File \"$filename\" has been checked \n";
    print FOUT "\n   SUCCESS File \"$filename\" has been checked \n";
}
else
{
    print "\n ***** Check path and key name(s) in file \"$filename\"\n";
    print " ***** DO NOT LOAD THIS FILE until the problem is fixed !! \n";
    print FOUT "\n ***** Check path and key names in file \"$filename\"\n";
    print FOUT " ***** Do not load this file until the problem is fixed!! \n";
}
print "\ndone\n";
print FOUT "done\n";
exit;
