#!/usr/bin/perl -w
#
# $Id: awg.pl,v 1.1 2011/04/15 18:40:58 suz Exp $
#
# Run this file e.g.
# /home/mpet/online/perl/awg.pl 
#    datafile nsample max_ms width_ms awg_data_filename online_dir run_num AWG_unit
#
# e.g. /home/ebit/online/ebit/perl/awg.pl 
#            /home/ebit/online/ppg/ppgload/ebit.dat 
#              21 1517.0000 20  
#            softdac-ch0
#            /home/ebit/online
#            451 
#            0
#
#                             Input Parameters:                             
#                                             
# awg.pl 
#         data file written by plot_transitions  (ebit.dat) including full path
#         number of points to plot
#         max time range (x axis)
#         pulse width of awg clock pulse
#         constant part of AWG data filename written by driver, e.g. softdac-ch0 or da816-ch0  
#         path of online  e.g. /home/ebit/online (= path of AWG data filename above)
#         run number
#         AWG unit 0 or 1
#
# Looks in ebit.dat for PPG signals AWGHV (Unit 0) or AWGAM (Unit 1)
# These signal names must agree with those defined in tri_config.h
use warnings;
use strict;


our ( $datafile, $nsample, $max_time, $pulse_width , $awgfilename, $online_path, $rn, $unit) = @ARGV;# input parameters
our $nparam=8; # need 8 input params 
my @last;
my @volt;
my ($i,$j,$t,$k);
my ($count,$chan,$filename);
my ($outfile, $userfile);
my @ppg_signals = ("AWGHV","AWGAM"); # must agree with awg_trigger_names defined in tri_config.h
my @unit_names = ("zero","one");
my @output_line;
my $item;
my $status;
my $name = "awg.pl";

##################### G L O B A L S ####################################
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $EXPERIMENT="ebit";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $DEBUG=0;
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
#our $MTALK=32; # talk

#######################################################################
use lib "/home/ebit/online/ppg/perl";
require "send_message.pl";

$|=1; # flush output buffers

$datafile = strip ($datafile);
$awgfilename = strip ($awgfilename);
$online_path = strip ( $online_path);

$count = @ARGV;
print "awg.pl: starting with num arguments = $count\n";
if($count != $nparam ){ die "not enough arguments\n";}
print   "awg.pl  starting with parameters:  \n";
print    "  filename = $datafile;  number of samples=$nsample  \n";
print    "  max time = $max_time;  pulse width=$pulse_width ms \n";
print    "  awg data file name = $awgfilename ; online dir path = $online_path \n";
print    "  run number=$rn;  AWG unit= $unit\n";

unless ( ($unit ==0)  or ($unit == 1) )
{
    send_message($name,$MINFO,"FAILURE: illegal AWG unit $unit, Must be 0 or 1" ) ;
    die "illegal AWG unit $unit. Must be 0 or 1\n";
}

my $ppg_name = $ppg_signals[$unit];
print "AWG Unit $unit  PPG signal name $ppg_name\n";

$chan = 0;
while ($chan < 8)
{
  $filename= "$online_path/$awgfilename$chan.log";# -> e.g. /home/online/ebit/softdac-ch0
  print "filename: $filename\n"; 
  unless (-e $filename)
  {
      send_message($name,$MINFO,"FAILURE file \"$filename\" does not exist" ) ;
      die "awg.pl: no such file as $filename\n";
  }

  open FILE,$filename or die $!;
  while (<FILE>)
  {
    push @last, $_; # add to the end
    shift @last if @last > $nsample; # remove line from beginning
  }
  print "last $nsample lines:\n",@last;
  

  $count=0;
  foreach $_ (@last)
  {
    if(/Off:([\d]+).*Volt:([\d\-\.]+)/)
    {
	unless($count == $1) {die "incorrect sample number; got $1 expect $count";}
#	print "sample=$1 Volt=$2\n";
	$volt[$chan][$count]=$2;
#	print "volt[$chan]=$volt[$chan][$count]\n";
	$count++;
    }
   }
   $chan++;
}
my $gcol = '' ;
open (IN,"$datafile") or die "Can't open input file: $datafile\n" ;
$_ = <IN> ; chomp ; tr/ / /s ;
my @col = split(/ /,$_) ;
for ($i=0;$i<@col;$i++) {
  if ($col[$i] =~ /$ppg_name/) { $gcol = $i ; last ; }
}

unless ($gcol) { 
    send_message($name,$MINFO,"FAILURE: could not find signal $ppg_name in file $datafile" ) ;
    die "$name: Error... could not find signal $ppg_name in file $datafile\n";
}

print "$ppg_name found in col $gcol\n" ;

my $p ; my $p0='' ; $i = 0 ; my @T ;
while (<IN>) {
  chomp ; tr/ / /s ;
  @col = split(/ /) ;
  $p = $col[$gcol] ;
  if ($p ne $p0 and $p) { 
    print "$col[0] $col[$gcol]\n" ;
    $T[$i] = $col[0] ; $i++ ;
  }
  $p0 = $p ;
}
#           top of data file might read:
#
#  SPARE(ch15) TDCBLOCK(ch14) TDCGATE(ch13) AWGAM(ch11) RFTRIG2(ch5) EGUN(ch2) LSTEP
#0.0000       1 1 0 0 0 0 0  # STDPULSE1,TDCBLK_S0


# open e.g.  /home/online/awg/awg_zero.dat
$outfile="$online_path/awg/awg_$unit_names[$unit].dat";
open (OUT,">$outfile") or die $! ;

# make a file that users can easily read to check on AWG voltages
$userfile = "$online_path/awg/awg_$unit_names[$unit].txt";
open (USER,">$userfile") or die $! ;

print OUT "# $max_time  $pulse_width $rn  $unit # max time (ms); pulse width (ms); run number; AWG unit\n";
print OUT "-1 0 0 0 0 0 0 0 0 0 0\n";
@output_line = ""; # clear the array

print USER "AWG Unit $unit   Run $rn  \n";
print USER "Index     Time  Channels:";

for ($j=0;$j<8;$j++) {
  push @output_line, sprintf "%4d    ",$j;
}
print USER "@output_line\n";
print USER "          (ms)       ";
for ($j=0;$j<8;$j++) {
  print USER "    Volts";
}
print USER "\n";



for ($j=0;$j<$nsample;$j++) {
    @output_line = ""; # clear the array
  print OUT "$j $T[$j] 1 " ;
#  $item =  sprintf "%4.0d ",$j;
#  push @output_line, $item;
 push @output_line, sprintf "%4d ",$j;
  #if($j<2){ print "array:@output_line\n"; }
  $item =  sprintf "%8.1f      ",$T[$j];
  push @output_line, $item;
  #if($j<2){ print "array:@output_line\n"; }

#  print USER "$j, $T[$j]";
  for ($i=0;$i<8;$i++) {
    print OUT "$volt[$i][$j] " ;
    $item = sprintf " %7.2f",$volt[$i][$j]  ;
    push @output_line, $item;    
   # if($j<2){ print "array:@output_line\n"; }
  }
# print "\n";
  print OUT "\n" ;
  print USER "@output_line\n";

  $t = $T[$j]+$pulse_width; 
  print OUT "$j $t 0 " ;
  for ($i=0;$i<8;$i++) {
    print OUT "$volt[$i][$j] " ;
  }
  print OUT "\n" ;
}
# write final values at final time 
  print OUT "$nsample $max_time 0 ";
  for ($i=0;$i<8;$i++) {
    print OUT "$volt[$i][$nsample-1] " ;
}
  print OUT "\n" ;
close OUT;
close USER;
print "awg.pl:  AWG Unit $unit data to plot is in:  $outfile\n";
print "         AWG Unit $unit user-readable file is:  $userfile\n";


