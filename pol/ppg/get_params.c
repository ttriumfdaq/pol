/* 
   Name: get_params.c
   Created by: SD

   Contents: Get the parameters for each block from odb and convert to transitions

   $Id: get_params.c,v 1.1 2011/04/15 18:50:31 suz Exp $
  
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h> // fabs
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
#include "get_params_constants.h"



#ifndef POL
extern TITAN_ACQ_SETTINGS settings;
#else
extern POL_ACQ_SETTINGS settings;
#endif
extern TRANSITIONS *ptrans;
extern HNDLE hDB;
extern const char awg_trigger_names[NUM_AWG_MODULES][16];
extern double min_delay;
//extern FILE *tfile;

INT my_unit;


/* get the parameters from the different blocks in odb */

INT get_params( HNDLE hKey, KEY *key, INT my_index, INT block_index, INT total_size, char *name )
{
  /* parameters: 

     my_index     array index  - block number of this block type
     block_index  array index to this block (i.e. block number of all blocks)
     name         name of this block (upper case, space replaced by underscores)
  */
  HNDLE hMyKey,hTmp;

  double fzero=0.0;
  DWORD dzero=0;

  INT i,j,size,status,my_ref,len;
  double my_offset,ftmp;
  INT define_refindex, my_refindex;
  BOOL gotit;
  INT this_loop;
  char string[20];
  char my_string[128],param[20];
  char my_loopname[10];
  double loopwidth,looptime;
  INT lindex;
  BOOL auto_param;



 printf("get_params: total number of blocks : %d \n", block_counter[TOTAL]);

#ifndef POL
      std_pulse_info.pulse_width_ms =  settings.ppg.input.standard_pulse_width__ms_;
#else
      std_pulse_info.pulse_width_ms =  settings.input.standard_pulse_width__ms_;
#endif

  if (strncmp(name,"TIME_REF",3)==0) 
    {
    
      /* can no longer use create rec, get rec (user wants defaults for absent params) */  
      /*     size = sizeof(time_ref_info.time_ref_block);
      if(size == total_size)  // structure has expected size
	{
	  if(debug)printf("getting record for block %s\n",name);
	  status= db_get_record(hDB, hKey, &time_ref_info.time_ref_block, &size, 0);

	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)\n",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
      else
      {*/
	  // see what parameters we do have
	  if(total_size == 0)
	    {
	      if(debug)printf("no parameters supplied for block %s; using defaults\n",name);
	      // no parameters.... use defaults
	      sprintf(time_ref_info.time_ref_block.time_reference,"");
	      time_ref_info.time_ref_block.time_offset__ms_ =0.0;
	    }
	  else
	    {
	      size=sizeof(  time_ref_info.time_ref_block.time_reference);
	      status = get_string(hKey, "time reference",
				  time_ref_info.time_ref_block.time_reference,&size, 
				  "",USE_DEFAULT,name); // default value found later
	      if(status != SUCCESS)return status;
	      
	      status = get_double(hKey, "time offset (ms)", 
				  &time_ref_info.time_ref_block.time_offset__ms_, 
				  &fzero,USE_DEFAULT,name); // default will be found later
	      if(status != SUCCESS)return status;
	      
	    }
	  //}
      if(debug)
	{
	  printf("\nget_params: found time reference block index %d at total block index %d\n",
		 my_index, block_index);
	  printf("number of time references defined so far : %d\n", gbl_num_time_references_defined);
	}
      /* make sure that our time reference exists */
      if(get_this_ref_index( time_ref_info.time_ref_block.time_reference, name,
			     &my_refindex) != SUCCESS)
	
	{
	  cm_msg(MERROR,"get_params","time reference \"%s\" is not defined (block %s) ",
		 time_ref_info.time_ref_block.time_reference   ,name);
	  return DB_INVALID_PARAM;
	}

      /* derive name for the reference we are defining */
        sprintf(string, "%s%s","_T",name); 
  

    
      
      /* check whether reference string to be defined is unique, and return the next available timeref_number  */
      if(get_next_ref_index( string,  name, 
			     &define_refindex)!= SUCCESS)
	return DB_INVALID_PARAM;
      /* fills defines_timeref_index and this_reference_name in structure time_ref_params
	 now fill the other parameters
      */
      if (add_time_ref(define_refindex, my_refindex, timeref_code_time,  
		   time_ref_info.time_ref_block.time_offset__ms_,
		   block_index, name) != SUCCESS)
	return DB_INVALID_PARAM;
      //if(debug)
	printf("defining reference refstring = %s (index %d) at time offset %f from reference %s (block %s)\n",
	       string,define_refindex,  time_ref_info.time_ref_block.time_offset__ms_,  
	       time_ref_info.time_ref_block.time_reference,name  );
    }
  

  else if  (strncmp(name,"STDPULSE",3)==0)
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params)  
      size = sizeof(std_pulse_info.std_pulse_block);
      if(size == total_size)  // structure has expected size
	{
	  status= db_get_record(hDB, hKey, &std_pulse_info.std_pulse_block, &size, 0);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)\n",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
      else
      {*/




	  // see what parameters we do have
	  if(total_size == 0)
	    {
	      cm_msg(MERROR,"get_params","PPG signal name must be supplied in block %s",name);
	      return FE_ERR_ODB;
	    }
	  size=sizeof( std_pulse_info.std_pulse_block.time_reference);
	  status = get_string(hKey, "time reference", 
			      std_pulse_info.std_pulse_block.time_reference,&size,
			      "", USE_DEFAULT, name); // default supplied later
	  if(status != SUCCESS)return status;

	  size=sizeof(   std_pulse_info.std_pulse_block.ppg_signal_name);  
	  status = get_string(hKey, "ppg signal name",
	     std_pulse_info.std_pulse_block.ppg_signal_name,&size,
			      default_ppg_signal_name,UPDATE_DEFAULT, name);
	  if(status != SUCCESS)return status;

	  status = get_double(hKey, "time offset (ms)", 
			      &std_pulse_info.std_pulse_block.time_offset__ms_,
			      &fzero,USE_DEFAULT ,name); // default will be found later
	  if(status != SUCCESS)return status;
	  // }

      if(strlen(std_pulse_info.std_pulse_block.ppg_signal_name)==0)
	{
	  cm_msg(MERROR,"get_params","no PPG signal name has been supplied \"%s\"(block %s) ",
		 std_pulse_info.std_pulse_block.ppg_signal_name,name);
	  return DB_INVALID_PARAM;
	}
	   
      pulse_params[my_index].block_name[0]='\0';
      strncat(pulse_params[my_index].block_name,name, BLOCKNAME_LEN);

      pulse_params[my_index].time_ms = std_pulse_info.std_pulse_block.time_offset__ms_;
      pulse_params[my_index].width_ms =   std_pulse_info.pulse_width_ms;
      strcpy(pulse_params[my_index].output_name,std_pulse_info.std_pulse_block.ppg_signal_name);
      i = check_ppg_name(pulse_params[my_index].output_name);
      //printf("after check_ppg_name i=%d my_index=%d\n",i,my_index);
      if(i == -1)
	{
	  cm_msg(MERROR,"get_params","no such PPG signal name as \"%s\"(block %s) ",
		 std_pulse_info.std_pulse_block.ppg_signal_name,name);
	  return DB_INVALID_PARAM;
	}
      
      pulse_params[my_index].ppg_signal_num = i;

   
      status = add_pulse_block( std_pulse_info.std_pulse_block.time_reference, my_index, 
				block_index, AUTO_TIMEREFS, name);
      if(status != SUCCESS)	 
	return status;
      

#ifdef AUTO_TDCBLOCK
      /* add TDCBLOCK pulse to mark start of this std width pulse
	 do not add timerefs for TDCBLOCK
      */
      
      status = auto_add_tdcblock( time_ref_params[pulse_params[my_index].time_ref_index].this_reference_name, 
			       pulse_params[my_index].time_ms,  my_index, name );
 
      if(status != SUCCESS)
	{ 
	  printf("get_params: Error status after returning from auto_add_pulse (%d) block %s\n",
		 status,name);
	  return DB_INVALID_PARAM;
	}
#endif //AUTO_TDCBLOCK

      print_default_time_reference();

    }
  
  else if  (strncmp(name,"PULSE",3)==0)
    {
      
     /* can no longer use create rec, get rec (user wants defaults for absent params)  
      size = sizeof(pulse_info.pulse_block);
            if(size != total_size)
	{
	  printf(" **** getting record for pulse..... \n");
	  status= db_get_record(hDB, hKey, &pulse_info.pulse_block, &size, 0);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)\n",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
      else
      {*/
      
	  // see what parameters we do have
	  if(total_size == 0)
	    {
	      cm_msg(MERROR,"get_params","PPG signal name must be supplied in block %s",name);
	      return FE_ERR_ODB;
	    }
	  size=sizeof( pulse_info.pulse_block.time_reference);
	  status = get_string(hKey, "time reference", 
			      pulse_info.pulse_block.time_reference,&size,
			      "", USE_DEFAULT, name); // default supplied later
	  if(status != SUCCESS)
	    return status;
	
	  size=sizeof( pulse_info.pulse_block.ppg_signal_name);
	  status = get_string(hKey, "ppg signal name", 
			      pulse_info.pulse_block.ppg_signal_name,&size,
			      default_ppg_signal_name, UPDATE_DEFAULT, name);
	  if(status != SUCCESS)return status;

	  status = get_double(hKey, "time offset (ms)", 
			      &pulse_info.pulse_block.time_offset__ms_,
			      &fzero,USE_DEFAULT,name); // default will be found later
	  if(status != SUCCESS)return status;

	  status = get_double(hKey, "pulse width (ms)",
			      &pulse_info.pulse_block.pulse_width__ms_, 
			      &default_pulse_width_ms, 
			      UPDATE_DEFAULT, name);
	  if(status != SUCCESS)return status;
	  //}


      if(strlen(pulse_info.pulse_block.ppg_signal_name)==0)
	{
	  cm_msg(MERROR,"get_params","no PPG signal name has been supplied \"%s\"(block %s) ",
		 pulse_info.pulse_block.ppg_signal_name,name);
	  return DB_INVALID_PARAM;
	}


      pulse_params[my_index].block_name[0]='\0';
      strncat(pulse_params[my_index].block_name,name, BLOCKNAME_LEN);

      pulse_params[my_index].time_ms = pulse_info.pulse_block.time_offset__ms_;
      pulse_params[my_index].width_ms =   pulse_info.pulse_block.pulse_width__ms_; 
      
    
	if( pulse_params[my_index].width_ms <  min_delay)
	  {
	    pulse_params[my_index].width_ms =  min_delay;
	    cm_msg(MINFO,"get_params","setting pulse width of pulse block \"%s\" to minimal delay",name);
	  }
      
      strcpy(pulse_params[my_index].output_name,pulse_info.pulse_block.ppg_signal_name);
      i = check_ppg_name(pulse_params[my_index].output_name);
      //printf("after check_ppg_name i=%d my_index=%d\n",i,my_index);
      if(i == -1)
	{
	  cm_msg(MERROR,"get_params","no such PPG signal name as \"%s\" (block \"%s\") ",
		 pulse_info.pulse_block.ppg_signal_name,name);
	  return DB_INVALID_PARAM;
	}
      pulse_params[my_index].ppg_signal_num = i;

      status = add_pulse_block(  pulse_info.pulse_block.time_reference,
			       my_index, block_index,  AUTO_TIMEREFS, name);
      if(status != SUCCESS)	 
	return status;   


#ifdef AUTO_TDCBLOCK
      /* add TDCBLOCK pulse to mark start of this pulse
	 do not add timerefs
      */     
      status = auto_add_tdcblock( time_ref_params[pulse_params[my_index].time_ref_index].this_reference_name, 
			       pulse_params[my_index].time_ms  ,
				  my_index, name );
      if(status != SUCCESS)
	{ 
	  printf("get_params: Error status after returning from auto_add_pulse (%d) block %s\n",
		 status,name);
	  return DB_INVALID_PARAM;
	}
#endif // AUTO_TDCBLOCK
      print_default_time_reference();
      
    }
  
	    

  else if  (strncmp(name,"TRANS",3)==0)
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(trans_info.trans_block);
      if(size == total_size)  // structure has expected size
	{
	  status= db_get_record(hDB, hKey, &trans_info.trans_block, &size, 0);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)\n",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
      else
      {*/
	  // see what parameters we do have
	  if(total_size == 0)
	    {
	      cm_msg(MERROR,"get_params","PPG signal name must be supplied in block %s",name);
	      return FE_ERR_ODB;
	    }
	  size = sizeof( trans_info.trans_block.time_reference);
	  status = get_string(hKey, "time reference", 
			      trans_info.trans_block.time_reference,&size,
			      "",USE_DEFAULT,name); // default value found later
	  if(status != SUCCESS)return status;
	  size = sizeof( trans_info.trans_block.ppg_signal_name);
	  status = get_string(hKey, "ppg signal name", 
			      trans_info.trans_block.ppg_signal_name,&size,
			      default_ppg_signal_name,   UPDATE_DEFAULT, name);
	  if(status != SUCCESS)return status;
	  
	  status = get_double(hKey, "time offset (ms)", 
			      &trans_info.trans_block.time_offset__ms_, 
			      &fzero,USE_DEFAULT,name); // default will be found later
	  if(status != SUCCESS)return status;
	  	
	  //}
      if(strlen(trans_info.trans_block.ppg_signal_name)==0)
	{
	  cm_msg(MERROR,"get_params","no PPG signal name has been supplied \"%s\"(block %s) ",
		 trans_info.trans_block.ppg_signal_name,name);
	  return DB_INVALID_PARAM;
	}

      pulse_params[my_index].block_name[0]='\0';
      strncat(pulse_params[my_index].block_name,name, BLOCKNAME_LEN);
      pulse_params[my_index].width_ms = 0.0; // transition; no pulse width
      pulse_params[my_index].time_ms = trans_info.trans_block.time_offset__ms_;
      strcpy(pulse_params[my_index].output_name,trans_info.trans_block.ppg_signal_name);
      i = check_ppg_name(pulse_params[my_index].output_name);
      if(i == -1)
	{
	  cm_msg(MERROR,"get_params","no such PPG signal name as \"%s\" ",
		 trans_info.trans_block.ppg_signal_name);
	  return DB_INVALID_PARAM;
	}
      pulse_params[my_index].ppg_signal_num = i;

      status = add_pulse_block( trans_info.trans_block.time_reference, my_index, 
				block_index, AUTO_TIMEREFS,   name);
      if(status != SUCCESS)	 
	return status;  

#ifdef AUTO_TDCBLOCK
     // add TDCBLOCK pulse to mark this transition; 
      status = auto_add_tdcblock(
	  time_ref_params[pulse_params[my_index].time_ref_index].this_reference_name,
			       pulse_params[my_index].time_ms,  my_index, name );
 
      if(status != SUCCESS)
	{ 
	  printf("get_params: Error status after returning from auto_add_pulse (%d) block %s\n",
		 status,name);
	  return DB_INVALID_PARAM;
	}
#endif // AUTO_TDCBLOCK
      if(debug)print_default_time_reference();            
    }
  



  else if  (strncmp(name,"PATTERN",3)==0)
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(pattern_info.pattern_block);
      if(size == total_size)  // structure has expected size
	{
	  status= db_get_record(hDB, hKey, &pattern_info.pattern_block, &size, 0);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)\n",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
      else
      {*/
	  // see what parameters we do have
	  if(total_size == 0)
	    {
	      cm_msg(MERROR,"get_params","Bit pattern must be supplied in block %s",name);
	      return FE_ERR_ODB;
	    }
	  size=sizeof(  pattern_info.pattern_block.time_reference);
	  status = get_string(hKey, "time reference", 
			      pattern_info.pattern_block.time_reference,&size,
			      "",USE_DEFAULT,name); // default value found later
	  if(status != SUCCESS)return status;

	  status = get_dword(hKey, "bit pattern", 
			     &pattern_info.pattern_block.bit_pattern,
			     &dzero, NO_DEFAULT,name); // bit pattern must be supplied
	  if(status != SUCCESS)return status;
	  
	  status = get_double(hKey, "time offset (ms)", 
			      &pattern_info.pattern_block.time_offset__ms_, 
			      &fzero,USE_DEFAULT,name); // default will be found later 
	  if(status != SUCCESS)return status;

	  
	  //}

	  // is this a replacement for PATTERN_T0 ?
          if (strcmp(name,"PATTERN_T0")==0 ||  ( pattern_info.pattern_block.time_offset__ms_ <= min_delay && 
						 strcmp(pattern_info.pattern_block.time_reference, "T0")==0))
	    { 
	      // replace existing T0 pattern block
	      printf ("Replacing T0 pattern block with %s\n",name);
	      status=replace_trans0(name, pattern_info.pattern_block.bit_pattern);
              print_transitions(ptrans,stdout);
	      return status;
	      
	    }

	 
	    
	  if(debug)printf (" pattern block: name=%s and offset=%f and time ref is %s\n",name,
		  pattern_info.pattern_block.time_offset__ms_, pattern_info.pattern_block.time_reference);
	    
	      
	  
      
      pulse_params[my_index].block_name[0]='\0';
      strncat(pulse_params[my_index].block_name,name, BLOCKNAME_LEN);
      pulse_params[my_index].width_ms = 0.0; // transition; no pulse width
      pulse_params[my_index].time_ms = pattern_info.pattern_block.time_offset__ms_;
      /* uses a bit pattern rather than a PPG signal name; set highest bit to indicate  */
      strcpy(pulse_params[my_index].output_name,"bitpattern");
      pulse_params[my_index].ppg_signal_num =( pattern_info.pattern_block.bit_pattern | 0x80000000) ;
      if(debug)printf("ppg_signal_num = 0x%x  0x%x\n", pattern_info.pattern_block.bit_pattern,
	     pulse_params[my_index].ppg_signal_num);


      /* add automatic time references to pattern blocks - but not TDCBLOCK pulse  */
	status = add_pulse_block( pattern_info.pattern_block.time_reference, my_index, 
				  	  block_index,  AUTO_TIMEREFS ,  name);

	if(status != SUCCESS)	 
	  return status;


    }


  else if  (strncmp(name,"DELAY",3)==0)
    { /*   can no longer use create rec, get rec (user wants defaults for absent params)
	   size = sizeof(delay_info.delay_block);
	   status= db_get_record(hDB, hKey, &delay_info.delay_block, &size, 0);
	   if (status != DB_SUCCESS)
	   {
	   cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)\n",
	   key->name,status);
	   return FE_ERR_ODB;
	   }
      */
      // see what parameters we do have
      
      if(total_size == 0)
	{
	  cm_msg(MERROR,"get_params","Delay time must be supplied in block %s",name);
	  return FE_ERR_ODB;
	}
      size=sizeof(  delay_info.delay_block.time_reference);
      status = get_string(hKey, "time reference", 
			  delay_info.delay_block.time_reference,&size,
			  "",USE_DEFAULT,name); // default value found later
      if(status != SUCCESS)return status;
      
      size=sizeof(  delay_info.delay_block.time_reference);
      status = get_string(hKey, "time reference", 
			  delay_info.delay_block.time_reference,&size,
			  "",USE_DEFAULT,name); // default value found later
      if(status != SUCCESS)return status;
      status = get_double(hKey, "time offset (ms)", 
			  &delay_info.delay_block.time_offset__ms_, 
			  &fzero,USE_DEFAULT,name); // default will be found later 
      if(status != SUCCESS)return status;
      
      pulse_params[my_index].block_name[0]='\0';
      strncat(pulse_params[my_index].block_name,name, BLOCKNAME_LEN);
      pulse_params[my_index].width_ms = 0.0; // delay/pattern/transition; no pulse width
      pulse_params[my_index].time_ms = delay_info.delay_block.time_offset__ms_;
      /*  set bit to indicate a delay  */
      strcpy(pulse_params[my_index].output_name,"delay");
      pulse_params[my_index].ppg_signal_num = 0x40000000 ;  // delay; do not change bit pattern
      /* add automatic time references to pattern blocks - but not TDCBLOCK pulse  */
      status = add_pulse_block( delay_info.delay_block.time_reference, my_index, 
				block_index,  AUTO_TIMEREFS ,  name);
      
      if(status != SUCCESS)	 
	return status;
    }


  else if(strncmp(name,"XY_STOP",6)==0)
    {      
      if(debug)
	{
	  printf("\nxy_stop blocks: \n");
	  for (i=0; i < block_counter[XY_STOP]; i++)
	    printf("xy_stop block %d ; no parameters\n",i); 
	} 
    }

#ifndef NO_AWG
  else if(strncmp(name,"EVSET",5)==0)
    { // set a single Voltage value to the AWG channels (loaded into AWG memory)
      size=sizeof( ev_set_info.ev_set_block.time_reference);
      status = get_string(hKey, "time reference", 
			  ev_set_info.ev_set_block.time_reference,&size,
			  "",USE_DEFAULT,name); // default value found later
      if(status != SUCCESS)return status;

      status = get_double(hKey, "time offset (ms)", 
			  &ev_set_info.ev_set_block.time_offset__ms_, 
			  &fzero,USE_DEFAULT,name); // default will be found later
      if(status != SUCCESS)return status;

     status = get_int(hKey, "AWG unit", 
		       &ev_set_info.ev_set_block.awg_unit, 
		       &default_awg_unit , UPDATE_DEFAULT, name);
     
     if( ev_set_info.ev_set_block.awg_unit < 0 || 
	 ev_set_info.ev_set_block.awg_unit > NUM_AWG_MODULES-1)
       {
	 cm_msg(MERROR,"get_params","AWG unit number (%d) is out of range. It must be between 0 and %d (block %s)\n",
		ev_set_info.ev_set_block.awg_unit, NUM_AWG_MODULES-1,name );
	 return DB_INVALID_PARAM;
       }

     printf("ev set params: AWG unit=%d,  time ref=\"%s\", time offset=%f  (block %s)\n",
	     ev_set_info.ev_set_block.awg_unit,
	     ev_set_info.ev_set_block.time_reference, 
	    ev_set_info.ev_set_block.time_offset__ms_,
	    name);
 
      awg_params[ev_set_info.ev_set_block.awg_unit].in_use=TRUE;

     /* check that my time reference is valid .. or if not supplied, default will be used ->my_refindex */     
     if(get_this_ref_index( ev_set_info.ev_set_block.time_reference, name,  &my_refindex)!=DB_SUCCESS)
       {
	 cm_msg(MERROR,"get_params","time reference \"%s\" is not defined (block %s) ",
		ev_set_info.ev_set_block.time_reference,name);
	 return DB_INVALID_PARAM;
       }

     // Update parameter block
     ev_set_params[my_index].block_name[0]='\0';
     ev_set_params[my_index].time_ms =  ev_set_info.ev_set_block.time_offset__ms_;
     strncat(ev_set_params[my_index].block_name,name, BLOCKNAME_LEN);
     ev_set_params[my_index].time_ref_index = my_refindex;
     ev_set_params[my_index].t0_offset_ms =  time_ref_params[my_refindex].t0_offset_ms +  
       ev_set_params[my_index].time_ms;
      ev_set_params[my_index].awg_unit = ev_set_info.ev_set_block.awg_unit;
  
     printf("get_params: after get_this_ref_index, my time_ref_string=%s for my_refindex=%d  t0_offset_ms = %f\n",	
	    time_ref_params[my_refindex].this_reference_name,  my_refindex, ev_set_params[my_index].t0_offset_ms );
     print_ev_set_params(my_index, FALSE,stdout);

     /* Now get the Set Values */

     sprintf(ev_set_params[my_index].set_values_string," "); //  string for get_ev_double must start with blank (not "(" )
     size = sizeof(ev_set_params[my_index].set_values_string)-1; 
     status = get_string(hKey,"Set Values (Volts)" , &ev_set_params[my_index].set_values_string[1],&size,
			 "", USE_DEFAULT, name);
     if(status != SUCCESS)
       return status;

     printf("ev_set_params[%d].set_values_string=%s\n",
	    my_index,
	    ev_set_params[my_index].set_values_string);

     
     print_ev_set_params(my_index,FALSE,stdout); // don't print the set values
    
   

      /* create a time reference for this block */
      sprintf(string, "_T%s", name); // name of time reference
      /* check whether time reference string to be defined is unique, and return the next available timeref_number  */
      if(get_next_ref_index( string,  name, &define_refindex)!= SUCCESS)
	return DB_INVALID_PARAM;
      /* get_next_ref fills  
	 time_ref_params[define_refindex].defines_timeref_index and  
	 time_ref_params[define_refindex].this_reference_name
	 now fill the other parameters by calling add_time_ref
      */
      printf("calling add_time_ref with params define_refindex=%d, my_refindex=%d\n",
	     define_refindex, my_refindex);    
      if (add_time_ref(define_refindex, my_refindex, timeref_code_begin_loop,  
		   ev_set_info.ev_set_block.time_offset__ms_, 
		   block_index, name) != SUCCESS)
	return DB_INVALID_PARAM;
      printf("defined ev_set reference = %s (time_ref index %d) using reference = %s\n",
	     time_ref_params[define_refindex].this_reference_name,
	     define_refindex,
	     time_ref_params[my_refindex].this_reference_name);

      /* add this to TRANSITIONS RECORD */
      //      if(add_transition(block_index,my_index, my_refindex, ev_set_params[my_index].time_ms) != SUCCESS) 
     strcpy( ptrans[gbl_num_transitions].block_name, ev_set_params[my_index].block_name);
      ptrans[gbl_num_transitions].code    = EV_SET_CODE;      
      ptrans[gbl_num_transitions].block_index    = block_index;
      ptrans[gbl_num_transitions].t0_offset_ms =  ev_set_params[my_index].t0_offset_ms;
      ptrans[gbl_num_transitions].time_reference_index = 
	ev_set_params[my_index].time_ref_index;
      ptrans[gbl_num_transitions].bitpattern = 0x40000000; // use a delay bitpattern (no change)
      ptrans[gbl_num_transitions].loop_index = my_index; // use this to point to index in ev_set_params block
      gbl_num_transitions++;
      if(gbl_num_transitions > MAX_TRANSITIONS )
	{
	  cm_msg(MERROR,"get_params","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
	  printf("get_params: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
	  return DB_INVALID_PARAM;
	}


      if(add_awg_pulses(  ev_set_params[my_index].awg_unit, my_refindex, 
			  ev_set_params[my_index].time_ms, name) != SUCCESS)
	return  DB_INVALID_PARAM;


     /* Note: TDCGATE not added automatically */

      print_transitions(ptrans,stdout);
	

    }




  else if(strncmp(name,"EVSTEP",5)==0)
    {  /* AWG step block */
      size=sizeof( ev_step_info.ev_step_block.time_reference);
      status = get_string(hKey, "time reference", 
			  ev_step_info.ev_step_block.time_reference,&size,
			  "",USE_DEFAULT,name); // default value found later
      if(status != SUCCESS)return status;

      status = get_double(hKey, "time offset (ms)", 
			  &ev_step_info.ev_step_block.time_offset__ms_, 
			  &fzero,USE_DEFAULT,name); // default will be found later
      if(status != SUCCESS)return status;

      status = get_int(hKey, "AWG unit", 
		       &ev_step_info.ev_step_block.awg_unit, 
		       &default_awg_unit , UPDATE_DEFAULT, name);
      if(status != SUCCESS)return status;

      
      if( ev_step_info.ev_step_block.awg_unit < 0 || 
	  ev_step_info.ev_step_block.awg_unit > NUM_AWG_MODULES-1)
	{
	  cm_msg(MERROR,"get_params","AWG unit number (%d) is out of range. It must be between 0 and %d (block %s)\n",
		 ev_step_info.ev_step_block.awg_unit, NUM_AWG_MODULES-1,name );
	  return DB_INVALID_PARAM;
	}
      
      awg_params[ev_step_info.ev_step_block.awg_unit].in_use=TRUE;
 
 
      //    if(debug)
      printf("ev step params: AWG unit=%d,  time ref=%s (block %s)\n",
	     ev_step_info.ev_step_block.awg_unit,
	     ev_step_info.ev_step_block.time_reference, name);
     
    


      /* check that my time reference is valid .. or if not supplied, default will be used ->my_refindex */     
      if(get_this_ref_index( ev_step_info.ev_step_block.time_reference, name,  &my_refindex)!=DB_SUCCESS)
	return  DB_INVALID_PARAM;

      /* this must be referenced to a BEGIN_LOOP index */
      if(time_ref_params[my_refindex].timeref_code != 1 )
	{
	  cm_msg(MERROR,"get_params","block %s must be referenced to a begin_loop time reference, not %s",
		 name, time_ref_params[my_refindex].this_reference_name );
	  return DB_INVALID_PARAM;
	}
      // check there is no other ev_step loop open at present (for this unit)
      if(ev_step_open_flag[ ev_step_info.ev_step_block.awg_unit] == TRUE )
	{
	  cm_msg(MERROR,"get_params",
"error - there is already an ev_step block for this unit (%d) waiting for an end loop (block %s)",
	 ev_step_info.ev_step_block.awg_unit,name);
	  return DB_INVALID_PARAM;  
	}
     
      ev_step_params[my_index].loop_index = -1;  // initialize to negative value
      /* find the loop with this time-reference */
      for (i = gbl_num_loops_defined-1; i>=0;  i--)
	{
	  if (strcmp (loop_params[i].defines_startref_string,time_ref_params[my_refindex].this_reference_name)==0)
	    {
	      printf("found loop %s with ev_step time ref %s at loop index=%d (block %s)\n",
		     loop_params[i].loopname,time_ref_params[my_refindex].this_reference_name, i, name);
	      
	      ev_step_params[my_index].loop_index = i; // remember this loop index
	      break;
	    }
	} // end of for loop

      if( ev_step_params[my_index].loop_index  == -1)
	{
	  cm_msg(MERROR,"get_params","error - could not find a loop with time reference %s (block %s)",
		 time_ref_params[my_refindex].this_reference_name,name);
	  return DB_INVALID_PARAM;
	}
      /* we have found the loop surrounding the ev_step block */
    
     

      ev_step_open_flag[ ev_step_info.ev_step_block.awg_unit] = TRUE; // set a flag;

      ev_step_params[my_index].num_steps =  loop_params[i].loop_count ;

      if( ev_step_params[my_index].num_steps < 1)
	{
	  cm_msg(MERROR,"get_params",
		 "EVSTEP block must be in a loop whose loop count is >= 1 (loopname \"%s\"  block %s)",
		 loop_params[i].loopname,name);

	  printf("get_params: ERROR - EVSTEP block must be in a loop whose loop count is >= 1 (loopname \"%s\"  block %s)",
		 loop_params[i].loopname,name);

	  return DB_INVALID_PARAM;

	}

      if (loop_params[i].open_flag != 1)
	{
	  printf("get_params: info - loop %s is closed already; (block %s time reference %s)\n",
		 loop_params[i].loopname,name,time_ref_params[my_refindex].this_reference_name);
	  ev_step_params[my_index].step_delay_ms = loop_params[i].one_loop_duration_ms ;
	  
	}
      else
	{
	  printf("get_params: info - loop %s is open; cannot fill step_delay yet (block %s time reference %s)\n",
		 loop_params[i].loopname,name,time_ref_params[my_refindex].this_reference_name);
	  ev_sindex=my_index;  // remember the index number for end_loop
	  ev_step_params[my_index].step_delay_ms = -1; // initialize to -1 ... filled by end_loop
	}
      

      /* Now get the parameter values for AWG  

      Start Value Array
      */
      sprintf(param,"start value (Volts)");
      sprintf(ev_step_params[my_index].start_values_string," "); // string must start with blank (not "(" )
      size=sizeof(ev_step_params[my_index].start_values_string)-1;
    
      status = get_string(hKey, param, &ev_step_params[my_index].start_values_string[1], &size,
			  "",USE_DEFAULT,name);
      if(status != SUCCESS)
	return status;

      /*
	End Value Array
      */
      sprintf(param,"end value (Volts)");
      size=sizeof(ev_step_params[my_index].end_values_string);
      sprintf(ev_step_params[my_index].end_values_string," "); // string must start with blank (not "(" )
      status = get_string(hKey, param, &ev_step_params[my_index].end_values_string[1],&size,
			  "",USE_DEFAULT,name);
      if(status != SUCCESS)
	return status;
      
      /*
	NSteps before ramp starts 
      */
      sprintf(param,"Nsteps before ramp");
      size=sizeof(ev_step_params[my_index].nsteps_before_ramp_string);
      sprintf(ev_step_params[my_index].nsteps_before_ramp_string," "); // string must start with blank (not "(" )
      status = get_string(hKey, param, &ev_step_params[my_index].nsteps_before_ramp_string[1], &size,"",USE_DEFAULT,name);
      if(status != SUCCESS)
	return status;

      /*
	N Steps after ramp ends
      */
      sprintf(param,"Nsteps after ramp");
      size=sizeof(ev_step_params[my_index].nsteps_after_ramp_string);
      sprintf(ev_step_params[my_index].nsteps_after_ramp_string," "); //  string must start with blank (not "(" )
      status = get_string(hKey, param, &ev_step_params[my_index].nsteps_after_ramp_string[1], &size,"",USE_DEFAULT,name);
      if(status != SUCCESS)
	return status;

     // Update parameter block
     ev_step_params[my_index].block_name[0]='\0';
     strncat(ev_step_params[my_index].block_name,name, BLOCKNAME_LEN);
     ev_step_params[my_index].time_ref_index = my_refindex;
     ev_step_params[my_index].time_ms =  ev_step_info.ev_step_block.time_offset__ms_;
     ev_step_params[my_index].start_t0_offset_ms= ev_step_params[my_index].time_ms
       + time_ref_params[my_refindex].t0_offset_ms;
     ev_step_params[my_index].awg_unit = ev_step_info.ev_step_block.awg_unit;

     print_ev_step_params(my_index, FALSE,stdout); // don't print the step values
     
     my_ref = ev_step_params[my_index].time_ref_index;
     my_offset=  ev_step_params[my_index].time_ms;
     
     /* add this to TRANSITIONS RECORD */
     strcpy( ptrans[gbl_num_transitions].block_name, ev_step_params[my_index].block_name);
     
     ptrans[gbl_num_transitions].time_reference_index = my_ref;    
     ptrans[gbl_num_transitions].code  = EV_STEP_CODE;
     ptrans[gbl_num_transitions].block_index    = block_index;
     ptrans[gbl_num_transitions].t0_offset_ms  = 
       time_ref_params[my_ref].t0_offset_ms + my_offset;
     ptrans[gbl_num_transitions].bitpattern = 0x40000000; // use a delay bitpattern (no change)
     ptrans[gbl_num_transitions].loop_index = my_index; // use this to point to index in ev_step_params block
     
     gbl_num_transitions++;
     if(gbl_num_transitions > MAX_TRANSITIONS )
       {
	 cm_msg(MERROR,"get_params","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
	 printf("get_params: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
	 return DB_INVALID_PARAM;
       }      
     
     if( add_awg_pulses( ev_step_params[my_index].awg_unit, my_refindex,  
			 ev_step_params[my_index].time_ms, name) != SUCCESS)
       return DB_INVALID_PARAM;
   
     print_transitions(ptrans,stdout);

	
    }
#endif // NO_AWG

  else if(strncmp(name,"BEGIN_LOOP",5)==0) 
    { 
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(begin_loop_info.begin_loop_block);
      if(size == total_size)  // structure has expected size
	{
	  status= db_get_record(hDB, hKey, &begin_loop_info.begin_loop_block, &size, 0);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
      else
      {*/
	  // see what parameters we do have
	  if(total_size == 0)
	    {
	      cm_msg(MERROR,"get_params","Loop count must be supplied at block %s",name);
	      return FE_ERR_ODB;
	    }
	  size=sizeof(  begin_loop_info.begin_loop_block.time_reference);
	  status = get_string(hKey, "time reference", 
			      begin_loop_info.begin_loop_block.time_reference,&size,
			      "",USE_DEFAULT,name); // default value found later
	  if(status != SUCCESS)return status;

	  status = get_int(hKey, "loop count", 
			     &begin_loop_info.begin_loop_block.loop_count,
			     &default_loop_count, UPDATE_DEFAULT, name);
	  if(status != SUCCESS)return status;
	  
	  status = get_double(hKey, "time offset (ms)", 
			      &begin_loop_info.begin_loop_block.time_offset__ms_, 
			      &fzero,USE_DEFAULT,name); // default will be found later
	  if(status != SUCCESS)return status;
	  
	  //}
      
	  if(debug)
	{
	  printf("get_params: from BEGIN_LOOP_BLOCK, my_index=%d,  loop counter: %d\n",
		 my_index, 
		 begin_loop_info.begin_loop_block.loop_count);
	  printf("            time offset (ms) = %f,  time reference: %s \n",
		 begin_loop_info.begin_loop_block.time_offset__ms_, 
		 begin_loop_info.begin_loop_block.time_reference);
	}
      
      if( begin_loop_info.begin_loop_block.loop_count < 1)
	{
	  cm_msg(MINFO,"get_params","loop count (%d) must be at least 1 (block %s). Using loop count = 1",
		 begin_loop_info.begin_loop_block.loop_count,name);
	  printf("get_params: loop count (%d) must be at least 1 (block %s). Using loop count = 1\n",
		 begin_loop_info.begin_loop_block.loop_count,name);
	  begin_loop_info.begin_loop_block.loop_count =1;
	}
      
      status =  get_loop_name( name, pbegin, loop_params[my_index].loopname); // loopname comes from block name
      if(debug)printf("get_params: loop_params[%d].loopname=%s\n",my_index, loop_params[my_index].loopname);
      if(status != SUCCESS)
	return status;

      /* loop names must be unique; my_index counts begin_loop blocks */
      for (i=0; i<my_index; i++)
	{
	  if(strcmp( loop_params[i].loopname, loop_params[my_index].loopname)==0)
	    {
	      cm_msg(MERROR,"get_params","Two loops have the same name (\"%s\"). Loop names must be unique (block %s)",
		     loop_params[my_index].loopname,name);
	      return DB_INVALID_PARAM;
	    }
	}
      /* check my time_ref is valid .. or if not supplied, default will be used ->my_refindex */
      if(get_this_ref_index( begin_loop_info.begin_loop_block.time_reference, name,  &my_refindex)!=DB_SUCCESS)
	return  DB_INVALID_PARAM;

    
      sprintf(string, "_TBEG%s", loop_params[my_index].loopname ); // name of time reference
      /* check whether time reference string to be defined is unique, and return the next available timeref_number  */
     if(get_next_ref_index( string,  name, &define_refindex)!= SUCCESS)
	return DB_INVALID_PARAM;
      /* get_next_ref fills  
	 time_ref_params[define_refindex].defines_timeref_index and  
	 time_ref_params[define_refindex].this_reference_name
	 now fill the other parameters by calling add_time_ref
      */
      printf("calling add_time_ref with params define_refindex=%d, my_refindex=%d\n",
	     define_refindex, my_refindex);    
      if(add_time_ref(define_refindex, my_refindex, timeref_code_begin_loop,  
		   begin_loop_info.begin_loop_block.time_offset__ms_, 
		   block_index, name) != SUCCESS)
      return DB_INVALID_PARAM;
      
      if (debug)
	printf("defined loop_start reference = %s (time_ref index %d) using reference = %s (t0 offset=%fms)\n",
	       time_ref_params[define_refindex].this_reference_name,
	       define_refindex,
	       time_ref_params[my_refindex].this_reference_name,
	       time_ref_params[define_refindex].t0_offset_ms);

      /* Make sure we do not have a begin_loop at T0. This does not work (begin_loop instruction disappears)*/
      if (compare(time_ref_params[define_refindex].t0_offset_ms,  min_delay) < 0)
	{
	  cm_msg(MERROR,"get_params",
		 " begin loop not allowed at T0; must have at least minimal delay of %fms (loop %s, blockname %s) ",  
		 min_delay, loop_params[my_index].loopname,name);
	  return DB_INVALID_PARAM;
	}
      

      gbl_num_loops_defined++; /* total number of loops */
      if(gbl_num_loops_defined > MAX_LOOP_BLOCKS)
	{
	  cm_msg(MERROR,"get_params","too many loop blocks (maximum=%d) (block %s)\n",
		 MAX_LOOP_BLOCKS ,name	 );
	  return DB_INVALID_PARAM;
	}
      strcpy(loop_params[my_index].defines_startref_string,
	     time_ref_params[define_refindex].this_reference_name);      
      loop_params[my_index].my_start_ref_index =my_refindex;
      loop_params[my_index].defines_start_ref_index =define_refindex;
      loop_params[my_index].start_block_name[0]='\0';
      strncat(loop_params[my_index].start_block_name,name, BLOCKNAME_LEN);
      loop_params[my_index].start_time_ms = begin_loop_info.begin_loop_block.time_offset__ms_;
      loop_params[my_index].loop_count =  begin_loop_info.begin_loop_block.loop_count;
      loop_params[my_index].open_flag = TRUE;
      // as a precaution, clear parameters to be filled by end_loop
      loop_params[my_index].end_block_name[0]='\0'; // clear this (to be filled by end loop block)
      loop_params[my_index].defines_endref_string[0]='\0';
      loop_params[my_index].one_loop_t0_offset_ms=loop_params[my_index].one_loop_duration_ms=
	loop_params[my_index].all_loops_duration_ms=	loop_params[my_index].all_loops_t0_offset_ms=
	loop_params[my_index].my_end_ref_index=0;
      

      gbl_open_loop_counter++; // count the open loops
      if(gbl_open_loop_counter > 16)
	{
	  cm_msg(MERROR,"get_params","maximum number of nested loops allowed by PPG is 16");
	  return DB_INVALID_PARAM;
	}

      if(debug)
	{
	  printf("\nbegin_loop blocks:  %d begin loops  so far \n", block_counter[BEGIN_LOOP]);
	  for (i=0; i < block_counter[BEGIN_LOOP]; i++)
	    printf("begin_loop block %d  begin loop name = %s   loop_counter =%d \n",
		   i, loop_params[i].loopname ,  loop_params[i].loop_count ); 
	  
	}
      
      
      /* add this to TRANSITIONS RECORD */
      strcpy( ptrans[gbl_num_transitions].block_name, loop_params[my_index].start_block_name);
      
      ptrans[gbl_num_transitions].code    = BEGLOOP_CODE;
      ptrans[gbl_num_transitions].block_index    = block_index;
      ptrans[gbl_num_transitions].loop_index = my_index; // remember which loop this is
      loop_params[my_index].start_t0_offset_ms = loop_params[my_index].start_time_ms
	+ time_ref_params[my_refindex].t0_offset_ms;
      if(  loop_params[my_index].start_t0_offset_ms != time_ref_params[define_refindex].t0_offset_ms)
	{	/* check */
	  printf("expect loop_params[%d].start_t0_offset_ms(%f) = time_ref_params[%d].t0_offset_ms(%f)\n",
		 my_index,        loop_params[my_index].start_t0_offset_ms, 
		 define_refindex, time_ref_params[define_refindex].t0_offset_ms);
	  return DB_INVALID_PARAM;
	}
      ptrans[gbl_num_transitions].t0_offset_ms =  loop_params[my_index].start_t0_offset_ms;
      ptrans[gbl_num_transitions].time_reference_index = 
       loop_params[my_index].my_start_ref_index;
      gbl_num_transitions++;
      if(gbl_num_transitions > MAX_TRANSITIONS )
	{
	  cm_msg(MERROR,"get_params","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
	  printf("get_params: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
	  return DB_INVALID_PARAM;
	}


      if(debug)
	{
	  print_loop_params(stdout);
	  print_transitions(ptrans,stdout);
	}
    }
  
  
  else if(strncmp(name,"END_LOOP",3)==0) 
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(end_loop_info.end_loop_block);
      if(total_size == 0)
	{
	  status= db_get_record(hDB, hKey, &end_loop_info.end_loop_block, &size, 0);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)",
		     key->name,status);
	      return FE_ERR_ODB;
	    }
	}
        else
	{*/
	  // see what parameters we do have
      if(total_size == 0)
	{
	  sprintf(end_loop_info.end_loop_block.time_reference,"");
	  end_loop_info.end_loop_block.time_offset__ms_=0;
	}
      else
	{ 
	  size=sizeof( end_loop_info.end_loop_block.time_reference);
	  status = get_string(hKey, "time reference", 
			      end_loop_info.end_loop_block.time_reference,&size,
			      "",USE_DEFAULT,name); // default value found later
	  if(status != SUCCESS)return status;
	  
	  status = get_double(hKey, "time offset (ms)", 
			      &end_loop_info.end_loop_block.time_offset__ms_, 
			      &fzero,USE_DEFAULT,name); // default will be found later
	  if(status != SUCCESS)return status;
	  
	}
      //}
      
      
      if(debug)
	printf("get_params: from END_LOOP_BLOCK, blockname=%s, number of open loops=%d time ref=%s offset=%fms\n",
	       name, gbl_open_loop_counter,
	       end_loop_info.end_loop_block.time_reference,
	       end_loop_info.end_loop_block.time_offset__ms_);
    
      gotit=FALSE;
      status = get_loop_name( name, pend, my_loopname);
      if(status != SUCCESS)
	return status;

      /* this end loop block should be closing the last open begin loop block (no overlapping loops!) */
      status = get_last_open_loop(&lindex,my_loopname,name);
      if(status != DB_SUCCESS)
	return status;

      if (debug)printf("get_params: expect to close last open loop, i.e. at loop_params lindex = %d\n",
	     lindex);

      /* make sure the loopname match */
      if(debug)printf("this end loop name: %s   loop_params[%d].loopname=%s\n",my_loopname,lindex, 
	     loop_params[lindex].loopname);
      if(strcmp(my_loopname, loop_params[lindex].loopname )== 0)
	{ 
	  if(debug)printf("get_params: found begin loop with matching name \"%s\" at loop index %d\n",
			  my_loopname,lindex);
	} 
      else
	{
	  printf("Expecting to end loop %s rather than loop %s\n",
		 my_loopname, loop_params[lindex].loopname );
	  cm_msg(MERROR,"get_params","overlapping loops are not allowed; end loop %s first",
		 my_loopname);
	  return DB_INVALID_PARAM;
	}

    
      if(debug)printf("closing loop %d loopname %s \n",lindex,my_loopname);
      loop_params[lindex].open_flag = FALSE;
      gbl_open_loop_counter--; // counts the open loops
      if(debug)printf("Now there are %d open loops\n",gbl_open_loop_counter);
  
  
      /* check that our time reference exists ... or use default if empty string */   
      if(get_this_ref_index( end_loop_info.end_loop_block.time_reference, name,  &my_refindex)!=DB_SUCCESS)
	return  DB_INVALID_PARAM;
   

      /* can no longer check that this end loop is referenced on the reference defined by the begin_loop ...
            there may be other loops or variable-length pulses in between
      */
      
      loop_params[lindex].end_block_name[0]='\0';
      strncat(loop_params[lindex].end_block_name,name, BLOCKNAME_LEN);
     
      /* add this to TRANSITIONS RECORD */
      ptrans[gbl_num_transitions].time_reference_index = my_refindex;	     
      strcpy( ptrans[gbl_num_transitions].block_name, loop_params[lindex].end_block_name);
      ptrans[gbl_num_transitions].code    = ENDLOOP_CODE;
      ptrans[gbl_num_transitions].loop_index = lindex;
      ptrans[gbl_num_transitions].block_index    = block_index;

     
      loop_params[lindex].one_loop_t0_offset_ms = end_loop_info.end_loop_block.time_offset__ms_
	+ time_ref_params[my_refindex].t0_offset_ms;

      /* now calculate the time ONE loop is going to take  */
      ftmp = ( loop_params[lindex].one_loop_t0_offset_ms - loop_params[lindex].start_t0_offset_ms);
      if( ftmp <= 0)
	{
	  cm_msg(MERROR,"get_params","loop \"%s\" cannot end before it starts",my_loopname);
	  printf("get_params: Error - loop \"%s\" starts at %fms from t0, but ends before this at %fms\n",
		 my_loopname,loop_params[lindex].start_t0_offset_ms, loop_params[lindex].one_loop_t0_offset_ms);
	  return DB_INVALID_PARAM;
	}
      loop_params[lindex].one_loop_duration_ms =  ftmp;


      ptrans[gbl_num_transitions].t0_offset_ms =  loop_params[lindex].one_loop_t0_offset_ms;
      
      gbl_num_transitions++;
      if(gbl_num_transitions +1 > MAX_TRANSITIONS ) // about to add another transition as well
	{
	  cm_msg(MERROR,"get_params","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
	  printf("get_params: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
	  return DB_INVALID_PARAM;
	}   
      
      
#ifndef NO_AWG      
      my_unit =  ev_step_params[ev_sindex].awg_unit; // temporary variable my_unit
      printf(" unit=%d ev_step_open_flag[%d]=%d lindex=%d  ev_step_params[ev_sindex].loop_index=%d\n",
	     my_unit,  my_unit,  ev_step_open_flag[my_unit],
	     lindex,   ev_step_params[ev_sindex].loop_index);
      
      
      if ( ev_step_open_flag[my_unit])
	{
	  // this loop should contain an ev_step block
	  if(lindex != ev_step_params[ev_sindex].loop_index)
	    {
	      cm_msg(MINFO,"get_params",
		     "expect evstep (unit %d) to be in current open loop (lindex=%d); block may be out-of-order %s",
		     my_unit, lindex,name);
	    }
	  else
	    {
	      printf("one_loop_duration[%d] = %f\n", lindex,loop_params[lindex].one_loop_duration_ms);
	      ev_step_params[ev_sindex].step_delay_ms = loop_params[lindex].one_loop_duration_ms ;
	      printf("get_params: this loop (block %s) closes ev_step block %s (awg unit %d)\n",
		     name, ev_step_params[ev_sindex].block_name,my_unit );
	      print_ev_step_params(ev_sindex,FALSE,stdout);// don't print the step values
	      ev_step_open_flag[my_unit]=FALSE; // clear this for next time      	  
	    }
	}
      
#endif // NO_AWG 
	   
      if(debug)
        print_loop_params(stdout);

      /* now calculate the total time this loop is going to take & add a reserved block */
      ftmp = ( loop_params[lindex].one_loop_t0_offset_ms - loop_params[lindex].start_t0_offset_ms)
	* (double)loop_params[lindex].loop_count ; // loop duration (all loops)
     

      if (loop_params[lindex].loop_count== 1)
	ftmp +=  min_delay; /* add minimal delay (reserved block =  1 minimal delay) */
      loop_params[lindex].all_loops_duration_ms = ftmp; 
      loop_params[lindex].all_loops_t0_offset_ms = ftmp + loop_params[lindex].start_t0_offset_ms;
     
     	  
      sprintf( ptrans[gbl_num_transitions].block_name,"RES_%s",loop_params[lindex].loopname);
      ptrans[gbl_num_transitions].code    = RES_CODE;
      ptrans[gbl_num_transitions].block_index = block_index; // goes with END LOOP
      ptrans[gbl_num_transitions].loop_index = lindex;
      ptrans[gbl_num_transitions].t0_offset_ms = loop_params[lindex].all_loops_t0_offset_ms;

      /* time reference to end of ONE loop ... for DEBUGGING ONLY */
      /* fill this before calling add_time_ref() */
      sprintf(string, "_TEND%s_ONE",  loop_params[lindex].loopname ); // name of time reference
      /* check whether time reference string to be defined is unique, 
	 and return the next available timeref_number  */
      if(get_next_ref_index( string,  name, &define_refindex)!= DB_SUCCESS)
	return DB_INVALID_PARAM;
      /* get_next_ref fills  
	 time_ref_params[define_refindex].defines_timeref_index and  
	 time_ref_params[define_refindex].this_reference_name
	 now fill the other parameters by calling add_time_ref
      */
      // offset from T0
      if(add_time_ref(define_refindex, 0, timeref_code_end_oneloop,  
		   loop_params[lindex].one_loop_t0_offset_ms,
		      block_index, name)!= SUCCESS)
	return DB_INVALID_PARAM;



      /* time reference to end of ALL loops */

      /* fill this before calling add_time_ref() */
      sprintf(string, "_TEND%s",  loop_params[lindex].loopname); // name of time reference
      /* check whether time reference string to be defined is unique, 
	 and return the next available timeref_number  */
      if(get_next_ref_index( string,  name, &define_refindex)!= DB_SUCCESS)
	return DB_INVALID_PARAM;
      /* get_next_ref fills  
	 time_ref_params[define_refindex].defines_timeref_index and  
	 time_ref_params[define_refindex].this_reference_name
	 now fill the other parameters by calling add_time_ref
      */
      // from T0
      if(add_time_ref(define_refindex, 0, timeref_code_end_loops,  
		   loop_params[lindex].all_loops_t0_offset_ms, 
		      block_index, name)!= SUCCESS)
	return DB_INVALID_PARAM;

      strcpy(loop_params[lindex].defines_endref_string,
	     time_ref_params[define_refindex].this_reference_name);      
      loop_params[lindex].my_end_ref_index = my_refindex;
      
      ptrans[gbl_num_transitions].time_reference_index = loop_params[lindex].my_end_ref_index; 
      gbl_num_transitions++;


      if(debug)
	{
	  printf("get_params: end loop block is: %s\n",name);
	  printf("        corresponding begin loop name is %s, loop count %d\n", 
		 loop_params[lindex].loopname,
		 loop_params[lindex].loop_count);
	  printf("  loop began at %f ms from T0; first loop ends at %f ms; last loop ends at %fms \n",
		 loop_params[lindex].start_t0_offset_ms, 
		 loop_params[lindex].one_loop_t0_offset_ms,
		 loop_params[lindex].all_loops_t0_offset_ms);

	  print_transitions(ptrans,stdout);
	}

      /* start or end cannot be within reserved blocks of already defined loops */
      if (check_loops(lindex) != SUCCESS)
	return DB_INVALID_PARAM;

  
    }
  


  /* NOTE: all types of RF sweep blocks are grouped together ( RF_SWEEP block type)  */
  else if (strncmp(name,"RF_SWEEP",6)==0) 
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(rf_sweep_info.rf_sweep_block);
      status= db_get_record(hDB, hKey, &rf_sweep_info.rf_sweep_block, &size, 0);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)",
		 key->name,status);
	  return FE_ERR_ODB;
	  }*/
      size=sizeof( rf_sweep_info.rf_sweep_block.generator);
      status = get_string(hKey, "generator",
			  rf_sweep_info.rf_sweep_block.generator,&size,
			  default_rf_generator, UPDATE_DEFAULT, name);
	      if(status != SUCCESS)return status;

      status = get_dword(hKey, "start frequency (kHz)", 
			 &rf_sweep_info.rf_sweep_block.start_frequency__khz_, 
			 &default_start_freq_khz, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "stop frequency (kHz)", 
			 &rf_sweep_info.rf_sweep_block.stop_frequency__khz_, 
			 &default_stop_freq_khz,UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "duration (ms)", 
			 &rf_sweep_info.rf_sweep_block.duration__ms_, 
			 &default_duration_ms,UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "amplitude (mv)", 
			 &rf_sweep_info.rf_sweep_block.amplitude__mv_, 
			 &default_amplitude_mv,UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;


      if(debug)
	printf("get_params: my_index=%d, from RF_SWEEP_BLOCK, start freq = %d kHz\n",
	       my_index, rf_sweep_info.rf_sweep_block.start_frequency__khz_);
      /*  
              sweep block params are counted in common to save duplication
	      of parameters ; unused params are set to 0;
      
	      NOTE - check whether we want to use defaults for RF values... if so, shouldn't zero them
      */
      zero_rfsweep_params(my_index);  // zero all parameters for this index

     // parameters  frequency start and stop, amplitude, duration
      rf_sweep_params[my_index].block_name[0]='\0';
      strncpy(rf_sweep_params[my_index].block_name,name, BLOCKNAME_LEN);
      
      for(j=0; j<strlen(rf_sweep_info.rf_sweep_block.generator); j++)	    
	rf_sweep_params[my_index].generator[j] = toupper(rf_sweep_info.rf_sweep_block.generator[j]);
      rf_sweep_params[my_index].generator[j+1]='\0';	  

      rf_sweep_params[my_index].sweep_type = 1;
      rf_sweep_params[my_index].start_freq_khz =  rf_sweep_info.rf_sweep_block.start_frequency__khz_ ;
      rf_sweep_params[my_index].stop_freq_khz =  rf_sweep_info.rf_sweep_block.stop_frequency__khz_ ;
      rf_sweep_params[my_index].amplitude_mv =  rf_sweep_info.rf_sweep_block.amplitude__mv_;      
      rf_sweep_params[my_index].duration_ms =  rf_sweep_info.rf_sweep_block.duration__ms_;
      

      if(debug)
	{
	  printf("\nrf_sweep block: block %d  my_index=%d\n", block_counter[RF_SWEEP],my_index);
	  print_rfsweep_params(name, my_index);
	}

      printf("later... do something with these parameters i.e. load rf_generator \n");

    }
  
  else if (strncmp(name,"RF_FM_SWEEP",5)==0) 
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(rf_fm_sweep_info.rf_fm_sweep_block);
      status= db_get_record(hDB, hKey, &rf_fm_sweep_info.rf_fm_sweep_block, &size, 0);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)",
		 key->name,status);
	  return FE_ERR_ODB;
	  }*/

      size=sizeof( rf_fm_sweep_info.rf_fm_sweep_block.generator);
      status = get_string(hKey, "generator",
			  rf_fm_sweep_info.rf_fm_sweep_block.generator,&size, 
			  default_rf_generator,UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "start frequency (kHz)",
			 &rf_fm_sweep_info.rf_fm_sweep_block.start_frequency__khz_,
			 &default_start_freq_khz, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "stop frequency (kHz)", 
			 &rf_fm_sweep_info.rf_fm_sweep_block.stop_frequency__khz_,
			 &default_stop_freq_khz, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;


      status = get_dword(hKey, "amplitude (mv)", 
			 &rf_fm_sweep_info.rf_fm_sweep_block.amplitude__mv_,
			 &default_amplitude_mv, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;


      if(debug)
	printf("get_params: from RF_FM_SWEEP_BLOCK, start freq = %d kHz\n",
	       rf_fm_sweep_info.rf_fm_sweep_block.start_frequency__khz_);
      
    /*  
              sweep block params are counted in common to save duplication
	      of parameters ; unused params are set to 0;
      */
      zero_rfsweep_params(my_index);   
      
      // parameters  frequency start and stop, amplitude 
      rf_sweep_params[my_index].block_name[0]='\0';
      strncat(rf_sweep_params[my_index].block_name,name, BLOCKNAME_LEN);
       rf_sweep_params[my_index].sweep_type=2;
       rf_sweep_params[my_index].start_freq_khz =  rf_fm_sweep_info.rf_fm_sweep_block.start_frequency__khz_ ;
       rf_sweep_params[my_index].stop_freq_khz =  rf_fm_sweep_info.rf_fm_sweep_block.stop_frequency__khz_ ;
       rf_sweep_params[my_index].amplitude_mv =  rf_fm_sweep_info.rf_fm_sweep_block.amplitude__mv_;
      // duration_ms will come from cycle duration TBD)

       for(j=0; j<strlen(rf_fm_sweep_info.rf_fm_sweep_block.generator); j++)	    
	 rf_sweep_params[my_index].generator[j] = toupper(rf_fm_sweep_info.rf_fm_sweep_block.generator[j]);
       rf_sweep_params[my_index].generator[j+1]='\0';	  

      
      if(debug)
	{
	  printf("\nrf_fm_sweep block: block %d my_index=%d\n", block_counter[RF_SWEEP],my_index);
	  print_rfsweep_params(name, my_index);
	}

      printf("later... do something with these parameters i.e. load rf_generator \n");
    }
  
  
  else if (strncmp(name,"RF_BURST",5)==0) 
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params)  
      size = sizeof(rf_burst_info.rf_burst_block);
      status= db_get_record(hDB, hKey, &rf_burst_info.rf_burst_block, &size, 0);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)",
		 key->name,status);
	  return FE_ERR_ODB;
	}
   */
      size=sizeof( rf_burst_info.rf_burst_block.generator);
      status = get_string(hKey, "generator",
			  rf_burst_info.rf_burst_block.generator,&size,
			  default_rf_generator, UPDATE_DEFAULT, name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "frequency (kHz)",
			 &rf_burst_info.rf_burst_block.frequency__khz_,
			 &default_single_freq_khz, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "number of cycles", 
			 &rf_burst_info.rf_burst_block.number_of_cycles,
			 &default_num_cycles, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;


      status = get_dword(hKey, "amplitude (mv)", 
			 &rf_burst_info.rf_burst_block.amplitude__mv_, 
			 &default_amplitude_mv, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;


        if(debug)
	printf("get_params: from RF_BURST_BLOCK, start freq = %d kHz\n",
	       rf_burst_info.rf_burst_block.frequency__khz_);
      
     /*  
              sweep block params are counted in common to save duplication
	      of parameters ; unused params are set to 0;
      */
      zero_rfsweep_params(my_index); 
         
      // parameters  frequency, amplitude, number of cycles
      rf_sweep_params[my_index].block_name[0]='\0';
      strncat(rf_sweep_params[my_index].block_name,name, BLOCKNAME_LEN);
       rf_sweep_params[my_index].sweep_type=3;
       rf_sweep_params[my_index].start_freq_khz =  rf_burst_info.rf_burst_block.frequency__khz_ ;      
       rf_sweep_params[my_index].number =  rf_burst_info.rf_burst_block.number_of_cycles;
       rf_sweep_params[my_index].amplitude_mv =  rf_burst_info.rf_burst_block.amplitude__mv_;

       for(j=0; j<strlen(rf_burst_info.rf_burst_block.generator); j++)	    
	 rf_sweep_params[my_index].generator[j] = toupper(rf_burst_info.rf_burst_block.generator[j]);
       rf_sweep_params[my_index].generator[j+1]='\0';	  
       

      
        if(debug)
	{
	  printf("\nrf_burst block: block %d my_index=%d\n", block_counter[RF_SWEEP],my_index);
	  print_rfsweep_params(name, my_index);
	}
	printf("later... do something with these parameters i.e. load rf_generator \n");
	 
    }
  
  else if (strncmp(name,"RF_SWIFT_AWG",6)==0) 
    {
   /* can no longer use create rec, get rec (user wants defaults for absent params) 
      size = sizeof(rf_swift_awg_info.rf_swift_awg_block);
      status= db_get_record(hDB, hKey, &rf_swift_awg_info.rf_swift_awg_block, &size, 0);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_params","error getting record for \"%s\" (%d)",
		 key->name,status);
	  return FE_ERR_ODB;
	  }*/
      size=sizeof( rf_swift_awg_info.rf_swift_awg_block.generator);
      status = get_string(hKey, "generator",
			  rf_swift_awg_info.rf_swift_awg_block.generator,&size, 
			  default_rf_generator, UPDATE_DEFAULT, name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "start frequency interval (kHz)",
			 &rf_swift_awg_info.rf_swift_awg_block.start_frequency_interval__khz_, 
			  &default_start_freq_interval_khz, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;
      status = get_dword(hKey, "stop frequency interval (kHz)",
			 &rf_swift_awg_info.rf_swift_awg_block.stop_frequency_interval__khz_,
			 &default_stop_freq_interval_khz, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;
      status = get_dword(hKey, "notch frequency (kHz)",
			 &rf_swift_awg_info.rf_swift_awg_block.notch_frequency__khz_, 
			 &default_notch_freq_khz, UPDATE_DEFAULT,name);

      if(status != SUCCESS)return status;

      status = get_dword(hKey, "number of frequencies", 
			 &rf_swift_awg_info.rf_swift_awg_block.number_of_frequencies,
			 &default_num_frequencies, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

      status = get_dword(hKey, "number of bursts", 
			 &rf_swift_awg_info.rf_swift_awg_block.number_of_bursts,
			 &default_num_bursts, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;


      status = get_dword(hKey, "amplitude (mv)", 
			 &rf_swift_awg_info.rf_swift_awg_block.amplitude__mv_, 
			 &default_amplitude_mv, UPDATE_DEFAULT,name);
      if(status != SUCCESS)return status;

        if(debug)
	printf("get_params: from RF_SWIFT_AWG_BLOCK, start freq = %d kHz\n",
	       rf_swift_awg_info.rf_swift_awg_block.start_frequency_interval__khz_);
      
        /*  
              sweep block params are counted in common to save duplication
	      of parameters ; unused params are set to 0;
      */
      zero_rfsweep_params(my_index);
      
      /* parameters amplitude, start frequency interval, stop freq interval,
	 notch freq, number of bursts
      */
      rf_sweep_params[my_index].block_name[0]='\0';
      strncat(rf_sweep_params[my_index].block_name,name, BLOCKNAME_LEN);
       rf_sweep_params[my_index].sweep_type=4;
       rf_sweep_params[my_index].start_freq_khz =  rf_swift_awg_info.rf_swift_awg_block.start_frequency_interval__khz_ ;
       rf_sweep_params[my_index].stop_freq_khz =  rf_swift_awg_info.rf_swift_awg_block.stop_frequency_interval__khz_ ;
       rf_sweep_params[my_index].freq_hz =  rf_swift_awg_info.rf_swift_awg_block.notch_frequency__khz_;
       rf_sweep_params[my_index].number =  rf_swift_awg_info.rf_swift_awg_block.number_of_bursts;
       rf_sweep_params[my_index].frequencies = rf_swift_awg_info.rf_swift_awg_block.number_of_frequencies; 
       rf_sweep_params[my_index].amplitude_mv =  rf_swift_awg_info.rf_swift_awg_block.amplitude__mv_;
     
       for(j=0; j<strlen(rf_swift_awg_info.rf_swift_awg_block.generator); j++)	    
	 rf_sweep_params[my_index].generator[j] = toupper(rf_swift_awg_info.rf_swift_awg_block.generator[j]);
       rf_sweep_params[my_index].generator[j+1]='\0';	  

       if(debug)
	 {
	   printf("\nrf_swift_awg block: block %d my_index=%d\n", block_counter[RF_SWEEP],my_index);
	   print_rfsweep_params(name, my_index);
	 } 
       printf("later... do something with these parameters i.e. load rf_generator \n");

    }  
  else
    {
      cm_msg(MERROR,"get_params","Unknown block %s",name);
      return DB_INVALID_PARAM;
    }
  return DB_SUCCESS;
}


    



/* initialize all rf_sweep params to zero (shared parameters) */
void  zero_rfsweep_params(INT i)
{
  if(debug)printf("zeroing rfsweep parameters at my_index=%d\n",i);
  rf_sweep_params[i].sweep_type =
   rf_sweep_params[i].start_freq_khz =
 rf_sweep_params[i].stop_freq_khz =
 rf_sweep_params[i].freq_hz =
 rf_sweep_params[i].amplitude_mv =
 rf_sweep_params[i].duration_ms =
 rf_sweep_params[i].number =
    rf_sweep_params[i].frequencies = 0;

  sprintf(rf_sweep_params[i].generator,"");

  return;
}


/*----------------------------------------------------------------*/
INT check_ppg_name(char *ppg_name)
{
  /* if a valid signal name, returns output bit for this signal
     if not, returns -1
  */

  INT len,i,j;
  char my_name[15];
  len = strlen(ppg_name);
  if(len>14)len=14;

  for(j=0; j<len; j++)
    my_name[j] = toupper(ppg_name[j]);   
  my_name[len]='\0';

  
  for (j=0; j<NUM_BITS; j++)
    {
      if(strncmp(my_name,ppg_signal_names[j],len)==0)
	break;
    }
  if(j<NUM_BITS)
    {
      if(debug)
	printf("check_ppg_name: found PPG signal name %s at index %d\n",my_name,j);
    }
  else
    {
      printf("check_ppg_name: could not find PPG signal name %s \n",my_name);
      printf("                PPG signal name should be one of: \n");
      for(j=0; j<NUM_BITS; j++)
	printf("%d %s ",j,ppg_signal_names[j]);
      printf("\n");
      j= -1;
    }

  return j;
}



/*---------------------------------------------------------*/


INT add_transition(INT block_index, INT my_index, INT refindex,  double my_offset)
{
  INT j,len;
  INT my_ref;
  
  if(debug)
    printf("add_transition called with parameters block_index=%d,my_index=%d,refindex=%d,my_offset=%f\n", 
	   block_index, my_index, refindex, my_offset);
  

  //  printf("pulse_params[%d].block_name = %s\n",my_index, pulse_params[my_index].block_name );
  strcpy( ptrans[gbl_num_transitions].block_name, pulse_params[my_index].block_name);
  // printf(" ptrans[%d].block_name = %s\n",gbl_num_transitions,  ptrans[gbl_num_transitions].block_name );

    ptrans[gbl_num_transitions].time_reference_index =   
      pulse_params[my_index].time_ref_index;  
  ptrans[gbl_num_transitions].code  = TRANS_CODE; // this is a transition 
  ptrans[gbl_num_transitions].block_index    = block_index;
  ptrans[gbl_num_transitions].t0_offset_ms  = 
    time_ref_params[refindex].t0_offset_ms + my_offset;
  ptrans[gbl_num_transitions].bit =  pulse_params[my_index].ppg_signal_num;
  ptrans[gbl_num_transitions].bitpattern =  1<<  pulse_params[my_index].ppg_signal_num;
  gbl_num_transitions++;
  if(gbl_num_transitions > MAX_TRANSITIONS )
    {
      cm_msg(MERROR,"get_params","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
      printf("get_params: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
      return DB_INVALID_PARAM;
    }
  if(debug)print_transitions(ptrans,stdout);
  return DB_SUCCESS;
}


INT  get_next_ref_index(char *string, char *name,  INT *index)
{
  /* get the next available time reference
        checks that string has not already been defined as a time reference

	returns SUCCESS, DB_INVALID_PARAM or DB_KEY_EXIST if timeref already exists
  */
  INT j,len;
  char my_string[20];

  if(gbl_num_time_references_defined+1 >= MAX_TIME_REFS)
    {	
      cm_msg(MERROR,"get_next_ref_index","Too many time references defined. Increase maximum from %d (block %s)\n",
	     MAX_TIME_REFS,    name);
      return DB_INVALID_PARAM ;
    }
 

  len = strlen(string);
  if(len <=0)
    {
       cm_msg(MERROR," get_next_ref_index","Name of time reference to be defined (block %s) is blank\n",
	     name);
      return DB_INVALID_PARAM ; 
    }

  for (j=0; j<len; j++)
    // my_string[j] = toupper(*index[j]);
    my_string[j]=toupper(string[j]);
  my_string[len]='\0';

  /* make sure user is not trying to redefine T0 */
  if  (strcmp(my_string,"T0")==0)
    {	
      cm_msg(MERROR," get_next_ref_index","Cannot redefine reserved time reference \"T0\" (block %s)\n",
	     name);
      return DB_INVALID_PARAM ;
    }


  for(j=0; j< gbl_num_time_references_defined ; j++ )
    {
      if(strcmp(time_ref_params[j].this_reference_name, my_string)==0)
	{
	  cm_msg(MINFO,"get_next_ref_index","reference string \"%s\" has already been defined (block %s)",
		 string, name);
	  return DB_INVALID_PARAM;
	  // return DB_KEY_EXIST; // time reference already exists
	}
    }
  j =  gbl_num_time_references_defined;  // j is an index
  strcpy( time_ref_params[j].this_reference_name,my_string);
  *index =j;  // return this value
  printf("get_next_ref_index: returning next index=%d\n",j);
  gbl_num_time_references_defined++; // increment
  	      
  return DB_SUCCESS; // success
}


INT  get_this_ref_index(char *my_ref_string, char *name,  INT *index)
{
  /* returns the ref index for the time reference string supplied;
     if string is empty, uses default time reference from last block
     default_time_reference;
  */
  INT j,len;
  char my_string[20];

  len = strlen(my_ref_string);
  if(len<=0)
    {
      /* we will use the default time reference */
      printf("\n*** get_this_ref: block %s - using default reference index %s (%d) from previous block\n",
	     name,
	     time_ref_params[default_time_reference].this_reference_name,
	     default_time_reference);
      if(default_time_reference >=  gbl_num_time_references_defined)
	{
	  cm_msg(MERROR,"get_this_ref_index"," (block %s) - invalid default reference index %d from previous block",
	 name,default_time_reference);
	  return DB_INVALID_PARAM;
	}
      *index = default_time_reference;
      return DB_SUCCESS;
    }

  /* look for this string in the list of time references already defined */
  for (j=0; j<len; j++)
    my_string[j] = toupper(my_ref_string[j]);   
  my_string[len]='\0';
  
  for(j=0; j< gbl_num_time_references_defined ; j++ )
    {
      if(strcmp(time_ref_params[j].this_reference_name, my_string)==0)
	{
	  if(debug)printf("get_ref_index:found this time reference (%s) at index %d; returning %d\n",
		 time_ref_params[j].this_reference_name,j,j);
	  *index = j;
	  return DB_SUCCESS;
	}
    }
  cm_msg(MERROR,"get_ref_index","reference string \"%s\" has not been defined (block %s)",
	 my_ref_string, name);
  printf("get_ref_index: reference string \"%s\" has not been defined (block %s)\n",
	 my_ref_string, name);
  printf("Valid references are:\n");
  for(j=0; j< gbl_num_time_references_defined ; j++ )
    printf("index=%d  time reference = \"%s\"\n",j,time_ref_params[j].this_reference_name);


  return DB_INVALID_PARAM;
}


INT add_time_ref(INT this_refindex, INT my_refindex,  INT code, double time_offset, INT block_index,
  char *name)
{
  INT i;

  /* fills the rest of the parameters in time_ref_params for the new time ref we are defining

     get_next_ref_index has already been called - this returns this_refindex and fills
         defines_timeref_index and this_reference_name in structure time_ref_params

  input:  this_refindex  the index we are defining   
          my_refindex   index to my time reference in time_ref_params
          time_offset define this reference at this offset from my time reference
	  code  time ref code (e.g. timeref_code_time,  timeref_code_begin_loop etc.)
          name  name of this block
	  block_index   index of this block

 */


  
    if(debug)
    {
      printf("add_time_ref: adding params for time reference %s,  defines_timeref_index=%d\n", 
	     time_ref_params[this_refindex].this_reference_name, this_refindex); 
      printf(" input params this_refindex=%d , my_refindex=%d   time_offset=%f    name=%s\n",
	     this_refindex, my_refindex, time_offset,  name);
    }

    if(this_refindex >= MAX_TIME_REF_BLOCKS)
      {
	cm_msg(MERROR,"add_time_ref","too many time references defined (%d). Increase maximum value (%d)",this_refindex,MAX_TIME_REF_BLOCKS);
	return DB_INVALID_PARAM;
      }  
 
  /* save the parameters */
  time_ref_params[this_refindex].timeref_code = code;
  time_ref_params[this_refindex].my_ref = my_refindex;
  time_ref_params[this_refindex].time_ms = time_offset;
  time_ref_params[this_refindex].block_index = block_index; 
  strcpy(time_ref_params[this_refindex].block_name,name);
  
  /* Offset of each reference block is calculated from T0 */
  time_ref_params[this_refindex].t0_offset_ms =  
    time_ref_params[this_refindex].time_ms + 
    time_ref_params[my_refindex].t0_offset_ms;
  if(debug)
    {
      printf("this_refindex = %d\n",this_refindex);
      printf("Defined time ref %d (%s) whose t0_offset_ms = %fms \n = time_ms + ref offset from t0, i.e. %f + %f\n",
	 this_refindex,	 
	 time_ref_params[this_refindex].this_reference_name,
	 time_ref_params[this_refindex].t0_offset_ms , 
	 time_ref_params[this_refindex].time_ms ,
	 time_ref_params[my_refindex].t0_offset_ms);
    }
  default_time_reference = this_refindex; // define this as the default for following blocks
  printf("add_time_ref: defining this reference \"%s\" (index %d)  as the default time ref index\n",
	 time_ref_params[this_refindex].this_reference_name,this_refindex);


  if(debug)
    print_ref_blocks(stdout);
  
    
  return SUCCESS;
}



INT add_pulse_block( char *my_time_ref, INT my_index, INT block_index, BOOL add_auto_timerefs,  char *name)
{
  /*
    parameters: 
    my_time_ref is the user-defined string for the time reference
    my_index     index to pulse_params structure
    block_index  index to this block among all blocks
    add_auto_timerefs define time refs if true; if pulse put in automatically - set false, no time_ref blocks defined
    name         name of this block
  */
  INT i,refindex,define_refindex,my_timeref_code,status;
  double my_offset_s, my_offset_e;
  char string[25];
  BOOL two;
      /* check time ref exists... returns pulse_params[my_index].time_ref_index */
     
  //if(debug)
    printf("add_pulse_block: starting with my_time_ref = %s, my_index = %d, block_index=%d add_autotimerefs=%d, name=%s\n",
	   my_time_ref, my_index, block_index, add_auto_timerefs, name);
  if(get_this_ref_index( my_time_ref, name,
			 &refindex) != SUCCESS)
    
    {
      cm_msg(MERROR,"add_pulse_block","time reference \"%s\" is not defined (block %s) ",
	     my_time_ref  ,name);
      return DB_INVALID_PARAM;
    }
  pulse_params[my_index].time_ref_index = refindex;

  // remember where this transition started
  pulse_params[my_index].t0_offset_ms =  time_ref_params[refindex].t0_offset_ms +  
    pulse_params[my_index].time_ms;
   
  printf("add_pulse_block: after get_this_ref_index, my time_ref_string=%s for refindex=%d gbl_num_transitions=%d\n",	
	 time_ref_params[refindex].this_reference_name,  refindex, gbl_num_transitions);
  
  
    // valid width has already been compared to minimal width; if a transition, width is set to 0
  printf("pulse_params[%d].width_ms=%f  min_delay=%f\n",
	 my_index,pulse_params[my_index].width_ms,  min_delay);

  if(  pulse_params[my_index].width_ms == 0)
    two=FALSE;
  else
    {
      if(compare( pulse_params[my_index].width_ms,  min_delay) >= 0)
	two=TRUE; // two transitions needed
      else
	two=FALSE;
    }
  /* add this to TRANSITIONS RECORD
     add_transitions add the reference time offset (if applicable) */
  my_offset_s=  pulse_params[my_index].time_ms;   
  add_transition(block_index,my_index, refindex, my_offset_s);
  if(two)
    {
      my_offset_e = my_offset_s + pulse_params[my_index].width_ms;
      add_transition(block_index,my_index, refindex, my_offset_e);
    }
  
  if(add_auto_timerefs)
    {
      /* (except for automatic pulse blocks ... no time references wanted)
	 now every pulse block defines a time reference _TSTART_<blockname> and _TEND_<blockname>
	 and every transition block defines _TEND_<blockname> */
      if(two)
	{
	  sprintf(string, "%s%s","_TSTART_",name);
	  my_timeref_code =  timeref_code_start_pulse;
	}
      else
	{
	  sprintf(string, "%s%s","_T",name);
	  my_timeref_code =  timeref_code_end_pulse;
	} 
      /* check whether reference string to be defined is unique, and return the next available timeref_number  */
      status = get_next_ref_index( string,  name, &define_refindex);
      if(status != SUCCESS)
	{
	  printf("add_pulse_block: status return from get_next_ref_index = %d\n",status);
	  return status;  // returns DB_KEY_EXISTS if timeref already exists
	}
      /* fills defines_timeref_index and this_reference_name in structure time_ref_params
	 now fill the other parameters
      */
      
      if(add_time_ref(define_refindex, refindex, my_timeref_code,  
		   my_offset_s,
		      block_index, name)!= SUCCESS)
	return DB_INVALID_PARAM;
      
      if(two)
	{
	  sprintf(string, "%s%s","_TEND_",name);
	  if(get_next_ref_index( string,  name, &define_refindex)!= SUCCESS)
	    return DB_INVALID_PARAM;
	  add_time_ref(define_refindex, refindex, timeref_code_end_pulse,  
		       my_offset_e,
		       block_index, name);
	}
      //print_ref_blocks(stdout); // print time reference blocks
    } // end of if not auto
  if(debug)
    {
      print_pulse_blocks(stdout);
      print_transitions(ptrans,stdout);
    }

  return DB_SUCCESS;
}



INT check_loops(INT loop_index)
{
  /* checks for overlapping loops */
  INT i;
  debug = TRUE; // TEMP

  print_sorted_loop_transitions(ptrans,stdout);




    /* do some serious checking here for overlapping loops */   
      printf("\nchecking for overlapping loops\n");
      for (i=0; i<gbl_num_loops_defined; i++)
	{
	  printf("Check loops: working on loop_index=%d, i=%d\n",loop_index,i);
	  if (i != loop_index)
	    {
	      /* nested loops: if the start falls within loop 1 of a closed loop, so must the end */
	      if ( loop_params[i].open_flag == FALSE)
		{ /* loop i is closed */
		  if(debug)printf("check_loops: loop %d is already closed\n",i);
		  if (loop_params[loop_index].start_t0_offset_ms >  loop_params[i].start_t0_offset_ms)
		    {
		      if(loop_params[loop_index].start_t0_offset_ms >  loop_params[i].all_loops_t0_offset_ms)
			{ /* this loop follows the closed loop (i.e. is completely outside closed loop */
			  if(debug)printf("check_loops:info... this loop %d (%s) follows closed loop %d (%s)\n",
					  loop_index,loop_params[loop_index].loopname,i,loop_params[i].loopname);
			}
		      else if (loop_params[loop_index].all_loops_t0_offset_ms <  loop_params[i].one_loop_t0_offset_ms)
			{
			  /* this loop is wholely within loop 1 of a closed loop; this is OK. */
			  if(debug)printf("check_loops:nested loops - this loop %d (%s) in wholly within closed loop %d  (%s)\n",
					  loop_index,loop_params[loop_index].loopname,i,loop_params[i].loopname);
			}
		      else
			{	
			  printf("check_loops: error... overlapping loops loop_index=%d and %d\n",loop_index,i);
			  printf("       loop %s (blockname %s) and loop %s (blockname %s) ",
				 loop_params[loop_index].loopname, loop_params[loop_index].end_block_name, 
				 loop_params[i].loopname, loop_params[i].end_block_name );
			  printf("Loop %d starts at .2%fms and all loops finish at %.2fms\n",
				 loop_index, loop_params[loop_index].start_t0_offset_ms,
				 loop_params[loop_index].all_loops_t0_offset_ms);
			  printf("Loop %d starts at .2%fms and one loop finishes at %.2fms\n",
				 i, loop_params[i].start_t0_offset_ms,
				 loop_params[i].one_loop_t0_offset_ms);

			  print_loop_params(stdout);

			  cm_msg(MERROR,"get_params",
				 "overlapping loops: loop %s (blockname %s) and loop %s (blockname %s) ",
				 loop_params[loop_index].loopname, loop_params[loop_index].end_block_name, 
				 loop_params[i].loopname, loop_params[i].end_block_name );
			  return DB_INVALID_PARAM;
			}
		    }
		  else
		    { 
		      if (loop_params[loop_index].all_loops_t0_offset_ms <  loop_params[i].start_t0_offset_ms)
			{ /* this loop is finished before closed loop starts */
			  if(debug)printf("this loop %d is  finished before closed loop %d starts \n",loop_index,i);
			}
		      else if ( loop_params[loop_index].one_loop_t0_offset_ms > loop_params[i].all_loops_t0_offset_ms )
			{ /* this loop and closed loop are nested with this loop outside */
			  if(debug)printf("loops are nested; this loop %d is outside closed loop %d \n",loop_index,i);
			}
		      else
			{
			  printf("check_loops: overlapping loops: loop %s (blockname %s) and loop %s (blockname %s)\n ",
				 loop_params[loop_index].loopname, loop_params[loop_index].end_block_name, 
				 loop_params[i].loopname, loop_params[i].end_block_name );
			  cm_msg(MERROR,"get_params",
				 "overlapping loops: loop %s (blockname %s) and loop %s (blockname %s) ",
				 loop_params[loop_index].loopname, loop_params[loop_index].end_block_name, 
				 loop_params[i].loopname, loop_params[i].end_block_name );
			  return DB_INVALID_PARAM;
			}
		    }
		} 
	    }
	}
      printf("check_loops: returning SUCCESS\n");
      return SUCCESS;
}

INT get_last_open_loop(INT *open_loop_index, char *loopname, char *name)
{
  /*  looks for index of last open loop in loop_params
  returns SUCCESS and index to last open loop in open_loop_index
  or
  returns DB_INVALID_PARAM

  */
  INT i;
 
  if(gbl_num_loops_defined ==0)
    {
      cm_msg(MERROR,"get_params","No begin loops have been found yet. Cannot end loop \"%s\" (block %s)",
	     loopname,name);
      return  DB_INVALID_PARAM;
    }
  else if(gbl_open_loop_counter <=0)
    {
      cm_msg(MERROR,"get_params","No loops are open. Cannot end loop \"%s\" (block %s)",
	     loopname,name);
      return  DB_INVALID_PARAM; 
    }
  
  i=gbl_num_loops_defined;
  while (i >= 0 )
    {
      if(loop_params[i].open_flag)
	{
	  printf("get_last_open_loop: last open loop is at index %d\n",i);
	  *open_loop_index = i;
	  return SUCCESS;
	}
      i--;
    }
  /* should not get here because gbl_open_loop_counter should be zero */
   cm_msg(MERROR,"get_params","Cannot find any open loops. Cannot end loop \"%s\" (block %s)",
	  loopname,name);
   return  DB_INVALID_PARAM;
}


INT get_string( HNDLE hKey, char *my_key_name, char *string, INT *psize, char *my_default, INT flag, char *name)
{
  /* flag values:
                   0     use default if value not present; update default with value
                   1     use default if value not present; do not update default 
		   2     value must be present; ignore default
		 
  */

  INT status;
  HNDLE hMyKey;


  printf("get_string: starting with key name %s, block %s\n",my_key_name,name);
  status = db_find_key(hDB, hKey, my_key_name, &hMyKey);
  if(status== SUCCESS) // found the Key
    {
      status = db_get_data(hDB, hMyKey, string, psize, TID_STRING);
      if(status != SUCCESS) // got the Data
	{
	  printf("get_string: error getting data for key \"%s\" (%d) block %s\n",
		 my_key_name,status,name);
	  cm_msg(MERROR,"get_string","error getting data for key \"%s\" (%d) block %s",
		 my_key_name,status,name);
	  //printf("size of string =%d\n",*psize);
	  return status;
	}
    
      if(flag == UPDATE_DEFAULT)
	{
	  printf("get_string: updating default value (%s) to current value %s (block %s)\n",
		 my_default,string,name);
	  strcpy(my_default,string);
	}
      return SUCCESS; // read the data successfully from the key
	
    }
  // no such key present
  if (flag == NO_DEFAULT)
    {
      cm_msg(MERROR,"get_string","error  key %s MUST be supplied (%d) block %s\n",my_key_name,status,name);
      return DB_INVALID_PARAM;
    }
      
  strcpy(string,my_default); // use default value
  if(debug)printf("get_string: no key for %s; returning default value, string=\"%s\", block %s\n",
		  my_key_name,string,name);

  return SUCCESS;
}
      
  
INT get_double( HNDLE hKey, char *my_key_name, double *fval, double *my_default, INT flag, char *name)
{
  /* flag values:
                   0     use default if value not present; update default with value
                   1     use default if value not present; do not update default 
		   2     value must be present; ignore default
		 
  */

  INT size,status;
  HNDLE hMyKey;
  double myval;

  printf("get_double: starting with key name %s, block %s\n",my_key_name,name);

  status = db_find_key(hDB, hKey, my_key_name, &hMyKey);
  if(status== SUCCESS) // got the Key
    {
      size = sizeof(double);
      status = db_get_data(hDB, hMyKey, &myval, &size, TID_DOUBLE);
      if(status != SUCCESS) 
	{
	  cm_msg(MERROR,"get_double","error getting data for key \"%s\" (%d) block %s\n",
		 my_key_name,status,name);
	  //printf("size of string =%d\n",*psize);
	  return status;
	}
      else
	printf("get_double: after get_data from key %s, myval=%f\n",my_key_name,myval);

      *fval = myval;
      // got the data from the key
      //  If the time offset is less than min delay, set it to minimal delay     
      if(  (myval < min_delay) &&  (strcmp (my_key_name, "time offset (ms)")==0))
	{
	  printf("** get_double: setting time offset (%f) to minimal delay for key \"%s\" (%d) block %s\n",
		 myval, my_key_name,status,name);
	  *fval = min_delay;
	}
      if(flag==UPDATE_DEFAULT)
	{
	  printf("get_double: updating default value (%f) to current value %f (block %s)\n",
		 *my_default,*fval,name);
	  *my_default = *fval;
	}


      return SUCCESS; // read the data successfully from the key
   
    }
 
  // Key is not present
  if (flag == NO_DEFAULT)
    {
      cm_msg(MERROR,"get_double","error  key %s MUST be supplied (%d) block %s\n",my_key_name,status,name);
      return DB_INVALID_PARAM;
    }
  
  *fval=*my_default; // use default value
  if(debug)printf("get_double: no key for %s; returning default value, fval=\"%f\", block %s\n",
		  my_key_name,*fval,name);
  return SUCCESS;
  
}
      
  
INT get_dword( HNDLE hKey, char *my_key_name, DWORD *ival, DWORD *my_default, INT flag, char *name)
{
  /* flag values:
                   0     use default if value not present; update default with value
                   1     use default if value not present; do not update default 
		   2     value must be present; ignore default
		 
  */
  INT size,status;
  HNDLE hMyKey;

  printf("get_dword: starting with key name %s, block %s\n",my_key_name,name);

  status = db_find_key(hDB, hKey, my_key_name, &hMyKey);
  if(status== SUCCESS) // found the key
    {
      size = sizeof(DWORD);
      status = db_get_data(hDB, hMyKey, ival, &size, TID_DWORD);
      if(status != SUCCESS)
	{ // can't read the data from the key
	  cm_msg(MERROR,"get_dword","error getting data for key %s (%d) block %s ",my_key_name,status,name);
	  printf("get_dword: error getting data for key %s (%d) block %s\n",my_key_name,status,name);
	  return status;
	}
	
      if(flag == UPDATE_DEFAULT)
	{
	  printf("get_dword: updating default value (%d) to current value %d (block %s)\n",
		 *my_default,*ival,name);
	  *my_default = *ival;
	}
      return SUCCESS; // data has been read successfully from key
	
 
    } // no key present

  if (flag == NO_DEFAULT)
    {
      cm_msg(MERROR,"get_dword","error  key %s MUST be supplied (%d) block %s",my_key_name,status,name);
      printf("get_dword: error  key %s MUST be supplied (%d) block %s\n",my_key_name,status,name);
      return DB_INVALID_PARAM;
    }

  *ival=*my_default; // use default value
  if(debug)printf("get_dword: no key for %s; returning default value, ival=\"%d\", block %s\n",
		  my_key_name,*ival,name);
  return SUCCESS;
}

  
INT get_int( HNDLE hKey, char *my_key_name, INT *ival, INT *my_default, INT flag, char *name)
{
  INT size,status;
  HNDLE hMyKey;

  printf("get_int: starting with key name %s, block %s\n",my_key_name,name);
  
  status = db_find_key(hDB, hKey, my_key_name, &hMyKey);
  if(status== SUCCESS) // found the key
    {
      size = sizeof(INT);
      status = db_get_data(hDB, hMyKey, ival, &size, TID_INT);
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"get_int","error getting data for key %s (%d) block %s\n",my_key_name,status,name);
	  return status;
	}

      if(flag == UPDATE_DEFAULT)
	{
	  printf("get_int: updating default value (%d) to current value %d (block %s)\n",
		 *my_default,*ival,name);
	  *my_default = *ival;
	}
      return SUCCESS; // data has been read successfully from the key
	
    }
    
  //no key present
  if (flag == NO_DEFAULT)
    {
      cm_msg(MERROR,"get_int","error  key %s MUST be supplied (%d) block %s\n",my_key_name,status,name);
      return DB_INVALID_PARAM;
    }
  
  *ival=*my_default; // use default value
  if(debug)printf("get_int: no key for %s; returning default value, ival=\"%d\", block %s\n",
		  my_key_name,*ival,name);
  return SUCCESS;

}




INT auto_add_pulse(char* my_time_reference, 
		   double my_time_offset_ms,
		   double my_pulse_width_ms,
		   char* ppg_signal_name,  
		   BOOL add_auto_time_refs,
		   char* orig_name)
{
  /* Add a pulse block automatically (NOT TDCBLOCK - these are added separately)
  


     If an identical autoblock is already added at this time, does not add a second

    
 
	  EV_STEP and EV_SET blocks adds AWGHV 
  */
  
  INT i,status,inum;
  INT my_pulse_index;
  INT block_index;
  INT refindex;
  char my_name[80];
  double t0_offset_ms;

  //if(debug)
    {
      printf("auto_add_pulse: starting with  time_reference=%s time_offset = %fms\n", 
	     my_time_reference,  my_time_offset_ms);
      printf("  pulse_width=%fms ppg_signal_name=%s add_auto_time_refs=%d orig name=%s\n",
	     my_pulse_width_ms,
	     ppg_signal_name,  
	     add_auto_time_refs, 
	     orig_name);
    }
  /* first check we do not already have this autopulse  */ 
  if(get_this_ref_index( my_time_reference, orig_name,
			 &refindex) != SUCCESS)
    
    {
      cm_msg(MERROR,"auto_add_pulse","time reference \"%s\" is not defined (block %s) ",
	     my_time_reference  ,orig_name);
      return DB_INVALID_PARAM;
    }
  // find where this transition started
  t0_offset_ms =  time_ref_params[refindex].t0_offset_ms +  my_time_offset_ms ;
  inum = check_ppg_name(ppg_signal_name);
  if(inum == -1)
    {
      cm_msg(MERROR,"auto_add_pulse","no such PPG signal name as \"%s\"(block %s) ",
	     ppg_signal_name   ,orig_name);
      return DB_INVALID_PARAM;
    }
  // pulse width is not actually checked
  status = compare_auto_trans (t0_offset_ms, inum,  my_pulse_width_ms, orig_name);
  if(status != SUCCESS)
    {
      printf("auto_add_pulse: pulse is already added; returning\n");
      return SUCCESS;    }


  my_pulse_index = block_counter[PULSE]; 
  block_counter[PULSE]++; // added a pulse block
  block_index = block_counter[TOTAL];
  block_counter[TOTAL]++; /* another block added */
  

  
  block_list[block_index] = ((1<<PULSE) << TYPE_SHIFT) | block_counter[PULSE];
  if(block_counter[PULSE] >  MAX_PULSE_BLOCKS)
    {
      cm_msg(MERROR,"auto_add_pulse", "too many PULSE blocks; maximum allowed = %d",
	     MAX_PULSE_BLOCKS);
      return DB_INVALID_PARAM;
    }

  printf("auto_add_pulse: block_counter[%d]=%d(num pulse blocks)  block_counter[%d]=%d (num blocks) \n",
	 PULSE,block_counter[PULSE],TOTAL,block_counter[TOTAL]);
  printf(" my_time_ref=%s  block_index=%d my_pulse_index=%d\n",my_time_reference,block_index,my_pulse_index);

  sprintf (my_name,"AP%d",my_pulse_index); // make a unique name
  concat(my_name, orig_name, BLOCKNAME_LEN);

  my_name[BLOCKNAME_LEN+1]='\0';

    printf("  block_index=%d my_pulse_index=%d\n",block_index,my_pulse_index);
   

  sprintf(pulse_params[my_pulse_index].block_name,"%s",my_name);
  pulse_params[my_pulse_index].time_ms = my_time_offset_ms;
  pulse_params[my_pulse_index].width_ms =  my_pulse_width_ms;
  strcpy(pulse_params[my_pulse_index].output_name,ppg_signal_name);
  printf("  block_index=%d my_pulse_index=%d\n",block_index,my_pulse_index);
  pulse_params[my_pulse_index].ppg_signal_num = inum;
  
  printf("auto_add_pulse: calling add_pulse_block with block_index=%d my_name=%s\n",
	 block_index,my_name);
  /* call with FALSE if no automatically defined time refs are wanted */
  status = add_pulse_block( my_time_reference, my_pulse_index, 
			    block_index,  add_auto_time_refs , pulse_params[my_pulse_index].block_name);
  if(status != SUCCESS)
    printf("auto_add_pulse: error return from add_pulse_block, status=%d\n",status);
  return status;
}


INT compare_auto_trans( double t0_offset_ms, INT ppg_signal_num, double width_ms, char *name)
{
  /* quick check on whether there is already a transition automatically added here 

  If identical start times for two identical PPG outputs are found (or within min. delay), do not add auto pulse.
  Pulse Widths are not checked - are assumed to be the standard sizes for autopulses.
  */
  INT i,status;
  INT skip=0;
  double diff;
  printf("** compare_auto_trans: starting with t0_offset=%f ppg_signal_num %d\n",
t0_offset_ms, ppg_signal_num	 );
  for(i=0; i <  block_counter[PULSE] ; i++)
    { // compare signal names
      if ( pulse_params[i].ppg_signal_num == ppg_signal_num)
	{
	  /* printf("i=%d ppg signal num = %d,%d; t0 offsets are %f and %f\n",
		 i,pulse_params[i].ppg_signal_num , ppg_signal_num,
		 pulse_params[i].t0_offset_ms, t0_offset_ms ); */
	  
	  if( compare( pulse_params[i].t0_offset_ms,t0_offset_ms) == 0)
	    {
	      // identical start times ... do not add auto pulse
	      printf(
		     "compare_auto_trans: existing transition (PPG bit %d) at identical time (%f) (block %s)\n", 
		     ppg_signal_num,	 
		     t0_offset_ms, pulse_params[i].block_name);
	      printf("          not adding auto block %s\n",name);
	      
	      return skip;
	    }
       
	  diff = pulse_params[i].t0_offset_ms -  t0_offset_ms;
	  if (diff <0) 
	    diff*=-1; // make sure value is positive
	  printf ("XXX diff = %f -%f; abs diff = %f\n", pulse_params[i].t0_offset_ms, t0_offset_ms,diff);
	  printf("comparing %f with minimal delay %f\n",diff, min_delay);
	  status =  compare(diff ,min_delay);
	  printf("after compare, status = %d\n",status);
	  //	  if ( compare(diff ,min_delay) <=0)
	  if(status <= 0) 
	  {// pulse starts are within minimum delay
	      printf(
		     "compare_auto_trans: existing transition (PPG bit %d) within min. delay (%fms) (block %s)\n", 
		     ppg_signal_num,	 
		     t0_offset_ms, pulse_params[i].block_name);
	      printf("          not adding auto block %s\n",name);
	      return skip;
	    }
	}
    } // end of for loop
  printf("compare_auto_trans: no identical transition already; adding auto pulse block %s\n",name);
  return SUCCESS;
}






INT  set_loop(INT index, char *str, BOOL ival, char *my_string, INT *param)
{
  // set or clear open loop flag

  INT status,j;
  char my_loop_name[25];
  BOOL gotit;
  
  if(debug)printf("set_loop: starting with index=%d str=\"%s\" ival=%d\n",
	 index,str,ival,my_string);

  status = get_loop_name(ptrans[index].block_name, str, my_loop_name);
  if(status != SUCCESS)
    return status;

  gotit=FALSE;
  for (j=0; j<gbl_num_loops_defined; j++)
    {
      if(debug)printf("comparing \"%s\" with \"%s\" (at most 9 char)\n",my_loop_name, loop_params[j].loopname);
      if (strncmp(my_loop_name, loop_params[j].loopname,9)==0)
	{
	  loop_params[j].open_flag=ival; // use open flag to indicate open loop
	  gotit=TRUE;
	  break;
	}
    }
  if(!gotit)
    {
      cm_msg(MERROR,"set_loop","cannot find loopname \"%s\" among loop parameters (trans %d,block %s)",
	     my_loop_name,index,ptrans[index].block_name);
      return DB_INVALID_PARAM;
    }

  sprintf(my_string, "%s loop %s %d",str, my_loop_name, loop_params[j].loop_count);
  printf("set_loop: returning my_string=\"%s\"\n",my_string);
  *param =  loop_params[j].loop_count;
  return SUCCESS;
}
  

INT get_loop_name(char *my_block_name, char *str,  char *loop_name)
{
  const int maxlen=32;
  char loopname[33];
  INT len,l,n;
  char *p,*s;

  sprintf(loop_name,"");
  s=loop_name;
  *s='\0';

  l=strlen(str);
  len =  strlen(my_block_name);
  if(debug)printf("get_loop_name: my_block_name: \"%s\"(length %d)  str: \"%s\" (length %d)\n",
		  my_block_name,len,str,l); 
  p=strstr(my_block_name,str);
  if(!p || len<=l)   
    {
      cm_msg(MERROR,"get_loop_name","invalid blockname %s; expect %s<name>",my_block_name,str);
      return DB_INVALID_PARAM;
    }
 
  /* find the loop name */  
  p=p+l;
  n=len-l+1;
  if(n>9)n=9; // maximum characters for loop name
  memcpy(s,p,n);
  loop_name[n]='\0'; // terminate the string
  if(debug) printf("get_loop_name: returning loop_name=%s strlen=%d\n",loop_name,strlen(loop_name));
  return DB_SUCCESS;
}



void concat(char *string1, char *string2, INT max_len)
{
  // concatenate string1 and string2,  unless maximum length of string1 will be exceeded
  INT len;
  len = strlen(string1);
  if ( len + strlen(string2) > max_len)
    printf("concat: cannot add \"%s\" to \"%s\"; string length (%d) would be exceeded\n",
		       string1,string2,len);
  else
    strcat(string1,string2);
  return;
}




#ifdef GONE
INT auto_add_loop(BOOL flag, INT loop_count, TRANSITIONS *ptrans)
{

  /* automatically  add outer loop around cycle
     if flag=TRUE add begin_loop
        flag=FALSE add end_loop
  */

  INT i,status,inum;
  INT my_index,lindex,bg_index;
  INT block_index;
  INT my_refindex, define_refindex;
  char beg_name[]="BEGIN_SCAN";
  char end_name[]="END_SCAN";
  double time_offset_ms=0;
  char time_reference[]="T0";
  char string[20];
  char my_loopname[15];
  double ftmp;


  printf("auto_add_loop: starting with flag=%d loop_count=%d\n",flag,loop_count);
  if(flag)
    {  // add begin_loop parameters  



      /* add a block for this begin_loop block */
      bg_index = block_counter[BEGIN_LOOP];
      block_counter[BEGIN_LOOP]++; // added a begin loop block
      block_index = block_counter[TOTAL];
      block_counter[TOTAL]++; /* another block added */
      
      printf("auto_add_loop: bg_index=%d block_index=%d\n",bg_index,block_index);

      block_list[block_index] = ((1<<BEGIN_LOOP) << TYPE_SHIFT) | block_counter[BEGIN_LOOP];
      if(block_counter[BEGIN_LOOP] >  MAX_LOOP_BLOCKS)
	{
	  cm_msg(MERROR,"auto_add_loop", "too many LOOP blocks; maximum allowed = %d",
		 MAX_LOOP_BLOCKS);
	  return DB_INVALID_PARAM;
	  
	}
      
      
      
      if(loop_count < 1)
	loop_count = 1;
      my_index =0; // first loop
      
      status =  get_loop_name( beg_name, pbegin, loop_params[my_index].loopname); // loopname comes from block name
      if(debug)printf("auto_add_loop: loop_params[%d].loopname=%s\n",my_index, loop_params[my_index].loopname);
      if(status != SUCCESS)
	return status;
      
      /* check my time_ref is valid .. or if not supplied, default will be used ->my_refindex */
      if(get_this_ref_index(time_reference, beg_name,  &my_refindex)!=DB_SUCCESS)
	return  DB_INVALID_PARAM;
      
      
      sprintf(string, "_TBEG%s", loop_params[my_index].loopname ); // name of time reference
      /* check whether time reference string to be defined is unique, and return the next available timeref_number  */
      if(get_next_ref_index( string,  beg_name, &define_refindex)!= SUCCESS)
	return DB_INVALID_PARAM;
      
      
      printf("auto_add_loop: calling add_time_ref with params define_refindex=%d, my_refindex=%d\n",
	     define_refindex, my_refindex);    
      if(add_time_ref(define_refindex, my_refindex, timeref_code_begin_loop,  
		   time_offset_ms, 
		      block_index, beg_name)!= SUCCESS)
	return DB_INVALID_PARAM;
      
      if (debug)
	printf("defined loop_start reference = %s (time_ref index %d) \n",
	       time_ref_params[define_refindex].this_reference_name,
	       define_refindex);
      
      
      gbl_num_loops_defined++; /* total number of loops */
      if(gbl_num_loops_defined > MAX_LOOP_BLOCKS)
	{
	  cm_msg(MERROR,"auto_add_loop","too many loop blocks (maximum=%d) (block %s)\n",
		 MAX_LOOP_BLOCKS ,beg_name	 );
	  return DB_INVALID_PARAM;
	}
      
      strcpy(loop_params[my_index].defines_startref_string,
	     time_ref_params[define_refindex].this_reference_name);      
      loop_params[my_index].my_start_ref_index =my_refindex;
      loop_params[my_index].defines_start_ref_index =define_refindex;
      loop_params[my_index].start_block_name[0]='\0';
      strncat(loop_params[my_index].start_block_name,beg_name, BLOCKNAME_LEN);
      loop_params[my_index].start_time_ms = time_offset_ms;
      loop_params[my_index].loop_count =  loop_count;
      loop_params[my_index].open_flag = TRUE;
      
      // as a precaution, clear parameters to be filled by end_loop
      loop_params[my_index].end_block_name[0]='\0'; // clear this (to be filled by end loop block)
      loop_params[my_index].defines_endref_string[0]='\0';
      loop_params[my_index].one_loop_t0_offset_ms=loop_params[my_index].one_loop_duration_ms=
	loop_params[my_index].all_loops_duration_ms=	loop_params[my_index].all_loops_t0_offset_ms=
	loop_params[my_index].my_end_ref_index=0;
      
      gbl_open_loop_counter++; // count the open loops
     
      if(debug)
	{
	  printf("\nbegin_loop blocks:  %d begin loops  so far \n", block_counter[BEGIN_LOOP]);
	  for (i=0; i < block_counter[BEGIN_LOOP]; i++)
	    printf("begin_loop block %d  begin loop name = %s   loop_counter =%d \n",
		   i, loop_params[i].loopname ,  loop_params[i].loop_count ); 
	  
	}

  
      /* add this to TRANSITIONS RECORD */
      strcpy( ptrans[gbl_num_transitions].block_name, loop_params[my_index].start_block_name);
      
      ptrans[gbl_num_transitions].code    = BEGLOOP_CODE;
      ptrans[gbl_num_transitions].block_index    = block_index;
      ptrans[gbl_num_transitions].loop_index = my_index; // remember which loop this is
      loop_params[my_index].start_t0_offset_ms = loop_params[my_index].start_time_ms
	+ time_ref_params[my_refindex].t0_offset_ms;

      if(  loop_params[my_index].start_t0_offset_ms != time_ref_params[define_refindex].t0_offset_ms)
	{	/* check */
	  printf("expect loop_params[%d].start_t0_offset_ms(%f) = time_ref_params[%d].t0_offset_ms(%f)\n",
		 my_index,        loop_params[my_index].start_t0_offset_ms, 
		 define_refindex, time_ref_params[define_refindex].t0_offset_ms);
	  return DB_INVALID_PARAM;
	}


      ptrans[gbl_num_transitions].t0_offset_ms =  loop_params[my_index].start_t0_offset_ms;
      ptrans[gbl_num_transitions].time_reference_index = 
	loop_params[my_index].my_start_ref_index;
      gbl_num_transitions++;


      if(debug)
	{
	  print_loop_params(stdout);
	  print_transitions(ptrans,stdout);
	}
    }
  else
    {
      // add end_loop


      /* add a block for this begin_loop block */
      bg_index = block_counter[BEGIN_LOOP];
      block_counter[END_LOOP]++; // added an end loop block
      block_index = block_counter[TOTAL];
      block_counter[TOTAL]++; /* another block added */
      
      printf("auto_add_loop: bg_index=%d block_index=%d\n",bg_index,block_index);

      block_list[block_index] = ((1<<END_LOOP) << TYPE_SHIFT) | block_counter[END_LOOP];
      if(block_counter[END_LOOP] >  MAX_LOOP_BLOCKS)
	{
	  cm_msg(MERROR,"auto_add_loop", "too many LOOP blocks; maximum allowed = %d",
		 MAX_LOOP_BLOCKS);
	  return DB_INVALID_PARAM;
	  
	}


      /* now end_loop for scan */
      time_offset_ms = 	ptrans[gbl_num_transitions-1].t0_offset_ms;
      if( ptrans[gbl_num_transitions-1].code != TRANS_CODE )
	time_offset_ms +=   min_delay; // add minimal delay

      printf("time_offset of end_loop will be %f\n",time_offset_ms);

      
      status = get_loop_name( end_name, pend, my_loopname);
      if(status != SUCCESS)
	return status;


      /* this end loop block should be closing the last open begin loop block (no overlapping loops!) */
      status = get_last_open_loop(&lindex,my_loopname, end_name);
      if(status != DB_SUCCESS)
	return status;
      
      if (debug)printf("auto_add_loop: expect to close last open loop, i.e. at loop_params lindex = %d\n",
		       lindex);
      
      /* make sure the loopname match */
      if(debug)printf("this end loop name: %s   loop_params[%d].loopname=%s\n",my_loopname,lindex, 
	     loop_params[lindex].loopname);

      if(strcmp(my_loopname, loop_params[lindex].loopname )== 0)
	{ 
	  if(debug)printf("auto_add_loop: found begin loop with matching name \"%s\" at loop index %d\n",
			  my_loopname,lindex);
	} 

      else
	{
	  printf("Expecting to end loop %s rather than loop %s\n",
		 my_loopname, loop_params[lindex].loopname );
	  cm_msg(MERROR,"auto_add_loop","overlapping loops are not allowed; end loop %s first",
		 my_loopname);
	  return DB_INVALID_PARAM;
	}
   
      if(debug)printf("closing loop %d loopname %s \n",lindex,my_loopname);
      loop_params[lindex].open_flag = FALSE;
      gbl_open_loop_counter--; // counts the open loops
      if(debug)printf("Now there are %d open loops\n",gbl_open_loop_counter);
  
      /* check that our time reference exists ... T0 */   
      if(get_this_ref_index( time_reference, end_name,  &my_refindex)!=DB_SUCCESS)
	return  DB_INVALID_PARAM;
      

      /* can no longer check that this end loop is referenced on the reference defined by the begin_loop ...
            there may be other loops or variable-length pulses in between
      */
      
      loop_params[lindex].end_block_name[0]='\0';
      strncat(loop_params[lindex].end_block_name,end_name, BLOCKNAME_LEN);

  /* add this to TRANSITIONS RECORD */
      ptrans[gbl_num_transitions].time_reference_index = my_refindex;	     
      strcpy( ptrans[gbl_num_transitions].block_name, loop_params[lindex].end_block_name);
      ptrans[gbl_num_transitions].code    = ENDLOOP_CODE;
      ptrans[gbl_num_transitions].loop_index = lindex;
      ptrans[gbl_num_transitions].block_index    = block_index;
      loop_params[lindex].one_loop_duration_ms =  time_offset_ms ; // this is T0 offset from T0, where this loop began
      loop_params[lindex].one_loop_t0_offset_ms = time_offset_ms
	+ time_ref_params[my_refindex].t0_offset_ms;  // same as one loop duration in this case (T0)

  

      ptrans[gbl_num_transitions].t0_offset_ms =  loop_params[lindex].one_loop_t0_offset_ms;
      gbl_num_transitions++;

      if(gbl_num_transitions +1 > MAX_TRANSITIONS ) // about to add another transition as well
	{
	  cm_msg(MERROR,"auto_add_loop","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
	  printf("auto_add_loop: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
	  return DB_INVALID_PARAM;
	}   
      if(debug)
        print_loop_params(stdout);
      

      /* now calculate the total time this loop is going to take & add a reserved block */
      ftmp = ( loop_params[lindex].one_loop_t0_offset_ms - loop_params[lindex].start_t0_offset_ms)
	* (double)loop_params[lindex].loop_count ; // loop duration (all loops)
      if( ftmp <= 0)
	{
	  cm_msg(MERROR,"auto_add_loop","loop \"%s\" cannot end before it starts",my_loopname);
	  printf("auto_add_loop: Error - loop \"%s\" starts at %fms from t0, but ends before this at %fms\n",
		 my_loopname,loop_params[lindex].start_t0_offset_ms, loop_params[lindex].one_loop_t0_offset_ms);
	  return DB_INVALID_PARAM;
	}
      
      if (loop_params[lindex].loop_count== 1)
	ftmp +=  min_delay; /* add minimal delay (reserved block =  1 minimal delay) */
      loop_params[lindex].all_loops_duration_ms = ftmp; 
      loop_params[lindex].all_loops_t0_offset_ms = ftmp + loop_params[lindex].start_t0_offset_ms;
      
      sprintf( ptrans[gbl_num_transitions].block_name,"RES_%s",loop_params[lindex].loopname);
      ptrans[gbl_num_transitions].code    = RES_CODE;
      ptrans[gbl_num_transitions].block_index = block_index; // goes with END LOOP
      ptrans[gbl_num_transitions].loop_index = lindex;
      ptrans[gbl_num_transitions].t0_offset_ms = loop_params[lindex].all_loops_t0_offset_ms;

      /* time reference to end of ONE loop ... for DEBUGGING ONLY */
      /* fill this before calling add_time_ref() */
      sprintf(string, "_TEND%s_ONE",  loop_params[lindex].loopname ); // name of time reference
      /* check whether time reference string to be defined is unique, 
	 and return the next available timeref_number  */
      if(get_next_ref_index( string,  end_name, &define_refindex)!= DB_SUCCESS)
	return DB_INVALID_PARAM;
      /* get_next_ref fills  
	 time_ref_params[define_refindex].defines_timeref_index and  
	 time_ref_params[define_refindex].this_reference_name
	 now fill the other parameters by calling add_time_ref
      */
      // offset from T0
      if(add_time_ref(define_refindex, 0, timeref_code_end_oneloop,  
		   loop_params[lindex].one_loop_t0_offset_ms,
		      block_index, end_name)!= SUCCESS)
	return DB_INVALID_PARAM;



      /* time reference to end of ALL loops */

      /* fill this before calling add_time_ref() */
      sprintf(string, "_TEND%s",  loop_params[lindex].loopname); // name of time reference
      /* check whether time reference string to be defined is unique, 
	 and return the next available timeref_number  */
      if(get_next_ref_index( string,  end_name, &define_refindex)!= DB_SUCCESS)
	return DB_INVALID_PARAM;


    /* get_next_ref fills  
	 time_ref_params[define_refindex].defines_timeref_index and  
	 time_ref_params[define_refindex].this_reference_name
	 now fill the other parameters by calling add_time_ref
      */
      // from T0
      if(add_time_ref(define_refindex, 0, timeref_code_end_loops,  
		   loop_params[lindex].all_loops_t0_offset_ms, 
		      block_index, end_name)!= SUCCESS)
	return DB_INVALID_PARAM;

      strcpy(loop_params[lindex].defines_endref_string,
	     time_ref_params[define_refindex].this_reference_name);      
      loop_params[lindex].my_end_ref_index = my_refindex;
      
      ptrans[gbl_num_transitions].time_reference_index = loop_params[lindex].my_end_ref_index; 
      gbl_num_transitions++;

 if(debug)
	{
	  printf("auto_add_loop: end loop block is: %s\n",end_name);
	  printf("        corresponding begin loop name is %s, loop count %d\n", 
		 loop_params[lindex].loopname,
		 loop_params[lindex].loop_count);
	  printf("  loop began at %f ms from T0; first loop ends at %f ms; last loop ends at %fms \n",
		 loop_params[lindex].start_t0_offset_ms, 
		 loop_params[lindex].one_loop_t0_offset_ms,
		 loop_params[lindex].all_loops_t0_offset_ms);

	  print_transitions(ptrans,stdout);
	}


 //  if(debug)
	{
	  print_loop_params(stdout);
	  print_transitions(ptrans,stdout);
	}

      /* start or end cannot be within reserved blocks of already defined loops */
      if (check_loops(lindex) != SUCCESS)
	return DB_INVALID_PARAM;;


    }
  return SUCCESS;
}
#endif

INT print_default_time_reference()
{
  printf("\nprint_default_time_reference: default reference is \"%s\" (refindex=%d) \n",
	 time_ref_params[default_time_reference].this_reference_name,
	 default_time_reference);
}


  
#ifndef NO_AWG
INT add_awg_pulses(INT my_awg_unit, INT my_refindex, double my_time_ms, char *name)
{
  // adds  AWG pulse(s)

  INT status;
  INT my_pulse_index;
  char signame[16];

  printf("add_awg_pulses: starting with my_awg_unit=%d my_refindex=%d my_time_ms=%f name=%s\n",
	 my_awg_unit,my_refindex,my_time_ms, name);



  /* Add AWG  pulses;  add time refs  */ 

  my_pulse_index = block_counter[PULSE];
  block_counter[PULSE]++; // added a pulse block
  //  block_index = block_counter[TOTAL];
  block_counter[TOTAL]++; /* another block added */

  printf("add_awg_pulses:Adding AWG pulse \"%s\" AWG unit %d my_pulse_index=%d\n", 
	 awg_trigger_names[my_awg_unit],my_awg_unit, my_pulse_index);

  sprintf(signame,"%s",awg_trigger_names[my_awg_unit]); // convert const char to char
  printf("add_awg_pulses: calling auto_add_pulse \n");
  status = auto_add_pulse( time_ref_params[my_refindex].this_reference_name, 
			  my_time_ms  , settings.ppg.input.awg_clock_width__ms_,
			   signame,
			 AUTO_TIMEREFS ,name );
  printf("add_awg_pulses: after auto_add_pulse for AWG unit %d, status=%d\n",
	 my_awg_unit, status);
  
  if(status != SUCCESS)
    {
      printf("add_awg_pulses: Error status after returning from auto_add_pulse (%d) (%s) block %s\n",
	     status, signame,name);
      return DB_INVALID_PARAM;
    }
#ifdef AUTO_TDCBLOCK
  /* add TDCBLOCK; no auto time refs */   
  printf("add_awg_pulses: calling auto_add_tdcblock  \n");
  //  (my_pulse_index incremented now that awg pulse is added)
  status = auto_add_tdcblock( time_ref_params[my_refindex].this_reference_name, 
			   my_time_ms , my_pulse_index+1,  name );


  //printf("after auto_add_pulse for TDCBLOCK, status=%d\n",status);

  if(status != SUCCESS)
    { 	 
      printf("add_awg_pulses: Error status after returning from auto_add_pulse (%d) (TDCBLOCK) block %s\n",
	     status,name);
      return DB_INVALID_PARAM;
    }
#endif // AUTO_TDCBLOCK

  printf("add_awg_pulses: printing time refs and returning\n");
  print_default_time_reference();
  return status;
}
#endif // NO_AWG

#ifdef GONE
INT add_tdcblock_pulses(void)
{
  INT status,i,inum;
  char ppg_signal_name[]="TDCBLOCK";
  DWORD bitpat;

  inum = check_ppg_name("TDCBLOCK");  // returns PPG signal number
  if(inum == -1)
    {
      cm_msg(MERROR,"add_tdcblock_pulses","no such PPG signal name as TDCBLOCK ",
	     ppg_signal_name);
      return DB_INVALID_PARAM;
    }
  bitpat = 1<< inum;

  for (i=0; i< gbl_num_tdcblock_pulses; i++)
    {
      if(gbl_num_transitions +2 > MAX_TRANSITIONS )
	{
	  cm_msg(MERROR,"get_params","too many transitions for array size (max=%d)",MAX_TRANSITIONS);
	  printf("get_params: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
	  return DB_INVALID_PARAM;
	}

      ptrans[gbl_num_transitions].time_reference_index = 0; // T0
      ptrans[gbl_num_transitions].code  = TRANS_CODE;
      ptrans[gbl_num_transitions].block_index    = i; // point this to tdcblock index
      ptrans[gbl_num_transitions].t0_offset_ms  =  tdcblock[i].t0_offset_ms;
      ptrans[gbl_num_transitions].bit = inum;
      ptrans[gbl_num_transitions].bitpattern = bitpat;
      gbl_num_transitions++;

      ptrans[gbl_num_transitions].time_reference_index = 0; // T0
      ptrans[gbl_num_transitions].code  = TRANS_CODE;
      ptrans[gbl_num_transitions].block_index    = i; // point this to tdcblock index
      ptrans[gbl_num_transitions].t0_offset_ms  =  tdcblock[i].t0_offset_ms + tdcblock[i].width_ms ;
      ptrans[gbl_num_transitions].bit = inum;
      ptrans[gbl_num_transitions].bitpattern = bitpat;
      gbl_num_transitions++;
    } // end of loop on TDCBLOCK pulses
  print_transitions(ptrans,stdout);
  return DB_SUCCESS;
}
#endif


#ifdef AUTO_TDCBLOCK
INT auto_add_tdcblock(char* my_time_reference, 
		      double my_time_offset_ms,  
		      INT orig_pulse_index,
		      char* orig_name)
{
  /* Add a TDCBLOCK pulse to tdcblock */
  
  INT i,status;
  INT block_index;
  INT refindex;
  char my_name[80];
  double t0_offset_ms;
  double tdcblock_width_ms;
  double diff,start,end;
  double value,temp;
  BOOL add;
  
  //if(debug)
  {
    printf("\n auto_add_tdcblock: starting with  time_reference=%s time_offset = %fms\n", 
	   my_time_reference,  my_time_offset_ms);
    printf("  orig name=%s  orig_pulse_index = %d\n",
	   orig_name,orig_pulse_index);
  }
  
  //  tdcblock_width_ms = settings.ppg.input.tdcblock_width__ms_;
  
  /* first look for the time ref of original pulse  */ 
  if(get_this_ref_index( my_time_reference, orig_name,
			 &refindex) != SUCCESS)
    
    {
      cm_msg(MERROR,"auto_add_tdcblock","time reference \"%s\" is not defined (block %s) ",
	     my_time_reference);
      return DB_INVALID_PARAM;
    }
  tdcblock[gbl_num_tdcblock_pulses].t0_offset_ms = 
    time_ref_params[refindex].t0_offset_ms +  my_time_offset_ms;
  tdcblock[gbl_num_tdcblock_pulses].pulse_index=orig_pulse_index;   
  gbl_num_tdcblock_pulses++;
 
  printf(" auto_add_tdcblock: added t0_offet=%f;  gbl_num_tdcblock_pulses=%d\n",
	 tdcblock[gbl_num_tdcblock_pulses-1].t0_offset_ms,	 gbl_num_tdcblock_pulses);

  print_tdcblock_params();
  return SUCCESS;
}
#endif // AUTO_TDCBLOCK

double max (double a, double b)
{
	if(a>b) 
	return a;
	else
	return b;
}
double min (double a, double b)
{
	if(a>b) 
	return b;
	else
	return a;
}


#ifdef GONE
void add_pulse_info(INT i, INT orig_pulse_index)
{
  /* tdcblock.pulses[] array contains list of pulses that contribute to this tdcblock
          the pulses are listed by their index in pulse_params structure (orig_pulse_index)
     tdcblock.num_pulses is the number of items in the above array, i.e. number of
          pulses that contribute to this tdcblock pulse
  */
  INT j;

 if( tdcblock[i].num_pulses +1 >= MAX_T)
     printf("add_pulse_info: need to increase pulses array size in TDCBLOCK\n");
 else
  {
    j =  tdcblock[i].num_pulses;
    tdcblock[i].pulses[j] = orig_pulse_index;
    tdcblock[i].num_pulses++;
  }
 return;
}

#endif
