/*  
   Name: ppg_structure.c
   Created by: SD

   Contents:  Functions originally in tri_config.c that need different versions for POL and TITAN due to structure difference

   $Id: ppg_structure.c,v 1.1 2011/04/15 18:50:31 suz Exp $

                                         
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>  // needed to interpret errors after "system" call
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
#include "print_blocks.h"
#include "info_blocks.h"

extern double min_delay;
extern INT error_flag;
extern BOOL first_msg;
extern HNDLE hDB, hSET, hPPG,hIn,hOut,hCycle;
#ifndef POL
extern TITAN_ACQ_SETTINGS settings;
#else
extern POL_ACQ_SETTINGS settings;
#endif
extern TRANSITIONS *ptrans;


#ifndef POL
INT tr_precheck(INT run_number, char *error)
{
  INT status,size;
  BOOL flag;
  
  error_flag=0;
  if(debug)first_msg=1;


  /* get settings all records */
  status = settings_rec_get();/* returns -1 for fail, or SUCCESS */
  if (status < 0)
    {
      printf("tr_precheck: Error return after settings_rec_get. See odb messages for details\n");
      return status;
    }
  

  settings.ppg.output.ppg_nominal_frequency__mhz_ =  PPG_CLOCK_MHZ; // internal clock freq and/or frequency coded in header.ppg

  printf("tr_precheck: PPG is running at =%f MHz; Nominal freq  = %f MHz \n",
	 settings.ppg.input.ppg_clock__mhz_,  settings.ppg.output.ppg_nominal_frequency__mhz_);
  
  if ( (settings.ppg.output.ppg_nominal_frequency__mhz_ < 1 ) ||   
       (settings.ppg.input.ppg_clock__mhz_ <  1 )             ||
       (settings.ppg.output.ppg_nominal_frequency__mhz_ > 100)||  
       (settings.ppg.input.ppg_clock__mhz_ > 100) )
    {
      cm_msg(MERROR,"tr_precheck",
	     "illegal ppg frequency value(s): PPG clock=%dMHz  nominal value=%dMHz. Must be between 1 and 100 MHz\n",
	     settings.ppg.output.ppg_nominal_frequency__mhz_ ,  settings.ppg.input.ppg_clock__mhz_);
      return DB_INVALID_PARAM;
    }
  
   settings.ppg.output.ppg_freq_conversion_factor = 
     settings.ppg.input.ppg_clock__mhz_ / settings.ppg.output.ppg_nominal_frequency__mhz_ ;
  printf("frequency conversion factor between actual and nominal PPG clock frequency is %f\n",
	 settings.ppg.output.ppg_freq_conversion_factor);
  

  settings.ppg.output.time_slice__ms_ = 1e-3/settings.ppg.input.ppg_clock__mhz_; // converted to ms
  settings.ppg.output.minimal_delay__ms_ = 5 * settings.ppg.output.time_slice__ms_; 

  // setup external minimal delay
  min_delay =  settings.ppg.output.minimal_delay__ms_;
  printf("tr_precheck:  min_delay is %f\n",min_delay);
  
  printf("tr_precheck: time slice =%f ms; min delay = %f ms; standard pulse width = %f ms\n",
	 settings.ppg.output.time_slice__ms_, settings.ppg.output.minimal_delay__ms_,
	 settings.ppg.input.standard_pulse_width__ms_);

  settings.ppg.output.loops.num_loops=0; 


  /*   Write the output values here in case of error prior to prestart 
       compile time of bytecode.dat will not yet be updated, of course */ 
     
  /* Update these output settings in ODB */
  size = sizeof(settings.ppg.output);
  status = db_set_record(hDB, hOut, &settings.ppg.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);



  /* all std pulses are of width "standard pulse width"  
     i.e.  settings.ppg.input.standard_pulse_width__ms_  */

  if(debug)printf("tr_precheck: standard pulse width is %fms\n", settings.ppg.input.standard_pulse_width__ms_   );
  
  init_blocks(); // initialize
  set_awg_params(); // setup constants and defaults for awg

 
  define_t0_ref();  /* defines a dummy block with T0 as the first reference */




  /* we can't add this automatically any more... need pulse(s) prior to scan loop   
  status = auto_add_loop(1,  settings.ppg.input.number_of_cycles ,ptrans); // add begin_scan loop
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"tr_precheck","error adding begin of scan loop");
      return status;
    }
  */
  status = define_trans0(); /* start with a dummy pattern at T0 */
  if( status != SUCCESS)
    {
      printf("tr_precheck: error from define_trans0\n");
      return status ;
    }

  print_transitions(ptrans,stdout);
  //  if(hLEGI)get_input_data(hLEGI);

  status = get_input_data(hCycle);
  if(status != SUCCESS)
    printf("tr_precheck: error from get_input_data\n");
  else
    print_sorted_loop_transitions(ptrans,tfile);
  return status;
}



INT tr_prestart(INT run_number, char *error)
{
  INT status,size;
  char  ppg_mode[30];
 char  outfile[256] , foutfile[256]  , cmd[512];
 char  infile[256]  , finfile[256];
 char  infofile[256];
 FILE *errfile;
  char str[256];
 time_t timbuf;
  char timbuf_ascii[30];
  DWORD elapsed;
  char path[80];
 struct stat stat_buf;

  check_params(ppg_mode);
  //printf("after check_params, ppg_mode = %s\n",ppg_mode);
  status = compute(ppg_mode);
  if(status != SUCCESS)
    {
      printf("Error from compute, see odb messages for details\n");
      return status;
    }

  print_delays(tfile);

  fclose(tfile);
  tfile=0;

  /* remove old bytecode.dat from the PPG path */
  sprintf(foutfile,"%sbytecode.dat", settings.ppg.input.ppgload_path);
  //printf("foutfile=%s\n",foutfile);
  status = ss_file_remove(foutfile);
  if (status != 0)
      cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);

  /* Get the name of the output file (e.g. titan.ppg) */
  sprintf(outfile,"%s%s%s",settings.ppg.input.ppgload_path,ppg_mode,".ppg");


 /* update odb parameters */
  settings.ppg.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (settings.ppg.output.compiled_file_time,"file deleted"); 
  

 sprintf(infofile,"%s/ppgmsg.txt",settings.ppg.input.ppgload_path );
 
 sprintf(cmd,"/usr/bin/perl %s/comp_int.pl %s %s  > %s", settings.ppg.input.ppg_perl_path, 
   outfile, foutfile, infofile );

 // ls > dirlist 2>&1  redirect stdout and stderr to the same file

  if(strlen(cmd) > sizeof(cmd))
    { /* overwriting cmd caused stat to fail due to overwrite of foutfile */
      cm_msg(MERROR,"prestart","Internal programming error. Length of cmd (%d) is too short for command string(%d)",strlen(cmd) , sizeof(cmd));
      printf("tr_prestart: Error return. See odb messages for details\n");
      return -1;
    }

  errno=0;  // clear errno
  status = system(cmd);
  if(debug)
    {
      printf("cmd=\"%s\"\n",cmd);
      printf("status after system command = %d\n",status);
    }

  if(status)
    {
     cm_msg(MERROR,"prestart","sys error (from ppg compiler) [%d/%d] %s", (status/256), errno, strerror(errno));
     printf("prestart: Error return from ppg compiler status/256=%d errno=%d strerror(errno)=%s\n",
	    status/256, errno, strerror(errno));
     printf("prestart: strerror(status/256)=%s\n",strerror(status/256));

     errfile = fopen(infofile,"r");
      
     if (errfile != NULL)
       {
	 sprintf(str,"PPG COMPILER ERROR MESSAGES:");
	 cm_msg(MERROR,"prestart","%s",str);
	 while(fgets(str,256,errfile) != NULL)
	   cm_msg(MERROR,"prestart","%s",str);
	 fclose(errfile);
       }      
     else
       cm_msg(MERROR,"prestart","Couldn't open ppg compiler error file %s",infofile);


     cm_msg(MERROR,"prestart","File bytecode.dat has NOT been produced by ppg compiler");
     printf("tr_prestart: Error return. See odb messages for details\n");
     return status;
    }
  else
    //    cm_msg(MINFO,"prestart","Successfully compiled ppg file & written ",foutfile);
    printf("prestart:Successfully compiled ppg file & written \n",foutfile);

  /* wait for compiled file */
  if(debug)printf("waiting for compiled file %sbytecode.dat...\n", settings.ppg.input.ppgload_path);
  status = file_wait(settings.ppg.input.ppgload_path, "bytecode.dat");
  //if(debug)printf("status after file wait = %d\n",status);
  if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %sbytecode.dat not found - Compile error", settings.ppg.input.ppgload_path);
      cm_msg(MINFO,"prestart","Compiler messages are in file %s\n",infofile); 
      printf("Compile command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
    else
      //  cm_msg(MINFO,"prestart","compiled file %s produced successfully",foutfile);
      printf("prestart: compiled file %s produced successfully\n",foutfile);

//printf("foutfile=%s\n",foutfile);
  if( stat (foutfile,&stat_buf) ==0 )
  {
    settings.ppg.output.compiled_file_time__binary_ = stat_buf.st_ctime; /* binary time */
    strcpy(timbuf_ascii, (char *) (ctime(&stat_buf.st_ctime)) );  
     if(debug)printf("Last change to file %s:  %s\n",foutfile,timbuf_ascii); 
    {
      int j;
      j=strlen(timbuf_ascii);
      strncpy(settings.ppg.output.compiled_file_time,timbuf_ascii, j);

      /* An extra carriage return appears unless we do this:  */
      settings.ppg.output.compiled_file_time[j-1] ='\0';
    }    
      
  }
  else
    {
    strcpy (settings.ppg.output.compiled_file_time,"no information available");
    cm_msg(MERROR,"prestart","No information available about compile time of file %s",foutfile);
    cm_msg(MINFO,"prestart","Front-end check on compile time of file will fail");
    }
  
   if(debug)printf("Writing %s to odb ( compiled file time)\n",
		   settings.ppg.output.compiled_file_time ); 

  /* Update output settings in ODB */
  size = sizeof(settings.ppg.output);
  status = db_set_record(hDB, hOut, &settings.ppg.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);

 

  return SUCCESS;
}

void check_params(char *ppg_mode)
{
  INT size, status,j;
  char str[128];
  


 if (settings.ppg.input.ppgload_path[0] != 0)
   if (settings.ppg.input.ppgload_path[strlen(settings.ppg.input.ppgload_path)-1] != DIR_SEPARATOR)          
     strcat(settings.ppg.input.ppgload_path, DIR_SEPARATOR_STR);
 
   
 if (settings.ppg.input.ppg_path[0] != 0)
   if (settings.ppg.input.ppg_path[strlen(settings.ppg.input.ppg_path)-1] != DIR_SEPARATOR)
     strcat(settings.ppg.input.ppg_path, DIR_SEPARATOR_STR);
 
 if(debug)
   {
     printf("ppg path (for input): %s \n",settings.ppg.input.ppg_path);
     printf("ppgload path (for output) : %s \n",settings.ppg.input.ppgload_path);
   }
 sprintf(ppg_mode, "%s", settings.ppg.input.experiment_name);

}


//========================================================================
#else  // POL

INT tr_precheck(INT run_number, char *error)
{
  INT status,size;
  BOOL flag;
  
  error_flag=0;
  if(debug)first_msg=1;


  /* get settings all records */
  status = settings_rec_get();/* returns -1 for fail, or SUCCESS */
  if (status < 0)
    {
      printf("tr_precheck: Error return after settings_rec_get. See odb messages for details\n");
      return status;
    }
  


  settings.output.ppg_nominal_frequency__mhz_ =  PPG_CLOCK_MHZ; // internal clock freq and/or frequency coded in header.ppg

  printf("tr_precheck: PPG is running at =%f MHz; Nominal freq  = %f MHz \n",
	 settings.input.ppg_clock__mhz_,  settings.output.ppg_nominal_frequency__mhz_);
  
  if ( (settings.output.ppg_nominal_frequency__mhz_ < 1 ) ||   
       (settings.input.ppg_clock__mhz_ <  1 )             ||
       (settings.output.ppg_nominal_frequency__mhz_ > 100)||  
       (settings.input.ppg_clock__mhz_ > 100) )
    {
      cm_msg(MERROR,"tr_precheck",
	     "illegal ppg frequency value(s): PPG clock=%dMHz  nominal value=%dMHz. Must be between 1 and 100 MHz\n",
	     settings.output.ppg_nominal_frequency__mhz_ ,  settings.input.ppg_clock__mhz_);
      return DB_INVALID_PARAM;
    }
  
  // perlscript for old ppg has a limit of about 80MHZ. Trick it by using a freq. conversion factor
   settings.output.ppg_freq_conversion_factor = 
     settings.input.ppg_clock__mhz_ / settings.output.ppg_nominal_frequency__mhz_ ;
  printf("frequency conversion factor between actual and nominal PPG clock frequency is %f\n",
	 settings.output.ppg_freq_conversion_factor);
  

  settings.output.time_slice__ms_ = 1e-3/settings.input.ppg_clock__mhz_; // converted to ms
  settings.output.minimal_delay__ms_ = 5 * settings.output.time_slice__ms_; 

  min_delay =  settings.output.minimal_delay__ms_;
  printf("tr_precheck:  min_delay is %f\n",min_delay);

  
  printf("tr_precheck: time slice =%f ms; min delay = %f ms; standard pulse width = %f ms\n",
	 settings.output.time_slice__ms_, settings.output.minimal_delay__ms_,
	 settings.input.standard_pulse_width__ms_);

  settings.output.loops.num_loops=0; 


  /*   Write the output values here in case of error prior to prestart 
       compile time of bytecode.dat will not yet be updated, of course */ 
     
  /* Update these output settings in ODB */
  size = sizeof(settings.output);
  status = db_set_record(hDB, hOut, &settings.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);



  /* all std pulses are of width "standard pulse width"  
     i.e.  settings.input.standard_pulse_width__ms_  */

  if(debug)printf("tr_precheck: standard pulse width is %fms\n", settings.input.standard_pulse_width__ms_   );
  
  init_blocks(); // initialize
  set_awg_params(); // setup constants and defaults for awg

 
  define_t0_ref();  /* defines a dummy block with T0 as the first reference */




  /* we can't add this automatically any more... need pulse(s) prior to scan loop   
  status = auto_add_loop(1,  settings.input.number_of_cycles ,ptrans); // add begin_scan loop
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"tr_precheck","error adding begin of scan loop");
      return status;
    }
  */
  status = define_trans0(); /* start with a dummy pattern at T0 */
  if( status != SUCCESS)
    {
      printf("tr_precheck: error from define_trans0\n");
      return status ;
    }

  print_transitions(ptrans,stdout);
  //  if(hLEGI)get_input_data(hLEGI);

  status = get_input_data(hCycle);
  if(status != SUCCESS)
    printf("tr_precheck: error from get_input_data\n");
  else
    print_sorted_loop_transitions(ptrans,tfile);
  return status;
}


INT tr_prestart(INT run_number, char *error)
{
  INT status,size;
 struct stat stat_buf;
  char  ppg_mode[30];
 char  outfile[256] , foutfile[256]  , cmd[512];
 char  infile[256]  , finfile[256];
 char  infofile[256];
 FILE *errfile;
  char str[256];
 time_t timbuf;
  char timbuf_ascii[30];
  DWORD elapsed;
  char path[80];
  char ppgload_path[80],ppg_perl_path[80];


 sprintf(ppgload_path,"%sppgload/",  settings.input.ppg_path);
 sprintf(ppg_perl_path,"%sperl/",  settings.input.ppg_path);
  check_params(ppg_mode);
  //printf("after check_params, ppg_mode = %s\n",ppg_mode);
  status = compute(ppg_mode);
  if(status != SUCCESS)
    {
      printf("Error from compute, see odb messages for details\n");
      return status;
    }

  print_delays(tfile);

  fclose(tfile);
  tfile=0;

  /* remove old bytecode.dat from the PPG path */
  sprintf(foutfile,"%sbytecode.dat", ppgload_path);
  //printf("foutfile=%s\n",foutfile);
  status = ss_file_remove(foutfile);
  if (status != 0)
      cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);

  /* Get the name of the output file (e.g. pol.ppg) */
  sprintf(outfile,"%s%s%s",ppgload_path,ppg_mode,".ppg");


 /* update odb parameters */
  settings.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (settings.output.compiled_file_time,"bytecode file deleted"); 
  

 sprintf(infofile,"%s/ppgmsg.txt",ppgload_path );
 
 sprintf(cmd,"/usr/bin/perl %s/comp_int.pl %s %s  > %s", ppg_perl_path, 
   outfile, foutfile, infofile );

 // ls > dirlist 2>&1  redirect stdout and stderr to the same file

  if(strlen(cmd) > sizeof(cmd))
    { /* overwriting cmd caused stat to fail due to overwrite of foutfile */
      cm_msg(MERROR,"prestart","Internal programming error. Length of cmd (%d) is too short for command string(%d)",strlen(cmd) , sizeof(cmd));
      printf("tr_prestart: Error return. See odb messages for details\n");
      return -1;
    }

  errno=0;  // clear errno
  status = system(cmd);
  if(debug)
    {
      printf("cmd=\"%s\"\n",cmd);
      printf("status after system command = %d\n",status);
    }

  if(status)
    {
     cm_msg(MERROR,"prestart","sys error (from ppg compiler) [%d/%d] %s", (status/256), errno, strerror(errno));
     printf("prestart: Error return from ppg compiler status/256=%d errno=%d strerror(errno)=%s\n",
	    status/256, errno, strerror(errno));
     printf("prestart: strerror(status/256)=%s\n",strerror(status/256));

     errfile = fopen(infofile,"r");
      
     if (errfile != NULL)
       {
	 sprintf(str,"PPG COMPILER ERROR MESSAGES:");
	 cm_msg(MERROR,"prestart","%s",str);
	 while(fgets(str,256,errfile) != NULL)
	   cm_msg(MERROR,"prestart","%s",str);
	 fclose(errfile);
       }      
     else
       cm_msg(MERROR,"prestart","Couldn't open ppg compiler error file %s",infofile);


     cm_msg(MERROR,"prestart","File bytecode.dat has NOT been produced by ppg compiler");
     printf("tr_prestart: Error return. See odb messages for details\n");
     return status;
    }
  else
    //    cm_msg(MINFO,"prestart","Successfully compiled ppg file & written ",foutfile);
    printf("prestart:Successfully compiled ppg file & written \n",foutfile);

  /* wait for compiled file */
  if(debug)printf("waiting for compiled file %sbytecode.dat...\n", ppgload_path);
  status = file_wait(ppgload_path, "bytecode.dat");
  //if(debug)printf("status after file wait = %d\n",status);
  if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %sbytecode.dat not found - Compile error", ppgload_path);
      cm_msg(MINFO,"prestart","Compiler messages are in file %s\n",infofile); 
      printf("Compile command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
    else
      //  cm_msg(MINFO,"prestart","compiled file %s produced successfully",foutfile);
      printf("prestart: compiled file %s produced successfully\n",foutfile);

#ifdef NEWPPG

  /* Convert bytecode.dat to ppgload.dat for new ppg

     remove old ppgload.dat file from the PPG path */
  sprintf(foutfile,"%sppgload.dat", ppgload_path);
  //printf("foutfile=%s\n",foutfile);
  status = ss_file_remove(foutfile);
  if (status != 0)
      cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);

  /* Get the name of the output file (bytecode.dat) */
  sprintf(outfile,"%s%s",ppgload_path,"bytecode.dat");


 /* update odb parameters */
  settings.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (settings.output.compiled_file_time,"ppgload file deleted"); 

  

  sprintf(cmd,"/usr/bin/perl %s/convert_bytecode.pl %s %s 0 %d", 
	 ppg_perl_path, 
	  outfile, foutfile, (INT)settings.input.ppg_clock__mhz_ );



  errno=0;  // clear errno
  status = system(cmd);
  // if (debug)
    {
      printf("Command string for conversion to NEW PPG code was: \n\"%s\"\n",cmd);
      printf("status after system command = %d\n",status);
    }

  if(status)
    {
     cm_msg(MERROR,"prestart","sys error (from ppg converter) [%d/%d] %s", (status/256), errno, strerror(errno));
     printf("prestart: Error return from ppg converter status/256=%d errno=%d strerror(errno)=%s\n",
	    status/256, errno, strerror(errno));
     printf("prestart: strerror(status/256)=%s\n",strerror(status/256));
     
     cm_msg(MERROR,"prestart","File ppgload.dat has NOT been produced by ppg converter");
     printf("tr_prestart: Error return. See odb messages for details\n");
     return status;
    }
  else
    {
      cm_msg(MINFO,"prestart","Successfully compiled ppg file & written ",foutfile);
      printf("prestart:Successfully converted ppg file & written \n",foutfile);
    }

  /* correct minimal delay time for the NEW PPG (3 clock cycles rather than 5 for OLD) */
    settings.output.minimal_delay__ms_ = 3 * settings.output.time_slice__ms_;


  /* wait for converted file */
  if(debug)printf("waiting for converted file %sppgload.dat...\n", ppgload_path);
  status = file_wait(ppgload_path, "ppgload.dat");
  //if(debug)printf("status after file wait = %d\n",status);
  if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %sppgload.dat not found - Conversion error", ppgload_path); 
      printf("Convert command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
    else
      //  cm_msg(MINFO,"prestart","compiled file %s produced successfully",foutfile);
      printf("prestart: converted file %s produced successfully\n",foutfile);
#endif // NEWPPG


  if( stat (foutfile,&stat_buf) ==0 )
  {
    settings.output.compiled_file_time__binary_ = stat_buf.st_ctime; /* binary time */
    strcpy(timbuf_ascii, (char *) (ctime(&stat_buf.st_ctime)) );  
     if(debug)printf("Last change to file %s:  %s\n",foutfile,timbuf_ascii); 
    {
      int j;
      j=strlen(timbuf_ascii);
      strncpy(settings.output.compiled_file_time,timbuf_ascii, j);

      /* An extra carriage return appears unless we do this:  */
      settings.output.compiled_file_time[j-1] ='\0';
    }    
      
  }
  else
    {
    strcpy (settings.output.compiled_file_time,"no information available");
    cm_msg(MERROR,"prestart","No information available about creation time of file %s",foutfile);
    cm_msg(MINFO,"prestart","Front-end check on creation time of file will fail");
    }
  
   if(debug)printf("Writing %s to odb ( time file created )\n",
		   settings.output.compiled_file_time ); 

  /* Update output settings in ODB */
  size = sizeof(settings.output);
  status = db_set_record(hDB, hOut, &settings.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);

  return SUCCESS;
}

void check_params(char *ppg_mode)
{
  INT size, status,j;
  char str[128];
  char ppgload_path[80];
  
 sprintf(ppgload_path,"%sppgload/",  settings.input.ppg_path);

 if (ppgload_path[0] != 0)
   if (ppgload_path[strlen(ppgload_path)-1] != DIR_SEPARATOR)          
     strcat(ppgload_path, DIR_SEPARATOR_STR);
 
   
 if (settings.input.ppg_path[0] != 0)
   if (settings.input.ppg_path[strlen(settings.input.ppg_path)-1] != DIR_SEPARATOR)
     strcat(settings.input.ppg_path, DIR_SEPARATOR_STR);
 
 if(debug)
   {
     printf("ppg path (for input): %s \n",settings.input.ppg_path);
     printf("ppgload path (for output) : %s \n",ppgload_path);
   }
 sprintf(ppg_mode, "%s", settings.input.experiment_name);

#ifndef NEWPPG
 if (settings.input.new_ppg)
   {
     cm_msg(MERROR,"check_params","New PPG selected in ODB settings/input tree, but tri_config NOT built for new ppg");
     return DB_INVALID_PARAM;
   }
#endif // NEWPPG
}
#endif // POL
