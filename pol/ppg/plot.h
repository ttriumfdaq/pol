// plot.h
#ifndef  PLOT_INCLUDE_H
#define  PLOT_INCLUDE_H

INT plot_transitions(FILE *plotf, TRANSITIONS *ptrans, double *max_ms, BOOL *plflag, BOOL *pfawg);
void plot_picture(char *filename, INT nplot, double max_ms, INT lflag, char *outfile);
INT plot_line(FILE *plotf, double offset, DWORD bitpattern, char *blockname,  INT *bitused);
INT check_used_bits(TRANSITIONS *ptrans,  INT *pbitused, char *namestring);
void plot_awg(char *filename, INT awg_unit,  double max_ms, double pulse_width_ms);
void plot_awg_picture(INT unit);
#endif
