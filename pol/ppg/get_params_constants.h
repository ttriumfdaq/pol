/*
   Name: get_params_constants.h
   Created by: SD

   Contents: defines parameter blocks needed by get_params.c

 $Id: get_params_constants.h,v 1.1 2011/04/15 18:50:31 suz Exp $

*/
#ifndef  PARAMS_INFO_H
#define  PARAMS_INFO_H


 // flag values for get_double etc.  
#define NO_DEFAULT 2 // value must be supplied otherwise return failure
#define USE_DEFAULT 1 // use value if supplied, otherwise use default value, but do not update default value
#define UPDATE_DEFAULT 0 /* use value if supplied, and update default with new value. 
				 Use default if value not supplied */


// auto timerefs
#define AUTO_TIMEREFS 1
#define NO_AUTO_TIMEREFS 0

#include "info_blocks.h"
STD_PULSE_INFO std_pulse_info;
PULSE_INFO pulse_info;
TRANS_INFO trans_info;
PATTERN_INFO pattern_info;
DELAY_INFO delay_info;
BEGIN_LOOP_INFO begin_loop_info;
END_LOOP_INFO end_loop_info;
EV_STEP_INFO ev_step_info;
EV_SET_INFO ev_set_info;
TIME_REF_INFO time_ref_info;
RF_SWEEP_INFO rf_sweep_info;
RF_FM_SWEEP_INFO rf_fm_sweep_info;
RF_BURST_INFO rf_burst_info;
RF_SWIFT_AWG_INFO rf_swift_awg_info;


static BOOL ev_step_open_flag[NUM_AWG_MODULES]; // per AWG unit
static INT ev_sindex;
static INT num_awg_units=0; // number of awg units in use

#endif
