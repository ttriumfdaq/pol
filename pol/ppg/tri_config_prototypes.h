/* function prototypes 
   in tri_config.h
*/
#ifndef  TRI_PROTOTYPES_H
#define  TRI_PROTOTYPES_H

INT get_a_block(HNDLE hKey,INT block_index,INT total_size,KEY *key, INT counter_index,
		INT max_blocks, char * p_struct_str) ;

INT get_a_block_without_params(HNDLE hKey, INT block_index,
		   INT total_size, KEY *key, INT counter_index,INT max_blocks) ;
INT file_wait(char *path, char *file);
INT tr_precheck(INT run_number, char *error);

INT settings_rec_get(void);
INT tr_prestart(INT run_number, char *error);
INT get_input_data( HNDLE hKey);
INT print_key(HNDLE hDB, HNDLE hKey, KEY * key, INT level, void *info);
INT get_block(HNDLE hKey, KEY *key  );
INT print_key_info1(HNDLE hDB, HNDLE hKey, KEY * pkey, INT level, void *info);
void init_blocks(void);
void check_params(char *ppg_mode);
INT file_wait(char *path, char *file);
void define_t0_ref(void);
INT define_trans0(void);
int main(unsigned int argc,char **argv);
void  init_default_param_values(void);
void print_some_info(void);
void set_awg_params(void);
#ifdef EBIT
void output_loops(void);
#endif
INT replace_trans0(char *name, DWORD bitpat);
/* more function prototypes */

#include "sort_transitions.h"
#include "get_params.h"
#include "print_blocks.h"
#include "compute.h"
#include "ev.h"
#include "awg_prototypes.h"
#include "plot.h"
#endif
