/* get_a_block.c 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
 
INT get_a_block(HNDLE hKey, 
		INT block_index,  // index to this block
		INT total_size,  // record size
		KEY *key, 
		INT counter_index, // e.g. RF_GATE
		INT max_blocks,    // e.g. MAX_RF_BLOCKS
		char * p_struct_str // e.g. RF_GATE_STR
		)
{
  /* No longer creates record  */

  INT size,status,i; 
  
  block_counter[counter_index]++;  /* count number of this type of block */
  if(block_counter[counter_index] >  max_blocks)
    {
      cm_msg(MERROR,"get_a_block", "too many %s blocks; maximum allowed = %d",
	     key->name, max_blocks);
      return DB_INVALID_PARAM;
    }

  block_list[block_index] = ((1<<counter_index) << TYPE_SHIFT) | block_counter[counter_index];

  if(debug)
    {
      printf("get_a_block: working on %s block %d\n",key->name, block_counter[counter_index]);
  
      printf("block_index=%d  counter_index=%d  \n",block_index, counter_index);

      //printf(" ((1<<counter_index) << TYPE_SHIFT)=%d\n", ((1<<counter_index) << TYPE_SHIFT) );
    
      printf("get_a_block:%s block is total block number %d, block number %d, block_list[%d]=0x%x \n",
	     key->name, block_index, block_counter[counter_index], block_index, block_list[block_index]);
  
      printf("get_a_block: record size is %d\n",total_size);
    }


  return DB_SUCCESS;
}


INT get_a_block_without_params(HNDLE hKey, 
		   INT block_index,  // index to this block
		   INT total_size,  // record size
		   KEY *key, 
		   INT counter_index, // e.g. RF_GATE
		   INT max_blocks)    // e.g. MAX_RF_BLOCKS
		   
{
  
  
  INT size,status,i; 
  
  block_counter[counter_index]++;  /* count number of this type of block */
  if(block_counter[counter_index] >  max_blocks)
    {
      cm_msg(MERROR,"get_a_block_without_params", "too many %s blocks; maximum allowed = %d",
	     key->name, max_blocks);
      return DB_INVALID_PARAM;
    }
  else
    if(debug)
      printf("get_a_block_without_params: working on %s block %d\n",key->name, block_counter[counter_index]);
  
  
  block_list[block_index] = ((1<<counter_index) << TYPE_SHIFT) | block_counter[counter_index];
  if(debug)printf("get_a_block_without_params:%s block is total block number %d, block number %d, block_list[%d]=0x%x \n",
		  key->name, block_index, block_counter[counter_index], block_index, block_list[block_index]);
  
  if(debug)printf("get_a_block_without_params: record size is %d; expected size is 0\n",total_size);
  if (total_size != 0)
    {
      printf("get_a_block_without_params: error  %s block should have no parameters \n", key->name );
      printf("                            ignoring any parameters supplied\n");
      
      
    }
  return DB_SUCCESS;
}

