/*
  Name:  plot.c
  Created by: SD

  Contents: routines to plot the expected PPG and AWG output 
  $Id: plot.c,v 1.1 2011/04/15 18:50:31 suz Exp $


*/

#ifdef PLOT

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
typedef struct {
  char line[256];
  double offset;
} PLOTLINES;

PLOTLINES plotlines[20];
int iline=0;
BOOL open_loop=FALSE;
 


extern TITAN_ACQ_SETTINGS settings;
extern TRANSITIONS *ptrans;
extern INT run_number;
extern char data_dir[256];
extern INT gbl_awg_sample_counter[ NUM_AWG_MODULES]; 


INT plot_line(FILE *plotf, double offset, DWORD bitpattern, char *blockname,  INT *bitused)
{
  char string[256],str[128],str2[80];
  int bit,shift;
  int my_bit=0;
 
  int j,len,l;


 
  sprintf(str,"%s"," ");

  for(bit=0; bit<NUM_BITS+ gbl_num_loops_defined-1; bit++)
    { 
      if (bitused[bit])
	{
	  shift=1<<bit;
	  my_bit = bitpattern & shift;
	  if(my_bit)
	    strcat(str," 1");
	  else
	    strcat(str," 0");
	}
    }
  sprintf(str2,"  # %s\n",blockname);
  strcat(str,str2);
  str[strlen(str)+1]='\0';

  sprintf(string,"%-10.4f ",offset);
  strcat(string,str);


  printf("%s",string);
  fputs(string,plotf);

  if(open_loop)
    {   
      //printf("open loop... iline=%d; saving line \"%s\" offset %f\n",iline,str,offset);
      /* save the lines from the loop so they can be repeated */
      sprintf(plotlines[iline].line,"%s",str); // without the offset value
      plotlines[iline].offset=offset;
      iline++;
      if(iline>=20)
	{
	  printf("too many lines in loop to save; setting iline to 0\n");
	  open_loop  = FALSE;
	  iline=0;
	  return SUCCESS;
	}
    }

  return SUCCESS;
}

INT check_used_bits(TRANSITIONS *ptrans,  INT *pbitused, char *namestring)
{
  INT i,j,bit;
  DWORD shift, bitpattern=0;
  char str[25];
  INT num_signals;

  num_signals=0;
  /* write the plotfile, missing out bits that are never changed */
  for(j=0; j<gbl_num_transitions; j++)
    {
      //      printf("bitpattern = 0x%x  bitpat[%d]=0x%x\n",bitpattern, j,tdelays[j].bitpat );
      bitpattern = (bitpattern | ptrans[j].bitpattern);
    }
  printf("Final bitpattern = 0x%x\n",bitpattern);
  /* any zeroes in bitpattern are not programmed */
  
  for (bit=0; bit<NUM_BITS; bit++)
    {
      shift=1<<bit;
      pbitused[bit] = bitpattern & shift;
      if(!pbitused[bit])
	printf("bit %d (%s) is not changed; will not be plotted\n",bit,bit_names[bit]);
      else
	{
	  num_signals++;
	  strcat(namestring," ");
	  sprintf(str,"%s(ch%d)",ppg_signal_names[bit],(16-bit));
	  strcat(namestring,str);
	}
    }

  /* add plots for loops; Scan Loop is  plotted as if its count is 1 */
  printf("num_signals=%d; %d loops will be plotted\n",num_signals,gbl_num_loops_defined-1);
  i=0;  
  for ( j=NUM_BITS; j < NUM_BITS + gbl_num_loops_defined-1 ; j++)
    {
      pbitused[j]=1;
      num_signals++;
      printf("loop %d (loopname=%s) is present\n",i,loop_params[i].loopname);
      strcat(namestring," ");   
      strcat(namestring,loop_params[i].loopname);
      i++;
    } 
  strcat(namestring,"\n");
  printf("check_used_bits: returning with num_signals=%d, namestring=%s\n",
	 num_signals,namestring);
  return num_signals;
}






INT plot_transitions(FILE *plotf, TRANSITIONS *ptrans, double *max_ms, BOOL *plflag, BOOL *pfawg )
{
  INT i,j,l,k,nplot;
  DWORD plot_bitpat=0; 
  DWORD loop_bitpat=0;
  INT bitused[NUM_BITS+MAX_LOOP_BLOCKS];
  char namestring[256];
  char str[10];
  char string[256];
  int loopcnt;
  float one_loop;
  BOOL evstep_loop;

  *plflag = *pfawg = 0; 
  printf("plot_transitions: starting with *pfawg=%d  \n",*pfawg);
  

  sprintf(namestring,"# ");
  nplot=check_used_bits(ptrans, bitused, namestring); // adds on bits for loops
  sprintf(str,"# %d\n",nplot); // number of ppg signals in plot file
  fputs(namestring,plotf);

  i=0;
  for (j=0; j<gbl_num_transitions; j++)
    {
      if( ptrans[j].code == 0)  // transition
	{
	  plot_bitpat = (loop_bitpat | ptrans[j].bitpattern);
	  plot_line(plotf, ptrans[j].t0_offset_ms, plot_bitpat, ptrans[j].block_name  ,bitused);
	  if (j>i)i=j;
	}
      else  if( ptrans[j].code == BEGLOOP_CODE ||ptrans[j].code == RES_CODE )  // begin loop or end of all loops
	{
	 

	  if(        ( strcmp(loop_params[ptrans[j].loop_index].loopname,"SCAN")==0)  
		     && (ptrans[j].code == RES_CODE))
	    printf("plot_transitions: not plotting Reserved area for SCAN loop\n");
	     
	    
	  else
	    { 
	     
	      loop_bitpat = get_pattern(loop_bitpat,(NUM_BITS+ptrans[j].loop_index-1))  ; // begin loop
	      plot_bitpat = loop_bitpat | ptrans[j].bitpattern;
	      plot_line(plotf, ptrans[j].t0_offset_ms, plot_bitpat, ptrans[j].block_name, bitused);
	      if (j>i)i=j;
	      
	      if( ptrans[j].code == BEGLOOP_CODE)		
		{
		  for(k=0; k<block_counter[EV_STEP]; k++)
		    {
		      if(ptrans[j].loop_index == ev_step_params[k].loop_index)
			{
			  evstep_loop = TRUE; ///ev_step_params[k].loop_index  ;  // this is an ev_step loop
			  //  if(open_loop)
			  //{
			  //  cm_msg(MERROR,"plot_transitions","Nested evstep loops are not presently allowed");
			  //  return -1;		     
			  //}
			  printf("Loop index %d : this loop surrounds evstep block %d\n",
				 ptrans[j].loop_index,k);
			}
		    }
		    
			
		}

	      if( ptrans[j].code == BEGLOOP_CODE && !open_loop)
		{
		  if( strcmp(loop_params[ptrans[j].loop_index].loopname,"SCAN")!=0)
		    {
		      loopcnt = loop_params[ptrans[j].loop_index].loop_count;
		      one_loop = loop_params[ptrans[j].loop_index].one_loop_duration_ms;
		      open_loop  = TRUE;
		      printf("open loop... this loop (%d) will be expanded in the plot\n",
			     ptrans[j].loop_index);
		    }
		  else // any nested loops are not expanded (ignoring scan loop)
		    printf("scan loop... this loop (%d) will not be expanded in the plot\n",
			   ptrans[j].loop_index);
		}
	      else
		printf("nested loop... this loop (%d) will not be expanded in the plot\n",
		       ptrans[j].loop_index);
		  

	    }
	    
	}
      else if  (ptrans[j].code ==ENDLOOP_CODE)
	{
	  if(open_loop)
	    { 
	      open_loop = FALSE;
	      if(loopcnt > PLOT_MAX_LOOPS)
		{        // limit the loop count for expansion
		  loopcnt = PLOT_MAX_LOOPS;
		  printf("closing loop; loop will be expanded for plot; loop count limited to %d \n",
			 PLOT_MAX_LOOPS);
		  *plflag=1; // tells the plot we need a warning message
		  if(evstep_loop)
		    {
		      *pfawg=1; // cannot display awg unless loop is expanded
		      printf("**** loop count limited;  cannot display awg unless loop is expanded completely(%d)\n",
			     *pfawg);
		    }
		}
	      else // expanding loop
		printf("closing loop; loop will now be expanded for plot\n");
	    

	      /* loop is recently closed  */
	      // repeat the saved lines for the loop
	      printf("iline=%d loopcnt = %d, one loop duration = %f\n",iline, loopcnt,one_loop);
	      for (k=1; k<loopcnt; k++) // loopcount 
		{ // starts at j=1 as already have first
		  
		  for(l=0; l<iline ; l++)
		    {
		      sprintf(string,"%-10.4f ",(plotlines[l].offset + (k * one_loop)));
		      strcat(string,plotlines[l].line);
		      
		      
		      printf("%s",string);
		      fputs(string,plotf);
		    }
		}
	      printf("End of expansion of loop for plot \n");
	    }
	  else if( strcmp(loop_params[ptrans[j].loop_index].loopname,"SCAN")==0)
	    printf("plot_transitions: end of scan loop detected\n");
	  else
	    printf("plot_transitions: strange...loop index %d is closed already at ENDLOOP\n",
		   ptrans[j].loop_index);
	  iline=0;
	  if(evstep_loop)
	    evstep_loop= FALSE; // evstep loop is now closed
	}
      
    }
  *max_ms= ptrans[i].t0_offset_ms; // max offset 
  return nplot; 
}




void plot_awg(char *filename, INT awg_unit,  double max_ms, double awg_clock_width_ms)
{
  // creates the file for plotting awg data
  char perl_path[80];
  char awg_perlname[]="awg.pl"; // generates plot file for awg
  char cmd[128];
  INT status;

  printf("plot_awg: starting with filename=%s awg_unit=%d max_ms=%f clock_width=%fms\n",
	 filename,awg_unit,max_ms,awg_clock_width_ms);
    if(awg_unit != 0)
    {
      printf("second awg_unit not supported yet\n");
      return;
    }
  
    /* Get the AWG data out of the ppgload/ebit.dat file (using awg.pl)
                  -> generates ppgload/awgN.dat 
    */

  sprintf(perl_path,"%s/%s",settings.ppg.input.ppg_perl_path,awg_perlname);
  /*

   were used by gnuplot_awg
  sprintf(outfile,"%sdata/run%d_awg%d.png",settings.ppg.input.ppg_path,run_number,awg_unit);
  sprintf(datafilename,"%s",awg_params[awg_unit].outfile); 
  */
  // filename e.g. /home/ebit/online/ppgload/ebit.dat
  // awgfilename e.g. "softdac-ch0"

  printf("awg outfile name=%s\n",awg_params[awg_unit].outfile);
  sprintf(cmd,"%s %s %d %f %f %s %s %d %d",
	  perl_path, 
	  filename, 
	  awg_params[awg_unit].sample_counter, 
	  max_ms,  
	  awg_clock_width_ms, 
	  awg_params[awg_unit].outfile,
	  settings.ppg.input.ppg_path, 
	  run_number, 
	  awg_unit );

  printf("plot_awg: sending system command:\"%s\" \n",cmd);
  status =  system(cmd);
  printf("plot_awg: after system command, status=%d\n",status);
  if(status)
    {
    cm_msg(MINFO,"plot_awg","could not assemble awg data for awg plots");
    }

  // /home/ebit/online/ppg/ppgload/awg1.dat or awg0.dat (AWG plot data file) has been created by awg.pl above.

  return;
}

void plot_picture(char *filename, INT nplot, double max_ms,  INT loop_flag, char *ppg_mode)
     /*	 gnuplot.pl 
	 #         filename
	 #         number to plot
	 #         max time range (x axis)	
	 #         flag  (0 = loops truncated, 1= not truncated)
	 #         output filename e.g.  runNN_ebit.png where NN =run number
	 #         run number
     */
{
  char perl_path[128];
  char perl_name[]="gnuplot.pl";
  char outfile[80];
  char cmd[256];
  INT status;
  

  // remove softlink "ppgplot.png"
  sprintf(cmd,"rm -f %sppgplot.png",settings.ppg.input.ppg_path );
  printf("cmd:%s\n",cmd);
  status =  system(cmd);
  if(status)
    cm_msg(MINFO,"plot_picture","could not remove softlink ppgplot.png ");


  sprintf(perl_path,"%s/%s",settings.ppg.input.ppg_perl_path,perl_name);
  sprintf(outfile,"%s/%s/run%d_%s.png",data_dir,"info",run_number,ppg_mode);
  printf("plot_picture: outfile=%s\n",outfile);

  sprintf(cmd,"%s %s %d %f %d %s %d",perl_path, filename, nplot, max_ms, loop_flag, outfile, run_number);
  printf("plot_picture: sending system command:\"%s\" \n",cmd);
  status =  system(cmd);
  
  printf("plot_picture: after system command, status=%d\n",status);
  if(status)
    cm_msg(MINFO,"plot_picture","could not plot picture of ppg cycle");
  else
    {
      printf("outfile=%s; settings.ppg.input.ppg_path=%s\n",outfile,settings.ppg.input.ppg_path);
      sprintf(cmd,"ln -s   %s %sppgplot.png ",outfile,settings.ppg.input.ppg_path);
      printf("sending cmd:%s\n",cmd);
      status =  system(cmd);
      if(status)
      	cm_msg(MINFO,"plot_picture","could not create link \"ppgplot.png\" ");
    }

  /*  else
    {
      sprintf(cmd,"mv %stitan.png %s/run%d_titan.png",settings.ppg.input.ppg_path,data_dir,run_number );
      printf("cmd:%s\n",cmd);
      status =  system(cmd);
      if(status)
	cm_msg(MINFO,"plot_picture","could not rename titan.png ");
      else
	{
	  sprintf(cmd,"ln -s   %s/run%d_titan.png %stitan.png",data_dir,run_number,settings.ppg.input.ppg_path);
	  printf("cmd:%s\n",cmd);
	  status =  system(cmd);
	  if(status)
	    cm_msg(MINFO,"plot_picture","could not create link to titan.png ");
	}
	}*/
  return;
}



void plot_awg_picture(INT awg_unit)
{
  INT status;
  char awg_plot_perlname[]="gnuplot_awg.pl"; // plots the data for each awg channel
  char perl_path[128];
  char cmd[256];
  char awg_names[2][5]={"zero","one"};

  sprintf(perl_path,"%s/%s",settings.ppg.input.ppg_perl_path,awg_plot_perlname);
  sprintf(cmd,"%s %s %sawg/awg_%s.dat %s/%s",perl_path,
	  settings.ppg.input.ppg_path, 
	  settings.ppg.input.ppg_path,
	  awg_names[awg_unit],
	  data_dir,"info");
  printf("plot_awg: sending system command:\"%s\" \n",cmd);
  status =  system(cmd); 
  printf("plot_awg: after system command, status=%d\n",status);
  if(status)
    {
    cm_msg(MINFO,"plot_awg","could not plot AWG data");
    return;
    }
}

#endif

