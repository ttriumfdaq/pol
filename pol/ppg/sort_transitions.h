/* sort_transitions.h */
#ifndef  SORT_TRANS_INCLUDE_H
#define  SORT_TRANS_INCLUDE_H

void sort_transitions(TRANSITIONS *ptrans);
int trans_compare(const void *arg1, const void *arg2) ;
INT precheck_sorted_transitions(TRANSITIONS *ptrans);
INT check_sorted_transitions(TRANSITIONS *ptrans);
INT check_loops_trans(TRANSITIONS *ptrans);
INT combine_transitions(TRANSITIONS *ptrans);
void swap(INT index_a, INT index_b, TRANSITIONS *trans);
INT shift_trans_down (INT index, INT shift_by,  TRANSITIONS *trans);
INT shift_trans_up (INT index, TRANSITIONS *trans);
DWORD get_pattern(DWORD pattern, DWORD bit);
INT check_same(TRANSITIONS *ptrans);
#ifdef AUTO_TDCBLOCK
INT add_tdcblocks( TRANSITIONS *ptrans, TDCBLOCK_PARAMS *ptdcblock);
void sort_tdcblock( TDCBLOCK_PARAMS *ptdcblock);
void tdc_compress(TDCBLOCK_PARAMS *ptdcblock);
INT shift_tdc_up (INT index, TDCBLOCK_PARAMS *ptdcblock);
int tdc_compare(const void *arg1, const void *arg2) ;
#endif //  AUTO_TDCBLOCK
#endif
