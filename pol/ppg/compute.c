/*  
   Name: compute.c
   Created by: SD

   Contents:  Computes list of delays and bitpatterns into files for PPG compiler

   $Id: compute.c,v 1.1 2011/04/15 18:50:31 suz Exp $

*/


/*------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
#include "print_blocks.h"

typedef struct {
  double delay_ms; // delay(ms)
  DWORD  bitpat;  // hold this bit pattern for above delay
  char   delay_name[10];
  DWORD  code; // 0=transition 1=begin_loop 2=end_loop etc.
  INT    loop_index; // if a loop so we can access loop name
  INT    trans_index; // this delay came from this transition
  double t0_offset_ms; // from trans[trans_index]
  char   block_name[COMBINED_BLOCKNAME_LEN+1]; // name of this block (or blocks) 
}TDELAYS;
TDELAYS tdelays[MAX_TRANSITIONS];

#ifndef POL
extern TITAN_ACQ_SETTINGS settings;
#else
extern POL_ACQ_SETTINGS settings;
#endif

extern TRANSITIONS *ptrans;
extern FILE *dfile;
extern INT run_number;
extern char data_dir[256];

/* standard delay names for template file */
  char str_dmin[]="d_min";
  char str_dpulsew[]="d_pulse";
  char str_ddelay[]="d_delay";

INT gbl_num_delays=0;
INT gbl_num_delays_nppg=0;

typedef struct {
  double delay_ms; // delay(ms)
  DWORD  bitpat;  // hold this bit pattern for above delay
  INT    code; // 0=transition 1=begin_loop 2=end_loop etc.
  char   loop_name[10];
  INT    loop_count; // if a loop so we can access loop name
  double t0_offset_ms;
  char   block_name[COMBINED_BLOCKNAME_LEN+1]; // name of this block (or blocks) 
}NDELAYS;
NDELAYS ndelays[MAX_TRANSITIONS];


INT compute(char *ppg_mode)
{
  
  FILE  *inf;
#ifdef BITS_24
  char  str [512]; // NOTE: this has to be very long if we have BITS_24 defined!
#else
  char  str [256]; // 16 bits only defined
#endif
  char  infile_name[256];
  
  FILE  *outf;
  char  outfile_name[256];

  FILE  *plotf;
  char  plotfile_name[256];

  //  FILE  *rulef;
  //char  rulefile_name[256];
  
  
  double d_count; 
  double d_minimal;
  double time_slice;
  double d_pulsew,d_delay;
  double ftmp;
  DWORD n_mcs_less_two; 
  DWORD n_mcs_less_three, n_trap_less_two;
  int i,j,k,l,m;
  char comment[30];  
  BOOL lastloop;
  DWORD count,n_steps;
  INT status;
  //  INT bitused[NUM_BITS+MAX_LOOP_BLOCKS];
  INT nplot;
  BOOL loop_flag=0; // if true, tells plot to print out msg about truncated loops  
  BOOL awg_flag=0; // if true, do not plot awg because loops are not fully expanded
  double max_ms;
  char ppgload_path[128];
  char ppg_path[128];
  float conversion_factor;
  float ppg_clock;

  if(debug)printf("compute is starting with ppg mode %s\n",ppg_mode);
  
  
  /* Compute necessary values */
  
#ifndef POL
  time_slice       = 	settings.ppg.output.time_slice__ms_;
  d_minimal        = 	settings.ppg.output.minimal_delay__ms_;
  d_pulsew         =    settings.ppg.input.standard_pulse_width__ms_;/* all standard pulses  have a fixed width of d_pulsew */
  strcpy(ppgload_path,  settings.ppg.input.ppgload_path);
  strcpy(ppg_path,      settings.ppg.input.ppg_path);
  conversion_factor =   settings.ppg.output.ppg_freq_conversion_factor;
  ppg_clock         =   settings.ppg.input.ppg_clock__mhz_;
#else
  time_slice       = 	settings.output.time_slice__ms_;
  d_minimal        = 	settings.output.minimal_delay__ms_;
  d_pulsew         =    settings.input.standard_pulse_width__ms_;  /* all standard pulses  have a fixed width of d_pulsew */
  //strcpy(ppgload_path,  settings.input.ppgload_path);
  sprintf(ppgload_path,"%sppgload/",  settings.input.ppg_path);
  strcpy(ppg_path,      settings.input.ppg_path);
  conversion_factor =   settings.output.ppg_freq_conversion_factor;
  ppg_clock         =   settings.input.ppg_clock__mhz_;
#endif
  /* time slice is only used in the compare at present  */
  if(time_slice == 0)
    {
      cm_msg(MERROR,"compute","time slice cannot be zero");
      return DB_INVALID_PARAM;
    }
  
  
  /*--------------------------------------------------------------------------------*/
  
  if(d_pulsew < d_minimal) 
    {
      printf("compute: Error - standard pulsewidth (%10.4f) is less than minimal value of %10.4f\n",
	     d_pulsew,d_minimal);
      cm_msg(MERROR,"compute","Error - standard pulsewidth (%10.4fms) must be at least %10.4fms\n",
	     d_pulsew,d_minimal);
      return -1;
    }
  

  /* Open the header template file */
  printf("ppgload path (for output files): %s \n",ppgload_path);
  printf("input ppg path: %s \n",ppg_path); // this is now set to ~titan/online

  sprintf(infile_name,"%s%s",ppg_path,"/ppg-templates/header.ppg");
 
  if(debug)printf("compute: about to open header template file: %s\n",infile_name);
  
  inf = fopen(infile_name, "r");
  if(! inf)
    {
      cm_msg(MERROR,"compute","error opening header template file %s", infile_name);
      return -1;
    }
  else
    printf("compute: successfully opened header template file: %s\n",infile_name);
  
  /* Open the output file (<expt_name>.ppg) */
  sprintf(outfile_name,"%s%s%s",ppgload_path,ppg_mode,".ppg");
  
  printf("compute: about to open output file: %s\n",outfile_name);
  
  outf = fopen(outfile_name, "w");
  
  if(! outf)
    {
      cm_msg(MERROR,"compute","error opening output file %s", outfile_name);
      if(inf) fclose(inf);
      return -1;
    }

  // copy all lines 
  while (fgets(str, 128, inf) != NULL)
    {
      strcat(str,"\n");
      str[strlen(str)-1] = '\0';
      fputs(str,outf);
    }

  fclose (inf);

  /* if the PPG is clocked at a different frequency to the nominal frequency (i.e. the frequency
in the header file header.ppg (  PPG_CLOCK_MHZ = 10MHz currently), a conversion factor (calculated in
tri_config.c) is used for the delays, so that the signals from the PPG have the correct timing 

e.g. nominal freq = 10MHz         nominal time slice =  5 clock periods  = 5/10**7 * 10**3 ms  
     actual clock freq (external clock) 20MHz  -> actual time slice = 0.5 * nominal time slice
     conversion factor = 2  (10Hz -> 20MHz).

  */

  printf("compute.c: frequency conversion factor = %f\n",  conversion_factor);

  if( conversion_factor != 1)
    sprintf(str,
	    "// Frequency conversion factor = %.2f (between nominal frequency (%.0fMHz) and actual PPG clock frequency (%.3fMHz)\n ", 
	    conversion_factor,
	    (float)PPG_CLOCK_MHZ,
	    ppg_clock);

  /*printf("settings.ppg.input.ppg_clock__mhz_=%f PPG_CLOCK_MHZ=%d\n",
	 settings.ppg.input.ppg_clock__mhz_, PPG_CLOCK_MHZ);
  */

  fputs(str,outf);

  //printf("str=\"%s\"\n",str);

  sprintf(comment,"\n"); 
  if(conversion_factor != 1)
    sprintf(comment," // (true=%.4f%s", d_minimal,"ms) \n");



  /* write the common delays */

       // minimal delay
  sprintf(str,"  %s=%10.4f%s%s",
	  str_dmin,
	  d_minimal * conversion_factor, 
	  "ms;",
	  comment); // true delay
  fputs(str,outf);


     // standard pulsewidth

  if(conversion_factor != 1)
    sprintf(comment," // (true=%.4f%s", d_pulsew,"ms) \n");

  sprintf(str,"  %s=%10.4f%s%s",
	  str_dpulsew,
	  d_pulsew * conversion_factor, 
	  "ms;",
	  comment); 
  fputs(str,outf);

#ifdef PLOT
  /* Open the output plot file */
  sprintf(plotfile_name,"%s%s%s",settings.ppg.input.ppgload_path,ppg_mode,".dat");
  
  printf("compute: about to open output plot file: %s\n",plotfile_name);
  
  plotf = fopen(plotfile_name, "w");
  
  if(! plotf)
    {
      cm_msg(MERROR,"compute","error opening output plot file %s", plotfile_name);
      if(outf)fclose(outf);
      return -1;
    }
  // if loop_flag is TRUE, plot has limited the number of loops shown
  // if awg_flag is TRUE, evstep loop(s) are not expanded; cannot plot awg
  awg_flag =0;
  nplot = plot_transitions(plotf, ptrans, &max_ms, &loop_flag , &awg_flag);
  printf("after plot_transitions, loop_flag=%d awg_flag=%d\n",loop_flag,awg_flag);
  fclose (plotf);
  plot_picture(plotfile_name,  nplot,  max_ms, loop_flag, ppg_mode);
#endif  // PLOT

  /* if there are EV blocks, generate AWG pictures */
  if ((block_counter[EV_SET] > 0) ||  (block_counter[EV_STEP] > 0))
    {
      print_awg_params();

#ifdef PLOT
      if (!awg_flag) // awg_flag is true if awg loop not expanded
	{
	  for (i=0; i< NUM_AWG_MODULES; i++)
	    {
	      if(awg_params[i].in_use && awg_params[i].plot )
		{
		  plot_awg(plotfile_name, i , max_ms, settings.ppg.input.awg_clock_width__ms_ ); // unit i
		  plot_awg_picture(i);
		}
	    }
	}
      else
	printf("compute: INFO -cannot display awg plots since ev_step loops have not been fully expanded (%d)\n",
	       awg_flag);
#endif  // PLOT
    }
#ifdef PLOT
  else
    {
      printf("compute: no AWG blocks found; no AWG plots will be made \n");
      cm_msg(MINFO,"compute","no AWG blocks are defined; removing softlink to AWG plots");
      status = system ("rm -f awg_*.png");
      printf("compute: after system command, status=%d\n",status);
    }
#endif


  // Now calculate the delays
  status = process_trans(ptrans, outf, conversion_factor,d_pulsew, d_minimal  );
  
  if(status != SUCCESS)
    {
      if(outf)fclose (outf);
      return status;
    }
  fputs("",outf);

  /* ======================================================================
     Now write the PPG bit assignments 
     ======================================================================*/

  bit_assignment(outf); // write bit assignments to the file

  if(debug)printf("compute: success from bit_assignment\n");

 
  /*-----------------------------------------------------------------
    Now go through the transitions again, this time assembling the 
    bit pattern and loops

    Delay structure contains times for which this bitpattern must be held
    ----------------------------------------------------------------------*/
  sprintf(str,"");
  for (j=0; j<gbl_num_delays; j++)
    {
      printf("Working on delay %d, code:%d delay_ms:%f",
	     j,tdelays[j].code,tdelays[j].delay_ms);
      
      printf("... bitpattern:0x%x\n", tdelays[j].bitpat);
     

      if( tdelays[j].code == TRANS_CODE)  // transition
	{	  
	  if(tdelays[j].delay_ms != 0)  
	    {   
	      //print a line in the file to hold previous bitpattern for this time
#ifdef BITS_24
	      sprintf(str,"//Block(s) %s;  Bitpattern = 0x%6.6x; True Delay=%10.4fms; T0_offset= %10.4fms; //\n",
		      tdelays[j].block_name ,tdelays[j].bitpat, tdelays[j].delay_ms, tdelays[j].t0_offset_ms);
#else
	      sprintf(str,"//Block(s) %s;  Bitpattern = 0x%4.4x; True Delay=%10.4fms; T0_offset= %10.4fms; //\n",
		      tdelays[j].block_name ,tdelays[j].bitpat, tdelays[j].delay_ms, tdelays[j].t0_offset_ms);
#endif
	      fputs(str,outf);
	      
	      
	      sprintf(str," %-15s   ",tdelays[j].delay_name);
	      assemble_line(str,tdelays[j].bitpat);
	      fputs(str,outf);
	    }
	  else	    
	    {
	      // zero delay is a spacer; does not produce a line in the output file
	    
	      printf("zero delay -> a spacer ...holding bitpattern:0x%x\n", tdelays[j].bitpat);
	    }

	}
      else if  (tdelays[j].code == BEGLOOP_CODE)  // begin loop
	{

	  if (  tdelays[j].delay_ms > 0)
	    {

	      // put in a delay before the begin_loop
	      // &  hold bitpattern for this time

#ifdef BITS_24          
	      sprintf(str,"//Block(s) %s;  Bitpattern = 0x%6.6x; True Delay=%10.4fms; T0_offset= %10.4fms; //\n",
		      tdelays[j].block_name ,tdelays[j].bitpat, tdelays[j].delay_ms, tdelays[j].t0_offset_ms);
#else
	      sprintf(str,"//Block(s) %s;  Bitpattern = 0x%4.4x; True Delay=%10.4fms; T0_offset= %10.4fms; //\n",
		      tdelays[j].block_name ,tdelays[j].bitpat, tdelays[j].delay_ms, tdelays[j].t0_offset_ms);
#endif
	      fputs(str,outf);
	      
	      
	      sprintf(str," %-15s   ",tdelays[j].delay_name);
	      assemble_line(str,tdelays[j].bitpat);
	      fputs(str,outf);
	      
	    } 
	  // then put in the loop command
	  sprintf(str,"//Block(s) %s;   Loop index=%d; //\n",
		  tdelays[j].block_name , 
		  tdelays[j].loop_index  );
	  fputs(str,outf);
	  
	  
	  i= tdelays[j].loop_index;
	  sprintf(str,"Loop    %s %d;\n",loop_params[i].loopname,loop_params[i].loop_count -1);
	  fputs(str,outf);
	}
      
      else if  (tdelays[j].code == ENDLOOP_CODE)  // end loop
	{

	  // may be zero delay prior to end loop 
	  if (  tdelays[j].delay_ms ==0)
	    printf("compute: Found a zero delay at j=%d prior to end_loop; this is a spacer\n",j);
	  
	  else
	    {
              printf("compute: Found a non-zero delay at j=%d prior to end_loop\n",j);
	      // put in a delay before the end_loop
	      // &  hold previous bitpattern for this time
#ifdef BITS_24
	      sprintf(str,"//Block(s) %s;  Bitpattern = 0x%6.6x; True Delay=%10.4fms; T0_offset= %10.4fms; //\n",
		      tdelays[j].block_name ,tdelays[j].bitpat, tdelays[j].delay_ms, tdelays[j].t0_offset_ms);
#else
	      sprintf(str,"//Block(s) %s;  Bitpattern = 0x%4.4x; True Delay=%10.4fms; T0_offset= %10.4fms; //\n",
		      tdelays[j].block_name ,tdelays[j].bitpat, tdelays[j].delay_ms, tdelays[j].t0_offset_ms);

#endif
	      fputs(str,outf);
	      printf("delay name:%s\n", tdelays[j].delay_name);
	      
	      sprintf(str," %-15s   ",tdelays[j].delay_name);
	      assemble_line(str,tdelays[j].bitpat);
	      fputs(str,outf);
	      
	    }
	  // now the end_loop itself
	  
	  sprintf(str,"//Block(s) %s;   Loop index=%d; //\n",
		  tdelays[j].block_name ,
		  tdelays[j].loop_index  );
	  fputs(str,outf);
	  
	  i= tdelays[j].loop_index;
	  sprintf(str,"End Loop %s;\n",loop_params[i].loopname);
	  fputs(str,outf);
	  
	  
	}
      else
	{
	  cm_msg(MERROR,"compute","Unexpected code value %d", tdelays[j].code);
	  if(outf)fclose(outf);
	  return DB_INVALID_PARAM;
	}
      
    }
  if(outf)fclose(outf);
  if(debug)printf("compute: closed %s\n",outfile_name);

 return SUCCESS;
  
}

	  





/*---------------------------------------------------------------
  assemble the next line for the ppg output file
  -----------------------------------------------------------------*/
 
void assemble_line(char *string, DWORD bitpat)
{
INT i;
  for (i=0; i<NUM_BITS; i++)
    {
      strcat(string,bit_names[i]);
      switch ( (bitpat>>i) & 1)
	{
	case 0:
	  strcat(string,"off + ");
	  break;
	case 1:
	  strcat(string,"on + ");
	  break;
	}
    }
  string[strlen(string)-3]='\0';
  strcat(string,";\n");
  return; 
}


void bit_assignment( FILE  *outf)
{

  int i,bitcount;
  char str[256];
  char off[]="= 0,1;";  
  char on[]= "= 1,1;   // bit ";
  char tmp[20];
 
  sprintf(str,"%s","\n//*******************************************************************************//\n");

  fputs(str,outf);
  sprintf(str,"%s","// Bit patterns ATTENTION!!: The bits are addressed from high to low\n");
 fputs(str,outf);
  sprintf(str,"%s","// the position in the sum gives the number of the bit affected.\n\n");
  fputs(str,outf);
  bitcount=0;

#ifdef AUTO_TDCBLOCK
  printf("AUTO_TDCBLOCK IS defined\n");
#else
  printf("AUTO_TDCBLOCK IS NOT defined\n");
#endif
#ifdef BITS_24
  printf("BITS_24 is defined\n");

  /* write these in ascending channel order */
  for(i=NUM_BITS-1; i>=0; i--)
    {
      bitcount++;
      printf("index=%d bit_names=%s; ppg_signal_names=%s\n",i,bit_names[i],ppg_signal_names[i]);
      sprintf(tmp,"%son",bit_names[i]);
      sprintf(str,"%-15s%s%d\n",tmp,on,bitcount);
      fputs(str,outf);
      sprintf(tmp,"%soff",bit_names[i]);
      sprintf(str,"%-15s%s\n", tmp,off);
      fputs(str,outf);
    }

#else
  // 16 bits only are implemented
  /* write these in ascending channel order */
  for(i=NUM_BITS-1; i>=0; i--)
    {
      bitcount++;
      sprintf(tmp,"%son",bit_names[i]);
      sprintf(str,"%-15s%s%d\n",tmp,on,bitcount);
      fputs(str,outf);
      sprintf(tmp,"%soff",bit_names[i]);
      sprintf(str,"%-15s%s\n", tmp,off);
      fputs(str,outf);
    }
  sprintf(str,"%s","f_dummy    = 0,9;   // mask for channel 16 - 24 FSC memory select channels\n");
  fputs(str,outf);
#endif // ifdef BITS_24
  sprintf(str,"%s","\n//*******************************************************************************//\n");
  fputs(str,outf);

  sprintf(str,"%s","\n// Program //--------------------------------------------------------------------//\n");
  fputs(str,outf);
  return;
}







INT process_trans(TRANSITIONS *ptrans , FILE *outf, float conversion_factor,  double d_pulsew, double d_minimal  )
{
  // This will calculate the delays 
  INT i,j,k,ic,ndelay,len;
  DWORD my_bitpat;
  double prev_time,my_delay, beg_delay;
  INT dncount;

  gbl_num_delays = 0; 
  dncount=0;

  //  settings.ppg.output.ppg_freq_conversion_factor is frequency conversion factor

  if(debug)
    printf("process_trans: starting with gbl_num_transitions=%d\n",gbl_num_transitions);

  /* the first transition should be at T0 (either supplied automatically or by user)*/
  if (ptrans[0].t0_offset_ms != 0.0)
    {
      cm_msg(MERROR,"process_trans", "strange....found no T0 transition");
      return DB_INVALID_PARAM;
    }

  i=1;
  prev_time = 0.0; // T0
  while (i < gbl_num_transitions)
    {
      if(debug)
	printf("process_trans: working on transition number %d trans code=%d :\n",i,ptrans[i].code);
      my_delay = ptrans[i].t0_offset_ms - prev_time;
      //if(debug)
      printf(" process_trans: trans=%d prev_time = %f  my_delay = %f prev bitpattern = 0x%x \n",
	     i,prev_time, 
	     my_delay,ptrans[i-1].bitpattern);


      ic = compare(my_delay, d_minimal ); /* check for minimal delay */
      if(ic <=0)
	printf("process_trans: transition %d and %d are within minimum delay\n",i,i-1);

      if(ptrans[i].code == RES_CODE)
	{ // have already checked so this cannot be the first transition
	  // prev_time set to  ptrans[i].t0_offset_ms below 
	  printf("process_trans: trans %d - reserved block %s disappears; taken care of by loop\n", 
		 i,ptrans[i].block_name);
	}
      
      else if  (ptrans[i].code == BEGLOOP_CODE ) // loop
	{	
	  if(ic>0)
	    {
	      if(debug)
		printf("process_trans: found begin loop at i=%d ; delay between begin_loop and previous trans = %f, (ic=%d)  \n",i,my_delay,ic);
	    }
	  else
	    {
	      //if(debug)
	      {
		//  printf("process_trans: found begin loop at i=%d ; delay between begin_loop and previous trans = %f, (ic=%d)  \n",i,my_delay,ic);
		printf("process_trans: trans %d begin_loop... < min. delay, setting my_delay to 0 \n",i);
	      }
	      my_delay = 0;  // less than minimal delay
	    }
	  
	  tdelays[gbl_num_delays].loop_index =  ptrans[i].loop_index;
	  tdelays[gbl_num_delays].delay_ms=my_delay;
	  tdelays[gbl_num_delays].bitpat = ptrans[i-1].bitpattern; 
	  tdelays[gbl_num_delays].code = ptrans[i].code;
	  tdelays[gbl_num_delays].trans_index = i;
	  tdelays[gbl_num_delays].t0_offset_ms =  ptrans[i].t0_offset_ms;
	  strncpy(tdelays[gbl_num_delays].block_name,  ptrans[i].block_name,COMBINED_BLOCKNAME_LEN );
          if(my_delay > 0)
	    get_delay_name(my_delay,gbl_num_delays,&dncount,outf, conversion_factor, d_pulsew , d_minimal);  // fills tdelays[ndelay].delay_name
	  if(debug)
	    {
	      printf("process_trans: tdelays[%d].delay_ms = %f t0_offset=%f bitpat=0x%x orig=0x%x\n",
		     gbl_num_delays,	     tdelays[gbl_num_delays].delay_ms,   
		     tdelays[gbl_num_delays].t0_offset_ms,
		     tdelays[gbl_num_delays].bitpat, ptrans[i].bitpattern);
	    }

	  gbl_num_delays++;
	 
	}
    

      else if  (ptrans[i].code == ENDLOOP_CODE ) // end loop
	{   // have already checked so this cannot be the first transition
	  if (ic >0 )
	    {
	      printf("process_trans: trans %d  delay between endloop and previous trans is %f; blocks %s and %s \n",
		     i,my_delay,
		     ptrans[i].block_name,  ptrans[i-1].block_name);	     
	    }
	  else
	    my_delay = 0;  // less than minimal delay

	  tdelays[gbl_num_delays].loop_index = ptrans[i].loop_index;
	  tdelays[gbl_num_delays].delay_ms=my_delay;
	  tdelays[gbl_num_delays].bitpat = ptrans[i-1].bitpattern; // hold this bitpat for this delay
	  tdelays[gbl_num_delays].code = ptrans[i].code;
	  tdelays[gbl_num_delays].trans_index = i;
	  tdelays[gbl_num_delays].t0_offset_ms =  ptrans[i].t0_offset_ms;
	  strncpy(tdelays[gbl_num_delays].block_name,  ptrans[i].block_name,COMBINED_BLOCKNAME_LEN );
	  if(my_delay > 0)
	    get_delay_name(my_delay,gbl_num_delays,&dncount,outf, conversion_factor, d_pulsew, d_minimal );  // fills tdelays[ndelay].delay_name
	  if(debug)
	    printf("process_trans: tdelays[%d].delay_ms = %f t0_offset=%f bitpat=0x%x orig=0x%x\n",
		   gbl_num_delays,	     tdelays[gbl_num_delays].delay_ms,   
		   tdelays[gbl_num_delays].t0_offset_ms,
		   tdelays[gbl_num_delays].bitpat, ptrans[i].bitpattern);
	  gbl_num_delays++;
	}	  
      
      
      
      
      
      else if (ptrans[i].code == TRANS_CODE) // transition
	{
	
	  if(ic < 0)
	    {
	      /*  within minimal delay or equal to previous transition.... should only occur after 
		  a begin_loop or res_code
		  since any other transitions should have been combined */
	      
	      if (ptrans[i-1].code == TRANS_CODE)
		{
		  cm_msg(MERROR,"process_trans","expect >= min delay between block %s and previous %s (index %d,%d)",
			 ptrans[i].block_name,  ptrans[i-1].block_name, i,i-1);
		  return DB_INVALID_PARAM;
		}
	    }
	    

	  tdelays[gbl_num_delays].delay_ms=my_delay;
	  printf("trans %d hold previous bitpattern 0x%x for %fms \n",i,ptrans[i-1].bitpattern,my_delay);
	  tdelays[gbl_num_delays].bitpat = ptrans[i-1].bitpattern; 
	  tdelays[gbl_num_delays].code = ptrans[i].code;
	  tdelays[gbl_num_delays].trans_index = i;
	  if(my_delay > 0)
	    get_delay_name(my_delay,gbl_num_delays,&dncount,outf, conversion_factor, d_pulsew, d_minimal );  // fills tdelays[ndelay].delay_name
	

	  tdelays[gbl_num_delays].t0_offset_ms =  ptrans[i].t0_offset_ms;
	  strncpy(tdelays[gbl_num_delays].block_name,  ptrans[i].block_name, COMBINED_BLOCKNAME_LEN );
	  if(debug)
	    printf("process_trans: tdelays[%d].delay_ms = %f t0_offset=%f bitpat=0x%x orig=0x%x\n",
		   gbl_num_delays,	     tdelays[gbl_num_delays].delay_ms,   
		   tdelays[gbl_num_delays].t0_offset_ms,
		   tdelays[gbl_num_delays].bitpat, ptrans[i].bitpattern);
	  gbl_num_delays++;
   
	      
	}
    
    next:
      prev_time =  ptrans[i].t0_offset_ms; // this transaction
      //if(debug)
	printf("process_trans: at next, trans i=%d  prev_time is now set to %f\n",i,prev_time);
      
      i++;
      
    }
  print_delays(stdout);

  /* now make absolutely sure that begin/end loops are separated by at least one transition 
     compiler does not allow consecutive begin/end loops and compiler error messages are not too good
  */
  for(j=0; j<gbl_num_delays-1; j++)
    { 
      if(tdelays[j].code > 0 && tdelays[j+1].code>0  ) // two loops
	{
	  if(compare( tdelays[j].delay_ms, d_minimal ) <=0 ) /* check for minimal delay */
	    {
	      cm_msg(MERROR,"process_trans","two begin or end loops MUST be separated by at least a minimal delay (blocks %s and %s)", tdelays[j].block_name, tdelays[j+1].block_name);
	      return DB_INVALID_PARAM;
	    }
	}
    }
  return DB_SUCCESS;

  
}

  



DWORD backpattern(DWORD bitpat)
{
  INT i,j;
  DWORD backpat;

#ifdef BITS_24
  INT max=23;
#else
  INT max=15;
#endif
  j=max;

  backpat=0;
  for(i=0;i<=max;i++)
    {
      if(bitpat & 1<<i)
	backpat = backpat | 1<<j;
      j--;
    }	    
  return backpat;
}

void get_delay_name(double my_delay, INT ndelay, INT *namecount, FILE *outf,  float conversion_factor, double d_pulsew , double d_minimal )
{
  /* find a unique name for each delay */
  INT i;
  char str[128];
  char comment[50];
  //  char s[3]="_a";

  printf("get_delay_name: ndelay=%d namecount=%d\n",ndelay,*namecount);
  if( compare(my_delay,d_minimal ) == 0)
    {
      strcpy(tdelays[ndelay].delay_name,str_dmin );
      return;
    }

  if( compare(my_delay,  d_pulsew  ) == 0)
    {
      strcpy(tdelays[ndelay].delay_name, str_dpulsew);
      return;
    }

  /* try to minimize number of different delays needed */
  for (i=0; i<ndelay; i++)
    {
      if( compare(my_delay, tdelays[i].delay_ms) == 0)
	{
	  strcpy(tdelays[ndelay].delay_name, tdelays[i].delay_name);
	  return;
	}
    }
  /* was using letters... go with numbers instead in case more than 26 delays 
   s[1]=s[1] + *namecount; // next letter
  sprintf(tdelays[ndelay].delay_name, "%s%s",str_ddelay,s);
  *namecount= *namecount+1;
  */

  sprintf(tdelays[ndelay].delay_name, "%s_%d",str_ddelay,*namecount);
  *namecount= *namecount+1;

  sprintf(comment,"\n"); 
  if(conversion_factor != 1)
    //  sprintf(comment," // (true=%.4f%s",tdelays[ndelay],"ms) \n");
  sprintf(comment," // (true=%.4fms) \n",tdelays[ndelay]);
  printf("comment=\"%s\"\n",comment);
  
  // write this delay into the output file
  sprintf(str,"  %s=%.4f%s%s",
	  tdelays[ndelay].delay_name, 
	  tdelays[ndelay].delay_ms*conversion_factor, 
	  "ms;",
	  comment); // true delay

  printf("str=\"%s\"\n",str);
  fputs(str,outf);  
}

int compare(double my_delay, double d_delay )
{

  double diff,time_slice,d_minimal;
#ifndef POL
  time_slice       = 	settings.ppg.output.time_slice__ms_;
  d_minimal        = 	settings.ppg.output.minimal_delay__ms_;
#else
   d_minimal        = 	settings.output.minimal_delay__ms_;
   time_slice       =   settings.output.time_slice__ms_;
#endif

   if(debug) printf("\ncompare:  Comparing %.10f and %.10f\n",my_delay,d_delay);
  diff = fabs (my_delay - d_delay);
  if(debug) printf("   diff = %.10f   min delay = %.10f\n",diff,d_minimal);

  if (diff > d_minimal) // larger than minimal delay
    {  // difference between the values is large; simply compare 
      if(my_delay < d_delay)
	return -1;
      else if (my_delay > d_delay)
	return 1;
      else
	return 0;
    } 

  if (diff < time_slice) 
    {
      printf ("values are effectively equal ( < 1 time slice )\n");
      return 0;
    }
  else if (diff < d_minimal)
    {
      printf("difference is within min delay\n");
      return 0; 
    }
  else 
    {
      printf ("Values are different ( > min delay) - shouldn't get here \n");
    }
  return -9;
}






#ifdef GONE

INT compare(double my_delay, double d_delay )
{
  /* compares two values
     if my_delay < d_delay  returns  -1
     if my_delay = d_delay  returns   0
     if my_delay > d_delay  returns   1
 */

  //INT idelay,jdelay;
  DWORD idelay,jdelay,imin;
  double r,ip,mdelay,ddelay;
  double rr,iipp;
  double d_minimal;

#ifndef POL
    d_minimal        = 	settings.ppg.output.minimal_delay__ms_;/* all standard pulses  have a fixed width of d_pulsew */
#else
   d_minimal        = 	settings.output.minimal_delay__ms_;
#endif  
  if (my_delay == d_delay) // identical 
    return 0;
  
      
  if (fabs (my_delay - d_delay) > d_minimal) // larger than any likely minimal delay
    {  // difference between the values is large; simply compare 
      if(my_delay < d_delay)
	return -1;
      else if (my_delay > d_delay)
	return 1;
      else
	return 0;
    } 

  if(debug)	
     printf("compare: starting with my_delay=%f d_delay=%f  diff=%f (minimal delay = %f)\n",
	    my_delay,d_delay,(my_delay-d_delay),d_minimal);

  // these values are close... but they may be too large to convert to integer.
  r=modf(my_delay,&ip);
  //printf("modf of %f : r=%f  ip=%f\n",my_delay,r,ip);
   rr=modf(d_delay,&iipp);
   //printf("modf of %f : rr=%f  iipp=%f\n",d_delay,rr,iipp);
  
  // subtract the integer part of my_delay from both values so they will be small.
  mdelay=my_delay-ip;
  ddelay=d_delay-ip;
  
   printf("Subtracting %f from both; now  mdelay=%f ddelay=%f\n",ip,mdelay,ddelay);
#ifndef POL  
   double  time_slice = settings.ppg.output.time_slice__ms_;
#else
   double  time_slice = settings.output.time_slice__ms_;
#endif
   printf("time_slice = %f \n",time_slice);
  // rounds up or down by half the time slice approximately.
   //  idelay = (INT) ( (mdelay + 0.000005) * 100000.0 );
   // jdelay = (INT) ( (ddelay + 0.000005) * 100000.0 );
   mdelay = mdelay + time_slice/2;
   ddelay = ddelay + time_slice/2;
   printf("now  mdelay = %f and ddelay = %f\n",mdelay,ddelay);
   idelay = (DWORD)(mdelay * 100000);
   jdelay = (DWORD)(ddelay * 100000);

   //idelay = (DWORD) ( (mdelay + time_slice) * 100000.0 );
   //jdelay = (DWORD) ( (ddelay + time_slice) * 100000.0 );
   //idelay = (DWORD)(mdelay * 100000);
   //ddelay = (DWORD)(ddelay * 100000);
   imin = (DWORD)(d_minimal * 100000);
   printf("Integers   idelay = %lu jdelay = %lu imin = %lu\n",idelay,jdelay,imin);

   if(debug)printf("compare: idelay = %lu jdelay=%lu\n",idelay,jdelay); 
   DWORD kdelay;
   kdelay = abs(jdelay-idelay);
   if (kdelay == imin) printf("delays are equal\n");
   else if (kdelay < imin)printf("delays are within min delay\n");
   else printf("delays are NOT within min delay\n");

  if(idelay < jdelay)
    {
       if(debug)
      	printf("delay %.4f  is less than  %.4f \n",my_delay,d_delay);
      return -1;
    }
  else if (idelay > jdelay)
    {
       if(debug)
      	printf("delay %.4f  is greater than  %.4f \n",my_delay,d_delay);
      return 1;
    }
  else
    {
       if(debug)
       	printf("delay %.4f  is equal to  %.4f \n",my_delay,d_delay);
      return 0;
    }
}
#endif  // GONE

void print_delays( FILE *fout)
{
  INT j;
  char string1[20];
  char string2[3];

  fprintf(fout, "\nDelays in tdelay array:  number of delays :%d \n",gbl_num_delays);
  fprintf(fout,"index delay(ms)  delayName   bitpat reversed Code Index t0_offset(ms) lindx block name(s)\n");
  for(j=0; j<gbl_num_delays; j++)
    {
      if(tdelays[j].code > 0 ) // loop
      	{
	  printf("%11.5f",tdelays[j].delay_ms);
	  sprintf(string2,"%2.2d", tdelays[j].loop_index);
	}	 
      else
	{
	  sprintf(string1,"%11.5f",tdelays[j].delay_ms);
	  sprintf(string2,"");
	}
#ifdef BITS_24
      fprintf(fout,"%3.3d %11.11s %10.10s   0x%6.6x 0x%6.6x    %2.2d  %2.2d     %11.5f   %2.2s %s\n",
	      j,  string1, tdelays[j].delay_name, tdelays[j].bitpat,
	      backpattern(tdelays[j].bitpat), tdelays[j].code, tdelays[j].trans_index, 
	      tdelays[j].t0_offset_ms, string2, tdelays[j].block_name); 
#else
      fprintf(fout,"%3.3d %11.11s %10.10s   0x%4.4x 0x%4.4x    %2.2d  %2.2d     %11.5f   %2.2s %s\n",
	      j,  string1, tdelays[j].delay_name, tdelays[j].bitpat,
	      backpattern(tdelays[j].bitpat), tdelays[j].code, tdelays[j].trans_index, 
	      tdelays[j].t0_offset_ms, string2, tdelays[j].block_name); 
#endif
    }
}

INT newppg()
{  
 INT i,j,n;
 double my_delay_ms;

 n=0;
  for(j=0; j<gbl_num_delays; j++)
    {
      if ( (j+1) < gbl_num_delays )  
	{   // check for loops with associated zero-delay instruction (same bitpattern)
	  if (tdelays[j].code > 0 || tdelays[j+1].code > 0)  // loop
	    {
	      if (tdelays[j].bitpat == tdelays[j+1].bitpat) // same bitpattern
		{
		  if (tdelays[j].delay_ms == 0 ||  tdelays[j+1].delay_ms == 0 )
		    {
		      my_delay_ms = tdelays[j].delay_ms;
		      if ( tdelays[j+1].delay_ms > my_delay_ms)
			my_delay_ms =  tdelays[j+1].delay_ms;
		      if (my_delay_ms == 0)
			{
			  printf("newppg: two consecutive instructions at index %d in delay list with zero delay\n");
			  return -1;
			}
		      if(tdelays[j+1].code == 0) // a transition
			{
			  
			  ndelays[n].delay_ms = my_delay_ms;
			  ndelays[n].bitpat = tdelays[j].bitpat;
			  ndelays[n].code = tdelays[j].code;
			  ndelays[n].t0_offset_ms = tdelays[j].t0_offset_ms;
			  i= tdelays[j].loop_index;
			  if ( tdelays[j].code == 1) // begin loop
			    ndelays[n].loop_count = (loop_params[i].loop_count -1);
			  strcpy(ndelays[n].loop_name,loop_params[i].loopname);
			  strcpy(ndelays[n].block_name,tdelays[j].block_name);
			  strcat(ndelays[n].block_name,",");
			  strcat(ndelays[n].block_name,tdelays[j+1].block_name);
			       n++;
                          j++; // skip next instruction
			  goto cont;			  
			}
		    }
		}
	    }
	} 
    
      ndelays[n].delay_ms = tdelays[j].delay_ms;      
      ndelays[n].bitpat = tdelays[j].bitpat;
      ndelays[n].code = tdelays[j].code;
      ndelays[n].t0_offset_ms = tdelays[j].t0_offset_ms;
      if ( tdelays[j].code > 0)  // begin/end loop
	{
	  i= tdelays[j].loop_index;
	  if ( tdelays[j].code == 1) // begin loop
	    ndelays[n].loop_count = (loop_params[i].loop_count -1);
	  strcpy(ndelays[n].loop_name,loop_params[i].loopname);
	}
      strcpy(ndelays[n].block_name,tdelays[j].block_name);
      n++;
		
    cont:
      printf ("loop count j=%d; n=%d\n",j,n);
    }

  gbl_num_delays_nppg=n;
  printf("New PPG... \n");
  printf("Index Delay(ms) Bitpat  code  loopname loopcount T0_offset block_name\n");

  for (i=0;  i <  gbl_num_delays_nppg; i++ )
    {
      printf("%2.2d %11.5f 0x%8.8x 0x%1.1d %10.10s %9.09d %11.5f %s\n",
	     i,ndelays[n].delay_ms,  ndelays[n].bitpat,   ndelays[n].code,  ndelays[n].loop_name,
	     ndelays[n].loop_count,  ndelays[n].t0_offset_ms,  ndelays[n].block_name);
    }
  return 0; // success
}


#ifdef GONE
void print_newppg( FILE *fout)
{
  INT i,j;
  char string1[20];
  char string2[3];
  INT pc;
  double my_delay_ms;

  pc =0;
  fprintf(fout, "\nDelays for NEWPPG.   Number of delays :%d \n",gbl_num_delays);
  fprintf(fout,"REG  Data  # index PC  bitpat  LoopCount  code Lindex  block name(s)\n");
  for(j=0; j<gbl_num_delays; j++)
    {
      // next instruction
      sprintf(string1," "); // set bitpat area  blank
      print ("0x%2.2x  0x%6.6x   # Program Address %d    \n", TPPG_PPG_ADDR, pc, pc);
      pc++;

      if(tdelays[j].code == 1 || (tdelays[j].code == 2 ) // begin loop 1; end loop 2
      	{
          // loops are often associated with zero delay instructions (preceding or following loop)
          // with identical bitpatterns
          // 
	  // bits:  0-31 SET   bitpat   TPPG_PPG_DATA_LO
          //       32-63 CLR   bitpat   TPPG_PPG_DATA_MED
          //       64-95 DELAY delay_ms   TPPG_PPG_DATA_HI
          //       96-115 DATA loop count   TPPG_PPG_DATA_TOP
          //       116-117 INS BEGLOOP

          if (tdelays[j].delay_ms == 0)
	    {
              // look at the next instruction
              if (tdelays[j+1] == 0) // not a transition
		{
		  printf("Loop instruction with zero delay at index %d followed by another; cannot convert\n",j);
		  return -1;
		}
	      if ((tdelays[j].bitpat == tdelays[j+1].bitpat) && (tdelays[j+1].delay_ms > 0))
		{
		  my_delay_ms = tdelays[j+1].delay_ms; 
		}
	      else
		{
		  printf("Loop instruction with zero delay at index %d. Cannot combine instructions (bitpatterns not identical)\n",j);
		  return -1;
		}
	      
          printf("0x%2.2x  0x%8.8x    # DATA LO set bits  from index %d  \n", TPPG_PPG_DATA_LO,  tdelays[j].bitpat ,j);
          printf("0x%2.2x  0x%8.8x    # DATA MED clr bits  from index %d  \n", TPPG_PPG_DATA_LO,  ~tdelays[j].bitpat ,j);
          printf("0x%2.2x  0x%8.8x    # DATA HI delay  from index %d  \n", TPPG_PPG_DATA_LO,  ~tdelays[j].bitpat ,j);
	  sprintf(string1,"%11.5f",tdelays[j].delay_ms);
          i= tdelays[j].loop_index;
	  sprintf(string2,"%2.2d", (loop_params[i].loop_count -1),  );
	  
	}	 
      else
	{
	  sprintf(string1,"%11.5f",tdelays[j].delay_ms);
	  sprintf(string2,"");
	}
#ifdef BITS_24
      fprintf(fout,"%3.3d %11.11s %10.10s   0x%6.6x 0x%6.6x    %2.2d  %2.2d     %11.5f   %2.2s %s\n",
	      j,  string1, tdelays[j].delay_name, tdelays[j].bitpat,
	      backpattern(tdelays[j].bitpat), tdelays[j].code, tdelays[j].trans_index, 
	      tdelays[j].t0_offset_ms, string2, tdelays[j].block_name); 
#else
      fprintf(fout,"%3.3d %11.11s %10.10s   0x%4.4x 0x%4.4x    %2.2d  %2.2d     %11.5f   %2.2s %s\n",
	      j,  string1, tdelays[j].delay_name, tdelays[j].bitpat,
	      backpattern(tdelays[j].bitpat), tdelays[j].code, tdelays[j].trans_index, 
	      tdelays[j].t0_offset_ms, string2, tdelays[j].block_name); 
#endif
    }
}
#endif
