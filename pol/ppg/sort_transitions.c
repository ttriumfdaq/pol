/* 
   Name:   sort_transitions.c
   Created by: SD

   Contents: sorts and checks list of transitions 

   $Id: sort_transitions.c,v 1.1 2011/04/15 18:50:31 suz Exp $


*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h> // fabs
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
#ifndef POL
extern TITAN_ACQ_SETTINGS settings;
#else
extern POL_ACQ_SETTINGS settings;
#endif
extern double min_delay;
void sort_transitions(TRANSITIONS *ptrans)
{
  INT j;
  /* start sorting */
  
  if(gbl_num_transitions > 1)
    {
      qsort( ptrans, gbl_num_transitions, sizeof(TRANSITIONS),
	     trans_compare);
     
      // if(debug)
	{
	  printf("sort_transitions: after qsort\n");
	  for(j=0; j<gbl_num_transitions; j++)
	    {
	      if(ptrans[j].bit & 0x80000000) // bit pattern
		printf("ptrans[%d] : time from T0 %f bitpat 0x%x  \n",
		       j,  ptrans[j].t0_offset_ms, (ptrans[j].bit & 0x7FFFFFFF));
	      else if(ptrans[j].bit & 0x40000000) // delay
		printf("ptrans[%d] : time from T0 %f bitpat unchanged  \n",
		       j,  ptrans[j].t0_offset_ms);
	      else
		printf("ptrans[%d] : time from T0 %f bit %d (or 0x%4.4x) \n",
		       j,  ptrans[j].t0_offset_ms, ptrans[j].bit, 1<< ptrans[j].bit);
	    }
	}
    }
  return;
}



int trans_compare(const void *arg1, const void *arg2) 
{
  double r;
  r = (double) ((TRANSITIONS *)arg1)->t0_offset_ms - ((TRANSITIONS *)arg2)->t0_offset_ms;
  if(r>0.00000)
    return 1;
  else if (r<0.000000)
    return -1;
  else 
    return 0;
}


/*---------------------------------------------------------*/

 
INT precheck_sorted_transitions(TRANSITIONS *ptrans)
{
  /* Makes sure begin loop and reserved blocks precede all transitions at identical times
           and  end loop  follows all transitions at identical times
        by swapping if necessary

	Also checks items at identical offsets to end_loop,beg_loop, res_block are TRANS_CODE

	Called before check_sorted
  */

  TRANSITIONS ttrans;
  int i,j,k,n;
  int blindex;

  printf("precheck_sorted_tr: starting\n");

  /* Begin loops and Reserved blocks must precede all  transitions at identical times */
  for (i=0; i< gbl_num_transitions; i++)
    {
      if(debug)
	printf("working on transition %d, block %s\n",i,ptrans[i].block_name);
      if((ptrans[i].code == BEGLOOP_CODE)||(ptrans[i].code == RES_CODE)) // begin_loop / reserved block
	{
	  
	  if(ptrans[i].code == BEGLOOP_CODE)
	    {
	      printf("found begin_loop at transition %d t0_offset=%f\n",i, ptrans[i].t0_offset_ms);
	      blindex =  loop_params[ptrans[i].loop_index].defines_start_ref_index;
	      printf("loop index=%d; defines reference refindex=%d\n",
		     ptrans[i].loop_index,blindex);
	    } 
	  else
	    printf("found reserved block at transition %d t0_offset=%f\n",i, ptrans[i].t0_offset_ms);
	    
	  
	  for (j=i-1; j>-1; j--) // look backwards for identical transitions
	    {
	      /* find the first previous non-identical transition offset */
	      if (compare  (ptrans[i].t0_offset_ms, ptrans[j].t0_offset_ms )!=0)
		{
		  printf( "found prior transition at j=%d at a different offset (%f)\n",
			  j, ptrans[j].t0_offset_ms);
		  if (j == i-1) 
		    break; // previous transition is NOT identical
		  else
		    { // we must move the begin_loop/reserved block instruction up to the top of the identical instructions
		      printf("break with i=%d and j=%d\n",i,j);  		    
		      
		      int k;
		      for (k=j+1; k<i; k++)
			{
			  printf("Transition %d is at identical T0_offset (or within min delay) to transition %d\n",k,i); 
			  printf("(i.e. Transition %d with T0_offset = %.6f and Transition %d with T0_offset = %.6f)\n", 
				 k, ptrans[k].t0_offset_ms, i, ptrans[i].t0_offset_ms);
			}
		    

		      printf("Moving identical transitions (or within min delay) below begin_loop/reserved block\n");
		      //printf("by overwriting trans %d, then shifting down at %d\n",i,j);
		      //ttrans = ptrans[i]; // store this
		      //shift_trans_up(i, ptrans); // overwrites transition i		  
		      //shift_trans_down(j+1, 1 ,ptrans); // make a space for begin loop trans
		      //ptrans[j+1] = ttrans; // copy the begin_run/reserved transition here
		      
		      k=j+1; // the earliest "identical" transition
		      printf("by swapping transition %d with transition %d\n",i,k);
		      swap(i,k,ptrans);
		      printf("now Transition %d has T0_offset = %.6f and Transition %d has T0_offset = %.6f\n", 
			     k, ptrans[k].t0_offset_ms,i, ptrans[i].t0_offset_ms);
			
		      //if(debug)
		      print_sorted_transitions(ptrans,0,stdout);
                      
		      for (j=k; j<i; j++)
			{
			   printf("comparing Transition %d (T0_offset = %.6f) and Transition %d (T0_offset = %.6f)\n", 
			     j, ptrans[j].t0_offset_ms,i, ptrans[i].t0_offset_ms);
			    if( ptrans[j].t0_offset_ms >  ptrans[i].t0_offset_ms) 
			     {
			       printf("delays must increase; setting Transition %d offset equal to (loop) Transition %d (i.e. %.6f)\n",
			   	     i,j, ptrans[j].t0_offset_ms);
			       ptrans[i].t0_offset_ms = ptrans[j].t0_offset_ms;
			    }
			}
			  //if(debug)
		      print_sorted_transitions(ptrans,0,stdout);
		      break;
		    }
		}
	      else
		{
		  if(ptrans[j].code != TRANS_CODE)
		    {
		      // this is likely an error 
		      cm_msg(MERROR,"precheck_sorted",
"Non-transition found at same offset (%.4f) to begin_loop/res block (trans %d block %s)", 
			     ptrans[j].t0_offset_ms,j, ptrans[j].block_name);
		      return DB_INVALID_PARAM;
		    }
		  if(debug) printf("transition (j=%d) at identical T0_offset found at %.4f\n",
				   j, ptrans[j].t0_offset_ms);
		}
	    } // end of for loop

	  if(ptrans[i].code == RES_CODE)
	    {
	      if(i < k+1)
		cm_msg(MERROR,"precheck_sorted_transitions",
		       "WARNING: res_code at %d does not follow end_loop at %d",i,k);
	    }
	} // end of beg_loop or reserved block
    
      else if(ptrans[i].code == ENDLOOP_CODE)// end_loop must follow all identical transtions
	{ 
	  if(debug)
	    printf("found end_loop at transition %d t0_offset=%f\n",i, ptrans[i].t0_offset_ms);
	  k=i;
	  for (j=i+1; j< gbl_num_transitions; j++)
	    {  /* find the first non-identical transition after end_loop  */
	      if(debug)
		printf("j=%d; i=%d\n",j,i);
	      /* find the next non-identical transition offset (if any) */
	      if (compare  (ptrans[i].t0_offset_ms, ptrans[j].t0_offset_ms) != 0)
		{
		  if(debug)
		    printf( "found transition at index=%d (compare to index %d) at different offset (%f)\n",
			    i,j, ptrans[j].t0_offset_ms);
		  k=j-1;
		  if(k == i)
		    break; // next transition is not identical; do nothing
		  else		  
		    {
		      if(debug)
			printf("overwriting trans %d, then shifting down at %d\n",i,k);
		      
		      
		      ttrans = ptrans[i]; // store this endloop trans
		      shift_trans_up(i, ptrans); // overwrites transition i
		      
		      //  print_sorted_transitions(ptrans,0, stdout);
		      shift_trans_down(k-1, 1 ,ptrans); // make a space for end loop trans
		      //print_sorted_transitions(ptrans,0, stdout);
		      ptrans[k] = ttrans; // copy the end_run transition here
		      //print_sorted_transitions(ptrans,0, stdout);
		      break; 
		    }
		}
	      else
		{ // found identical transition
		  printf("found identical transition to end_loop at j=%d\n",j);
		  if(ptrans[j].code != TRANS_CODE && ptrans[j].code != RES_CODE)
		    {
                      printf("ptrans[i].code=%d  ptrans[j].code =%d\n",ptrans[i].code,ptrans[j].code );
		      cm_msg(MERROR,"precheck_sorted",
	       "item at identical offset to end_loop (%.4f) must be a transition (trans %d block %s)", 
			     ptrans[j].t0_offset_ms,j,ptrans[j].block_name);
		      return DB_INVALID_PARAM;
		    }
			   
		}
	  
	    } // end of for loop
	} // end of end loop code
    }
  print_sorted_transitions(ptrans,0, stdout);
  return SUCCESS;
}


INT check_same(TRANSITIONS *ptrans)
{
  INT i,j, itime;
  INT bitpat,mypat,pattern_flag;
  double time;


  /*
    called after sorting but before combining transitions

     checks for transitions whose time is the same, and which operate on the same bits.
     also checks for patterns and transitions at the same times
         same time means  within minimal delay
  */
  
  printf("check_same: looking for illegal transitions and patterns\n");
  printf("******************** min_delay= %f\n",min_delay);
  

 restart:
  i=itime=0;
  // find the first transition
  for(i=0; i< gbl_num_transitions; i++)
    {    
      if(ptrans[i].code ==  TRANS_CODE)
	break;
    }
  if(debug)
    printf("check_same: ptrans[%d].bit = 0x%x\n",i, ptrans[i].bit);
  time=ptrans[i].t0_offset_ms;
  itime=i; // remember transition number
  bitpat=1<<ptrans[i].bit;
  if(debug)printf("check_same: starting with transition %d time=%f bitpat=0x%x\n",
		  i,time,bitpat);
  if(ptrans[i].bit & 0x80000000) // bit pattern
    {
      if(debug)printf("transition i=%d  setting pattern_flag TRUE\n",i);
      pattern_flag = TRUE;
    }
  else
    {
      if(debug)printf("transition i=%d  setting pattern_flag FALSE\n",i);
      pattern_flag = FALSE;
    }


 start:
  i++;  // next transition

  if(debug)printf ("At start, incremented i to %d, pattern_flag=%d\n",i,pattern_flag);
  if( i >= gbl_num_transitions) 
    goto done;
  
  if(ptrans[i].code !=  TRANS_CODE)
    {
      if(debug)printf("check_same: skipping i= %d; not a transition\n",i);
      goto start;  // next transition
    }
  
  if(debug)printf("check_same: TRANS_CODE at transition %d, bit=%d\n",i,ptrans[i].bit);

  
  printf("ptrans[%d].t0_offset_ms=%f  and time=%f\n",i, ptrans[i].t0_offset_ms,time);
  while ( compare( (ptrans[i].t0_offset_ms - time), min_delay) <0)
    {
      /* both transitions are at the same time (or within min delay) */
      if(debug)
	  printf("trans %d effectively at same time %f as previous trans (%f) \n",i,ptrans[i].t0_offset_ms,time);
 
      if( (ptrans[i].bit & 0x80000000) || pattern_flag) // bit pattern 
	{  /* One (or both) of these transitions is a pattern
	  
	   Except for t0 (because  pattern block name="PATTERN_T0" is automatically added),
	   don't allow delays or bit patterns to be within a minimal delay of 
	   each other or transitions
	   */
	  if(debug)printf("one of these transitions is a bit pattern; looking for auto pattern at t0\n");
	  if(compare  (time, 0.0) == 0)
	    {
	      /* look for the pattern at t0 that was automatically put in */
	      j=-1;
	      
	      if(debug)printf("i=%d itime=%d ptrans[%d].block_name=%s,  ptrans[%d].block_name=%s \n",
			      i,itime,i,ptrans[i].block_name, itime, ptrans[itime].block_name);
	      if(strcmp("PATTERN_T0",ptrans[itime].block_name)==0)
		j=itime;
	      else if (strcmp("PATTERN_T0",ptrans[i].block_name)==0)
		j=i;
	      
	      if(j>=0)
		{
		  /* lose the pattern at t0 that was automatically put in */
		  printf("check_same: removing default pattern at t0=0, block_name %s\n",ptrans[j].block_name);
		  shift_trans_up(j,ptrans);
		  print_sorted_transitions(ptrans,FALSE,stdout);
		  goto restart; // let's restart the check
		}
	      printf("j=%d\n",j);
	    }  // end of t0 comparision
	  
	  /* otherwise this is an error */
	  printf("Got to Y\n");

	  cm_msg(MERROR,"check_same",
		 "Pattern block 0x%x at same time (%f) as another transition or pattern (blocks %s,%s) is not allowed\n",
		 ptrans[i].bit, time,  ptrans[i].block_name,
		 ptrans[itime].block_name);
	  return DB_INVALID_PARAM;
		
	} // end of one/both transitions is a pattern
    
      else
	{ /* transitions at the same time (or within min delay); neither is a pattern */
	  mypat = 1<<ptrans[i].bit;
	  if (bitpat & mypat)
	    {
	      printf("check_same:cannot operate on same bit %d  at same time %f  - or within min delay - (transition %d)\n",
		     ptrans[i].bit,time,i);
	      cm_msg(MERROR,"check_same",
		     "cannot operate on same bit %d  at same time %f  - or within min delay -  (block %s)",
		     ptrans[i].bit,time,ptrans[i].block_name);
	      return DB_INVALID_PARAM;
	    }
	  else
	    bitpat = bitpat | mypat;
      
	  //	printf("different bit %d  at same time %d mypat=0x%x bitpat=0x%x\n",
	  //	       bits[i],time,mypat,bitpat);
	  i++;
	  if(i >= gbl_num_transitions) goto done;
	} 
    }  // end of while loop on transitions at the same time
  

  /* times are not the same; get the new time */
 next:
  if(debug)printf("next - new time is %f (trans %d)\n", ptrans[i].t0_offset_ms,i);
  time=ptrans[i].t0_offset_ms; // update previous time
  itime = i; // remember i for this time
  if(ptrans[i].bit & 0x80000000)
    { 
      pattern_flag = TRUE;
      bitpat = ptrans[i].bit & 0xFFFF;
    }
  else
    {
      pattern_flag = FALSE;
      bitpat = 1<< ptrans[i].bit; // new bitpat
    }
  goto start;     
  
 done:
  printf("check_same:  done: \n");
  return SUCCESS;
}



INT check_sorted_transitions(TRANSITIONS *ptrans)
{
  /*  Checks and adds "delays" (actually markers)  to begin/end loops 
     
  NOTE:  if begin/end loop have associated identically timed transitions, presort
         will already have swapped them
	 EV blocks have already been removed before this routine is called

  This routine checks
      if any transitions within a loop are not referenced from a regular time reference.
        (they are referenced from begin_loop, end_loop or variable pulse); issues a warning
      
     Apart from check number 1, it does nothing to transitions with code=TRANS_CODE

     Returns SUCCESS or  DB_INVALID_PARAM
 */

  INT i,j,k,c,d,loop,status;
  INT my_timeref_code;
  INT bits[10];
  INT nbits,my_bit;
  double prev_time,my_time;
 

  status = check_same(ptrans); // check for identical bits changed at one time
  if(status != SUCCESS)
    {
      printf("check_sorted: error from check_same\n");
      return status;
    }


  loop = 0;
  /* first check for nested loops with begin or end at the same time */
  printf("check_sorted: looking for illegally nested transitions\n");
  for (i=0; i< gbl_num_transitions; i++)
    {
      if ( ((ptrans[i].code ==  BEGLOOP_CODE) || (ptrans[i].code ==  ENDLOOP_CODE) ) &&
	   ((ptrans[i+1].code ==  BEGLOOP_CODE) ||  (ptrans[i+1].code ==  ENDLOOP_CODE)) )
	{
	  if(compare ( (ptrans[i+1].t0_offset_ms - ptrans[i].t0_offset_ms),
		       min_delay) <= 0 )
	    {
	      cm_msg(MERROR,"check_sorted","two begin or end loops MUST be separated by at least a minimal delay (blocks %s and %s)",ptrans[i].block_name, ptrans[i+1].block_name);
	      if(debug)printf("min delay=%f  diff =%f\n", min_delay,
		     (ptrans[i+1].t0_offset_ms - ptrans[i].t0_offset_ms));
	      return DB_INVALID_PARAM;
	    }
	}
    }



  for (i=0; i< gbl_num_transitions; i++)
    {
      if(debug)
	printf("check_sorted_transitions: working on transition %d transcode=%d timeref_code=%d\n",
	       i,ptrans[i].code,
	       time_ref_params[ptrans[i].time_reference_index].timeref_code );
      /* check for T references within a loop. Issue a warning...it may be a mistake */   
      if(loop > 0)
	{
	  my_timeref_code =  time_ref_params[ptrans[i].time_reference_index].timeref_code;
	  /* if(my_timeref_code == timeref_code_time)
	    {  /* make this a warning... remove altogether as whole cycle is now within a scan loop */
	  /*  cm_msg(MINFO,"check_sorted", 
		     "Item within a loop is referenced to a time reference(%s, code %d); expect a loop (or pulse) reference (block %s)",
		     time_ref_params[ptrans[i].time_reference_index].this_reference_name,
		     my_timeref_code,
		     ptrans[i].block_name);
	      
		     } */
	}
      

      if(ptrans[i].code == BEGLOOP_CODE)// begin_loop
	loop++;
      else if(ptrans[i].code == ENDLOOP_CODE)// end_loop
	loop--;

      if(debug)printf("Trans code is %d; loop counter =%d\n",ptrans[i].code, loop);

      if(ptrans[i].code == TRANS_CODE  ) // EV blocks already gone
	goto next;  // next transition


      /* 
	 This transition has code BEGIN or END LOOP, or RESERVED   
      */
     
      /* do some checking on the next transition */
      /*      if (i+1 >= gbl_num_transitions)


		 if this transition is RESERVED BLOCK
      */
      if (ptrans[i].code == RES_CODE) // reserved block
	{ 
	  if(debug)printf("reserved block.... checking for preceding end_loop\n");
	  
	  /* check again for preceding end loop (may have been swapped) */
	  if(i==0 || (ptrans[i-1].code != ENDLOOP_CODE))  // 2=end loop
	    { 
	      cm_msg(MERROR,"check_sorted",
		     "Error - reserved block (block %s) must follow an end loop block \n",
		     ptrans[i].block_name);
	      return DB_INVALID_PARAM;
	    }
	}

      else if (ptrans[i].code == ENDLOOP_CODE)
	{
	  
	  printf("check_sorted: detected end loop at trans %d\n",i);
	  if ( (i+1) >= gbl_num_transitions) 
	    {  // can't be the last transition
	      cm_msg(MERROR,"check_sorted",
		     "Error - expect reserved block after end loop (block %s) \n",
		     ptrans[i].block_name);
	      return DB_INVALID_PARAM;
	    }

	  /* expect a reserved block (for the rest of the loops) immediately after the end loop */
	  if (ptrans[i+1].code != RES_CODE) 
	    {		
	      cm_msg(MERROR,"check_sorted",
		     "Error - illegal transition (block %s) in reserved block after end loop (block %s) \n",
		     ptrans[i+1].block_name, ptrans[i].block_name);
	      return DB_INVALID_PARAM;
	    }
	}

    next:   // next transition
      if(debug)printf("check_sorted_transitions: finished working on transition %d code=%d\n",
		      i,ptrans[i].code); 
      
    } // end of for loop on all transitions
  
  
  printf("After checking sorted transitions:\n");
  print_sorted_transitions(ptrans,FALSE, stdout); // bitpattern not yet calculated
  


  if(combine_transitions(ptrans) != SUCCESS)
    return DB_INVALID_PARAM;
  gbl_combined=TRUE; /* transitions may now be combined; alters printout in print_sorted */

  printf("After combining transitions:\n");  
  print_sorted_transitions(ptrans,FALSE, stdout); // bitpattern not yet calculated
  print_sorted_transitions(ptrans,TRUE, stdout);
  
  if(check_loops_trans(ptrans) != SUCCESS)
    return DB_INVALID_PARAM;


  return DB_SUCCESS;

}

       

#ifdef GONE
/*---------------------------------------------------------*/
INT check_sorted_transitions(TRANSITIONS *ptrans)
{
  /*  Checks and adds "delays" (actually markers)  to begin/end loops 
     
  NOTE:  if begin/end loop have associated identically timed transitions, presort
         will already have swapped them
	 EV blocks have already been removed before this routine is called

  This routine checks
     1. if any transitions within a loop are not referenced from a regular time reference.
        (they are referenced from begin_loop, end_loop or variable pulse); issues a warning
     2. it adds delay blocks as markers (if necessary) before and after begin_loop
     3. it adds a delay block if necessary before end_loops
  
     Apart from check number 1, it does nothing to transitions with code=TRANS_CODE

     Returns SUCCESS or  DB_INVALID_PARAM
 */

  INT i,j,k,c,d,loop,status;
  INT my_timeref_code;
  INT bits[10];
  INT nbits,my_bit;
  double prev_time,my_time;



 
  status = check_same(ptrans); // check for identical bits changed at one time
  if(status != SUCCESS)
    {
      printf("check_sorted: error from check_same\n");
      return status;
    }


  loop = 0;

  /* first check for nested loops with begin or end at the same time */
  printf("check_sorted: looking for illegally nested transitions\n");
  for (i=0; i< gbl_num_transitions; i++)
    {
      if ( ((ptrans[i].code ==  BEGLOOP_CODE) || (ptrans[i].code ==  ENDLOOP_CODE) ) &&
	   ((ptrans[i+1].code ==  BEGLOOP_CODE) ||  (ptrans[i+1].code ==  ENDLOOP_CODE)) )
	{
	  if(compare ( (ptrans[i+1].t0_offset_ms - ptrans[i].t0_offset_ms),
		       min_delay) <= 0 )
	    {
	      cm_msg(MERROR,"check_sorted","two begin or end loops MUST be separated by at least a minimal delay (blocks %s and %s)",ptrans[i].block_name, ptrans[i+1].block_name);
	      if(debug)printf("min delay=%f  diff =%f\n", min_delay,
		     (ptrans[i+1].t0_offset_ms - ptrans[i].t0_offset_ms));
	      return DB_INVALID_PARAM;
	    }
	}
    }



  for (i=0; i< gbl_num_transitions; i++)
    {
      if(debug)
	printf("check_sorted_transitions: working on transition %d transcode=%d timeref_code=%d\n",
	     i,ptrans[i].code,
	     time_ref_params[ptrans[i].time_reference_index].timeref_code );
      /* check for T references within a loop. Issue a warning...it may be a mistake */   
      if(loop > 0)
	{
	  my_timeref_code =  time_ref_params[ptrans[i].time_reference_index].timeref_code;
	  if(my_timeref_code == timeref_code_time)
	    {  /* make this a warning */
	      cm_msg(MINFO,"check_sorted", 
		     "Item within a loop is referenced to a time reference(%s, code %d); expect a loop (or pulse) reference (block %s)",
		     time_ref_params[ptrans[i].time_reference_index].this_reference_name,
		     my_timeref_code,
		     ptrans[i].block_name);
	      
	    }
	}
      

      if(ptrans[i].code == BEGLOOP_CODE)// begin_loop
	loop++;
      else if(ptrans[i].code == ENDLOOP_CODE)// end_loop
	loop--;

      if(debug)printf("Trans code is %d; loop counter =%d\n",ptrans[i].code, loop);

      if(ptrans[i].code == TRANS_CODE  ) // EV blocks already gone
	goto next;  // next transition


      /* 
      This transition has code BEGIN or END LOOP, or RESERVED   
      */
     
      /* do some checking on the next transition */
      if (i+1 >= gbl_num_transitions)
	{
	  if(debug)printf("check_sorted_trans: last transition is at i=%d (gbl_num_transitions=%d\n",
		 i,gbl_num_transitions);
	  break; // this was the last transition
	}
	     
  

      /* 
	 if this transition is RESERVED BLOCK
      */
      if (ptrans[i].code == RES_CODE) // reserved block
	{ 
	  if(debug)printf("reserved block.... checking for preceding end_loop\n");
	  
	  /* check again for preceding end loop (may have been swapped) */
	  if(i==0 || (ptrans[i-1].code != ENDLOOP_CODE))  // 2=end loop
	    { 
	      cm_msg(MERROR,"check_sorted",
		     "Error - reserved block (block %s) must follow an end loop block \n",
		     ptrans[i].block_name);
	      return DB_INVALID_PARAM;
	    }

	  /* this should be OK 	  
	  if(compare ( (ptrans[i+1].t0_offset_ms - ptrans[i].t0_offset_ms),
		       min_delay) < 0 )
	    { /* must be followed by minimal delay 
	      cm_msg(MERROR,"check_sorted",
		     "Error - reserved block (block %s) must be followed by a transition with at least a minimal delay (block %s) \n",
		     ptrans[i].block_name, ptrans[i+1].block_name);
	      return DB_INVALID_PARAM;
	    }
	  */
	
	} // end of reserved block
	

	 
      else if (ptrans[i].code == BEGLOOP_CODE || ptrans[i].code == ENDLOOP_CODE) // begin or end loop
	{
	  /* 
	     If this transition is BEGIN or END LOOP

	     precheck checked that all identical transitions follow begin_loops
                                                        and precede end_loops

	     check_loops will have checked that end loops follow begin loops

             if no matching transition, add a delay before  begin/end loop
	  */

	  //printf("detected begin or end loop\n");
	  c=d=-9; // initialize  (c for comparison to previous, d for next)

	  
	  if(i<=0)
	    {
	      printf("should be OK.... begin_loop is the first transitions \n");
	      //    return DB_INVALID_PARAM;
	    }

	  else
	    {
	      if(debug)
		{
		  printf("i=%d; t0_offset for ptrans[%d], [%d], [%d] are %f %f %f\n",
			 i,i-1,i,i+1,ptrans[i-1].t0_offset_ms,
			 ptrans[i].t0_offset_ms, ptrans[i+1].t0_offset_ms);
		}
	    }
	  
	  /* check for identical items afterwards and before (looking for an identical transition) */
	  if(i>0)
	    {
	      if(ptrans[i-1].code == TRANS_CODE)
		c = compare  (ptrans[i-1].t0_offset_ms, ptrans[i].t0_offset_ms);
	    }

	     
	  if(ptrans[i+1].code == TRANS_CODE )
	    d = compare  (ptrans[i+1].t0_offset_ms, ptrans[i].t0_offset_ms);

	  if(debug)printf("begin/end loop compared with next (c=%d) & prev.(d=%d) transitions\n",
			  c,d);



	  if(c==-9 && d==-9)
	    {
	      /* there is no transition associated with a begin/end loop 
		 if there is a separation > minimal delay, one will be supplied
	      */
	      c = compare  ( (ptrans[i].t0_offset_ms - ptrans[i-1].t0_offset_ms),
			     min_delay) ;
	      if (c <= 0)
		{
		  cm_msg(MERROR,"check_sorted",
			 "Error - two begin/end loops must be separated by at least a minimal delay (blocks %s and %s) \n",
			 ptrans[i].block_name, ptrans[i+1].block_name);
		  printf("check_sorted: warning... non-trans and non-loop blocks may get this error. If so, need to change code\n");
		  return DB_INVALID_PARAM;
		}
	      
	      /* add a delay as a place-holder */
	      if(shift_trans_down(i, 1, ptrans)!= SUCCESS)
		return DB_INVALID_PARAM;
	      
	      concat(ptrans[i].block_name,"_delay_c", COMBINED_BLOCKNAME_LEN);
	      ptrans[i].code = TRANS_CODE;
	      ptrans[i].bit = 0x40000000; // mark this as a delay
	    }
	  
	
	  
	  else if ( c !=0 && d !=0)
	    {  /* no identical transition associated with a begin/end loop 
		  this is OK as we'll add some delays
	       */
	      
	      if(debug)printf("no identical trans associated with begin/end loop (block %s); a delay will be added(index=%d)\n",ptrans[i].block_name,i);
	      
	      if (ptrans[i].code == BEGLOOP_CODE ) // begin loop
		{ // begin loop needs identical delay before and after
		  if(shift_trans_down(i, 2, ptrans) != SUCCESS)
		    return DB_INVALID_PARAM;
		  concat(ptrans[i].block_name,"_delay_b", COMBINED_BLOCKNAME_LEN);
		  /* add this extra delay to TRANSITIONS RECORD */
		  // ptrans[i+1] = ptrans[i]; // mostly the same info
		  // strcpy( ptrans[i+1].block_name,  ptrans[i].block_name );
		  concat(ptrans[i+2].block_name,"_delay_a", COMBINED_BLOCKNAME_LEN);
		  
		  ptrans[i+2].code =  ptrans[i].code  = TRANS_CODE;
		  ptrans[i+2].bit =   ptrans[i].bit = 0x40000000; // mark this as a delay
		  
		  k=ptrans[i+2].loop_index;
		  ptrans[i+2].time_reference_index = loop_params[k].defines_start_ref_index;
		  i++; // increment i so we get to the right place
		  if(debug)printf("check_sorted_transitions: skipping transition %d since we've added a delay\n",i);
		  //print_sorted_transitions(ptrans,FALSE, stdout);
		}


	      else // end loop
		{
		  if(debug)printf("this is an end loop, shifting down\n");
		  
		  if(shift_trans_down(i, 1,ptrans) != SUCCESS)
		    return DB_INVALID_PARAM; 
		  
		  /* add this extra delay to TRANSITIONS RECORD */
		  ptrans[i] = ptrans[i+1]; // mostly the same info
		  strcpy( ptrans[i].block_name,  ptrans[i+1].block_name );
		  concat(ptrans[i].block_name,"_delay", COMBINED_BLOCKNAME_LEN);
		  
		  ptrans[i].bit =  0x40000000; // mark this as a delay		      
		  ptrans[i].code    = TRANS_CODE;
		  i++; // increment i so we don't recheck the end loop
		  //print_sorted_transitions(ptrans,FALSE, stdout);
		}
	    }
	  else  if( (c==0) && ptrans[i].code == BEGLOOP_CODE ) // begin loop
	    { // user has supplied an identical transition... this must come AFTER begin_loop
	      if(debug)printf("check_sorted: swapping transitions %d and %d (blocks %s and %s) with identical offsets (%10.4fms\n",
			      i,i+1,  ptrans[i].block_name, ptrans[i-1].block_name,
			      ptrans[i].t0_offset_ms);
	      swap(i-1,i,ptrans);
	      //print_sorted_transitions(ptrans,FALSE, stdout);
	      
	      // now add our identical transition BEFORE begin loop
	      if(debug)printf("now shifting down by 1 at index i-1=%d\n",i-1);
	      if(shift_trans_down(i-1, 1, ptrans) != SUCCESS)
		return DB_INVALID_PARAM;
	      
	      concat(ptrans[i-1].block_name,"_delay_b", COMBINED_BLOCKNAME_LEN);
	      
	      ptrans[i-1].bit =  0x40000000; // mark this as a delay		      
	      ptrans[i-1].code    = TRANS_CODE;
	      //print_sorted_transitions(ptrans,FALSE, stdout);
	      
	    }
		  
	  else  if( (d==0) && ptrans[i].code == ENDLOOP_CODE ) // end loop
	    {  /* user-supplied identical transision must come BEFORE end_loop */
	      if(debug)printf("check_sorted: swapping transitions %d and %d (blocks %s and %s) with identical offsets (%10.4fms)\n",
		     i,i+1,  ptrans[i].block_name, ptrans[i+1].block_name,ptrans[i].t0_offset_ms);
	      swap(i,i+1,ptrans);
	      //print_sorted_transitions(ptrans,FALSE, stdout);
	    }
	  
	} // end of either begin or end loop code
	  
      if(ptrans[i].code==ENDLOOP_CODE) // end loop
	{
	  if(debug)printf("detected end loop... checking for reserved block\n");
	  if(ptrans[i+1].code != RES_CODE) /* expect a reserved block (for the rest of the loops) 
					      immediately after the end loop */
	    {		
	      cm_msg(MERROR,"check_sorted",
		     "Error - illegal transition (block %s) in reserved block after end loop (block %s) \n",
		     ptrans[i+1].block_name, ptrans[i].block_name);
	      return DB_INVALID_PARAM;
	    }
	  
	  if(debug)printf("found endloop at transition %d\n",i);
	  
	  
	}
   
    next:   // next transition
      if(debug)printf("check_sorted_transitions: finished working on transition %d code=%d\n",
	     i,ptrans[i].code); 
    } // end of for loop on all transitions
  
  printf("After checking sorted transitions:\n");
  print_sorted_transitions(ptrans,FALSE, stdout); // bitpattern not yet calculated



  if(combine_transitions(ptrans) != SUCCESS)
    return DB_INVALID_PARAM;
  gbl_combined=TRUE; /* transitions may now be combined; alters printout in print_sorted */

  printf("After combining transitions:\n");  
  print_sorted_transitions(ptrans,TRUE, stdout);
  
  if(check_loops_trans(ptrans) != SUCCESS)
    return DB_INVALID_PARAM;


  return DB_SUCCESS;
}

#endif

INT check_loops_trans(TRANSITIONS *ptrans)
{
  /* check for
e.g.
index t0_offset(ms)  PPG bit          code  time_ref   block_index  block_name
002       10.0000                    01    T0             03      BEGIN_LOOP
003       10.0000    02 TDCBLOCK     00    T0             02      PULSE2
004      110.0000    02 TDCBLOCK     00    T0             02      PULSE2
005      110.0000                    02    L1             04      END_LOOP
   i.e.  this does not make sense
  */
  INT i,j,k,ol,ntrans;
  INT bi[MAX_LOOP_BLOCKS],bl[MAX_LOOP_BLOCKS],ei[MAX_LOOP_BLOCKS],el[MAX_LOOP_BLOCKS];
  j=k=0;
  ol=0;
  for(i=0; i<MAX_LOOP_BLOCKS; i++)


  if(debug)printf("check_loops_trans: starting\n");
  for (i=0; i< gbl_num_transitions; i++)
    {
      if(ptrans[i].code == BEGLOOP_CODE)
	{
	  bi[j]=i;
	  bl[j]=ptrans[i].loop_index;
	  j++;
	  ol++;
	}
      else if(ptrans[i].code == ENDLOOP_CODE)
	{
	  ei[k]=i;
	  el[k]=ptrans[i].loop_index;
	  k++;
	  ol--;
	}
     
    }

  if(k!=j)
    {
      cm_msg(MERROR,"check_loops_trans", "numbers of begin loop(%d) and end loop(%d) are different",j,k);
      return DB_INVALID_PARAM;
    }

  for (i=0; i<k; i++)
    {
      
      if(debug)printf("loop %d opens at transition %d",  i,bi[i]);
      for (j=0; j<k; j++)
	{
	  if(el[j]==bl[i])
	    {
	      if(debug)
		{
		  printf(" and closes at transition %d \n",ei[j]);
		  ntrans = ei[j] - bi[i];
		  printf("number of transitions for loop %d = %d\n", i, ntrans );
		  if(ntrans <=2)
		    { // change to a warning
		      cm_msg(MINFO,"check_loops_params","may not be not enough transitions in loop name %s",
			     loop_params[ptrans[i].loop_index].loopname);
		    }
		}
	    }
	}

 
    }
  return DB_SUCCESS;
}

	 
INT combine_transitions(TRANSITIONS *ptrans)
{
  DWORD my_bitpat,pattern;
  INT i,l,c;
  double ftmp;
  DWORD trans_pattern,my_trans_pattern;
  
  printf("\n\nCombine Transitions starting\n");
  /* Let's go through them again, this time looking 
     1  to combine transitions 
     2  for begin_loop that will end up as the first instruction
  */ 


  my_bitpat = 0;
  
  for (i=0; i< gbl_num_transitions; i++)
    {
      //if(debug)
	printf("\ncombine_transitions: working on transition %d (block %s)\n",
		      i, ptrans[i].block_name);
      //print_sorted_transitions(ptrans,FALSE, stdout);
      //  { //extra bracket
	if(ptrans[i].code > TRANS_CODE)
	{
	  ptrans[i].bitpattern = my_bitpat; // no change to bitpattern
	  if (ptrans[i].code == BEGLOOP_CODE) // begin loop
	    { // begin loop
	      if (i < 2 )
		{  // i<2
		  /* Begin loop near the start of a file; add a minimal delay */
		  if(i==0)
		    ftmp = ptrans[i].t0_offset_ms;
		  else
		    ftmp = ptrans[i].t0_offset_ms - ptrans[i-1].t0_offset_ms;

		  if(debug)
		    printf("combine_transitions: found begin_loop at start of file i=%d, ftmp=%f\n",i,ftmp);
		  c = compare  ( ftmp,  2*min_delay);
		
		  if (c > 0)
		    { 
		      printf("combine_transitions: adding a  delay at t0_offset = %f before block %s\n", 
			     (ptrans[i].t0_offset_ms),
			     ptrans[i].block_name);
		      if(shift_trans_down(i,1, ptrans) != SUCCESS)
			return DB_INVALID_PARAM;
		      //print_sorted_transitions(ptrans,FALSE, stdout);
		      /* add this extra delay to TRANSITIONS RECORD */
		      ptrans[i] = ptrans[i+1]; // mostly the same info
		      strcpy( ptrans[i].block_name,  ptrans[i+1].block_name );
		      
		      concat(ptrans[i].block_name,"_delay", COMBINED_BLOCKNAME_LEN);
		      
		      ptrans[i].bit =  0x40000000; // mark this as a delay		      
		      ptrans[i].code    = TRANS_CODE;
		      ptrans[i].t0_offset_ms =  ptrans[i].t0_offset_ms;
		      //print_sorted_transitions(ptrans,FALSE, stdout);
		    }
		      
		} // i<2
	    }	 
	} // end of non-transitions	
	else
	  {
	    pattern = get_pattern(my_bitpat, ptrans[i].bit); // returns new bit pattern;
	    if(debug)printf("old bit pattern = 0x%x new pattern 0x%x bit %d(0x%x)\n",my_bitpat,pattern,
		   (ptrans[i].bit & 0xFFFF), (ptrans[i].bit & 0xFFFF));
	    ptrans[i].bitpattern = pattern;
	    my_bitpat=pattern;
	    /* remember which bit was set for this transition */
	    trans_pattern =  get_pattern(0,ptrans[i].bit);
	  }
	
	while( ((i+1) < gbl_num_transitions) &&   
	       (ptrans[i].code == TRANS_CODE) && (ptrans[i+1].code == TRANS_CODE))
	  {
	    /* try to combine with next transition(s) */
	    {
	      ftmp = (ptrans[i+1].t0_offset_ms - ptrans[i].t0_offset_ms);
	      if(debug)printf("combine_transitions: ftmp=%f;  t0_offset_ms[%d]=%f next t0_offset_ms=%f\n",
		     ftmp, i,ptrans[i].t0_offset_ms, ptrans[i+1].t0_offset_ms);
	      c = compare  ( ftmp,  min_delay);
	      if(c >= 0 || i>= gbl_num_transitions)
		{
		  if(debug)printf("breaking with ftmp= %f c=%d i=%d gbl_num_transitions=%d\n",
				  ftmp, c,i, gbl_num_transitions);
		  break;   // avoid infinite loop
		}
	      /* Transitions are within minimal delay */
	      if(debug)
		printf("combine_transitions:found transitions %d and %d (blocks %s,%s)are within minimal delay\n",
		       i,i+1, ptrans[i].block_name, ptrans[i+1].block_name);
	      
	      /* Already checked for this in check_same; shouldn't get this */
	      if( (ptrans[i].bit & 0x80000000)||( ptrans[i+1].bit & 0x80000000))  // patterns
                {
		  cm_msg(MERROR,"check_sort_trans",
			 "cannot have a bitpattern and a transition within a minimal delay, transitions %d and %d, blocks  %s and %s",
			 i, i+1,ptrans[i].block_name, ptrans[i+1].block_name);
		  return DB_INVALID_PARAM;
	      
		}
	      else
		{ // neither transition is a bit pattern; 
		  /*   also checked for in check_same, shouldn't get this  */
		  my_trans_pattern =  get_pattern(0,ptrans[i+1].bit);
		  if(debug)		  
		    printf("Operating within min delay i=%d, my_trans_pattern = 0x%4.4x trans_pattern=0x%4.4x\n",
			   i, my_trans_pattern, trans_pattern );
		  if( (my_trans_pattern & trans_pattern) > 0)
		    {
		      cm_msg(MERROR,"check_sort_trans",
			     "cannot operate on the same bit within a minimal delay,  transitions %d and %d, blocks  %s and %s",
			     i, i+1,ptrans[i].block_name, ptrans[i+1].block_name);
		      return DB_INVALID_PARAM;
		      
		    }
		}
	      
	      //if(debug)
	      printf("\ncombine_transitions: **** combining transitions at index %d,%d,(blocks %s,%s) difference=%f d_min=%f\n",
		     i,i+1, ptrans[i].block_name, ptrans[i+1].block_name,ftmp, min_delay);
	      pattern = get_pattern(my_bitpat, ptrans[i+1].bit); // returns new bit pattern;
	      if(debug)
		printf("old bit pattern = 0x%x new pattern 0x%x bit %d (0x%x)\n",my_bitpat,pattern,
		       ptrans[i+1].bit, ptrans[i+1].bit);
	      ptrans[i].bitpattern =  pattern;
	      my_bitpat=pattern;
	      
	      l=strlen(ptrans[i].block_name);
	      if ((l + 1 + strlen(ptrans[i+1].block_name) + 1) > COMBINED_BLOCKNAME_LEN+1)
		{
		  printf("cannot combine block names; out of room at i=%d blockname=%s length %d\n",
			 i,ptrans[i].block_name,l);
		  printf(" next blockname[%d]=%s length %d\n",
			 i+1,ptrans[i+1].block_name, strlen(ptrans[i+1].block_name));
		}
	      else
		{
		  concat(ptrans[i].block_name,  ",", COMBINED_BLOCKNAME_LEN);
		  concat(ptrans[i].block_name,ptrans[i+1].block_name , COMBINED_BLOCKNAME_LEN);
		}
	      //printf(" ptrans[%d].bit=%d\n",i+1, ptrans[i+1].bit);
	      pattern = get_pattern(trans_pattern, ptrans[i+1].bit); // get new pattern for this transition
	      trans_pattern=pattern;
	      if(debug)
		printf("This transition now has bit pattern 0x%x; overall bit pattern is 0x%x\n",
		       trans_pattern, my_bitpat);
	      shift_trans_up(i+1, ptrans);// decrements gbl_num_transitions

	      if(debug)
		{
		  print_sorted_transitions(ptrans,TRUE, stdout);
		  printf("*** Note: bitpattern not yet calculated after index %d\n",i);
		}
	    }
	  next:
	    if(debug)
	      printf("next transition\n");
	   

	  } // end of while loop on combining transitions
	
	
	//} extra bracket
	trans_pattern =0;
    }
  
  
  return SUCCESS;
}



void swap(INT index_a, INT index_b, TRANSITIONS *trans)
{
  /* swap over two elements of structure */

    TRANSITIONS ttemp[2];
    INT i;


    ttemp[1] = trans[index_a];
    ttemp[0] = trans[index_b];

   
    trans[index_a]=ttemp[0];
    trans[index_b]=ttemp[1];
    return;
}



INT shift_trans_down (INT index, INT shift_by,  TRANSITIONS *trans)
{
  /*  INDEX  shift down at index
      SHIFT_BY     how many to shift down by

      shift trans structure down so we can add shift_by extra transitions
      at index (index becomes index+1)
      e.g. shift down at index 2 by 1 gives 
      index contents  becomes  index   contents
      0       100                0       100
      1       120                1       120
      2       400                2       400 <-index 2 contents is unchanged
      3       405                3       400 <-index 3 contains copy of index 2
      4       600                4       405 <-index 4 contains previous contents of index 3
                                 5       600
  */

  
  INT i,z,y;
  
  z=gbl_num_transitions -1 ; // largest index
  
  if(gbl_num_transitions + shift_by> MAX_TRANSITIONS )
    {
      cm_msg(MERROR,"sort_transitions",
	     "too many transitions for array size (max=%d)",MAX_TRANSITIONS);
      printf("sort_transitions: too many transitions for maximum (%d). Increase MAX_TRANSITONS\n",MAX_TRANSITIONS);
      return DB_INVALID_PARAM;
    }
  if(debug)printf("Shifting down from index %d by %d\n",index,shift_by);
  //	printf("z=%d, index=%d\n",z,index);

  while(z>index)
    {
      trans[z+shift_by]=trans[z];
      /*printf("trans[%d].code=%d; trans[%d].code=%d\n",
	z+shift_by, trans[z+shift_by].code,z,trans[z].code);*/
      z--;
    }
  /* copy the contents at index to the "new" transitions */
  for(i=0; i<shift_by; i++)
    trans[index+i+1]=trans[index];
    
  gbl_num_transitions = gbl_num_transitions + shift_by;
  
  return SUCCESS;
}

INT shift_trans_up (INT index, TRANSITIONS *trans)
{
  /* shift trans up at index so that index is overwritten
   e.g. shift up at index 2 gives 
  index contents  becomes  index   contents
  0       100                0       100
  1       120                1       120
  2       400                2       405
  3       405    
  */

  INT i,z;
  
  z=index;
  
  //	printf("z=%d, index=%d\n",z,index);
  while (z < gbl_num_transitions-1)
    {
      trans[z]=trans[z+1];
      z++;
    }
  gbl_num_transitions--;
  return SUCCESS;
}

 
DWORD get_pattern(DWORD pattern, DWORD bit)
{

  DWORD my_pattern,shift;
  if(debug)printf("get_pattern: starting with pattern=0x%x and bit %d(0x%x)\n",pattern,bit,bit);
  if(bit & 0x80000000) // this is a bit pattern; replace pattern
    {
      if(debug)
	printf("detected PATTERN;  returning 0x%x\n",(bit  & 0x7FFFFFFF));
      return (bit &  0x7FFFFFFF);
    }

  if(bit & 0x40000000) // this is a delay. No change to bit pattern
  {
      if(debug)printf("get_pattern: detected DELAY; returning unchanged pattern 0x%x\n",pattern);
      return pattern;
  }

  shift=1<<bit;
  my_pattern = pattern & shift;
  if(my_pattern)
    { 
      //      printf("bit %d is currently true (shift=0x%x)\n",bit,shift);
      shift = shift ^ 0xFFFFFFFF;
      my_pattern = pattern & shift;
    }
  else
    {
      //printf("bit %d currently false (shift=0x%x)\n",bit,shift);
      my_pattern = pattern | shift;
    }
  if(debug)printf("get_pattern: returning bit pattern 0x%x\n",my_pattern);
  return my_pattern;
}

#ifdef AUTO_TDCBLOCK

INT add_tdcblocks( TRANSITIONS *ptrans, TDCBLOCK_PARAMS *ptdcblock)
{
  typedef    struct {
    double start;
    double end;
  } TDC;
  
  TDC tdc[MAX_PULSE_BLOCKS];
  
  double width,diff, tmp,minimal;
  INT i,j,c,index,my_index;
  INT inum;
  DWORD bitpat;


  printf("add_tdcblocks: calling tdc_compress\n");
  tdc_compress(ptdcblock);
  print_tdcblock_params();

#ifndef POL
  width = settings.ppg.input.tdcblock_width__ms_;
  minimal = settings.ppg.output.minimal_delay__ms_;
#else
   width = settings.input.tdcblock_width__ms_;
  minimal = settings.output.minimal_delay__ms_;
#endif
 
  /* combine any overlapping tdcblock pulses */   
  index=j=0;
  while (j < gbl_num_tdcblock_pulses )
    { 
      tdc[index].start=ptdcblock[j].t0_offset_ms;
      tdc[index].end=ptdcblock[j].t0_offset_ms + width;
      printf("add_tdcblocks: tdc[%d].start=%f end=%f (j=%d)\n",
             index, tdc[index].start, tdc[index].end, j);
      my_index = index; 
      j++;
      while (my_index == index && j < gbl_num_tdcblock_pulses)
	{  // compare end of one pulse to start of next
	  diff = tdc[index].end - ptdcblock[j].t0_offset_ms;
	  c = compare( fabs(diff),minimal);
	  if(c <=0) // within min delay of start of next pulse
            {
	      if(debug)
		printf("j=%d combining; end (%.4f) within min delay of start of next pulse (%.4f)\n",
		       j,tdc[index].end,  ptdcblock[j].t0_offset_ms);
     
      
	      tdc[index].end = ptdcblock[j].t0_offset_ms + width; // combine
	      if(debug) printf("set tdc[%d].end to tdcblock[%d]=%f + width=%f ->%f\n",
			       index,j, ptdcblock[j].t0_offset_ms, width,tdc[index].end);
	      j++;
	    }
	  else if( compare ( tdc[index].end,  tdcblock[j].t0_offset_ms ) >0)
            { // now look for overlapping pulses, next pulse has std pulsewidth 
	      if(debug)
		printf("combining overlapping pulses at j=%d; end (%.4f) is < end of next pulse (%.4f)\n",
		       j,tdc[index].end,  tdcblock[j].t0_offset_ms);
	      tdc[index].end = tdcblock[j].t0_offset_ms + width; // combine
	      j++;
	    }
	  else
	    { // cannot combine this pulse with next. Not within min delay or overlapping
	      if(debug)
		printf("add_tdcblocks:pulse at j=%d cannot be combined; incrementing index to %d\n",
		       j,index+1);
	      index++;
            }
	} // end of trying to combine pulses
    } // end of outer while
  if(debug) printf("add_tdcblocks: after while, j=%d index=%d\n",index,j);
  index++; // number of values in tdc structure


  printf("add_tdcblocks: TDCBLOCK pulses after combining: \n");
  for (j=0; j<index; j++)
    {
      printf("tdc[%d].start=%f  tdc[%d].end  = %f\n",
	     j,tdc[j].start, j, tdc[j].end);
      
    }

  /* Now add these to the transitions structure */

  if(gbl_num_transitions +( 2* index) > MAX_TRANSITIONS )
    {
      cm_msg(MERROR,"add_tdcblocks","need to add %d transitions; Increase MAX_TRANSITIONS (max=%d)",
	     (2*index),MAX_TRANSITIONS);
      return DB_INVALID_PARAM;
    }
  
  /* Find the PPG output number of TDCBLOCK pulse */
  inum = check_ppg_name("TDCBLOCK");
  //printf("after check_ppg_name i=%d my_index=%d\n",i,my_index);
  if(inum == -1)
    {
      cm_msg(MERROR,"add_tdcblocks","no such PPG signal name as \"TDCBLOCK\" ",
	     inum);
      return DB_INVALID_PARAM;
    }
  bitpat = 1<< inum;

  printf("add_tdcblocks: Combined TDCBLOCK pulses: start and end\n");
  for (j=0; j<index; j++)
    {           
      /* turn this into transitions */
      sprintf( ptrans[gbl_num_transitions].block_name,"TDCBLK_S%d",j );
      ptrans[gbl_num_transitions].time_reference_index =   0; // T0
      ptrans[gbl_num_transitions].code  = TRANS_CODE; // this is a transition 
      ptrans[gbl_num_transitions].block_index    = 999; // not used
      ptrans[gbl_num_transitions].t0_offset_ms  = tdc[j].start;
      ptrans[gbl_num_transitions].bit = inum;
      ptrans[gbl_num_transitions].bitpattern = bitpat;
      gbl_num_transitions++;
      
      sprintf( ptrans[gbl_num_transitions].block_name,"TDCBLK_E%d",j );
      ptrans[gbl_num_transitions].time_reference_index =   0; // T0
      ptrans[gbl_num_transitions].code  = TRANS_CODE; // this is a transition 
      ptrans[gbl_num_transitions].block_index    = 999; // not used
      ptrans[gbl_num_transitions].t0_offset_ms  = tdc[j].end;
      ptrans[gbl_num_transitions].bit = inum;
      ptrans[gbl_num_transitions].bitpattern =  bitpat;
      gbl_num_transitions++;
    }

  print_transitions(ptrans,stdout);
  printf("add_tdcblocks: returning success\n");
  return SUCCESS;
}





void sort_tdcblock( TDCBLOCK_PARAMS *ptdcblock)
{
  INT j;
  /* start sorting */
  
  print_tdcblock_params();

  if(gbl_num_tdcblock_pulses > 1)
    {
      qsort( ptdcblock, gbl_num_tdcblock_pulses, sizeof(TDCBLOCK_PARAMS),
	     tdc_compare);
      
      if(debug)
	{
	  printf("sort_tdcblock: after qsort\n");
	  print_tdcblock_params();
	}
    }
  return;
}

void tdc_compress(TDCBLOCK_PARAMS *ptdcblock)
{
  /* if start values are less than minimal delay, combine by removing
     one of the pulses  
     ( start values are already sorted; widths are identical at this stage)
  */

  INT i,j;
  double diff;

  printf("TDCBLOCK sorted data :\n");
  for(j=0; j<gbl_num_tdcblock_pulses; j++)
    printf("j=%d t0_offset_ms = %.4f\n",j,ptdcblock[j].t0_offset_ms);

  for(j=1; j<gbl_num_tdcblock_pulses; j++)
    {
      diff = ptdcblock[j].t0_offset_ms - ptdcblock[j-1].t0_offset_ms;
    
      if( compare (diff, settings.ppg.output.minimal_delay__ms_)  < 0)
	{
	  printf("shifting up at index j=%d; diff=%.4f, orig pulse index %d is combined with %d\n",
		 j,diff,(j-1), ptdcblock[j-1].pulse_index, ptdcblock[j].pulse_index	 );
	  shift_tdc_up(j,ptdcblock);
	  if(debug)print_tdcblock_params();
	  j--; // remove 1 from j so we don't skip a comparison
	}
    }
  printf("After compressing, gbl_num_tdcblock_pulses = %d\n", gbl_num_tdcblock_pulses);
  print_tdcblock_params();

}

INT shift_tdc_up (INT index, TDCBLOCK_PARAMS *ptdcblock)
{
  /* shift ptdcblock up at index so that index is overwritten
   e.g. shift up at index 2 gives 
  index contents  becomes  index   contents
  0       100                0       100
  1       120                1       120
  2       400                2       405
  3       405    
  */

  INT i,z;
  
  z=index;
  
  //	printf("z=%d, index=%d\n",z,index);
  while (z <gbl_num_tdcblock_pulses -1)
    {
      ptdcblock[z]=ptdcblock[z+1];
      z++;
    }
  gbl_num_tdcblock_pulses--;
  return SUCCESS;
}
	   
	
    






int tdc_compare(const void *arg1, const void *arg2) 
{
  float r;
  r = (float) ((TDCBLOCK_PARAMS *)arg1)->t0_offset_ms - ((TDCBLOCK_PARAMS *)arg2)->t0_offset_ms;
  if(r>0.0)
    return 1;
  else if (r<0.0)
    return -1;
  else 
    return 0;
}



#endif // AUTO_TDCBLOCK
