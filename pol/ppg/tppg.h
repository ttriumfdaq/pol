/*   tppg.h
     Author: Suzannah Daviel 

     Include file for VMEIO PPG (Pulse Programmer) for TITAN

  $Id: tppg.h,v 1.1 2011/04/15 18:50:31 suz Exp $
*/
#ifndef _TPPG_INCLUDE_H_
#define _TPPG_INCLUDE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef long unsigned int       DWORD;
#endif /* MIDAS_TYPE_DEFINED */
#include "vmicvme.h"

#define MINIMAL_DELAY 0 // 3 clock cycles 
// Masks
  

// Registers
#define TPPG_PPG_TEST            0x00   /*      DWORD   RW  */
#define TPPG_PPG_CSR_REG         0x04   /*      DWORD   RW  */
#define TPPG_PPG_ADDR            0x08   /*      DWORD   RW  Address 1-255   */
#define TPPG_PPG_DATA_LO         0x0C   /*      DWORD   RW  data bits 0-31  */
#define TPPG_PPG_DATA_MED        0x10   /*      DWORD   RW  data bits 32-63 */
#define TPPG_PPG_DATA_HI         0x14   /*      DWORD   RW  data bits 64-95 */
#define TPPG_PPG_DATA_TOP        0x18   /*      DWORD   RW  data bits 64-95 */
#define TPPG_PPG_INV_MASK        0x1C   /*      DWORD   RW  data bits 64-95 */
#define TPPG_FIRMWARE_ID         0x20   /*      DWORD   R  */
// Instructions
#define HALT_INS 0
#define CONT_INS 1
#define LOOP_INS 2
#define ENDLOOP_INS 3
#define CALL_INS 4
#define RETURN_INS 5
#define BRANCH_INS 6

// shifted instructions
#define CONT 0x00100000
#define BEGLOOP 0x00200000
#define ENDLOOP 0x00200000
#define HALT 0x0

//  firmware date
#define FWTBUFSIZ 128
#define FWDATESTRLEN 13 /* Length of formatted data string */

#define MAX_COUNT 63

struct parameters
{
    unsigned char   opcode;
    unsigned long   branch_addr;
    unsigned long   delay;
    unsigned long   flags;
    char            opcode_width;
    char            branch_width;
    char            delay_width;
    char            flag_width;
};
typedef struct parameters PARAM;

static char *instructions[4]={"HALT","DELAY_SET","LOOP","ENDLOOP"};

void  TPPGInit(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD bitpat );

int   TPPGLoad(MVME_INTERFACE *mvme,const DWORD base_adr, char *file);

void  TPPGDisable(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGEnable(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGStatusRead(MVME_INTERFACE *mvme, const DWORD base_adr);
PARAM lineRead( FILE *input);
void  byteOutputOrder(PARAM data, char *array);
DWORD  TPPGRegWrite(MVME_INTERFACE *mvme, const DWORD base_adr, 
			 DWORD reg_offset, DWORD value);
DWORD  TPPGRegRead(MVME_INTERFACE *mvme, const DWORD base_adr, 
			 DWORD reg_offset);

BYTE  TPPGDisableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr);
BYTE  TPPGEnableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGStopSequencer(MVME_INTERFACE *mvme, const DWORD base_adr);
void  TPPGStartSequencer(MVME_INTERFACE *mvme, const DWORD base_adr);
BYTE  TPPGExtTrigRegRead(MVME_INTERFACE *mvme, const DWORD base_adr);
DWORD TPPGSetIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr, DWORD instruction, 
		 DWORD setbits, DWORD clrbits, DWORD delay);
DWORD TPPGReadIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr);
char *firmware_date(time_t val);

int getline(char *line, int max);
int getcmd(char *command);
int findcmt(char *string);
#endif
