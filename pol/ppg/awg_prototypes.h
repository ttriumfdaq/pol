#ifndef  AWG_PROTO_H
#define  AWG_PROTO_H

void close_AWG(INT unit);

INT config_AWG(INT unit);
/*INT AwgConfLoad( INT Nsample, DWORD last_address,
			 INT clock_mode ,
			 INT configuration,
			 INT mode_number,
			 INT update_mode);*/
INT AwgMemLoad(double Vstart, double Vend, INT nsteps, INT step_start, INT buffer, 
		 INT AWG_channel, INT AWG_unit);
INT AwgSetSample( INT awg_unit, int nsamples, int offset, int ibuf, int maxchan, int flag, int bank);
//INT da816BankSwitch(void);
//INT da816HandUpdate(double Volt, INT AWG_channel);
#endif
