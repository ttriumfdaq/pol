/* 
   Name: tri_config.c
   Created by: SD

   Contents: Main configuration program -  for TRIUMF Titan Experiment (mpet, ebit).
             Converts information blocks from odb 
	         writing a time/bitpattern file to be downloaded into the PPG
		 loading the AWG module(s) 
		 writing files to be plotted by gnuplot
                 etc.
 $Id: tri_config.c,v 1.1 2011/04/15 18:50:31 suz Exp $

                                         
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>  // needed to interpret errors after "system" call
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "tri_config.h"
#include "parameters.h"
#include "tri_config_prototypes.h"
#include "info_blocks.h"


  /* ODB keys */
char eqp_name[32];
BOOL single_shot;
INT size;
INT error_flag=0;
BOOL first_msg=1;
FILE *tfile;
/* ODB */

#ifndef POL
TITAN_ACQ_SETTINGS settings;
#else
POL_ACQ_SETTINGS settings;
#endif
double min_delay;
HNDLE hDB, hSET, hPPG,hIn,hOut,hAWG0,hAWG1,hCycle,hLEGI;
#ifdef EBIT
HNDLE hLoops=0;  // EBIT needed for graphical interface (only on EBIT at present)
#endif
INT block_list[MAX_BLOCKS]; // list of block types (shifted by TYPE_SHIFT) & block numbers
TRANSITIONS p_transitions[MAX_TRANSITIONS];
TRANSITIONS *ptrans;
#ifdef AUTO_TDCBLOCK
TDCBLOCK_PARAMS *ptdc;
#endif

BOOL gbl_skip=FALSE;
INT run_number;
char data_dir[256];

typedef struct {
   int flags;
   char pattern[32];
   int index;
} PRINT_INFO;

PRINT_INFO print_info;

#define PI_LONG      (1<<0)
#define PI_RECURSIVE (1<<1)
#define PI_VALUE     (1<<2)
#define PI_HEX       (1<<3)
#define PI_PAUSE     (1<<4)

static char data[50000];
INT  ls_line, ls_abort;

  





/*------------------------------------------------------------------*/
INT settings_rec_get(void)
{
  /* retrieve the ODB structure
     check if the rule file exists in the given dir
     return the rulefile
  */
  INT size, status,j;
  char str[128];


 /* get the record for hOut */
#ifndef POL
  size = sizeof(settings.ppg.output);
  status = db_get_record(hDB, hOut, &settings.ppg.output, &size, 0);
#else // POL
  size = sizeof(settings.output);
  status = db_get_record(hDB, hOut, &settings.output, &size, 0);
#endif
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for Output  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Output  (%d)",status);
      return (-1);
    }
  
  /* get the record for hIn */
#ifndef POL
  size = sizeof(settings.ppg.input);
  status = db_get_record(hDB, hIn, &settings.ppg.input, &size, 0);
#else // POL
  size = sizeof(settings.input);
  status = db_get_record(hDB, hIn, &settings.input, &size, 0);
#endif
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for Input  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Input  (%d)",status);
      return (-1);
    }

#ifndef NO_AWG
  /* get the record for AWG unit 0 */
  size = sizeof(settings.awg0);
  status = db_get_record(hDB, hAWG0, &settings.awg0, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for AWG unit 0  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for AWG unit 0 (%d)",status);
      return (-1);
    }



  /* get the record for AWG unit 1 */
  size = sizeof(settings.awg1);
  status = db_get_record(hDB, hAWG1, &settings.awg1, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for AWG unit 1  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for AWG unit 1 (%d)",status);
      return (-1);
    }
#endif // NO_AWG
  printf("settings_rec_get: returning success\n");
  return SUCCESS;

}



void print_some_info(void)
{
  
  if( gbl_num_time_references_defined > 0)
    {
      if(single_shot)
	print_ref_blocks(stdout);
      print_ref_blocks(tfile);
    }

  if(gbl_num_loops_defined > 0)
    {
      if(single_shot)
	print_loop_params(stdout);
      print_loop_params(tfile);
#ifdef EBIT
      output_loops();
#endif
    }
  

  print_transitions(ptrans,stdout);
  print_transitions(ptrans,tfile);
  
  
  printf("\n");
  
}

/*--------------------------------------------------------*/
INT get_input_data(HNDLE hKey)
{
  INT status;
  HNDLE hSubkey;
  KEY key;
  INT i,j;
  INT swapped;
  char tfile_name[128];
  char cmd[128];
  char path[128];

  print_info.flags = 0;
  print_info.pattern[0] = 0;
  ls_line = 0;


  /* get info for tfile */
  size = sizeof(run_number);
  status = db_get_value(hDB, 0, "/Runinfo/run number", &run_number, &size, TID_INT, 0);
 
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_input_data","error getting run number (%d)",status);
      return status;
    }

  size = sizeof(data_dir);
  status = db_get_value(hDB, 0, "/logger/data dir",
                                   data_dir, &size, TID_STRING, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_input_data","error getting \"data dir\" (%d)",status);
      return status;
    }


  sprintf(tfile_name,"%s/%s/run%d_%s",data_dir,"info",run_number,"config.txt");
  printf("get_input_data: about to open debug file: %s\n",tfile_name);
  tfile = fopen(tfile_name, "w");
  
  if(! tfile)
    {
      cm_msg(MERROR,"get_input_data","error opening output transitions file %s", tfile_name);
      printf("get_input_data: error opening output transitions file %s\n", tfile_name);
      return -1;
    }

#ifndef POL
  strcpy(path,settings.ppg.input.ppg_path);
#else
  strcpy(path,settings.input.ppg_path);
#endif
  sprintf(cmd, "rm %s/infofile",path);
  status = system (cmd);
  printf("get_input_data: after cmd: %s status=%d\n",cmd,status);

  sprintf(cmd, "ln -s %s %s/infofile",tfile_name, path);
  status = system (cmd);
  printf("get_input_data: after cmd: %s status=%d\n",cmd,status);

  printf("get_input_data: linked %s to %s/infofile\n",tfile_name, path);


  if (hKey) 
    {
      if(debug)printf("get_input_data: calling db_scan_tree with hKey=%d\n",hKey);
      status = db_scan_tree(hDB, hKey, 0, print_key, &print_info);
      if(status != DB_SUCCESS || error_flag)
	{
	  printf("get_input_data: ERROR... after db_scan_tree, status=%d error_flag=%d\n",status,error_flag);
	  printf("get_input_data: PRINTING after error detected\n");
	  print_some_info();	  
	  return DB_INVALID_PARAM;
	}

      if ( block_counter[TOTAL] <= 3) // two blocks are defined by default (T0 and pattern + "ppg cycle" dir)
	{
	  printf("get_input_data: no user blocks defined in \"ppg cycle\"; nothing to do\n");
	  cm_msg(MINFO,"get_input_data","INFO: there are no user blocks defined in ODB under \"ppg cycle\"");
	  return DB_INVALID_PARAM;
	}

        if(debug)
	{
	  printf("get_input_data:total number of blocks : %d\n", block_counter[TOTAL]);
	 
	  	  for (i=0; i< block_counter[TOTAL]; i++)  // from 0 to total number of blocks
	   printf("get_input_data:block_list[%d] : 0x%x\n", i,block_list[i]) ;
	}
    }


  printf("get_input_data:PRINTING\n");
  print_some_info();

  
  if( (block_counter[BEGIN_LOOP] ) != (block_counter[END_LOOP] ) )
    {
      cm_msg(MERROR,"get_input_data","ERROR: there are %d begin loops and %d  end loops; they must match\n",
	     block_counter[BEGIN_LOOP], block_counter[END_LOOP]     );
      return DB_INVALID_PARAM;
    }

  print_ref_blocks(stdout);

#ifdef AUTO_TDCBLOCK
  /* Now sort out TDCBLOCK pulses - call before SORT (or would need to re- sort after)
     Adds them to TRANSITIONS
  */ 
  printf("get_input_data: calling sort_tdcblock\n");
  sort_tdcblock(ptdc);
 
  printf("get_input_data: calling add_tdcblocks\n");
  if (add_tdcblocks(ptrans,ptdc) != SUCCESS)
    return DB_INVALID_PARAM;

  printf("\n After adding TDCBLOCK transitions:\n");

#endif // AUTO_TDCBLOCK
  print_transitions(ptrans,stdout);

  printf("\nSorting transitions...\n");
  sort_transitions(ptrans);


  print_sorted_transitions(ptrans,FALSE,stdout); // bitpattern not filled; prints bit instead
  print_sorted_transitions(ptrans,FALSE,tfile); // print to file transitions.txt


 

  status = precheck_sorted_transitions(ptrans); // gets beg/end run in correct order
  if(status != SUCCESS)
    return status;

  /* 
*****    Do not add extra Clock pulse - not needed

  if ((block_counter[EV_SET] > 0) ||  (block_counter[EV_STEP] > 0))
    {
      /* if there are EV blocks, add final clock pulse(s) before end-of-scan loop */
  /*
      printf ("get_input_data: EV block(s) found; adding extra clock pulse(s) to reset AWG(s) \n");
      //      if( add_last_awg() != SUCCESS)
      //	{
      //  cm_msg(MERROR,"get_input_data","error from add_last_awg");
      //  return DB_INVALID_PARAM;
      //}
    
// re-sort
       printf("\nResorting transitions ...\n");
       sort_transitions(ptrans);
       precheck_sorted_transitions(ptrans); // gets beg/end run in correct order
      }

  fprintf(tfile,"Re-sorted after adding last EV clock pulse(s) \n");
  print_sorted_transitions(ptrans,FALSE,tfile); // print to file transitions.txt
  */  


  /*
 ***  Cannot add scan loop automatically any more; has to be present in the Block List in ODB


    status = auto_add_loop(0,1,ptrans); // add end-of-scan loop
    if(status != SUCCESS)
    {
      cm_msg(MERROR,"tr_precheck","error adding end of scan loop");
      return status;
    }
  */



  /* if there are EV blocks, deal with them now */
  if ((block_counter[EV_SET] > 0) ||  (block_counter[EV_STEP] > 0))
    {
#ifdef NO_AWG
      cm_msg(MERROR,"get_input_data","EV blocks are present but no AWG is defined in this code");
      return  DB_INVALID_PARAM;
#else
      if (awg_params[0].in_use ==0 && awg_params[1].in_use==0)
	{
	  cm_msg(MERROR,"get_input_data","EV blocks are present but neither AWG is in use");
	  return  DB_INVALID_PARAM;
	}

      for (i=0; i<NUM_AWG_MODULES; i++)
	{
	  if(awg_params[i].in_use)
	      status = config_AWG(i);
	  if(status != SUCCESS)
	    return status;
	}

      /* load AWG; removes EV_SET and EV_STEP from transition list */  
      if( check_ev(ptrans) != SUCCESS)
	{
	  printf("get_input_data: error from checking EV_SET and EV_STEP blocks\n");
	  return DB_INVALID_PARAM;
	}

      // load AWG Unit(s)
      /*  for (i=0; i<NUM_AWG_MODULES; i++)
	{
	  if(awg_params[i].in_use) // if Unit is in use
	  {*/	  
	      if( load_AWG_mem(ptrans) != SUCCESS)
		return DB_INVALID_PARAM;
	      /* }
		 }*/

	      for (i=0; i<NUM_AWG_MODULES; i++)
		{
		  if(awg_params[i].in_use)
		    close_AWG(i);
		}

      
      fprintf(tfile,"EV blocks have been removed \n");
      print_sorted_transitions(ptrans,TRUE, tfile); // with bitpats      
    }
  else
    {
    //    printf("get_input_data: no EV blocks found; no need to add extra clock pulse to reset AWG \n");
  
      printf("get_input_data: no EV blocks found; AWG(s) will not be loaded\n");
      cm_msg(MINFO,"get_input_data","no EV blocks found; AWG(s) will not be loaded");
#endif // NO_AWG 
    }

  printf("\n Checking sorted transitions...\n");
  status = check_sorted_transitions(ptrans);
  if(status != DB_SUCCESS)
    {
      if(single_shot)
	print_sorted_transitions(ptrans,TRUE, stdout); // with bitpats
      fprintf(tfile, "Error after checking sorted transitions\n");
      print_sorted_transitions(ptrans,TRUE, tfile); // with bitpats
      return status;
    }
  
  printf("\n");
  return status;
}



INT print_key(HNDLE hDB, HNDLE hKey, KEY * key, INT level, void *info)
{
  INT i, size, status;
  static char data_str[50000], line[256];
  DWORD delta;
  PRINT_INFO *pi;
  HNDLE hmykey;
  INT total_size;
  
  //printf("print_key: starting with hKey=%d\n",hKey);
  if(error_flag) 
    {
      if(debug)
	{
	  if(first_msg) printf("print_key: returning because error_flag is set\n");
	  first_msg=0;
	}
      return DB_INVALID_PARAM;
    }

  pi = (PRINT_INFO *) info;
  
 
  size = sizeof(data);

  if (pi->flags & PI_VALUE) 
    {
      /* only print value */
      if (key->type != TID_KEY) 
	{
	  status = db_get_data(hDB, hKey, data, &size, key->type);
	  if (status == DB_NO_ACCESS)
	    strcpy(data_str, "<no read access>");
	  else
	    for (i = 0; i < key->num_values; i++) 
	      {
		if (pi->flags & PI_HEX)
		  db_sprintfh(data_str, data, key->item_size, i, key->type);
		else
		  db_sprintf(data_str, data, key->item_size, i, key->type);
		
		if ((pi->index != -1 && i == pi->index) || pi->index == -1)
		  printf("%s\n", data_str);
		if(error_flag)return 0;
		//	if (check_abort(pi->flags, ls_line++))
		// return 0;
	      }
	}
    } 
  else
    {
      /* print key name with value */
      memset(line, ' ', 80);
      line[80] = 0;
      sprintf(line + level * 4, "%s", key->name);
      if (pi->index != -1)
	sprintf(line + strlen(line), "[%d]", pi->index);
      line[strlen(line)] = ' ';
      if (key->type == TID_KEY) 
	{
	  if (pi->flags & PI_LONG)
	    sprintf(line + 32, "DIR");
	  else
	    line[32] = 0;
	  if(debug)printf("%s; KEY level %d\n", line,level);
	  //if (level == 1)
	    {
	      if(get_block(hKey, key) != DB_SUCCESS)
		{
                  printf("print_key: failure from get_block, setting error flag\n");
		  error_flag=1; //set error flag
		  return DB_INVALID_PARAM;
		}
	    }
	  
	}
      else 
	{ 
	  status = db_get_data(hDB, hKey, data, &size, key->type);
	  if (status == DB_NO_ACCESS)
	    strcpy(data_str, "<no read access>");
	  else {
	    if (pi->flags & PI_HEX)
	      db_sprintfh(data_str, data, key->item_size, 0, key->type);
	    else
	      db_sprintf(data_str, data, key->item_size, 0, key->type);
	  }
	  
	  if (pi->flags & PI_LONG) {
	    sprintf(line + 32, "%s", rpc_tid_name(key->type));
	    line[strlen(line)] = ' ';
	    sprintf(line + 40, "%d", key->num_values);
	    line[strlen(line)] = ' ';
	    sprintf(line + 46, "%d", key->item_size);
	    line[strlen(line)] = ' ';
	    
	    db_get_key_time(hDB, hKey, &delta);
	    if (delta < 60)
	      sprintf(line + 52, "%lds", delta);
	    else if (delta < 3600)
	      sprintf(line + 52, "%1.0lfm", delta / 60.0);
	    else if (delta < 86400)
	      sprintf(line + 52, "%1.0lfh", delta / 3600.0);
	    else if (delta < 86400 * 99)
	      sprintf(line + 52, "%1.0lfh", delta / 86400.0);
	    else
	      sprintf(line + 52, ">99d");
	    line[strlen(line)] = ' ';
	    
            sprintf(line + 57, "%d", key->notify_count);
            line[strlen(line)] = ' ';
	    
            if (key->access_mode & MODE_READ)
	      line[61] = 'R';
            if (key->access_mode & MODE_WRITE)
	      line[62] = 'W';
	    if (key->access_mode & MODE_DELETE)
	      line[63] = 'D';
            if (key->access_mode & MODE_EXCLUSIVE)
	      line[64] = 'E';
	    
            if (key->type == TID_STRING && strchr(data_str, '\n'))
	      strcpy(line + 66, "<multi-line>");
            else if (key->num_values == 1)
	      strcpy(line + 66, data_str);
            else
	      line[66] = 0;
	  } 
	  else if (key->num_values == 1)
	    if (key->type == TID_STRING && strchr(data_str, '\n'))
	      strcpy(line + 32, "<multi-line>");
	    else
	      strcpy(line + 32, data_str);
	  else
	    line[32] = 0;
	
	  printf("%s\n", line);
	  
	  if (key->type == TID_STRING && strchr(data_str, '\n'))
	    puts(data_str);
	 
	  if(error_flag)return 0;
	  //if (check_abort(pi->flags, ls_line++))
	  //  return 0;
	  
	  if (key->num_values > 1) 
	    {
	      for (i = 0; i < key->num_values; i++) 
		{
		  if (pi->flags & PI_HEX)
		    db_sprintfh(data_str, data, key->item_size, i, key->type);
		  else
		    db_sprintf(data_str, data, key->item_size, i, key->type);
		  
		  memset(line, ' ', 80);
		  line[80] = 0;
		  
		  if (pi->flags & PI_LONG) 
		    {
		      sprintf(line + 40, "[%d]", i);
		      line[strlen(line)] = ' ';
		      
		      strcpy(line + 56, data_str);
		    } 
		  else
		    strcpy(line + 32, data_str);
		  
		  if ((pi->index != -1 && i == pi->index) || pi->index == -1)
		    printf("%s\n", line);
		  
		  if(error_flag)return 0;
		  //if (check_abort(pi->flags, ls_line++))
		  // return 0;
		}
	    }
	}

    }  
  return SUCCESS;
}
/*-----------------------------------------------------------------*/
INT get_block(HNDLE hKey, KEY *key  )
{
  INT i,j,len, size, status, total_size, my_index, block_index;
  INT skip;
  char Name[32];
  char str[128];
  
  status = DB_SUCCESS;
  skip=FALSE;
  
  /*printf("get_block: starting with hKey=%d and key->name = %s \n",
    hKey,key->name );
  */
  block_index=block_counter[TOTAL];
  printf("get_block: block_index=%d\n",block_index);

  
  /* check record size */
  status = db_get_record_size(hDB, hKey, 0, &total_size);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_block","error getting record size for %s (%d)",
	     key->name,status);
      return status;
    }
  
  len = strlen(key->name);
  if(len >= sizeof(Name))
    {
      cm_msg(MERROR,"get_block",
	     "Key name %s is too long for temp array (%d). Max len=%d ",key->name,len,sizeof(Name));
      return DB_INVALID_PARAM;
    }
  
  for   (j=0; j<len; j++)
    {
      Name[j]  = toupper (key->name[j]);
      if(Name[j]==' ')
	{
	  Name[j]='_';
	  //printf("replaced space by underscore\n");
	}
    }
  
  Name[len]='\0';
  //if(debug)
    printf("get_block: Name:%s\n",Name);

  if(strncmp(Name,"SKIP",4)==0) 
    {
      printf("get_block: skipping blocks until find a UNSKIP block\n");
      gbl_skip=TRUE;
    }
  else if(strncmp(Name,"UNSKIP",5)==0) 
    {
      printf("get_block: parsing blocks is restarting\n");
      gbl_skip=FALSE;
    }

  if(gbl_skip)
    {
      printf("get_block: gbl_skip is set; skipping block %s; returning\n",Name);
      return SUCCESS;
    }


  block_counter[TOTAL]++; /* another block added */
  if( block_counter[TOTAL] > MAX_BLOCKS)
    {
      cm_msg(MERROR, "get_block","limit on maximum number of blocks reached (%d); increase array size",MAX_BLOCKS);
      return DB_INVALID_PARAM;
    }

  if(strncmp(Name,"STDPULSE",3)==0) 
    {
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      if(debug)printf("Working on STDPULSE block %d\n",my_index);
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS,  STD_PULSE_BLOCK_STR );
      if(status != DB_SUCCESS)
	return status;    
      
    }
  
  else if(strncmp(Name,"PULSE",3)==0) 
    { 
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      if(debug) printf("Working on PULSE block %d\n",my_index);
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, PULSE_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
    }
  
  //  transition = pulse block with zero pulse width
  else if(strncmp(Name,"TRANS",3)==0) 
    {
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      if(debug) printf("Working on TRANS block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, TRANS_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
    }
  
  //  pattern block (special type of pulse block)
  else if(strncmp(Name,"PATTERN",3)==0) 
    {
      
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      if(debug) printf("Working on PATTERN block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, PATTERN_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
    }
  
  //   delay block (special type of pulse block)
      
  else if(strncmp(Name,"DELAY",3)==0) 
    {
      printf("detected DELAY block\n");
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      if(debug) printf("Working on DELAY block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, DELAY_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
    }
  
  
      
      
  else if(strncmp(Name,"TIME_REF",3)==0)
    {
      my_index =block_counter[TIME_REF]; // my_index counts TIME_REF blocks & goes from 0 (array index)
      if(debug) printf("Working on TIME_REF block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   TIME_REF, MAX_TIME_REF_BLOCKS, TIME_REF_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
    }
      
  else if(strncmp(Name,"XY_START",6)==0)
    {
      my_index =block_counter[XY_START]; // my_index counts XY_START blocks & goes from 0 (array index)
      if(debug) printf("Working on XY_START block %d\n",my_index);
      
      status = get_a_block_without_params(hKey, block_index,total_size, key,
					  XY_START, MAX_XY_BLOCKS);
      if(status != DB_SUCCESS)
	return status;  
    }
      
  else if(strncmp(Name,"XY_STOP",6)==0)
    {
      my_index =block_counter[XY_STOP]; // my_index counts XY_STOP blocks & goes from 0 (array index)
      if(debug) printf("Working on XY_STOP block %d\n",my_index);
      
      status = get_a_block_without_params(hKey, block_index,total_size, key,
					  XY_STOP, MAX_XY_BLOCKS);
      if(status != DB_SUCCESS)
	return status;  
    }
  
  else if(strncmp(Name,"EVSET",5)==0)
    { 
      my_index =block_counter[EV_SET]; // my_index counts ev_set blocks & goes from 0 (array index)
      
      status = get_a_block(hKey, block_index,total_size, key,
			   EV_SET, MAX_EV_BLOCKS, EV_SET_BLOCK_STR );
      if(status != DB_SUCCESS)
	return status;  
    }
  
  
  else if(strncmp(Name,"EVSTEP",5)==0)
    { 
      my_index =block_counter[EV_STEP]; // my_index counts ev_step blocks & goes from 0 (array index)
      status = get_a_block(hKey, block_index,total_size, key,
			   EV_STEP, MAX_EV_BLOCKS, EV_STEP_BLOCK_STR);
      if(status != DB_SUCCESS)
	return status;  
    } 
  
  
  
  else if(strncmp(Name,"BEGIN_LOOP",6)==0) 
    { 
      //printf("sizeof begin_loop_block is %d\n",sizeof(BEGIN_LOOP_BLOCK) );
      my_index =block_counter[BEGIN_LOOP]; // my_index counts begin_loop blocks & goes from 0 (array index)
      if(debug) printf("Working on BEGIN_LOOP block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   BEGIN_LOOP, MAX_LOOP_BLOCKS, BEGIN_LOOP_BLOCK_STR   );
      if(status != DB_SUCCESS)
	{
	  if(debug)printf("Error from get_a_block (%d) returning\n",status);
	  
	  return status;  
	}
    }
      
      
  else if(strncmp(Name,"END_LOOP",4)==0) 
    { 
      my_index =block_counter[END_LOOP]; // my_index counts end_loop blocks & goes from 0 (array index)
      if(debug) printf("Working on END_LOOP block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   END_LOOP, MAX_LOOP_BLOCKS, END_LOOP_BLOCK_STR  );
      if(status != DB_SUCCESS)
	return status;  
    }
  
  else if (strncmp(Name,"RF_SWEEP",6)==0)
    {
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug) printf("Working on RF_SWEEP block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS, 
			   RF_SWEEP_BLOCK_STR 
			   );
      
      if(status != DB_SUCCESS)
	return status;  
      
    }
      
  
  else if(strncmp(Name,"RF_FM_SWEEP",6)==0) 
    {
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug)  printf("Working on RF_FM_SWEEP block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS,  
			   RF_FM_SWEEP_BLOCK_STR 
			   );
      
      if(status != DB_SUCCESS)
	return status;    
    }
  
  else if(strncmp(Name,"RF_BURST",6)==0) 
    {
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug) printf("Working on RF_BURST block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS, 
			   RF_BURST_BLOCK_STR 
			   );
      
      if(status != DB_SUCCESS)
	return status;    
    }
  
  else if(strncmp(Name,"RF_SWIFT_AWG",6)==0) 
    {
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug) printf("Working on RF_SWIFT_AWG block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS,
			   RF_SWIFT_AWG_BLOCK_STR  
			   );
      
      if(status != DB_SUCCESS)
	return status;    
    }
  else if(strncmp(Name,"LOAD_EG_IONIZE",6)==0) 
    {
      // do nothing .... should have already found this
      if(hLEGI)
	{
	  printf("found LOAD_EG_IONIZE... skipping\n");
	  skip = TRUE;
	}
      else
	{
	  printf("ERROR... found LOAD_EG_IONIZE but hLEGI==0\n");
	  return DB_INVALID_PARAM;
	}
    }
  else
    { /* not one of the known block names */  
      str[0]='\0';
      printf("skipping block %s\n",Name);
      skip=TRUE;  /* block may be a superblock */
      
      /*      cm_msg(MERROR,"get_block","unknown block %s. Name must begin with one of :\n",Name  );
	      printf("get_block: unknown block %s. Name must begin with one of :\n",key->name  );
	      for(i=0; i<  NUM_BLOCK_TYPES; i++)
	      {
	      printf("i=%d block_names[%d]=%s \n",i,i,block_names[i]);
	      strcat(str, block_names[i]);
	      strcat(str,",");
	      }
	      str[strlen(str)-1]='\0';
	      cm_msg(MINFO,"get_block","%s",str);
	      printf("%s\n",str);
	      return DB_INVALID_NAME; */
    }
 
  if(skip)return SUCCESS;
  if(debug)printf("get_block: calling get_params with Name=%s\n",Name);
  status = get_params(hKey, key, my_index, block_index, total_size, Name);


 return status;
}
/*-----------------------------------------------------------------*/

INT print_key_info1(HNDLE hDB, HNDLE hKey, KEY * pkey, INT level, void *info)
{
   int i;
   char *p;
   printf("print_key_info1: starting\n");
   p = (char *) info;

   sprintf(p + strlen(p), "%08X  %08X  %04X    ",
           (int)(hKey - sizeof(DATABASE_HEADER)),
           (int)(pkey->data - sizeof(DATABASE_HEADER)), (int)pkey->total_size);

   for (i = 0; i < level; i++)
      sprintf(p + strlen(p), "  ");

   sprintf(p + strlen(p), "%s\n", pkey->name);

   return SUCCESS;
}
/*-----------------------------------------------------------------*/
void init_blocks(void)
{

  INT i;
  for (i=0; i<MAX_BLOCKS; i++)
    block_list[i]=0;
  for (i=0; i<NUM_BLOCK_TYPES+1; i++)
    block_counter[i]=0;

  // initialize some globals
  gbl_skip = FALSE;
  gbl_open_loop_counter=gbl_num_loops_defined=gbl_num_transitions=0;

  for (i=0; i<NUM_AWG_MODULES; i++)
    awg_params[i].sample_counter=0; 
  gbl_combined=FALSE;

  /* initialize defaults for input parameters  */
  init_default_param_values();
  return;
}

void set_awg_params(void)
{
  /* Set up the AWG parameters :
       AWG Units are fixed as follows:
         awg_trigger_names  PPG signals :  UNIT 0 "AWGHV"   fast   softdac
                                           UNIT 1 "AWGAM"   slow   da816

	 awg_trigger_names are defined in tri_config.h
  */
  INT i;
  // set up some defaults

  //printf("set_awg_params: %s,%s\n",psoftdac,pda816);

  sprintf(awg_params[0].type,"%s",psoftdac);
  sprintf(awg_params[1].type,"%s",pda816);
  sprintf(awg_params[0].outfile,"%s-ch0",psoftdac);
  sprintf(awg_params[1].outfile,"%s-ch0",pda816);


  for (i=0; i<NUM_AWG_MODULES; i++)
    {
      awg_params[i].plot=1; // default is to plot AWG output plots
      awg_params[i].in_use=0; // default is that AWG units are not in use
      ////awg_params[i].available=0; // default is that AWG units are not initialized

      //printf("set_awg_params: %s,%s\n",awg_params[i].type,awg_params[i].outfile);
    }
  return;  
}




void init_default_param_values(void)
{
  default_stop_freq_khz=0;
  default_start_freq_interval_khz=0;
  default_stop_freq_interval_khz=0;
  default_single_freq_khz=0; // single freq
  default_notch_freq_khz=0; // notch  freq
  default_amplitude_mv=0;
  default_duration_ms=0;
  default_num_cycles=0; // number of cycles
  default_num_bursts=0; // number of bursts
  default_num_frequencies=0; //number of freq
  default_frequency_inc=0; // freq increment)
  default_sweep_type=0; // index to above, 1=rf_sweep 2=rf_fm_sweep 3=rf_burst 4=rf_swift_awg
  default_start_freq_khz=0;
  sprintf(default_rf_generator,"none");
  sprintf(default_ppg_signal_name,"none");
  default_awg_unit=0; // Unit number can be 0 or 1 (only one unit at present, 0)
  default_time_reference=0;
#ifndef POL
  default_pulse_width_ms = settings.ppg.input.standard_pulse_width__ms_;
#else
  default_pulse_width_ms = settings.input.standard_pulse_width__ms_;
#endif
  default_loop_count = 1; // loop count if none supplied
  return;
}
  
/*------------------------------------------------------------------*/
INT file_wait(char *path, char *file)
{
  /* wait for compiled file */
  INT    j, nfile;
  char   *list = NULL;

  j = 0;
  do {
    ss_sleep(500);
    nfile = ss_file_find(path, file, &list); 
    j++;
  } while(j<5 && nfile != 1);

  if (nfile != 1)
  {
   cm_msg(MERROR,"file_wait","File not found (%s%s)", path, file);
   return -6;  
  }
  else
    if(debug)printf("file_wait: found file %s%s\n",path,file);
  return 1;
}
/*------------------------------------------------------------*/

void define_t0_ref(void)
{
  INT my_index,block_index;

  block_index=block_counter[TOTAL];
  block_counter[TOTAL]++;
  my_index = block_counter[TIME_REF]; // my_index is zero, defining T0
  block_counter[TIME_REF]++;
  sprintf(time_ref_params[my_index].this_reference_name,"T0");  // define Ref 0 as T0
  time_ref_params[my_index].time_ms=time_ref_params[my_index].t0_offset_ms=0;
  time_ref_params[my_index].my_ref=my_index; // referenced to itself
  time_ref_params[my_index].block_index=block_index; // should be zero
  strcpy(time_ref_params[my_index].block_name,"TIME_REF0"); 
  gbl_num_time_references_defined=1;
  if(debug)
    print_ref_blocks(stdout);
}

INT define_trans0(void)
{
  /* called after add_auto_loop for begin_scan loop or time_ref_index 1 will not be defined */

  INT block_index,my_index;

  if(debug)
    printf("define_trans0: default_time_reference = %d\n",default_time_reference);
  if(default_time_reference != 0)
    {
      cm_msg(MERROR,"define_trans0",
	     "internal programming error. Expect default_time_reference to be 0  not %d",default_time_reference);
      return DB_INVALID_PARAM;   
    }
  print_default_time_reference();
  printf("gbl_num_transitions=%d\n",gbl_num_transitions);
  p_transitions[gbl_num_transitions].t0_offset_ms = 0;
#ifndef POL
  p_transitions[gbl_num_transitions].time_reference_index=1; // _TBEGSCAN for EBIT,MPET (assumes scan loop at T0)
#else
  p_transitions[gbl_num_transitions].time_reference_index=0; // _T0 for POL (may be no scan loop)
#endif
  p_transitions[gbl_num_transitions].bit =  0x80000000; //bit pattern of 0 ORed with  0x80000000
  p_transitions[gbl_num_transitions].code = TRANS_CODE;
  block_index=block_counter[TOTAL];
  block_counter[TOTAL]++;
  my_index = block_counter[PULSE];
  block_counter[PULSE]++;
  p_transitions[gbl_num_transitions].block_index = block_index;
  sprintf(p_transitions[gbl_num_transitions].block_name,"PATTERN_T0");
  gbl_num_transitions++;

  return SUCCESS;
}

INT replace_trans0(char *name, DWORD bitpat)
{
  /* called if user has defined a pattern block at T0  */

  INT block_index,my_index;

  if(debug)
    printf("replace_trans0: starting with block name %s and bitpat 0x%x; gbl_num_trans=%d\n",
	   name, bitpat, gbl_num_transitions);
 
  if(strcmp(p_transitions[0].block_name,"PATTERN_T0") != 0)
    {
      cm_msg(MERROR,"replace_trans0",
	     "Expect transition 0 to be PATTERN_T0 not %s",p_transitions[0].block_name);
      return DB_INVALID_PARAM;   
    }

  printf("gbl_num_transitions=%d\n",gbl_num_transitions);
  //p_transitions[0].t0_offset_ms = 0;
  // p_transitions[0].time_reference_index=1; // assumes scan loop will be defined at T0
  p_transitions[0].bit =  0x80000000 |  bitpat; //bit pattern of 0 ORed with  0x80000000
  //  p_transitions[0].code = TRANS_CODE;
  //block_index=block_counter[TOTAL];
  //block_counter[TOTAL]++;
  //my_index = block_counter[PULSE];
  //block_counter[PULSE]++;
  //p_transitions[gbl_num_transitions].block_index = block_index;
  sprintf(p_transitions[0].block_name,name);
  //gbl_num_transitions++;

  return SUCCESS;
}










#ifdef EBIT
void output_loops(void)
{
  INT i,size,status;
  settings.ppg.output.loops.num_loops= gbl_num_loops_defined;
 
    
    for (i=0; i<gbl_num_loops_defined; i++)
      {
	/* T0 offsets, duration in ms */
	sprintf(settings.ppg.output.loops.loop_names[i], "%s",loop_params[i].loopname);
        settings.ppg.output.loops.start_loop[i]=loop_params[i].start_t0_offset_ms;
        settings.ppg.output.loops.one_loop_duration[i]=loop_params[i].one_loop_duration_ms;
        settings.ppg.output.loops.all_loops_end[i]=loop_params[i].all_loops_t0_offset_ms;
      }

  /* Update these output settings in ODB */
    if(hLoops)
      {
	size = sizeof(settings.ppg.output.loops);
	status = db_set_record(hDB, hLoops, &settings.ppg.output.loops, size, 0); 
	if (status != DB_SUCCESS)
	  cm_msg(MINFO,"prestart","Failed to set output loops record (size %d) (%d)",size,status);
      }
    else
      cm_msg(MINFO,"prestart","No key; cannot update output loops record",size,status);

    return;    
}
#endif

/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT    status, stat1;
  DWORD  j, i;
  HNDLE  hKey;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   svpath[64], info[128];
  INT    fHandle;
  INT    msg, ch;
  BOOL   daemon;

#ifndef POL
   TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); /* (from experim.h) */
  // TITAN_ACQ_PPG_CYCLE_STR(titan_acq_ppg_cycle_str);
#else  // ebit,mpet
   POL_ACQ_SETTINGS_STR(pol_acq_settings_str); /* (from experim.h) */
#endif
  char str[128];
  INT size,ssize,run_state;
  char *s;

  daemon = FALSE;
 
  ptrans = p_transitions;

#ifdef AUTO_TDCBLOCK
  ptdc   = tdcblock;
#endif // AUTO_TDCBLOCK
  /* set default */
  cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
  /* initialize global variables */
#ifndef POL
  sprintf(eqp_name,"TITAN_acq");
#else
  sprintf(eqp_name,"POL_acq");
#endif
  single_shot=FALSE;
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
    {
      if (argv[i][0] == '-' && argv[i][1] == 'd')
	debug = TRUE;
      else if (argv[i][0] == '-' && argv[i][1] == 'D')
	daemon = TRUE;
      else if (argv[i][0] == '-' && argv[i][1] == 's')
        single_shot=TRUE;
      else if (argv[i][0] == '-')
	{
	  if (i+1 >= argc || argv[i+1][0] == '-')
	    goto usage;
	  if (strncmp(argv[i],"-f",2) == 0)
	    strcpy(svpath, argv[++i]);
	  else if (strncmp(argv[i],"-e",2) == 0)
	    strcpy(expt_name, argv[++i]);
	  else if (strncmp(argv[i],"-h",2)==0)
	    strcpy(host_name, argv[++i]);
	}
      else
	{
	usage:
	  printf("usage: tri_config -d (debug) -s (single loop) \n");
	  printf("             [-h Hostname] [-e Experiment] [-D] \n\n");
	  return 0;
	}
    }

  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
   }
  init_blocks();

  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "tri_config", 0);
  if (status != SUCCESS)
    return 1;
  printf("connected to hostname %s and expt_name %s\n",host_name,expt_name);

#ifdef _DEBUG
  cm_set_watchdog_params(TRUE, 0);
#endif

  /* turn off message display, turn on message logging */
  cm_set_msg_print(MT_ALL, 0, NULL);


  if(! single_shot)
    {
  /* register transition callbacks
     frontend start is 500,  frontend stop is 500 */


      if(cm_register_transition(TR_START, tr_precheck, 350) != CM_SUCCESS)
	{
	  cm_msg(MERROR, "main", "cannot register to transition for tr_precheck");
	  goto error;
	}
  
      if(cm_register_transition(TR_START, tr_prestart,400) != CM_SUCCESS)
	{
	  cm_msg(MERROR, "main", "cannot register to transition for prestart");
	  goto error;
	}
    }
  
  /* connect to the database */
  cm_get_experiment_database(&hDB, &hKey);
  
  /* Check if the structure "Settings"  exists */
  sprintf(str,"/Equipment/%s/Settings",eqp_name);
  status = db_find_key(hDB,0,str, &hSET);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("main: can't find key %s",str);
      
      /* So try to create settings record */
      if(debug) printf("Attempting to create record for %s\n",str);

#ifndef POL      
      status = db_create_record(hDB, 0, str , strcomb(titan_acq_settings_str));
#else
      status = db_create_record(hDB, 0, str , strcomb(pol_acq_settings_str));
#endif
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"main","Could not create settings record (%d)\n",status);
        }
      else
        if (debug)printf("Success from create record for %s\n",str);
      
      /* Check again if the structure "settings"  exists  */
      status = db_find_key(hDB,0,str, &hSET);
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"main","\"%s\" is still not present",str);
          goto error;
        }
    }



  /* check that the record size for hSET is as expected */
  status = db_get_record_size(hDB, hSET, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "main", "error during get_record_size (%d)",status);
      return status;
    }
#ifndef POL
  ssize = sizeof(TITAN_ACQ_SETTINGS);
#else
  ssize = sizeof(POL_ACQ_SETTINGS);
#endif

  printf("Size of Settings saved structure: %d, size of Settings record: %d\n",ssize,size); 
  
  if (ssize != size) 
    {
      /* create Settings record */
      cm_msg(MINFO,"main","creating record \"%s\"; mismatch between size of structure (%d) & record size (%d)", str,ssize ,size);
#ifndef POL
      status = db_create_record(hDB, 0, str , strcomb(titan_acq_settings_str));
#else
      status = db_create_record(hDB, 0, str , strcomb(pol_acq_settings_str));
#endif
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Could not create \"%s\"  record (%d)\n",str,status);
	  return status;
	}
      else
	if (debug)printf("Success from create record for %s\n",str);
    }
    


  
  /* Find PPG directory under Settings */
#ifndef POL
  sprintf(str,"/Equipment/%s/Settings/ppg",eqp_name);
#else
  sprintf(str,"/Equipment/%s/Settings/",eqp_name);
#endif
  status = db_find_key(hDB,0,str, &hPPG);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","\"%s\" is not present",str);
      printf("tri_config: \"%s\" is not present (%d)\n",status);
      goto error;
    }
  
  
  /* check if Output tree is available */
  status = db_find_key(hDB, hPPG, "output", &hOut);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Output record (%d)",status);
      printf("tri_config: cannot find \"%s\" Output tree (%d) \n",status);
      goto error;
    }

  /* check if Input tree is available */
  status = db_find_key(hDB, hPPG, "input", &hIn);
  if (status != DB_SUCCESS)
    {
      printf("tri_config: cannot find \"%s\" Input tree (%d) \n",status);
      goto error;
    }

#ifndef NO_AWG
  /* check if AWG tree is available */
  /* Check if the structure "AWG"  exists */
  status = db_find_key(hDB,hSET,"AWG0", &hAWG0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find AWG0 record (%d)",status);
      goto error;
    }
 
  /* check if AWG tree is available */
  /* Check if the structure "AWG1"  exists */
  status = db_find_key(hDB,hSET,"AWG1", &hAWG1);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find AWG1 record (%d)",status);
      goto error;
    }
#endif

#ifdef EBIT
  /* check if loops tree is available */
  status = db_find_key(hDB, hOut, "loops", &hLoops);
  if (status != DB_SUCCESS)
      cm_msg(MINFO,"main","Equipment/TITAN_Acq/settings/ppg/output/loops is not present");
  
#endif

      
    
  /* Now check if the structure "ppg cycle"  exists.
      This contains a block list (MPET,POL) or is a link to one of the defined block lists (EBIT,ALPHA)  */
  sprintf(str,"/Equipment/%s/ppg cycle",eqp_name);
  status = db_find_key(hDB,0,str, &hCycle);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find PPG Cycle params (key %s) (%d)",str,status);
      printf("tri_config: cannot find PPG Cycle params (key %s) (%d)\n",str,status);
      goto error;
    }

#ifndef POL
  // load_EG_ionize is a super-block that is usually defined for MPET        
  hLEGI=0;
  status = db_find_key(hDB,hCycle,"load EG ionize", &hLEGI);
  if (status == DB_SUCCESS)
    printf("found super-block \"load EG ionize\" handle hLEGI=%d \n",hLEGI);
  //else
  //  printf("did not find super-block \"load EG ionize\"\n");
#endif


  if (single_shot)
    {         /* single sweep */
      
      status = tr_precheck(10, info);

      
      if (status != SUCCESS)
	goto error;
      
      status = tr_prestart(10, info);
      if (status)
        {
          printf("Thank you\n");
          goto exit; /* force exit */
        }
      else
        goto error;
    }
  
  /* initialize ss_getchar() */
  ss_getchar(0);
  do
    {
      while (ss_kbhit())
        {
          ch = ss_getchar(0);
	  if (ch == -1)
	    ch = getchar();
	  if (ch == 'R')
	    ss_clear_screen();
	  ss_clear_screen();
	  if ((char) ch == '!')
	    break;
	}
      msg = cm_yield(1000);
    } while (msg != RPC_SHUTDOWN && msg != SS_ABORT && ch != '!');
  
  
  printf("Thank you\n");
  goto exit;
  
 error:
  printf("\n Error detected. Check odb messages for details\n");

 exit:
  if(tfile)
    fclose(tfile); // make sure file is closed
 
  /* reset terminal */
  ss_getchar(TRUE);
  
  cm_disconnect_experiment();
  return 1;
}




