/********************************************************************\

  Name:         pol_fevme.cxx
  Created by:   Suzannah Daviel   
                based on code by K.Olchanski

  Contents:     Frontend for the POL VME DAQ

  $Id: pol_fevme.cxx,v 1.10 2014/05/29 23:59:51 suz Exp $

  Supercycle version - Number of LNE for SIS scaler is now set for the supercycle (not the cycle) 
  Threshold and single cycle histos removed

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <math.h>
#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "mvmestd.h"
#include "evid.h"


#define HAVE_SIS3820
#define HAVE_PPG  // Any PPG
#define NEW_PPG  // New PPG;
//#undef  NEW_PPG // OLD PPG
//#define DUMMY_DAC // dummy DAC

#define HAVE_GALILRIO  // DAC,ADC



extern "C" {
#ifdef HAVE_VMEIO
#include "vmeio.h"
#endif

#ifdef HAVE_SIS3820
#include "sis3820drv.h"
#include "sis3820_nodma.h"
#include "sis_code.h" // prototypes
#endif

#ifdef NEW_PPG 
#include "newppg.h"
#include "ppg_modify_file.h" // prototypes
#else
#include "vppg.h"
#endif


#ifdef HAVE_PPG 
  // PPG 
#include <sys/stat.h> // time 
#include "experim.h"
#include "unistd.h" // for sleep 
#include "ppg_code.h" // prototypes
#endif

#include "pol_dac.h"
//#include "threshold_code.h" // threshold prototypes and globals
}


#ifdef HAVE_GALILRIO
#include <iostream>
#include "TGalilRIO.h"
#endif



INT  process_histo(void);
int stop_PPG(void);


#define N_HISTO_MAX 6  // four channels
#define N_SCALER_MAX 6  // presently four channels used for POL
BOOL send_mcs=0;  // if true raw scaler data bank will be sent out (only sums sent otherwise)   bank MCS0


// globals for POL

// minimum dwell times - otherwise the SIS scaler does not see every LNE from PPG
const float  SIS8_min_dwell_time_ms = 0.000224;  //   minimum dwell time for SIS in ms (8 bits data)
const float  SIS16_min_dwell_time_ms = 0.000234;  //   minimum dwell time for SIS in ms  (16 bits data)
const float  SIS32_min_dwell_time_ms = 0.000244;  //   minimum dwell time for SIS in ms  (16 bits data)

/* Input Parameters to control PPG and SIS (read from ODB) that need to be global:
 */

POL_ACQ_SETTINGS ps;
char  cmd[128]; // holds command to run tri_config
char  data_path[256]; // for /logger/data dir  (to save tri_config output file)

/*int  sismode (ps.input.sis3820.sis_mode)                        PPGMODE

		0 TEST pulses - no PPG   
		1 external LNE with test input pulses   1h
		2 Ext LNE, real inputs;                 1h

*/

//DWORD  requested_bins;  // number of bins requested by user
DWORD  num_bins; // number of bins  required = LNE_preset  (this is the number of bins * number of cycles/supercycle)  
                // NOTE:  if LNE_prescale is > 1, will need more LNE_input pulses to produce each bin
                //        for POL, LNE_prescale will ALWAYS be 1
//DWORD  LNE_prescale_factor; // 1 for no prescale
DWORD LNE_total;
INT  num_cycles_per_supercycle;
INT    data_bytes=2; // number bytes for each data word default= 4 for 32-bit data, 2 for 16-bit, 1 for 8-bit 
                     // decoding only for 16-bit data currently

//BOOL   run_triconfig=TRUE; // run triconfig or not. POL does not run tri_config 
INT    num_scans; // 0= free-running  number of complete DAC scans wanted

// DAC 
float   dac_val, dac_read;        /* dac value set and readback */
float   prev_dac_val, prev_dac_read; // dac values at last step
float   my_dac_val; // dac value for current cycle

INT last_gSumMcsEvents=0;

INT gvmeisidle_cntr,gvmenotidle_cntr;
INT hist_true_cntr,cycl_true_cntr;


/* More globals */

// ODB handles
HNDLE hTASet,hOut,hPPG,hfe=0,hStop=0,hTimer=0;

BOOL  ppg_running=0; // flag
BOOL  dac_mon_flag=0; // flag true if second galil_rio DAC channel is used
int   gbl_DAC_n=0, gbl_SCAN_n=0, gbl_CYCLE_n=0, gbl_SCYCLE_n=0;// counters
INT   dac_ninc;      /* number of dac increments */
BOOL  flush=FALSE, data_flushed=FALSE;
char  ppgmode[]="1h"; // 1h for POL
BOOL  bor_flag=0; // TRUE if begin_of_run
BOOL  resume_flag=0; // TRUE if run is being continued
uint32_t acq_count; // readback of number of LNE received
BOOL hot_debug=FALSE, lhot_debug=FALSE,  hot_stop=FALSE;
BOOL hot_timer=FALSE, lhot_timer=FALSE ; // turn on timers

int NumSisChannels; // actual number of SIS channels enabled (calculated from bitpat)
char ppgfile[128]; // ppg file to load (including path)
BOOL stop_run_flag=FALSE;

INT watchrabbit=0;
INT watchdog=0; // temporary
INT watchInterval=100;
// These are needed to program new ppg without running tri_config
DWORD dw_count, lne_count, dac_sleep_count, mcs_gate_count, tof_width_count;


INT dac_step_cntr[3];

#define CYCLE 0  // send of cycle scalers
#define HISTO 1  // send out histo banks 
#define FIFO 2   // scaler data to be read out
#define INFO 3   // data sent to ODB
#define N_WAITEQP 4  // number of items defined above
BOOL    waiteqp [N_WAITEQP] = {TRUE,TRUE,TRUE,FALSE};
const char *names[]={"CYCLE","HISTO","FIFO","INFO"};

INT gbl_IN_CYCLE;
BOOL gbl_skip_cycle;
INT  gbl_skip_cycle_cntr;
INT gbl_his_cntr;
INT gbl_cycle_cntr;
BOOL gbl_in_tol;
INT gbl_rd;
uint32_t *gbl_data_buffer;
int gbl_buf_index=0;
int gbl_db_size32=0;

// RIO filter
INT rio_debug=0;
float rio_average[4];
int rio_max=4; // maximum number of channels being averaged
char REPLY[100];


const int ghbd_len = 7; // size of array gbl_histo_bank_data
float gbl_histo_bank_data[ghbd_len]; // holds cycle number, dac value to go with histograms etc.
const int gcbd_len = 15;
float gbl_cycle_bank_data[gcbd_len]; // holds cycle number, dac value to go with histograms etc.
const int gsdd_len=9;
float store_dbg_data[gsdd_len];

/* Individual scaler channel */
typedef struct {
    DWORD   nbins;
    double  sum_cycle;   /* scaler sum over one cycle */
    DWORD   *ps;         /* scaler bin values over one cycle */
} MSCALER;
MSCALER  scaler[N_SCALER_MAX];

/* Histogram struct  */
typedef struct {            
    DWORD   nbins;
    double   sum;          /* sum over N cycles (one supercycle) */
    DWORD   *ph;         /* pointer to histogram */
} MHISTO;
MHISTO   histo[N_SCALER_MAX]; // one histo only, but sums for all the defined scaler channels.

double HSums[N_SCALER_MAX]; // store sum/SC  for info event


DWORD bin_zero;
struct timeval eoct; // time at end-of-cycle
struct timeval wcyc; // time when waiteqp[CYCLE] set true
struct timeval riot; // time when DAC incremented; rio starts averaging
//INT MCSptr; // pointer to MCS_channel

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   const char *frontend_name = "pol_fevme";
/* The frontend file name, don't change it */
   const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 500*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 10*1024*1024;


  INT debug = 0;

  extern INT run_state;
  extern HNDLE hDB;
  extern char exp_name[NAME_LENGTH]; 

/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop(); 
 static double timeDiff(const struct timeval&t1,const struct timeval&t2);
  INT send_hist_event(char *pevent, INT off);
  INT read_mcs_done(char *pevent, INT off);
  INT read_info_event(char *pevent, INT off);
  INT read_sispulser_event(char *pevent, INT off);
  INT read_mcs_event(char *pevent, INT off);
  INT read_vme_event(char *pevent, INT off);
  INT init_vme_modules(void);
  INT set_ppg_alarm(BOOL active);
  void send_scaler_banks(char *pevent);
  INT  prestart(INT run_number, char *error); 
  INT  poststart(INT run_number, char *error); 
  INT  post_stop(INT run_number, char *error); 
  //INT  pre_stop(INT run_number, char *error); 
   
  BOOL deferred_stop(INT run_number, BOOL first); 
  void stop_call_back(HNDLE hDB, HNDLE hkey, void * info);
  INT DAC_set_scan_params(void);
  void clear_mcs_counters(void);
//INT get_SIS_dataformat ( INT num_bits );
// INT init_sis3820(void);
//  void csr_bits(unsigned int read_data);
// void SIS_readback(unsigned int wrote_mode, unsigned int wrote_csr);
//  void opmode_bits(unsigned int read_data);
  void  setup_hotlinks(void);
  void call_back_debug(HNDLE hDB, HNDLE hkey, void * info);
  void call_back_timer(HNDLE hDB, HNDLE hkey, void * info);
  INT set_dac_sleep_time(void);
  void scaler_clear(INT i);
  INT get_rio_averages(void);
  INT set_rio_filter_params(void);
  INT get_pol_settings(void);
  INT set_rio_input_config(void);
  //int write_modifications (char *modfilename, char *outfile, unsigned int array[]);
  INT read_hsum_event(char *pevent, INT off);
  INT incr_DAC(void);
  void histo_clear(INT h);
  INT cycle_start(void);
  INT stop_run(void);
  INT fi_init(void);
  INT decode_scaler_data(void);
  INT set_watchdog(void);
  INT read_dummy(char *pevent, INT off);
  void set_DAC(int dac_channel,float dac_val);
  float read_DAC(void); 
  void check_stuck_mcs(void);
  
/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {
    

    {"POL_ACQ",               /* equipment name  DUMMY  */
     {EVID_POL, (1<<EVID_POL), /* event ID  13 , trigger mask */
      "",                   /* event buffer  (no banks) */
      EQ_MULTITHREAD, //EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* when to read */
      10,                     /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_mcs_done,          /* readout routine  checks if Scaler has all LNEs i.e. SC done  */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
  
    
    {"VME",                  /* equipment name */
     {EVID_VME, (1<<EVID_VME), /* event ID  11 , trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,         /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* when to read this event */
      20,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* whether to log history */
      "", "", "",}
     ,
     read_vme_event,          /* readout routine  reads scaler data */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
  
   
     {"HISTO",               /* equipment name   */
     {EVID_MCS, (1<<EVID_MCS), /* event ID  5 , trigger mask */
      "SYSTEM",                   /* event buffer  (no banks) */
      EQ_PERIODIC,//EQ_MULTITHREAD,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* when to read */
      10,                     /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     send_hist_event,          /* readout routine  checks if Scaler has all LNEs i.e. SC done  */
     NULL,
     NULL,
     NULL,       /* bank list */
    },




    
    {"INFO",                   /* equipment name */
     {EVID_INFO, (1<<EVID_INFO),  /* event ID  3 , trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,            /* read only when running */
      100,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* whether to log history */
      "", "", "",}
     ,
     read_info_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
  
    {"SisPulser",               /* equipment name */
     {EVID_PULSER, (1<<EVID_PULSER), /* event ID  7 , trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING|RO_TRANSITIONS, /* when to read */
      10000,                  /* read every 10000 milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_sispulser_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    },
    
    {""}
  };

  
#ifdef __cplusplus
}
#endif



// equipment indices
#define VME 0
#define MCS 1
#define ADC 2
#define SCAL 3
#define PULS 4
#define SISP 5
#define PPG  6


/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

MVME_INTERFACE *gVme = 0;

#ifdef HAVE_GALILRIO
  TGalilRIO *rio = new TGalilRIO(rio_ip);
#endif


uint16_t gIoreg = 0;

int gVmeioBase   = 0x780000;
int gIoregBase   = 0x510000;
//int gMcsBase[] = { 0x38000000, 0x39000000, 0 };
int gMcsBase =  0x38000000;
int gTtcBase     = 0x37000000;

#ifdef NEW_PPG
const DWORD gPpgBase     = 0x00100000; // NEWPPG
#else
const DWORD gPpgBase     = 0x00008000; // OLDPPG
#endif

int vmeread16(int addr)
{
  mvme_set_dmode(gVme, MVME_DMODE_D16);
  return mvme_read_value(gVme, addr);
}

int vmeread32(int addr)
{
  mvme_set_dmode(gVme, MVME_DMODE_D32);
  return mvme_read_value(gVme, addr);
}

void vmewrite8(int addr, uint8_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D8);
  mvme_write_value(gVme, addr, data);
}

void vmewrite16(int addr, uint16_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D16);
  mvme_write_value(gVme, addr, data);
}

void vmewrite32(int addr, uint32_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D32);
  mvme_write_value(gVme, addr, data);
}

void encodeU32(char*pdata,uint32_t value)
{
  pdata[0] = (value&0x000000FF)>>0;
  pdata[1] = (value&0x0000FF00)>>8;
  pdata[2] = (value&0x00FF0000)>>16;
  pdata[3] = (value&0xFF000000)>>24;
}

void xusleep(int usec)
{
#if 0
  struct timespec req;
  req.tv_sec = 0;
  req.tv_nsec = usec*1000;
  nanosleep(&req, NULL);
#endif
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = usec;
  select(0, NULL, NULL, NULL, &tv);
}

#include "utils.cxx"
#include "sis_code.cxx"
#include "ppg_code.cxx"
//#include "pol_thresholds.cxx"
#include "dac_code.cxx"

void enable_SIS()
{
#ifdef HAVE_SIS3820
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_KEY_OPERATION_ENABLE,0);
  if(hot_debug)printf ("Enabled SIS3820 module  at address  0x%x \n", gMcsBase);
#endif
}

int stop_PPG()
{
  if(ps.input.sis3820.sis_mode == 0) // test mode - no PPG
    return SUCCESS;


  if(ps.input.ppg_external_start)
    { // real mode
#ifdef NEW_PPG
      TPPGDisableExtTrig(gVme,   gPpgBase); // Disable external trigger to prevent PPG restarting
#else
      VPPGDisableExtTrig(gVme,   gPpgBase); // Disable external trigger to prevent PPG restarting
#endif
    }

  check_ppg_running(); // updates global ppg_running
  if(ppg_running)
    {
      if(hot_debug) printf("stop_PPG: stopping PPG\n");
      /* Stop the PPG sequencer  */ 
#ifdef NEW_PPG
      TPPGStopSequencer(gVme,   gPpgBase); // Halt pgm and stop
#else
      VPPGStopSequencer(gVme,   gPpgBase); 
#endif

      ppg_running = FALSE; // not running 
      ss_sleep(100);
      check_ppg_running(); // updates global ppg_running
      if(ppg_running)
	{
	  printf("stop_PPG: PPG will not stop\n");
	  return FAILURE;
	}
    }
  else
    {
      if(debug)
	printf("stop_PPG: PPG is already stopped\n");
    }
  
  return SUCCESS;
}

int enable_PPG()
{
  // called at beginning of a supercycle

  INT status;
  struct timeval now;
  double td=0;


#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0xFFFF);
#endif

#ifdef HAVE_PPG

  if(ps.input.sis3820.sis_mode == 0) // test mode - no PPG
    return SUCCESS;

  if(ps.input.ppg_external_start)
    { // real mode
#ifdef NEW_PPG
      TPPGEnableExtTrig(gVme,   gPpgBase); // Enable external trigger for subsequent cycles
      TPPGStartSequencer( gVme, gPpgBase); // start PPG
#else
      VPPGEnableExtTrig(gVme,   gPpgBase); // Enable external trigger for subsequent cycles
      VPPGStartSequencer( gVme, gPpgBase);// start PPG
#endif
    
     if(hot_debug)printf("enable_PPG: started PPG by software; subsequent cycles will be triggered externally\n");
    }
  else
    {
      // test mode - internal start
      check_ppg_running(); // updates global ppg_running
      if(ppg_running)
	{
	  if(hot_debug)
	    printf("enable_PPG: test mode (internal start)  PPG is still running (DAC sleep time set too long ?) ...stopping it \n");
	  if( stop_PPG() != SUCCESS)
	    {
	      cm_msg(MERROR,"enable_PPG","PPG would not stop when running in test mode");
	      status =stop_run();
	      return status;
	    }
	}


      /* start the PPG  */ 
      
#ifdef NEW_PPG
      TPPGStartSequencer( gVme, gPpgBase);
#else
      VPPGStartSequencer( gVme, gPpgBase);
#endif      
      
      if(hot_debug)
	printf("enable_PPG: restarted PPG by software\n");
      
    }
#endif /* PPG */ 

  if (!bor_flag)  
    {
      if(hot_timer)
	{
	  // time how long before PPG restarted by software
	  gettimeofday(&now,NULL);
	  // compute elapsed time in millisec
	  td = (now.tv_sec - eoct.tv_sec) * 1000.0;      // sec to ms
	  td += (now.tv_usec - eoct.tv_usec) / 1000.0;   // us to ms
	  // td = timeDiff(now, eoct);
	  printf("enable_ppg: Total down time between SuperCycles  = %f ms\n",td);
	}
    }

  if(dac_mon_flag)
    {
      set_DAC(dac_monitor_chan, 0.0); // clear dac monitor output
      printf("Cleared DAC monitor channel\n");
    }
  
  return SUCCESS;
}


int disable_trigger()
{
  //printf("disable_trigger: starting with sismode=%d\n",ps.input.sis3820.sis_mode);
#ifdef HAVE_SIS3820
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_KEY_OPERATION_DISABLE,0);
#endif
#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0);
#endif

  if(stop_PPG() != SUCCESS)
    return FAILURE;

  return SUCCESS;
}



/*-- VME setup     -------------------------------------------------*/

INT init_vme_modules(void)
{
  INT status;

  //printf("\ninit_vme_modules: starting \n");

#ifdef HAVE_VMEIO
  printf ("\ninit_vme_modules: setting up VMEIO...\n");
  vmeio_OutputSet(gVme,gVmeioBase,0);
  vmeio_StrobeClear(gVme,gVmeioBase);
  printf("VMEIO at 0x%x CSR is 0x%x\n",gVmeioBase,vmeio_CsrRead(gVme,gVmeioBase));
#endif
  
  status = init_ppg();
  if (status != SUCCESS)
    return status;

  status =init_sis3820();
  if (status != SUCCESS)
    return status;
  
  printf ("init_vme_modules: SUCCESS \n");
  return SUCCESS;
}


EQUIPMENT* findEquipment(const char*name)
{
  for (int i=0; equipment[i].name[0] != 0; i++)
    if (strcmp(equipment[i].name, name) == 0)
      return &equipment[i];
  cm_msg(MINFO, frontend_name, "Cannot find equipment %s", name);
  abort();
}


/*-- Global variables ----------------------------------------------*/

static int gRunNumber      = 0;

//static double gPulserPeriod[8];

static EQUIPMENT* gSisPulserEquipmentPtr = 0;

static int gSisBufferOverflow = 0;
INT imax;

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int status;
  INT size;
  int i;
  char str[128];
  char errstr[256];
  BOOL fe_flag;
  float diff;
  float max_offset = 0.05; // rough check on difference between read and set value of DAC to make sure it is working
 
  if (run_state != STATE_STOPPED)
    {
      gVme=NULL;

      cm_msg(MINFO,"frontend_init","Stopping the previous run");
      status = cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), TR_ASYNC, 0);
      if(status != SUCCESS)
	{
	  printf("frontend_init: Could not stop previous run. cm_transition returns status=%d\n",status); 
	  sprintf(errstr,"Could not stop previous run. cm_transition returns status=%d\n",status);
	  goto err;
	}
      ss_sleep(1000);
    }
  

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);
  
  cm_enable_watchdog(0);
  
  /* Init all pointers to potential MALLOC calls to NULL */
  for (i=0 ; i < N_SCALER_MAX ; i++)
      scaler[i].ps = NULL;
  for (i=0 ; i < N_HISTO_MAX ; i++)
      histo[i].ph = NULL;


  gbl_data_buffer = NULL;

  gSisPulserEquipmentPtr = findEquipment("SisPulser");
  status = mvme_open(&gVme,0);
  status = mvme_set_am(gVme, MVME_AM_A24_ND);


  status = get_pol_settings(); // create records, get ODB keys etc.
  if(status !=SUCCESS)
    {
      sprintf(errstr,"Problem creating ODB records/getting ODB keys (%d)", status);
      goto err;
    }

 
  

#ifdef HAVE_GALILRIO
//const float min_DAC_incr  // minimum increment (Volts) - must be > DAC_jitter


  // Make sure we can set and read back the DAC
 
  dac_val = 2.5;
  set_DAC(dac_output_chan, dac_val);
  dac_read = read_DAC(); // readback

  if (fabs (dac_val - dac_read) > max_offset)
    {
      // retry
      ss_sleep (500);
     
      dac_read = read_DAC();
      max_offset*=2;
      
      diff= (fabs (dac_val - dac_read)) ;
      if ( diff > max_offset )
	{
	  cm_msg(MERROR, "frontend_init", "DAC device (Galil RIO 47120) does not seem to be working. Please check connections");
	  printf("frontend_init: wrote %f and read back %f \n", dac_val, dac_read);
	  sprintf(errstr,"DAC device (Galil RIO 47120) does not seem to be working. Wrote  %f and read back %f", dac_val, dac_read);
	

	  printf("frontend_init: fails because fabs(difference) > max_offset( i.e. %f > %f )  \n", diff, max_offset);
	  goto err;
	}
    }
#endif // HAVE_GALILRIO   

  status = cm_register_transition(TR_START, prestart, 350); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for prestart (%d)", status); 
      sprintf(errstr,"cannot register to transition for prestart (%d)",status);
      goto err;
    } 

  status = cm_register_transition(TR_START, poststart, 600); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for poststart  (%d)",status); 
      sprintf(errstr,"cannot register to transition for poststart (%d)", status);
      goto err;
    } 


  // deferred stop does not work with latest MIDAS
  //  status = cm_register_deferred_transition(TR_STOP, deferred_stop); 
  //  if(status != CM_SUCCESS) 
  //   { 
  //    cm_msg(MERROR, "frontend_init", "cannot register to transition for deferred_stop (%d)", status); 
  //   sprintf(errstr,"cannot register to transition for deferred_stop (%d)",status);
  //   goto err;
  //  } 


  printf("frontend_init:  calling  set_rio_input_config\n");
  status = set_rio_input_config(); // e.g. set ADC1 to =/- 5V
  if(status != SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot set input configuration for galil rio DAC/ADC (%d)",status); 
      sprintf(errstr,"error setting galil rio parameters (%d)",status);
      goto err;
    } 

#ifdef GONE // Testing only
  status = cm_register_transition(TR_STOP, pre_stop, 400); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for pre_stop (%d)",status); 
      sprintf(errstr,"cannot register to transition for pre_stop (%d)",status);
      goto err;
    } 
#endif




 
  status = cm_register_transition(TR_STOP, post_stop, 600); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for post_stop (%d)",status); 
      sprintf(errstr,"cannot register to transition for post_stop (%d)",status);
      goto err;
    } 





  

  sprintf(str,"/Logger/data dir"); 
  size=sizeof(data_path);
  status = db_get_value(hDB, 0, str, data_path, &size, TID_STRING, FALSE); 
  if(status != DB_SUCCESS) 
  { 
    cm_msg(MINFO,"frontend_init","cannot get data path at %s (%d)",str,status); 
    data_path[0]='\0'; 
  } 
  printf("data path is :%s\n",data_path); 

  status = init_vme_modules();
  if (status != SUCCESS)
    {
      cm_msg(MERROR, "frontend_init","error from init_vme_modules (%d)",status);
      printf("frontend_init: error from init_vme_modules (%d)\n",status);
      sprintf(errstr,"error from init_vme_modules (%d)", status);
      goto err;
    }

  disable_trigger();
  setup_hotlinks();
  fe_flag=FALSE; // VME etc now initialized
  status = db_set_value(hDB, 0,"/Equipment/pol_acq/settings/output/fe_flag" , &fe_flag, sizeof(fe_flag), 1, TID_BOOL);  
  if(status !=SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","cannot clear \"/Equipment/pol_acq/settings/output/fe_flag\" (%d)",status); 
      return FE_ERR_HW;
    }


  printf("frontend_init: SUCCESS\n\n");
  return SUCCESS;



 err:
  fe_flag=TRUE;

  status = db_set_value(hDB, 0,"/Equipment/pol_acq/settings/output/fe_flag" , &fe_flag, sizeof(fe_flag), 1, TID_BOOL);  
  if(status !=SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","cannot set \"/Equipment/pol_acq/settings/output/fe_flag\" (%d)",status); 
      return FE_ERR_HW;
    }
  
  status = db_set_value(hDB, 0, "/Equipment/pol_acq/settings/output/frontend msg", errstr, sizeof(errstr), 1, TID_STRING);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"frontend_init","cannot set ODB key \"/Equipment/pol_acq/settings/output/frontend msg\" (%d) ",status); 
  
  return FE_ERR_HW;
  
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  INT i;
  disable_trigger();

  mvme_close(gVme);


  printf("free histo ");
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      if (histo[i].ph != NULL)
	{
	  printf(" %d ", i);
	  free(histo[i].ph);
	  histo[i].ph = NULL;
	}
    }

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  INT i;
  INT status;
  char str[80];

  gvmeisidle_cntr=0;
  gvmenotidle_cntr=0;
  hist_true_cntr=cycl_true_cntr=0;
  bor_flag = 1;
  gRunNumber = run_number;
  stop_run_flag = FALSE;
  waiteqp[FIFO]=waiteqp[HISTO]=waiteqp[CYCLE]=FALSE;  
  
  for (i=0 ; i < ghbd_len ; i++)
    gbl_histo_bank_data[i]=0.0; // clear
  for (i=0; i<3; i++)
    dac_step_cntr[i]=0; // clear DAC error counters


  // clear hotlink "stop at end-of-sweep"
  sprintf(str,"input/stop at end-of-sweep");
  hot_stop=0;
  status = db_set_value(hDB, hTASet, str, &hot_stop, sizeof(hot_stop), 1, TID_BOOL);  
  if(status !=SUCCESS)
    {
      cm_msg(MERROR,"begin_of_run","cannot set ODB key  ...%s to %d (%d)",str,hot_stop,status); 
      return status;
    };
    
  // num_scans is the number of complete scans required
  num_scans = odbReadInt("/Equipment/POL_ACQ/settings/input/Number of scans",0,0); // 0 is free running  
  printf("number of scans requested: %d\n",num_scans);
  
 
  
  send_mcs = odbReadBool("/Equipment/POL_ACQ/settings/input/send raw data bank",0,0);

  
  al_reset_alarm("sis_overflow");
  gSisBufferOverflow = 0;
  
  
  
  status = init_vme_modules();
  if (status != SUCCESS)
    return status;

#ifdef HAVE_GALILRIO
  status = set_rio_input_config(); // e.g. set ADC1 to =/- 5V
  if(status != SUCCESS) 
    return status;
    

  status =  set_rio_filter_params();
  if(status != SUCCESS)
    return status;
#endif

  // temp .. set preset
  sis3820_RegisterWrite(gVme,gMcsBase , SIS3820_ACQUISITION_PRESET, LNE_total); // select number of LNE       

  if(NumSisChannels > N_SCALER_MAX)
    {
      cm_msg(MERROR,"Begin_of_Run","Number of Scaler channels selected (%d) exceeds maximum permitted for POL (%d)",
	     NumSisChannels,N_SCALER_MAX);
      return DB_INVALID_PARAM;
    }
  for (i=0 ; i < NumSisChannels ; i++)
    {
      printf("scaler %d ", i);     
      if (scaler[i].ps != NULL)
	{
	  if(debug)printf("free scaler %d\n", i);
	  free(scaler[i].ps);
	  scaler[i].ps = NULL;
	}  
      
      scaler[i].nbins = num_bins ;
      scaler[i].ps =  (DWORD *) malloc(sizeof(DWORD) * num_bins ); 
      printf("malloc scaler %d [%d] (%p)\n", i,  num_bins, scaler[i].ps);
      scaler_clear(i);
      printf("cleared scaler %d (%d)\n",i , scaler[i].nbins); 
    }

  clear_mcs_counters();
  
  
  /* cleanup previous booking
     realloc memory for histogram(s) */
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      // CumSum[i]=0;
      if (histo[i].ph != NULL)
	{
	  if(debug)printf("free histo %d\n", i);
	  free(histo[i].ph);
	  histo[i].ph = NULL;
	}
      histo[i].nbins = num_bins;
      histo[i].ph = (DWORD *) malloc(sizeof(DWORD) * num_bins);
      if(histo[i].ph == NULL)
	{
	  cm_msg(MERROR,"begin_of_run","could not allocate space for histogram storage");
	  return DB_INVALID_PARAM;
	}
      
      // if(debug)
	printf("malloc histo %d [%d] (%p)\n", i, (num_bins), histo[i].ph);
      histo_clear(i);
      //  if(debug)
	printf("cleared histo %d (%d)\n",i , histo[i].nbins);
    }
  

  // malloc a buffer to store the data from 1 supercycle
  float db=(float)data_bytes;
  if (db == 3) db = 4;  // 24-bit data uses 32 bits per word
  printf("LNE_total=%d  NumSisChannels=%d  db/4=%f\n", LNE_total,  NumSisChannels, db/4);
  gbl_db_size32 = (LNE_total * NumSisChannels * db/4) + 500 ;  // size in 32-bit words plus 500 for luck

  if(gbl_data_buffer != NULL)
    free (gbl_data_buffer);
  gbl_data_buffer = (uint32_t *) malloc (sizeof(uint32_t) * gbl_db_size32);
  
  if(gbl_data_buffer != NULL)
    printf("malloc data buffer of size %d words\n",gbl_db_size32); 
  else
    {
      cm_msg(MERROR,"begin_of_run","malloc for %d words fails for data buffer", gbl_db_size32);
      return  DB_INVALID_PARAM;
    }
  gbl_buf_index=0;






  
  if (ps.input.sis3820.sis_mode > 0 )  // REAL mode
    {
      status = DAC_set_scan_params();
      if(status != SUCCESS) 
	return status;
      


    } // end of real mode
  else
    dac_ninc = 0;

 
  
  // Clear the counters
  gbl_DAC_n = (dac_ninc + 1); // set for incr_DAC to start a new sweep
  gbl_SCAN_n = 0; // incr_DAC will increment this
  gbl_CYCLE_n = 0; // incremented by cycle_start
  gbl_SCYCLE_n = 1; // starting supercycle 1
  gbl_his_cntr=gbl_cycle_cntr=0;
  gbl_skip_cycle_cntr=0;
  gbl_skip_cycle=FALSE; 
  if(hot_timer)
    gettimeofday(&eoct,NULL);


  if(stop_run_flag)
    {
      printf("begin_of_run: Waiting for run to stop\n");
      return status;
    }

  if(debug)printf("begin_of_run: calling incr_DAC\n");
  
   status = incr_DAC(); // set DAC to first value

  if(status != SUCCESS)
    {
      printf("begin_of_run: error from incr_DAC\n");
      return status;
    }
 
  if(hot_timer)
    {
      // time how long rio has to average the ADC values
      gettimeofday(&riot,NULL);
    }


  bin_zero=num_bins;
  cycle_start(); // start the cycle
  bor_flag = 0; // turn off flag
  gbl_in_tol = 1; // in tolerance by default
  
  watchInterval = 25;
  printf("End of begin-of-run  (debug=%d)\n\n",debug); 
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  INT status;

  printf("end_of_run: starting\n");


  gRunNumber = run_number;

  static bool gInsideEndRun = false;
  watchInterval = 50;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  printf("end run %d\n",run_number);

  if(gVme == NULL) // end_of_run called from frontend_init if run in progress
      printf("end_of_run: VME is not open\n");
  else
    {
#ifdef HAVE_PPG
      disable_trigger(); // stop the PPG
#endif
    }

  // GalilRio connects before frontend_init
      dac_val=0.0;
#ifdef HAVE_GALILRIO
      set_DAC(dac_output_chan, 0.0); // set DAC to zero
      dac_read= read_DAC(); // readback DAC set value
#endif
  
  gInsideEndRun = false;

  if(debug)
    {
      printf("\nCounters (debugging)\n");
      printf("   Counters Is idle %d Not idle %d\n", gvmeisidle_cntr,gvmenotidle_cntr);
      printf("   HIST true in send_hist_event counter = %d\n",hist_true_cntr);
      printf("   CYCLE true in send_hist_event counter = %d\n",cycl_true_cntr);
      
      printf("   DAC step statistics:\n");
      printf("   DAC step within jitter: %d\n",   dac_step_cntr[0] );
      printf("   DAC step < minimum step: %d\n",   dac_step_cntr[1] );
      printf("   DAC step < jitter*5 (begin sweep only): %d\n\n",   dac_step_cntr[2] );
    }
  fflush(stdout);
  printf("end_of_run: done\n");

  // cm_yield(500); // wait a bit   // transition has started workin
  // status = post_stop(run_number, error);  // cannot use transition mechanism with new midas. Calls end_of_run twice.
  return status;

  //return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gRunNumber = run_number;
  printf("Not supported... should be linked to delayed transition which isn't working\n");
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gRunNumber = run_number;
  resume_flag=1;
  printf("Not supported... should be linked to delayed transition which isn't working\n");
  return SUCCESS;
  //  printf("resume_run: re-enabling SIS and PPG\n");
  //enable_SIS();
  //enable_PPG();
  //resume_flag=0;
  return SUCCESS;
}

static int gVmeIsIdle = 0;
static unsigned int lacq_count = 0;  // store last value of acq_count
//DWORD my_time;

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  INT status;
  struct timeval t1; // timer
  double td1;
 

#ifdef HAVE_PPG
  watchrabbit++;
  if(watchrabbit % watchInterval == 0)
    set_watchdog();
  
  if (gVmeIsIdle) // this is always true as amount of data is very small
    {	
      gvmeisidle_cntr++;
      if (run_state == STATE_RUNNING)
	{ /*      RUNNING  */
	  if(!gbl_IN_CYCLE)
	    {
	      /* wait until other relevent  equipment have run */
	      
	      if (waiteqp[CYCLE] )
		{
		  if(hot_debug)
		    printf("frontend_loop: Waiting for CYCLE decoding\n" );
		  
		  return SUCCESS; /* readout is not yet finished i.e. all equipments have not run */
		}
	    
	      
	      if(stop_run_flag)  // this goes with deferred stop which is not working
		{ // ... but we are waiting to stop
		  if(hot_debug)  
		    printf("frontend_loop: NOT restarting cycle... waiting for run to stop\n");
		  return SUCCESS;
		}
	  
	      // Set DAC to next value 
          
	      if(hot_debug) 
		printf("frontend_loop: calling incr_DAC\n ");
	      prev_dac_val = dac_val;  prev_dac_read=dac_read;  // remember present values
	     
	      status = incr_DAC(); // will stop run if fails
	      gettimeofday(&riot,NULL);
	      
	      // new supercycle
	      gbl_SCYCLE_n++; // increment supercycle counter
	      gbl_cycle_cntr = 0; // clear cycle cntr (counts cycles within supercycle i.e. per DAC value)
	      enable_SIS(); // enable hardware
	      
	      
	      // Start a new PPG supercycle 
	  
	      if(hot_debug)
		{
		  printf("frontend_loop: about to restart SC with gbl_rd=%d and waiteqp[CYCLE],waiteqp[FIFO],waiteqp[HISTO],waiteqp[INFO]=%d %d %d %d %d\n",
			 gbl_rd, waiteqp[CYCLE],waiteqp[FIFO],waiteqp[HISTO],waiteqp[INFO],waiteqp[FIFO]);
		}
		
	      
	      
	      if(hot_timer)
		{
		  gettimeofday(&t1,NULL);
		  // compute elapsed time in millisec
		  td1 = (t1.tv_sec - wcyc.tv_sec) * 1000.0;      // sec to ms
		  td1 += (t1.tv_usec - wcyc.tv_usec) / 1000.0;   // us to ms 
		  printf("frontend_loop: SuperCycle restarted %f ms since waiteqp[CYCLE] set TRUE (in send_hist_event) \n",td1);
		}
	      
	      cycle_start(); // start a new supercycle... calls enable_PPG
	    }
	  //  cm_yield(100);
	} // end of running 
      
    }
  else
    gvmenotidle_cntr++;
  
#endif  
  return SUCCESS;
}

INT cycle_start(void)
{
  struct timeval now;
  double td=0;
  float ftmp;
 
  // start a new supercycle 
  
  if(hot_debug)printf("cycle_start: starting and clearing gbl_rd\n");

  gbl_rd=gbl_buf_index=0;
  my_dac_val = dac_val;
  ftmp = (dac_val * 1000)+0.5; // round up
  
  gbl_data_buffer[0]=(int)ftmp;  // store current DAC value

  //printf("cycle_start: dac_val=%f storing value 0x%x %d in gbl_data_buffer[0]\n",dac_val, (unsigned int)(dac_val*1000), (int)(dac_val*1000));
  gbl_buf_index++; // increment for dac value

  lacq_count = acq_count = 0;
  
  if (!bor_flag)  
    {
      if(hot_timer)
	{
	  // time how long it takes to get the histos out
	  gettimeofday(&now,NULL);
	  // compute elapsed time in millisec
	  td = (now.tv_sec - eoct.tv_sec) * 1000.0;      // sec to ms
	  td += (now.tv_usec - eoct.tv_usec) / 1000.0;   // us to ms
	  // td = timeDiff(now, eoct);
	  printf("\ncycle_start: starting   %f ms since end of last SuperCycle was detected (in send_hist_event) \n",td);
	}

    }    


  if(hot_debug)
    { 
	printf("cycle_start: Cycle %d  SCycle %d  Cyc %d DAC %.2fV Sweep %d (Cycles Skipped : %d) histo readout time: %f \n", 
	       gbl_CYCLE_n,gbl_SCYCLE_n,gbl_cycle_cntr, dac_val, gbl_SCAN_n,gbl_skip_cycle_cntr, td);
    }
  flush=FALSE; // reset flush
  data_flushed=FALSE;
  
  gbl_IN_CYCLE=TRUE;
  waiteqp[FIFO]=TRUE; // waiting for scaler data to be read
  /* Cycle process sequencer */
  if(bor_flag)
    enable_SIS();  // normally enabled in frontend_loop
    
  enable_PPG(); //  restarts PPG at beginning of supercycle
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  assert(!"Not implemented");
}

/*-- Event readout -------------------------------------------------*/ 
static const int kMaxNumSisChannels = 32; // index of arrays; 32 channels max
static uint32_t  gSumMcsEvents; // sum the number of events
//static uint32_t  gMaxMcs[kMaxNumSisChannels];  // max value of SIS channels
//static uint64_t  gSumMcs[kMaxNumSisChannels];  // sum SIS channels
//static uint32_t  gSaveMcs[kMaxNumSisChannels]; // sampled SIS data
static uint32_t wc = 0;


static double timeDiff(const struct timeval&t1,const struct timeval&t2)
{
  return (t1.tv_sec - t2.tv_sec) + 0.000001*(t1.tv_usec - t2.tv_usec);
}

static struct timeval gSisLastRead;

//static int lastmsg=0;

static int have_SIS_data(void)
{
  struct timeval now;
  

  int haveData = sis3820_DataReady(gVme,gMcsBase);
  if (haveData == (-1))
    haveData = 0;

  int minread  = 1000*32;

  gettimeofday(&now,NULL);

  double td = timeDiff(now, gSisLastRead);

  //printf("sis: td: %f, haveData: %d\n",td,haveData);

  if(!flush)
    {
      if (td < 1.0 && haveData < minread)
	return 0;
    }
  //int toRead = minread;
  //int toRead = 128*1024/4;
  int toRead = max_event_size/4 - 5000;

  if (toRead > haveData)
    toRead = haveData;
  if (0)
    if (toRead & 0x1F)
      printf("odd available data: %d (%d)\n",toRead,toRead&0x1F);

  if (!flush) // flush last data words
    toRead = toRead & 0xFFFFFFE0;

  if(flush)
    data_flushed=TRUE; // data has now been flushed
  // if(haveData>0 || toRead>0)  
  //  {
      //  if(hot_debug)
      //	printf("have_SIS_data:  haveData=%d flush=%d toRead=%d\n",haveData,flush,toRead) ;
  //   }
    
  if (toRead == 0)
    return 0;
  
  return toRead;
}

static int read_SIS(char* pevent,  int toRead32)
{
  
  // called from read_vme_event

  //if (toRead32*4 < 20000)
  //  return 0;
  //
  //toRead32 = 32*1;

 
  if(debug)printf("read_SIS: starting with toRead32=%d\n",toRead32);
  int used = bk_size(pevent);
  int free = max_event_size - used;
  if (toRead32*4 > free)
    {
      printf("read_SIS: buffer used %d, free %d, toread %d  (in bytes %d)\n", used, free, toRead32, toRead32*4);
      toRead32 = (free-10*1024)/4;
      printf("read_SIS: 1 toRead32= %d\n",toRead32);
      toRead32 = toRead32 & 0xFFFFFFE0;
      printf("read_SIS: 2 toRead32= %d\n",toRead32);
      if (toRead32 <= 0)
	{
	  printf("read_SIS: nothing to read  - returning\n");
	  return 0;
	}
    }

  //int maxDma = 256*1024;
  int maxDma = 64*1024;
  if (toRead32*4 > maxDma)
    toRead32 = maxDma/4;

  gettimeofday(&gSisLastRead, NULL);


  /* create data bank */
  uint32_t *pdata32;

  const char bankname[] = "MCS0";  // raw data
  
  bk_create(pevent, bankname, TID_DWORD, (void **)&pdata32);

  if(hot_debug)
    printf("read_SIS: read %d 32-bit words\n",toRead32);

  *pdata32  = (uint32_t)(my_dac_val*1000);  // put current DAC set value as first word of every MCS0 bank
  pdata32++; // increment

  if(hot_debug)printf("read_SIS: writing current dac value %d 0x%x into pdata \n", (uint32_t)(my_dac_val*1000), (uint32_t)(my_dac_val*1000));

  int rd = sis3820_FifoRead(gVme,gMcsBase,pdata32,toRead32);
 
  gbl_rd+=rd;
  wc += toRead32*4;

  if(hot_debug)
    printf("read_SIS: to read %d 32-bit words;  number read is rd=%d; now gbl_rd=%d and wc=%d\n",toRead32,rd,gbl_rd,wc);
  
  if (debug) // dump first four words 
        printf("sis3820 32-bit data (first 4 data words from SIS): 0x%x 0x%x 0x%x 0x%x\n",pdata32[0],pdata32[1],pdata32[2],pdata32[3]);
     
  if(gbl_buf_index+rd >= gbl_db_size32)
    {
    cm_msg(MERROR,"read_SIS","Array overflow - about to copy more words gbl_buf_index (%d) + rd (%d) = %d into array whose size is gbl_db_size32=%d \n",
	   gbl_buf_index,rd,(gbl_buf_index + rd), gbl_db_size32);
    stop_run();
    return 0;
    }
  
  if(debug)printf("gbl_data_buffer[0] = 0x%8.8x  %d  (DAC value) \n",gbl_data_buffer[0], gbl_data_buffer[0] );
  for (int j=0; j<rd; j++)
    {
      gbl_data_buffer[gbl_buf_index+j] = pdata32[j]; // pdata has already been incremented to point to 1st word of SIS data
      if(debug)printf("gbl_data_buffer[%d] =  0x%8.8x  %d\n", (gbl_buf_index+j),  gbl_data_buffer[gbl_buf_index+j], gbl_data_buffer[gbl_buf_index+j]);
    }
 
  gbl_buf_index+=rd;
  if(debug)
    printf("read_SIS: gbl_buffer_index is now=%d  rd=%d  array size  gbl_db_size32=%d \n",
	   gbl_buf_index,rd,gbl_db_size32);

  //printf("read_SIS: pevent= %p \n",pevent);
  if(send_mcs)
    bk_close(pevent, pdata32 + rd);  // send out MCS0 data bank (raw data)  pdata32 alreading incremented  for DAC Set Value
  else
    bk_close(pevent,pdata32);  // don't send MCS0

 
  // decoding scaler data was here; moved to read_mcs_event

  if (send_mcs)
    return 1;
  else
    return 0;
}

INT decode_scaler_data(void)
{
  // called from send_his_event at end of supercycle
  //  uint32_t *pdata32;
  // int toRead32;
  int i,k;
  INT histo_bin_cntr;
  uint16_t data[2];
  // uint8_t  data8[4];
  int db = data_bytes;
  float first_word=-101;

  // currently only supports 16-bit data


  int total_event_length = (NumSisChannels * db)/4 * num_bins;

  struct timeval t1,t2; // timer
  double td1=0;


 if(hot_timer)
   gettimeofday(&t1,NULL);


 i=(int)gbl_data_buffer[0]; // may be negative
 first_word = (float)i;
 first_word = first_word/1000;
 if(hot_debug) printf("decode_scaler_data: i=%d first_word=%f \n",i,first_word);
 // first_word = ( (float) gbl_data_buffer[0])/1000 ;
 
 
  if(hot_debug)
    {
      printf("decode_scaler_data: first word is DAC value= %f; current dac_val is %f\n",((float)gbl_data_buffer[0]/1000),(float)dac_val );
      printf("decode_scaler_data: gbl_buf_index=%d;  Data length (for SC) =gbl_rd=%d; Data length for 1 cycle = %d \n",gbl_buf_index,gbl_rd, total_event_length);   
    }
  if(fabs(first_word - dac_val) > 0.0005)  // may be a rounding error
      printf("First word from data bank and dac_val should be (more or less) identical. First word=%f, dac_val=%f\n",first_word,dac_val);

 
  if(debug)
    {  /* print scaler data */
      printf(" 0001 ->            "); 
      for(i=1; i <= gbl_buf_index; i++)
	{
	  if(i%8 ==0 )
	    printf("\n %4.4d -> ",i);
	  
	  printf(" 0x%8.8x",gbl_data_buffer[i]);
	}
      printf("\n last i=%d \n",i);
    }

  int istart=1; // skip DAC value
  if(ps.input.discard_first_cycle) // in real mode, first cycle probably not valid
    {  
      // ignore the first event in the buffer;
      //printf("index to first valid event is %d; data is 0x%x\n", (total_event_length +1), gbl_data_buffer[total_event_length +1]);
      istart=total_event_length +1;
      gbl_skip_cycle_cntr++; // skipped first cycle
    }

  
  // int event_bytes = NumSisChannels * db;   // per time bin
  //int numEvents = gbl_rd * 4 /event_bytes; 

 

  if(debug)
    {
      /* 
	 Debug...  print data as cycles  
      */
      int index=1; // skip DAC set value
      int l= (NumSisChannels * db)/4  ;
      printf("Bin  Inputs 1,0 etc.  NumSisChannels=%d  l=%d \n",NumSisChannels,l);
      DWORD j=0;
      int idx=0;
      int cycle=1;
      printf("Cycle %d\n",cycle);
      
      for( i=index; i < gbl_buf_index; i++)
	{
	  if(idx%l ==0 )
	    {
	      if(j == 1 ) 
		printf("... discard bin 0");
	      if(j==num_bins)
		{
		  cycle++;
		  printf("\n Cycle %d  at index %d\n", cycle, (i+1));
		  j=0;
		}
	      printf("\n Time Bin %d  -> ",j);
	      j++;
	    }
	  printf(" 0x%8.8x",gbl_data_buffer[i]);
	  idx++;
	}
      printf("\n");

    }      /*  end of debug */

  uint32_t *mptr = &gbl_data_buffer[istart];  // point to first valid event
  uint32_t *eptr = &gbl_data_buffer[gbl_buf_index];  // pointer to end of buffer
 
  if(debug)printf("decoding starting at index istart=%d contents= 0x%x\n",istart,*mptr);

  histo_bin_cntr=num_bins+1; // will start at time bin zero
  gbl_cycle_cntr=0;

  // decode the data (16-bit only at present)
   

  while (mptr < eptr)   // for the whole buffer
    {
      gSumMcsEvents++;  // number of LNE Events
      
      if(histo_bin_cntr >= (INT)num_bins)
	{
	  histo_bin_cntr=0; // bin 0  - next PPG  cycle
          if(debug)printf("histo_bin_cntr set to 0;  gbl_his_cntr=%d  \n", gbl_his_cntr );
	}
      if(debug)
	printf("\n  Working on time bin %d  of cycle %d within supercycle %d \n",histo_bin_cntr,gbl_cycle_cntr,  gbl_SCYCLE_n);

      for ( i=0; i<NumSisChannels/2; i++) // use number of channels actually enabled 
	{
	  int j = i*2;
	  
	  uint32_t v = *mptr++;
	  //printf("Working on data word i=%d j=%d v=0x%x  (bin=%d) \n",i,j,v,histo_bin_cntr);
	  data[0] = v & 0xFFFF;
	  data[1] = (v & 0xFFFF0000) >> 16;
	  for (k=0; k<2; k++)
	    {
	      // check the pointer
	      if(scaler[j+k].ps == NULL)
		{
		  cm_msg(MERROR,"decode_scaler_data","Error - scaler pointer is null for index %d. Cannot histogram",(j+k));
		  return 0;
		}

	      if (histo_bin_cntr > 0 ||  ps.input.discard_first_bin == FALSE)
		{
		  scaler[j+k].sum_cycle += data[k];
		  // if(data[k] !=0) printf("added data[%d]=%d to scaler[%d].sum_cycle -> %f\n",
		  //			 k,data[k],(j+k), scaler[j+k].sum_cycle);
		}

	    
	      if( ps.input.discard_first_bin)
		{
		  if(histo_bin_cntr > 0 )
		    {
		      scaler[j+k].ps[histo_bin_cntr-1] += (DWORD) data[k];
		      //  if(data[k] !=0) printf("added data[%d]=%d to scaler[%d].ps[%d] -> %d\n",
		      //		     k,data[k],(j+k),(histo_bin_cntr-1), scaler[j+k].ps[histo_bin_cntr-1]);
		    }
		}
	      else
		{
		  scaler[j+k].ps[histo_bin_cntr] += (DWORD) data[k];
		  // if(data[k] !=0) printf("added data[%d]=%d to scaler[%d].ps[%d] -> %d\n",
		  //		 k,data[k],(j+k),histo_bin_cntr, scaler[j+k].ps[histo_bin_cntr]);
		  
		}
	    } // end for k loop
	} // end of number of SIS channels loop

      histo_bin_cntr++;  // next time bin

     
      if(histo_bin_cntr >= (INT)num_bins)   // End of 1 cycle
	{
	  gbl_his_cntr++;   // count number of complete cycles
          gbl_cycle_cntr++; // next cycle within the supercycle
	 
	  if(debug)printf("decode_scaler_data: Histogrammed total of %d cycles ;  %d cycles were skipped; gbl_cycle_cntr=%d\n ",
			      gbl_his_cntr, gbl_skip_cycle_cntr, gbl_cycle_cntr);
	  
	  process_histo();  // increments gbl_CYCLE_n
	  clear_mcs_counters(); // clear cycle scalers
	}
    
    } // while

  if(hot_debug)printf("decode_scaler_data: Histogrammed total of %d cycles ;  %d cycles were skipped; gbl_cycle_cntr=%d \n ",
			      gbl_his_cntr, gbl_skip_cycle_cntr, gbl_cycle_cntr);

  // Store this data  for sending out histogram
  gbl_histo_bank_data[0] = (float)gbl_CYCLE_n; // cycle number
  gbl_histo_bank_data[1] =  (float)gbl_SCYCLE_n; // supercycle number
  gbl_histo_bank_data[2] = dac_val; // DAC set value i
 
#ifdef HAVE_GALILRIO
  // gbl_histo_bank_data[3] =  rio->GetAnalogInput(adc_chan_b); // ADC1  user readback... 
  gbl_histo_bank_data[3] = 0; //  later replaced by average ADC1  value when HISI is sent
#endif
  gbl_histo_bank_data[4] = float(gbl_DAC_n -1); // DAC increment number (0 to ninc) -  rather than 1 to ninc+1
  gbl_histo_bank_data[5]++; // increment counter for number of PPG cycles added   
  gbl_histo_bank_data[6]=first_word; // DAC set value from bufferi
  



  if(debug)
    printf(" decode_scaler_data: setting waiteqp[FIFO] false \n");
  waiteqp[FIFO]=FALSE;
  


 // End of processing supercycle
  gbl_buf_index=0; //  reset index read for next supercycle's data



  //if(hot_debug)
  //printf("decode_scaler_data: setting waiteqp[HISTO]  true\n");


   //waiteqp[HISTO]=TRUE; // histo banks (not to ODB)
  
    if(hot_timer)
	    {
	      gettimeofday(&t2,NULL);
	      // compute elapsed time in millisec
	      td1 = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
	      td1 += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms 
	      printf("decode_scaler_data: routine took %f ms  \n",td1);
	    }


  return SUCCESS;
}




INT  process_histo(void)
{
  INT i;
  DWORD j;

  // called from decode_scaler_data which is called at end of supercycle
  // processes scaler data from each PPG cycle into a histo

  // fill histogram

  

  
  // add the cycle scaler data to the cumulative histogram
  for (i=0; i<NumSisChannels; i++)
    {
      for (j=0; j<num_bins; j++)
	histo[i].ph[j] += scaler[i].ps[j];
	  
      histo[i].sum += scaler[i].sum_cycle;
    
      if(debug)	printf("histo[%d].sum = %f;   scaler[%d].sum_cycle=%f\n",i, histo[i].sum, i, scaler[i].sum_cycle);
    }
    
 
  gbl_CYCLE_n++;  // next PPG cycle to histogram (starts at 0)

  return SUCCESS;
}





INT read_sispulser_event(char *pevent, INT off)
{

#ifdef HAVE_SIS3820
   printf("read_sispulser_event:  Pulse SIS3820 user LED\n");

 
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_CONTROL_STATUS,1<<0);
  // issue a PCI read to flush posted PCI writes
  uint32_t d1 = sis3820_RegisterRead(gVme,gMcsBase,SIS3820_CONTROL_STATUS);
  //usleep(1);
  sis3820_RegisterWrite(gVme,gMcsBase,SIS3820_CONTROL_STATUS,1<<16);
  // issue a PCI read to flush posted PCI writes
  uint32_t d2 = sis3820_RegisterRead(gVme,gMcsBase,SIS3820_CONTROL_STATUS);
  return 0*(d1+d2);
#else
  return 0;
#endif
}





extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  //printf("poll_event %d %d %d!\n",source,count,test);

  for (int i = 0; i < count; i++)
    {
      //udelay(1);
      if (test)
        xusleep(1000);
      else
        return TRUE;
    }
  return 0;

#if 0
  for (int i = 0; i < count; i++)
    {
      //udelay(1);
      int lam = 0;
      lam |= have_ADC_data();
#ifdef HAVE_SIS3820
        lam |= have_SIS_data();
#endif

      if (lam)
        if (!test)
          return lam;
    }
  return 0;
#endif
}

INT read_vme_event(char *pevent, INT off)
{
  static int gCountEmpty = 0;
  int size = 0;

#ifdef HAVE_SIS3820
  int count;


  // SIS has how much data?
  
  count = have_SIS_data();
  
  // printf("read_vme_event:  SIS  count=%d, fifo %d  flush=%d\n",  count, sis3820_DataReady(gVme,gMcsBase),flush);
  
  if (count > 0)
    {
      if (!size)
	bk_init32(pevent);
      size |= read_SIS(pevent, count); 
      if(debug)printf("read_vme_event: after read_SIS size=%d\n",size);
    }
      
#endif  // SIS3820
    


  if (size)
    {
      gCountEmpty = 0;
      gVmeIsIdle = 0;
      if(debug)
	printf("read_vme_event %d bytes\n", bk_size(pevent));
      return bk_size(pevent);
    }
  

  gCountEmpty++;

  if (gCountEmpty > 2) {
    gVmeIsIdle = 1;
    xusleep(1000);
  }
  
  return 0;
}



#ifdef HAVE_PPG

INT prestart(INT run_number, char *error) 
{ 
  struct stat stbuf; 
  int size,status; 
  time_t timbuf; 
  char timbuf_ascii[30]; 
  int elapsed_time; 
  char str[80];

  gbl_IN_CYCLE=FALSE;

  printf("prestart: starting\n");

  /* Get current  settings */
  size = sizeof(ps);
  status = db_get_record(hDB, hTASet, &ps, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "prestart", "cannot retrieve /Equipment/Pol_Acq/Settings record (size of ps=%d)",size);
      return DB_NO_ACCESS;
    }


  printf("sismode = %d\n",ps.input.sis3820.sis_mode); 
  printf("requested bins = %d\n",ps.input.num_bins_requested); // requested_bins

  // read globals ppgmode sismode, num_bins, min_delay

  num_bins = ps.input.num_bins_requested; 
  if( ps.input.discard_first_bin)
     num_bins++; // add one extra bin so we can discard first bin

 // Write actual number of time bins to output for display
  ps.output.num_bins_per_cycle = num_bins;
  status = db_set_value(hDB, hOut, "num bins per cycle", &ps.output.num_bins_per_cycle, sizeof(INT), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"num bins per cycle\" (%d)",status); 
      return status;
    }
 
  num_cycles_per_supercycle = odbReadInt("/Equipment/POL_ACQ/settings/input/Num cycles per supercycle",0,1);
  printf("number of cycles/supercycle requested: %d\n",num_cycles_per_supercycle);
  if(num_cycles_per_supercycle < 1) 
    num_cycles_per_supercycle =1; // minimum value

  ps.output.num_cycles_per_sc = num_cycles_per_supercycle;
  if(ps.input.discard_first_cycle) // in real mode, first cycle probably not valid
    {
      LNE_total = num_bins  * (num_cycles_per_supercycle +1); // throw away first cycle
      ps.output.num_cycles_per_sc = num_cycles_per_supercycle +1;
    }
  else
    LNE_total = num_bins  * num_cycles_per_supercycle; 

 // Write number of cycles/supercycle to output for display
  status = db_set_value(hDB, hOut, "num cycles per sc", &ps.output.num_cycles_per_sc, sizeof(INT), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"num cycles per sc\" (%d)",status); 
      return status;
    }

  // Write number of valid time bins to output for display
  ps.output.valid_bins_per_sc = ps.input.num_bins_requested * num_cycles_per_supercycle;
  status = db_set_value(hDB, hOut, "valid bins per sc", &ps.output.valid_bins_per_sc, sizeof(INT), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"valid bins per sc\" (%d)",status); 
      return status;
    }
  
  // Write this to LNE_preset 
  sprintf(str,"input/sis3820/LNE preset");
  status = db_set_value(hDB, hTASet, str, &LNE_total, sizeof(LNE_total), 1, TID_DWORD);  
  if(status !=SUCCESS)
    {
      cm_msg(MERROR,"prestart","cannot set ODB key  ...%s to %u (%d)",str,LNE_total,status); 
    }
  ps.input.sis3820.lne_preset = LNE_total;
  
  if(ps.input.sis3820.sis_mode == 0)
    {
      if( num_bins <= 0)
	{
	  cm_msg(MERROR,"prestart","Illegal number of bins (%u)",num_bins);
	  return  DB_INVALID_PARAM;
	}
      return SUCCESS; // no more checks for SIS test mode (no PPG)
    }


  status = check_PPG_input_params(); // check for valid PPG parameters
  if (status !=SUCCESS)
    return  DB_INVALID_PARAM;

  printf("prestart: read DAC sleep time from odb as %f ms\n",ps.input.dac_sleep_time__ms_);
 
 

    
  printf("prestart: read scan_loop_counter from ODB as %d\n",ps.input.scan_loop_count);

 // In real mode, PPG is restarted after each cycle by the end-of-TOF signal 
  if(ps.input.ppg_external_start) // real mode
    ps.input.scan_loop_count =1;  // scan loop count must be 1. Cycles restarted by external start.
  else
    ps.input.scan_loop_count =  ps.output.num_cycles_per_sc;  // actual number of cycles needed
      

  // Write scan_loop_count to output for debug
  ps.output.scan_loop_count =  ps.input.scan_loop_count ;
  status = db_set_value(hDB, hOut, "scan loop count", &ps.output.scan_loop_count, sizeof(ps.output.scan_loop_count), 1, TID_INT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"prestart","cannot write output parameter  \"scan loop count\" (%d)",status); 
      return status;
    }

  

  // set the DAC sleep time and TOF pulse width (DAC service pulse is used for TOF) - this is included in PPG cycle
  status = set_dac_sleep_time();
  if(status != SUCCESS)
    return status;



  

  if(ps.input.run_tri_config) //  (POL expt does not run tri_config)
    { 
      // Set the scan loop count to  ps.output.scan_loop_count
      sprintf(str,"begin_scan/loop count");
      status = db_set_value(hDB, hPPG, str, &ps.output.scan_loop_count, sizeof(ps.output.scan_loop_count), 1, TID_INT); 
      if(status != DB_SUCCESS) 
	{ 
	  cm_msg(MERROR,"prestart","cannot set ODB key \"%s\" to %d  (%d)",str, ps.input.scan_loop_count, status); 
	  return status;
	}
    
      // run tri_config
      printf("prestart: sending system command:\"%s\" \n",cmd); 
      status =  system(cmd); 
 
      stat (ppgfile,&stbuf); 
      printf("prestart: file %s  size %d\n",ppgfile, (int)stbuf.st_size); 
      
      if(stbuf.st_size < 0) 
	{ 
	  cm_msg(MERROR,"prestart","PPG load file %s cannot be found",ppgfile); 
	  //set_equipment_status(equipment[PPG].name, "PPG param check","red");
	  return DB_FILE_ERROR ; 
	} 
 
      strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) ); 
      printf ("prestart: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size); 
 
 
      // get present time 
      time (&timbuf); 
      
      strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) ); 
      printf ("prestart: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf); 
      
      elapsed_time= (int) ( timbuf - stbuf.st_mtime ); 
      
      printf("prestart: time since ppg loadfile last updated: %d seconds \n",elapsed_time); 
      if(elapsed_time > 3) 
	{ 
	  cm_msg(MERROR,"prestart","PPG load file %s is too old",ppgfile); 
	  //set_equipment_status(equipment[PPG].name, "PPG param check","red");
	  return DB_FILE_ERROR ; 
	} 
    }
#ifdef NEW_PPG
  else
    {  // POL experiment modifies the ppg loadfile with the relevent parameters (e.g. number of bins, DAQ sleep time)
      status = modify_ppg_loadfile(ppgmode);
      if(status != SUCCESS)
	return status;
    }
#endif


  return SUCCESS; 
} 

 
 
INT poststart(INT run_number, char *error) 
{ 
  INT status; 
  char copy_cmd[132]; 
 
  printf("poststart: starting\n");

  if(ps.input.sis3820.sis_mode == 0)  // SIS test mode 
    return SUCCESS;

  if(run_state == STATE_RUNNING) 
    {
      if(ps.input.run_tri_config)
	{
	  //printf("data path: %s\n",data_path);  // data disk not mounted from lxpol
	  sprintf(copy_cmd,"cp %s/ppgload/bytecode.dat %s/ppgload/run%d_bytecode.dat &", 
		  ps.input.ppg_path ,ps.input.ppg_path,run_number); 
	  status =  system(copy_cmd);
	  if(status != 0)
	    cm_msg(MINFO,"poststart","error status after system command \"%s\" is %d",copy_cmd, status);
	}
#ifdef NEW_PPG      
      sprintf(copy_cmd,"cp %s/ppgload/ppgload.dat  %s/ppgload/run%d_ppgload.dat &", 
	      ps.input.ppg_path,ps.input.ppg_path,run_number); 
      status =  system(copy_cmd); 
      if(status != 0)
	cm_msg(MINFO,"poststart","error status after system command \"%s\" is %d",copy_cmd, status); 
    } 
#endif
  return SUCCESS; 
}
#endif // HAVE_PPG

#ifdef GONE   // testing only
INT pre_stop(INT run_number, char *error) 
{
  printf("pre_stop: starting\n");
  return SUCCESS;
}
#endif

INT post_stop(INT run_number, char *error) 
{
  INT i;

  printf("post_stop: starting\n");
  if(!waiteqp[HISTO])
    {
      printf("post_stop: free scaler,  NumSisChannels =%d \n", NumSisChannels);
      // for (i=0 ; i <  NumSisChannels ; i++)
      for (i=0 ; i <  N_SCALER_MAX ; i++)
	{
	  if (scaler[i].ps != NULL)
	    {
	      if(debug)	
		printf("freeing scaler %d [%d] (%p)\n", i, num_bins, scaler[i].ps); 
	      free(scaler[i].ps);
	      scaler[i].ps = NULL;
	    }
	  else
	    {
	      if(debug)
		printf("scaler[i].ps is NULL, i=%d\n",i);
	    }
	}
      

      printf("free histo, N_HISTO_MAX =%d\n", N_HISTO_MAX);
      for (i=0 ; i <  N_HISTO_MAX ; i++)
	{
	  if (histo[i].ph != NULL)
	    {
	      if(debug) 
		printf("freeing histo %d [%d] (%p)\n", i, num_bins, histo[i].ph);
	      free(histo[i].ph);
	      histo[i].ph = NULL;
	    }
	  else
	    {
	      if(debug)
		printf("histo[i].ph is NULL, i=%d\n",i);
	    }
	}
      
      
      
      if(gbl_data_buffer != NULL)
	{
	  if(debug)
	    printf("freeing gbl_data_buffer  (%p)\n", gbl_data_buffer);
	  free (gbl_data_buffer);
	  gbl_data_buffer=NULL;
	}
      
    }
  else
    printf("post_stop: waiteqp[HISTO] is true; did not free arrays\n");
  
  fflush(stdout);
  return SUCCESS;
}

INT get_rio_channels(void)
{
  int range_code;
  INT status;
  int i;

  // assign the currently 2 DAC channels from the ODB
  dac_output_chan =  ps.input.galil_rio.dac_params.dac_channels[0];  // index 0
  dac_monitor_chan =  ps.input.galil_rio.dac_params.dac_channels[1]; // index 1

  num_dac_chan = ps.input.galil_rio.dac_params.num_channels_used;
  if(num_dac_chan > 2)
    {
      num_dac_chan=2;
      cm_msg(MINFO,"get_rio_channels","maximum of 2 DAC channels are currently supported");
    }



  num_adc_chan = ps.input.galil_rio.adc_params.num_channels_used;
  if(num_adc_chan > 4) 
    {
      num_adc_chan=4;
      cm_msg(MINFO,"get_rio_channels","maximum of 4 ADC channels are currently supported");
    }



  // Addresses of some of the adc channels
  adc_chan_a = ps.input.galil_rio.adc_params.adc_channels[0]; // direct readback for dac_output_chan
  adc_chan_b = ps.input.galil_rio.adc_params.adc_channels[1]; // user readback

  for(i=0; i<num_adc_chan; i++)
  {
    if(ps.input.galil_rio.adc_params.adc_channels[i] < 0 || ps.input.galil_rio.adc_params.adc_channels[i] > 9)
      {
	cm_msg(MERROR,"get_rio_channels","error adc channel [%d] : input channel number must be between 0 and 9",i);
        return DB_INVALID_PARAM;
      }

  }



  if(num_dac_chan > 1)
    {
      dac_mon_flag = 1;  // flag will cause dac monitor chan to be set
      if(dac_monitor_chan == dac_output_chan)
	{
	  cm_msg(MERROR,"get_rio_channels","the 2 DAC channels selected must have different channel numbers");
	  return DB_INVALID_PARAM;
	}
      if(dac_monitor_chan > 9 || dac_monitor_chan < 0)
	{
	  cm_msg(MERROR,"get_rio_channels","DAC monitor channel value must be between 0 and 9");
	  return DB_INVALID_PARAM;
	}
        
    }
  else
    dac_mon_flag = 0;

   if(dac_output_chan > 9 || dac_output_chan < 0)
	{
	  cm_msg(MERROR,"get_rio_channels","DAC output channel value must be between 0 and 9");
	  return DB_INVALID_PARAM;
	}
  



  // Set up max_DAC and min_DAC for the selected range (dac_output_chan index 0)
  range_code = ps.input.galil_rio.dac_params.dac_range[0];
  switch(range_code)
    {
    case 1:  // 0-5 Volts
      max_DAC=5;
      min_DAC=0;
      break;
    
    case 2:  // 0-10 Volts
      max_DAC=10;
      min_DAC=0;
      break;
    
    case 3:  // -5 to +5 Volts
      max_DAC=5;
      min_DAC=-5;
      break;
    
     default:  // case 4  -10 to +10 Volts
      max_DAC=10;
      min_DAC=-10;
      break;
    }
  // set max and min DAC range as output parameters for custom page
  status = db_set_value(hDB, hOut, "DAC_max", &max_DAC, sizeof(max_DAC), 1, TID_FLOAT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_rio_channels","cannot write output parameter  \"DAC_max\" (%d)",status); 
      return status;
    }

  status = db_set_value(hDB, hOut, "DAC_min", &min_DAC, sizeof(min_DAC), 1, TID_FLOAT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_rio_channels","cannot write output parameter  \"DAC_min\" (%d)",status); 
      return status;
    }
  
  return SUCCESS;
}



INT set_rio_input_config(void)
{
  
  get_rio_channels();
  
  // Set ADC and DAC ranges for each enabled channel
  
  //  char QUERY[]="MG _AQ0,_AQ1;";  // Message command; return range settings for analogue inputs 
  /* Set rio ADC channel 1 into +/- 5V (default is +/- 10V)
     char COMMAND[]="AQ 1,1;";  // AQ Analog Input Configuration  set ADC channel 1 to +/- 5V (default is +/- 10V)
  */
  char QUERY[32];
  char COMMAND[32];
  char str[10];
  float fval[4];
  int i,len;
  

  // ADC channels

  for (i=0; i<4; i++) // 4 channels max
    fval[i]=0; // clear
  
  // Set ADC channel range codes

  printf("set_rio_input_config: setting ADC channel ranges...\n");
  for (i=0; i< num_adc_chan; i++)
    {
      // check values
      if( (ps.input.galil_rio.adc_params.adc_channels[i]< 0) || (ps.input.galil_rio.adc_params.adc_channels[i] > 7))
	{
	  cm_msg(MERROR,"set_rio_input_config","illegal ADC channel %d at index %d. Must be 0-7",ps.input.galil_rio.adc_params.adc_channels[i],i);
	  return DB_INVALID_PARAM;
	}
      if((ps.input.galil_rio.adc_params.adc_range[i]< 1) || (ps.input.galil_rio.adc_params.adc_range[i] > 4))
	{
	  cm_msg(MINFO,"set_rio_input_config","illegal ADC range %d at index %d. Must be 1-4. Using default value (2) ",ps.input.galil_rio.adc_params.adc_range[i],i);
	  ps.input.galil_rio.adc_params.adc_range[i]=2;
	}

  
      sprintf(COMMAND,"AQ %d,%d;", ps.input.galil_rio.adc_params.adc_channels[i],  ps.input.galil_rio.adc_params.adc_range[i]);
      rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );  
      //if(rio_debug)
      printf ("set_rio_input_config:  i=%d  RIO command was \"%s\" \n",i,COMMAND);
    }


  // Now generate the command to read back the range of all ADC channels
  sprintf(QUERY, "MG "); // message
  for (i=0; i< num_adc_chan; i++)
    {
      sprintf(str, "_AQ%d,", ps.input.galil_rio.adc_params.adc_channels[i]); // read range of this ADC channel
      strcat(QUERY, str);
    }
   len=strlen(QUERY);
   QUERY[len-1]='\0'; // remove final comma
   strcat(QUERY,";"); // add final semicolon
   

   rio->Command(QUERY,&REPLY[0],100 );   // read back range of ADC channels
   //if(rio_debug)
   printf (" set_rio_input_config: RIO command was \"%s\" reply: \"%s\"\n",QUERY,REPLY);
   sscanf(REPLY, "%f %f %f %f ",&fval[0],&fval[1],&fval[2],&fval[3]);

   printf("set_rio_input_config: ADC range codes: %s\n", ps.input.galil_rio.adc_params.adc_range_codes);
   for (i=0; i< num_adc_chan; i++)
     printf("set_rio_input_config index %d ADC channel %d range code set %d read back %f  \n",
	    i,ps.input.galil_rio.adc_params.adc_channels[i], ps.input.galil_rio.adc_params.adc_range[i],  fval[i]);
   





   // DAC channels

   printf("\n set_rio_input_config: setting DAC channel ranges...\n");

   for (i=0; i<2; i++) // 2 channels max
     fval[i]=0;

  // Set DAC channel ranges
  for (i=0; i< num_dac_chan; i++)
    {

  // check values
      if( (ps.input.galil_rio.dac_params.dac_channels[i]< 0) || (ps.input.galil_rio.dac_params.dac_channels[i] > 7))
	{
	  cm_msg(MERROR,"set_rio_input_config","illegal DAC channel %d at index %d.  Must be 0-7",ps.input.galil_rio.dac_params.dac_channels[i],i);
	  return DB_INVALID_PARAM;
	}
      if((ps.input.galil_rio.dac_params.dac_range[i]< 1) || (ps.input.galil_rio.dac_params.dac_range[i] > 4))
	{
	  cm_msg(MERROR,"set_rio_input_config","illegal DAC range %d at index %d. Must be 1-4; using default (4)",ps.input.galil_rio.dac_params.dac_range[i],i);
	  ps.input.galil_rio.dac_params.dac_range[i]=4;
	}


      sprintf(COMMAND,"DQ %d,%d;", ps.input.galil_rio.dac_params.dac_channels[i],  ps.input.galil_rio.dac_params.dac_range[i]);
      rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );  
      //if(rio_debug)
      printf ("set_rio_input_config:  i=%d  RIO command was \"%s\" \n",i, COMMAND);
    }

  // Now generate the command to read back the range of all selected DAC channels
  sprintf(QUERY, "MG "); // message
  for (i=0; i< num_dac_chan; i++)
    {
      sprintf(str, "_DQ%d,", ps.input.galil_rio.dac_params.dac_channels[i]); // read  range of this DAC channel
      strcat(QUERY, str);
    }
  len=strlen(QUERY);
  QUERY[len-1]='\0'; // remove final comma
  strcat(QUERY,";"); // add final semicolon
   


  rio->Command(QUERY,&REPLY[0],100 );
  printf (" set_rio_input_config: RIO command was \"%s\" reply: \"%s\"\n",QUERY,REPLY);
  sscanf(REPLY, "%f %f ",&fval[0],&fval[1]);  // 2 channels max

  printf("set_rio_input_config: DAC range codes: %s\n", ps.input.galil_rio.dac_params.dac_range_codes);
  for (i=0; i< num_dac_chan; i++)
    printf("set_rio_input_config index %d DAC channel %d range code set %d read back %f  \n",
	    i,ps.input.galil_rio.dac_params.dac_channels[i], ps.input.galil_rio.dac_params.dac_range[i],  fval[i]);



  if(dac_mon_flag)
    {
      set_DAC(dac_monitor_chan, 0.0); // clear dac monitor output
      printf("Cleared DAC monitor channel\n");
    }

  rio_debug=0;

  return SUCCESS;
}


INT get_rio_averages(void)
{ // 4 channels are averaged 
  int i;
  char QUERY[]="MG AV[0],AV[1],AV[2],AV[3];";
  
  rio->Command(QUERY,&REPLY[0],100 );
  if(rio_debug)printf ("RIO command was: \"%s\" reply: \"%s\"\n",QUERY,REPLY);
  sscanf(REPLY, "%f %f %f %f",&rio_average[0],&rio_average[1],&rio_average[2],&rio_average[3]);
  for (i=0;i<rio_max; i++)
    if(hot_debug)printf("rio_average[%d]=%f\n",i,rio_average[i]);
  return SUCCESS;
}

INT set_rio_filter_params(void)
{
  // called from begin of run
 
  float fread[4];
  char COMMAND[30];
  char WQUERY[]="MG W[0],W[1],W[2],W[3];";
  char FQUERY[]="MG F[0],F[1],F[2],F[3];";
  int i;
  if(rio_debug)printf("size of reply:%d\n",sizeof(REPLY));

  // Write window parameters
  for (i=0; i<rio_max; i++)
    {
      sprintf (COMMAND,"W[%d]=%f;", i, ps.input.galil_rio.adc_averaging.window[i]);
      rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );  
      if(rio_debug)printf ("RIO command was: \"%s\" \n",COMMAND);
    }


  // read back filter values
  
  rio->Command(WQUERY,&REPLY[0], sizeof(REPLY) );
  if(rio_debug)printf ("set_rio_filter_params: RIO command was: \"%s\" reply: \"%s\"\n",WQUERY,REPLY);
  sscanf(REPLY, "%f %f %f %f",&fread[0],&fread[1],&fread[2],&fread[3]); // float numbers
  for (i=0;i<rio_max; i++)
    {
      if(rio_debug) printf("window[%d]=%f\n",i,fread[i]);
      if(fread[i] != ps.input.galil_rio.adc_averaging.window[i])
	{
	  cm_msg(MERROR,"set_rio_filter_params","error setting RIO filter window parameter. Wrote %f read back %f",
		 ps.input.galil_rio.adc_averaging.window[i],fread[i] );
	  return FE_ERR_HW; 
	}
    }

  // Write filter parameters
  for (i=0; i<rio_max; i++)
    {
      sprintf (COMMAND,"F[%d]=%d;", i, ps.input.galil_rio.adc_averaging.filter[i]);
      rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) ); // invalid reply  
      if(rio_debug)printf ("RIO command was: \"%s\" \n",COMMAND);
    }
  // read back filter values
  
  rio->Command(FQUERY,&REPLY[0], sizeof(REPLY) );
  if(rio_debug)printf ("set_rio_filter_params: RIO command was: \"%s\" reply: \"%s\"\n",FQUERY,REPLY);
  sscanf(REPLY, "%f %f %f %f",&fread[0],&fread[1],&fread[2],&fread[3]); // float numbers
  for (i=0;i<rio_max; i++)
    {
      if(rio_debug)printf("filter[%d]=%f\n",i,fread[i]);
      if(fread[i] != (float)ps.input.galil_rio.adc_averaging.filter[i])
	{
	  cm_msg(MERROR,"set_rio_filter_params","error setting RIO filter parameter. Wrote %d read back %f",
		 ps.input.galil_rio.adc_averaging.filter[i],fread[i] );
	  return FE_ERR_HW; 
	}
    }
  return SUCCESS;
}


INT DAC_set_scan_params(void)
{
  /* called from begin_of_run */
  INT status;
  float   dac_stop;       /* stop value for dac sweep (now calculated) */

  dac_ninc = ps.input.num_dac_incr;
  if( dac_ninc < 1)  // dac_ninc
    {
      cm_msg(MERROR,"DAC_set_scan_params","Invalid number of dac sweep steps: %d",
	     dac_ninc );
      return FE_ERR_HW;
    }

  if(!ps.input.galil_rio.disable_step_check)
    {
      if( fabs(ps.input.dac_inc) < fabs( ps.input.galil_rio.dac_step_check.minimum_step__v_ ))
	{
	  cm_msg(MERROR,"DAC_set_scan_params","DAC scan increment value (%.2fV) must be greater than %.2fV",
		 ps.input.dac_inc,ps.input.galil_rio.dac_step_check.minimum_step__v_ );
	  cm_msg(MINFO,"DAC_set_scan_params","DAC minimum step and jitter are in the odb under ..input/dac and can be modified");
	  return FE_ERR_HW;
	}
    }

  dac_stop =  ps.input.dac_start + ( ps.input.dac_inc *  dac_ninc);
  
  if(debug)
    printf("DAC_set_scan_params: DAC start=%f dac_ninc=%d inc=%f; calculated dac_stop=%f\n",
	   ps.input.dac_start, dac_ninc, ps.input.dac_inc, dac_stop);


  if( (dac_stop > max_DAC || dac_stop < min_DAC))
    {
      cm_msg(MERROR,"DAC_set_scan_params",
	     "DAC stop value (%.2fV) is out of range (allowed range is %.2fV to %.2fV)",
	     dac_stop,min_DAC,max_DAC);
      return FE_ERR_HW;
    }
  if( (ps.input.dac_start > max_DAC || ps.input.dac_start < min_DAC))
    {
      cm_msg(MERROR,"DAC_set_scan_params",
	     "DAC start value (%.2fV) is out of range (allowed range is %.2fV to %.2fV)",
	     ps.input.dac_start,min_DAC,max_DAC);
      return FE_ERR_HW;
    }
    
  if ( fabs(ps.input.dac_inc) > fabs(dac_stop - ps.input.dac_start)) 
    {
      cm_msg(MERROR,"DAC_set_scan_params","Invalid DAC scan increment value: %.2f",
	     ps.input.dac_inc);
      return FE_ERR_HW;
    }
  
  status = db_set_value(hDB, hOut, "DAC Stop", &dac_stop, sizeof(dac_stop), 1, TID_FLOAT); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"DAC_set_scan_params","cannot write output parameter  \"DAC stop\" (%d)",status); 
      return status;
    }


  
  dac_val = ps.input.dac_start;
  if(debug)  
    printf("DAC_set_scan_params: DAC device will be set to =%.2f Volts\n",dac_val);
  printf(" DAC_set_scan_params: selected %d DAC scan increments starting with %.3f Volts by steps of %.3f Volts\n",
	  dac_ninc, ps.input.dac_start, ps.input.dac_inc);


  prev_dac_val = dac_val - ps.input.dac_inc;   // initialize previous set value
  prev_dac_read = prev_dac_val;                // and read value

  return SUCCESS;
}

#ifdef HAVE_PPG
INT set_ppg_alarm(BOOL active)
{
  BOOL activ = active;
  INT size, status;

  size=sizeof(activ);
  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &activ, size, 1, TID_BOOL);
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"set_ppg_alarm","cannot set /Alarms/Alarms/PPG/active to %d  (%d)",activ, status); 
  
  al_reset_alarm("PPG"); // reset alarm
  return status;
}
#endif

INT set_dac_sleep_time(void)
{ 
  INT status,size;
  char str[80];
  double dst,dsp;
  

  // Dac sleep time is used to delay sending of the TOF start
  // TOF start pulse is the Dac Service pulse
  

  dst = (double)ps.input.dac_sleep_time__ms_;  // global read from ODB
  dsp = (double)ps.input.tof_pulse_width__ms_;  // global read from ODB
  
  //printf("set_dac_sleep_time: starting with DAC sleep time = %f ms\n",ps.input.dac_sleep_time__ms_);
  if(ps.input.run_tri_config)
    {
      printf("set_dac_sleep_time: Setting DAC Sleep time to %f  \n",dst);
      sprintf(str,"pulse_dacsv/time offset (ms)");
      
      size = sizeof(dst);	  
      status = db_set_value(hDB, hPPG, str, &dst, size, 1, TID_DOUBLE);  
      if(status != DB_SUCCESS) 
	cm_msg(MERROR,"set_dac_sleep_time","cannot set ODB key \"%s\" to %f  (%d)",str,dst, status); 
      return status;   


      printf("set_dac_sleep_time: Setting TOF pulse width to %f  \n",dsp);
      sprintf(str,"pulse_dacsv/pulse width (ms)");
      
      size = sizeof(dsp);	  
      status = db_set_value(hDB, hPPG, str, &dsp, size, 1, TID_DOUBLE);  
      if(status != DB_SUCCESS) 
	cm_msg(MERROR,"set_dac_sleep_time","cannot set ODB key \"%s\" to %f  (%d)",str,dst, status); 
      return status;   


      // Also set duplicate pulse (pulse 7) for testing

      printf("set_dac_sleep_time: Also setting Pulse 7 (duplicate DAC Sleep time for testing) to %f  \n",dst);
      sprintf(str,"pulse_ch7/time offset (ms)");
      size = sizeof(dst);	 

      status = db_set_value(hDB, hPPG, str, &dst, size, 1, TID_DOUBLE);  
      if(status != DB_SUCCESS) 
	cm_msg(MERROR,"set_dac_sleep_time","cannot set ODB key \"%s\" to %f  (%d)",str,dst, status); 
      return status; 


      printf("set_dac_sleep_time: Setting PULSE 7 pulse width to %f  \n",dsp);
      sprintf(str,"pulse_ch7/pulse width (ms)");
      
      size = sizeof(dsp);	  
      status = db_set_value(hDB, hPPG, str, &dsp, size, 1, TID_DOUBLE);  
      if(status != DB_SUCCESS) 
	cm_msg(MERROR,"set_dac_sleep_time","cannot set ODB key \"%s\" to %f  (%d)",str,dst, status); 
      return status;   

    } // tri_config
  
#ifndef NEW_PPG
  else
    {
      cm_msg(MINFO,"set_dac_sleep_time","Cannot change parameter unless tri_config is run");
      return DB_INVALID_PARAM;
    }
#endif

    
#ifdef NEW_PPG
  if( !ps.input.run_tri_config)
    {
      dac_sleep_count  = (DWORD)((dst/ps.output.time_slice__ms_)+0.5); // set delay before daq service pulse to dac_sleep_count
      if(dac_sleep_count > 3)
	dac_sleep_count -=3; // minimum 3 clock cycles included for newppg
      else
	dac_sleep_count = 0;

      printf("set_dac_sleep_time:  dac_sleep_time= %f  dac_sleep_count=%u \n",ps.input.dac_sleep_time__ms_,dac_sleep_count); 


      tof_width_count = (DWORD) ((dsp/ps.output.time_slice__ms_)+0.5); // set daq service pulse width (i.e. TOF pw) to tof_pw_count
      if(tof_width_count > 3)
	tof_width_count -=3; // minimum 3 clock cycles included for newppg
      else
	tof_width_count = 0;
      printf("set_dac_sleep_time:  tof pulse width = %f ms  tof_width_count=%u \n",ps.input.tof_pulse_width__ms_,tof_width_count); 
    }    
#endif
  return SUCCESS;

}

 


void  setup_hotlinks(void)
{
  INT status;
  char str[80];
  
  if (hfe==0)
    {
      sprintf(str,"input/hot debug");
    
      status = db_find_key(hDB, hTASet, str, &hfe);
      if (status == DB_SUCCESS)
	{
        
	  status = db_open_record(hDB, hfe, &lhot_debug
				  , sizeof(lhot_debug)
				  , MODE_READ, call_back_debug, NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }


  if (hTimer==0)
    {
      sprintf(str,"input/hot timer");
    
      status = db_find_key(hDB, hTASet, str, &hTimer);
      if (status == DB_SUCCESS)
	{
        
	  status = db_open_record(hDB, hTimer, &lhot_timer
				  , sizeof(lhot_timer)
				  , MODE_READ, call_back_timer, NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }





  if (hStop==0)
    {
      sprintf(str,"input/stop at end-of-sweep");
      status = db_find_key(hDB,  hTASet, str, &hStop);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hStop, &hot_stop
				  , sizeof(hot_stop)
				  , MODE_READ, stop_call_back, NULL);
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }


  return;

}
/*-- Call_back  ----------------------------------------------------*/
void call_back_debug(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
  //printf("lhot_debug is %d\n",lhot_debug); 
  if (hot_debug)
    hot_debug = 0;
  else
      hot_debug = 1;
  printf("Hot debug has been toggled; hot_debug is now %d\n",hot_debug); 
}


/*-- Call_back  ----------------------------------------------------*/
void call_back_timer(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
  //printf("lhot_timer is %d\n",lhot_timer); 
  if (hot_timer)
    hot_timer = 0;
  else
      hot_timer = 1;
  printf("Hot timer has been toggled; hot_timer is now %d\n",hot_timer); 
}



/*-- Call_back  ----------------------------------------------------*/
void stop_call_back(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,(char *)info);
  if(hot_stop)
    printf("\nstop_call_back: hot_stop=%d Request received to stop run at end of sweep (scan) \n",hot_stop);
  else
    {
      //if(debug)
	printf("\nstop_call_back: hot_stop=%d  cleared \n",hot_stop);
    }
  return;
}



INT stop_run(void)
{
  INT status;
  INT debug_flag=0;
  char str[128];


  if(stop_run_flag)
    { // not actually deferred stop as that is not working
      printf("stop_run: stop_run_flag is already set. Stop already in progress\n");
      return SUCCESS;
    }

  // Prevent the PPG from restarting with an external trigger

  if(ps.input.ppg_external_start)
    { // real mode
      printf("stop_run: disabling PPG  external trigger\n");
#ifdef NEW_PPG
      TPPGDisableExtTrig(gVme,   gPpgBase); // Disable external trigger to prevent PPG restarting
#else
      VPPGDisableExtTrig(gVme,   gPpgBase); // Disable external trigger to prevent PPG restarting
#endif
    }

  stop_run_flag = TRUE; // set a flag
  status = cm_transition(TR_STOP, 0, str, sizeof(str), TR_ASYNC, debug_flag);
 	  
  if (status == CM_DEFERRED_TRANSITION)
      printf("Deferred transition:  %s\n", str);
  else if (status == CM_TRANSITION_IN_PROGRESS)
    cm_msg(MINFO,"stop_run",
      "Deferred stop already in progress, in an xterm, enter odbedit command \"stop now\" to force stop");

  else if(status==SUCCESS)
    printf("stop_run:  after cm_transition, status=SUCCESS\n");
  
  else 
    {
      printf("stop_run:   Error: %s\n", str);      
      cm_msg(MERROR,"stop_run","Automatic stop has failed. Please stop the run now"); 
    }


  return status;
}

BOOL deferred_stop(INT run_number, BOOL first) 
{   // NOT USED 
  INT i; 
 

  if(!stop_run_flag)   // set flag in case stop is triggered by user rather than pgm.
    stop_run_flag=TRUE; // user wants to stop the run; prevent next supercycle from starting

  printf("\n***  deferred_stop: stop_run_flag = %d   *** \n",stop_run_flag);
 
  // defer stop until all equipments have run, i.e. decode and write the last data into banks

  for (i=0; i<N_WAITEQP; i++)
    {
      if(waiteqp[i] != 0)
	{
	  printf("deferred_stop: waiting for equipment %s to finish\n", names[i]);
	  return FALSE;
	}
    }
 
  printf("\n***    deferred_stop: All equipments have run...stopping run now   *** \n");
  return TRUE;
}


INT incr_DAC(void)
{
  INT status;
  float set_diff;
  float read_diff;
  float fdiff,diff;

  // increment DAC
  //printf("incr_DAC: gbl_DAC_n = %d  dac_ninc= %d\n",gbl_DAC_n, dac_ninc);

  if(dac_mon_flag)
    {
      set_DAC(dac_monitor_chan, 2.5); // set dac monitor output
      printf("Set DAC monitor channel to 2.5V\n");
    }

  if( gbl_DAC_n ==  ( dac_ninc + 1) ) 
    { /* Finished SWEEP;   
	 N E W  S W E E P  FOR DAC */
      gbl_DAC_n = 0;
      gbl_SCAN_n ++;
      // check whether we have reached num_scans (0 for free-running)
      if (num_scans > 0 && (gbl_SCAN_n > num_scans))
	{
	  cm_msg(MINFO,"incr_DAC","Requested number of scans reached (%d). Attempting to stop the run",num_scans);
	  printf("incr_DAC: stopping the run...(number of scans reached)\n");
          status = stop_run();
	  return status;
	}

      if(hot_stop)
	{
	  printf("\n");
	  cm_msg(MINFO,"incr_DAC","Request to stop at end-of-sweep is set (hot-link). The run will be stopped at the end of sweep %d",num_scans);
          status = stop_run();
	  return status;
	}

      if(hot_debug) 
	printf("\n incr_DAC: ***  N E W  S W E E P  ...  sweep %d is  starting  \n",gbl_SCAN_n);
      else
	printf("\r incr_DAC: ***  N E W  S W E E P  ...  sweep %d is  starting  ",gbl_SCAN_n);
    }

  
  dac_val =  ps.input.dac_start + (gbl_DAC_n  *  ps.input.dac_inc);

  
  set_DAC(dac_output_chan, dac_val); // set DAC value
  dac_read= read_DAC(); // readback DAC set value

  if(!ps.input.galil_rio.disable_step_check)
    {

      set_diff = fabs(dac_val - prev_dac_val);
      read_diff= fabs(dac_read - prev_dac_read);
      fdiff = fabs (set_diff - read_diff);

      // Check that DAC has changed
      
      if ( fdiff <=  fabs (ps.input.galil_rio.dac_step_check.jitter__v_) )
	dac_step_cntr[0]++; // good step counter

      else if (fdiff <  fabs (ps.input.galil_rio.dac_step_check.minimum_step__v_) )
	dac_step_cntr[1]++;
    
	  
      else if  (gbl_DAC_n == 0) // new sweep
	{
	  if (fabs (dac_val - dac_read) >  (fabs (ps.input.galil_rio.dac_step_check.jitter__v_) * 5))
	    {
	      dac_step_cntr[2]++;
	      goto cont;
	    }
	  goto err;
	}
      else
	{
	  goto err;
	}
    } //  check on dac stepping
 cont:
  if(hot_debug)
    printf("incr_DAC: called set_DAC with value %.3f volts ; read back %.3f volts \n",dac_val, dac_read);
  
  gbl_DAC_n++;
  return SUCCESS;


 err:
  cm_msg(MERROR, "incr_DAC", "Problem setting DAC value %f (read back %f). DAC device (Galil RIO 47120) does not seem to be working.",dac_val,dac_read);
  if(gbl_DAC_n == 0)
    {
      diff= (fabs (dac_val - dac_read)) ;
      printf("dac_incr: fails because fabs difference  between set and read back values  %f > 5 * dac_jitter %f  \n", diff,(5* ps.input.galil_rio.dac_step_check.jitter__v_));
    }
  else 
    printf("dac_incr: fails because fabs difference between  read step value (%f) and write step value (%f) (i.e. %f) > jitter (%f) and > minimum step (%f)\n",
	   read_diff,set_diff,fdiff, fabs (ps.input.galil_rio.dac_step_check.jitter__v_),  fabs (ps.input.galil_rio.dac_step_check.minimum_step__v_));
  
  status=stop_run();
  return FE_ERR_HW;
}

/*-- Clear scaler i ------------------------------------------------------*/
void scaler_clear(INT i)
/*------------------------------------------------------------------------*/
{
  /* scaler_clear  */
  DWORD j;
  
  for (j=0;j<scaler[i].nbins;j++)
    scaler[i].ps[j]=0;
  scaler[i].sum_cycle = 0.0;
}


void clear_mcs_counters(void)
{
  int i;
  if(debug)printf("clear_mcs_counters: clearing cycle scalers\n");
  for (i=0; i<NumSisChannels; i++) // use actual max number of SIS channels
    {
      scaler_clear(i); // clear cycle histo
      if(debug)printf("clearing scaler[%d]\n",i);
    }
  // remember gSumMcsEvents for DBUG bank
  last_gSumMcsEvents= (INT)gSumMcsEvents;
  gSumMcsEvents=0;
 
}
/*-- Clear histo h ------------------------------------------------------*/
void histo_clear(INT h)
/*------------------------------------------------------------------------*/
{
  DWORD j;
  /* clear histogram h */
  for (j=0;j<histo[h].nbins;j++)
    histo[h].ph[j]=0;
  histo[h].sum =0.0;
}

#ifdef GONE
/*-- Clear histo h ------------------------------------------------------*/
void dacsum_clear(INT h)
/*------------------------------------------------------------------------*/
{
  DWORD j;
  /* clear  dacsum increment h */
  printf("dacsum_clear: clearing dac sums\n");
  for (j=0;j<dacsum[h].ninc;j++)
    dacsum[h].ptr[j]=0.0;
}
#endif
  

INT get_pol_settings(void)
{
  INT status,size;
  char set_str[128];

  /* Book Setting space */ 
  POL_ACQ_SETTINGS_STR(pol_acq_settings_str); 
 
  /* Map /equipment/pol_acq/settings for the sequencer */ 
  sprintf(set_str, "/Equipment/POL_ACQ/Settings"); 
  status = db_create_record(hDB, 0, set_str, strcomb(pol_acq_settings_str)); 
  status = db_find_key (hDB, 0, set_str, &hTASet); 
  if (status != DB_SUCCESS) 
    cm_msg(MINFO,"get_pol_settings","Key %s not found", set_str); 
  
  /* check that the record size is as expected */
  status = db_get_record_size(hDB, hTASet, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_pol_settings", "error during get_record_size (%d)",status);
      return status;
    }
  
  printf("Size of sis saved structure: %d, size of sis record: %d\n", sizeof(POL_ACQ_SETTINGS) ,size);
  if (sizeof(POL_ACQ_SETTINGS) != size)
    {
      /* create record */
      cm_msg(MINFO,"get_pol_settings",
	     "creating record for %s; mismatch between size of structure (%d) & record size (%d)", 
	     set_str, sizeof(POL_ACQ_SETTINGS) ,size);
      status = db_create_record(hDB, 0, set_str, strcomb(pol_acq_settings_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_pol_settings","Could not create %s record (%d)",set_str,status);
	  return status;
	}
      else
	printf("\nget_pol_settings: Success from create record for %s\n",set_str);
    }

  /* Get current  settings */
  size = sizeof(ps);
  status = db_get_record(hDB, hTASet, &ps, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_pol_settings", "cannot retrieve %s record (size of ps=%d)", set_str,size);
      return DB_NO_ACCESS;
    }

  // printf("thr2 = %f\n", ps.cycle_thresholds.threshold2);
  printf("get_pol_settings:  PPGpath = %s \n", ps.input.ppg_path); //  e.g. /home/pol/online/pol/ppg  

#ifdef HAVE_PPG
#ifdef NEW_PPG
  sprintf(ppgfile,"%s/ppgload/ppgload.dat", ps.input.ppg_path);
#else 
  sprintf(ppgfile,"%s/ppgload/bytecode.dat", ps.input.ppg_path); 
#endif
  printf("get_pol_settings:  ppgfile:%s\n",ppgfile); 
#endif
 


  /* find key for output */
  sprintf(set_str,"output"); 
  status = db_find_key (hDB, hTASet, set_str, &hOut); 
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_pol_settings","cannot get ODB key \"/Equipment/pol_acq/settings/output\"  (%d)",status); 
      return status;
    }
  //printf("got the key hOut\n");


  /* find key for ppg cycle
     "ppg cycle" is fixed for POL experiment
  */
  hPPG = 0;
  sprintf(set_str,"/equipment/POL_ACQ/ppg cycle"); 
  status = db_find_key (hDB, 0, set_str, &hPPG); 
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_pol_settings","cannot get ODB key at %s (%d)",set_str,status); 
      return status;
    }
  

  printf("PPGpath:%s  Exp name = %s \n", ps.input.ppg_path, exp_name); 
  sprintf(cmd,"%s/tri_config  -e %s -s -d", ps.input.ppg_path,exp_name); 
  //printf("tri_config command is :\"%s\"\n",cmd); 
 
  // initialize hot_debug in ODB to whatever is set in program (usually FALSE)
  status = db_set_value(hDB, hTASet, "input/hot debug", &hot_debug, sizeof(BOOL), 1, TID_BOOL);  
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_pol_settings","cannot set ODB key \"input/hot debug\"  (%d)",status); 
      return status;
    }
  else
    printf("get_pol_settings: hot_debug initialized to %d\n",hot_debug);


#ifdef HAVE_GALILRIO
  printf("get_pol_settings: Num Galil dac channels enabled : %d  Num adc channels enabled = %d \n", 
	 ps.input.galil_rio.dac_params.num_channels_used,ps.input.galil_rio.adc_params.num_channels_used ); 
#endif

  return SUCCESS;
}


INT set_watchdog()
{
  INT status;
  // counter in ../output indicated frontend is still alive
  watchrabbit=0; // reset
  watchdog++;
  if(watchdog >= 100)
    watchdog = 0;

  //printf("set_watchdog: setting watchdog to %d\n",watchdog);
  status = db_set_value(hDB, hOut, "watchdog", &watchdog, sizeof(INT), 1, TID_INT);  
  if(status !=SUCCESS)
    cm_msg(MERROR,"set_watchdog","cannot set ODB key ..output/watchdog to %d (%d)",watchdog,status); 

  return status;
}

INT read_dummy(char *pevent, INT off)
{
  return 0;
}





INT read_mcs_done(char *pevent, INT off)
{

  //  printf("read_mcs_done: starting with gbl_IN_CYCLE=%d\n",gbl_IN_CYCLE);

  if (!gbl_IN_CYCLE)
    return 0;


  lacq_count = acq_count; // remember previous count
  // Check whether we have reached num_bins ( num_bins = LNE_preset)
  acq_count = sis3820_RegisterRead(gVme,gMcsBase, SIS3820_ACQUISITION_COUNT); // global
  
  //  if(hot_debug)
  //  printf("read_mcs_done:  acq_count, prevcount, num_bins, LNE_total  = %d,%d,%d,%d\n",
  //	   acq_count,lacq_count,(int)num_bins,(int)LNE_total);
  
 


      if(acq_count <= 0 ||  acq_count < (unsigned int) LNE_total)
	return 0;  // wait for end of supercycle
    

  
  

  // At end of PPG supercycle
  data_flushed = FALSE;
  flush=TRUE; // flush the last data from SIS
  
  // Stop the PPG from restarting using real fp input
  disable_trigger();
  gbl_IN_CYCLE = FALSE; // not running in cycle
  waiteqp[HISTO]= waiteqp[CYCLE]=TRUE;  // histo and cycle decoding
  
  if(hot_timer)
    gettimeofday(&eoct,NULL);  // Time length of readout
  
  return 0;  
}


INT send_hist_event(char *pevent, INT off)
{
  // Event ID 5 ; NOT sent to odb
  /*
      checks if data has been flushed
      calls decode_scaler_data which calls process_histo
      makes a copy of relevent data for info event
      causes DAC to be incremented
      restarts next cycle
      sends out histograms
  */
  DWORD *pdata;
  float *pfloat;
  double *pdouble;
  int i,j;
  DWORD nbins;
  INT status;
  INT index;
  
  uint32_t count; // readback of number of LNE received
  uint32_t preset; // readback preset reg
  
  
  struct timeval t1,t2; // time  read_mcs_event
  double td1=0;
  double td2=0;
 
  struct timeval now; // time rio
  double td=0;
  

  if(!waiteqp[HISTO])
    return 0;

  if(!data_flushed)
    return 0;

 

 if(hot_timer)  // time how long this routine takes
    {
      gettimeofday(&t1,NULL);
      // compute elapsed time in millisec
      td1 = (t1.tv_sec - eoct.tv_sec) * 1000.0;      // sec to ms
      td1 += (t1.tv_usec - eoct.tv_usec) / 1000.0;   // us to ms
    
      printf("send_hist_event: time since supercycle ended  %f ms \n",td1);
    }

  
  if(hot_debug)
    printf("\nsend_hist_event: end-of-cycle acq_count=%d; cycle %d supercycle %d  \n",
	   (int)acq_count,gbl_CYCLE_n,gbl_SCYCLE_n);
  

 

#ifdef HAVE_SIS3820

  int haveData = sis3820_DataReady(gVme,gMcsBase);

   if(debug)printf("send_hist_event: sis3820_DataReady returns haveData=%d  gbl_IN_CYCLE=%d, waiteqp[CYCLE]= %d  \n",
		      haveData,gbl_IN_CYCLE,waiteqp[CYCLE]);
 #ifdef GONE
  const int kSisBufferSize = 16000000;
  if (haveData > kSisBufferSize)
    if (!gSisBufferOverflow)
      {
        gSisBufferOverflow = 1;
        printf("send_hist_event: data (%d) larger than buffer (%d)\n",haveData,kSisBufferSize);
        al_trigger_alarm("sis_overflow", "SIS buffer overflow: LNE rate is too high",
                         "Warning", "", AT_INTERNAL);
      }
#endif // GONE

 if(hot_timer)
   gettimeofday(&t2,NULL);

  status = decode_scaler_data();  // decodes raw scaler data and fills histograms for SC... calls process_histo
  if(status != SUCCESS)
    return 0; // error


  if(hot_timer)  // time how long this routine takes
    {
      gettimeofday(&t1,NULL);
      // compute elapsed time in millisec
      td1 = (t1.tv_sec - t2.tv_sec) * 1000.0;      // sec to ms
      td1 += (t1.tv_usec - t2.tv_usec) / 1000.0;   // us to ms
    
      // printf("send_hist_event: time to decode data  %f ms \n",td1);
    }


  // Store data for read_info_event (data sent to ODB)
 
  count = sis3820_RegisterRead(gVme,gMcsBase, SIS3820_ACQUISITION_COUNT);
  preset = sis3820_RegisterRead(gVme,gMcsBase , SIS3820_ACQUISITION_PRESET); // debug 



   // if add another word then increment gsdd_len;
   store_dbg_data[0] = haveData;         // amount of data still buffered in the SIS
   store_dbg_data[1] = (float)last_gSumMcsEvents;  // number of LNE events from the SIS (before being cleared)
   store_dbg_data[2]  = count;  // LNE count from reg SIS380_ACQUISITION_COUNT
   store_dbg_data[3] = preset;  // LNE_preset value from register
   store_dbg_data[4] = num_bins;  // num_bins; actual number of bins sent out (may be 1 more than requested number)
   store_dbg_data[5] = (float)data_bytes;  // 2 for 16-bit data
   store_dbg_data[6] = (float)NumSisChannels; // number of Scaler Channels enabled 
   store_dbg_data[7] = (float)ps.input.discard_first_bin;
   store_dbg_data[8] = (float)ps.input.discard_first_cycle;

 
  // keep a copy to send in INFO event ID 3 CYCL bank
  // if add another word then increment gcbd_len;
  gbl_cycle_bank_data[0] = 1; // code to indicate scan type  
  gbl_cycle_bank_data[1] = gbl_CYCLE_n; // Cycle number  i.e. number of good cycles
  gbl_cycle_bank_data[2] = gbl_SCYCLE_n; // Supercycle number
  gbl_cycle_bank_data[3] = gbl_cycle_cntr; // counts cycles within supercycle (max=num cycles per SC)
  gbl_cycle_bank_data[4] = gbl_SCAN_n; // Scan number (sweep number)
  gbl_cycle_bank_data[5] = gbl_skip_cycle_cntr; // Number of cycles skipped  
  gbl_cycle_bank_data[6] = gbl_his_cntr; // Number of cycles histogrammed (should be same as gbl_CYCLE_n )
  gbl_cycle_bank_data[7] = gbl_DAC_n-1; // DAC increment number (0 to ninc) -  rather than 1 to ninc+1
  gbl_cycle_bank_data[8] = dac_val; // DAC set value 
  gbl_cycle_bank_data[9] = dac_read; // DAC readback value 
  index=10; // array index 


  // Send out histogram data (this data NOT sent to ODB)

  if(hot_timer)  // time how long sending histograms takes
      gettimeofday(&wcyc,NULL);
 

  if(debug)
    printf("send_hist_event: init banks with pevent=%p\n",pevent);

  /* init bank structure */
  bk_init32(pevent);

 // Create CYCL bank 
  bk_create(pevent, "CYCL", TID_FLOAT, (void **)&pfloat);
  for(i=0; i<(index-1); i++)  // don't want dac_read
    *pfloat++ = gbl_cycle_bank_data[i];
 

 // Read ADC data (not sent to INFO CYCL bank)
 #ifdef HAVE_GALILRIO
  for (i=0; i< num_adc_chan; i++)  // read from ODB, currently 4 (4 ch are averaged)
    {
      j= ps.input.galil_rio.adc_params.adc_channels[i]; // channel number of ADC channel
      *pfloat++ =  rio->GetAnalogInput(j);
    }
 #endif

  // Now the averages

  if(hot_timer)
    {
      // time how long the RIO was averaging
      gettimeofday(&now,NULL);
      // compute elapsed time in millisec
      td = (now.tv_sec - riot.tv_sec) * 1000.0;      // sec to ms
      td += (now.tv_usec - riot.tv_usec) / 1000.0;   // us to ms
      printf("\nsend_hist_event: %f ms since DAC last incremented \n",td);
    }
  get_rio_averages(); // fills rio_average
  //printf("send_hist_event: SC Cntr: %d  dac set val: %f  dac_read: %f  dac_av: %f\n",gbl_SCYCLE_n,dac_val,dac_read,rio_average[0]);

  // currently the first 4 channels are averaged. 

  for(i=0; i<rio_max; i++)
    {
      j= ps.input.galil_rio.adc_params.adc_channels[i]; // channel number of ADC channel 
      gbl_cycle_bank_data[index+i]=rio_average[j];  // Averaged readback - keep a copy for INFO equipment
      *pfloat++ = rio_average[j];// Add rio averages to CYCL bank
    }
  //===========================================================================================

  //      Now increment DAC
   waiteqp[CYCLE] = FALSE;  // no longer waiting for CYCLE to be decoded
                            // frontend_loop() will increment DAC, then calls cycle_start() to start next SC

  //============================================================================================
   waiteqp[INFO]=TRUE; // info event can be sent out (to ODB)

   // Add rio averages to CYCL bank  - now done above
   //for (i=0; i<rio_max; i++)  
   // *pfloat++ = rio_average[i];
 bk_close(pevent, pfloat);  // close CYCL bank

 gbl_cycle_bank_data[14]=0; // clear spare(s)

   
 //printf("send_hist_event: CYCL bank DAC set: %f DAC read: %f Average: %f\n", gbl_cycle_bank_data[8],gbl_cycle_bank_data[9],gbl_cycle_bank_data[10]);


 //  gbl_histo_bank_data[3] now contains user readback adc_chan_b  average value
 gbl_histo_bank_data[3]= rio_average[adc_chan_b];

  bk_create(pevent, "HISI", TID_FLOAT, (void **)&pfloat);  // information
  for (i=0; i<ghbd_len; i++)  //gbl_histo_bank_data[ghbd_len]
    *pfloat++ = gbl_histo_bank_data[i];
  bk_close(pevent, pfloat);


  char str[5];
  for (j=0; j<NumSisChannels; j++)
    {
      sprintf(str,"HIS%d",j);
      //printf("creating bank %s \n",str);
      bk_create(pevent, str, TID_DWORD, (void **)&pdata);  // Histograms for 1 supercycle
      nbins = histo[j].nbins;
      if( ps.input.discard_first_bin)
	nbins--;

      memcpy(pdata, histo[j].ph, nbins * sizeof(DWORD));
      pdata +=  nbins;
      bk_close(pevent, pdata);
    }

  // Histo Sums
  bk_create(pevent,"HSUM",TID_DOUBLE,(void **)&pdouble);
  if (debug)printf("Histo sums per channel : ");
  for (i=0; i<NumSisChannels; i++)
    {
      if(debug) printf(" %f ",histo[i].sum);
      *pdouble++ = histo[i].sum;	
      HSums[i]=histo[i].sum; // copy this for info event
      //printf("send_hist_event: HSums[%d]=%f\n", i,HSums[i]);
    }
  if(debug)printf("\n");
  bk_close(pevent, pdouble); 
  
  
  // histo sums have also been sent
  for (i=0; i<N_HISTO_MAX; i++)
    histo_clear(i); // clear the histogram and the sums now they have been sent
  
  gbl_histo_bank_data[5]=0.0; //clear counter


  if(hot_debug  ||  stop_run_flag)
    printf("send_hist_event: sending histo event of size %d; histograms now cleared\n",
	   bk_size(pevent));


  if(hot_timer)
    {
      gettimeofday(&t2,NULL);
      
      // compute elapsed time in millisec
      td2 = (t2.tv_sec - wcyc.tv_sec) * 1000.0;      // sec to ms
      td2 += (t2.tv_usec - wcyc.tv_usec) / 1000.0;   // us to ms

      printf("send_hist_event: time to decode data  %f ms;  time to send histos out %f ms \n",td1,td2);
     
      // compute elapsed time in millisec
      td1 = (t2.tv_sec - eoct.tv_sec) * 1000.0;      // sec to ms
      td1 += (t2.tv_usec - eoct.tv_usec) / 1000.0;   // us to ms
       printf("send_hist_event: total time since end of SuperCycle detected : %f\n",td1); 

    }
 
  //printf("histo_read: returning with event size %d\n",bk_size(pevent));
  waiteqp[HISTO]=FALSE; // histos are sent
  return bk_size(pevent);   



#else
  return 0; // prevent creation of this event
#endif
}



INT read_info_event(char *pevent, INT off)
{
  
  // Event ID 3 ; sent to odb
  float *pfloat;
  double *pdouble;
  int i;


  // Also checks for stuck scaler occasionally to save having another event
  static int count=0;

 
  if (gbl_IN_CYCLE)
    {
      count++;
      if(count == 10)
	{  // don't check too often
	  check_stuck_mcs();
	  count=0;
	}
    }

  if(!waiteqp[INFO])
    return 0;

  //printf("read_info_event: starting\n");

  /* init bank structure  */
  bk_init32(pevent);
  
  bk_create(pevent, "DBUG", TID_FLOAT, (void **)&pfloat);
  for(i=0; i< gsdd_len; i++)
    *pfloat++ = store_dbg_data[i];
  bk_close(pevent, pfloat);
  


  
  bk_create(pevent, "CYCL", TID_FLOAT, (void **)&pfloat);
  for(i=0; i< gcbd_len; i++)
    *pfloat++ = gbl_cycle_bank_data[i]; 
  bk_close(pevent, pfloat);
  
  // Send a copy of Histo Sums so it can go to ODB for status display 
  bk_create(pevent,"SUMS",TID_DOUBLE,(void **)&pdouble);
  for (i=0; i<NumSisChannels; i++)
    *pdouble++ =  HSums[i];
  bk_close(pevent, pdouble); 
  
  if(debug)printf("read_info_event: sent INFO event of size %d\n",bk_size(pevent));
  waiteqp[INFO]=FALSE;
  
  return bk_size(pevent);
    
}


void check_stuck_mcs(void)
{
  /* called from read_info_event
 
   checks for stuck PPG (external trigger) or restart PPG (internal trigger) occasionally
   MCS has not received enough LNE
  */

  static int stuck_cntr=0;
  static BOOL wrote_stuck_msg=0;
  static int stuck_at_cycle=0;

  if ( acq_count < (unsigned int) LNE_total)
    {      
      if( stuck_at_cycle !=  gbl_CYCLE_n )
	{ // not stuck
	  // printf(" different cycles; last stuck at cycle %d and cycle is now %d\n",stuck_at_cycle, gbl_CYCLE_n);
	  stuck_cntr=0;
	  if(wrote_stuck_msg)
	    {
	      cm_msg(MINFO,"read_info_event"," PPG no longer stuck at Cycle %d SC %d (acq_count=%d lacq_count=%d  LNE_total= %d)",
		     gbl_CYCLE_n,gbl_SCYCLE_n, acq_count, lacq_count, (int)LNE_total);
	      wrote_stuck_msg=0;
	    }
	  return; // not stuck, nothing to do
	}


      if(acq_count == lacq_count)
	{ // PPG may be waiting for trigger... scaler may be stuck waiting for LNE  (depending on dwell time)
	  // or it may be paused after each cycle waiting for TOF
	  if (acq_count % ps.output.num_bins_per_cycle != 0)  // modulo  remainder will be zero after each completed cycle	 
	    {
	      stuck_at_cycle = gbl_CYCLE_n; // remember the cycle
	      stuck_cntr++;
	      //printf("stuck_cntr=%d  gbl_CYCLE_n=%d\n",stuck_cntr,gbl_CYCLE_n);
	      if(stuck_cntr > 5)
		{
		  if(ps.input.ppg_external_start)
		    {      // external start
		      if(!wrote_stuck_msg)
			{
			  cm_msg(MERROR,"read_info_event"," PPG may be stuck waiting for external trigger at Cycle %d SC %d (acq_count=%d lacq_count=%d  LNE_total= %d)",
				 gbl_CYCLE_n,gbl_SCYCLE_n, acq_count, lacq_count, (int)LNE_total);
			  
			  wrote_stuck_msg=1;
			}
			  
		    } // end of external start
		  else
		    { // internal start
		      check_ppg_running(); // updates global ppg_running
		      if(!ppg_running) // PPG is stopped
			{  // should not get this; indicates PPG and Scaler are out of sync somehow
			  cm_msg(MERROR, "read_info_event","PPG stopped with acq_count=lacq_count=%d. Expect ppg to be running for all cycles within SuperCycle",acq_count);
			  stop_run();
			}
		      
		    } // end of internal start

		} // stuck_cntr > 5
	    } // modulo
	} // end of PPG may be stuck	  
    }
  return;
}
//end
  
