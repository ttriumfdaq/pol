/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Thu Apr 21 12:41:29 2016

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      number_of_time_bins[128];
  char      bin_width__ms_[128];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"number of time bins = STRING : [128] Number of bins (i.e. number of LNE pulses) sent by PPG in REAL mode",\
"bin width (ms) = STRING : [128] Width in ms of each time bin (REAL mode)",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      write_data;
  BOOL      send_raw_data_bank;
  BOOL      pedestals_run;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"Write data = LINK : [19] /logger/Write data",\
"Send raw data bank = LINK : [53] /Equipment/POL_ACQ/Settings/input/send raw data bank",\
"Pedestals run = BOOL : n",\
"",\
NULL }

#ifndef EXCL_POL_ACQ

#define POL_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    char      experiment_name[32];
    char      ppg_path[50];
    float     dac_sleep_time__ms_;
    float     standard_pulse_width__ms_;
    float     awg_clock_width__ms_;
    float     tdcblock_width__ms_;
    float     ppg_clock__mhz_;
    BOOL      ppg_external_clock;
    BOOL      ppg_external_start;
    DWORD     polarity;
    INT       number_of_scans;
    float     dac_start;
    float     dac_inc;
    INT       num_dac_incr;
    float     dac_value;
    double    dwell_time__ms_;
    BOOL      new_ppg;
    BOOL      hot_debug;
    INT       num_cycles_per_supercycle;
    BOOL      run_tri_config;
    INT       scan_loop_count;
    struct {
      float     dwell_time;
      char      dwell_time_units[8];
    } scratch;
    DWORD     num_bins_requested;
    BOOL      discard_first_bin;
    BOOL      stop_at_end_of_sweep;
    BOOL      discard_first_cycle;
    float     tof_pulse_width__ms_;
    BOOL      hot_timer;
    BOOL      send_raw_data_bank;
    struct {
      INT       sis_mode;
      INT       data_format;
      DWORD     test_pulse_mask;
      DWORD     enable_channels_mask;
      DWORD     lne_prescale_factor;
      DWORD     lne_preset;
      DWORD     input_mode;
      DWORD     output_mode;
      INT       mcs_channel__1_32_;
      DWORD     module_id_and_rev;
    } sis3820;
    struct {
      BOOL      disable_step_check;
      struct {
        INT       num_channels_used;
        INT       dac_channels[8];
        char      dac_range_codes[128];
        INT       dac_range[8];
      } dac_params;
      struct {
        INT       num_channels_used;
        INT       adc_channels[8];
        char      adc_range_codes[128];
        INT       adc_range[8];
      } adc_params;
      struct {
        float     window[4];
        INT       filter[4];
      } adc_averaging;
      struct {
        float     jitter__v_;
        float     minimum_step__v_;
      } dac_step_check;
    } galil_rio;
  } input;
  struct {
    char      compiled_file_time[32];
    DWORD     compiled_file_time__binary_;
    float     time_slice__ms_;
    float     ppg_nominal_frequency__mhz_;
    float     minimal_delay__ms_;
    float     ppg_freq_conversion_factor;
    struct {
      INT       num_loops;
      double    start_loop[10];
      double    one_loop_duration[10];
      double    all_loops_end[10];
      char      loop_names[10][20];
      BOOL      ppg_running;
    } loops;
    BOOL      ppg_running;
    INT       scanloop_pc;
    float     dwell_time__ms_;
    INT       lne_frequency__hz_;
    float     one_cycle_time__ms_;
    float     one_cycle_time__sec_;
    struct {
      struct {
        char      parameter_names[10][32];
        DWORD     parameters[10];
      } _h_params;
    } newppg;
    float     dac_stop;
    INT       valid_bins_per_sc;
    INT       num_bins_per_cycle;
    INT       num_cycles_per_sc;
    INT       scan_loop_count;
    INT       watchdog;
    char      frontend_msg[256];
    BOOL      fe_flag;
    float     dac_max;
    float     dac_min;
  } output;
  struct {
    BOOL      reref_all;
    BOOL      reref1;
    BOOL      reref2;
    BOOL      reref3;
    BOOL      reref4;
    float     threshold1;
    float     threshold2;
    float     threshold3;
    float     threshold4;
    INT       num_cycles_to_skip_out_of_tol;
    INT       alarm;
    BOOL      enable_threshold_checking;
    INT       mode__0___1_level_;
  } cycle_thresholds;
} POL_ACQ_SETTINGS;

#define POL_ACQ_SETTINGS_STR(_name) const char *_name[] = {\
"[input]",\
"Experiment name = STRING : [32] pol",\
"PPG path = STRING : [50] /home/pol/online/pol/ppg/",\
"DAC sleep time (ms) = FLOAT : 1",\
"standard pulse width (ms) = FLOAT : 0.001",\
"AWG clock width (ms) = FLOAT : 0.001",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 100",\
"PPG external clock = BOOL : n",\
"PPG external start = BOOL : y",\
"Polarity = DWORD : 160",\
"Number of scans = INT : 0",\
"DAC Start = FLOAT : 0.43",\
"DAC Inc = FLOAT : 0.0001",\
"Num DAC incr = INT : 1",\
"DAC Value = FLOAT : 0",\
"dwell time (ms) = DOUBLE : 10",\
"New PPG = BOOL : y",\
"hot debug = BOOL : n",\
"num cycles per supercycle = INT : 4",\
"Run tri_config = BOOL : n",\
"scan loop count = INT : 1",\
"",\
"[input/scratch]",\
"dwell time = FLOAT : 10",\
"dwell time units = STRING : [8] ms",\
"",\
"[input]",\
"num bins requested = DWORD : 15",\
"discard first bin = BOOL : y",\
"stop at end-of-sweep = BOOL : n",\
"discard first cycle = BOOL : y",\
"TOF pulse width (ms) = FLOAT : 0.001",\
"hot timer = BOOL : n",\
"send raw data bank = BOOL : n",\
"",\
"[input/sis3820]",\
"SIS mode = INT : 2",\
"data format = INT : 16",\
"Test pulse mask = DWORD : 65536",\
"Enable Channels mask = DWORD : 4279238655",\
"LNE prescale factor = DWORD : 1",\
"LNE preset = DWORD : 80",\
"Input Mode = DWORD : 3",\
"Output Mode = DWORD : 2",\
"MCS channel (1-32) = INT : 18",\
"Module ID and Rev = DWORD : 941621514",\
"",\
"[input/galil_rio]",\
"disable step check = BOOL : y",\
"",\
"[input/galil_rio/dac params]",\
"num channels used = INT : 2",\
"dac channels = INT[8] :",\
"[0] 0",\
"[1] 1",\
"[2] 2",\
"[3] 3",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"dac range codes = STRING : [128] 1=0-5V 2=0-10V 3=+/-5V 4=+/-10V (default=4)",\
"dac range = INT[8] :",\
"[0] 4",\
"[1] 4",\
"[2] 4",\
"[3] 4",\
"[4] 4",\
"[5] 4",\
"[6] 4",\
"[7] 4",\
"",\
"[input/galil_rio/adc params]",\
"num channels used = INT : 4",\
"adc channels = INT[8] :",\
"[0] 0",\
"[1] 1",\
"[2] 2",\
"[3] 3",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"adc range codes = STRING : [128] 1=+/-5v 2=+/-10v 3=0-5v 4=0-10v (default=2)",\
"adc range = INT[8] :",\
"[0] 2",\
"[1] 1",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 2",\
"",\
"[input/galil_rio/adc averaging]",\
"window = FLOAT[4] :",\
"[0] 1000",\
"[1] 1000",\
"[2] 1000",\
"[3] 1000",\
"filter = INT[4] :",\
"[0] 10",\
"[1] 10",\
"[2] 10",\
"[3] 10",\
"",\
"[input/galil_rio/dac step check]",\
"jitter (V) = FLOAT : 20",\
"minimum step (V) = FLOAT : 0.001",\
"",\
"[output]",\
"compiled file time = STRING : [32] Tue May 13 15:21:10 2014",\
"compiled file time (binary) = DWORD : 1400019670",\
"Time Slice (ms) = FLOAT : 1e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 3e-05",\
"PPG freq conversion factor = FLOAT : 10",\
"",\
"[output/loops]",\
"num_loops = INT : 0",\
"start_loop = DOUBLE[10] :",\
"[0] 5e-05",\
"[1] 1.0001",\
"[2] 8.012749999998736",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"one_loop_duration = DOUBLE[10] :",\
"[0] 12.10169999999874",\
"[1] 11.1015",\
"[2] 10",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"all_loops_end = DOUBLE[10] :",\
"[0] 12101700.00004874",\
"[1] 12.10164999999874",\
"[2] 18.01279999999747",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"loop_names = STRING[10] :",\
"[20] SCAN",\
"[20] EXTR",\
"[20] SCLR",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"PPG running = BOOL : y",\
"",\
"[output]",\
"PPG running = BOOL : n",\
"scanloop pc = INT : 1",\
"dwell time (ms) = FLOAT : 10",\
"LNE frequency (Hz) = INT : 100",\
"one cycle time (ms) = FLOAT : 160",\
"one cycle time (sec) = FLOAT : 0.16",\
"",\
"[output/newppg/1h_params]",\
"parameter_names = STRING[10] :",\
"[32] number of parameters",\
"[32] scan loop counter",\
"[32] cycle loop counter (num bins)",\
"[32] lne pulse width (counts)",\
"[32] end cycle loop delay (counts)",\
"[32] DAC sleep/TOF delay (counts)",\
"[32] TOF pulse width (counts)",\
"[32] not used",\
"[32] not used",\
"[32] not used",\
"parameters = DWORD[10] :",\
"[0] 6",\
"[1] 1",\
"[2] 16",\
"[3] 7",\
"[4] 999987",\
"[5] 99997",\
"[6] 97",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"",\
"[output]",\
"DAC stop = FLOAT : 0.4301",\
"valid bins per sc = INT : 60",\
"num bins per cycle = INT : 16",\
"num cycles per sc = INT : 5",\
"scan loop count = INT : 1",\
"watchdog = INT : 59",\
"frontend msg = STRING : [256] DAC device (Galil RIO 47120) does not seem to be working. Wrote  2.500000 and read back 0.042400",\
"fe_flag = BOOL : n",\
"DAC_max = FLOAT : 10",\
"DAC_min = FLOAT : -10",\
"",\
"[Cycle Thresholds]",\
"Reref all = BOOL : n",\
"Reref1 = BOOL : n",\
"Reref2 = BOOL : n",\
"Reref3 = BOOL : n",\
"Reref4 = BOOL : n",\
"Threshold1 = FLOAT : 10",\
"Threshold2 = FLOAT : 9",\
"Threshold3 = FLOAT : 0",\
"Threshold4 = FLOAT : 0",\
"Num cycles to skip out-of-tol = INT : 1",\
"Alarm = INT : 0",\
"Enable threshold checking = BOOL : n",\
"Mode (0=% 1=level) = INT : 1",\
"",\
NULL }

#define POL_ACQ_MODE_NAME_DEFINED

typedef struct {
} POL_ACQ_MODE_NAME;

#define POL_ACQ_MODE_NAME_STR(_name) const char *_name[] = {\
"mode name = STRING : [8] 1h",\
NULL }

#define POL_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} POL_ACQ_COMMON;

#define POL_ACQ_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 13",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] ",\
"Type = INT : 8",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 10",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxpol.triumf.ca",\
"Frontend name = STRING : [32] pol_fevme",\
"Frontend file name = STRING : [256] pol_fevme.cxx",\
"Status = STRING : [256] pol_fevme@lxpol.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#define POL_ACQ_PPG_CYCLE_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    DWORD     bit_pattern;
  } pattern_x;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans_mcsen;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    INT       loop_count;
  } begin_cycle;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_lne;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ch5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_cycle;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans_mcsdis;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_dacsv;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ch7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    DWORD     bit_pattern;
  } pattern_y;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
} POL_ACQ_PPG_CYCLE;

#define POL_ACQ_PPG_CYCLE_STR(_name) const char *_name[] = {\
"[pattern_X]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"bit pattern = DWORD : 4096",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.0001",\
"loop count = INT : 0",\
"",\
"[trans_MCSEn]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] MCSGATE",\
"",\
"[begin_cycle]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TTRANS_MCSEN",\
"loop count = INT : 500",\
"",\
"[pulse_LNE]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGCYCLE",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] MCSNXT",\
"",\
"[pulse_ch5]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGCYCLE",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[end_cycle]",\
"time offset (ms) = DOUBLE : 0.000125",\
"time reference = STRING : [20] _TEND_PULSE_LNE",\
"",\
"[trans_MCSDis]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDCYCLE",\
"ppg signal name = STRING : [15] MCSGATE",\
"",\
"[pulse_dacsv]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TENDCYCLE",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] DACSERVP",\
"",\
"[pulse_ch7]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TENDCYCLE",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TEND_PULSE_CH7",\
"",\
"[pattern_Y]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] _TENDSCAN",\
"bit pattern = DWORD : 4096",\
"",\
"[skip]",\
"dummy = DOUBLE : 1",\
"",\
"[image]",\
"name = STRING : [32] ebit_pc_skip_trans.gif",\
"mode = STRING : [8] 1a",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq06.triumf.ca",\
"Frontend name = STRING : [32] fedeferred",\
"Frontend file name = STRING : [256] frontend.c",\
"Status = STRING : [256] fedeferred@isdaq06.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : y",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 16777215",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 10",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isdaq06.triumf.ca",\
"Frontend name = STRING : [32] fedeferred",\
"Frontend file name = STRING : [256] frontend.c",\
"Status = STRING : [256] fedeferred@isdaq06.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : y",\
"",\
NULL }

#endif

#ifndef EXCL_VME

#define VME_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} VME_COMMON;

#define VME_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 2048",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 20",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxpol.triumf.ca",\
"Frontend name = STRING : [32] pol_fevme",\
"Frontend file name = STRING : [256] pol_fevme.cxx",\
"Status = STRING : [256] pol_fevme@lxpol.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_SISPULSER

#define SISPULSER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SISPULSER_COMMON;

#define SISPULSER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 7",\
"Trigger mask = WORD : 128",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 121",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxpol.triumf.ca",\
"Frontend name = STRING : [32] pol_fevme",\
"Frontend file name = STRING : [256] pol_fevme.cxx",\
"Status = STRING : [256] pol_fevme@lxpol.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : y",\
"",\
NULL }

#endif

#ifndef EXCL_INFO

#define INFO_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} INFO_COMMON;

#define INFO_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 8",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] lxpol.triumf.ca",\
"Frontend name = STRING : [32] pol_fevme",\
"Frontend file name = STRING : [256] pol_fevme.cxx",\
"Status = STRING : [256] pol_fevme@lxpol.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#define INFO_SETTINGS_DEFINED

typedef struct {
  char      names_dbug[9][32];
  char      names_cycl[15][32];
  char      names_sums[4][32];
} INFO_SETTINGS;

#define INFO_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names DBUG = STRING[9] :",\
"[32] Data still buffered",\
"[32] Num SIS LNE events",\
"[32] LNE count from SIS",\
"[32] LNE preset value",\
"[32] number of bins",\
"[32] data bytes code",\
"[32] num enabled channels",\
"[32] discard first bin",\
"[32] discard first cycle",\
"Names CYCL = STRING[15] :",\
"[32] Scan type code",\
"[32] Number good cycles",\
"[32] Number supercycles",\
"[32] Cycle num within supercycle",\
"[32] Sweep number",\
"[32] Num cycles skipped",\
"[32] Num cycles histogrammed",\
"[32] DAC Increment number",\
"[32] DAC Set Value (volts)",\
"[32] DAC Readback (volts)",\
"[32] ADC1 Average",\
"[32] ADC2 Average",\
"[32] ADC3 Average",\
"[32] ADC4 Average",\
"[32] spare",\
"Names SUMS = STRING[4] :",\
"[32] SC Sum ch17",\
"[32] SC Sum ch18",\
"[32] SC Sum ch19",\
"[32] SC Sum ch20",\
"",\
NULL }

#endif

#ifndef EXCL_HISTO

#define HISTO_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} HISTO_COMMON;

#define HISTO_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 5",\
"Trigger mask = WORD : 32",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 10",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxpol.triumf.ca",\
"Frontend name = STRING : [32] pol_fevme",\
"Frontend file name = STRING : [256] pol_fevme.cxx",\
"Status = STRING : [256] pol_fevme@lxpol.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

