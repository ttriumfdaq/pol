
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "TGalilRIO.h"

int download_file(char* buffer, int size, char * filename);

// Little test program that sets and reads the analog 
// output and input on Galil 47120. 
int main(int argc, const char * argv[]){

  TGalilRIO *rio = new TGalilRIO("192.168.1.100");


  
  char REPLY[200];
  char COMMAND[5000];
  
  std::cout << "Sending Command MG @AN[0] " <<  std::endl;
  rio->Command("MG @AN[0];",&REPLY[0] );
  std::cout <<"reply: " <<REPLY << std::endl;
  
  char filename[]="POL.dmc";


  
  download_file(COMMAND, sizeof(COMMAND), filename);

  printf("\n\nSending COMMAND: \n %s\n\n",COMMAND);

  rio->SendBuffer(COMMAND, sizeof(COMMAND), &REPLY[0] );
  std::cout <<"reply: " <<REPLY << std::endl;


  printf("Starting the program  XQ;\n");
   std::cout << "Sending Command XQ; " <<  std::endl;
  rio->Command("XQ;",&REPLY[0] );
  std::cout <<"reply: " <<REPLY << std::endl;

  for (int i=0; i< 10; i++)
    {
      sleep(2);

      printf("\nReading AV data  MG AV[0]\n");

      std::cout << "Sending Command MG AV[0] " <<  std::endl;
      rio->Command("MG AV[0];",&REPLY[0] );
      std::cout <<"reply: " <<REPLY << std::endl;
    }
  


 

}

int download_file(char *FILEBUFFER, int size, char * filename)
{

  char *p,*q;
  FILE *fin;
  int c,cntr=0;

  sprintf(FILEBUFFER,":DL;");
  printf("FILEBUFFER = %s\n",FILEBUFFER);

  p=&FILEBUFFER[4]; q=&FILEBUFFER[size-3];


  printf("download_file:  opening file %s\n",filename);
  fin = fopen(filename,"r");
  if(fin==NULL)
    {
      printf("Could not open file %s\n", filename);
      return 0; // Failure
    }

  cntr=0;
  while((c=getc(fin))!=EOF)
    {
      *p=c;
      p++;
      cntr++;
      if(p > q)
	{
	  printf("test_file: file has been truncated after %d characters\n",cntr);
	  break; // too much data
	}
    }
  // End of file
  *p++='\\';	
  *p='\0';
  printf("test_file: cntr=%d  strlen(FILEBUFFER)=%d\n",cntr,strlen(FILEBUFFER));

  
  printf("FILEBUFFER: \n %s\n",FILEBUFFER);
  return 1;

}

