#include <string.h>
#include <stdio.h>

int test(char *cmd);
int test_file(char *cmd,int len);

int main(void)
{

  char cmd[]="B;";
  int len,size;
  char command[2000];

  printf("cmd=%s\n",cmd);
  len=strlen(cmd);
  printf("test: cmd=%s and len=%d\n",cmd,len);
  test(cmd);

  size=sizeof(command);
  printf("size of command string is =%d \n",size);
 

  test_file(command,size);
  len=strlen(command);
  if(len>0)
      printf("len=%d command=\n%s\n",len,command);
      

}

int test(char *cmd)
{
  int i,len;
  char *p;
  char command[300];

  len=strlen(cmd);
  printf("test: cmd=%s and len=%d\n",cmd,len);


  strncpy(command, cmd,len-1);
  printf("command=\"%s\" length=%d\n",command,strlen(command));
  return;
}

int test_file(char * cmd, int length)
{
  char filename[]="POL.dmc";
  char *p,*q;
  p=cmd; q=&cmd[length-2];
  FILE *fin;
  int len,c,cntr=0;
  printf("test_file: cntr=%d  strlen(cmd)=%d\n",cntr,strlen(cmd));
  printf("size of cmd string is %d\n",length);

  printf("Now opening file %s\n",filename);
  fin = fopen(filename,"r");
  if(fin!=NULL)
    {
      cntr=0;
      while((c=getc(fin))!=EOF)
	{
	  // putc(c,stdout);
          *p=c;
	  p++;
	  cntr++;
	  if(p > q)
	    {
	      printf("test_file: file has been truncated after %d characters\n",cntr);
	      break; // too much data
	    }
	}
      *p='\0';
      printf("test_file: cntr=%d  strlen(cmd)=%d\n",cntr,strlen(cmd));
    }
  else
    printf("Could not open file %s\n", filename);


  return 1;


}
