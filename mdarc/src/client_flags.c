/* client_flags.c
       included by mdarc

       When mdarc starts, if rn=0, clear all client flags.
       On a stop transition, clear all client flags.
       On a prestart transition, DO NOT clear all client flags, and rf_config runs on a prestart, and
       has usually set its flag before mdarc can clear it. Can clear all flags EXCEPT ones that run on a 
       prestart - this may be necessary if tr_stop did not work properly.

     If running, rf_config (which also runs on a prestart transition) clears its own flag

$Log: client_flags.c,v $
Revision 1.9  2004/11/24 21:16:20  suz
add support for POL's fe_runlog

Revision 1.8  2004/10/19 17:51:10  suz
print statements now printed only if debug_cf is true

Revision 1.7  2004/04/02 22:20:16  suz
change a message

Revision 1.6  2004/02/11 02:29:05  suz
imusr now uses mheader not fe_camp

Revision 1.5  2004/01/09 22:00:10  suz
remove some debugging

Revision 1.4  2004/01/08 20:41:43  suz
change debug to debug_cf

Revision 1.3  2003/10/15 19:25:18  suz
add support for imusr

Revision 1.2  2003/07/31 21:10:07  suz
change client flag to int as alarm didn't like bool

Revision 1.1  2003/07/29 18:17:01  suz
original


*/


/*------------------------------------------------------------------*/
INT create_ClFlgs_record(void)
/*------------------------------------------------------------------*/
{
  /* called from setup_hotlink */
  INT struct_size;
#ifdef MUSR
  MUSR_TD_ACQ_CLIENT_FLAGS_STR(acq_client_flags_str);
#else
  FIFO_ACQ_CLIENT_FLAGS_STR(acq_client_flags_str); 
#endif

  if(debug_cf) printf("create_ClFlgs_record starting\n");

  /* find the key for client flags */
  sprintf(client_str,"/Equipment/%s/client flags",eqp_name); 
  status = db_find_key(hDB, 0, client_str, &hCF);
  if (status != DB_SUCCESS)
    {
      hCF=0;
      if(debug_cf) printf("create_ClFlgs_record: Failed to find the key %s ",client_str);
      
      /* Create record for client flags area */     
      if(debug_cf) printf("Attempting to create record for %s\n",client_str);
      
      status = db_create_record(hDB, 0, client_str , strcomb(acq_client_flags_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"create_ClFlgs_record","Failure creating client flags record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"create_ClFlgs_record","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug_cf) printf("Success from create record for %s\n",client_str);
    }    
  else  /* key hCF has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hCF, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "create_ClFlgs_record", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}
#ifdef MUSR
      struct_size =   sizeof(MUSR_TD_ACQ_CLIENT_FLAGS);
#else
      struct_size =  sizeof(FIFO_ACQ_CLIENT_FLAGS);
#endif
      if(debug_cf)
	printf("create_ClFlgs_record:Info - size of saved structure: %d, size of client flags record: %d\n",
	       struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"create_ClFlgs_record","creating record (client flags); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, client_str , strcomb(acq_client_flags_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"create_ClFlgs_record","Could not create client flags record (%d)\n",status);
          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"create_ClFlgs_record","May be due to open records while running. Stop the run and try again");
          return status;
        }
        else
          if (debug_cf)printf("Success from create record for %s\n",client_str);
      }
    }
  
  /* try again to get the key hCF  */
  
  status = db_find_key(hDB, 0, client_str, &hCF);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_ClFlgs_record", "key %s not found (%d)", client_str,status);
    write_message1(status,"create_ClFlgs_record");
    return (status);
  }

  size = sizeof(fcf);
  status = db_get_record (hDB, hCF, &fcf, &size, 0);/* get the whole record for client flags */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"create_ClFlgs_record","Failed to retrieve %s record  (%d)",client_str,status);
    write_message1(status,"create_ClFlgs_record");
    return(status);
  }
  return status;
}

/*------------------------------------------------------------------*/
INT clear_client_flags()
/*------------------------------------------------------------------*/
{

  /* Clear the flags of the clients run on frontend_init and after a (post) stop transition.
      -   called from tr_stop.
     Do not call from tr_prestart or rf_config flag will be cleared after it has already been set by rf_config.

   */


#ifdef MUSR
  fcf.musr_config =  FALSE;
#else
  fcf.rf_config =  FALSE;
  fcf.fe_epics = FALSE;  // may not have fe_epics involved
#endif
  fcf.mheader = FALSE;   //BNMR/BNQR
  fcf.frontend = FALSE;  // common to all
  fcf.mdarc = FALSE;
  fcf.client_alarm =  0;

  // set record
  size = sizeof(fcf);
  status = db_set_record (hDB, hCF, &fcf, size, 0);/* set the whole record for client flags */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"clear_client_flags","Failed to set %s record  (%d)",client_str,status);
    write_message1(status,"clear_client_flags");
    return(status);
  }
  return status;
}

/*------------------------------------------------------------------*/
INT check_transition(BOOL *trans)
/*------------------------------------------------------------------*/
{
  INT i;
  i=0;

  /* check if transition in progress */
   
  size = sizeof(INT);
  status = db_get_value(hDB, 0, "/Runinfo/Transition in progress", &i, &size, TID_INT, TRUE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"check_transition","Error getting value for \"/Runinfo/Transition in progress\" (%d)",status);
    return(status);
  }
  *trans = (BOOL)i;
  if(debug_cf)printf("check_transition: returning a value of %d\n",i);
  return(status);
}




/*------------------------------------------------------------------*/
INT check_client_flags(BOOL *tr_flag)
/*------------------------------------------------------------------*/
{
  /* parameter: tr_flag
     true if this is the second time check_client_flags was called, false if the first time
  */
  BOOL ibool;
  char client_list[128];
  char str[128];
  INT i;

  if(*tr_flag || debug_cf)
    printf("check_client_flags: starting with *tr_flag=%d\n",*tr_flag);

  /* get the record */
  size = sizeof(fcf);
  status = db_get_record (hDB, hCF, &fcf, &size, 0);/* get the whole record for client flags */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"check_client_flags","Failed to retrieve %s record  (%d)",client_str,status);
      write_message1(status,"check_client_flags");
      return(status);
    }

  /* all client SUCCESS flags should be set */
  sprintf(client_list,"");
#ifdef MUSR
#ifdef IMUSR
  if (!fcf.mdarc)
    strcat(client_list, "imdarc, ");      
  if (!fcf.musr_config)
    strcat(client_list,"imusr_config, ");
  if (!fcf.fe_camp)
    strcat(client_list, "fe_camp, ");
#else //MUSR
  if (!fcf.mdarc)
    strcat(client_list, "mdarc, ");      
  if (!fcf.musr_config)
    strcat(client_list,"musr_config, ");
#endif
            
#else 
#ifdef POL
  if (!fcf.mdarc)  /* POL uses fe_runlog instead of mdarc */
    strcat(client_list, "fe_runlog, ");
 if (!fcf.rf_config)
    strcat(client_list,"rf_config, ");
#else // BNMR/BNQR
  if (!fcf.mdarc)
    strcat(client_list, "mdarc, ");      
  if (!fcf.rf_config)
    strcat(client_list,"rf_config, ");
  if (!fcf.mheader)
    strcat(client_list, "mheader, ");
   //  if (!fcf.fe_epics)
   //    strcat(client_list, "fe_epics ");   
#endif
#endif      
  if (!fcf.frontend)  // common
    strcat(client_list, "frontend, ");      
   

  if(debug_cf)printf("client_list = %s\n",client_list);

#ifdef MUSR
#ifdef IMUSR
if(debug_cf)
  printf("check_client_flags: for IMUSR checking on mdarc,musr_config,fe_camp, frontend flags\n");
if(!fcf.mdarc || !fcf.musr_config || !fcf.fe_camp || !fcf.frontend)
#else  // MUSR
if(debug_cf)
  printf("check_client_flags: for TD-MUSR checking on mdarc,musr_config, frontend flags\n");
if(!fcf.mdarc || !fcf.musr_config ||  !fcf.frontend)
#endif // IMUSR

#else 
#ifdef POL
if(debug_cf)
  printf("check_client_flags: for POL checking on fe_runlog rf_config and frontend flags\n");
if(!fcf.mdarc || !fcf.rf_config || !fcf.frontend)

#else  // BNMR/BNQR
if(debug_cf)
  printf("check_client_flags: for BNMR/BNQR checking on mdarc, rf_config and frontend flags\n");
if(!fcf.mdarc || !fcf.rf_config || !fcf.mheader || !fcf.frontend)
#endif
#endif

  { /* client flag(s) not set */
      client_list[strlen(client_list)-2]='\0'; // strip off the last comma and space
      printf("check_client_flags: Success flag not set by client(s): %s \n",client_list);
      /* check if transition in progress */
      status=check_transition(&ibool);
      if(status != DB_SUCCESS) 
	return (status);
      if (ibool)
	{
	  printf("check_client_flags: run in transition, *tr_flag= %d\n",*tr_flag);
	  if (! *tr_flag)  // is this the first time? 
	    {     // yes
	      cm_msg(MINFO,"check_client_flags","run in transition, waiting and retrying\n",*tr_flag);
	      cm_yield(1000);
	      *tr_flag = TRUE;
	      return (status);  /* try again after a bit */
	    }
	  // set transition in progress to 0 prior to stopping the run
	  i = 0;
	  status = db_set_value(hDB, 0, "/Runinfo/Transition in progress", &i, sizeof(INT), 1, TID_INT);
	  if(status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"check_client_flag","Error setting \"/Runinfo/Transition in progress\" to %d (%d)",i,status);
	      return (status);
	    }
	} // if transition
      *tr_flag =  FALSE;  // clear flag for next time
      
      /* set the stop alarm flag - this should cause mhttpd to give an alarm condition */
      i=1;
      size = sizeof(i);
      sprintf(str,"%s/client alarm",client_str);
      status = db_set_value(hDB, 0, str, &i, size, 1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "check_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
		 str,i,status);
	  return status;
	}

      if (fcf.enable_client_check)
	{
	  /* Stop the run */
	  printf("check_client_flags: attempting to stop the run\n");
	  cm_msg(MERROR,"check_client_flags","stopping run due to failure from at least one of these client(s) at run start: %s",client_list);

	  cm_msg(MINFO,"check_client_flags","calling stop_run()");
	  status = stop_run();      
	  if(status != DB_SUCCESS)
	    {
	      printf("check_client_flags: failure attempting to stop the run\n");
	      cm_msg(MERROR,"check_client_flags","Stop the run now and restart");
	    }
	  else
	    printf("check_client_flags: success from stop_run; run should now be stopped\n");
	}
      else   // mdarc's auto run stop is disabled
	{
	  cm_msg(MERROR,"check_client_flags",
		 "client flags indicate YOU MUST STOP RUN & RESTART (auto stop is disabled)");
	}
  }
  else
    printf("check_client_flags: all clients have started successfully\n");
  
  return status;  
}



/*------------------------------------------------------------------*/
INT setup_hot_clients(void)
/*------------------------------------------------------------------*/
{
  /* called by tr_start only on genuine run start 


     Sets up a hotlinks on clients that may detect errors
     during the run.
     Hotlinks are set on odb keys "frontend" and "mdarc"
*/
  HNDLE hktmp;
  
  
  if(debug_cf) printf("setup_hot_clients starting\n");

  
  /* set up hot link on frontend frontend flag */
  if (db_find_key(hDB, hCF, "frontend", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fcf.frontend);
    status = db_open_record(hDB, hktmp, &fcf.frontend, size, MODE_READ, hot_frontend , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hot_clients","Failed to open record (hotlink) for client flag frontend (%d)", status );
	write_message1(status,"setup_hot_clients");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hot_clients","Failed to find key for client flag frontend (%d)", status );
      return(status);
    }

  /* set up hot link on mdarc flag */
  if (db_find_key(hDB, hCF, "mdarc", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fcf.mdarc);
    status = db_open_record(hDB, hktmp, &fcf.mdarc, size, MODE_READ, hot_mdarc , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hot_clients","Failed to open record (hotlink) for client flag mdarc (%d)", status );
	write_message1(status,"setup_hot_clients");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hot_clients","Failed to find key for client flag mdarc (%d)", status );
      return(status);
    }
  return (status);
}



/*------------------------------------------------------------------*/
INT stop_run(void)
/*------------------------------------------------------------------*/
{
  INT debug_flag; // needed for cm_transition
  INT i;
  INT stat;
  char str[256];
  BOOL transition;

  /* this code based on that in odbedit.c */

  if(debug_cf)
    printf("stop_run: starting\n");

  if(gbl_stopping_flag)
    return; /* do not stop run more than once */

  /* check if transition in progress */
  transition=FALSE;
  status=check_transition(&transition);  // check_client_flags should have made sure this is not set
  if(status != DB_SUCCESS) 
    return (status);

  if(transition)
    {
      cm_msg(MERROR,"stop_run","start/stop already in progress; may not be able to stop run automatically\n");
       cm_msg(MERROR,"stop_run","if run does not stop automatically, use browser or odbedit to\n");
     
      cm_msg(MERROR,"stop_run", "set \"/Runinfo/Transition in progress\" manually to zero, then STOP the RUN");
      return (DB_INVALID_PARAM);
    }

  /* Set transition in progress */
  i = 1;
  status = db_set_value(hDB, 0, "/Runinfo/Transition in progress", &i, sizeof(INT), 1, TID_INT);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","Error setting \"/Runinfo/Transition in progress\" to %d (%d)",i,status);
      return (status);
    }

  // stop the run - also forces a stop if deferred transition c.f. "stop now"
  debug_flag = debug_cf; // debugging 
  status = cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), SYNC, debug_flag);      
  if (status != CM_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","Error after cm_transition: \"%s\", status=%d", str,status);
      printf("stop_run: error  after cm_transition: \"%s\", status=%d\n", str,status);
    }
  else
    {
      if(debug_cf)printf("stop_run: success from cm_transition \n");
      gbl_stopping_flag = TRUE;
    }
  i = 0;
  stat = db_set_value(hDB, 0, "/Runinfo/Transition in progress", &i, sizeof(INT), 1, TID_INT);
  if(stat != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","Error setting \"/Runinfo/Transition in progress\" to 0 (%d)",stat);
      return (stat);
    }

  // Clear the alarm flag
  i=0;  
  sprintf(str,"/equipment/%s/client flags/client alarm",eqp_name );
  printf("stop_run: setting alarm flag to %d\n",i); 

  size = sizeof(i);
  status = db_set_value(hDB, 0, str, &i, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "stop_run", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     str,i,status);
      return status;
    } 
  return status;
}
     


/*------------------------------------------------------------------*/
void hot_frontend (HNDLE hDB, HNDLE hktmp ,void *info)
/*------------------------------------------------------------------*/
{
  char str[128];
  char cmd[256];
  BOOL dummy=TRUE;
  INT i;

  debug_cf=TRUE;
  /* client flag has been touched */
  printf ("hot_frontend: starting ..... frontend client flag has been touched.  \n");
  if(debug_cf)
    printf ("hot_frontend: frontend success flag = %d, run number = %d,  closing record\n",fcf.frontend,run_number);

  printf("fcf.mdarc=%d, fcf.rf_config=%d, fcf.mheader=%d, fcf.frontend=%d, fcf.fe_epics=%d\n",
	 fcf.mdarc,fcf.rf_config,fcf.mheader,fcf.frontend,fcf.fe_epics);
  if(fcf.frontend) 
    {
    return; // success flag is true
    debug_cf=FALSE;
    }
  else
    {
      if (fcf.client_alarm == 0)
	printf("client_alarm is not set!!! may be false alarm\n");
      else
	printf("client_alarm is set\n");


      printf("hot_frontend: frontend has an error condition, or wants run to be stopped\n");
      printf("hot_frontend: Setting the client alarm flag\n");
      /* set the stop alarm flag - this should cause mhttpd to give an alarm condition */
      i=1;
      size = sizeof(i);
      sprintf(str,"%s/client alarm",client_str);
      status = db_set_value(hDB, 0, str, &i, size, 1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "hot_frontend", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
		 str,i,status);
	}

      if (fcf.enable_client_check)
	{
	  cm_msg(MERROR,"hot_frontend","frontend indicates run should stop ..... attempting to stop the run");
	  status = close_client_hotlinks();
	  if(status != SUCCESS)
	    cm_msg(MINFO,"hot_frontend","after close_client_hotlinks, status=%d");
	  cm_msg(MINFO,"hot_frontend","calling stop_run()");
	  stop_run();  // sends a stop transition
	}
      else
	{
	  cm_msg(MERROR,"hot_frontend","automatic stopping disabled. PLEASE STOP THE RUN \n");
	}
      
    }
  debug_cf=FALSE;
  return;
}

/*------------------------------------------------------------------*/
void hot_mdarc (HNDLE hDB, HNDLE hktmp ,void *info)
/*------------------------------------------------------------------*/
{
  char str[128];
  char cmd[256];
  INT i;
  
  /* client flag has been touched */
  if(debug_cf)
    {
      printf ("hot_mdarc: starting ..... mdarc client flag has been touched.  \n");
      printf ("hot_mdarc: mdarc success flag = %d, run number = %d \n",fcf.mdarc,run_number);
    }
  if(fcf.mdarc) 
    return; // success flag is true
  else
    {
      printf("hot_mdarc: mdarc has detected an error, or wants run to stop. \n");
      printf("hot_mdarc: Setting the client alarm flag\n");
      /* set the stop alarm flag - this should cause mhttpd to give an alarm condition */
      i=1;
      size = sizeof(i);
      sprintf(str,"%s/client alarm",client_str);
      status = db_set_value(hDB, 0, str, &i, size, 1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "hot_mdarc", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
		 str,i,status);
	}
      if (fcf.enable_client_check)
	{
	  cm_msg(MERROR,"hot_mdarc","frontend indicates run should stop ..... attempting to stop the run\n");
	  cm_msg(MINFO,"hot_mdarc","calling stop_run()");
	  stop_run();  // sends a stop transition
	}
      else
	cm_msg(MERROR,"hot_mdarc","mdarc wants run stopped; automatic stopping disabled. PLEASE STOP THE RUN \n");

    }
  return;
}
#ifdef MUSR
/*------------------------------------------------------------------*/
void hot_musr_config (HNDLE hDB, HNDLE hktmp ,void *info)
/*------------------------------------------------------------------*/
{
  /* this is useful for debugging, but is not used to stop the run, since
     musr_config does not indicate error conditions during the run */

  char str[128];
  char cmd[256];
  
  
  /* client flag has been touched  */
  if(debug_cf)
    {
      printf ("hot_musr_config: starting ..... musr_config client flag has been touched.  \n");
      printf ("hot_musr_config: musr_config success flag = %d, run number = %d\n",fcf.musr_config,run_number);
    }

  return;
}
#else
/*------------------------------------------------------------------*/
void hot_rf_config (HNDLE hDB, HNDLE hktmp ,void *info)
/*------------------------------------------------------------------*/
{
  /* this is useful for debugging, but is not used to stop the run, since
     rf_config does not indicate error conditions during the run */

  char str[128];
  char cmd[256];
  
  
  /* client flag has been touched  */
  if(debug_cf)
    {
      printf ("hot_rf_config: starting ..... rf_config client flag has been touched.  \n");
      printf ("hot_rf_config: rf_config success flag = %d, run number = %d\n",fcf.rf_config,run_number);
    }
  return;
}
#endif

/*------------------------------------------------------------------*/
INT close_client_hotlinks(void)
/*------------------------------------------------------------------*/
{
  HNDLE hktmp;
  INT stat;

  printf ("Close_client_hotlinks: starting...\n");
  /* remove hot link on client flags while the run is off */
  status = db_find_key(hDB, hCF, "frontend", &hktmp);
  if(status == DB_SUCCESS)
    {
      if(debug_cf)printf("close_client_hotlinks: Closing frontend client flag hotlink\n");
      status = db_close_record(hDB, hktmp);
      if(status != DB_SUCCESS)
	printf("close_client_hotlinks: error closing frontend hotlink (%d)\n",status);
    }
  else
    printf("close_client_hotlinks: error finding key for client flag frontend (%d)\n",status);
  stat = status;


  status = db_find_key(hDB, hCF, "mdarc", &hktmp);
  if(status== DB_SUCCESS)
    {
      if(debug_cf)printf("close_client_hotlinks: Closing mdarc client flag hotlink\n");
      status = db_close_record(hDB, hktmp);
      if(status != DB_SUCCESS)
	printf("close_client_hotlinks: error closing mdarc hotlink (%d)\n",status);
    }
  else
    printf("close_client_hotlinks: error finding key for client flag mdarc (%d)\n",status);

  if(stat != DB_SUCCESS)
    return stat;
  else
    return status;
}


/*------------------------------------------------------------------*/


