/* This code included by mheader and rf_config (BNMR/BNQR)
        and by fe_camp (IMUSR)

  $Log: check_camp.c,v $
  Revision 1.3  2004/01/14 19:36:35  suz
  change path of mdarc/camp_hostname to mdarc/camp/camp_hostname to agree with MUSR

  Revision 1.2  2003/09/26 20:58:49  suz
  change a comment

  Revision 1.1  2003/07/29 18:33:57  suz
  original for mheader


  Was Revision 1.3  2003/01/23 19:06:03  used by fe_camp.
     Now moved to mdarc area for mheader


*/

void check_camp()
{
  /* Check camp host name is valid
     Fills global serverName
     Sets  global camp_available true or false
   */
  char ctemp[LEN_NODENAME+1];
  int len,j;
  INT stat;

  if(debug)printf("Check_camp: starting\n");
  /* Check there is a valid camp hostname */

  len = strlen(fmdarc.camp.camp_hostname);  
  if(len > 0)
    {
      fmdarc.camp.camp_hostname[len]='\0';    
      strcpy (ctemp,fmdarc.camp.camp_hostname );

      for  (j=0; j< LEN_NODENAME ; j++) ctemp[j] = toupper (ctemp[j]); /* convert to upper case */                
      
      trimBlanks (ctemp,ctemp);
      if (strncmp(ctemp,"NONE",4) == 0)
	{
	  cm_msg(MINFO, "check_camp:", "camp hostname is not set. No camp data available");
	  camp_available = FALSE  ; /* name is NONE */
	} 
      else
	{
	  strcpy( serverName, ctemp);
	  if(debug)printf("check_camp : Camp hostname: %s\n",serverName );
	  camp_available = TRUE   ; /* camp is running */
	}
    }
  else
    {
      cm_msg(MINFO, "check_camp", "camp hostname is not set. No camp data available");
      camp_available = FALSE;
    }
  return;
}

INT ping_camp()
{
  /* must be called with camp_available true i.e. a valid CAMP hostname */
      
  /* ping the camp host because if someone turns off the crate, there is
     no timeout and it can waste a lot of time  */ 
  
  INT status;
  char cmd[256];

  if(debug) printf("ping_camp: starting\n");
  sprintf(cmd,"ping -c 1 %s &>/dev/null",serverName);
  if(debug) printf(" ping_camp: pinging camp host %s using command: %s\n",serverName,cmd);
  status=system(cmd);
  
  if(debug)printf("status after ping %d\n",status);
  if(status)
    {
      camp_available= FALSE ; /* no name specified */
      if(debug)printf("ping_camp: Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      return  DB_NO_ACCESS;
    }
  return DB_SUCCESS;
}
  

