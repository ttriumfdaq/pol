# do_link.pl 
#
# subroutines to make and remove links
#     called by change_mode.pl
#
# $Log: do_link.pl,v $
# Revision 1.16  2006/06/19 17:34:24  suz
# change link to always be PPG (not e.g.PPG1f,PPG20)
#
# Revision 1.15  2005/09/26 17:53:39  suz
# add subroutine load_mode_defaults; loads default values when changing mode
#
# Revision 1.14  2004/11/18 19:32:13  suz
# fix change_link_num so it can successfully clear num cycles (a link). Added extra param to odb_cmd to fix link problem
#
# Revision 1.13  2004/06/29 22:33:07  suz
# add a check on value of num cycles; turn off debugging
#
# Revision 1.12  2004/06/10 18:21:04  suz
# use our; last Rev message is bad
#
# Revision 1.11  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.10  2004/03/30 22:54:03  suz
# add timing info using write_time
#
# Revision 1.9  2004/03/29 19:49:40  suz
# fix small bug
#
# Revision 1.8  2004/03/29 19:47:08  suz
# add print_2
#
# Revision 1.7  2003/07/31 22:44:28  suz
# add num cycle/num scans parameter
#
# Revision 1.6  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.5  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.4  2001/11/01 17:53:58  suz
# add cvs tag
#
#
use strict;
# globals (main program defines these)
# our not supported
our ($COMMAND,$EXPERIMENT,$ANSWER,$ODB_SUCCESS);
our ( $TRUE, $FALSE, $FAILURE,  $SUCCESS) ;
our ( $DEBUG, @ARRAY);
our ( $STATE_STOPPED, $STATE_RUNNING);
# for odb  msg cmd:
our ($MERROR, $MINFO, $MTALK);
our ($DIE, $CONT);

our $debug =0;


sub make_a_link 
{
#
# If expt is "1b", calls 
#            remove_a_link to remove /alias/ppg1b if present,
# then   makes links /ppg/ppg1b to  /alias/ppg1b
# 
# input parameter:  experiment_name
# returns           $SUCCESS or $FAILURE
    
    my ($expt_name) = @_; # get the input parameters
    my ($ppg_name,$status);
    my $name="make_a_link";
    
    write_time($name,$CONT);
    if($debug) { print ("$name: input params: expt name = $expt_name\n");}
    print FOUT ("$name: input params: expt name = $expt_name\n");
    $expt_name =~ s/_$//;  # remove any trailing underscore
    
    
# check if link exists - if so, remove it
    ($status)= remove_a_link ($expt_name);
    unless ($status)
    {
	print_2 ($name, "Failure returned from remove_a_link for $expt_name",$CONT );
	return ($status);
    }
    
# make the link
    
    $ppg_name=sprintf("PPG$expt_name"); # no appended & here! 
    ($status) = odb_cmd ( "ln","/ppg","/$ppg_name" ,"/alias/$ppg_name&") ;
    
#print "status=$status\n";
    unless($status)
    { 
	print_3($name, "Failure linking \"/ppg/$ppg_name\" to \"/alias/$ppg_name&\" ",$MERROR,$CONT); # $CONT -> return
	return ($status);
    }
    else
    {
	print_2 ($name, "Successfully linked   /ppg/$ppg_name to /alias/$ppg_name& ",$CONT);
    }   
    write_time($name,3);
    return($status);
}



sub remove_a_link
{
#
# If expt is "1b", checks if link /alias/ppg1b& is present and removes it
# 

    # input parameters:
    my ($expt_name) = @_; # get the input parameters
# returns $SUCCESS or $FAILURE
    
    my ($ppg_name,$status,$string,$key,$temp_name);
    my $name="remove_a_link";
    my $msg;
    my $debug=0;
#     write_time($name,$CONT);

    if($debug) { print ("$name: input params: expt name = $expt_name\n");}
    print FOUT ("$name: input params: expt name = $expt_name\n");

   $expt_name =~ s/_$//;  # remove any trailing underscore
    
    $ppg_name=sprintf("ppg$expt_name&");
    if($debug) { print ("$name:  ppg_name = $ppg_name\n");}
    print FOUT ("$name:  ppg_name = $ppg_name\n");

    
#  Check if link exists e.g.  /alias/ppg2b& (does not detect /alias/ppg2b)

   ($status) = odb_cmd ( "ls","/alias/$ppg_name","") ;
    ($string,$msg) = get_string($key);
#    if($debug) 
    { 
	print "$name: returned string for \'ls /alias/$ppg_name\':\"  $string\"\n"; 
	unless ($msg eq "") { print "  and msg =$msg\n";}
    }
    if($status)  
    {              # found the link
	
	if($debug) { print "$name: found a link for /alias/$ppg_name and about to remove it\n"; }
	print FOUT "$name: found a link for /alias/$ppg_name and about to remove it\n"; 
	($status) = odb_cmd ( "rm","/alias/$ppg_name"," ") ;
	unless($status)
	{          # failure removing link
	    print_3 ($name,"Failure from odb_cmd (rm); Error removing old link \"/alias/$ppg_name\" ",$CONT);# $CONT-> return
	    return($status);
	}
	else       # success removing link
	{
	    print_2 ($name, "Success - old link /alias/$ppg_name has been removed",$CONT);
	}
    }  
    else 
    {             # no link found
	print_2 ($name, "info - no link found for /alias/$ppg_name",$CONT) ;
    }
#    write_time($name,3);
    return($SUCCESS);
}

sub test_a_link
{
# test if there is a link already for expt_name

    # input parameters:
    my ($expt_name, $link_name) = @_; # get the input parameters
# returns 1 (TRUE) for a link, 0 (FALSE)  for no link 
    
    my ($ppg_name,$status,$string,$key);
    my $name="test_a_link";
    my $msg;
    my $temp=" ";

    write_time($name,$CONT);
    print_2( $name, "input params: expt name = $expt_name  link name =$link_name",$CONT);
    $expt_name =~ s/_$//;  # remove any trailing underscore
    
    
    
#  Check if link exists 
    ($status) = odb_cmd ( "ls","/alias", " ") ;
    ($string,$msg) =get_string($key);
    if($debug) { print FOUT  "$name: returned string for \'ls $link_name\':\"  $string\"\n"; }
    if($status)  
    {              # find the link in /alias output
	if($string = ~/$link_name/i)
	{
	    $temp="ppg$expt_name";
	    print_2 ($name, "found \"$link_name\"... now looking for $temp ",$CONT);
# and make sure it points to correct expt name
	    if ($string = ~/$temp/i) 
	    {
		print_2 ($name, "and found \"$temp\" ",$CONT);
		write_time($name,3);
		return($TRUE);
	    }
	    else
	    {             # correct link not found
		print_2( $name,"link to $temp not found for $link_name ... removing it",$CONT) ;
		($status)= remove_link($link_name);
		unless($status) 
		{ 	
		    print_2 ($name, "error cannot remove link $link_name",$DIE); 
		}  # failure after remove_link
		return ($FALSE);
	    }
	}
	else
	{
	   print_2 ($name, "error cannot remove link $link_name",$DIE);   
	}
    }
    else
    {             # no link found
	print_2( $name,"no $link_name has been found",$CONT) ;
    }
    write_time($name,3);
    return($FALSE);    
}

#=============================================================
sub make_link
{
#
# makes a  links between input parameters
# 
# input parameters:  path to link, link name
# returns           $SUCCESS or $FAILURE
    
    my ($link_path,$link_name) = @_; # get the input parameters
    my ($ppg_name,$status);
    my $name="make_link";
    my ($string,$key,$msg);
    my $edit_rn="/experiment/edit on start/Edit run number";

     write_time($name,$CONT);
#    if($debug) {
 print ("$name: input params: link_path = $link_path, link_name = $link_name\n"); #}
    print FOUT ("$name: input params: link_path = $link_path, link_name = $link_name \n");
    
    unless ($link_path) 
    { 
	print  "$name: No link path has been supplied\n";
	print      "$name: Failure returned from make_link \n";
	return ($status);
    }
    unless ($link_name) 
    { 
	print  "$name: No link name has been supplied\n";
	print      "$name: Failure returned from make_link \n";
	return ($status);
    } 
    
    
    
# make the link
    
    
    ($status) = odb_cmd ( "ln","$link_path","" ,"$link_name") ;
    ($string,$msg) =get_string($key); 
    print "$name: returned string for \'ln $link_path $link_name\':\"  $string\"\n"; 
    unless ($msg eq "") { print "  and msg =$msg\n";}
    #print "status=$status\n";
    unless($status)
    { 
	print_3 ($name,"Failure from odb_cmd (ln). Could not link \"$link_path\"  to \"$link_name\" ",$MERROR,$CONT);# CONT->return
	return ($status);
    }
    else
    { 
	print_2 ($name, "Successfully linked   $link_path to  $link_name", $CONT);
    } 

    ($status) = odb_cmd ( "move","$edit_rn","" ,"bot") ;
    ($string,$msg) =get_string($key); 
    print "$name: returned string for \'move $edit_rn bot\'  $string\"\n"; 
    unless ($msg eq "") { print "  and msg =$msg\n";}
    #print "status=$status\n";
    unless($status)
    { 
	print_3( $name,"Failure from odb_cmd (move) failed to move \"$edit_rn\" to  bot",$MERROR,$CONT); # CONT->return
	write_time($name,3);
	return ($status);
    }
    write_time($name,3);
    return($status);
}



sub remove_link
{
#
# Remove the link at link_path
    
    # input parameters:
    my ($link_path) = @_; # get the input parameters
# returns $SUCCESS or $FAILURE
    
    my ($ppg_name,$status,$string,$key,$temp_name);
    my $name="remove_link";
    my $msg;

    write_time($name,$CONT);   
    if($debug) { print ("$name: input params: link_path = $link_path\n");}
    print FOUT ("$name: input params: link_path = $link_path\n");
    
    
    print FOUT "$name: about to remove link at path $link_path\n"; 
    ($status) = odb_cmd ( "rm","$link_path"," ") ;
    unless($status)
    {          # failure removing link
	print_3( $name,"Failure from odb_cmd (rm); Error removing old link \"$link_path\" ",$MERROR,$CONT); #CONT->return
	return($status);
    }
    else       # success removing link
    {
	print_2( $name,"Success - old link $link_path has been removed",$CONT);
    }
    
    write_time($name,3);
    return($SUCCESS);
}


sub change_link
{
# change_link is called when ppg mode is being changed from Type 1 to 2 or vice versa
#
# action: change link in edit on start to the log area (mdarc or mlogger)
#
# test if existing link in edit on start is to mdarc/mlogger
# unlink and relink if necessary
# 

    # input parameters:
    my ($expt_name) = @_; # get the input parameters
# returns 1 (TRUE) for a link, 0 (FALSE)  for no link 
    
    my ($ppg_name,$status,$string,$key);
    my $name="change_link";
    my ($msg,$path);
    my $mdarc_link ="/Equipment/FIFO_acq/mdarc/enable mdarc logging";
    my $mlogger_link = "/Logger/Channels/0/Settings/Active";
    my $write_data="/experiment/edit on start/write data"; 	
    my $link_exists;
    my $current_write_data=$FALSE;


    write_time($name,$CONT);
    print_2($name, "input params: expt name = $expt_name", $CONT);
    $expt_name =~ s/_$//;  # remove any trailing underscore
    

    $write_data=sprintf("/experiment/edit on start/write data"); 
    print FOUT  ("$name:  write_data = $write_data\n");
    if($debug) { print ("$name:  write_data = $write_data\n"); }
    
    
    
#  Check if link exists 
    ($status) = odb_cmd ( "ls","$write_data"," ") ;
    
    if($debug) { print FOUT  "$name: returned string for \'ls $write_data\':\"  $string\"\n"; }
    if($status)  
    {              # found the link
	print "$name: found $write_data \n";
	print FOUT "$name: found $write_data \n";
	$link_exists = $TRUE;
    }
    else
    {       # no link found
	print "$name: no link found for $write_data \n" ;
	print FOUT "$name: no link found for $write_data \n" ;
	$link_exists = $FALSE;
    }
#    
# CHANGE TO TYPE 2 EXPT
#
    if ( $expt_name =~ /^2/    )  # Type 2 experiment  ) 
    {
	print_2( $name,"we will be running a type 2 experiment, $expt_name ; type 1 logging will be set inactive ",$CONT);
	
#  let's see if type 1 logging is currently  enabled 
	#   Read the status of the midas logger from odb
	($status, $path, $key) = odb_cmd ( "ls"," /logger",  "write data " ) ; # need specific key for get_bool
	
	unless ($status) { print_3($name,"Error reading key /logger/write data from odb",$MERROR,1); }
	$current_write_data = get_bool($key);
	unless ($current_write_data)
	{
	    # midas logging is currently disabled; Logging should be enabled all the time so enable it 
	    print_2($name,"Midas logging currently disabled: now enabling (should be enabled for both Types)",$CONT);
	    # enable midas logger
	    ($status) = odb_cmd ( "set","/logger","write data" ,"y") ;
	    unless($status)
	    {
		print_3 ($name," Failure from odb_cmd (set); Error setting \"/logger/write data\" to y",$MERROR,1);
	    }	  
	}
	# midas logging is currently enabled
	#   Check if  midas logger is active 
	($status, $path, $key) = odb_cmd ( "ls","/logger/Channels/0/Settings",  "active " ) ; # need specific key for get_bool
	unless ($status) { print_3($name,"Error reading odb key /logger/Channels/0/Settings/active ",$MERROR,1); }
	$current_write_data = get_bool($key);
	if ( $current_write_data )
	{ # midas logging is currently active
	    print_2($name, "Midas logging currently active: now setting midas logging inactive (for Type1)",$CONT);
	    # disable type 1 logging
	    ($status) = odb_cmd ( "set","$mlogger_link","" ,"n") ;
	    unless($status){ print_3 ($name," Failure from odb_cmd (set); Error setting $mlogger_link to n",$MERROR,1); }	  
	}
	else
	{  # midas logging is currently inactive
	    print_2($name,"Midas logger is currently inactive",$CONT);
	}
 

	# see if "/experiment/edit on start/write data" is linked to mdarc area already	
	unless ($string =~ /mdarc/i) 
	{
	    if ($link_exists)
	    {
		($status)= remove_link($write_data);
		unless($status) { return($status); }  # failure after remove_link
	    }
	    ($status)= make_link($mdarc_link, $write_data);
	    unless($status) { return($status); } # failure after remove_link
	}
	
	if ($current_write_data)
	{
	    # enable type 2 logging
	    print_2( $name,"enabling mdarc logging",$CONT);
	    ($status) = odb_cmd ( "set","$mdarc_link","" ,"y") ;
	    unless($status) { print_3 ($name, "Failure from odb_cmd (set); Error setting $mdarc_link to y",$MERROR,0); }
	}	
	
    }
    
# 
# CHANGE TO TYPE 1 EXPT
#
    else
    {
	# Type 1 experiment 
	print_2( $name, "we will be running a type 1 experiment, $expt_name ; type 2 logging will be disabled ",$CONT);
	
	#  let's see if type 2 logging is currently  enabled 
	
	#   Read the status of the mdarc logger from odb
	($status, $path, $key) = odb_cmd ( "ls","/Equipment/FIFO_acq/mdarc",  "enable mdarc logging " ) ; # need specific key for get_bool
	unless ($status) { print_3($name,"Error reading key $mdarc_link from odb",$MERROR,1); }
	$current_write_data = get_bool($key);
	if ( $current_write_data )
	{ # mdarc logging is currently enabled
	    print_2($name,"mdarc logging currently enabled: now disabling mdarc logging (for Type2)",$CONT);
	    # disable type 2 logging
	    ($status) = odb_cmd ( "set","$mdarc_link","" ,"n") ;
	    unless($status)
	    { print_3 ($name," Failure from odb_cmd (set); Error setting $mdarc_link to n",$MERROR,1); }
	  
	}
	else
	{  # mdarc logging is currently disabled
	    print ($name,"Mdarc logging is currently disabled",$CONT);
	}

	# see if "/experiment/edit on start/write data" is linked to mlogger area already
	unless ($string =~ /mlogger/i) 
	{
	    if ($link_exists)
	    {
		($status)= remove_link($write_data);
		unless($status) { return($status); }  # failure after remove_link
	    }
	    ($status)= make_link($mlogger_link, $write_data);
	    unless($status) { return($status); } # failure after remove_link
	}
	if ($current_write_data)
	{
	    print_2( $name,"activating midas logging",$CONT);
	    # activate type 1 logging
	    ($status) = odb_cmd ( "set","$mlogger_link","" ,"y") ;
	    unless($status) { print_3 ($name, "Failure from odb_cmd (set); Error setting $mlogger_link to y",$MERROR,0); }
	}	

    }
    write_time($name,3);
    return($TRUE);
}



sub change_link_num
{
# change_link_num is called when ppg mode is being changed from Type 1 to 2 or vice versa
#
# action: change link in edit on start to the log area (mdarc or mlogger)
#
# test if existing link in edit on start is to mdarc/mlogger
# unlink and relink if necessary
# 

    # input parameters:
    my ($expt_name, $changing_mode) = @_; # get the input parameters
# returns 1 (TRUE) for a link, 0 (FALSE)  for no link 
    
    my ($ppg_name,$status,$string,$key);
    my $name="change_link_num";
    my ($msg,$path);
    my $cycle_link_exists;
    my $scan_link_exists;

    my $current_write_data=$FALSE;

    my $hardware_path =  "/Equipment/FIFO_acq/sis mcs/hardware/";
    my $nscan_link = "num scans"; # name of linked key
    my $ncycle_link = "num cycles"; # name of linked key
    my $edit_on_start= "/experiment/edit on start/";
    my $num_scan = "Number of scans"; #name of link
    my $num_cycle = "Number of cycles"; # name of link
   
    my ($num_scan_path,$num_cycle_path,$nscan_link_path,$ncycle_link_path);
    my $val=-1;

    $num_scan_path=sprintf("$edit_on_start$num_scan");
    $num_cycle_path=sprintf("$edit_on_start$num_cycle");
    $nscan_link_path=sprintf("$hardware_path$nscan_link");
    $ncycle_link_path=sprintf("$hardware_path$ncycle_link");

    write_time($name,$CONT);
    print_2($name,"input param: expt name = $expt_name",$CONT);
    $expt_name =~ s/_$//;  # remove any trailing underscore

#  Check if link exists to cycle 
    ($status) = odb_cmd ( "ls","$edit_on_start","$num_cycle","","","$ncycle_link") ;
    if($debug) 
    {
	($string,$msg) = get_string($ncycle_link); # looks for key in answer
	print FOUT  "$name: returned string for \'ls $num_cycle\':\"  $string\"\n"; 
    }
    if($status)  
    {              # found the link to cycle
	print_2($name, "found link $num_cycle ",$CONT);
	$cycle_link_exists = $TRUE;
    }
    else
    {       # no link to num_cycle  found
	print_2($name," no link found for $num_cycle ",$CONT) ;
	$cycle_link_exists = $FALSE;
    }
#    

#  Check if link exists to scan 
    ($status) = odb_cmd ( "ls","$edit_on_start","$num_scan","","","$nscan_link") ;    
    if($debug)
    {
	($string,$msg) = get_string($nscan_link); # looks for key in answer
	print FOUT  "$name: returned string for \'ls $edit_on_start$num_scan\':\"  $string\"\n";
    }
    if($status)  
    {              # found the link
	print_2( $name," found link $num_scan ",$CONT);
	$scan_link_exists = $TRUE;
    }
    else
    {       # no scan link found
	print_2 ($name,"no link found for $edit_on_start$num_scan ",$CONT) ;
	$scan_link_exists = $FALSE;
    }
#

#    
# CHANGE TO TYPE 2 EXPT
#
    if ( $expt_name =~ /^2/    )  # Type 2 experiment  ) 
    {
	if ($scan_link_exists)
	{ # remove old link
	    ($status)= remove_link($num_scan_path);
	    unless($status) { return($status); }  # failure after remove_link
	}

	if($changing_mode)
	{
	    print_2( $name,"we will be running a type 2 experiment, $expt_name ; link will be made to num_cycle",$CONT);
	}


#  let's see if we are set up for Type 2 already 
	if (! $cycle_link_exists)
	{
	    ($status)= make_link($ncycle_link_path, $num_cycle_path);
	    unless($status) { return($status); } # failure after make_link


	    # clear num cycles if changing to Type 2 (to avoid confusion) 
	    ($status) = odb_cmd ( "set","$edit_on_start","$num_cycle","0" ,"","","") ;
	    print_2($name, "clearing $edit_on_start$num_cycle",$CONT);
	    unless($status)
	    { 
		print_3( $name,"Failure from odb_cmd (set); Error setting $edit_on_start$num_cycle to 0",$MERROR,$CONT);  # continue anyway
	    } 
	    ($status, $path, $key) = odb_cmd ( "ls","$hardware_path","$ncycle_link") ;
	    unless($status)
	    {
		print_3( $name,"Failure from odb_cmd (set); Error reading $edit_on_start$num_cycle ",$MERROR,$CONT);  # continue anyway
	    }
	    else
	    {
		$val = get_int ( $key);
		print FOUT  "$name: \'ls $ncycle_link_path\': returns value= $val\n"; 
		if($val != 0)
		{
		    print_3( $name,"Failure from odb_cmd (set);  \"$num_cycle\" is not 0",$MERROR,$CONT);  # continue anyway
		}
	    }
	}

    }
    
# 
# CHANGE TO TYPE 1 EXPT
#
    else
    {
	if ($cycle_link_exists)
	{
	    ($status)= remove_link($num_cycle_path);
	    unless($status) { return($status); }  # failure after remove_link
	}
	if ($changing_mode)
	{
	    # Type 1 experiment 
	    print_2($name,"we will be running a type 1 experiment, $expt_name ; link will be made to num_scans ".$CONT);
	}
	if( ! $scan_link_exists)
	{
	    ($status)= make_link($nscan_link_path, $num_scan_path);
	    unless($status) { return($status); } # failure after make_link
	}
    }
    write_time($name,3);
    return($TRUE);
}


sub load_mode_defaults 
{
#
# 
# loads file /home/<$EXPERIMENT>/online/modes/<$mode_filename>.odb  if it exists
# 
# input parameter:  filename of mode file
# returns           $SUCCESS or $FAILURE
    
    my ($mode_filename) = @_; # get the input parameters
    my ($ppg_name,$status);
    my $name="load_mode_defaults";
    
    my $filename;
    my $path = "/home/$EXPERIMENT/online/modefiles";
	
    print "$name: input params: mode filename= $mode_filename\n";
    
    
    print FOUT "$name: input params: mode filename= $mode_filename\n";
    
    $filename = "$path/$mode_filename";
    
# check if file exists
    unless (-e $filename)
    {
	print_3 ($name, "Error - mode file $filename does not exist",$MINFO,$CONT);
	return ($FAILURE);
    }
    
    print "calling odb_cmd for \"load $filename\" \n"; 
    ($status) = odb_cmd ( "load","$filename","" ,"") ;
    unless($status)
    { 
        print_3($name,"Failure from odb_cmd (load); Error loading file $filename",$MINFO,$CONT);
	return ($FAILURE);
    }
    return ($SUCCESS);
}


# IMPORTANT
# this 1 is needed at the end of the file so require returns a true value   
1;   












