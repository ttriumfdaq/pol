#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from Pol's "Bad Scan" button
# 
# invoke this script with cmd:
#
#                     include path             experiment      scan flag  beamline   
# pol_bad_scan.pl /home/pol/online/mdarc/perl    pol             n         pol
#
# Sets a flag in odb (/Equipment/FIFO_acq/Pol params/Bad scan flag) that will
# will be detected by the analyzer, which will then not use the data from this scan
# Flag will be reset automatically by frontend at begin of next scan
#
# $Log: pol_bad_scan.pl,v $
# Revision 1.1  2005/02/22 19:46:08  suz
# original for POL expt
#
#
use strict;
##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir,$expt, $bad_flag, $beamline ) = @ARGV;
our $len =  $#ARGV; # array length
our $name = "pol_bad_scan"; # same as filename
our $nparam=4;
our $outfile = "pol_bad_scan.txt"; #path supplied by file open
our $parameter_msg = "include_path, experiment; bad flag; beamline\n";
#######################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($flags_path) ;
my $debug=$FALSE;
my $eqp_name;
#######################################################################
#
$|=1; # flush output buffers


# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

#
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; bad scan flag = $bad_flag\n";
#

if ($bad_flag eq "y") { $bad_flag = $TRUE;} 
else { $bad_flag = $FALSE ; }

#
#      determine equipment name from beamline
#
unless ( $beamline =~ /pol/i)
{
    print_3($name,  "ERROR: only beamline \"pol\" is supported ",$MERROR,$DIE);
} 

# POL experiment uses equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
    $flags_path = "/Equipment/$eqp_name/sis mcs/flag bad scan/";

print FOUT "flags_path = $flags_path\n";

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state == $STATE_RUNNING)
{   # Run in progress

    # check state of bad flag (supplied as a parameter)
    if ($bad_flag) 
    {
	print_3($name,  "INFO: bad scan flag is already set",$MERROR,$DIE);
    }	


    ($status) = odb_cmd ( "set","$flags_path","Bad scan flag" ,"y") ;
    unless($status)
    { 
	print_3($name,"Failure from odb_cmd (set); Error clearing bad scan flag",$MERROR,$DIE);
    }
    print_3($name,"Info: bad scan flag has been set",$MERROR,$CONT);
}
else
{    # not running; no action
    print_3($name,"Not running. Bad Scan button has no action.",$MERROR,$CONT);
}
exit;









