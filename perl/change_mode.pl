#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from user button
# 
# invoke this script with cmd e.g.
#                  include_path          experiment   select_mode   mode_name   
# change_mode.pl  /home/bnmr/online/perl     bnmr         2a        defaults
#
#   file with defaults is loaded when changing mode, e.g. 2a_defaults.odb
#       (providing it exists)
#   if mode_name is blank, looks for 2a_defaults.odb to load
#   if mode_name is "none", no defaults are loaded
#   if mode_name is "test", looks for 2a_test.odb to load
#
# Links e.g.
# /alias/PPG2a& to  /ppg/PPG2a
#
#  Note: assumes & is appended to all /alias/ppg* to suppress new window
#     will not detect /alias/ppg1a without &
#
# $Log: change_mode.pl,v $
# Revision 1.31  2008/04/17 21:40:51  suz
# add mode 1j (bnmr)
#
# Revision 1.30  2006/06/19 17:33:23  suz
# change link to always be PPG (not e.g.PPG1f,PPG20)
#
# Revision 1.29  2005/09/26 17:55:14  suz
# Extra input parameter mode_name....load default values from file when changing mode)
#
# Revision 1.28  2005/08/12 19:27:20  suz
# add experiment 2e
#
# Revision 1.27  2005/05/18 01:37:43  suz
# add mode 2d
#
# Revision 1.26  2005/04/08 20:16:32  suz
# set number of midbnmr regions to default
#
# Revision 1.25  2005/04/08 20:10:21  suz
# add 1e mode for BNQR
#
# Revision 1.24  2005/03/07 19:20:40  suz
# BNQR now has PSM so supports all modes
#
# Revision 1.23  2005/02/22 19:43:43  suz
# add 1d (dye laser) for POL
#
# Revision 1.22  2004/06/16 18:22:16  suz
# add remove_a_link 1h for pol, fix bug
#
# Revision 1.21  2004/06/15 20:02:53  suz
# add support for POL 1h mode; other_name set blank for pol
#
# Revision 1.20  2004/06/10 17:31:06  suz
# remove temp sleep;requires init2_check.pl; our; Rev 1.19 bad message
#
# Revision 1.19  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.17  2004/04/02 22:33:59  suz
# add check on whether check_next_run_number.pl is running
#
# Revision 1.16  2004/03/30 22:54:13  suz
# add timing info using write_time
#
# Revision 1.15  2004/03/29 19:46:45  suz
# change message from ERROR to INFO
#
# Revision 1.14  2004/03/29 18:23:22  suz
# allows only one copy to run; open_output_file now appends to message file; extra parameter process_id;bnqr supports 1f
#
# Revision 1.13  2003/12/01 23:17:44  suz
# add mode 1g (FAST)
#
# Revision 1.12  2003/07/31 22:45:11  suz
# add call to do_link for num cycle/num scans parameter
#
# Revision 1.11  2003/06/25 00:48:06  suz
# add 1e (Epics field scan)
#
# Revision 1.10  2003/05/21 17:12:10  suz
# improve transition message
#
# Revision 1.9  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.8  2003/01/20 22:44:59  suz
# add mode 10 (dummy freq scan)
#
# Revision 1.7  2003/01/08 18:37:10  suz
# add polb
#
# Revision 1.6  2002/06/07 20:51:50  suz
# add 1d
#
# Revision 1.5  2002/04/12 20:32:25  suz
# add parameter include_path
#
# Revision 1.4  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.3  2001/11/01 17:53:07  suz
# add cvs tag
#
#
#
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ($inc_dir, $expt, $select_mode, $mode_name ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "change_mode" ; # same as filename
our $other_name = "get_next_run_number";
our $outfile = "change_mode.txt"; # path will be added by file open
our $parameter_msg = "include path , experiment , select_new_mode  mode_name";
our $nparam = 4;  # no. of input parameters
our $beamline = $expt; # beamline is not supplied. Same as $expt for bnm/qr, pol
############################################################################
# local variables:
my ($transition, $run_state, $path, $key, $status);
my $input_path = "/Equipment/FIFO_acq/sis mcs/input/";
my $old_ppg;
my ($ppg_path,$string);
my ($process,$run_state,$transition);
my ($msg,$loop,$flag);
my $changing_mode=$FALSE;
my $mode_filename;
# for midbnmr
my $midbnmr_path = "/Equipment/FIFO_acq/mdarc/histograms/midbnmr";
my $midbnmr_regions=1;  # defaults 
my $midbnmr_1g_regions=3;
my $experiment_name;
my $fixed_link = "/alias/PPG&"; # name of fixed PPG link
#my $fixed_link1 = "/alias/PPG\&"; # name of fixed PPG link

#--------------------------------------------------------
$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/do_link.pl";

#bypass check on get_run_number.pl for pol as pol doesn't use MUSR run numbering
if ($expt =~ /pol/i  ){ $other_name=""; }

# Init2_check.pl checks:
#   one copy of this script running
#   no copies of $other_name running (may interfere with this script)
#   no. of input parameters is correct
#   opens output file:
#
require "$inc_dir/init2_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt, select new mode = $select_mode;  load file mode_name=$mode_name \n";

print FOUT ("fixed_link = $fixed_link\n");
#print FOUT ("fixed_link1 = $fixed_link1\n");

unless ($select_mode)
{
    print FOUT "FAILURE: selected mode  not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  selected mode not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
        die  "FAILURE:  selected mode  not supplied \n";
}
unless ($select_mode =~/^[12]/)
{
    print_3 ($name,"FAILURE: invalid selected mode ($select_mode)",$MERROR,1);
}
unless ($expt =~ /bn[qm]r/i || $expt =~ /pol/i  )
{
    print FOUT "FAILURE: experiment $expt not supported\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  experiment $expt not supported " ) ;
    unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
    die  "FAILURE:  experiment $expt not supported\n";
}

$select_mode =~ tr/A-Z/a-z/; # lower case

# make a few sanity checks:
if ($select_mode =~ /^2/ ) # Type 2 experiment 
{
    if ( $expt =~   /bn[qm]r/i  ) # bnqr now has PSM so more modes are allowed 
    {
	unless ($select_mode=~/[abcde0]$/  ) # modes 2a 2b 2c 2d 2e 20 exist
	{
	    print_3 ($name, " Unknown  mode  for $expt ($select_mode)",$MERROR, 1);
	}
    }
    else  # experiment pol
    { 
	unless ($select_mode=~/0$/  ) # mode 20 only exists
	{
	    print_3 ($name, " Unknown  mode  for $expt ($select_mode)",$MERROR, 1);
	}
    }
}
elsif ($select_mode =~ /^1/) # Type 1 experiment 
{
    if ( $expt =~  /bn[qm]r/i    ) 
    {
	unless ($select_mode=~/[abcefgjnd0]$/  )  # modes 1a 1b 1n 1f 1g 1c 1d 10 1j 1e (1e actually for BNQR only)
	{
	    print_3 ($name, " Unsupported  mode  for $expt ($select_mode)",$MERROR, 1);
	}
    }
    else
    {
	unless ($select_mode=~/[0ncfhd]$/  )  # modes 10 1n 1h 1c 1f 1d exist for POL
	{
	    print_3 ($name, " Unsupported  mode  for $expt ($select_mode)",$MERROR, 1);
	}
    }
}

# BNMR/BNQR support loading a file of defaults when changing mode
unless ( $expt =~  /bn[qm]r/i    )
{ 
    $mode_filename = "none";
}
else
{
    $mode_name =~ tr/A-Z/a-z/; # translate to lower case
# generate mode_filename 
    unless ($mode_name =~/none/i)
    {
	$mode_filename = "$select_mode"."_"."$mode_name.odb"; #concatenate
    }
    else
    {
	$mode_filename = $mode_name;
    }
    print_2 ($name,"mode_filename=\"$mode_filename\"; select_mode=$select_mode, 
mode_name=$mode_name",$CONT);
}

#
# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if($transition ) 
{ 
    print FOUT "Run is in transition. Try again later \n";
    print FOUT "Or set /Runinfo/Transition in progress to 0\n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "Run is in transition. Try again later or set /Runinfo/Transition in progress to 0 " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in transition. Try again later or set /Runinfo/Transition in progress to 0 " ;
}

if ($run_state != $STATE_STOPPED) 
{   # Run is not stopped; 
    print FOUT "Run is in progress. Cannot change experimental mode\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run in progress. Cannot change experimental mode" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in  progress. Cannot change experimental mode " ;
}

 
($status, $path, $key) = odb_cmd ( "ls","$input_path","Experiment name " ) ;
unless ($status)
{ 
    print FOUT "$name: Failure from odb_cmd (ls); Error getting $input_path experiment name \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE getting experiment name" ) ;
    die "$name: Failure getting $input_path experiment name  ";
}
($old_ppg, $msg) =get_string($key);

$old_ppg =~ tr/A-Z/a-z/; # lower case

print "Present ppg mode =$old_ppg\n";
print FOUT "Present ppg mode =$old_ppg\n";



# remove links from any of the other defined experiments if present
unless ($select_mode =~ /2a/)
{
    print FOUT "\n -------- removing link for 2a ------------\n";
    $status =  remove_a_link ("2a");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 2a\n"; 
    }
}
unless ($select_mode =~ /2b/)
{
    print FOUT "\n -------- removing link for 2b ------------\n";
    $status =  remove_a_link ("2b");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 2b\n"; 
    }
}
unless ($select_mode =~ /2c/)
{
    print FOUT "\n -------- removing link for 2c ------------\n";
    $status =  remove_a_link ("2c");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 2c\n"; 
    }
}
unless ($select_mode =~ /20/)
{
    print FOUT "\n -------- removing link for 20 ------------\n";
    $status =  remove_a_link ("20");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 20\n"; 
    }
}

unless ($select_mode =~ /2d/)
{
    print FOUT "\n -------- removing link for 2d ------------\n";
    $status =  remove_a_link ("2d");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 2d\n"; 
    }
}
unless ($select_mode =~ /2e/)
{
    print FOUT "\n -------- removing link for 2e ------------\n";
    $status =  remove_a_link ("2e");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 2e\n"; 
    }
}
unless ($select_mode =~ /1a/)
{
    print FOUT "\n -------- removing link for 1a ------------\n";
    $status =  remove_a_link ("1a");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1a\n"; 
    }
}
unless ($select_mode =~ /1b/)
{
    print FOUT "\n -------- removing link for 1b ------------\n";
    $status =  remove_a_link ("1b");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1b\n"; 
    }
}
unless ($select_mode =~ /1f/)
{
    print FOUT "\n -------- removing link for 1f ------------\n";
    $status =  remove_a_link ("1f");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1f\n"; 
    }
}
unless ($select_mode =~ /1g/)
{
    print FOUT "\n -------- removing link for 1g ------------\n";
    $status =  remove_a_link ("1g");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1g\n"; 
    }
}
unless ($select_mode =~ /1j/)
{
    print FOUT "\n -------- removing link for 1j ------------\n";
    $status =  remove_a_link ("1j");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1j\n"; 
    }
}
unless ($select_mode =~ /1n/)
{
    print FOUT "\n -------- removing link for 1n ------------\n";
    $status =  remove_a_link ("1n");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1n\n"; 
    }
}
unless ($select_mode =~ /1d/)
{
    print FOUT "\n -------- removing link for 1d ------------\n";
    $status =  remove_a_link ("1d");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1d\n"; 
    }
}
unless ($select_mode =~ /1c/)
{
    print FOUT "\n -------- removing link for 1c ------------\n";
    $status =  remove_a_link ("1c");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1c\n"; 
    }
}
unless ($select_mode =~ /10/)
{
    print FOUT "\n -------- removing link for 10 ------------\n";
    $status =  remove_a_link ("10");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 10\n"; 
    }
}
unless ($select_mode =~ /1e/)
{
    print FOUT "\n -------- removing link for 1e ------------\n";
    $status =  remove_a_link ("1e");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1e\n"; 
    }
}
unless ($select_mode =~ /1h/)
{
    print FOUT "\n -------- removing link for 1h ------------\n";
    $status =  remove_a_link ("1h");
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for expt 1h\n"; 
    }
}
print FOUT "\nFinished all remove-a-link calls \n";
write_time($name,$CONT);


print FOUT "\n -------- removing any spurious link for num cycles or num scans  ------------\n";
# make sure any spurious link is removed for num cycles/scans
$changing_mode = $FALSE;
($status) = change_link_num ($select_mode, $changing_mode);
if($status)
{
    print "$name: success after change_link_num,  status =$status\n";
}


# check if same expt. is already set up (check underscore though this should now be obsolete)
if($old_ppg eq $select_mode) # both are lower case
{ 
    print FOUT "\n -------- old and new modes are the same  ------------\n";
    print FOUT ("calling test_a_link with select_mode = $select_mode, fixed_link =$fixed_link\n");
    $status = test_a_link ($select_mode, $fixed_link);
    unless ($status) 
    { 
	print FOUT "\n -------- making a link for $select_mode  ------------\n";
###	$status =  make_a_link ($select_mode);
	$status = make_link("/ppg/ppg$select_mode",$fixed_link);
	unless($status) 
	{    
	    print FOUT "$name: Error return from make_link for /ppg/$select_mode\n"; 
	    die "$name: Error return from make_a_link for $select_mode\n ";
	}
    }
    else
    {
	print FOUT "$name: Experiment $old_ppg is set up already\n";
	print      "$name: Experiment $old_ppg is set up already\n";
	odb_cmd ( "msg","$MINFO","","$name","INFO: experiment $old_ppg is set up already");
    }
}
else
{
    print FOUT "\nOld ($old_ppg) and new ($select_mode) modes are not the same\n";
# remove the link /alias/PPG& ($fixed_link) "
{
    print FOUT "\n -------- removing old link \"$fixed_link\" ------------\n";
    $status =  remove_link ($fixed_link);
    unless($status) 
    {    # write a message to file and continue 
	print FOUT "$name: Error return from remove_a_link for \"$fixed_link\"\n"; 
    }
}
# make a new link for this experiment
    print FOUT "\n -------- making new link for $select_mode  ------------\n";
    $status =  make_link ("/ppg/ppg$select_mode",$fixed_link);
    unless($status) 
    {    # don't change the experiment name 
	print FOUT "$name: Error return from make_link\n"; 
	die "$name: Error return from make_link\n ";
    }


# load the file of default values for this mode if mode_filename is not "none"
    unless ($mode_filename =~/none/i)
    {
	print FOUT "\n---- loading default values for mode $select_mode from file \"$mode_filename\"-------\n";
	print "\n----loading default values for mode $select_mode from file \"$mode_filename\"-------\n";
	$status =  load_mode_defaults ($mode_filename);
	unless($status) 
	{  
	    print_2($name, "Error return from load_mode_defaults",$CONT); 
	    print_3($name, "WARNING: could not load mode defaults from file $mode_filename ",
		    $MINFO,$CONT); 
	}
	else
	{  
	    print_3($name, "INFO - mode default values from file $mode_filename have been loaded ",$MINFO,$CONT); 
	}
    }
    else
    {
	print FOUT "\n---- no default values for $select_mode will be loaded -------\n";
	print "\n---- no default values for $select_mode will be loaded -------\n";
    }

 
# POL does not use midbnmr
    if ( $expt =~   /bn[qm]r/i  )
    { 
# if a type 1, set no. of midbnmr regions to the default
	if ($select_mode =~ /^1/) # Type 1 experiment 
	{
	    $default = $midbnmr_regions;
	    if( ($select_mode =~ /^1g/) ||  ($select_mode =~ /^1j/)){ 
		$default = $midbnmr_1g_regions; # more regions for 1g,1j 
	    }
	    $default = $default +0; # make sure it's an integer 
	    
	    print FOUT "\n --- setting no. midbnmr regions to default ($default) for mode $select_mode  ----\n";
	    ($status) = odb_cmd ( "set","$midbnmr_path","number of regions" ,$default) ;
	    unless($status)
	    { 
		print FOUT "$name: Failure from odb_cmd (set); Error setting $midbnmr_path number of regions \n";
		odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting ...midbnmr/number of regions" ) ;
		die "$name: Failure setting  $midbnmr_path number of regions";
	    }
	}
    } # end of bnmr/bnqr only
   
    print FOUT "\n -------- update experiment name  ------------\n";
    ($status) = odb_cmd ( "set","$input_path","Experiment name" ,$select_mode) ;
    unless($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $input_path experiment name \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting experiment name" ) ;
	die "$name: Failure setting $input_path experiment name  ";
    }
    print FOUT "INFO: Success -  experiment (& ppg mode) $select_mode is now selected \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: experiment (& ppg mode) $select_mode is now selected " );
    print  "INFO: Success - experiment (& ppg mode)  $select_mode is now selected  \n";
    
    
    
# now check if the type has changed
    unless ( ( $select_mode =~ /^2/ && $old_ppg  =~ /^2/) ||  ( $select_mode =~ /^1/ && $old_ppg  =~ /^1/))
    {
	$changing_mode = $TRUE;
	print FOUT "\n -------- update /experiment/write data for different type   ------------\n";
	print  "\n -------- update /experiment/write data for different type   ------------\n";
	
	print "$name: experimental modes have changed ($old_ppg to $select_mode )\n";
	($status) = change_link ($select_mode);
	if($status)
	{
	    #print "$name: success after change_link,  status =$status\n";
	}
	print FOUT "\n -------- update /experiment/num scan/cycle for different type   ------------\n";
	print  "\n -------- update /experiment/num scan/cycle for different type   ------------\n";

	($status) = change_link_num ($select_mode, $changing_mode);
	if($status)
	{
	    print "$name: success after change_link_num,  status =$status\n";
	}
    }
}
write_time($name, 3);
exit;




























