#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# 
#
# invoke this with cmd e.g :
#                         include_path             experiment   beamline  
# musr_update.pl  /home/musrdaq/online/mdarc/perl      musr       dev
#
#
# $Log: musr_update.pl,v $
# Revision 1.5  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.4  2004/04/21 19:08:01  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.3  2004/04/08 17:43:18  suz
# add a comment
#
# Revision 1.2  2004/04/07 17:20:25  suz
#  allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.1  2002/04/14 03:46:03  suz
# original: updates histo bin params
#
#
#
# Purpose: called by mdarc to update bin parameters from values in musr area if necessary
#      
use strict;
sub update_array ($$$); # prototype
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
###########################################################################

#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $beamline ) = @ARGV;
our $len =  $#ARGV; # array length
our $name = "musr_update"; # same as filename
our $outfile = "musr_update.txt";
our $parameter_msg = "include_path, experiment ,   beamline";
our $nparam = 3;  # no. of input parameters
# these values are used by musr_update.pl to check return value and suppress output
# when opening the file.
our $status=5;
our $suppress=$FALSE;
##############################################################
# local parameters
my ($transition, $run_state, $path, $key);
my ($mdarc_path, $eqp_name, $musr_path);
my ($cmd);
my $debug=$FALSE;  # local debug
my $update_flag;
my ($val, $from_path, $to_path, $update);



$|=1; # flush output buffers


# Inc_dir needed because if script is invoked by browser it can't find the
# code for require (may never be needed for musr but keep all scripts similar)

unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
$suppress = $TRUE; # suppress messages
require "$inc_dir/init_check.pl";

unless ($status)
  {  # do not continue to attempt access to the odb on failure; ends up with many client unknowns to be killed
    print_3($name,"FAILURE: open_output_file could not access odb",$MERROR,$DIE);
  }

# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#
print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; beamline = $beamline \n";
#
#
# Does not matter whether data logger is running or not
# But automatic run numbering must be enabled
#
#


#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)  )
{
# BNMR experiments use equipment name of FIFO_ACQ
#  $eqp_name = "FIFO_ACQ";
    print FOUT "$name: no action for beamline $beamline \n";
    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO - no action for beamline $beamline" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die " no action for beamline $beamline \n";
}

# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
$mdarc_path= "/Equipment/$eqp_name/mdarc";
$musr_path = "/Equipment/$eqp_name/Settings";
print FOUT "mdarc path: $mdarc_path; musr_path: $musr_path\n";


## decided not to check, as this script will only be called
## from mdarc if we have data (therefore when run is going)

# check whether run is in progress

#($run_state,$transition) = get_run_state();
#if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 


#if ($run_state != $STATE_RUNNING)
#{
#    print FOUT "$name: no action as run is stopped \n";
#    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO - no action as run is stopped" ) ;
#    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
#    die " no action as run is stopped \n";
#}

# Run is going

$to_path = "$mdarc_path/histograms";
$from_path =   "$musr_path/mode/histograms";

$update_flag = $FALSE;
#  copies any elements across that have changed
$update_flag = update_array( $from_path, $to_path, "bin zero");
$update_flag = update_array( $from_path, $to_path, "first good bin");
$update_flag = update_array( $from_path, $to_path, "last good bin") ;
$update_flag = update_array( $from_path, $to_path, "first background bin");
$update_flag = update_array( $from_path, $to_path, "last background bin") ;

if ($update_flag) 
{ 
    if($debug) { print "$name: parameters have been updated\n";}
    print FOUT "$name: parameters have been updated\n";
}
else 
{  
    print "$name: no mdarc bin parameters required updating\n";
    print FOUT "$name:  bin parameters NOT updated\n";
}
exit;




sub update_array($$$)
{
# Copies elements of arrays across if they are not identical
#
# Input:   from_path  path of key from where array values are read
#          to_path    path of key to which array values are written
#          item       key name of array to be copied
#                      ( key name must be identical in each directory)   
#
# Output:  update_flag  set TRUE if any parameter needed to be updated
# 
    my $name = "update_array";
    my ($status, $path, $key) ;
    my ($val, $index, $update_flag);
    my $debug;
    my @from_array;

    my ($from_path, $to_path, $item)   = @_;
    unless ( $from_path && $to_path && $item)
    {
        print FOUT  "$name: bad or missing parameters supplied \n";
        ($status)=odb_cmd ( "msg","$MERROR","$name", " bad or missing parameters supplied");
        die      "$name: bad or missing parameters supplied\n ";
    }
    
    print FOUT "\n$name: update_array starting for $item \n";
    print FOUT "       from_path=$from_path \n to_path=$to_path  \n"; 

    $update_flag = $FALSE;
    
    ($status, $path, $key) = odb_cmd ( "ls","$from_path","$item","" ) ;
    unless($status)
    { 
        print FOUT "$name: Error reading key $from_path/$item\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading key $from_path/$item " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Error reading key $from_path/$item\n";
    }
    else 
    { 
        print FOUT "$name: Success -  read key $from_path/$item \n";
        if($debug) { print  "$name: Success -  read key $from_path/$item \n"; }
    }

    # $DEBUG=1;  #debug get_array
    get_array ($key, $ANSWER);  # returns array contents in global @ARRAY
    @from_array = @ARRAY;
    if($debug)
    {
        print "from_array: @from_array\n";
        print FOUT "from_array: @from_array\n";
        print "array length: $#from_array\n";
        print FOUT "array length: $#from_array\n";
    }

    ($status, $path, $key) = odb_cmd ( "ls","$to_path","$item","" ) ;
    unless($status)
    { 
        print FOUT "$name: Error reading key $to_path/$item\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading key $to_path/$item " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Error reading key $to_path/$item\n";
    }
    else 
    { 
        if($debug) { print FOUT "$name: Success -  read key $to_path/$item \n"; }
        print FOUT "$name: Success -  read key $to_path/$item \n"; 
    }
    # $DEBUG=1;  debug get_array
    get_array ($key, $ANSWER);  # returns array contents in global @ARRAY
    if($debug)
    {
        print "ARRAY:      @ARRAY\n";
        print FOUT "ARRAY:      @ARRAY\n";
        print "array length: $#ARRAY\n";
        print FOUT "array length: $#ARRAY\n";
    }
    if( $#from_array !=  $#ARRAY )
    {
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE - array lengths are not the same " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}        

        print FOUT "$name: ERROR - array lengths are not the same - from: ($#from_array) & to ($#ARRAY)  \n";
        die "$name: ERROR - array lengths are not the same  - from: ($#from_array) & to ($#ARRAY) \n";
    }

# $#ARRAY gives index of highest element
    for $index (0..$#ARRAY)
    {
        if($debug) { print ("array index  $index =  $ARRAY[$index]\n"); }
        $val = $from_array[$index];
        if ($val != $ARRAY[$index])
        {
#            if ($debug) { print "$name:updating element $index of array $item\n"; }
            print "$name:updating  mdarc's array \"$item [$index]\" to $val\n"; 
            print FOUT "$name:updating element $index of mdarc array $item to $val\n";

            ($status, $path, $key) = odb_cmd ( "set","$to_path","$item\[$index\]","$val" ) ;
            unless($status)
            { 
                print FOUT "$name: Error setting key $to_path/$item\[$index\]\n";
                ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading key $to_path/$item\[$index\] " ) ;
                unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
                die "$name: Error setting key $to_path/$item\[$index\]\n";
            }
            $update_flag = $TRUE;
        }
    }
    if($update_flag)
    {
#  'scalar @ARRAY'   gives the number of elements  
        if($debug) { print "$name: successfully updated array $item from $from_path to $to_path (",scalar @ARRAY, " elements) \n";}
        print FOUT "$name: successfully updated array $item from $from_path to $to_path (",scalar @ARRAY, " elements) \n";
    }
    else
    {
        if($debug) { print "$name: array $item did not need updating \n";}
        print FOUT "$name: array $item did not need updating \n";
    }
    return($update_flag);
}
    
    
    
    
