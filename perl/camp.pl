#!/usr/bin/perl -w
# above is magic first line to invoke perl
# type echo $? to get the status after running the perl
#
######################################################################
# Perlscript executed by fe_camp to check validity of camp paths etc.
#
#  $Log: camp.pl,v $
#  Revision 1.15  2008/04/17 21:36:24  suz
#  add a timeout
#
#  Revision 1.13  2004/11/12 01:47:21  asnd
#  Handle simple 0/1 status from camp_cmd
#
#  Revision 1.12  2004/06/10 18:17:17  suz
#  add beamline param; require init_check; use our; last Rev message is bad
#
#  Revision 1.11  2004/06/09 21:36:23  suz
#  add check on beamline
#
#  Revision 1.10  2004/04/21 19:12:36  suz
#  add a space in search string to fix bug for beamlines m15/m20
#
#  Revision 1.9  2004/04/08 17:47:58  suz
#  add a comment
#
#  Revision 1.8  2004/03/29 18:26:17  suz
#  allows only one copy to run; open_output_file now appends to message file
#
#  Revision 1.7  2004/01/14 19:49:19  suz
#  mdarc/camp_hostname now in  mdarc/camp/camp_hostname for bnmr/bnqr (same as MUSR)
#
#  Revision 1.6  2003/10/03 06:12:34  asnd
#  Merge fixes made to 1.4 for M15 (August) with new changes now (M20, Sept),
#  all with version 1.5 changes hiding in CVS.
#
#  Revision 1.5  2003/06/25 00:48:56  suz
#  remove error if not set pollable
#
#  Revision 1.4  2003/01/16 20:55:26  suz
#  add path to require odb_access
#
#  Revision 1.3  2003/01/08 01:43:04  suz
#  add code to return numerical value from camp_cmd
#
#  Revision 1.2  2002/05/08 01:15:14  suz
#  add require path as first parameter
#
#  Revision 1.1  2002/04/29 17:27:45  suz
#  version 1.4 moved from bnmr1/clients to perl directory
#
#  Revision 1.4  2001/05/09 17:34:24  suz
#  Change odb command msg to imsg to avoid speaker problem
#
#  Revision 1.3  2001/05/01 21:29:49  suz
#  minor change
#
#  Revision 1.2  2001/04/26 17:52:51  suz
#  Fix use lib name
#
#  Revision 1.1  2001/04/25 19:23:43  suz
#  Added to cvs
#
# 
######################################################################
#                             Input Parameters:   
#           include                    experiment    equipment     beamline  timeout(s)  
#           directory                                  name
# camp.pl  /home/musrdaq/musr/perl      musr      musr_td_acq         dev      
# camp.pl  /home/bnmr/online/perl       bnmr      fifo_acq            bnmr      5
# An output file (opened on FOUT) is used for informational messages

     
#
use warnings;
use strict;
use Config;   # Trap signals
##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
# these are constants 
#status = 2304 is success for camp
my $CAMP_SUCCESS = 2304;
my $CAMP_VAR_ATTR_POLL = 8; # these are in camp.h
my $CAMP_IF_ONLINE = 1 ; 
# status = 0 is success for odb
# paths
my $CAMP_ODB_PATH = "/equipment/camp/settings";
my $MDARC_PATH =  " "; # "/equipment/$eqp_name/mdarc"
##########################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
#
our ( $inc_dir, $expt, $eqp_name, $beamline, $timeout ) = @ARGV;# input parameters
our $name = "camp"; # same as filename
our $len =  $#ARGV; # array length
our $nparam = 4; # need 4 input parameters (can be 5)
our $parameter_msg = "include_path, experiment , equipment name, beamline, [timeout] ";
our $outfile = "camp.txt";
#############################################
# local variables
my ($status,$path,$key);  # for call to odb_cmd
my ($camp_status, $camp_path, $camp_answer); # for camp_cmd
my ($camp_host,$n_camp);
my (@poll_int, $poll_int, $pollint);
my (@camp_path_array, $camp_path_array);
my ($junk,$elem, $i, $command, $msg);
my ($units,$title);
my $camp_cmd ="/home/musrdaq/musr/camp/linux/camp_cmd";
my $debug=0; # local debug
my $return_val;
my $default_timeout=10; # 10seconds timeout unless $timeout supplied
####################################################################
#
# TRAPPING SIGNALS
#
# ctrl-C is TERM, the default kill signal
# HUP usually is used for re-reading config files
# IOT - I/O trap I think
# SEGV - segment violation, memory error
# PIPE - reading/writing to/from pipes
# ILL - illegal instruction I think
# usually just trap TERM - people & "reboot" usually use TERM
# or KILL, which is untrappable. Unless you're using pipes
# and need to reap child processes

#my %sig_num;
#my @sig_name;

$|=1; # flush output buffers

our %SIG;

$SIG{'ALRM'} = "timed_out" ;

#$SIG{'TERM'} = "clean" ; # declare signal handler
#$SIG{'INT'} = "clean" ; # declare signal handler




# if $timeout not supplied, use default
unless ($timeout) {$timeout = $default_timeout;}


# this eval is the timeout wrapper around a function:
# we don't need it if we are just exiting when times out
#eval 
#{
alarm($timeout) ;    # Timeout in seconds (int) on this whole script


if($DEBUG) { $debug=1;}


unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl";

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";


# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null


print FOUT    "$name  starting with parameters:  \n";
print FOUT    "     Experiment = $expt;  Equipment name = $eqp_name  \n";
print     "$name  starting with parameters:  \n";
print     " Experiment = $expt;  Equipment name = $eqp_name; Timeout=$timeout  \n";


# Check parameters
unless ($eqp_name) 
{
    print FOUT "$name: Equipment name is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Equipment name not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 

    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n";     
    die      "Equipment name is not supplied";
}

$MDARC_PATH =  "/equipment/$eqp_name/mdarc"; # assign global MDARC_PATH
if ( $eqp_name=~/MUSR_I_/i)
{
    $MDARC_PATH =  "/equipment/musr_td_acq/mdarc"; # assign global MDARC_PATH as TD equipment
}
if ($debug) { print   "MDARC_PATH : $MDARC_PATH\n";}

# ------------------------------------------------------------
# get number of camp variables
# ------------------------------------------------------------
($status, $path, $key) = odb_cmd ( "ls"," $CAMP_ODB_PATH ",  "n_var_logged " ) ;
unless ($status) 
{ 
    print  FOUT "$name: Error reading $path/$key from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read number of camp variables logged from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: Error reading $path/$key from odb\n";
}
$n_camp= get_int ( $key);

print FOUT  "Number of camp variables logged : $n_camp\n"; 
if ($debug) { print "Number of camp variables logged : $n_camp\n"; }
# TEMP for Testing
##print "TEMP Setting n_camp to 1\n";
##$n_camp =1;
unless ($n_camp)
{
    print  FOUT "$name: No camp variables are to be logged (no checks done)\n";
    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO: No camp variables are to be logged" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print  "$name: No camp variables are to be logged (no checks done) \n";
    exit;
}
# ------------------------------------------------------------
# get camp host name
# ------------------------------------------------------------

($status, $path, $key) = odb_cmd ( "ls"," $MDARC_PATH/camp",  "camp hostname " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading camp host name from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read camp host name from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading camp host name from odb";
}
($camp_host, $msg) = get_string($key);
print FOUT "Camp hostname:  $camp_host \n";
print      "Camp hostname:  $camp_host \n";


# ------------------------------------------------------------
# get the camp paths
# ------------------------------------------------------------
($status, $path, $key) = odb_cmd ( "ls"," $CAMP_ODB_PATH ",  "var_path " ) ;
unless ($status) 
{ 
    print FOUT  "$name: Error reading $path/$key from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read camp variable paths from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: Error reading $path/$key from odb\n";
}
# process  $ANSWER into $camp_path_array
&get_array($key);  # fills ARRAY

@camp_path_array=@ARRAY; # transfer contents
#    print      "camp paths:  = @camp_path_array\n";

# the first $n_camp are valid (#array = last index)
$#camp_path_array=$n_camp - 1 ;
if ($debug) { print      "Valid camp paths : @camp_path_array\n";}
# ------------------------------------------------------------
# get the camp polling intervals
# ------------------------------------------------------------
($status, $path, $key) = odb_cmd ( "ls"," $CAMP_ODB_PATH ",  "polling_interval " ) ;
unless ($status) 
{ 
    print FOUT  "$name: Error reading $path/$key from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read camp polling interval from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: Error reading $path/$key from odb\n";
}
# process  $ANSWER into $pol_int
&get_array($key);  # fills ARRAY

@poll_int=@ARRAY; # transfer contents
#print"polling interval array  = @poll_int\n";

# the first $n_camp are valid (#array = last index)
$#poll_int=$n_camp - 1 ;
if  ($debug) 
{
    print      "Valid polling intervals: @poll_int\n";
    print      "\n";
    print      "About to check camp paths:\n";
}


#----------------------------------------------------------------------
# Access CAMP
#----------------------------------------------------------------------
if  ($debug)  { print "Info - accessing camp for each path...\n"; }

for ($i=0; $i<$n_camp; $i++)  # main loop on no. of camp variables to poll
{
    print FOUT "Accessing camp path $i = $camp_path_array[$i] ... \n\n";
    print      "Accessing camp path $i = $camp_path_array[$i] ... \n\n";

    $camp_path=$camp_path_array[$i];
    if($debug)
    {
	$command = " -node $camp_host \"varGetIdent $camp_path\" "; 
	print "command = $command\n";
    }
# check the path
    $camp_answer=`$camp_cmd -node $camp_host \"varGetIdent $camp_path\"`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    $return_val=check_camp_status($camp_status,$camp_answer);
#    print("return val = $return_val\n");
    if ($camp_status && $camp_status != $CAMP_SUCCESS)
    {
        print FOUT "Camp variable $camp_path not found. Check the path is correct \n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Camp variable $camp_path not found" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
        die "Camp variable $camp_path not found. Check the path is correct \n";
    }

    if  ($debug) { print "Info: path  $camp_path has been checked \n";}
    if (0) 
    {       # don't do this any more - not needed
# read the title from camp
	$camp_answer=`$camp_cmd -node $camp_host \"varGetTitle $camp_path\"`;
	$camp_status=$?;
	chomp $camp_answer;  # strip trailing linefeed
 	$return_val = check_camp_status($camp_status,$camp_answer);
	print("return val = $return_val\n");
	if ($camp_status && $camp_status != $CAMP_SUCCESS)
	{
	    print FOUT "Could not get the title for $camp_path \n";
	    die "Could not get the title for $camp_path \n";
	    
	}
	$title=$camp_answer;
	if ($debug) {  print "Read the title from camp for $camp_path : $title\n";}
	
# write the title to odb
	($status) = odb_cmd ( "set"," $CAMP_ODB_PATH ",  "title[$i]", "$title" ) ;
	unless ($status) 
	{ 
	    print  FOUT "$name: Error setting odb key $CAMP_ODB_PATH/title[$i] to $camp_answer \n";
	    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not set title in odb for $camp_path" ) ;
	    unless ($status) { print  "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "$name: Error setting odb key $CAMP_ODB_PATH/title[$i] to $camp_answer \n";
	}
	
# read the units from camp
	$camp_answer=`$camp_cmd -node $camp_host \"varNumGetUnits $camp_path\"`;
	$camp_status=$?;
	chomp $camp_answer;  # strip trailing linefeed
	$return_val=check_camp_status($camp_status,$camp_answer);
	print("return val = $return_val\n");
	if ($camp_status && $camp_status != $CAMP_SUCCESS)
	{
	    die "Could not get the units for $camp_path \n";
	    
	}
	$units = $camp_answer;
	if ($DEBUG) { print "received answer from camp for $camp_path units: $units\n";}
	
# write the units to odb
	($status) = odb_cmd ( "set"," $CAMP_ODB_PATH ",  "units[$i]", "$units" ) ;
	unless ($status) 
	{ 
	    print FOUT "$name: Error setting odb key $CAMP_ODB_PATH/units[$i] to $units \n";
	    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not set units in odb for $camp_path" ) ;
	    unless ($status) { print  "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "$name: Error setting odb key $CAMP_ODB_PATH/units[$i] to $units \n ";
	}
	
	print FOUT "Wrote title $title and units $units to odb for path $camp_path\n";
    } # end of if (0)

# check if this path is  pollable ;
    if  ($debug) 
    {        
	print "****   Info: checking if path is pollable \n";
	$command = " -node $camp_host \"varGetAttributes $camp_path\" ";
	print "command = $command\n";
    }
    $camp_answer=`$camp_cmd -node $camp_host "varGetAttributes  $camp_path "`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    $return_val=check_camp_status($camp_status,$camp_answer);
    if ($debug)
    { 
	print("camp_answer = $camp_answer;  return val = $return_val\n");  # numerical part of camp_answer
    }
    if ($camp_status && $camp_status != $CAMP_SUCCESS)
    {
        print FOUT "Error return from camp while checking if $camp_path is pollable \n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: checking if $camp_path is pollable" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
        #  NEVER SAY die ...
    }
    $return_val = $return_val & $CAMP_VAR_ATTR_POLL; # return_val = numerical part of the answer

    if($debug) { print("CAMP_VAR_ATTR_POLL = $CAMP_VAR_ATTR_POLL.  AND with $return_val ->  return_val becomes $return_val\n");}
    if(!$return_val) 
    {
        print FOUT "$name:  Warning - camp variable $camp_path is NOT pollable\n";
        print  "$name:  Warning - camp variable $camp_path is NOT pollable\n";
        ($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING: Camp variable $camp_path is NOT pollable" ) ;
	# Never say die ...
    }
    else 
    {   # Camp variable is pollable
	print FOUT "$name: INFO: camp variable $camp_path IS pollable\n";
	print "$name: INFO: camp variable $camp_path IS pollable\n";
#               Check polling interval

	if  ($debug) { print "reading polling interval\n"; }
	$camp_answer=`$camp_cmd -node $camp_host "varGetPollInterval $camp_path"`;
	$camp_status=$?;
	chomp $camp_answer;  # strip trailing linefeed
	$return_val=check_camp_status($camp_status,$camp_answer);
	if($debug) { print("numerical return val = $return_val\n"); }
	if ($camp_status == 0 || $camp_status == $CAMP_SUCCESS)
	{
	    if  ($debug) {  print "Read back polling interval for $camp_path : $return_val seconds \n"; }
	}
	else
	{
	    print"Warning - problem reading polling interval for $camp_path \n";
	}

#       Use polling intervals from odb 
	$pollint=$poll_int[$i];
    
	if($pollint < 0.5 || $pollint > 20.0)
	{
	    print "WARNING: you have selected a polling interval for path $camp_path outside limits [0.5-20]\n";
	    print FOUT "WARNING: you have selected a polling interval for path $camp_path outside limits [0.5-20]\n";
	    if($return_val > 0.5 && $return_val < 15.0)
	    {
		print "The polling interval will remain at present value of $return_val seconds\n";
		print FOUT "The polling interval will remain at present value of $return_val seconds\n";
		$pollint = $return_val ;
	    }
	    else
	    {
		if($pollint < 2.0 ) {
		    $pollint = 2.0 ;
		}
		if($pollint > 20.0 ) {
		    $pollint = 20.0 ;
		}
		print "The polling interval will be set to $pollint seconds\n";
		print FOUT "The polling interval will be set to $pollint seconds\n";
	    }
	    ($status)=odb_cmd ( "msg","$MINFO", "","$name", 
				"WARNING: Invalid polling interval in odb for $camp_path. A value of $pollint seconds will be used ") ;
	}
	if  ($debug) { print "Polling interval will be set to $pollint seconds.\n";}
        
	unless( $pollint == $return_val)  # set polling interval if it is not already correct
	{  
	    print FOUT " Setting polling interval to $pollint for $camp_path\n";
	    $camp_answer=`$camp_cmd -node $camp_host "varSet $camp_path -p on -p_int $pollint "`;
	    $camp_status=$?;
	    chomp $camp_answer;  # strip trailing linefeed
	    $return_val=check_camp_status($camp_status,$camp_answer);
	    
	    if ($camp_status && $camp_status != $CAMP_SUCCESS)
	    {
		print FOUT "camp.pl: Failure attempting to set polling interval to $pollint for camp path $camp_path\n";
		($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE attempting to set polling interval $pollint for $camp_path" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
		die "camp.pl: Failure attempting to set polling interval to $pollint for camp path $camp_path\n";
	    }
	}
    } # end of else (camp variable is pollable) block


    
# check the instrument is online
#  magic incantation to get 1st element of path 
    ($junk,$elem) = split("/",$camp_path);
    $elem = "/$elem";
    if($debug)
    {
	print " First element of path is elem=$elem \n";
	print "Checking if instument is online \n";
	$command = "camp_cmd -node $camp_host \"insGetIfStatus $elem\" "; 
	print "command = $command\n";
    }
    $camp_answer=`$camp_cmd -node $camp_host "insGetIfStatus  $elem"`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    if($debug) { print " camp_answer=$camp_answer \n"; }
    $return_val=check_camp_status($camp_status,$camp_answer);
    if ($debug) 
    { 
	print "camp_status = $camp_status \n";
	print "camp_answer = $camp_answer \n";
	print("return val = $return_val\n");   # numerical value
    }
    
    if ($camp_status == 0 || $camp_status == $CAMP_SUCCESS)
    {
        if ($return_val != 1)
        {
            print FOUT "WARNING: CAMP  instrument $elem is offline\n";
            print FOUT "Use CAMP to set instrument online\n";
            ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Camp instrument is offline. Use CAMP to set it online" ) ;
            unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
            
            print "WARNING: the CAMP instrument $elem is offline\n";
            die "Use CAMP to set instrument online\n";
        }
	else { print "Instrument $elem is online\n" ; }
    }
    else
    {
	print FOUT "$name: Error while checking if instrument $elem  is online\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:  Error while checking if instrument $elem  is online" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
	die "$name: Error while checking if instrument $elem  is online\n";
    }    
    print "\n";
}  # end of loop on all camp paths

print FOUT "Successful exit from camp.pl\n";
($status)=odb_cmd ( "msg","$MINFO", "","$name", "SUCCESS: Camp perl script exiting after checking camp variables" ) ;
unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }

#alarm 0 ;  # end of timeout handler (eval)
#} ;


if ($@) { # $@ contains errors from last eval, undef if all is well
#    die unless $@ eq "alarm\n";   # propagate unexpected errors
    if ( $@ =~ /alarm/) {
	print "Operation timed out after $timeout seconds\n";
    }
else 
{ die "Error condition: $@\n"; }
}

exit;  


##############################################################
sub get_yn
###############################################################
{
#   input:  string to ask question
#   output: return value is TRUE or FALSE depending on $ANSWER
#
    my $ans;
    my $string= $_[0];
    my $status;

    while($TRUE)
    {
        print "$string?\n";
        $_=<STDIN>;
        $status=$?;
        print "status = $status\n";
        tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
        ($ans)=split / /;
        if ( $ans=~/^y/i || $ans=~/^t/i || $ans=~/^1/ ) 
        {
            print "get_yn finds yes \n";
            return ($TRUE);
        }
        elsif  ( $ans=~/^n/i || $ans=~/^f/i || $ans=~/^0/ ) 
        {
            print "get_yn finds no \n";
            return ($FALSE);
        }
        else
        {
            print "get_yn finds invalid reply\n";
        }
    }
}

sub check_camp_status
{
# Checks the status of last camp command, looks for "invalid" and returns any numerical value or -1
#
# Input parameters:
#      $camp_status
#      $camp_answer

# Returns any numerical value found  or -1 if no numerical value

    my $camp_status = shift; # get the input parameters 
    my $camp_answer = shift; 
    my @fields;
    my $len;
    my $i;

    if($debug)
    {
	print("check_camp_status: camp_answer = \"$camp_answer\", camp_status=$camp_status\n"); 
    }
    if($camp_status && $camp_status != $CAMP_SUCCESS) 
    {
        print STDERR "camp.pl: error status from camp_cmd: $camp_status\n";
    } 
    if($camp_answer=~/^invalid/i)
    {
        print STDERR "camp.pl: camp_cmd returns message: $camp_answer \n";
    } 

#   Ouch!  The multi-field answers from camp_cmd came from debugging statements that
#   were accidentally left active (displaying server name and command).  We should
#   get rid of the following fields.

    @fields=split("\n",$camp_answer);
    $len = $#fields; # array length
    #print("No. of fields found: $len+1\n");
      
    if($debug)
    {
	for ($i=0; $i<$len+1; $i++)  
	{
	    print "field $i: $fields[$i]\n";
	}
    }
# Any numerical reply seems to be in field[$len];
    $fields[$len] =~ /([\d]+)$/; # find digits anchored to end of string -> $1 
    if (defined $1)
    {
	if($debug){ print ("check_camp_status: Returning numerical return value: $1\n"); }
	return $1;
    }
    else
    {
# No numerical value anchored to the end of the string
	if($debug) { print ("check_camp_status: no numerical return value found\n"); }
	return -1;
    }
}

##sub clean {
  # signal handler routine
   
##    if (stat (FOUT) ) 
##    {
##	print "FOUT is opened\n";
##	print_3($name,  "INFO: $name trapped a signal SIG@_",$MINFO,$CONT);
##	close(FOUT) ;  # close output file
##    }
##    else 
##    {
##	print "FOUT is NOT opened\n";
##	print ("$name trapped a signal SIG@_  \n");
##    }
##  exit 2 ; # returns exit status 2 to caller
##}


sub timed_out {
#  
    if (stat (FOUT) ) 
    {
	print "FOUT is opened\n";
	print_3($name,  "INFO: $name timed out after $timeout seconds",$MINFO,$CONT);
	close(FOUT) ;  # close output file 
    }
    else 
    {
	print "FOUT is NOT opened\n";
	print "$name timed out after $timeout seconds\n" ;
    }
    exit 1 ;   # returns exit status 1 to caller
}
