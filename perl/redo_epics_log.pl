#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# invoke this script with cmd
#
#                     include path                experiment     
#                                               
# redo_epics_log.pl  /home/bnmr/online/perl    bnmr/bnqr 
#
# $Log: redo_epics_log.pl,v $
# Revision 1.1  2008/04/19 05:14:50  suz
# original resetepicslog button calls this
#
use strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
##########################################################################
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3  (print_3 now handles no FOUT)
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################

my $name="redo_epics_log";
our ($inc_dir, $expt ) = @ARGV;# input parameters
my ($command,$exp,$ans,$status,$answer);
my $mode=4; # write 4 to $path
our $parameter_msg = "include_path, experiment";
our $nparam = 2;  # no. of input parameters
our $len = $#ARGV; # array length
my $ival;
my $path="/equipment/epicslog/settings/";
my $epics_ok="epics ok";
my $key;
$|=1; # flush output buffers
# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

$len++;
unless ($len==$nparam)
{
    print_3 ($name,"not enough args ($len); enter $parameter_msg",$MERROR,$DIE);
}

unless($expt =~ /bn[qm]r/i)   
{
    print_3 ($name,"experiment must be bnmr or bnqr",$MERROR,$DIE);
}
$EXPERIMENT = $expt;  # set this up for odb_access

#my $string = "\'/equipment/nepicslog/settings/epics ok\' $mode";

#print "expt:$expt\n";
#print "string:$string\n";

($status) = odb_cmd ( "set","$path","$epics_ok" ,"$mode") ;
unless($status)
{ 
    print_3 ($name,"Failure setting $path$epics_ok to $mode",$MERROR,$DIE);
    exit;
}
else 
{ 
    print_3 ($name,"Success - $epics_ok is set to 4 (redo epics)",$MINFO,$CONT); 
}

($status,$path,$key) = odb_cmd ( "ls","$path","$epics_ok" );
unless  ($status) { die "Failure accessing $path/$key from odb_cmd\n"; }
$ival = get_int ( $key);
print ("$name: ival=$ival\n");
if($ival != $mode)
{  print ("$name: looks like hotlink has noticed epics ok set to $mode and reset it back to $ival\n"); }
else
{   print ("$name: hotlink on epics ok in mheader not currently operational\n"); }

##$command = "ls '/equipment/epicslog/settings/epics ok'"; 
#$command = "\"set $string\"";
##$ans ="`/home/midas/linux/bin/odbedit -e $expt -c $command` ";
#$ans ="`odb -e $expt -c $command` ";
#print "ans=$ans\n";
# $status=$?;
#    chomp $ans;  # strip trailing linefeed
#print"answer: $ans ($status)\n";

exit;
