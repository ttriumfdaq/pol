#!/usr/bin/perl 
# above is magic first line to invoke perl
#
#     delete_empty_mid_file.pl
#
############################################################################***
#                                                                             #
#          This program should be invoked by start-all or run number checker  #
#                                                                             *
############################################################################***
#
#
# invoke this with cmd        
#   get_next_run_number.pl
#
#                          Input Parameters:   
#                          include directory        experiment    equipment name   delete flag  failed rn beamline                           
# delete_empty_mid_file.pl /home/bnmr/online/perl    bnmr            FIFO_acq         1/0         -1       bnmr
#                                                                                           or e.g. 34213
#
# if delete is 1, the unlink is actually done. Can be set to 0 for testing
#
#  this script will read current run_number & saved directory from odb 
#
# Output goes into file $outfile
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $next_run      next run number   - writes this to odb location for communication
#
# $Log: delete_empty_mid_file.pl,v $
# Revision 1.9  2005/02/22 19:44:02  suz
# add POL
#
# Revision 1.8  2004/06/10 18:03:43  suz
# add parameter beamline; require init_param.pl; use our; last Rev message is bad
#
# Revision 1.7  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.6  2004/03/29 18:29:08  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.5  2003/12/01 23:16:49  suz
# try to improve this; but it is not used at present
#
# Revision 1.4  2003/05/21 17:14:11  suz
# empty file size is now 0; supports BNMR and BNQR
#
# Revision 1.3  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.2  2003/01/08 18:26:46  suz
# change a message
#
# Revision 1.1  2002/07/11 21:50:10  suz
# original
#
#
#
use_strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
############################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ( $inc_dir, $expt, $eqp_name, $delete, $failed_rn, $beamline) = @ARGV;
our $len =  $#ARGV; # array length
our $name = "delete_empty_mid_file"; # same as filename
our $nparam = 6; # need 6 input parameters
our $parameter_msg = "include_path,  experiment, equipment name, delete_flag, failed run number beamline ";
our $outfile = "delete_empty_mid_file.txt";
#######################################################################
# local variables
my $run_state;
my $debug =  $FALSE;
my ($age,$filename);
my ($run_number, $saved_dir ); # read directly from odb
my ( $path, $key, $status);
my $next_run;
######################################################################
$|=1; # flush output buffers

print " $inc_dir, $expt, $eqp_name, $delete, $failed_rn, $beamline\n";

unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
#require "$inc_dir/run_number_check.pl";
require "$inc_dir/odb_access.pl";
#require "$inc_dir/set_run_number.pl";

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

#
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null



print FOUT    "$name  starting with parameters:  \n";
print FOUT    " Include_dir = $inc_dir; Experiment = $expt; equipment name = $eqp_name; delete_flag $delete; failed rn=$failed_rn\n";  

#
#          Check the parameters
#
 unless(  ($EXPERIMENT  =~ /bn[mq]r/i || $EXPERIMENT =~ /pol/i) )
{
    print FOUT "$name does not support experiment type: $EXPERIMENT \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Experiment type $EXPERIMENT not supported " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name does not support experiment type: $EXPERIMENT \n";
}
unless ($eqp_name) 
{
    print FOUT "$name: Equipment name is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Equipment name not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    die      "Equipment name is not supplied";
}

$delete = $delete + 0;

unless ( ($delete) || $delete == 0 ) # specifically detect delete=0 
{
    print FOUT "$name: Delete flag is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Delete flag not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    die      "Delete flag is not supplied";
}
unless ($delete ) { print "Delete flag is false - no files will actually be deleted\n";}

$failed_rn = $failed_rn + 0;
unless ( $failed_rn ) 
{
    print FOUT "$name: run number of failed run is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Failed run number not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    die      "Failed run number  is not supplied";
}
unless ($failed_rn > 0 ) { print "$name: Last run did not fail\n";}
else { print "$name: Last run failed : $failed_rn\n";}
 
# if run is not stopped, do nothing

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going
    print_3($name,"Run is in progress; no action", $MINFO ,1);
}
#
#    
#              Read parameters from the odb
#
#
#

#   Read the  MLOGGER saved data directory from odb (same as that in mdarc area)
($status, $path, $key) = odb_cmd ( "ls"," /logger",  "data dir " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading saved data directory from Midas logger area in odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read Midas logger saved data directory from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading odb key \"/logger/data dir\"";
}
($saved_dir, $message) = get_string ($key);
print FOUT "$name: after get_string, saved_dir=$saved_dir\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }



print FOUT  "$name: Current directory for Midas saved files = $saved_dir\n";
unless ($saved_dir)
{
    # This is an ERROR condition. Saved_dir must be supplied.
    print FOUT " Saved directory is a blank string \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Saved directory is a blank string " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "FAILURE: Saved directory is a blank string."; 
}


# Check we have  write access to the saved directory (or mdarc/mlogger will fail to write files to it)
unless (-w $saved_dir ) 
{ 
    $process= getpwuid($<);
    print FOUT "WARNING: this process (username=$process) cannot write to saved data directory: $saved_dir \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "WARNING :This process (username=$process) cannot write to  $saved_dir " ) ; 
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }  
} 

#
#    
#              Read more parameters from the odb
#
#
#
# read the current run number
#
($status, $path, $key) = odb_cmd ( "ls"," /Runinfo"," /Run number  " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading run number from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read run number from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading run number from odb";
} 
$run_number = get_int ( $key);
print FOUT  "$name: Current run number = $run_number\n";

#
# read the current filename
#
($status, $path, $key) = odb_cmd ( "ls"," /logger/channels/0/settings"," /current filename  " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading current saved filename from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read current saved filename from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading current saved filename from odb key \"/logger/channels/0/settings\"";
} 
($filename, $message) = get_string ($key);

print FOUT "$name: after get_string, filename = $filename\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }


unless ( chdir ("$saved_dir"))
{
    print FOUT  "$name: FAILURE - cannot change directory to $saved_dir\n";
    ($status)=odb_cmd ( "msg","$MERROR", "", "$name: FAILURE - cannot change directory to $saved_dir");
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: FAILURE - cannot change directory to $saved_dir";
}

print FOUT "$name: changed directory to  $saved_dir\n";

$next_run = $run_number + 1; # run number may have been reset to one less by run number checker, expection mlogger
                             # to delete empty file.
if( $filename =~ /$run_number/)
{
    print "$name: Expected filename $filename is for current run number\n";
    print FOUT "$name: Expected filename $filename  is for current run number\n";    
}
elsif( $filename =~ /$next_run/)
{
    print "$name: Expected filename $filename will be for next run number ($next_run) \n";
    print FOUT "$name: Expected filename   $filename  will be for next run number ($next_run)\n";
}
else
{
   print "$name: exiting since expected file $filename is not the same as the run number $run_number\n";
   print "       no action needed\n";
   print FOUT "$name: exiting since expected file $filename is not the same as the run number $run_number; no action needed\n";
   exit;
}

if($filename =~ /$failed_rn/)
{
#    FAILED  LAST RUN 
    print "$name: expected filename  $filename is the same as failed run number ($failed_rn)\n";
    print FOUT "$name: expected filename  $filename is the same as failed run number ($failed_rn)\n";
# check the size (and other attributes)  of this midas file
    if ( -e $filename ) # if file exists
    {
	$age = ( -M $filename);
	print   "$name:  file $filename:  modification age in days: $age\n"; 
	print FOUT  "$name:  file $filename:  modification age in days: $age\n"; 
	
	
	$size = (-s $filename);
	print "$name: file $filename exists (and has size $size)\n";
	print FOUT "$name: file $filename exists (and has size $size)\n";
	if($size ==0)
	{
	    print_3($name,"Not deleting saved file $filename for failed run $failed_rn (size=0)",$MINFO,0);
	    exit;
	}	
	if ($delete)
	{
	    unless (  unlink ($filename))
	    {
		print FOUT "$name: failure deleting saved file  $filename in directory $saved_dir for failed run $failed_rn\n";
		($status)=odb_cmd ( "msg","$MERROR","","$name", "Could not delete failed run $failed_rn saved file $filename" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		unless ( -w $filename) 
		{
		    printf FOUT "$name: this process does not have write permission to file\n";
		    printf  "$name: this process does not have write permission to file\n";
		}
		die "$name: failure deleting empty saved file  $filename in directory $saved_dir";
	    }
	    else 
	    { 
		print FOUT "$name: Successfully deleted saved  file $filename for failed run $failed_rn\n"; 
		print  "$name: Successfully deleted saved  file $filename for failed run $failed_rn\n"; 
		($status)=odb_cmd ( "msg","$MINFO","","$name", "Successfully deleted saved file $filename for failed run $failed_rn" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    }
	}
	else
	{
	    print_3($name,"Would have deleted saved file $filename for failed run $failed_rn (delete flag is false)",$MERROR,0);
	}
    }
    else
    {
	print_3($name,"Expected saved file $filename for failed run $failed_rn does not exist; no file found",$MERROR,0); 
    }
    exit;
}


# check the size (and other attributes)  of this midas file
if ( -e $filename ) # if file exists
{
    $age = ( -M $filename);
#    print   "$name:  file $filename:  modification age in days: $age\n"; 
    print FOUT  "$name:  file $filename:  modification age in days: $age\n"; 
    

    $size = (-s $filename);
#    print "$name: file $filename exists (and has size $size)\n";
    print FOUT "$name: file $filename exists (and has size $size)\n";
    


    if ( $size <=0 ) # size of empty file
    {

	if ($debug) { print "$name: File $filename is empty and will be deleted \n"; } # midas logger will overwrite this
	print FOUT "File $filename is empty and will be deleted \n"; # midas logger will overwrite this

# Check if we actually want to delete the file
	if ($delete)
	{
	    unless (  unlink ($filename))
	    {
		print FOUT "$name: failure deleting empty saved file  $filename in directory $saved_dir\n";
		($status)=odb_cmd ( "msg","$MERROR","","$name", "Could not delete empty saved file $filename" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		unless ( -w $filename) 
		{
		    printf FOUT "$name: this process does not have write permission to file\n";
		    printf  "$name: this process does not have write permission to file\n";
		}
		die "$name: failure deleting empty saved file  $filename in directory $saved_dir";
	    }
	    else 
	    { 
		print FOUT "$name: Successfully deleted empty saved  file $filename\n"; 
		print  "$name: Successfully deleted empty saved  file $filename\n"; 
		($status)=odb_cmd ( "msg","$MINFO","","$name", "Successfully deleted empty saved file $filename" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    }
	}
	else
	{
	    print_3($name,"Would have deleted empty saved  file $filename (delete flag is false)",$MERROR,1); 
	}
    }
    else  
    { 
	print FOUT "$name: $filename has a valid size; no action taken \n";     
	print "$name: $filename has a valid size; no action taken \n";     
    }
}
else 
{  
    print  "$name: $filename does not exist; no action taken \n"; 
    print FOUT "$name: $filename does not exist; no action taken \n"; 
}
exit;

