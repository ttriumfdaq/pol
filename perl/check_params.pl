#
# Check the input parameters 
#
#      expt  run_number  saved_dir
#
#  needed by kill.pl
#
# $Log: check_params.pl,v $
# Revision 1.7  2004/06/10 18:05:55  suz
# use our; last Rev message is bad
#
# Revision 1.6  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.5  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.4  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.3  2001/09/14 19:07:05  suz
# use strict added
#
# Revision 1.2  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.1  2001/02/23 17:56:41  suz
# initial version
#
#
# globals
# FOUT file handle is a global
use strict;
our ($EXPERIMENT,$FAILURE,$SUCCESS);
# for odb  msg cmd:
our ($MERROR, $MINFO, $MTALK);


sub check_params
{
# input parameters:  experiment   run #   saved dir   routine name
    my ( $expt, $run_number, $saved_dir ) =  @_; 
    my $name = "check_params";
    my ($status,$path,$key,$rn);

#         All output will be written to file

    print FOUT  "check_params starting with parameters:  \n";
    print FOUT  "Experiment = $expt; Run number= $run_number  \n";
    print FOUT  "Saved dir  = $saved_dir\n\n";
#
#
# Check the parameters
#
#
    if ($expt) { $EXPERIMENT = $expt; }
    else 
    { 
# print to file and to screen, then exit. Cannot msg without an experiment. 
        print FOUT "check_params:  FAILURE -  No experiment supplied \n";
        print  "check_params:  FAILURE -  No experiment supplied \n";
        return ($FAILURE);
    }
    
    
    
# Check the run number is supplied
    unless ($run_number)
    {
        print FOUT "FAILURE: No run number has been supplied. \n";
        print "FAILURE: No run number has been supplied. \n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: No run number supplied." ) ;
        return ($FAILURE);
    }
    
    
# Check the saved data directory has been supplied
    unless ($saved_dir)
    {
        # This is an ERROR condition. Saved_dir must be supplied.
        print FOUT "FAILURE: No saved directory has been supplied.\n"; 
        print  "FAILURE: No saved directory has been supplied.\n"; 
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Saved directory not supplied. " ) ; 
        return ($FAILURE);
    }
    
#
# read the current run number as a check
#
    $run_number = $run_number + 0; # make sure run number is an integer
    print FOUT "Checking current run number from odb against value supplied ...\n";
    ($status, $path, $key) = odb_cmd ( "ls"," /Runinfo"," /Run number  " ) ;
    unless ($status) { exit_with_message(); } 
    $rn = get_int ( $key);
    print FOUT "run number = $rn\n";
    if ($run_number != $rn)
    {
        print FOUT "FAILURE: Current run number ($rn) is different to run number supplied ($run_number)\n";
        print  "FAILURE: Current run number ($rn) is different to run number supplied ($run_number)\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run number mismatch." ) ;
        return ($FAILURE);
    }
    else { print FOUT "...OK\n";}
    
    return ($SUCCESS);
    
}



# this 1 is needed at the end of the file so require returns a true value   
1;   
