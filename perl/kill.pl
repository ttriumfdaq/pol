#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#  kill.pl
# 
# invoke this perlscript with cmd 
#        include    expt  run #   saved dir               disable_run   purge  hold   beam toggle suppress ppg    
#        path                                              _number_check  flag  flag  line  flag   save   mode 
# kill.pl  *       bnmr  30102 /home/bnmr/online/dlog/2001    n           y      n     bnmr   n      y     2a
#          +       m20   30030 /data/m20/2004                 n           y      %     musr   n      y     2 (always)
#
#   * include_path = /home/bnmr/online/perl 
#   +  /home/musrdaq/musr/mdarc/perl
#
#   %  "hold flag"  for MUSR should be linked to /runinfo/state (can be 1,2,3)
#
#
# $Log: kill.pl,v $
# Revision 1.29  2004/11/16 23:17:42  suz
# use stop now for kill after implementing deferred transition for bnmr
#
# Revision 1.28  2004/11/10 19:53:41  suz
# change some messages
#
# Revision 1.27  2004/11/10 00:16:34  suz
# mdarc param endrun_save_event renamed to suppress_save_temporarily; sets this flag before killing run and resets it afterwards
#
# Revision 1.26  2004/11/08 20:57:06  suz
# add extra parameter (endrun_save_last_event)
#
# Revision 1.25  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.24  2004/04/23 18:57:59  suz
# changes to hold_flag for MUSR; hold_flag=run_state for MUSR
#
# Revision 1.23  2004/04/21 19:11:14  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.22  2004/04/08 17:41:42  suz
# add a comment
#
# Revision 1.21  2004/03/29 18:38:14  suz
# replace Die & Cont by DIE and CONT for consistency with other pgms
#
# Revision 1.20  2004/03/29 18:26:53  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.19  2004/03/22 19:48:27  suz
# fix bug
#
# Revision 1.18  2004/02/13 20:58:30  suz
# fix bug (mdarc odb key purge/rename's name changed) & use print_3
#
# Revision 1.17  2004/02/09 22:14:49  suz
# add changes for MUSR (BNMR's hold_flag -> MUSR's /runinfo/state)
#
# Revision 1.16  2003/05/21 17:11:10  suz
# improve transition message
#
# Revision 1.15  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.14  2003/01/08 18:35:37  suz
# add polb
#
# Revision 1.13  2002/04/15 19:43:25  suz
# Fix bug in messages about file permissions
#
# Revision 1.12  2002/04/15 17:15:52  suz
# add parameter include_path and support for musr pause
#
# Revision 1.11  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.10  2001/11/01 17:55:22  suz
# change parameter message
#
# Revision 1.9  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.8  2001/09/14 19:20:49  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.7  2001/06/19 18:20:29  suz
# run must now be on hold to kill
#
# Revision 1.6  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.5  2001/04/30 20:01:05  suz
# Add support for midas logger
#
# Revision 1.4  2001/03/29 17:49:01  suz
# Fix minor bug (replaced dir by saved_dir)
#
# Revision 1.3  2001/03/01 19:46:35  suz
# Check write access rather than ownership of saved directory.
#
# Revision 1.2  2001/03/01 19:03:53  suz
# Sets purge & rename flag before ending run, so file is not archived. Access to
# archive_dir removed, checks all files are intermediate or symlink before
# deleting. Checks permissions on files. Output file is now written to
# /var/log/midas  rather than /tmp.
#
# Revision 1.1  2001/02/22 22:36:57  suz
# Initial version
#
#
use strict; 
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_PAUSED=2;  # Run state is paused
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our($inc_dir, $expt, $run_number, $saved_dir, $dis_rn_check, $purge_flag, $hold_flag, $beamline, $toggle_flag, $suppress_save, $ppg_mode) =@ARGV;
# note: for BNMR/BNQR saved directory in /script/kill is a link to mdarc's (type 2) or mlogger's (type 1). 
# These directories must be the same (this is checked)
# note also that toggle is not supported for BNMR type 1
our $len =  $#ARGV; # array length
our $name = "kill"; # same as filename
our $nparam = 11;
our $outfile = "kill.txt";
our $parameter_msg = "include_dir, expt , run #, saved dir, disable_rn_check flag, purge flag , hold flag, beamline, toggle flag, save flag, ppg mode";
our $suppress=$FALSE; # do not suppress message from open_output_file
#########################################################
my $status;
my ($old_run, $eqp_name);
my $mdarc_path = "/Equipment/FIFO_acq/mdarc/";
my ($transition, $run_state);
#########################################################
# prototypes
sub kill_run_type_2($$$$);
sub kill_run_type_1($$$);

$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/check_params.pl";
# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#

print FOUT  "$name: Arguments supplied:  @ARGV\n";
print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; Run number= $run_number  \n";
print FOUT  "Paths:  Saved file directory  = $saved_dir; Disable automatic run number check = $dis_rn_check\n"; 
print FOUT  "Purge flag=$purge_flag; hold flag(BNMR)/run state(MUSR)=$hold_flag; beamline=$beamline;\n";
print FOUT  "toggle flag=$toggle_flag; suppress_save flag=$suppress_save; ppg mode=$ppg_mode \n";

unless ($dis_rn_check eq "n") 
{
    print_3 ($name, "FAILURE: Automatic run numbering is DISABLED ",$MERROR, $DIE); 
}


$status = check_params($expt, $run_number, $saved_dir, $name);
unless ($status) 
{
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - problem with input parameters" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print      "Invoke this perl script $name with the parameters:\n";
    die      "   $parameter_msg\n"; 
}

unless ($ppg_mode =~/^[12]/)
{
    print_3 ($name, "FAILURE: invalid ppg mode ($ppg_mode) supplied", $MERROR, $DIE);
}

# see if the run is stopped
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
if ($run_state == $STATE_STOPPED)
{   # Run is stopped
    print_3($name,"Run is stopped. Can only kill when running",$MERROR,$DIE);
}

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[mq]r/i) )
{
    
#   BNMR/BNQR uses the hold flag to pause the run
    unless ($hold_flag eq "y") 
    {
	print_3($name, "FAILURE: Run must be on HOLD to be killed. ",$MERROR,$DIE);
    }
    
    # BNMR/BNQR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
}
else
{
    # MUSR experiments used midas PAUSE; hold_flag is /runinfo/state so will be STATE_RUNNING,STATE_STOPPED or STATE_PAUSED
    $hold_flag=$hold_flag+0; # make sure it's an integer
    print FOUT ("MUSR expt...parameter run state=$hold_flag; latest run state = $run_state\n");
    unless ($run_state == $hold_flag) {  $hold_flag=$run_state; } # use the most recent value
    unless ($hold_flag == $STATE_PAUSED)
    {   # Run is NOT paused
	print_3 ($name,"INFO: Kill not allowed UNLESS  run is PAUSED (presently run_state=$hold_flag) " ,$MERROR,$DIE);
    }
    
    # All MUSR experiments use equipment name of MUSR_TD_ACQ
    $eqp_name = "MUSR_TD_ACQ";
} 
$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";


#
#              Type 2 Experiment
#
if ( $ppg_mode =~ /^2/)  # Type 2 experiment ( including all MUSR I/TD ) 
{ 
    ($status) = kill_run_type_2( $saved_dir, $toggle_flag, $purge_flag, $suppress_save);
}
else
{     # bnmr/ bnqr only
    ($status) = kill_run_type_1($saved_dir, $purge_flag, $suppress_save);
}
#
#
#
#
#      Now decrement run number ( starting next run will increment it)
#
#

$old_run = $run_number;  # remember old run for message 
$run_number = $run_number -1;

unless (set_run_number($run_number))  { die " $name:  Error return from set_run_number";}

# success
print_3($name, "Successfully killed run $old_run ",$MINFO,$CONT);
exit;


##########################################################################################################
#
#      Type 2
#
##########################################################################################################

sub kill_run_type_2($$$$)
{
# parameters
#    $toggle_flag
#    $purge_flag
#    $suppress_save
    
    my $name="kill_run_type_2";
    my ($saved_dir, $toggle_flag, $purge_flag, $suppress_save) = @_;
    my ($status, $nextname, $found_files_flag);
    my ($process,$run_state,$transition);
    
    print "$name: params saved_dir:$saved_dir; toggle_flag:$toggle_flag; purge_flag:$purge_flag; suppress_save:$suppress_save\n";
    print FOUT "$name: params saved_dir:$saved_dir; toggle_flag:$toggle_flag; purge_flag:$purge_flag; suppress_save:$suppress_save\n";
# toggle_flag is true if not supplied 
    if ($toggle_flag eq "n") { $toggle_flag = $FALSE; }
    else { $toggle_flag = $TRUE ; }  
    
# set purge flag true if not supplied
    if ($purge_flag eq "n") { $purge_flag = $FALSE;} 
    else { $purge_flag = $TRUE ; } # if parameter is not supplied, assume purge is set.
    
#
# Check whether mdarc is running (after $EXPERIMENT is set up by check_params)
#
    unless (mdarc_running())   # subroutine to check if mdarc is running
    {
#  Don't kill without mdarc (no saved files) & run number not been validated 
	print_3($name, "Mdarc is not running. Cannot kill run. ",$MERROR,$DIE);
    }
    
    
# check state of toggle flag
    if ($toggle_flag) 
    { 
	print_3($name, "Toggle flag is set. Run cannot be killed until toggle is complete.",$MERROR,$DIE);
    }
# check if this process owns the directory containing the saved files
# saved_dir is not blank (checked on entry as an input parameter).
    unless (-w $saved_dir )
    { 
	$process= getpwuid($<);
	print_3($name, " This process ($process) does not have write access to $saved_dir. Kill cannot proceed.",$MERROR,$DIE);
    }
    else {     print FOUT "INFO - this process DOES have write access to directory $saved_dir \n"; } 
    
    
# check whether run is in progress
    ($run_state,$transition) = get_run_state();
    if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
    
    if($transition ) 
    { 
	print_3 ($name, "Run is in transition. Run cannot be killed. Try again later ",$MINFO,$CONT);
	print_3 ($name,"Or set /Runinfo/Transition in progress to 0 ",$MERROR,$DIE);
    }
    
    
    if ($DEBUG) { print "Run state = $run_state \n"; }
    if ($run_state != $STATE_STOPPED) 
    {   # Run is not stopped; 
	
#   prevent saving of final event
#   set suppress_save flag true if not already set
	unless ($suppress_save eq "y")
	{
	    print FOUT  "Attempting to set mdarc parameter: suppress_save_temporarily TRUE\n";
	    
	    ($status) = odb_cmd ( "set","$mdarc_path","suppress_save_temporarily" ,"y") ;
	    unless($status)
	    { 
		print_3 ($name, "Failure from odb_cmd (set); Error setting flag suppress_save_temporarily",$MERROR,$DIE);
	    }
	}
	
#   prevent archiving (and renaming) of final saved files	
	print FOUT  "Attempting to clear mdarc parameter endrun_purge_and_archive\n";
	
	($status) = odb_cmd ( "set","$mdarc_path","endrun_purge_and_archive" ,"n") ;
	unless($status)
	{ 
	    print_3 ($name, "Failure from odb_cmd (set); Error clearing endrun_purge_and_archive flag",$MERROR,$DIE);
	}
	print_3($name,"INFO: Success - purge/archive flag disabled & suppress save flag enabled ",$MINFO,$CONT);
	
#   stop the run
	print FOUT  "Attempting to stop run $run_number (run_state=$run_state) with a stop now cmd \n";
	
	($status) = odb_cmd ( "stop now" ) ;
	unless ($status) { exit_with_message($name); }
	($run_state,$transition) = get_run_state();
	get_run_state($transition,$run_state);
	#if($transition) { print FOUT  "Run is in transition ($trans) \n"; }
	if($run_state != $STATE_STOPPED ) 
	{
	    print_3($name," FAILURE: Can't stop the run ",$MERROR,$DIE);
	}
    }
    else
    {
	# run is already stopped. A stopped run can only be killed if 
	# it has the current run number, or risk destroying an earlier saved file...
	# but mdarc is running so it should have checked the run number ... maybe it is OK
	
	# but for now ...
	print_3( $name,"Run is already stopped. Can only kill a run that is in progress",$MERROR,$DIE);
    }
    
#
# Run is now stopped. 
#
    
# Reset suppress_save flag to "n"
    print FOUT  "Attempting to reset mdarc parameter suppress_save_temporarily\n";
    
    ($status) = odb_cmd ( "set","$mdarc_path","suppress_save_temporarily" ,"n") ;
    unless($status)
    { 
	print_3 ($name,"Failure from odb_cmd (set); Error resetting  suppress_save_temporarily flag ",$MERROR,$DIE);
    }
    print_3 ($name,"INFO: flag to suppress save temporarily has been cleared",$MINFO,$CONT);


 # Reset purge/rename flag to original state
    if($purge_flag)
    {
	print FOUT  "Attempting to reset mdarc parameter endrun_purge_and_archive\n";
	
	($status) = odb_cmd ( "set","$mdarc_path","endrun_purge_and_archive" ,"y") ;
	unless($status)
	{ 
	    print_3 ($name,"Failure from odb_cmd (set); Error resetting endrun_purge_and_archive flag ",$MERROR,$DIE);
	}
	print_3 ($name,"INFO: Purge and archive flag has been reset",$MINFO,$CONT);
    }
#
#  Now find and delete any saved files
# saved_dir is not blank  (checked on entry as an input parameter).
    if ($DEBUG) { print FOUT  "saved_dir ($saved_dir) is not blank\n";}
    unless ( chdir ("$saved_dir") )
    {
	print_3 ($name,"cannot change directory to $saved_dir, Run not killed ",$MERROR,$DIE);
    }
    
    $found_files_flag = $FALSE; # flag to indicate found any run files
# Expect to delete intermediate saved files (versioned or a symlink thanks to endrun_purge_and_archive flag being cleared)
# No archived files will be deleted.
    while ($nextname = <*$run_number.msr*>) 
    {
	print FOUT  "found saved  file  $nextname ( and about to delete it ) \n";
	$found_files_flag = $TRUE;
#   make sure this is NOT a final saved file
	if ( $nextname =~ /msr$/i)   # anchored search for .msr files
	{
	    unless (-l $nextname)
	    {
		print_3 ($name,"ERROR: found final saved file $nextname. It will NOT be deleted. Run not killed.",$MERROR,$DIE);
	    }
	    else 
	    { print FOUT "INFO - found a file $nextname that IS a symlink\n"; }
	}
#   check we are the owner
	unless (-o $nextname)
	{
	    print_3 ($name, "ERROR - this process is not the owner of $nextname ",$MERROR,$DIE); 
	}
	
# check the permissions on the file
	unless (-w $nextname)
	{   # add write permissions
	    print FOUT "INFO: changing permission on file $nextname to allow deletion\n";
	    unless (chmod(0600,$nextname) )
	    { 
		print_3 ($name,"ERROR -  Couldn't change permissions on $nextname. Run not killed",$MERROR,$DIE); 
	    }
	}
	unless (  unlink ($nextname))
	{
	    print_3 ($name,"failure deleting saved run file $nextname. Run not killed ",$MERROR,$DIE);
	}
	else
	{
	    print_3($name, "INFO: Deleted saved run file $nextname ",$MINFO,$CONT);
	}
	
    }
    unless ($found_files_flag)
    {
	print_3 ($name, "Info - No saved run files were found for run $run_number in $saved_dir",$MINFO,$CONT);
    }
    
    return;
}

##########################################################################################################
#
#      Type 1
#
##########################################################################################################
sub kill_run_type_1($$$)
{
# parameters
#    $saved_dir
#
#  Assumes midas logger channel is 0
			
    my $name="kill_run_type_1";
    my ($saved_dir, $purge_flag, $suppress_save) = @_;
    
    my ($run_state,$transition, $process);
    my ($status, $nextname, $found_files_flag);
    
    my $debug=$FALSE;
    
    if($debug) { print "$name: parameters midas data dir: $saved_dir; purge_flag: $purge_flag; suppress_save: $suppress_save  \n"; }
    print FOUT "$name: parameters  midas data dir: $saved_dir ; purge_flag: $purge_flag; suppress_save: $suppress_save \n";
    
    
# Midas logger
#
#    Make sure  mlogger is running and logging is turned on, or no saved files  
    unless (mlogger_running() )   # subroutine to check if midas logger is running
    {
	print_3 ($name,"Midas logger is not running or is disabled in odb. Kill button not enabled",$MERROR,$DIE);
    }
    
    
    
# check if this process owns the directory containing the saved files
# saved_dir is not blank (checked on entry as an input parameter).
    unless (-w $saved_dir )
    { 
	$process= getpwuid($<);
	print_3 ($name,"This process ($process) does not have write access to $saved_dir. Kill cannot proceed ",$MERROR,$DIE);
    }
    else {     print FOUT "INFO - this process does have write access to directory $saved_dir \n"; } 
# check whether run is in progress
    ($run_state,$transition) = get_run_state();
    if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
    
    if($transition ) 
    { 
	print_3 ($name, "Run is in transition. Run cannot be killed. Try again later ",$MINFO,$CONT);
	print_3 ($name,"Or set /Runinfo/Transition in progress to 0 ",$MERROR,$DIE);
    }
    
    # set purge flag true if not supplied
    if ($purge_flag eq "n") { $purge_flag = $FALSE;} 
    else { $purge_flag = $TRUE ; } # if parameter is not supplied, assume purge is set.
    
    
    if ($DEBUG) { print "Run state = $run_state \n"; }
    if ($run_state != $STATE_STOPPED) 
    {   # Run is not stopped; 
	
#   prevent saving of final event data
	# set suppress flag true if not already set true (should be already set by the pause)
	unless ($suppress_save eq "y")
	{
	    print FOUT  "Attempting to set mdarc parameter suppress_save_temporarily TRUE\n";
	    
	    ($status) = odb_cmd ( "set","$mdarc_path","suppress_save_temporarily" ,"y") ;
	    unless($status)
	    { 
		print_3 ($name, "Failure from odb_cmd (set); Error setting suppress_save_temporarily flag",$MERROR,$DIE);
	    }
	}
#   prevent archiving (and renaming) of final saved files
	print FOUT  "Attempting to clear mdarc parameter endrun_purge_and_archive\n";
	($status) = odb_cmd ( "set","$mdarc_path","endrun_purge_and_archive" ,"n","") ;
	unless($status)
	{ 
	    print_3( $name,"Failure from odb_cmd (set); Error clearing endrun_purge_and_archive flag ",$MERROR,$DIE);
	}
	print_3($name, "INFO: Success - Endrun purge/archive &save flags have been cleared ",$MINFO,$CONT);
	
#   stop the run
	print FOUT  "Attempting to stop run $run_number (run_state=$run_state) \n";
	
	($status) = odb_cmd ( "stop now" ) ;
	unless ($status) { exit_with_message($name); }
	($run_state,$transition) = get_run_state();
	get_run_state($transition,$run_state);
	#if($transition) { print FOUT  "Run is in transition ($trans) \n"; }
	if($run_state != $STATE_STOPPED ) 
	{
	    print_3($name, " FAILURE: Can't stop the run ",$MERROR,$DIE);
	}
    }
    else
    {
	# run is already stopped. 
	print_3($name, "Run is already stopped. Can only kill a run that is in progress",$MERROR,$DIE);
    }
    
#
# Run is now stopped. 
#
#
# Reset purge/rename flag to original state
    if($purge_flag)
    {
	print FOUT  "Attempting to reset mdarc parameter endrun_purge_and_archive\n";
	($status) = odb_cmd ( "set","$mdarc_path","endrun_purge_and_archive" ,"y") ;
	unless($status)
	{ 
	    print_3 ($name, "Failure from odb_cmd (set); Error resetting endrun_purge_and_archive flag.",$MERROR,$DIE);
	}
	print_3($name,"INFO: Success - endrun_purge_and_archive flag has been reset ",$MINFO,$CONT);
    }

    # Reset temporary suppress save flag to "n" 
    print FOUT  "Attempting to reset mdarc parameter suppress_save_temporarily \n";
    ($status) = odb_cmd ( "set","$mdarc_path","suppress_save_temporarily" ,"n") ;
    unless($status)
    { 
	print_3 ($name, "Failure from odb_cmd (set); Error resetting  suppress_save_temporarily flag.",$MERROR,$DIE);
    }
    print_3($name,"INFO: Success -  suppress_save_temporarily flag has been cleared ",$MINFO,$CONT);
    
#  Now find and delete any saved files
# saved_dir is not blank  (checked on entry as an input parameter).
    unless ( chdir ("$saved_dir") )
    {
	print_3($name, "cannot change directory to $saved_dir",$MERROR,$DIE);
    }
    
    $found_files_flag = $FALSE; # flag to indicate found any run files
    while ($nextname = <*$run_number.m*>)    # MIDAS file, .msr as well  
    {
	print FOUT  "found saved  file  $nextname ( and about to delete it ) \n";
	$found_files_flag = $TRUE;
#   check we are the owner
	unless (-o $nextname)
	{
	    print_3 ($name, "ERROR - this process is not the owner of $nextname",$MERROR,$DIE);
	}
	
# check the permissions on the file
	unless (-w $nextname)
	{   # add write permissions
	    print FOUT "INFO: changing permission on file $nextname to allow deletion\n";
	    unless (chmod(0600,$nextname) )
	    { 
		print_3 ($name," ERROR -  Couldn't change permissions on $nextname" ,$MERROR,$DIE);
	    }
	}
	unless (  unlink ($nextname))
	{
	    print_3($name, "failure deleting $nextname in $saved_dir. Run not killed",$MERROR,$DIE);
	}
	else
	{
	    print_3($name, "Deleted saved run file $nextname",$MINFO,$CONT);
	}
	
    }
    unless ($found_files_flag)
    {
	print_3($name, "Info - No saved run files were found for run $run_number in $saved_dir",$MINFO,$CONT);
    }
    
    return;
}













