#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
#    BNMR/BNQR only - set rereference or other flag
#
#   Normally invoked from reref (or other) button
#
# invoke this script with cmd
#                  include         experiment   current   beamline  flag name
#                   path                      flag value            in odb
# reref.pl  /home/bnqr/online/perl    bnqr        y       bnqr     re-reference
# reref.pl  /home/pol/online/perl     pol         y       pol      re-ref P+
# reref.pl  /home/pol/online/perl     pol         y       pol      flag bad scan
#
#
# Sets a flag (usually a re-reference flag) in the hardware area of odb.
# flag is hot linked
#
#
# $Log: reref.pl,v $
# Revision 1.6  2004/06/10 18:11:18  suz
# require init_check; use our; last Rev message is bad
#
# Revision 1.5  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.4  2004/04/21 19:07:31  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.3  2004/03/30 22:54:52  suz
# change a message
#
# Revision 1.2  2004/03/29 18:27:26  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.1  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#

use strict;


##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $flag_value, $beamline, $flag_name ) = @ARGV;
our $len =  $#ARGV; # array length
our $name = "reref"; # same as filename
our $nparam=5;
our $outfile = "reref.txt";
our $parameter_msg = "include_path, experiment; flag value; beamline; flag name\n";
############################################################################
my ($transition, $run_state, $path, $key, $status);
my $input_path, $status ;
my $debug=$FALSE;
my $eqp_name;

$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl";

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

#
# Output will be sent to file $outfile
# because this is for use with the browser and STDOUT and STDERR get set to null


print FOUT   "$name starting with parameters:  \n";
print FOUT   "Experiment = $expt; flag value = $flag_value; beamline: $beamline; flag name: \"$flag_name\";\n";
#
if ($flag_value eq "y")
{
    # flag should not be set already
    print_3($name, "Warning: flag \"$flag_name\" is already set",$MINFO,0 ) ;
}

# set flag (program clears it after use)
$flag_value = $TRUE ;

#
#      determine equipment name from beamline
#
print  ("beamline = $beamline\n");
unless( ($beamline =~ /bn[qm]r/i) || ($beamline =~ /pol/i) )
{
    # Not supported for MUSR experiments
    print_3($name,"INFO: Supported only for BNMR/POL experiments",$MINFO,1);
}    

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_RUNNING)
{          #      Not Running
    print_3($name,"Not Running (run_state=$run_state)... no action",$MINFO,1);
}



# BN[QM]R &POL experiments use equipment name of FIFO_ACQ
$eqp_name = "FIFO_ACQ";
$input_path = "/Equipment/$eqp_name/sis mcs/hardware";

print FOUT "input_path = $input_path\n";

($status) = odb_cmd ( "set","$input_path","$flag_name" ,"$flag_value") ;
unless($status)
{
    print_3($name,"Failure from odb_cmd (set); Error updating odb hardware flag ($flag_name)",$MERROR,1);
} 
print_3($name,"INFO: Success - odb hardware flag ($flag_name) has been set", $MINFO,0);

exit;
