#!/usr/bin/perl
# above is magic first line to invoke perl
# warnings on : !/usr/bin/perl -w
#     get_next_run_number.pl
#
############################################################################
#                                                                          #
#          This program should be called from mdarc                        #
#             since it may reset the current run number!!                  #
#                                                                          #
#          DO NOT RUN THIS SCRIPT UNLESS INVOKED BY MDARC                  #
#             or problems may occur with saved run files!!!                #
#              (except for testing!)                                       #
############################################################################
#
#
# invoke this with cmd    from mdarc of course!)      (1) when mdarc is started
#                                                     (2) on begin-of-run
#                                                     (3) if toggle flag is set 
#                  
#   get_next_run_number.pl
#
#                                        Input Parameters:   
#                          include                      expt    rn    equipment     check   beamline  ppg_mode   
#                          directory                                   name       for holes
# get_next_run_number.pl /home/bnmr/online/mdarc/perl   bnmr     0    FIFO_acq      0/1       bnmr     2a
#                        /home/bnqr/online/mdarc/perl   bnqr     0    FIFO_acq      0/1       bnqr     1c
#                        /home/musrdaq/musr/mdarc/perl  musr     0    MUSR_acq      0/1       dev      2
#                        /home/musrdaq/musr/mdarc/perl  musr     0    MUSR_acq      0/1       m15      2
#
#  this script will read current run_number, run mode & saved directory from odb 
#
# Note - run_number parameter rn=0 for dummy run (see tr_start in  mdarc.c), i.e.
#       for an actual begin_run, rn !=0
#
# Note - musr does not use ppg_mode but this parameter is permanently set to 2 as both musr and imusr
#        use versions of mdarc, not mlogger.
#
# Note - need eqp_name to determine the path of mdarc area, as this will
# be different for bnmr, musr etc.

#
# Output goes into file $outfile
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $next_run      next run number   - writes this to odb location for communication
#
# $Log: get_next_run_number.pl,v $
# Revision 1.23  2005/06/10 17:45:19  suz
# minimum run numbers are incremented; add temp fix to allow old minimum values in data area until next year, when new minima will take effect
#
# Revision 1.22  2004/06/10 17:35:03  suz
# requires init2_check.pl;our; remove temp sleep; Rev1.21 message is bad
#
# Revision 1.21  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.18  2004/04/02 22:33:35  suz
# add check on whether change_mode.pl is running
#
# Revision 1.17  2004/03/30 22:52:56  suz
# add timing info using write_time & change to print_3
#
# Revision 1.16  2004/03/29 18:29:24  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.15  2004/02/09 22:04:37  suz
# add changes for musr
#
# Revision 1.14  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.13  2003/01/08 18:36:17  suz
# add polb
#
# Revision 1.12  2002/05/08 01:16:58  suz
# fix some comments
#
# Revision 1.11  2002/04/12 21:44:54  suz
# check on writing to saved data dir now returns a warning
#
# Revision 1.9  2001/12/07 22:05:04  suz
# take out message about holes in run numbering
#
# Revision 1.8  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.7  2001/11/01 17:54:54  suz
# change variable msg to message
#
# Revision 1.6  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.5  2001/09/14 19:18:42  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.4  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.3  2001/04/30 19:58:30  suz
# Add support for midas logger
#
# Revision 1.2  2001/03/01 19:07:00  suz
# Check saved_data directory is writable for mdarc. Output file written in
# /var/log/midas rather than /tmp.
#
# Revision 1.1  2001/02/23 17:59:53  suz
# initial version
#
#
use strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ( $inc_dir, $expt, $rn, $eqp_name, $check_for_holes, $beamline, $ppg_mode ) = @ARGV;
our $len = $#ARGV; # array length
our $other_name = "change_mode";
our $name = "get_next_run_number"; # same as filename
our $outfile = "get_next_run_number.txt";
our $nparam = 7; # need 7 input parameters
our $parameter_msg = "include_path,  experiment , rn, equipment_name, check_for_holes,  beamline, ppg_mode";
############################################################################

# Globals needed by  this file and to run_number_check:
our $MAX_TEST = 30499; 
our $MIN_TEST = 30001;

# BNMR values
our $MAX_BNMR = 44499; 
our $MIN_BNMR = 40001;
#other beamlines
our $MIN_M20  =     1;
our $MAX_M20  =  4999;
our $MIN_M15  =  5001;
our $MAX_M15  =  9999;
our $MIN_M9B  = 15001;
our $MAX_M9B  = 19999;
our $MIN_DEV  = 20001;
our $MAX_DEV  = 29999;
our $MIN_BNQR = 45001;
our $MAX_BNQR = 49999; 
# The following only used if $check_for_holes is true.
our ($LIST_OF_HOLES, $CHECK_FOR_HOLES);
our $FOUND_HOLE = $FALSE;  # flag indicates if there are holes in the run numbers
############################################################################
# local variables:
my ($run_number,  $run_type,  $saved_dir1,$saved_dir2,$saved_dir ); # read directly from odb
my ($transition, $run_state, $path, $key, $status);
my ($next_run, $info, $process);
my $msg_flag = $FALSE;
my $debug =  $FALSE;
my ($m1,$m2);
my ($loop,$flag);
my $message=" ";
$DEBUG=$TRUE;
#------------------------------------------------------
#
$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/run_number_check.pl";
require "$inc_dir/odb_access.pl";
require "$inc_dir/set_run_number.pl";

# Init2_check.pl checks:
#   one copy of this script running
#   no copies of $other_name running (may interfere with this script)
#   no. of input parameters is correct
#   opens output file:
#
require "$inc_dir/init2_check.pl";

print FOUT    "get_run_number  starting with parameters:  \n";
print FOUT    "     Experiment = $expt;  Equipment name = $eqp_name  run number = $rn; \n";
print FOUT    "     Check for holes = $check_for_holes;  beamline = $beamline \n";  

#
#          Check the parameters
#
unless( ($EXPERIMENT =~ /bn[qm]r/i)  ||  ($EXPERIMENT =~ /musr/i) )
{
    print_3($name, "FAILURE: Experiment type $EXPERIMENT not supported ",$MERROR,$DIE ) ;
}

# check for holes parameter
unless ($check_for_holes) { $check_for_holes = $FALSE } ;


# rn will be set to run number for genuine tr_start, or 0 for dummy.
# can't use "unless ($rn) " since it will catch rn = 0 
$rn = $rn + 0; # make sure rn is an integer
 
unless ($eqp_name) 
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print_3 ($name, "FAILURE: Equipment name not supplied. ",$MERROR,$DIE ) ;
}
unless ($ppg_mode) 
{
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print_3 ($name, "FAILURE: ppg mode not supplied. ",$MERROR,$DIE ) ;
}
unless ($ppg_mode=~/^[12]/)
{
    print_3 ($name, "Invalid PPG mode supplied ($ppg_mode)",$MERROR,1);
}



#
# Saved directory for BNMR/BNQR type 2   and  all MUSR experiments
#   Read the  MDARC saved data directory from odb
($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc/",  "saved_data_directory " ) ;
unless ($status) 
  { 
    print_3($name,"FAILURE: Could not read Mdarc saved data directory from odb",$MERROR,1);
  }
($saved_dir2,$message) = get_string ( $key);
print FOUT "$name: after get_string, saved_dir for Type 2 =$saved_dir2\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }

unless ($saved_dir2)
  {
    # This is an ERROR condition. Saved_dir2 must be supplied.
    print_3($name,"Saved directory for Type 2 is a blank string",$MERROR,$DIE);
  }
$saved_dir=$saved_dir2;


# read the mlogger saved directory only for BNMR/BNQR experiments (both types)
unless ($EXPERIMENT =~ /musr/i)
  {
    #    
    #              Read parameters from the odb
    #
    # Read the saved directories for both types
    #
    # Type 1 Saved directory 
    #   Read the  MLOGGER saved data directory from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /logger",  "data dir " ) ;
    unless ($status) 
      { 
	print_3($name, "FAILURE: Could not read Midas logger saved data directory from odb",$MERROR,$DIE ) ;
      }
    ($saved_dir1, $message) = get_string ($key);
    print FOUT "$name: after get_string, saved_dir for Type 1=$saved_dir1\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }
    
    unless ($saved_dir1)
      {
	# This is an ERROR condition. Saved_dir1 must be supplied.
	print_3($name,"Saved directory for Type 1 is a blank string",$MERROR,$DIE);
      }

    # check these two directories are identical (bnmr/bnqr both types)
    $status = check_directories($saved_dir1,$saved_dir2);
    unless ($status)
      {
	print_3($name,"FAILURE: Saved directories between mlogger and mdarc areas are not identical\n",
		$MERROR,1);
      }
    if( ($ppg_mode =~ /^1/i)  ) { $saved_dir=$saved_dir1; }

  } # end of identical directory check for bnmr/bnqr only

print FOUT  "$name: Current directory for saved files = $saved_dir\n";
# Check we have  write access to the saved directory (or mdarc/mlogger will fail to write files to it)
unless (-w $saved_dir ) 
  { 
    $process= getpwuid($<);
    print_3($name, "WARNING: this process (username=$process) cannot write to saved data directory: $saved_dir",$MINFO,$CONT);
  } 

#
#    
#              Read more parameters from the odb
#
#
#
# read the current run number
#
($status, $path, $key) = odb_cmd ( "ls"," /Runinfo"," /Run number  " ) ;
unless ($status) 
{ 
    print_3 ($name,"Error reading run number from odb",$MERROR, $DIE);
} 
$run_number = get_int ( $key);
print FOUT  "$name: Current run number = $run_number\n";
#
# read the run type from odb
#
($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc  "," /run type  " ) ;
unless ($status) 
{ 
    print_3 ($name,"Error reading key  /Equipment/$eqp_name/mdarc/run type  from odb",$MERROR,$DIE);
} 
($run_type,$message) = get_string ( $key);
print FOUT "$name: after get_string, run_type=$run_type\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }

#  Check the run type is valid
unless ($run_type)
{
    print_3($name,"FAILURE - Empty string for run type has been supplied (expect test/real)",$MERROR,$DIE);
}
unless( ($run_type =~ /test/i) ||  ($run_type =~ /real/i) )
{
    print_3 ($name,"FAILURE - Run type ($run_type)  must be test or real",$MERROR,$DIE);
}
# Set check for holes to be TRUE for REAL runs
if ($run_type =~ /real/i)
{
    $check_for_holes = $TRUE;
}
else
{
    $check_for_holes = $FALSE;
}
#
#
#           Call run_number_check:
#
#
print FOUT   "Calling run_number_check for a $run_type run... \n";
print   "$name: Calling run_number_check for a $run_type run... \n";
#   FOUT is a global so we don't need to pass it.
($status, $next_run, $message  ) = run_number_check (  $saved_dir, $run_type, $check_for_holes, $beamline);
# note - run_number_check recycles test run numbers and deletes any old TEST saved run files for $next_run

unless ($status) 
{            #  FAILURE status from run_number_check   
    print_3 ($name,"Failure status returned from run_number_check",$MINFO,$CONT); 
    if ( $next_run ==  -2)      # -2 indicates a message
    { 
	# see if message should be split into 2 lines (contains a + )
	($m1,$m2) = split (/\+/,$message);
	print("m1 :$m1\n");
	print("m2 :$m2\n");

# Write the message(s)
	unless($m2)
	{
	    print_3($name,"$m1",$MERROR,$DIE); # relay the message to the user
	}
	else
	{
	    print_3($name,"$m1",$MERROR,$CONT); # relay both messages to the user
	    print_3($name,"$m2",$MERROR,$DIE);
	}
    }
    print_2 ($name,"Failure status returned from run_number_check",$DIE);
}
print FOUT   "Success from  run_number_check. Next $run_type run should be = $next_run\n"; 
if ($debug)
 { print "Success from  run_number_check. Next $run_type run should be = $next_run\n"; } 


if ($FOUND_HOLE)
  {
    print    "$name: ** WARNING: Run_number_check has found holes in the sequence of saved files for REAL run numbers  ** \n"; 
    print FOUT   "$name: ** WARNING: Run_number_check has found holes in the sequence of saved files for REAL run numbers  ** \n"; 
    ###
###  T E M P
### temporary - remove warning about HOLES 
#    ($status)=odb_cmd ( "msg","$MINFO","","$name", "** WARNING: Run_number_check has found HOLES in the sequence of saved files for REAL run numbers: **" );
#    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 

    print "$name: $LIST_OF_HOLES\n";
    print FOUT "$name: $LIST_OF_HOLES\n";
###    ($status)=odb_cmd ( "msg","$MINFO","","$name", "$LIST_OF_HOLES" );
###    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }     
  }
print FOUT "$name: Info - present run number is $run_number\n";
#
#
#
if( $ppg_mode =~ /^2/i)
{
    print FOUT "$name: detected type 2 experiment\n";
    print FOUT "$name: calling set_rn_type2 with parameters $expt, $rn, $eqp_name, $next_run,  $run_number, $saved_dir\n" ;
    ($status) = set_rn_type2($rn, $eqp_name, $next_run,  $run_number, $saved_dir );
}
else              #  Type 1 experiment 
{ 
    print FOUT "$name: detected type 1 experiment\n";
    print FOUT "$name: calling set_rn_type1 with parameters $rn, $eqp_name, $next_run,  $run_number, $saved_dir\n" ;
    ($status) = set_rn_type1 ( $rn, $eqp_name, $next_run,  $run_number, $saved_dir );
}
if($status != $SUCCESS)
{
    print_3($name,"Failure checking or setting the run number",$MERROR,$DIE);
}
#already checked above that ppg_mode starts with 1 or 2.

write_time($name, 3); # 3 = write_time writes an exit message
print FOUT "$name: after write_time status=$status\n";
exit;                           

#  CODE NO LONGER USED
sub check_process
{
# piece of code to look for mdarc in ps  - later found it was easier to use odb cmd scl
    my $mdarc_is_running = $FALSE;

    open ( PS, "ps -auwx |") || die "ps fails\n";   # pipe into a file handle
    while ( <PS> )  # read from PS
    {
        chomp;
        unless (/mdarc/i) {next;}    # look for string "mdarc"
        
        print "$_\n";
        if ( /(-e+)\W+(\w+)/i)       # look for string e.g. "-e suz2" and load it into $1 $2 (with brackets)
        { 
            print "matched  $1 and $2\n"; 
            if ( $2=~ /$EXPERIMENT/i )   # now match $2 with this experiment
            {
                $mdarc_is_running = $TRUE;
                print "found $EXPERIMENT\n"; 
            }
        }
    }
    close (PS);
    return ($mdarc_is_running);
}

# this does not seem to be used anywhere:
sub print_file ($$)
{
    my $filename = shift;
    my $name =   shift;

    open FOUT, "$filename" or die "$name: couldn't open output file $filename" ;
    while (<FOUT>) 
    { 
        print "$_" ;
    }
}

sub check_last_saved_file
{
#
# Input
#      Current saved_dir  equipment_name
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $run_last_saved     run number of last valid saved file
#   $info          TRUE if a final saved file, otherwise false
#
# Output goes into file handle FOUT
    
#
# 
    my ( $saved_dir, $eqp_name ) = @_;
    
    my ($path, $key, $status);
    
    my $name = "check_last_saved_file";
    my ($last_saved_filename, $ext, $num, $info, $a, $b);
    my (@file, @dir);
    my $filename;
    my $debug = $TRUE;
    
    print FOUT    "\n        = = = = $name = = = =    \n";
    print FOUT    "$name  starting with parameters:  \n";
    print FOUT    "     saved directory: $saved_dir \n";
    print FOUT    "     equipment name : $eqp_name \n";
    
#
# Check the parameters
#
#
    check_one ($saved_dir, "saved data directory", $name); 
    check_one ($eqp_name, "equipment name", $name);
    
    
#   Read the  last saved filename  from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc/",  "last_saved_filename " ) ;
    unless ($status) 
    { 
        print FOUT "$name: Error reading last_saved_filename from odb\n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read key mdarc/last_saved_filename from odb" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        if($debug) { print  "$name: Error reading last_saved_filename from odb\n"; }
        return ($FAILURE);
    }
    ($last_saved_filename,$message) = get_string ( $key);
    print FOUT "$name: after get_string, last saved filename=$last_saved_filename\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }
    if($debug) { print FOUT  "Read parameter last_saved_filename from odb = $last_saved_filename\n"; }
#print "saved_dir = $saved_dir\n";
    
    
# Check file exists
    unless (-f $last_saved_filename)  # includes directory
    { 
        print FOUT "$name: last saved file from odb key $last_saved_filename has not been found\n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:last saved file from odb key $last_saved_filename has not been found " ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        return ($FAILURE);  # strange error. No file found
    }
    
#
#  Check directory of last_saved_filename  is the same as saved_dir
#
    @file = split (/\//,$last_saved_filename); # split by /
    @dir  = split (/\//,$saved_dir);           # split by /
    
# check array lengths - @file should be one element longer
    if (  ($#dir+1) != ($#file + 0) ) 
    { 
        print FOUT "$name: array lengths of saved_dir ($saved_dir) and directory part of last_saved_filename ($last_saved_filename) don't match\n";
        return ($FAILURE);  # no information available on last saved run
    }
    
    $filename = pop (@file);  # pop the filename
    if ($debug) { print FOUT " filename =  $filename\n"; }

    
# make sure that directories are the same.
    
    while ( $a = pop (@dir) )   # pop last element from @dir  
    { 
        $b = pop (@file);       # pop last element from @file 
        
#    print "array element from dir = $a, & from filename  = $b\n ";
        unless ($a eq $b) 
        { 
            print FOUT "$name: no match between $a and $b (elements of saved_dir % last_saved_file strings)\n"; 
            return ($FAILURE);  # no information available on last saved run
        }
    } 

if ($debug) {print FOUT "directories are matching\n";}

# get the run number % extension out of the filename
($num, $ext)  = split (/\./,$filename);
if ($debug) { print FOUT "$name:  num = $num, extension = $ext\n"; }
$num = $num + 0;  # make sure it's an integer

# set flag for final saved file
unless ( $ext =~ /_v/i) { $info = $TRUE ;  } # final saved file
else                    { $info = $FALSE; }  # intermediate
 
if ($debug)
{
    if ($info) { print FOUT "$name: Found a final saved file for run $num \n"; }
    else { print FOUT "$name: Found an intermediate saved file for run $num \n"; }
}
return ($SUCCESS, $num, $info);  # return 

}
sub check_one
{
    # inputs:   value  string  name
    
    my ($value, $string, $name) =  @_;
    unless ($value) 
    {
        print_3( $name, "FAILURE: $string not supplied. ",$MERROR,$DIE ) ;
    }
    return;
}
1;



































