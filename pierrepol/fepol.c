/********************************************************************\
  Name:         fepol.c

        This version supports all POL
        ------------------------------

  Acquire 32 channels of a SIS3801 multi channel scaler plus MAX_CHAN_SIS3801B
  of a 2nd multi channel scaler.
  Generate individual channel SUM by cycle and ODB them in /variables
  Keep individual channel cumulative scaler histogram
  Generate periodically event containing n_histo_a + n_histo_b histograms
   2 histograms are composed of the sum of 16 scaler histos (0-15, 16-31)
   3rd histogram is the user bits   
   .. to n histogram for each individual channels of multiscaler B
  
  cycle_start() :
  The cycle is started by cycle_start() at BOR or during loop while running.
    reset the gbl_BIN time bin.
    clear the FIFO
    set flag gbl_IN_CYCLE
   
  FIFO_acq() :
  The polling function will call FIFO_acq which does the following:
   Check gbl_IN_CYCLE  to find out if the cycle is still in progress.
    yes : read 1/2 FIFO or FIFO FULL flags; read FIFO if either is set
    no  : flush FIFO, make cycle histograms and update run histograms

  frontend_loop():
  The looping function is called periodically independently of the ACQ.
  The main function of the loop is to switch helicity when cycle is
  complete AND while the run is in progress:
   Check if running:
    yes : check if still gbl_IN_CYCLE
          yes : nothing to do
          no  : wait for all Equipments to run
                start new cycle
    no  : nothing to do
  
  gbl_BIN : current time bin of the current cycle.
            cleared at BOR and every end of cycle (when full cycle is taken)

  Epics Scans:
  The frontend (ppc) can scan Epics devices (e.g. NaCell, Laser, Field) through direct access
  to Epics.

  Camp Scans:
  The frontend (ppc) can scan CAMP devices through direct access to CAMP.

  ** NOTE: fe_epics   ***
      This pgm also sends data to EPICS (i.e. some scaler totals, sums, differences etc). This
      is completely separate from the direct access frontend Epics device scan, and is handled by
      fe_epics running on the (linux) host. 
  
  Created by:   Pierre-Andre Amaudruz
  Adapted for BNMR expt 1a/1b by: Renee Poutissou
  Extensively modified by Suzannah Daviel

March 2003: Renee Poutissou
  Adapted again for BNQR merging TDmusr and Imusr type experiments


CVS log information:
$Log: febnmr.c,v $
Revision 1.68  2006/07/21 18:26:33  suz
fix helicity sleep bug

Revision 1.67  2006/06/26 19:35:33  suz
fix bug so threshold alarm is reset; check for end-of-run while reconnecting camp

Revision 1.66  2006/06/21 18:40:11  suz
add mode 1j (modes 20 and 1c combination)

Revision 1.65  2006/06/20 22:34:42  suz
make sure threshold alarms stay off when not running

Revision 1.64  2006/06/19 17:27:35  suz
added broken scaler input fix for bnqr; define HACK2TO1

Revision 1.63  2006/06/16 23:55:17  suz
add threshold alarms for bnmr

Revision 1.62  2005/10/18 22:03:43  suz
fix bug for FSC version

Revision 1.61  2005/10/11 23:12:18  suz
check on whether rf tripped added while run is off & odb updated - otherwise alarm could not be reset when run is off

Revision 1.60  2005/08/13 01:37:40  suz
move checks on epics dual channel params to rf_config

Revision 1.59  2005/08/04 00:51:29  suz
rewrite scaler routine to add 2e; get rid of % on threshold

Revision 1.58  2005/07/26 22:28:07  suz
remove TEMP for POL, fix for thresh % in str

Revision 1.57  2005/07/26 02:17:36  suz
thresholds are back to (%) and add more ifdef POL

Revision 1.56  2005/07/19 21:27:31  suz
new for POL: P+,laser,FC15 scaler readings, 3 thresholds, Epics bias, DVM[2] changed, alarms etc

Revision 1.55  2005/07/04 19:05:21  suz
remove % from odb hardware threshold names (e.g. cycle thr1 (%))  to accomodate POL

Revision 1.54  2005/06/08 00:08:12  suz
add flag to disable Epicscheck at BOR; fix bug that lost neutral beam histos for 2 scalers after adding ubit histo; set watchdog & try to avoid rf_config timeout when epics connection is slow

Revision 1.53  2005/05/20 17:40:20  suz
remove spurious debugging statement

Revision 1.52  2005/05/20 17:29:13  suz
setup 2d like 20; load single freq not a file

Revision 1.51  2005/05/18 19:33:06  suz
add ifdefs around psmLoadFreqDM_ptr & message since version for fsc not yet available

Revision 1.50  2005/05/18 05:04:50  suz
add another ifdef BNMR

Revision 1.49  2005/05/17 23:25:48  suz
add randomize 2a, pulse pairs etc.

Revision 1.48  2005/04/27 20:46:44  suz
modify helicity checking; add a param to disable checking

Revision 1.47  2005/04/22 21:06:10  suz
set helicity to down at end of run. Review helicity checks and messages

Revision 1.46  2005/04/21 20:01:17  suz
use helicity set value not readback; set PPG internal start at EOR; read/check EPICS single/dual ch switch param against DAQ

Revision 1.45  2005/04/20 20:08:50  suz
change helicity to use set value (gbl_ppg_hel) not read value; problems in dual mode

Revision 1.44  2005/04/18 17:59:59  suz
send an internal strobe to PSM in mode 20

Revision 1.43  2005/04/08 19:33:43  suz
add a second cycle threshold

Revision 1.42  2005/04/08 18:01:32  suz
remove debugging messages for helicity; some checks are done only for single channel mode

Revision 1.41  2005/04/08 17:19:06  suz
changes to setting and clearing long watchdog

Revision 1.40  2005/03/16 19:02:59  suz
disable psm for modes which do not use it

Revision 1.39  2005/02/28 21:45:10  suz
small changes for pol

Revision 1.38  2005/02/22 23:20:27  suz
fix bugs for pol; add dbug() to list debugging flags

Revision 1.37  2005/02/21 18:22:20  suz
changes to set_init_hel_state; allow POL without epics

Revision 1.36  2005/02/16 20:50:57  suz
put ifdefs around deferred transition, which does not work on isdaq01. Changes to helicity switching; helicity switch hardware will be altered rather than ppg (dual channel mode)

Revision 1.35  2005/02/14 20:38:47  suz
change helicity routines for expected mods to helicity switch. Now VME control for PPG helicity output for dual and single channel mode

Revision 1.34  2005/02/04 22:17:28  suz
changes to debug direct access to Helicity Epics channels

Revision 1.33  2005/02/03 22:43:03  suz
add changing the ppg polmsk for dual channel mode

Revision 1.32  2005/02/02 18:31:25  suz
helicity_read changed for dual channel mode, changes to frontend_loop

Revision 1.31  2005/01/26 21:51:44  suz
big changes preparing for dual channel mode; add 1a/1b randomize freqs; 1g flip every cycle; split code into more subroutines; mods to ppg for dual ch mode; helicity read using direct channel access; not fully tested

Revision 1.29  2004/12/10 22:59:25  suz
fix bug: trigger read of DVM chan 2 for DAC scan

Revision 1.28  2004/12/02 18:01:28  suz
POL changes: add read of DVM[2] (Faraday cup) to CYCI bank word 7 for DAC scan; if no handshake then skip cycle, no deferred transition for Midas 1.9.4

Revision 1.27  2004/11/24 01:37:30  suz
POL jump version now working; cleared epics_params.epics_Bad_Flag; added check on Midas versionas deferred stop doesn't work for older Midas

Revision 1.26  2004/11/17 00:07:24  suz
add deferred transition to send final data, and first version of POL's jump in the scan

Revision 1.25  2004/10/25 19:04:37  suz
add changes for PSM

Revision 1.24  2004/10/20 01:01:53  suz
POL: changes for Phil's expt (POL+EPICS)

Revision 1.23  2004/09/14 18:01:38  suz
remove some messages for POL (d11 flag)

Revision 1.22  2004/09/09 21:27:22  suz
add bad scan flag for pol

Revision 1.20  2004/07/08 17:22:47  renee
fix a bug in bank CYCL; move some CAMP code in conditionnal assembly CAMP_ACCESS

Revision 1.19  2004/07/05 22:51:27  suz
POL uses d11=1 to turn off messages

Revision 1.18  2004/07/05 17:29:44  suz
Put ifndef POL around some comments (cycle_start); not wanted for POL

Revision 1.17  2004/07/05 16:52:38  suz
fix two bugs

Revision 1.16  2004/06/30 00:32:27  suz
change some comments

Revision 1.15  2004/06/14 16:47:46  suz
remove a debug message

Revision 1.14  2004/05/20 18:40:10  suz
add support for POL. Add more #ifdef EPICS_ACCESS

Revision 1.13  2004/05/03 18:22:50  suz
add helicity flip sleep time (hotlinked); beam off at end of run; field scan

Revision 1.12  2004/03/31 00:03:45  suz
generalize Epics device names (Xx)

Revision 1.11  2004/03/09 21:48:49  suz
changes for midas 1.9.3

Revision 1.10  2004/01/14 19:58:39  suz
mdarc/camp hostname now mdarc/camp/camp hostname (same as MUSR)

Revision 1.9  2003/12/09 19:52:43  suz
fix more bugs; check for loss of camp connection

Revision 1.8  2003/12/09 18:31:31  suz
fix bug in camp scan

Revision 1.7  2003/12/01 19:39:39  suz
epics now reconnects & devices connected only when needed. Cycle_start rearranged. POL changes are not in this version

Revision 1.5  2003/07/31 21:02:17  suz
Fix up stopping the run (by mdarc) for all types

Revision 1.4  2003/07/29 19:10:20  suz
add client flag so mdarc can stop the run on failure; also mdarc can now stop run after N cycles

Revision 1.3  2003/06/26 21:30:22  suz
add debugging so Epics scan value not overwritten

Revision 1.2  2003/06/24 23:17:12  suz
Add Epics Field scan

Revision 1.1  2003/05/01 21:15:50  suz
new version supports all expt types

\********************************************************************/

#include <stdio.h>
#include <math.h>



#define FAILURE 0

#include "midas.h"
#include "sis3801.c"
#include "experim.h"
#include "../ppg/trPPG.h"

/* Currently, the VMEIO module is only in the BNMR DAQ crate 
   now VMEIO is defined in the Makefile */
#ifdef VMEIO
#include "vmeio.h"
#endif

#ifdef PSM
#include "../psm/trPSM.h"
#else
#ifdef FSC
#include "../fsc/trFSC.h"
#endif
#endif

#ifdef EPICS_ACCESS    /* direct EPICS access by the frontend (ppc) defined in Makefile */
#include "bnmr_epics.h"
#include "connect.h"      /* for scanning Epics device */
#else
BOOL hel_warning;
#endif

#include "fepol.h"
#include "pol_dac.h"



/*-- Frontend globals ------------------------------------------------*/

/* odb handles */
/*extern HNDLE hDB, hFS, hEPD, hEPM, hBsf ; /* these defined in febnmr.h, assigned in pol_init.c */
HNDLE   hHs=0, hRR=0, hCT1=0, hCT2=0, hFT=0, hHM=0, hDC=0, hFF=0, hPd=0, hSK=0; 
HNDLE   hR1, hR2, hR3;
HNDLE   hCamp=0, hKey=0;

/* Individual scaler channel */
typedef struct {
    double  sum_cycle;   /* cycle histogram sum */
    DWORD   nbins;
    DWORD   *ps;         /* cumulative histogram */
} MSCALER;

/* Histogram struct for Event builder */
typedef struct {
    double  sum;                    /* not used yet */
    double  bin_max;                /* not used yet */
    DWORD   nbins;
    DWORD   *ph;         /* cumulative histogram over run */
} MHISTO;

MHISTO   histo[N_HISTO_MAX];
MSCALER  scaler[N_SCALER_MAX];    /* max  (real + any calculated) */ 

extern  run_state;


static char bars[] = "|/-\\";
static int  i_bar;

/* Aug 2003, Renee, d6 is used extensively by routines in file EpicsStep.c
   so I change the "freq scan" to use d10 instead */ 
INT     ddd = 0, dddd=0, dd=0 , d3=0, d5=0, d7=0, d8=0, d9=0, d10 = 0,  d11=1;
INT     dhw=0, dpsm=0, dj=0, dran=0, ddac=0, d12=0;
INT   dh=0; /* helicity */
INT dq=0; /* set to 1 for manual ppg start */
INT debug_2e;

/*   
    dh  helicity 
    dq=0;  set to 1 for manual ppg start 
    dd  debugs are in sis_setup and poll_event
    ddd frontend_loop, begin_run, debug scalers, histo banks
    dddd indicates if running in cycle in frontend loop, vmeio
    d3 = reference thresh
    d5= Epics scan (higher level ... bnmr_epics.c)
    d6  Epics scan (lower level... EpicsStep.c)
    d7 = Epics watchdog (to keep Epics channel open )
    d8 display: for debugging, don't overwrite  scan values in cycle_start 
    d9 client flag and automatic stop and delayed transition
    d10 = freq scan  
    dc = Camp scan (is in camp_acq.h)
    d11 silent mode; do not display anything 
    d12 debug POL DAC scan
    dpsm debug psm; also set pdd=1 for trPSM. If using pdd=1 may need to set midas watchdog 

    ddac set to 1 if no feDVM client available or hardware off (bypass check on handshake);
    dran debug randomizing freq values (can also use dr)
    dhw  set to 1 to turn off helicity warning messages (for testing when beam kicker in wrong position)
*/

INT     gbl_run_number;      
BOOL    gbl_IN_CYCLE;              /* software in cycle flag */
INT     gbl_FREQ_n;                /* current cycle # in freq/Camp/Dac sweep */
INT     gbl_inc_cntr;              /* seems to be used only for Epics scan */
DWORD   gbl_CYCLE_N;               /* current valid cycle */
DWORD   gbl_SCYCLE_N;              /* current super cycle */
DWORD   gbl_SCAN_N;                /* current scan */
DWORD   gbl_watchdog_timeout;      /* midas watchdog timeout */
BOOL    gbl_watchdog_flag=TRUE;         /* midas watchdog flag */
BOOL    gbl_dachshund_flag=FALSE;  /* true when a long watchdog timeout is set */
BOOL    gbl_hel_flipped;
DWORD   userbit_A,userbit_B;           /* userbits for last bin */
BOOL    waiteqp[4] = {TRUE,TRUE,TRUE,TRUE};
BOOL    hot_rereference=FALSE, lhot_rereference=TRUE;
BOOL    lhot_reref1=FALSE,lhot_reref2=FALSE, lhot_reref3=FALSE;
BOOL    hot_reref1=FALSE, hot_reref2=FALSE, hot_reref3=FALSE;
BOOL    skip_cycle=FALSE;
BOOL    fill_uarray = FALSE;  /* uarray contains users_bits per dwell times */
BOOL    gbl_transition_requested= FALSE;
BOOL    gbl_waiting_for_run_stop = FALSE; /* global flag to prevent restarting cycle on error */
DWORD   n_bins;          /* same as # or dwell times = n_his_bins except 2a pulse pairs compaction modes 
			                      and e2e mode  */
DWORD   n_his_bins;       /* number of hist bins for e2a pulse_pairs compaction modes (1st,2nd,diff) 
                                              and e2e mode */
INT     exp_mode;        /* =1 for all TYPE1 ; = 2 for others */
INT     n_histo_total;   /* number of histos for this run */
INT     n_histo_a;       /* number of histos in scalerA for this run */
INT     n_histo_b;       /* number of histos in scalerB for this run */
INT     n_scaler_total;  /* number of scalers for this run */
INT     n_scaler_cum;  /* number of scalers for this run */

/* assign these as DWORD (unsigned long int) for large freq. values */
DWORD     freq_val;        /* frequency value to load */
DWORD     freq_start;      /* start value for frequency sweep */
DWORD     freq_stop;       /* stop value for frequency sweep */

INT     freq_inc;        /* increment value for frequency at each sweep */
INT      freq_ninc;       /* number of frequency increments to do */
float   set_camp_val;  /* camp value to set */
float     camp_start;      /* start value for camp sweep */
float     camp_stop;       /* stop value for camp sweep */
float     camp_inc;        /* increment value for camp at each sweep */
INT     camp_ninc;       /* number of camp increments to do */
float   dac_val;  /* dac value to set */
float   dac_start;      /* start value for dac sweep */
float   dac_stop;       /* stop value for dac sweep */
float   dac_inc;        /* increment value for dac at each sweep */
INT     dac_ninc;       /* number of dac increments to do */
INT     gbl_scan_flag=0; /* scan type encoded at begin_of_run for histo_read */
INT     rftime=60;       /* check_file_time uses this */
INT  rf_count=0;

INT     epicstime;
char ppg_mode[3];
INT gbl_fix_inc_cntr;
/* Scan Type Flags  */
BOOL rf_flag = FALSE;   /* flag indicates RF scan (1f, 1a,1b); not used for Type 2, all type 2 use RF */    
BOOL camp_flag = FALSE;   /* flag indicates CAMP scan */
BOOL  epics_flag = FALSE; /* flag to indicate direct EPICS scan by frontend i.e. NaCell/Laser/Field */ 
BOOL mode10_flag = FALSE; /* flag to indicate ppg mode 10 (SCALER mode) is selected */
BOOL mode1g_flag = FALSE; /* flag to indicate ppg mode 1g is selected */
                         /* mode 1g is a combination of SLR (20) and 1f */
BOOL mode1j_flag = FALSE; /* flag to indicate ppg mode 1g is selected */
                         /* mode 1j is a combination of SLR (20) and 1c */

BOOL random_flag = FALSE; /* randomize the RF values (supported for types 1a,1b 2a) */
BOOL e2a_flag,e2a_pulse_pairs,e2a_compress; /* flags for running "2a" */ 
BOOL e2e_flag; /* flag for running mode 2e */
BOOL pol_DAC_flag = FALSE;/* mode 1h; used by POL/CFBS to control McGill PS */
                          /* CFBS -> Colinear Fast Beam Spectroscopy */
                          /* First exp using the above is E920 */
BOOL pol_Rmon_flag;
BOOL CAMP_repeat_flag = FALSE;
BOOL first_time = FALSE;  /* used by routine cycle_start to know if this is begin_run */
BOOL first_cycle = FALSE;  /* used by readout routines to know if this is begin_run */
BOOL client_check = TRUE; /* if true, mdarc will stop the run on error (assuming mdarc is running!) */
char who_stops_run[7]; /* mdarc or user */
BOOL gbl_first_call = TRUE; /* used for deferred stop routine */
/* Epics scan variable
   Most are in header file */
INT     Epics_last_time, Hel_last_time, RFtrip_last_time;
BOOL    gbl_epics_live, gbl_hel_live; /* flags to indicate Epics channel should be kept alive */
INT     N_Scans_wanted;
BOOL    hold_flag = FALSE; /* BNMR hold flag */
BOOL    flip=FALSE; /* helicity flipping flag */
INT     gbl_ppg_hel; /* helicity state according to PPG */
DWORD   lastBCDfreq; /* needed for checking fsc is correct */
DWORD   freq0A;
INT     ncycle_sk_tol; /* number of cycles to skip AFTER returning in tolerance */
INT     loop_error_cntr=0;
float RmonVal;
int psleep=0;
HNDLE hVar;
HNDLE hHand,hReq,hRmon;
HNDLE hHand2,hReq2 ; /* Needed to trigger update of DVM when not scanning the DAC */
BOOL bad_scan_flag = FALSE;
INT size_bsf; /* will be set to sizeof(bad_scan_flag) */
DWORD   gbl_jinc1,gbl_jinc2; /* jump parameters */
BOOL    gbl_jump_now;
float   gbl_prejump_value;  /* used by NaCell */

char Rname[32]; /* name of EPICS channel to be read (helicity (BN[MQ]R) bias (POL) );*/
INT Rchid;  /* channel ID of above channel */



/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fePOL";




/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 000;

/* maximum event size produced by this frontend 
   must be less than MAX_EVENT_SIZE (midas.h)*/
#ifdef SMALL_MEMORY
INT max_event_size = 32768; /* small memory PPC */
#else
 INT max_event_size = 520000; 
#endif
/* buffer size to hold events  NOT RELEVANT FOR VXWORKS*/
INT event_buffer_size = 1400000;
/* maximum event size (for fragmented events only) */
INT max_event_size_frag = 5*1024*1024;

#define POLL_FIFO_ACQ 10          /* in ms */
#define POLL_EQUIPMENTS 100       /* in ms */
#define POLL_MONITOR   500      /* polled every 0.5 s */
/*-- Equipment list ------------------------------------------------*/
/* Note: Equipment FIFO_ACQ is polled every POLL_FIFO_ACQ ms.
   
         Equipments Histo, Cycle_Scalers, Hdiagnosis and Info ODB should
	 be polled with the same time period, i.e. every POLL_EQUIPMENTS ms.
*/
  EQUIPMENT equipment[] ={
  { "FIFO_acq",           /* equipment name */
    1, 1,                 /* event ID, trigger mask */
    "",                   /* Don't send data */
    EQ_PERIODIC,           /* equipment type */
    0,
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING,           /* read only when running */
    POLL_FIFO_ACQ,        /* poll interval */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub-event in the super event */
    0,                    /* don't log history */
    "", "", "",
    FIFO_acq,             /* readout routine */
    NULL,NULL,
    NULL,
  },
  
  { "Histo",              /* equipment name */
    2, 1,                 /* event ID, trigger mask */
    "SYSTEM",             /* send data banks in SYSTEM event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING | RO_EOR,  /* read when running and on end_of_run */
    POLL_EQUIPMENTS,      /* polling period */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub-event in the super event */
    0,                    /* log history */
    "", "", "",
    histo_read,           /* readout routine */
    NULL,NULL,NULL,
  },

  { "Cycle_Scalers",      /* equipment name */
    3, 1,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING | RO_ODB,  /* read when running and on transitions */
    POLL_EQUIPMENTS,      /* polling period */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub-event in the super event */
    0,                    /* log history */
    "", "", "",
    scalers_cycle_read,   /* readout routine */
    NULL,NULL,NULL,
  },

  { "Hdiagnosis",         /* equipment name */
    4, 1,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    FALSE,                /* disabled */
    RO_RUNNING,           /* read when running  */
    POLL_EQUIPMENTS,      /* polling period */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub-event in the super event */
    0,                    /* log history */
    "", "", "",
    diag_read,            /* readout routine */
    NULL,NULL,NULL,
  },

  { "Info ODB",           /* equipment name */
    10, 0,                /* event ID, trigger mask */
    "",                   /* no banks sent */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "FIXED",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING | RO_ODB | RO_EOR,  /* read when running; send to odb */
    500,                  /* polling period */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub-event in the super event */
    0,                    /* log history */
    "", "", "",
    info_odb,             /* readout routine */
    NULL,NULL,NULL,
  },

/* note - setting RO_ODB means event is copied to
      /Equipment/equipment_name/variables
  i.e./Equipment/Info ODB/variables 
*/
  { "Monitor",             /* equipment name */
    11, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",               /* format */
    TRUE,                 /* enabled */
    RO_RUNNING|RO_BOR|RO_ODB, /* read when running and at BOR transitions */
    POLL_MONITOR,         /* polled */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub event */
    0,                    /* log history */
    "", "", "",
    monitor_event,               /* readout routine */
    NULL,NULL,NULL,       /* keep null */
  },

{ "" }
};



/*-- Call_back  ----------------------------------------------------*/
void call_back(HNDLE hDB, HNDLE hkey, void * info)
{
  printf("odb (hot link) touched; hkey= %d \"%s\" \n",hkey,info);
}





/*-- Call_back  ----------------------------------------------------*/

void hot_bad_scan(HNDLE hDB, HNDLE hkey, void * info)
{
  INT status;
  
  printf("hot_bad_scan: odb (hot link) touched; hkey= %d \"%s\" \n",hkey,info);
  if(bad_scan_flag)
    printf("host_bad_scan: bad scan flag is already set\n");
  else
    printf("host_bad_scan: setting bad scan flag true for this scan\n");
  
  bad_scan_flag = TRUE;
  return;
}



/*-- Dummy routines ------------------------------------------------
  called by mfe.c                                */
INT interrupt_configure(INT cmd, INT source[], PTYPE adr){return 1;};

#ifdef DEFERRED
BOOL wait_end_cycle(INT run_number, BOOL flag)
{
  
  INT i;
  float max_cycle_time;
  static INT first_time,max_wait;
  INT elapsed_time;
  
  if(gbl_first_call)
    {
      printf("\n");
      if(d9)
	printf("\nwait_end_cycle: starting with waiteqp[HISTO]=%d, gbl_first_call=%d\n",
	       waiteqp[HISTO],gbl_first_call);
      cm_msg(MINFO,"wait_end_cycle","waiting for final cycle to complete before stopping run");
      first_time=ss_time();
      gbl_transition_requested = TRUE; /* prevents restarting cycle */
      /* calculate the approximate time one cycle should take */
      max_cycle_time = fs.output.dwell_time__ms_ * (float)  fs.output.num_dwell_times;
      max_wait = (INT) ((max_cycle_time *1.1/1000) + 0.5); /* add 10%, convert to seconds and round up */
      if (max_wait < 1) 
	max_wait = 3; 
      printf("wait_end_cycle:time to complete one cycle:%.1f ms (or %.0f sec)\n",max_cycle_time,max_cycle_time/1000);
      printf("    Maximum time to wait for last histo event (before timeout) is: %d sec\n",max_wait);
      gbl_first_call = FALSE;
      ss_sleep(200);
      cm_yield(100);
    }
  
  if(waiteqp[HISTO])
    {
      elapsed_time=ss_time()-first_time;
      if(d9)
	printf("wait_end_cycle: Waiting for last HISTO event... max_wait=%d sec, elapsed time=%d sec\n",
	       max_wait,elapsed_time);
      if(elapsed_time < max_wait)
	return FALSE; 
      
      cm_msg(MINFO,"wait_end_cycle","timeout waiting for last event after %d seconds. Stopping run now",elapsed_time);
    }
  else
    cm_msg(MINFO,"wait_end_cycle","last histo event has been sent, stopping run now");
  
  
  gbl_transition_requested=FALSE;
  return TRUE;
  
}
#endif

/*-- Frontend Init -------------------------------------------------
  called by mfe.c                                */
INT frontend_init()
{
  INT status,i,size;
  char str[128];
  BOOL watchdog_flag;
  
  

  printf ("frontend_init: %s frontend code is starting (%s)\n",BEAMLINE, beamline);
#ifdef HACK2TO1
  printf (" note: code is built for scaler with broken input \n");
#endif
  status =   cm_register_transition(TR_STOP, post_end_run, 750) ;
  if(status != CM_SUCCESS)
     {
      cm_msg(MERROR,"frontend_init","Failed to register transition post end_of_run (%d)\n",status);
      return status;
    }

  cm_get_watchdog_params(&watchdog_flag, &gbl_watchdog_timeout);
  printf("frontend_init: current watchdog parameters are gbl_watchdog_timeout=%d; watchdog_flag=%d \n",
	 gbl_watchdog_timeout, watchdog_flag);

  exp_mode = -1;

#ifdef EPICS_ACCESS
  /* initialize epics scan globals in bnmr_epics.h */
  epics_params.Epics_bad_flag = 0;
  epics_params.NaCell_flag= epics_params.Laser_flag = epics_params.Field_flag = FALSE;
  epics_params.XxRchid = epics_params.XxWchid  =-1;
  Rchid=-1;
#endif
  /* common initialize for type1 and type2 */
  status = pol_init(equipment[FIFO].name,equipment[INFO].name,equipment[CYCLE].name );
  if(status != FE_SUCCESS)
    return status;
  
#ifdef VXWORKS 

#ifndef EPICS_ACCESS
  printf("INFORMATION:EPICS ACCESS is not defined in this code; no EPICS scan available\n");
#else
  caInit(); /* calls  ca_task_initialize(); */
  
  /* GetIDs for Epics scan ... now done at begin run */
  /*  EpicsGetIds( &epics_params );
      
  /* Time for watchdog (keeps channel open) on direct access to Epics channels */
  /* Epics_last_time = ss_time(); */
  
#endif /* Epics access */
#endif /* VxWorks */
  
  /* Init all pointers to potential MALLOC calls to NULL */
  for (i=0 ; i < N_SCALER_MAX ; i++)
    {
      scaler[i].ps = NULL;
    }
  for (i=0 ; i < N_HISTO_MAX ; i++)
    {
      histo[i].ph = NULL;
    }
#ifdef VXWORKS
  rpcInit( );  /* initialize needed for any VxWorks access  */
   rpcTaskInit( );
#endif
  

  d11=1;  /* do not display anything for POL */
  status = pol_find_keys();
  if(status != DB_SUCCESS)
    return status;
  
  size_bsf = sizeof( bad_scan_flag);

  if(print_hardware_flags() != SUCCESS)
    return FE_ERR_HW;

  status = al_reset_alarm(NULL); /* reset all alarms */
 if(status != CM_SUCCESS)
      cm_msg(MINFO,"frontend_init","problem trying to reset alarms automatically (%d)\n",status);
 else
   printf("frontend_init: Info... cleared all alarms on main status page\n");

 printf("MAX_EVENT_SIZE=%d\n",MAX_EVENT_SIZE);
  printf("\n End of routine frontend_init. PPC is READY \n");
  return FE_SUCCESS;
}


/*-- Frontend Exit -------------------------------------------------
  called by mfe.c                                */
INT frontend_exit()
{
  int i;
  INT status;

  /* free SIS FIFO memory */
#ifdef TWO_SCALERS
  if (pfifo_A) free(pfifo_A);
  /* force acquisition stop */
#ifdef VXWORKS
  sis3801_next_logic(SIS3801_BASE_A, DISABLE_NEXT_CLK);
#endif
#endif

  if (pfifo_B) free(pfifo_B);
#ifdef VXWORKS
  sis3801_next_logic(SIS3801_BASE_B, DISABLE_NEXT_CLK);
#ifdef INTERRUPT
  sis3801_int_source_disable(SIS3801_BASE_B, SOURCE_CIP);
#endif
#endif
  printf("\nfree scaler ");
  for (i=0 ; i < N_SCALER_MAX ; i++)
  {
    if (scaler[i].ps != NULL)
    {
      printf(" %d", i);
      free(scaler[i].ps);
      scaler[i].ps = NULL;
    }
  }
  printf("\n");
  printf("free histo ");
  for (i=0 ; i < N_HISTO_MAX ; i++)
  {
    if (histo[i].ph != NULL)
    {
      printf(" %d ", i);
      free(histo[i].ph);
      histo[i].ph = NULL;
    }
  }
  printf("\n");


  /*Disable the PPG module just in case */
  ppgStopSequencer(PPG_BASE);
  ppgDisableExtTrig(PPG_BASE);

#ifdef PSM
  /* Shut down the PSM */
  status = disable_psm(PSM_BASE);
  if(status == SUCCESS)
    printf("frontend_exit: PSM is now disabled\n");
  else
    cm_msg(MINFO,"frontend_exit","error disabling PSM");
#endif

  /* shut down access via Epics */
#ifdef EPICS_ACCESS 
  caExit(); /* closes any open channels for Epics  */
#endif

  printf("frontend_exit:  mfe procedure exiting \n");
  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------
called periodically by a loop in mfe.c           
*/
INT frontend_loop()
     /*
       - Main function for re-starting a cycle.
       - if RUNNING and off cycle 
       - start new cycle
     */
{
  INT status,size, i=0;
  DWORD rf_data=5;
  INT running,stopped;


#ifndef INTERRUPT
  /* Has the PPG stopped ? this will only work if PPG is programmed to perform ONE CYCLE only  
     i.e. is  restarted each new cycle  (otherwise use interrupt method to determine end of one cycle )  */
  if (run_state == STATE_RUNNING && gbl_IN_CYCLE)  /* Run started AND PPG is running */
    {
      status = ppgStatusRead(PPG_BASE);
      running = status & 2;
      if (!running)  
	{
	  /* ppgStopSequencer(PPG_BASE);  /* stop the sequencer (it's stopped anyway) */
          if(ddd)printf("frontend_loop: END of CYCLE.... PPG stopped, disabling SIS\n");
	  /* end of cycle */  
	  sis3801_next_logic(SIS3801_BASE_B, DISABLE_NEXT_CLK); /* disable SIS */ 
	  gbl_IN_CYCLE = FALSE;
	}
      else
	{
	  /* csrdataB = sis3801_CSR_read(SIS3801_BASE_B, CSR_FULL);  read CSR */
	     if(ddd)printf ("frontend_loop: PPG IS running\n");
	}
    }
#endif
    

  
#ifdef EPICS_ACCESS 
  if ((ss_time() - Epics_last_time) > 5)  /* is it time to check?  */
    {
      if(gbl_epics_live)
	{
	  /* Maintain EPICS scan channels live only when running epics scan */
	  /* check that channel(s) have not disconnected */
	  if(d7)printf("frontend_loop: calling epics_watchdog\n");
	  status = epics_watchdog( &epics_params, FALSE);
	  
	  if (status != SUCCESS)
	    printf("frontend_loop: error from epics_watchdog\n");
	  Epics_last_time = ss_time(); 
	}
    } /* ss_time */
  
  if ((ss_time() - Hel_last_time) > 5)  /* is it time to check?  */
    {
      if(gbl_hel_live)
	{
	  /* Maintain EPICS Helicity channel live (or Bias for POL)  */
	  /* check that channel has not disconnected */
	  if(d7)printf("frontend_loop: calling epics_watchdog\n");
	  status = epics_watchdog( &epics_params, TRUE);
	  
	  if (status != SUCCESS)
	    printf("frontend_loop: error from epics_watchdog for helicity\n");
	  Hel_last_time = ss_time(); 
	}
    } /* ss_time */
#endif
  
  
  if (run_state == STATE_RUNNING)
    { /*      RUNNING 
	      check if cycle is finished AND histo update being done */
      if (!gbl_IN_CYCLE)
	{ /* not in cycle */
	  if(dddd)printf("frontend_loop: Not in cycle\n");

#ifdef DEFERRED 
	  if(gbl_transition_requested) /* we are waiting for final histos before stopping the run */
	    {
	      if(d9 || ddd)
		{
		  printf("frontend_loop: a deferred transition has been requested\n");
		  if (waiteqp[HISTO])
		    printf("frontend_loop: waiting for HISTO to run \n");
		  else
		    printf("frontend_loop: HISTO equipment has run, not restarting cycle\n");
		}
	      return SUCCESS; /* do not restart the cycle as we are waiting to stop */
	    }
#endif /* DEFERRED */	  
	  
	  /* wait until other equipment have run */
	  if (waiteqp[FIFO] || waiteqp[HISTO] || waiteqp[CYCLE] || waiteqp[DIAG])
	    return SUCCESS; /* cycle is not yet finished i.e. all equipments have not run */
	  
	  /* TEMP debug read the ppg status reg */
	  if(dq)
	    {
	      i=ppgStatusRead(PPG_BASE);
	      printf("frontend_loop: expect PPG to be stopped, ppgStatusRead=0x%x\n",i);
	    }
	  /* check if we are trying to stop this run */
	  if( loop_error_cntr> 0 )
	    {
	      if ((loop_error_cntr % 1000000) ==0)
		{  /* repeat the message every so often */
		  printf("frontend_loop: waiting for %s to stop the run ...(%d)\n",
			 who_stops_run,loop_error_cntr);
		}
	      ss_sleep(1500);
	      cm_yield(100); /* let mdarc have a chance to stop this run */
	      loop_error_cntr++;
	      return SUCCESS;
	    }

  /* POL does not use helicity */
	  if (!d11 && !d8)
	    printf(";end"); 
	  
	  if (fs.hardware.num_cycles > 0)  
	    {
	      /* fixed number of cycles per run is enabled
		 Stop run after n_cycles +1 (first cycle is thrown away)  */
	      if (gbl_CYCLE_N >= fs.hardware.num_cycles+1)
		{
		  if(loop_error_cntr>0)  /* TEMP */
		    printf("error.. don't expect to get here if loop_error_cntr>0 (%d)\n",
			   loop_error_cntr);
		  status=prepare_to_stop();
		  cm_msg(MINFO,"frontend_loop",
			 "waiting for %s to stop the run after reaching %d cycles...\n",
			 who_stops_run,fs.hardware.num_cycles);
		  return status;  
		}
	    }	    
	  /* Request for Fixed number of cycles is disabled (=0) 
	     i.e. free running, or we haven't reached the limit yet  */
	  
	  
	  
	  if(gbl_waiting_for_run_stop)
	    {
	      printf("frontend_loop: Not restarting cycle... waiting for run to be stopped after fatal error or max no. cycles reached\n");
	      return SUCCESS;
	    }

	  if(pol_Rmon_flag)
	    {
	      printf("frontend_loop: Not restarting cycle... waiting for monitor_event to run\n");
	      ss_sleep(500);
	      cm_yield(100);
	      return SUCCESS;
	    }

	  
	  if(ddd)printf("frontend_loop: restarting cycle\n");
	  
	  /* Start new cycle */
	  status = cycle_start();
	  if(status != SUCCESS)
	    {
	      status=prepare_to_stop();	  
	      cm_msg(MINFO,"frontend_loop",
		     "waiting for %s to stop the run after failure from cycle_start(%d)",
		     who_stops_run,status);
	      return status;
	    }
	  
	} /* end of not in cycle */ 
      else
	{ /* running in Cycle (i.e. gbl_IN_CYCLE is TRUE) 
	     nothing to do */
	  if(dddd)
	    printf("frontend_loop: running in cycle; nothing to do; exit\n");
	  return SUCCESS;
	} 
    } /* end of RUNNING */
  else
    { /* NOT running */
      /* nothing to do */
      if(dddd)printf("frontend_loop: not running, nothing to do; exit\n"); 
      return SUCCESS;
    } /* Not running */
}

/*-- Begin of Run sequence ------------------------------------------
                              called by mfe.c                        */
INT begin_of_run(INT run_number, char *error)
/*
  - update fs struct based on settings names (no hot links)
  - cleanup and book previous histo and scalers memory
  - check acq settings
  - book new histograms
  - reset histo
  - start acq

note: mdarc initializes client status flag for frontend on a prestart transition
       - set flag to success on successful begin of run
*/
{
  INT  i, h, status, size;
  char str[128];
  INT j,k;
  BOOL rfc_flag;
  /* TEMP for PSM debug */
  
  INT ninc; /* number of increments of any type 1 scan */
  CYCLE_SCALERS_TYPE1_SETTINGS_STR(type1_str);
  CYCLE_SCALERS_TYPE2_SETTINGS_STR(type2_str);

  CYCLE_SCALERS_POL_SETTINGS_STR(pol_str);

  clear_scan_flag(); /*  clear pol's bad scan flag */
  hot_reference_clear(0);
  pol_Rmon_flag=TRUE; /* get a monitor event at begin-of-run */

  gbl_transition_requested=FALSE; /* used for deferred transition */
  gbl_waiting_for_run_stop = FALSE; /* clear flag for unrecoverable error */
  loop_error_cntr=0;
  gbl_run_number = run_number;
  gbl_IN_CYCLE = FALSE;
  gbl_CYCLE_N = 0;
  gbl_SCYCLE_N = 0;
  gbl_SCAN_N = 0;
  skip_cycle = FALSE;
  cyinfo.cancelled_cycle = 0;
  ncycle_sk_tol = 0;
  rfc_flag = 0; 
  gbl_fix_inc_cntr = 0;
  gbl_first_call = TRUE; /* for prestop */
#ifndef EPICS_ACCESS
  hel_warning=FALSE;
#endif

  
  fflush(stdout);
  
  /* make sure  hold flag is cleared at begin_of_run */
  hold_flag = FALSE;
  size = sizeof(hold_flag);
  status = db_set_value(hDB, hFS, "flags/hold", &hold_flag, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    cm_msg(MERROR, "begin_of_run", "cannot clear hold flag at begin_of_run");
  
  /* Get current FIFO_ACQ settings */
  size = sizeof(fs);
  status = db_get_record(hDB, hFS, &fs, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin_of_run", "cannot retrieve FIFO_ACQ/Settings");
      return DB_NO_ACCESS;
    }


#ifdef TWO_SCALERS
  status = db_find_key(hDB, 0, "/Equipment/Cycle_scalers/Settings/",&hKey);
  if(status != DB_SUCCESS && status != DB_NO_KEY)
    { 
      cm_msg(MERROR, "begin_of_run", "error accessing \"/Equipment/Cycle_scalers/Settings/\"  (%d)",status);
      return status;
    }
  if(status == DB_SUCCESS)
    db_delete_key(hDB,hKey,FALSE);
  
  if (  strncmp(fs.input.experiment_name,"1",1) == 0) {
    exp_mode = 1; /* Imusr type - scans */
    status = db_create_record(hDB,0,"/Equipment/Cycle_scalers/Settings",strcomb(type1_str));
  }
  else {
    exp_mode = 2; /* TDmusr types - noscans */
    status = db_create_record(hDB,0,"/Equipment/Cycle_scalers/Settings",strcomb(type2_str));
    /*    status = db_set_record(hDB,hKey, &type2_str,size,0); */
  }
  if(status !=DB_SUCCESS){
    cm_msg(MERROR, "begin_of_run", "cannot create \"/Equipment/Cycle_scalers/Settings\" (%d)",status);
    return DB_NO_ACCESS;
  }
#else
  if (  strncmp(fs.input.experiment_name,"1",1) == 0) 
    exp_mode = 1; /* Imusr type - scans */
  else 
    exp_mode = 2; /* TDmusr types - noscans */
  
  printf("begin_of_run: creating record for cycle_scalers/settings for pol \n");
  status = db_create_record(hDB,0,"/Equipment/Cycle_scalers/Settings",strcomb(pol_str));
  if(status !=DB_SUCCESS){
    cm_msg(MERROR, "begin_of_run", "cannot create \"/Equipment/Cycle_scalers/Settings for POL\" (%d)",status);
    return DB_NO_ACCESS;
  }
  
#endif
  
  /* Get the flag that allows mdarc to stop the run (from client_flag area of odb)  */
  size = sizeof(client_check); 
  sprintf(str,"/Equipment/%s/client flags/enable client check",equipment[FIFO].name); 
  status = db_get_value(hDB, 0, str, &client_check, &size, TID_BOOL, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MINFO,"begin_of_run","cannot get value of \"%s\" (%d)",str,status);
      return (status);
    }
  if(client_check)  /* who_stops_run is used for messages only */
    sprintf(who_stops_run,"fe_runlog");
  else
    sprintf(who_stops_run,"user");
  if(d9)printf("begin_of_run: client check flag = %d, who_stops_run=%s\n",client_check,who_stops_run);
  
#ifdef VXWORKS
  ppgInit(PPG_BASE); /* loads default mask, VME controls Hel, disable ext start,beam off */
#endif
  /* ppg_mode is needed for PSM IQ pair files as well as PSM or FSC freq files */
  strncpy(ppg_mode,fs.input.experiment_name,2);
  ppg_mode[2]='\0'; /* terminate */
  for  (i=0; i< strlen(ppg_mode) ; i++) 
    ppg_mode[i] = tolower (ppg_mode[i]); 
  
  printf("Experiment type = %s; mode = %i, ppg_mode=%s ppg template=\"%s\"\n", 
	 fs.input.experiment_name,
	 exp_mode, ppg_mode, fs.output.ppg_template);
  
  flip = fs.hardware.enable_helicity_flipping;
  
#ifdef EPICS_ACCESS
  /* POL with EPICS */
  
  if(fs.hardware.disable_epics_checks)
    {
      epicstime=0;
      printf("begin_of_run: epics bias read is disabled in the odb \n");
    }
  else
    {
      epicstime=ss_time();
      /*     Reconnect to Epics Bias (channel access) */
      gbl_hel_live = FALSE;  /* borrow this flag */
      printf("begin_of_run: opening direct channel access for HV bias read\n");
      set_long_watchdog(300000);
      Rchid = -1;
      status = Bias_init(fs.output.bias_source_code, &Rchid, Rname); /* Opens direct access to Epics bias read 
									fills Rchid, Rname
								     */ 
      restore_watchdog();
      if (status != SUCCESS) 
	{
	  cm_msg(MERROR,"begin_of_run","failure from Bias_init(%d)",status);
	  /* Epics device cannot be initialized  or the parameters were invalid ....
	     mdarc will stop the run since client flag will  not be set to success at end of BOR */
	  return  (FE_ERR_HW);  
	}
      Hel_last_time = ss_time(); /* for watchdog (to keep Epics channel open) */ 
      gbl_hel_live = TRUE ;
      epicstime=Hel_last_time - epicstime;
      
      printf("begin_of_run: it took %d sec to open epics HV Bias channel\n",epicstime);
    }
#else /* no EPICS */
  /* POL, no EPICS */
  cm_msg(MINFO,"begin_of_run","No EPICS access. Check on HV Bias is disabled");
  
  set_init_hel_state(flip,HEL_DOWN); /* initial helicity state is hel_down */
#endif /* EPICS_ACCESS */
  
  
  if(fs.input.num_cycles_per_supercycle <=0)
    fs.input.num_cycles_per_supercycle=1; /* default = 1 */
  /* get the number of scans or cycles needed */
  N_Scans_wanted = fs.hardware.num_scans;  /* only used for Type 1 */
  
  /* clear EPICS and CAMP and other scanning flags */
#ifdef EPICS_ACCESS
  epics_params.NaCell_flag = epics_params.Laser_flag = epics_params.Field_flag = FALSE;
#endif
  camp_flag = epics_flag= gbl_epics_live = random_flag = e2a_flag = rf_flag = FALSE;
  mode10_flag = mode1g_flag= mode1j_flag = pol_DAC_flag = FALSE;
  gbl_scan_flag=0;
  
  /* Check that rf_config has run here. It makes the frequency file which is
     needed now if random_flag is true */
  
  status = check_file_time(); /* check if bytecode.dat has been created recently */
  if(status != CM_SUCCESS)
    {
      if(fs.input.check_recent_compiled_file) /* check flag */
	return FE_ERR_HW;   /* don't let the run start if file is not recent */
      else            /* send an informational message because check flag is off */
	cm_msg(MINFO,"begin_of_run",
	       "File bytecode.dat has not been created recently ... continuing as check flag is FALSE");
    }
  else
    rfc_flag=TRUE; /* indicates rf_config has run successfully */
  
  /* The number of bins as calculated by rf_config 
     (n_his_bins needed for e2e & e2a pulse pairs compaction modes ; initialize it to n_bins) */
  n_bins = n_his_bins = fs.output.num_dwell_times ;
  
  
  /* Here is included all the settings for Imusr type scans */
  if(exp_mode == 1)
    {
      /* Set mode flags */
      if (  strncmp(fs.input.experiment_name,"10",2) == 0)
	mode10_flag = TRUE;  /* "Dummy mode" no frequency scan */
      else if (  strncmp(fs.input.experiment_name,"1g",2) == 0)
	{
	  mode1g_flag = TRUE;  /* Combination of SLR (20) and 1f */
	  rf_flag =  TRUE;  /* frequency scan needs RF */
	}
      else if (  strncmp(fs.input.experiment_name,"1h",2) == 0)
	pol_DAC_flag = TRUE;  /* POL/CFBS DAC */
      
      else if ( ( strncmp(fs.input.experiment_name,"1c",2) == 0 )
		|| ( strncmp(fs.input.experiment_name,"1j",2) == 0 ) )
	{ 
	  if (  strncmp(fs.input.experiment_name,"1j",2) == 0)
	    {
	      mode1j_flag = TRUE;  /* Combination of SLR (20) and 1c ; RF is always OFF */
	      
	    }
	  
	  printf("begin_of_run: CAMP scan is not supported in this version\n");
	  cm_msg(MERROR,"begin_of_run","CAMP scan is not supported in this version of frontend code");
	  return(FE_ERR_HW);
	  
	}
      else if (  strncmp(fs.input.experiment_name,"1n",2) == 0  ||  
		 strncmp(fs.input.experiment_name,"1d",2) == 0  ||
		 strncmp(fs.input.experiment_name,"1e",2) == 0  )
	{    /* EPICS device */
#ifdef EPICS_ACCESS
	  printf("begin_of_run: EPICS scan detected\n");
	  epics_flag = TRUE;
#else
	  cm_msg(MERROR,"begin_of_run","EPICS scan is not available");
	  return (FE_ERR_HW);
#endif
	}
      
      
      /* Set scan parameters for Type 1 experiments */
      if( mode10_flag)
	{
	  status = mode10_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
      
#ifdef EPICS_ACCESS
      else if(epics_flag)
	{
	  printf("begin_of_run: calling epics_set_scan_params\n");
	  status = epics_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
#endif
      
      
      
      
      else if (pol_DAC_flag)   
	{
	  status = DAC_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
      
      
      else  /* all other frequency scans */
	{
	  status = RF_set_scan_params(&ninc);
	  if(status != SUCCESS) return status;
	}
      printf("begin_of_run: set_scan_params returns ninc=%d; gbl_scan_flag=0x%x\n",ninc,gbl_scan_flag);
      
      /* setup Type 1 specific histograms, scalers, related variables */
      
      fill_uarray = TRUE;
      n_histo_a = 0;
      n_histo_b = N1_HISTO_MAXB;
      n_scaler_total = N1_SCALER_TOTAL;
      n_scaler_cum = N1_SCALER_CUM;
    } /* end of Type 1 */
  else
    { /* Type 2 */
      /* Type 2 specific histograms, scalers, related variables */
      fill_uarray = FALSE;
      n_histo_a = 0;
      n_histo_b = N2_HISTO_MAXB;
      n_scaler_total = N2_SCALER_TOTAL;
      n_scaler_cum = N2_SCALER_CUM;
    } /* end of Type 2 */
  

  n_histo_total = n_histo_b;

  
  
  
  /* Check number of frontend histograms is correctly listed in odb; 
     these only change when more histograms are hardcoded */
  if (exp_mode == 1)
    {   /* type 1 */
      if( fs.input.num_type1_frontend_histograms != n_histo_total)
	{
	  /* write the number of frontend histograms to odb (for rf_config to update mdarc area) */
	  printf("Number of frontend histos for type 1 has changed to : %d; previous odb value was %d\n",
		 n_histo_total,fs.input.num_type1_frontend_histograms);
	  fs.input.num_type1_frontend_histograms = n_histo_total;
	  size=sizeof(fs.input.num_type1_frontend_histograms) ;
	  status=db_set_value(hDB, hFS, "input/num type1 frontend histograms", &fs.input.num_type1_frontend_histograms, size, 1, TID_INT);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR, "begin_of_run", "could not write # type1 frontend histos to odb (%d)",status);
	      return DB_NO_ACCESS;
	    }
	}
    }
  else
    {
      /* type 2 */
      if( fs.input.num_type2_frontend_histograms != n_histo_total)
	{
	  /* write the number of frontend histograms to odb (for rf_config to update mdarc area) */
	  printf("Number of frontend histos for type 2 has changed to : %d; previous odb value was %d\n",
		 n_histo_total,fs.input.num_type2_frontend_histograms);
	  fs.input.num_type2_frontend_histograms = n_histo_total;
	  size=sizeof(fs.input.num_type2_frontend_histograms) ;
	  status=db_set_value(hDB, hFS, "input/num type2 frontend histograms", &fs.input.num_type2_frontend_histograms, size, 1, TID_INT);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR, "begin_of_run", "could not write # type2 frontend histos to odb (%d)",status);
	      return DB_NO_ACCESS;
	    }
	}
    }
  
  /* Initialize scaler structure  */

  if (n_scaler_total < N_SCALER_REAL )
    {
      cm_msg(MERROR,
	     "begin_of_run", "Total no. of scalers (%d) too small for no. real scalers (%d)",
	     n_scaler_total,N_SCALER_REAL); 
      return FE_ERR_HW; 
    }
  printf("N_SCALER_REAL = %d, N_SCALER_TOTAL = %d\n",N_SCALER_REAL, n_scaler_total);
  
  
  
  /*  Stopping after num_scans or num_cycles is implemented June 2003 */
  
  /* the number of scans or cycles needed has been assigned above 
    N_Scans_wanted = fs.hardware.num_scans;   only used for Type 1 */
  
  /* We are using fs.hardware.num_scans as the input parameter (edit on start) for Type 1 and
     fs.hardware.num_cycles  for Type 2.
     When changing mode, fs.hardware.num_cycles will be set to 0 to avoid any confusion (by do_link.pl)
     
     For type 1,  fs.hardware.num_cycles will be calculated, and written to odb
  */
  
  if(exp_mode == 1)
    {
      if(N_Scans_wanted > 0)
	{
	  printf("ninc = %d\n",ninc);
	  fs.hardware.num_cycles = ninc * N_Scans_wanted;
	  /* not necessary as one complete scan is scan up (or down)  flipping helicity each time
	     if( (mode1g_flag && flip)|| (mode1j_flag && flip))
	     fs.hardware.num_cycles *=2;  one cycle at each helicity */
	  
	  if(fs.input.num_cycles_per_supercycle > 1) /* number of cycles repeated at each frequency */
	    fs.hardware.num_cycles *= fs.input.num_cycles_per_supercycle;
	  
	  if(flip)fs.hardware.num_cycles *= 2.0 ; /* helicity flipping; one complete cycle; scan up then down
						     if modes 1g or 1j, one cycle at each helicity, scan up only */
	  
	  if(client_check)  
	    {  /* we assume Mdarc is running (or fe_runlog for POL)  */
	      printf("begin_of_run: run will be stopped after %d complete scans (%d cycles) are complete\n",
		     N_Scans_wanted, fs.hardware.num_cycles);
	      cm_msg(MINFO,"begin_of_run","run will be stopped after %d complete scans (%d cycles) are completed",
		     N_Scans_wanted, fs.hardware.num_cycles);	    
	    }
	  else
	    {
	      cm_msg(MINFO,"begin_of_run",
		     "Mdarc auto stop is disabled. User must stop run when indicated (after %d scans (%d cycles) are completed)",
		     N_Scans_wanted,fs.hardware.num_cycles);	
	    }
	}
      else  /* free-running */
	fs.hardware.num_cycles=0; /* free-running; make sure this is zero */
      
      status = db_set_value(hDB, hFS, "hardware/num cycles", &fs.hardware.num_cycles, 4, 1, TID_DWORD);
      if(status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "begin_of_run", "could not write num cycles to odb (%d)",status);
	  return DB_NO_ACCESS;
	}
      
    }  /* end of Type 1 */
  
  else
    {   /* type 2 */
      if( fs.hardware.num_cycles > 0 )
	{
	  if(client_check)  
	    {  /* we assume Mdarc is running  */
	      printf("begin_of_run: mdarc will stop run after %d cycles are complete\n",fs.hardware.num_cycles);
	      cm_msg(MINFO,"begin_of_run","Mdarc will stop the run after %d cycles are completed",
		     fs.hardware.num_cycles);	    
	    }
	  else
	    {
	      printf("begin_of_run: client check flag is disabled. Mdarc will not stop run automatically\n");
	      printf("         User must stop run when indicated (after %d cycles are completed)\n",
		     fs.hardware.num_cycles);
	      cm_msg(MINFO,"begin_of_run",
		     "Mdarc auto stop is disabled. User must stop run when indicated (after %d cycles are completed)",
		     fs.hardware.num_cycles);	
	    }
	}
    } /* end of type 2 */
  
  setup_hotlinks();/* set up hot links */
  
  
  
  
  
  
  /* Stop the PPG sequencer  */
  ppgStopSequencer(PPG_BASE);
  
  /* already checked that rf_config has run recently */
  sprintf(str,"bytecode.dat");
  printf("opening %s\n",str);
  status = ppgLoad(PPG_BASE, str); 
  if (status < 0)
    {
      cm_msg(MERROR,"begin_of_run","Failure from ppgLoad: file %s not loaded",str);
      return FE_ERR_HW;
    }
  cm_msg(MINFO,"begin_of_run","File bytecode.dat successfully loaded");
  
  
  /* parameters needed for SIS setup 
     fs.hardware.n_channels : # of requested channels (add 1 for ref )
     fs.hardware.num_bins     : # of time bins
     fs.hardware.dwell_time__ms_ : dwell time in msec
  */
  
  /* check number of bins is valid for outputting histos */
  
  if (n_his_bins >= N_BINS_MAX) /* n_his_bins=n_bins except for pulse_pairs compaction modes */
    {
      cm_msg(MINFO,"begin_of_run", "Number of bins too large %d > %d",n_his_bins, N_BINS_MAX);
      return FE_ERR_HW;
    }
  
  if (ddd) { 
    printf("begin_of_run: Number of incoming scaler bins: %d; number of histo bins %d\n",n_bins,n_his_bins);
    /* Initialization of hardware SIS3801 */
    printf("Calling sis setup with N_SCALER_REAL=%d, dwell time = %f\n",N_SCALER_REAL,  fs.sis_test_mode.dwell_time__ms_);
  }
  status = sis_setup(N_SCALER_REAL,  fs.sis_test_mode.dwell_time__ms_);
  /* note: for real mode, sis_setup  ignores dwell_time param */ 
  
  if (status != FE_SUCCESS)
    {
      cm_msg(MERROR,"begin_of_run","SIS parameters out of range");
      return status;
    }

  j=k=0;
  /* check approximate space needed for shipping histos */
  i = n_his_bins * n_histo_total * sizeof(DWORD); 
  if (i >  max_event_size)
    j=max_event_size/(sizeof(DWORD)* N_SCALER_REAL);
  
  /* check approximate space needed for shipping scaler histos */
  i = n_his_bins * N_SCALER_REAL  * sizeof(DWORD); 
  if (i >  max_event_size)      
    k=max_event_size/(sizeof(DWORD)* N_SCALER_REAL);
  
  if(j>0 || k>0)
    {
      if(j<k)j=k;	 
      cm_msg(MERROR,"begin_of_run", "Event size (%d) too large (max=%d); nbins=%d, reduce no. his bins (%d) to < %d",i, max_event_size,n_bins, n_his_bins,j);	
      return FE_ERR_HW;
    }

  
  /* cleanup previous booking
     realloc memory for scalers */
  
  for (i=0 ; i < n_scaler_total ; i++)
    {
      if (ddd) {
	printf("scaler %d ", i);
      }
      
      if (scaler[i].ps != NULL)
	{
	  printf("free scaler %d\n", i);
	  free(scaler[i].ps);
	  scaler[i].ps = NULL;
	}
      
      /* do not malloc any space for software (calculated) scalers */
      if (i < N_SCALER_REAL)
	{
	  scaler[i].nbins = n_his_bins;
	  scaler[i].ps = malloc(sizeof(DWORD) * n_his_bins);
	  if (ddd) 
	    printf("malloc real scaler %d [%d] (%p)\n", i, n_his_bins, scaler[i].ps);
	  scaler_clear(i);
	  if (ddd) 
	    printf("cleared real scaler %d (%d)\n",i , scaler[i].nbins);
	}
      else
	{
	  scaler[i].nbins = 0;     /* set bin count to zero */
	  scaler[i].sum_cycle=0.0; /* software scalers clear sums only */
	  if (ddd) printf("cleared sum of software scaler %d (no malloc) \n", i);      
	}
    }
  
  /* cleanup previous booking
     realloc memory for histograms */
  for (i=0 ; i < n_histo_total ; i++)
    {
      if (histo[i].ph != NULL)
	{
	  printf("free histo %d\n", i);
	  free(histo[i].ph);
	  histo[i].ph = NULL;
	}
      histo[i].nbins = n_his_bins;
      histo[i].ph = malloc(sizeof(DWORD) * n_his_bins);
      if(ddd)
	printf("malloc histo %d [%d] (%p)\n", i, n_his_bins, histo[i].ph);
      histo_clear(i);
      if (ddd) printf("cleared histo %d (%d)\n",i , histo[i].nbins);
    }
  
  if(ddd)
    {
      printf("begin_of_run:\n");
      printf("# cycle:%d\n",fs.hardware.num_cycles);
      printf("# bins :%d\n",n_bins);
      printf("# his bins:%d\n",n_his_bins); 
      printf("thresh1 :%f\n",fs.hardware.cycle_thr1);
      printf("thresh2 :%f\n",fs.hardware.cycle_thr2);
      
      printf("thresh3 :%f\n",fs.hardware.cycle_thr3);
      
      printf("skip #cyc: %d\n",fs.hardware.skip_ncycles_out_of_tol);
      printf("diag   :%d\n",fs.hardware.diagnostic_channel_num);
    }
  /* now set Beam Control to VME (for continuous beam)  or PPG  (pulsed beam)  */
#ifdef VXWORKS
  if(fs.output.vme_beam_control)
    {
      printf("begin_of_run: Continuous beam: setting beam on with VME control\n");
      ppgBeamOn(PPG_BASE); /* set beam on with VME control */
    }
  else
    {
      printf("begin_of_run: Pulsed beam: setting beam control to PPG (PPG script controls beam)\n");
      ppgBeamCtlPPG(PPG_BASE); /* PPG script controls the beam */
    }
#endif
  
  
  if (fs.hardware.disable_epics_checks)
    printf("\n\nbegin_of_run: WARNING No read of EPICS bias will be done (epics check disabled) \n");
  
  printf("begin_of_run: gbl_scan_flag = %d, calling cycle_start\n",gbl_scan_flag);
  
  /* Start cycle */
  first_time = TRUE;
  status = cycle_start();
  if(status != SUCCESS)
    {
      printf("begin_run: failure from cycle_start, calling set_client_flag with FAILURE\n");
      status = set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag). */
      
      cm_msg(MINFO,"begin_of_run","waiting for %s to stop the run after failure from cycle_start",
	     who_stops_run);
      gbl_waiting_for_run_stop =  TRUE;
      ss_sleep(1000); /* let mdarc have a chance to stop this run */
      cm_yield(100);
      return FE_ERR_HW;
    }
  
  first_cycle = TRUE;
  
  /* set client status flag for frontend to SUCCESS */
  status = set_client_flag("frontend",SUCCESS) ; /* set client flag to success */  
  
  if(ddd)printf("end of begin_of_run\n");
  return CM_SUCCESS;
}

#include "setup_scan_params.c"


/*-- Start cycle sequence ------------------------------------------*/
INT cycle_start(void)
{
  /*
    - clear cycle sum scalers
    - reset flags for sequencer
    - clear FIFO, enable interrupt on SIS, start SIS
    - generate hardware pulses
    - Enable system interrupt
  */
  
  INT i,status, size;

  float read_camp_val; /* CAMP value read back */
  BOOL new_supercycle;
  BOOL epics_same_value;
  INT icount;
  INT errcnt=10;
  INT ramp_status;
  DWORD first_frequency;
  INT ihand;
  
  if(ddd) 
    printf("start of cycle_start\n");  
  /* New cycle => start at bin 0 */
  gbl_BIN_A = 0;
  gbl_BIN_B = 0;

#ifdef PSM
  if (dpsm)  
    {
      if( fs.hardware.psm.quadrature_modulation_mode)
	{
	  status = psmReadIQlen (PSM_BASE);
	  if(status != -1) printf("cycle_start: IQlen=%d\n",status);
	  status = psmReadIQptr (PSM_BASE);
	  if(status != -1) printf("cycle_start: IQpointer=%d\n",status);
	}
    }
#endif


  if(!skip_cycle)
    {  /* !skip_cycle */
      if( gbl_CYCLE_N % fs.input.num_cycles_per_supercycle == 0 )
	{
	  if(d8) 
	    printf("\ncycle_start: start of supercycle\n"); 
	  new_supercycle=TRUE;
	  gbl_SCYCLE_N ++;
	}
      else
	new_supercycle=FALSE; /* do not want to clear histos or step Epics/Camp/freq etc */
      
      
      /* Note - first time through, gbl_cycle_N=0 */  
      gbl_CYCLE_N++;  /* Count cycle from 1 on */
      if(d8)printf("cycle_start: cycle: %d Supercycle: %d exp_mode=%d, new_supercycle=%d\n",
		   gbl_CYCLE_N,gbl_SCYCLE_N,exp_mode, new_supercycle);

      if(exp_mode == 1  && new_supercycle)
	{
	  if(d8)printf("\ncycle_start: clearing histos at start of new SuperCycle\n");
	  /* clear histograms if starting a new SuperCycle */
	  for (i=0 ; i < n_histo_a - 1 ; i++)
	    histo_clear(i);
	  /* do not clear n_histo_a since this is the USER BIT histo */
	  for (i=0 ; i < n_histo_b ; i++)
	    histo_clear(n_histo_a + i);    
	  
	  
	  if (epics_flag) 
	    {
#ifdef EPICS_ACCESS
	      epics_same_value = FALSE;
	      /* ---------------------------------------------------------
		 Epics scan (NaCell/Laser/Field) 
		 ---------------------------------------------------------*/
	      if(gbl_inc_cntr ==  epics_params.Epics_ninc)  
		{
		  if (!d11)printf("cycle_start: new scan, flip=%d     \n",flip);
		  /*  N E W  S C A N */

		  clear_scan_flag(); /*  clear pol's bad scan flag */
		  
		  
		  /* check if helicity flipping is enabled (dummy helicity flip for POL)*/
		  if(flip) 
		    {
		      /* really flip the helicity ONLY when terminating a positive scan 
		         POL has a dummy flip_helicity() */
		      if(epics_params.Epics_inc > 0)
			{
			  gbl_hel_flipped = FALSE;
			  gbl_hel_flipped = flip_helicity(); /* prints a message */
			}
		      else
			if(!d8)printf("\n");
		      
		      if(gbl_CYCLE_N !=1)
			{
			  update_fix_counter(epics_params.Epics_ninc); /* update fix_incr_cntr */
			  epics_same_value = TRUE; /* set a flag to say Epics set value is kept the same
						      (except at begin of run) */
			}
		      else
			{
			  if(d5)
			    printf("cycle_start: detected BOR, gbl_CYCLE_N=%d  flip=%d ,epic_same_value=%d gbl_fix_inc_cntr=%d\n",
				   gbl_CYCLE_N,flip,epics_same_value,gbl_fix_inc_cntr);
			}
		      
		      epics_params.Epics_inc *= -1.; /* change scan direction */
		    } /* end of flip=TRUE */
		  else 
		    { /* helicity flipping is not enabled */
		      if(d5)printf("cycle_start: setting 1 incr. away from start value (large change) \n");
		      epics_params.Epics_val = epics_params.Epics_start - epics_params.Epics_inc; 
		      /* this may be a large change, so set 1 incr. away from start & allow to 
			 stabilize before stepping... except at Begin run, because this value has
		         already been set */
		      if(gbl_CYCLE_N > 1)
			{
			  if(d5)printf("cycle_start: calling set_epics_val with  epics_parameters.Epics_last_offset=%.2f\n",epics_params.Epics_last_offset);
			  if (set_epics_val() != SUCCESS)
			    return (status);
			}
		      else
			{
			  if(d8)
			    printf("cycle_start:detected BOR .... NOT calling set_epics_val, gbl_CYCLE_N=%d flip is %d, epic_same_value FALSE \n", gbl_CYCLE_N,flip);
			}
		    }
		  gbl_inc_cntr = 0;
		  gbl_SCAN_N ++;
		} /* E N D  of new scan for Epics */

	      else
		{ /* Phil's expt... check for end-of-scan */
		  if (gbl_inc_cntr == epics_params.Epics_ninc -1)
		    {
		      pol_Rmon_flag = TRUE; /* end of sweep: set a flag for monitor equipment to run */
		      printf("cycle_start: end of sweep detected, setting pol_Rmon_flag\n");
		    }
		
		}

	      /* calculate new EPICS set value; calculate from start value to avoid rounding errors */
	      epics_params.Epics_val = epics_params.Epics_start + 
		( (gbl_inc_cntr - gbl_fix_inc_cntr) * epics_params.Epics_inc);

	      /* check for jump scan  */
	      if( fs.input.enable_scan_jump)
		{
		  /* check if we need to jump next cycle */
		  i=check_jump(gbl_inc_cntr,dj);/* sets gbl_jump_now true if time to jump */
		  if (gbl_jump_now)
		    {  /* note...Epics will use gbl_jump_now flag to reset device to gbl_prejump_val 
			  after stability check to give the device time to settle  */
		      if(dj)
			printf("cycle_start: gbl_jump_now=TRUE, resetting gbl_inc_cntr=%d to %d (for jump) after this setting\n"
			       ,gbl_inc_cntr,i);
		      gbl_inc_cntr=i; /* reset counter for next time; it will be incremented */
		      /* calculate EPICS value one increment different from jump value */
		      gbl_prejump_value = epics_params.Epics_start + 
			( (gbl_inc_cntr - gbl_fix_inc_cntr) * epics_params.Epics_inc);
		      if(dj)
			printf("cycle_start: calculated gbl_prejump_value = %.2f%s \n", 
			       gbl_prejump_value, epics_params.Epics_units);
		    }
		  else
		    gbl_prejump_value = -999; /* this value set to a bad value for debugging */
		} /* end of jump */

	      gbl_inc_cntr ++; 

	      if(!epics_same_value)
		{
		  if( set_epics_incr() != SUCCESS)   
		    {
		      return status; /* on error, set_epics_incr sets client flag to stop the run */
		    }
		}
	      else
		if(d5)printf("cycle_start: keeping epics value the same, skipping call to set_epics_incr\n");
/* POL */
	      /* EPICS with POL defined... this is Phil's experiment to scan NaCell */

	      
	      /* Now DVM[2] is not longer Faraday Cup (FC2) Phil's expt no longer needs
		 to read the DVM */

	      /* Previously: 
		 Set a hotlink to indicate we want to read the DVM
		 using a dummy DAC because we are not scanning the real DAC in this expt. 
             
	      * Wait for "psleep" time (pol_params/settling time ) 
	      * 
	      * At end of cycle( in routine scaler_cycle read) 
	      *     check for ODB handshake word =2 or more
	      *     read  DVM[2] from ODB, etc
	      */ 
	      
	      /* to trigger DVM read, we will do a dummy write to DAC channel 2  */
	      /*	      ihand = 0;
	      db_set_data(hDB,hHand2,&ihand,sizeof(ihand),1,TID_INT);
	      db_set_data(hDB,hReq2,&dac_val,sizeof(dac_val),1,TID_FLOAT); /* dummy for hotlink - nothing actually is set */
	      /* ss_sleep (psleep);
	      if(d12)printf("\n cycle_start %d SCyc %d;  hel %d ;   now setting DVM handshake\n",
	      gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel);
	     
 /* POL */

#endif /* and EPICS */
	    } /* end of epics flag */

	  else if(pol_DAC_flag) {
	    if( gbl_FREQ_n ==  dac_ninc) 
	      { /* N E W  S C A N  FOR DAC */

		if(d12)printf("New Scan for DAC: pol_DAC_flag=%d\n",pol_DAC_flag);

		clear_scan_flag(); /* clear POL's bad scan flag */
		gbl_FREQ_n = 0;
		gbl_SCAN_N ++;
		/* check if dummy helicity flipping is enabled */
		if(flip)
		  {
		    /* really flip the dummy helicity ONLY when terminating a positive scan
		       dummy flip_helicity() for POL */
		    if(dac_inc > 0)
		      {
			gbl_hel_flipped = FALSE;
			gbl_hel_flipped =  flip_helicity();
		      }
		    else
		      if (!d11 && !d8)printf("\n"); /* d11=print nothing  d8=verbose mode*/
		    
		    if(gbl_CYCLE_N !=1) /* except at Begin of Run,
					   for up/down scan, increment counter has to be fixed */
		      update_fix_counter(dac_ninc); /* update fix_incr_cntr */
		    else
		      {  /* run is just starting */
			/* if(d8) */
			printf("cycle_start: detected BOR, gbl_CYCLE_N=%d  flip=%d gbl_fix_inc_cntr=%d\n",
			       gbl_CYCLE_N,flip,gbl_fix_inc_cntr);
		      }
		    
		    dac_inc *= -1.; /* reverse scan direction */
		    
		  } /* end of if flip */
	      } /* end of new scan */
	    else
	      {  /* check for end-of-sweep */
		if (gbl_FREQ_n ==  dac_ninc-1)
		  {
		    pol_Rmon_flag = TRUE; /* end of sweep: set a flag for monitor equipment to run */
		    printf("cycle_start: end of sweep detected, setting pol_Rmon_flag\n");
		  }
	      }
	    dac_val = dac_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * dac_inc);

	    if( fs.input.enable_scan_jump)
	      {
		/* check if we need to jump next cycle */
		i=check_jump(gbl_FREQ_n,dj);
		if (gbl_jump_now)
		  {
		    if(dj)
		      printf("cycle_start: resetting gbl_FREQ_n=%d to %d (for jump) after this setting\n",gbl_FREQ_n,i);
		    gbl_FREQ_n=i; /* reset counter for next time; it will be incremented */
		  }
	      } /* end of jump */

	    gbl_FREQ_n ++;
	    

	    /*** Set the DAC to the next value ***/
            /* Set ODB handshake word = 0
             * Set ODB DAC value 
             * Wait for ODB handshake word = 1 (not needed it seems )
             * Potentially wait for "psleep" time
             * 
             * At end of cycle( in routine scalers_cycle read) 
             *     check for ODB handshake word =2 or more
             *     read the DVM from ODB, etc
	     */

	    if(d12) 
	      printf("\n **** cycle_start: pol_DAC_flag=%d  pol_Rmon_flag=%d\n",
			  pol_DAC_flag , pol_Rmon_flag);
	    ihand = 0;
	    /* trigger read of DVM[0] (DAC-readback by hotlink */
	     if(d12) 
	      printf("cycle_start: writing ihand=0 to trigger read of DVM[0]\n");
	    status = db_set_data(hDB,hHand,&ihand,sizeof(ihand),1,TID_INT);
	    if(status != DB_SUCCESS)
	      {
		cm_msg(MERROR,"cycle_start","cannot trigger read of DVM[0] (%d)",status);
		return status;
	      }
	    if(d12) 
	      printf("cycle_start: writing dac_val=%f to trigger update of DVM[0]\n\n",dac_val);
	    status = db_set_data(hDB,hReq,&dac_val,sizeof(dac_val),1,TID_FLOAT);
	    if(status != DB_SUCCESS)
	      {
		cm_msg(MERROR,"cycle_start","cannot trigger update of DVM[0] (%d)",status);
		return status;
	      }
         
	    /* now scaler channel 5 is used instead of this */
#ifdef GONE
	    /*  and trigger read of DVM[2] (Locking feedback) by hotlink  */
	    db_set_data(hDB,hHand2,&ihand,sizeof(ihand),1,TID_INT);
	    db_set_data(hDB,hReq2,&dac_val,sizeof(dac_val),1,TID_FLOAT); /* dummy for hotlink - nothing actually set */
 
	    ss_sleep (psleep);
	    cm_yield(100);
#endif
	     if(!d11) 
	      printf("\r cycle_start %d SCyc %d; set DAC=%.2f",
			   gbl_CYCLE_N,gbl_SCYCLE_N,dac_val);
	    
	    
	    if(gbl_jump_now)  /* write a message */
	      {
		if(!d11)
		  printf("\rcycle_start: scan value will be jumping to %.2f V\n",
			 dac_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * dac_inc));
	      }
	  } /* end of POL_DAC_flag... */
		 
	  else /* all other freq scans */
	    {	      
	      /* -----------------------------------------------------------------------------------------------
		 FREQUENCY SCAN (FSC/PSM) 1f & any other Type 1 freq modes 
		           (i.e. 10, 1a/1b (not randomized) & 1g without flip  
		 ---------------------------------------------------------------------------------------------*/
	      if( gbl_FREQ_n ==  freq_ninc)
		{ /* N E W  S C A N  FOR FREQ */

		  clear_scan_flag(); /* clear POL's bad scan flag */

		  gbl_FREQ_n = 0;
		  gbl_SCAN_N ++;
		  /* check if helicity flipping is enabled */
		  if(flip)
		    {
		      /*  really flip the helicity ONLY when terminating a positive scan
			  (POL has a dummy helicity flip */
		      if(freq_inc > 0)
			{
			  gbl_hel_flipped =  FALSE;
			  gbl_hel_flipped =  flip_helicity();
			}
		      else
			{
			  if (!d11 && !d8)printf("\n"); /* d11=print nothing  d8=verbose mode*/
			}		      
		      if(gbl_CYCLE_N !=1)     /* except at beginning of run */
			{
			  /* printf("cycle_start: calling update_fix_counter with freq_ninc=%d\n",freq_ninc);*/
			  update_fix_counter(freq_ninc); /* update fix_incr_cntr */
			}
		      else
			{
			  if(d10) 
			    printf("cycle_start: detected BOR, gbl_CYCLE_N=%d  flip=%d , gbl_fix_inc_cntr=%d\n",
				   gbl_CYCLE_N,flip,gbl_fix_inc_cntr);
			}
		      
		      freq_inc *= -1.; /* reverse scan direction */
		    } 
		  else   /* flip is false */
		    freq_val = freq_start; /* set to start value */	 
		} /* end of new scan */

	      else /* not a new scan */
		{

		  if( gbl_FREQ_n ==  freq_ninc -1)
		    {
		      pol_Rmon_flag = TRUE; /* end of sweep: set a flag for monitor equipment to run */
		      printf("cycle_start: end of sweep detected, setting pol_Rmon_flag\n");
		    }


		  freq_val = freq_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * freq_inc); /* calculate new value */ 

		  if(d10) 		
		    printf("cycle_start: freq_val = %u, gbl_FREQ_n=%d, gbl_fix_inc_cntr=%d, freq_inc=%d,(gbl_FREQ_n - gbl_fix_inc_cntr)=%d\n",
			   freq_val, gbl_FREQ_n, gbl_fix_inc_cntr, freq_inc, 
			   (gbl_FREQ_n - gbl_fix_inc_cntr));			
		}
#ifdef TEST_JUMP
	      /* TEMP for testing JUMP 
		 note: odb params  scan jump start  (float)
		 scan jump stop   (float)
		 enable scan jump (bool)
		 must be defined for scan jump
		 in directory 	 /equipment/fifo_acq/sis mcs/input/
		 
	      */
	      if( fs.input.enable_scan_jump)
		{
		  /* check if we need to jump next cycle */
		  i=check_jump(gbl_FREQ_n,dj);
		  if (gbl_jump_now)
		    { 
		      if(dj)
			printf("cycle_start: resetting gbl_FREQ_n=%d to %d (for jump) after this setting\n",gbl_FREQ_n,i);
		      gbl_FREQ_n=i; /* reset counter for next time; it will be incremented */
		    }
		} /* end of jump */

#endif /* TEST_JUMP */


	      gbl_FREQ_n ++;  /* increment counter for next time

			      /* Set Frequency */ 
	      if (mode10_flag) /*  Dummy scan (SCALER mode ) */
		{
		  if(!d11) /* d11 print nothing */
		    {
		      printf("\r cycle_start %d SCyc %d: Mode10: nothing scanned;  hel %d ",
			     gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel);
		    }
		}
	      else
		{ /* type1 mode but NOT mode10 */
		  printf("\r cycle_start %d SCyc %d; hel %d ;   setting frequency to %u hz ",
			 gbl_CYCLE_N,gbl_SCYCLE_N,gbl_ppg_hel,freq_val);

#ifdef TEST_JUMP
		  if(gbl_jump_now) /* write a message */
		    printf("\ncycle_start: scan value will be jumping to %u Hz",
			   freq_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * freq_inc));
#endif
		}   /* end of NOT mode 10 */
	      
	    }  /* end of frequency scan */
	  
	} /* End for exp_mode == 1 && new supercycle */


      else if(exp_mode == 2)
	{
	  if(flip) 
	    {
	      gbl_hel_flipped=FALSE;
	      gbl_hel_flipped = flip_helicity();
	    }
	  
 
	

	} /* end of mode 2 */
      
    }   /* end of if (!skip_cycle ) */

  else
    {  /* skipping cycle */
      if(!d8 && !d11)printf("\r cycle_start: skipping cycle ");
      else
	if(d3)printf("cycle_start: last cycle was skipped \n");
    } /* end of else skipping cycle */
  
  
  /* clear REAL sum cycle scalers */
  for (i=0 ; i < N_SCALER_REAL; i++)
    scaler_clear(i);
  if (ddd)printf("cycle_start: called scaler_clear to clear REAL scalers 0 to %d\n",(i-1));
  
  /* clear software scalers  BUT not CUMULATIVE ones */
  for (i=N_SCALER_REAL ; i < n_scaler_total-n_scaler_cum; i++)
    {
      scaler[i].sum_cycle=0.0; /* software cycle scalers clear sums only */
      if(ddd)printf("cycle_start: cleared software CYCLE scaler %d (sum only)\n",i);
    }

  
  /* setup the bin  counter */
  gbl_bin_count = n_bins;  /* count the incoming bins (same as outgoing except pulse pair mode)*/
  
  /* Cycle process sequencer */
  waiteqp[HISTO] = waiteqp[CYCLE] = waiteqp[DIAG] = FALSE;
  waiteqp[FIFO] = TRUE;                /* FIFO acq */
  if ((&equipment[HISTO].info)->enabled)  /* Histo */
    waiteqp[HISTO] = TRUE;             
  if ((&equipment[CYCLE].info)->enabled)  /* cycle */
    waiteqp[CYCLE] = TRUE;
  if ((&equipment[DIAG].info)->enabled)  /* diag */
    waiteqp[DIAG] = TRUE;



  /* hotlink rereferences */
  if (lhot_rereference)
    {    /* re-ref ALL hotlinks (p+, laser, Fcup) */
      hot_rereference = TRUE;
      if(d3)printf("\ncycle_start: detected hot rereference...\n");
    }

  else if(lhot_reref1)
    {
      hot_reref1=TRUE;
      if(d3)printf("\ncycle_start: detected hot reref on P+...\n");
    }
  else if(lhot_reref2)
    {
      hot_reref2=TRUE;
      if(d3)printf("\ncycle_start: detected hot rereference on Laser...\n");
    }
  else if(lhot_reref3)
    {
      hot_reref3=TRUE;
      if(d3)printf("\ncycle_start: detected hot rereference on Faraday Cup...\n");
    }

  /* reset all these */
  lhot_rereference = FALSE;
  lhot_reref1=lhot_reref2=lhot_reref3=FALSE; /* clear all flags */


  
  /* clear FIFO */
#ifdef TWO_SCALERS
  sis3801_FIFO_clear(SIS3801_BASE_A);
#endif
  sis3801_FIFO_clear(SIS3801_BASE_B);
  skip_cycle = FALSE;
  first_time = FALSE;
  
  /* Now we are ready to restart the hardware */
  /* Cycle started */
  gbl_IN_CYCLE = TRUE;
  
#ifdef VMEIO
  /* Ensure cycle off */
  vmeio_latch_write(VMEIO_BASE, 0);
#endif

#ifdef INTERRUPT    
  /* enable interrupt CIP source (0) */
  sis3801_int_source_enable(SIS3801_BASE_B, SOURCE_CIP);
#endif

  /* Enable Next clock (but disable by hardware) */
  sis3801_next_logic(SIS3801_BASE_B, ENABLE_NEXT_CLK);
  
  /* trigger acquisition */
  /* directly by using a VME IO */
  /*
    vmeio_pulse_write(VMEIO_BASE,  BOC_PULSE);
    vmeio_latch_write(VMEIO_BASE,  CYCLE_ON);
  */
 /* POL - start the sequencer */
#ifdef VXWORKS
  ppgStartSequencer(PPG_BASE); /* PPG software start for POL */

  if(ddd)printf("\n NEW CYCLE...Starting PPG\n");
#endif

#ifdef INTERRUPT
  /* enable system interrupt */
#ifdef VXWORKS
  sysIntEnable(IRQ_LEVEL);
#endif /* VXWORKS */
#endif /* INTERRUPT */
  if(d8)  
    printf("\ncycle_start %d  SCyc %d   go\n ",gbl_CYCLE_N,gbl_SCYCLE_N);
  else
    {
      if(!d11)  /* d11->print nothing */
	printf("\r cycle_start %d SCyc %d ",gbl_CYCLE_N,gbl_SCYCLE_N);
    }
  if(exp_mode == 2)
    if(!d8 && !d11)printf(", last ub :%d ;",userbit_A);

  if(!d11 && !d8)  printf(" go"); /* d11=print nothing, d8=verbose mode */
  return CM_SUCCESS;
}  /* end of cycle_start routine */


/*--------------------------------------------------------------*/
BOOL flip_helicity(void)
/*--------------------------------------------------------------*/
{
  /* POL experiment doesn't want to actually flip the helicity,
     but it does want to scan in both directions

  so this routine is a DUMMY  */


  if(d8)
    printf("Reversing scan direction (PPG hel bit NOT actually flipped for POL\n");
  else
    if(!d11)printf("\nReversing scan direction\n");
  
  return TRUE; /* always returns true  */
}



/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  char * ptemp;
  INT i,status,size;

  printf("\nend_of_run: starting... and ending\n");  

#ifdef VXWORKS 
  ppgStop(); /* stops sequencer, disables external trig, sets beam off, set HEL_DOWN */
  if(ddd)printf("For exp_mode %i, ppgStop\n",exp_mode);
  sysIntDisable(IRQ_LEVEL);
#ifdef PSM
  printf("end_of_run: disabling PSM");
  disable_psm(PSM_BASE);
  psmWriteGateControl(PSM_BASE,"all",0); /* disable external gates; ppg sends spurious gates when loaded */
#endif
#endif /* VxWorks */


  if( pol_DAC_flag )
    {
      printf("\nend_of_run: resetting the POL DAC to zero\n");
      dac_val = 0;
      status = db_set_data(hDB,hReq,&dac_val,sizeof(dac_val),1,TID_FLOAT);
      /* read RmonVal at end of run */
      size = sizeof(RmonVal); /* a float */
      status = db_get_data_index(hDB,hRmon,&RmonVal,&size,0,TID_FLOAT);
      if(status != SUCCESS)
	{
	  printf("end_of_run: error getting RmonVal using get_data_index (%d) \n",status);
	  RmonVal=-99.0; /* put some unlikely value to indicate error */
	}
      else
	printf("end_of_run: read wavemeter as %f\n",RmonVal);
    }


  /* Make sure threshold alarms don't keep going off when not running */
  cyinfo.last_failed_thr_test=0;
  size = sizeof(cyinfo.last_failed_thr_test);
  status = db_set_value(hDB, hInfo, "last failed thr test", 
			&cyinfo.last_failed_thr_test, size, 1, TID_DWORD);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "end_of_run", "cannot clear \"...info_odb/var/last failed thr test\" (%d)",
	     status);
    }
  
  clear_alarm(11); /* all thr alarms */
  fflush(stdout);
  return CM_SUCCESS;
}

/*-- Post End of Run ----------------------------------------------------*/

INT post_end_run(INT run_number, char *error)
{
  char * ptemp;
  INT i,status,size;
  /* Since end_of_run runs BEFORE equipments EOR transition add this post-stop routine
     running after the logger, to close Epics channels, clear histo pointers etc. */
 
  printf("\npost_end_run: starting\n"); 

#ifdef EPICS_ACCESS
  printf("post_end_run: setting epics flag(s) false and disconnecting\n");
  gbl_epics_live = gbl_hel_live = FALSE;
  if(epics_flag)
    EpicsDisconnect(&epics_params); /* epics scan */
  ChannelDisconnect(&Rchid); /* hel or bias */
  caExit(); /* close any open channels */
  Rchid=-1;
#endif




  printf("\npost_end_run: free scaler, n_scaler_total=%d \n",n_scaler_total);
  for (i=0 ; i < n_scaler_total ; i++)
  {
    if (scaler[i].ps != NULL)
      {
	printf(" %d", i);  
	if(ddd)printf("\nfreeing scaler %d [%d] (%p)\n", i, n_his_bins, scaler[i].ps); 
	fflush(stdout);
	free(scaler[i].ps);
	scaler[i].ps = NULL;
      }
    else
      {
	if(ddd)printf("scaler[i].ps is NULL, i=%d\n",i);
	fflush(stdout);
      }
  }
  printf("\n");
  fflush(stdout);

  printf("free histo, n_histo_total=%d\n",n_histo_total);
  for (i=0 ; i < n_histo_total ; i++)
  {
    if (histo[i].ph != NULL)
    {
      printf(" %d", i); 
       if(ddd)printf("\nfreeing histo %d [%d] (%p)\n", i, n_his_bins, histo[i].ph);
      fflush(stdout);
      free(histo[i].ph);
      histo[i].ph = NULL;
    }
    else
      if(ddd)printf("histo[i].ph is NULL, i=%d\n",i);
  }
  
  printf("\nend of run %d\n",run_number);
  fflush(stdout);
  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  cm_msg(MTALK,"pause_run","MIDAS PAUSE detected. Stop the run and restart.");
  cm_msg(MERROR,"pause_run","This command DOES NOT WORK for %s experiment. Use \"%s hold\" button instead",
	 beamline,beamline);
  cm_msg(MERROR,"pause_run"," MIDAS PAUSE detected. RUN MUST BE STOPPED then restarted.");
  printf("pause_run: calling set_client_flag with FAILURE\n");
  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/

  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  cm_msg(MTALK,"resume_run","MIDAS RESUME detected. Stop the run and restart.");
  cm_msg(MERROR,"resume_run","This command DOES NOT WORK for BNMR experiment Use BNMR resume button instead");
  cm_msg(MERROR,"resume_run","MIDAS RESUME detected. RUN MUST BE STOPPED and restarted.");
  printf("resume_run: calling set_client_flag with FAILURE\n");
  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/

  return CM_SUCCESS;
}

/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int   i;
  for (i=0 ; i<count ; i++)
  {
    if (!test)
      return FALSE;
  }
  return FALSE;
}

/*-- Faked Event readout ---------------------------------------------*/
INT FIFO_acq(char * pevent, INT off)
/*
  - Check if still in cycle
  - acquire the FIFO data
  - increment the histo
    Takes about 10ms for 1/2FIFO and scaler incrementation
  - update the /variable scalers if it's time
  - return the in cycle status

  - runs every 50ms & tests if FIFO is 1/2 full
 */
{
  INT status, nwords_A, nwords_B;
  INT readbck_val;
  DWORD data;
  INT ppgTrigReg;

  if(ddd)
    printf("FIFO_acq starting...\n");

  if (gbl_IN_CYCLE)
  {          /* cycle started */

    if (dd)logMsg("IN CYCLE ,gbl_bin_count(%d), gbl_BIN_A(%d), gbl_BIN_B(%d)\n",
                  gbl_bin_count,gbl_BIN_A,gbl_BIN_B,0,0);

    

    /*
              Test MODULE B FIFO  Half full
    */
    if (sis3801_CSR_read(SIS3801_BASE_B, IS_FIFO_HALF_FULL) != 0) /* test on FIFO B */
    {
      /* FIFO B is Half full at least so read out data */
      nwords_B = sis3801_HFIFO_read(SIS3801_BASE_B, pfifo_B);
      
      /* check for error conditions:  FULL (nwords=-1) or EMPTY (nwords=0)  */
      if (nwords_B < 1)
      {
        logMsg ("FIFO_acq- Module B FULL(-1) or Empty(0);  nwords:%d\n",nwords_B,0,0,0,0,0);
        die();
	return 0;
      }
      /* Move data & increment corresponding scaler histogram */
      scaler_increment(nwords_B, pfifo_B, MAX_CHAN_SIS3801A, MAX_CHAN_SIS3801B, &gbl_BIN_B, &userbit_B);       
      if (dd) logMsg ("IN CYCLE eqp[FIFO]:%d  nW:%d gbl_BIN_B :%d\n",waiteqp[FIFO],nwords_B,gbl_BIN_B,0,0,0);
      if(ddd)
      {
        status = sis3801_CSR_read(SIS3801_BASE_B,CSR_FULL);
        logMsg("After read %d words, Module B  gbl_BIN_B=%d, CSR 0x%x\n",nwords_B,gbl_BIN_B,status,0,0,0);
      }
      
    } /* Module B not half full yet */

  } /* end of  if gbl_IN_CYCLE */ 

  else
  { /* Cycle is OFF expected to be in end of cycle state */
    if (dd) logMsg ("OFF cycle: gbl_bin_count=%d waiteqpF:%d H:%d C:%d D:%d\n"
                     ,gbl_bin_count,waiteqp[FIFO],waiteqp[HISTO],waiteqp[CYCLE],waiteqp[DIAG],0);
    
    if (!waiteqp[FIFO]) return 0; /* not in end-of-cycle state */

    nwords_A = nwords_B = 0; 


    /* Flush Module B FIFO   (Test FIFO empty) */
    if (sis3801_CSR_read(SIS3801_BASE_B, IS_FIFO_EMPTY) == 0)
    {
      /* Not empty */
      nwords_B = sis3801_FIFO_flush(SIS3801_BASE_B, pfifo_B); /* get the last data */
      if(dd)logMsg("FIFO_ACQ_flush - read %d words from Module B \n",nwords_B,0,0,0,0,0);
      
      /* check for error conditions:  FULL (nwords=-1) or EMPTY (nwords=0)  */
      if (nwords_B < 1)
      {
        logMsg ("FIFO_acq-Module B FULL(-1) or Empty(0);  nwords:%d\n",nwords_B,0,0,0,0,0);
        die();
	return 0;
      }
      /* Move data & increment corresponding scaler histogram */
      scaler_increment(nwords_B, pfifo_B, MAX_CHAN_SIS3801A, MAX_CHAN_SIS3801B, &gbl_BIN_B, &userbit_B);     
    } /* Module B has been flushed */

    if (nwords_B <= 0 )
    {       /* Error if module does not have data */
      logMsg ("FIFO_acq-Flush: Module B is EMPTY \n",0,0,0,0,0,0);
      die();
      return 0;
    }

    

    if (ddd)
    {
       status = sis3801_CSR_read(SIS3801_BASE_B,CSR_FULL);
      logMsg("             gbl_BIN_B %d,  CSR B 0x%x\n",
             gbl_BIN_B,status,0,0,0,0);
    }


 
    /* As the cycle is completed compact histo S0 to 15 H0..1, S16..S31 to H2..3 */
    status = histo_process();

    /* inform that new cycle has been added for histo_read equipment */
    waiteqp[FIFO] = FALSE;
  }
    
  return 0;
}


/*-- Scaler Channel Event  ---------------------------------------------------------*/
INT diag_read(char *pevent, INT off)
/*
  - Compose scaler event for one channel only
*/
{
  INT    ch;
  DWORD *pdata;
  char   bank_name[4];
  
  ch = fs.hardware.diagnostic_channel_num;
  if (!waiteqp[FIFO] && waiteqp[DIAG])
  {
    if (ddd) printf("diag_read %d diag ch %d ", waiteqp[FIFO], ch);     
    if ( (ch >= 0) && (ch < N_SCALER_REAL)) 
    {
      /* init bank structure */
      bk_init32(pevent);
      sprintf(bank_name, "CH%2.2i", ch);
      bk_create(pevent, bank_name, TID_DWORD, &pdata);
      memcpy(pdata, scaler[ch].ps, scaler[ch].nbins * sizeof(DWORD));
      pdata +=  scaler[ch].nbins;
      bk_close(pevent, pdata);
      /* inform that the histo bank has been sent out */
      if (ddd) printf("Sbksize:%d\n",bk_size(pevent));
      waiteqp[DIAG] = FALSE;
      return bk_size(pevent);
    }
    else
    {
      if (ddd) printf("diag_read: Invalid channel\n");
      waiteqp[DIAG] = FALSE;
    }
  }
  return 0;
}

/*-- Histo Event  ---------------------------------------------------------*/
INT histo_read(char *pevent, INT off)
/*
                           00   01   02  
  - Generate histo event 
                          HM01 HM02 HM03  Scaler B - 

			  when cycle is completed
*/
{
  INT    h, status, nhb;
  DWORD *pdata;
  char   bank_name[4];
  
  
  if (!waiteqp[FIFO] && waiteqp[HISTO])
    {
      if(d8)printf("histo_read is starting\n");
      /* printf("histo_read: starting with pevent=%p and skip_cycle=%d, gbl_HEL=%d, flip=%d\n",pevent,skip_cycle,gbl_HEL,flip); */
      
      if(skip_cycle)
	{
	  waiteqp[HISTO] = FALSE; /* do not send out this event */
	  return 0;
	}
      if(exp_mode == 1 &&  gbl_CYCLE_N % fs.input.num_cycles_per_supercycle != 0 )
	{
	  waiteqp[HISTO] = FALSE; /* mid-supercycle: do not send out this event */
	  return 0; 
	}
      
	
      if(d8) printf("histo_read: end of supercycle, sending histos, gbl_CYCLE_N=%d\n",gbl_CYCLE_N);

 
      if(ddd)printf("histo_read: assembling banks \n");
      
      /* For TDtype modes */
      if(exp_mode == 2) 
	{


	  /* init bank structure (Type 2) */
	  /*    
	  if(ddd)printf("creating bank CYCL\n");
	  bk_init32(pevent);
	  bk_create(pevent, "CYCL", TID_DWORD, &pdata);
	  *pdata++ = gbl_CYCLE_N;     
	  *pdata++ = gbl_ppg_hel; /*  use ppg set value */
	  /*	  bk_close(pevent, pdata);
	  /* printf("after CYCL closed, pevent=%p\n",pevent);*/



	   if(ddd) 
	  printf("n_histo_b=%d n_histo_a=%d\n", n_histo_b,n_histo_a);
	  for (h=0; h < n_histo_b ; h++)
	    {
	      sprintf(bank_name, "HM%2.2d", h);
	       if(ddd) printf("Creating bank %s\n",bank_name);
	      bk_create(pevent, bank_name, TID_DWORD, &pdata);
	      memcpy(pdata, histo[h].ph, histo[h].nbins * sizeof(DWORD));
	      /* printf("adding size %d to pdata \n", histo[n_histo_a+h].nbins);*/
	      pdata += histo[h].nbins;  
	      bk_close(pevent, pdata);
	      /*  printf("after bk_close, pevent=%p\n",pevent); */
	    }
	  
	  
	} /* end of TD type modes */
      else 
	{/* For I type modes */
	  /* init bank structure */
	  /* printf("initializing bank structure for type 1\n"); */
	  bk_init32(pevent);
	          
	  nhb= n_histo_b ; /* no userbit histo for POL */ 
	  for (h=0; h < nhb ; h++)
	    {
	      sprintf(bank_name, "HI%2.2d", h);
	      bk_create(pevent, bank_name, TID_DWORD, &pdata);
	      memcpy(pdata, histo[h].ph, histo[h].nbins * sizeof(DWORD));
	      pdata += histo[h].nbins;  
	      bk_close(pevent, pdata);
	    }
	  
	} /* end of I-type modes */
      /* inform that the histo bank has been sent out */
      waiteqp[HISTO] = FALSE;
        if (ddd)  printf(" bksize:%d\n",bk_size(pevent));
      return bk_size(pevent);
    }
  else
    {
       if(ddd) 
	printf("histo_read: nothing to do\n");
    }
  return 0;
}

/*-- Event readout -------------------------------------------------*/
INT scalers_cycle_read(char *pevent, INT off)
/* - periodic equipment reading the scalers sum over a cycle
   - generate event only when cycle has been completed 
   and cycle is !skip_cycle
*/
{
  DWORD  *pwdata,*pWord7;

  double *pdata;
  double dTemp;
  float fTemp;
  double sum_f, sum_b, sum_all; /*temporary variables */
  INT    i,offset,offsetb;
  INT    back_cyc,front_cyc,ratio_cyc,asym_cyc;
  INT size;
  float rdata0,rdata2;
  INT icount, icount_max;
  INT status,  ihand, dvm_val2,  dvm_val0;
  static INT cntr_no_handshake1=0, cntr_no_handshake2=0;

  if (!waiteqp[FIFO] && waiteqp[CYCLE])
  {
    if (ddd)
    printf("scalers_cycle_read %d \n ", waiteqp[FIFO]);
    bk_init(pevent);



    /* POL : need a copy of the bank CYCI in this event 


CYCI Bank Contents for POL:

Word 1  cycle
Word 2  supercycle counter
Word 3  scan number
Word 4  helicity (dummy)

           POL DAC SCAN                             POL EPICS Scan (Phil's)

Word 5  8=POL DAC scan                           1=NaCell 2=Laser
Word 6  not used (was wavemeter)                 Epics Value Read
Word 7  not used (was DVM[2] (locking feedback)  Epics Value Written
Word 8  DAC Set value                            0
Word 9  DVM[0] DAC readback value                0 DVM[2] Locking feedback
          (-1 for error)                       

*/

    bk_create(pevent, "CYCI", TID_DWORD, &pwdata);
    *pwdata++ = gbl_CYCLE_N;    /* word 1 */     
    *pwdata++ = gbl_SCYCLE_N;   /* word 2 */  
    *pwdata++ = gbl_SCAN_N;     /* word 3 */
    *pwdata++ = gbl_ppg_hel; /*  word 4  use set value */
 
    /* Use next value as flag to indicate what kind of data 
       is in this bank 
       gbl_scan_flag has been set up at begin_of_run
       
       gbl_scan_flag = 0 freq scan
       = 1 NaCell 
       = 2 Laser
       = 3 Field
       = 4 dummy
       = 8 pol DAC 
       = 0x100 (256) CAMP frq 
       = 0x200 (512) CAMP Mag
       = 0x400 (1024)CAMP DAC (POL) 
       = 0x800 (2048)1a/1b with randomized frequencies (BNMR/BNQR)
       = 0x1000 1g with flip (helicity pairs) BNMR/BNQR
    */
    *pwdata++ = gbl_scan_flag;  /* word 5 */
    
    /* LockingFeedBack replaced by scaler 5 */
#ifdef GONE
    /* read DVM[2] ; "locking feedback signal (LFS)" needed for DAC scans (was FC2)  */
    ihand = 0;
    size = sizeof(ihand);
    icount = 0;
    icount_max = 1500; /* increase from 1000 as we are getting a few timeouts */ 
      do 
      {
	db_get_data(hDB, hHand2,&ihand,&size,TID_INT);
	icount +=1;
      }
    while ( (ihand == 0) && (icount != icount_max) );
    if( icount == icount_max) 
      {
	dvm_val2 = -1; /* flag an error */
	if(cntr_no_handshake2 > 3 && !ddac)
	  {
	    cm_msg(MERROR,"scalers_cycle_read","DAC2 handshake failing, check feDVM is running or hardware");
	    printf("scalers_cycle_read : no DAC2 handshake, calling set_client_flag with FAILURE\n");
	    set_client_flag("frontend",FAILURE); 
	  }
	else
	  { /* skip the cycle */
	    if(!d11)printf("\n");
	    printf("\rTimeout waiting for DAC2 handshake at cycle %d, skipping cycle \n",gbl_CYCLE_N);
	    cntr_no_handshake2++;
	    skip_cycle = TRUE;
	  }  
      }
    else 
      { /* we have a handshake */ 
	cntr_no_handshake2 = 0;
	size = sizeof(rdata2);
	status = db_get_data_index(hDB,hVar,&rdata2,&size,2,TID_FLOAT); /* read DVM[2] LFS (was FC2) */
	if(status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"scalers_cycle_read","failure from get_data_index for /equipment/DVM/Variables/Measured[2] (%d)",status);
	    dvm_val2 = -2; /* flags an error */
	  }
	else
	  {			       
	    dvm_val2 = (INT)( fabs(1000000*rdata2)+0.5);
	    if(rdata2 < 0) dvm_val2 *=-1;
	  }
      }
#endif
    dvm_val2=0; /* dummy */

#ifdef EPICS_ACCESS      
    if(epics_flag)  /* epics scan flag */
      {
	/* Epics scan value */
	*pwdata++ = (DWORD) (epics_params.Epics_read*1000);   /* word 6 EPICS value read */
	*pwdata++ = (DWORD) (epics_params.Epics_val*1000);    /* word 7 EPICS value written */
	*pwdata++ = 0;  /* word 8  clear set point value of  CAMP or PSM/FSC freq or POL DAC */
	
	/* POL and EPICS: Phil's experiment 
	   Put DVM[2] value (locking feedback signal) into Word 9 now  (now a Dummy)  */

	 *pwdata++ = dvm_val2; /* word 9 is DVM index [2] value for Phil's expt  */
	 /*	 if(!d11)
	   printf("\n dvm_val[2] =%d(%f)\n",
	   dvm_val0,rdata2); */ 
      }
    else
#endif /* end of EPICS */
      { /* NOT EPICS scan; clear EPICS parameters */
	   
	*pwdata++ =0; /*  word 6 */ 
	pWord7=pwdata; /* remember where word 7 is */
	*pwdata++ =0; /*  clear word 7 */
	
	if(mode10_flag)
	  {
	    *pwdata++ =0 ;    /* word 8; nothing is actually being scanned */
	    *pwdata++ =0 ;    /* word 9; empty */
	  }
 
	else if(pol_DAC_flag)
	  {
	    *pwdata++ = (DWORD)( (dac_val*1000.)+0.5); /* word 8; store DAC Set value */
	    /* Get DVM[0] (DAC readback value)  now */
	    ihand = 0;
	    size = sizeof(ihand);
            icount = 0;
	    icount_max = 1000;
            do 
	      {
		db_get_data(hDB, hHand,&ihand,&size,TID_INT);
		icount +=1;
	      }
	    while ( (ihand == 0) && (icount != icount_max) );
            if( icount == icount_max) {
	      *pwdata++ = -1; /* word 9 flags an error */
	      /* try skipping the cycle */
	      if(cntr_no_handshake1 > 3 && !ddac)
		{
		  cm_msg(MERROR,"scalers_cycle_read","DAC1 handshake failing, check feDVM is running or hardware");
		  printf("scalers_cycle_read : no DAC1 handshake, calling set_client_flag with FAILURE\n");
		  set_client_flag("frontend",FAILURE); 
		}
	      else
		{
		  printf("Timeout waiting for DAC handshake at cycle %d, skipping cycle \n",gbl_CYCLE_N);
		  cntr_no_handshake1++;
		  skip_cycle = TRUE;
		}
	    }
	    else 
	      {
		cntr_no_handshake1 = 0;
		size = sizeof(rdata0);
		status = db_get_data_index(hDB,hVar,&rdata0,&size,0,TID_FLOAT);
		if(status != DB_SUCCESS)
		  {		  
		    cm_msg(MERROR,"scalers_cycle_read",
			   "failure from get_data_index for /equipment/DVM/Variables/Measured[0] (%d)",status);
		    *pwdata++ = -2; /* word 9 flags an error */
		  }
		else
		  {
		    dvm_val0 = (INT)( fabs(1000000.*rdata0)+0.5);
		    if(rdata0<0) dvm_val0*=-1;
		    *pwdata++ = dvm_val0; /* word 9 is DVM[0] value */
		  }
		
	      
		
		/* now save the value dvm_val2 into word 7 ... now a dummy */      
		*pWord7 = dvm_val2; /* word 7 is the DVM[2], Locking Feedback Signal (previously FC2) */

		if(d12)
		  printf("\n +++ DAC +++gbl_scan_flag=%d,dac_val=%f, dvm_val[0]=%d(%f) \n",
			 gbl_scan_flag,dac_val,dvm_val0,rdata0);
		else
		  if(!d11)printf("\t\t\tDVM[0] (DAC readback)=%.2f",rdata0);
#ifdef GONE
		/* locking feedback replaced by scaler 5 */
		if(d12)
		  printf("\n +++ DAC +++gbl_scan_flag=%d,dac_val=%f, dvm_val[0]=%d(%f) dvm_val[2]=%d(%f)\n",
			 gbl_scan_flag,dac_val,dvm_val0,rdata0,dvm_val2,rdata2);
		else
		  if(!d11)printf("\t\t\tDVM[0] (DAC readback)=%.2f; DVM[2] (LockingFeedback)=%f",rdata0,rdata2);
#endif
	      }
	  }
	else
	  {
	    if(d10)
	      printf("\n +++ FREQ +++gbl_scan_flag=%d,freq_val=%u\n",
		     gbl_scan_flag,freq_val);
	    *pwdata++ = freq_val; /* word 8; store FSC/PSM frequency value */
	    *pwdata++ = 0; /* word 9 is empty */
	  }
      }
    
    bk_close(pevent, pwdata);  /* end of CYCI bank */


    bk_create(pevent, "HSCL", TID_DOUBLE, &pdata);
    
    /* Note: for experiment 1A/1B there are  software scalers filled in 
       directly in scaler_increment */
    
    /* add in any software scalers here (e.g. sums of inputs) */
/*
  !!!!!!!!!!!!!!!!!!!!!!!!        NOTE           !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!    Note: these must be filled into pdata in the SAME ORDER as the INDEX  or   !!!!!
  !!    they will be muddled up in odb & scaler event                              !!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/  

/* one scaler */
    offset = POL_OFFSET;
	   
    /* add only POL's scalers to the event (POL_OFFSET 12) */
    *pdata++ = scaler[0].sum_cycle; /* Ref channel */
     for (i=offset ; i < offset + NUM_POL_CHAN ; i++)
      *pdata++ = scaler[i].sum_cycle;
  
    bk_close(pevent, pdata);
    
    /* Add to Epics  (fe_epics) (BNMR.BNQR only) */
    if (hEPD)
      {
	  /* Cycle # to Epics in var[A+9] */
	  fTemp = (float) gbl_CYCLE_N;
	  db_set_data_index(hDB, hEPD, &fTemp, sizeof(fTemp), N_EPICS_VAR_A+9, TID_FLOAT);
      }

      waiteqp[CYCLE] = FALSE;
      
      if (ddd) printf("bksize:%d\n",bk_size(pevent));
      if(skip_cycle)
	return 0;  /* No bank generated when skip_cycle is TRUE */

      if(ddd)printf("scalers_cycle_read: returning event length=%d\n",bk_size(pevent));
      return bk_size(pevent);
  }
  else
    return 0;    
}

/*-- Event readout -------------------------------------------------*/
INT info_odb(char * pevent, INT off)
/* - periodic equipment updating the ODB ONLY
   - no event generation for the data stream.
*/
{  
  cyinfo.helicity = gbl_ppg_hel; /* use set value */
  cyinfo.current_cycle = gbl_CYCLE_N;
 /* Aug 22nd, RP now that the concept of "scans" is working changed the variable
     spare to --> scan #  
     cyinfo.spare = gbl_bin_count;*/
  cyinfo.current_scan = gbl_SCAN_N;
#ifdef EPICS_ACCESS
  if(epics_flag)
    {
      cyinfo.epicsdev_set_v_ = epics_params.Epics_val;   /* epics scan */
      cyinfo.epicsdev_read_v_ = epics_params.Epics_read; /* epics scan */
    }
  else
#endif
    {
      cyinfo.epicsdev_set_v_ = 0;   /* epics scan */
      cyinfo.epicsdev_read_v_ = 0; /* epics scan */
    }
  if(!camp_flag)
    {
      cyinfo.campdev_set = 0;   /* camp scan */
      cyinfo.campdev_read = 0; /* camp scan */
    }

  memcpy(pevent, (char *)&(cyinfo.helicity), sizeof(cyinfo));
  pevent += sizeof(cyinfo);
  if (dddd) logMsg ("info_odb %d size:%d\n",gbl_CYCLE_N,sizeof(cyinfo),0,0,0,0);
  return sizeof(cyinfo);
}



/* 
This is the original scaler_increment without any complications such as
randomized frequency and pulse pair mode 

*/
/*-- individual scaler histo increment ------------------------------------*/
INT scaler_increment(const DWORD nwords, DWORD * pfifo, INT scaler_offset, 
                            INT maximum_channel,INT *gbl_bin, DWORD *userbit )
/*
  - The fifo is composed of sequences of nscalers.
  Each DWORD data contains the channel and data info
  32      24                      1
  v       v                      v
  10987654321098765432109876543210
  UUFCCCCCddddDDDDddddDDDDddddDDDD
  where U: user bits (front panel inputs)
  F: latch bit (not used)
  C: channel bit (C4..C0)
  dD: data bit  (24bit)
  - for each word extract channel number (h) from the C4-C0 field
  - add that channel (h) total sum (per cycle).
  - increment current scaler[h] bin by fifo content (by cycle).
  - increment gbl_BIN every h=0 refering to a new scaler sequence.
*/
{
  DWORD *ps, i, h, ub, *pbuf,  sdata;
  INT     channel_field;
  INT  index;
  INT cnt;
  cnt=0;
  
  pbuf = pfifo;
  if (ddd) 
    {
      printf("scaler_increment  nwords:%d pfifo:%p, \n",nwords, pbuf);
      printf("scaler_increment  scaler_offset:%d max_channel:%d, *gbl_bin:%d\n",
	     scaler_offset, maximum_channel ,*gbl_bin);
    }

  for (i=0 ; i < nwords ; i++)
    {
      /* extract channel number */
      channel_field = (*pbuf >> 24);
      
      /* h has a range from 0..31 */
      h = channel_field & 0x1F;
     
      

      /* make sure channel is correct */
      if (h < maximum_channel)
	{
	  index = h + scaler_offset;
	  /* assign pointer */
	  ps = scaler[index].ps;
	  sdata = *pbuf & DATA_MASK; 
	  /* increment local cycle sum scaler */
	  scaler[index].sum_cycle += (double)sdata;
	  
	  /* temp measure for debugging 
	     bnmr/bnqr  write up to first hundred bins for channel 0
	     pol       write up to first hundred words for all channels
	  */
	  
	  if(ddd){
	    if(i<=100){
	      printf("i,pbuf,h,ub,sdata,gbl_bin: %d, %x, %d, %d, %x, %d\n",i,*pbuf,h,ub,sdata,*gbl_bin);
	    
	      } 
	    if(i == nwords -1 ){
	      printf("Last word i,pbuf,h,ub,sdata,gbl_bin: %d, %x, %d, %d, %x, %d\n",i,*pbuf,h,ub,sdata,*gbl_bin); 
	    }
	  }
	  /* add current time bin content to cumulative scaler histo */
	  *(ps+*gbl_bin) += sdata;
	  

	  /* next incoming data */
	  pbuf++;
	  
	  /* Increment time bin every h = N_SCALER_REAL */
	  if (h == (maximum_channel-1) )
	    {
	      (*gbl_bin)++;
              if(ddd) {
		if (cnt < 10 )
		  { 
		    printf("next time bin at h=%d i=%d gbl_bin=%d\n",h,i,*gbl_bin);
		    cnt++;
		  }
	      }
	    }
	}
      else
	printf("Oops - incorrect channel: Idx:%d nW:%d ch:%d  data:0x%8.8x\n",i, nwords, h, *pbuf);    
    }
  return 0;
}




/*-- Histo Event  ---------------------------------------------------------*/
INT histo_process(void)
/*------------------------------------------------------------------------*/
/*

  - check threshold if > 0
    threshold = sum of 32 scalers under this helicity
    check versus cycle_thr
      if > don't include cycle
      if < increment histo[x] form scaler[y] see comment below 
      Type 1:
           - 2 scalers: sum scaler A  0-15  to h0 
           - 2 scalers: sum scaler A  16-31  to h1 
	   - 2 scalers: h2 filled in scaler_increment (type 1)

	   - sum scaler B ch 0,1 to h1,2 (fluorescence monitors or SIS Ref Ch1 may be enabled)
	   - sum scaler B ch 2,3 to h3,4 (pol monitors )
	   - sum scaler B ch 4-7  to h5 (backward  neutral beam counters)
	   - sum scaler B ch 8-11 to h6 (forward neutral beam counters)
	   - 1 scaler ONLY:  h7 filled in scaler_increment (type 1)

*/
{
  double sum032, ratio, rtmp;
  INT h, i, j;
  DWORD async_data=0;
  /*  BOOL check_rf_tripped = FALSE; */
  INT size,status;
  float   end_value;
  float ftemp;

  float ref_value1, ref_value2, ref_value3;

  float  prev_last_failed;
  INT hel_offset,type_offset;

  if(ddd) 
    printf("Entering histo_process routine\n");

  /* Check if someone has set skip_cycle to TRUE before - IT SHOULD NOT BE DONE 
     but better make sure ... */
  if(skip_cycle){
    printf("\n skip_cycle is TRUE when entering routine histo_process \n It should not happen \n");
    cyinfo.cancelled_cycle ++;
    return 1;  
  }

  if(first_cycle){
    printf("\n Histo process: this is the first cycle of the run - ignore it \n");
    if(d11)printf("\n ** Note: progress display is turned off by default **\n");
    first_cycle = FALSE;
    skip_cycle = TRUE;
    cyinfo.cancelled_cycle ++;

    return 1;  
  }

  /* check if the run has been put on hold (using (hot-linked) BNMR hold flag)  */
  if (fs.flags.hold)
  {
    if (!hold_flag)
    {
      if(ddd) printf("BNMR Hold flag is detected; no histograms will be sent\n");
      cm_msg(MINFO,"histo_process","Run is on hold. No histograms will be sent.");
    }
   
    hold_flag = TRUE;
    skip_cycle = TRUE;
    cyinfo.cancelled_cycle ++;
    return 1;  
  }
  else
  {
    if (hold_flag)
    {
      if(ddd) printf("Run is being resumed; histograms will now be sent\n");
      cm_msg(MINFO,"histo_process","Run is resumed. Histograms will now be sent.");
    }
    hold_flag= FALSE;
  }


  /* check epics if relevant */
  if (epics_flag)
    {
      /* check value again before histogramming data */
      /*  recheck value by comparing present value */
      end_value=0.0;

#ifdef EPICS_ACCESS

      if(d5)printf("histo_process: calling read_epics_val to recheck value before histogramming \n");
      status = read_epics_val( &end_value);
      if(status==-1)
	{
	  printf("Bad status returned from read_epics_val for %s (%d)\n",fs.output.e1n_epics_device,status);
	  cm_msg(MERROR, "histo_process", "error  reading %s Epics scan value (%d)",fs.output.e1n_epics_device,status);
	  return DB_NO_ACCESS;
	}
      if(d5)
	  printf ("histo_process: read back %.4f, expected value %.4f for %s\n",
		       epics_params.Epics_read, end_value, fs.output.e1n_epics_device);
 /* check the stability */
      if(fabs (end_value - epics_params.Epics_read) > epics_params.Epics_stability )
	{
	  printf("\nhisto_process: Unstable %s Value, Set value=%.4f%s,just read  %.4f,expected %.4f, diff > %.4f\n",
		 fs.output.e1n_epics_device,epics_params.Epics_val, epics_params.Epics_units, end_value, epics_params.Epics_read, epics_params.Epics_stability);
	  skip_cycle = TRUE; 
	  status = set_epics_val(); /* set Epics device to epics_params.Epics_val */
	  cyinfo.cancelled_cycle ++;
	  if(status != SUCCESS) 
	    return FE_ERR_HW;
	}
      else
	epics_params.Epics_last_value = end_value; /* remember this value ... */

      /* check for jump scan  */
      if( fs.input.enable_scan_jump)
	{
	  if (gbl_jump_now)
	    {
	      float ftemp_value;
	      gbl_jump_now = FALSE;
	      /* next value will be a jump ...
	         for Epics we have to set one incr. away from jump value now */
	      if(dj)printf(
               "histo_process: Now setting Epics to pre-jump value ( %.2f), prior to jumping to %.2f  \n",
	       gbl_prejump_value,  epics_params.Epics_start + 
	       ( (gbl_inc_cntr - gbl_fix_inc_cntr) * epics_params.Epics_inc)); 

	      ftemp_value =   epics_params.Epics_val; /* remember this value */
	      epics_params.Epics_val = gbl_prejump_value; /* set_epics_val uses this */
	      if(dj||d5)printf(
	       "histo_process: calling set_epics_val with  epics_parameters.Epics_last_offset=%.2f\n",
	       epics_params.Epics_last_offset);
	      status = set_epics_val() ;
	      epics_params.Epics_val = ftemp_value;
	      if (status != SUCCESS)
		{
		  printf("histo_process: can't set Epics to pre-jump value of %.2f, last offset was %.2f\n",
			 gbl_prejump_value,  epics_params.Epics_last_offset);
		  /* continue regardless */
		} 
	    } /* end of gbl_jump_now */
	} /* end of enable_scan_jump */

#endif
    } /* end of epics flag */
  

  

/* ====================================================
        POL's threshold checks 
   ====================================================*/

  /*  P+ beam threshold  (cycle_thr1) */

   
  ref_value1  = (float)scaler[POL_P_PLUS].sum_cycle;
  ref_value2  = (float)scaler[POL_LASER].sum_cycle;
  ref_value3  = (float) scaler[POL_FC15].sum_cycle;

  prev_last_failed= cyinfo.cycle_when_last_failed_thr;
  cyinfo.last_failed_thr_test=0;
  if(gbl_CYCLE_N ==1)
    {
      cyinfo.ref_p__thr = ref_value1;
      cyinfo.ref_laser_thr = ref_value2;
      cyinfo.ref_fcup_thr = ref_value3; 
      printf("\nFirst cycle...reset reference thresholds (p+,laser,Fcup) to current (%f,%f,%f)\n",
	     ref_value1,ref_value2,ref_value3);
    }
  else
    {
      /* check for Re-reference of all the thresholds  */
      if (hot_rereference)
	{
	  cyinfo.ref_p__thr = ref_value1;
	  cyinfo.ref_laser_thr = ref_value2;
	  cyinfo.ref_fcup_thr = ref_value3; 
      
	  hot_reference_clear(0); /* 0 clears all */
	  printf("\nRereference.....reset all reference thresholds (p+,laser,Fcup) to current (%f,%f,%f)\n",
		 ref_value1,ref_value2,ref_value3);
	  ncycle_sk_tol = 0; /* counter for skipping cycles out-of-tol */	      
	}
      else if (hot_reref1)
	{
	  cyinfo.ref_p__thr = ref_value1;
	  hot_reference_clear(1); /* 1 clears p+ */
	  printf("\nRereference.....reset P+ reference threshold to current (%f)\n",
		 ref_value1);
	  ncycle_sk_tol = 0; /* counter for skipping cycles out-of-tol */	      
	}
      else if (hot_reref2)
	{
	  cyinfo.ref_laser_thr = ref_value2;
	  hot_reference_clear(2); /* 2 clears laser */
	  printf("\nRereference.....reset laser reference threshold to current (%f)\n",
		 ref_value2);
	  ncycle_sk_tol = 0; /* counter for skipping cycles out-of-tol */	      
	}

      else if (hot_reref3)
	{
	  cyinfo.ref_fcup_thr = ref_value3;
	  hot_reference_clear(3); /* 3 clears Fcup */
	  printf("\nRereference.....reset Faraday Cup reference threshold to current (%f)\n",
		 ref_value3);
	  ncycle_sk_tol = 0; /* counter for skipping cycles out-of-tol */	      
	}
    }

  /* compose total sum */

  cyinfo.current_p__thr = ref_value1; /* update current value */
  cyinfo.current_laser_thr = ref_value2; /* update current value */
  cyinfo.current_fcup_thr = ref_value3; /* update current value */
  
  if(d3)printf("updating current thr values to p+=%7.0f, Laser=%7.0f, Fcup=%7.0f\n",
	       ref_value1, ref_value2, ref_value3); 


 /* check three thresholds for POL (p+ beam, laser, faraday cup) */
     
 /* Test 1   p+ beam  */
 if (fs.hardware.cycle_thr1 > 0.f)
   {
     if(d3)
       printf("Test1 (p+) starting: Cycle p+ thr level:%f; current %f; ref %f\n",
	      fs.hardware.cycle_thr1,
	      cyinfo.current_p__thr,
	      cyinfo.ref_p__thr);
     /* check total sum versus corresponding thr */
     if (cyinfo.ref_p__thr!=0)
       ratio = abs(cyinfo.ref_p__thr - cyinfo.current_p__thr) / cyinfo.ref_p__thr;
     else
       ratio = 0.;
     if(d3)printf("test1 p+ ratio=%.3f thr1=%.3f \n",
		  ratio, ((double)fs.hardware.cycle_thr1 / 100.) );
     if ( ratio > ((double)fs.hardware.cycle_thr1 / 100.))
       {
	 /* Test 1 fails */
	 cm_msg(MINFO,"histo_process","P+ cycle below thr (%5.3f);  out of tolerance",ratio);
	 /* cm_msg(MTALK,"histo_process","Out of tolerance"); */
	 if(d3)printf("Test1 (p+) fails, Ref = %7.0f; Current = %7.0f; Ratio (%5.3f) > ref cycle thr/100 (%5.3f)\n",
		      cyinfo.ref_p__thr,  cyinfo.current_p__thr,ratio, 
		      ((double)fs.hardware.cycle_thr1 / 100.));   
	 skip_cycle = TRUE;
	 cyinfo.cancelled_cycle ++;
	 ncycle_sk_tol = fs.hardware.skip_ncycles_out_of_tol;
	 cyinfo.last_failed_thr_test=1; /* alarm should go off */
	 if(  prev_last_failed > 1)
	     clear_alarm(prev_last_failed);
	 cyinfo.cycle_when_last_failed_thr=gbl_CYCLE_N;
	 return 1; /* fail Test 1 */
       }	
     /* Test 1 passes */
     if(d3)printf("passed test 1 for p+\n");
   }  /* end of Test 1 (p+ beam) */
 
 
 /* Test 2   (laser)  */
 if (fs.hardware.cycle_thr2 > 0.f)
   {
     if(d3)
       printf("Test2 (laser) starting: Cycle laser thr level:%f; current %f; ref %f\n",
	      fs.hardware.cycle_thr2,
	      cyinfo.current_laser_thr,
	      cyinfo.ref_laser_thr);
     /* check total sum versus corresponding thr */
     if (cyinfo.ref_laser_thr!=0)
       ratio = abs(cyinfo.ref_laser_thr - cyinfo.current_laser_thr) / cyinfo.ref_laser_thr;
     else
       ratio = 0.;
     if(d3)printf("test2 laser ratio=%.3f thr2=%.3f \n",
		  ratio, ((double)fs.hardware.cycle_thr2 / 100.) );
     if ( ratio > ((double)fs.hardware.cycle_thr2 / 100.))
       {
	 /* Test 2 fails */
	 cm_msg(MINFO,"histo_process","Laser cycle below thr (%5.3f);  out of tolerance",ratio);
	 /* cm_msg(MTALK,"histo_process","Out of tolerance"); */
	 if(d3)
	   printf("Test2 (laser) fails, Ref = %7.0f; Current = %7.0f; Ratio (%5.3f) > ref cycle thr/100 (%5.3f)\n",
		  cyinfo.ref_laser_thr,  cyinfo.current_laser_thr,ratio, 
		  ((double)fs.hardware.cycle_thr2 / 100.));   
	 skip_cycle = TRUE;
	 cyinfo.cancelled_cycle ++;
	 ncycle_sk_tol = fs.hardware.skip_ncycles_out_of_tol;
	 cyinfo.last_failed_thr_test=2; /* alarm should go off */
	 if(  prev_last_failed > 0 && prev_last_failed !=2 )
	     clear_alarm(prev_last_failed);
	 cyinfo.cycle_when_last_failed_thr=gbl_CYCLE_N;
	 return 1; /* fail Test 2 */
       }	
     /* Test 2 passes */
     if(d3)printf("passed test 2 for laser\n");
   }  /* end of Test 2 */
 

 /* Test 3   (Faraday Cup 15 )  */
 if (fs.hardware.cycle_thr3 > 0.f)
   {
     if(d3)
       printf("Test3 starting: Cycle Fcup thr level:%f; current %f; ref %f\n",
	      fs.hardware.cycle_thr3,
	      cyinfo.current_fcup_thr,
	      cyinfo.ref_fcup_thr);
     /* check total sum versus corresponding thr */
     if (cyinfo.ref_fcup_thr!=0)
       ratio = abs(cyinfo.ref_fcup_thr - cyinfo.current_fcup_thr) / cyinfo.ref_fcup_thr;
     else
       ratio = 0.;
     if(d3)printf("test3 Faraday cup ratio=%.3f thr=%.3f \n",
		  ratio, ((double)fs.hardware.cycle_thr3 / 100.) );
     if ( ratio > ((double)fs.hardware.cycle_thr3 / 100.))
       {
	 /* Test 3 fails */
	 cm_msg(MINFO,"histo_process","Faraday cup cycle below thr (%5.3f);  out of tolerance",ratio);
	 /* cm_msg(MTALK,"histo_process","Out of tolerance"); */
	 if(d3)
	   printf("Test3 (fcup) fails, Ref = %7.0f; Current = %7.0f; Ratio (%5.3f) > ref cycle thr/100 (%5.3f)\n",
		  cyinfo.ref_fcup_thr,  cyinfo.current_fcup_thr,ratio, 
		  ((double)fs.hardware.cycle_thr3 / 100.));   
	 skip_cycle = TRUE;
	 cyinfo.cancelled_cycle ++;
	 ncycle_sk_tol = fs.hardware.skip_ncycles_out_of_tol;
	 cyinfo.last_failed_thr_test =3; /* alarm should go off */
	 if(  prev_last_failed > 0 && prev_last_failed !=3 )
	     clear_alarm(prev_last_failed);
	 cyinfo.cycle_when_last_failed_thr=gbl_CYCLE_N;
	 return 1; /* fail Test 3 */
       }	
     /* Test 3 passes */
     if(d3)printf("passed test 3 for Fcup\n");
   }  /* end of Test 3 */
 
 if (ncycle_sk_tol > 0)
   {
     skip_cycle = TRUE;
     cyinfo.cancelled_cycle++;
     printf("Cycle is in tolerance but ncycle_sk_tol still positive (%i)\n",
	    ncycle_sk_tol);
     ncycle_sk_tol--;
	
  
     /* Repeat scan if user sets  gbl_repeat_scan.
 
     gbl_repeat_scan ->  fs.hardware.out_of_tol_repeat_scan.
     user sets this param true to repeat the scan 
     */
     if((ncycle_sk_tol == 0)  && fs.hardware.out_of_tol_repeat_scan) 
       {
	 /* must restart the current scan for type 1 (Type 1 runs only for POL)  */
	 if (epics_flag) 
	   {
#ifdef EPICS_ACCESS
	     gbl_inc_cntr = 0;
	     gbl_SCAN_N ++;
	     if(epics_params.Epics_inc > 0)
	       epics_params.Epics_val= epics_params.Epics_start - epics_params.Epics_inc;
	     else
	       epics_params.Epics_val= (epics_params.Epics_start - 
					((epics_params.Epics_ninc+1)*epics_params.Epics_inc));
#endif
	   }
	 else if (!mode10_flag) 
	   { /* DA */
	     gbl_FREQ_n = 0;
	     gbl_SCAN_N ++;
	     if(freq_inc > 0)
	       freq_val= freq_start - freq_inc;
	     else
	       freq_val= (freq_start - ((freq_ninc+1)*freq_inc));
	   } /* DA added these braces, right??? Yes!!  */
       }
	else if(pol_DAC_flag)
	  {
	    clear_scan_flag(); /* clear POL's bad scan flag */
	    gbl_FREQ_n = 0;
	    gbl_SCAN_N ++;
	    dac_val = dac_start + ((gbl_FREQ_n - gbl_fix_inc_cntr) * dac_inc);
	  }
   }  
  /* end of POL threshold checks */

  /* add POL's fluorescence monitor counts to cyinfo (no threshold check on this one) */

   cyinfo.fluor_monitor_counts = scaler[FLUOR_CHAN2].sum_cycle; /* fluor monitor channel 12 for POL  */


  /*  NOW all tests are done - fill histograms */
  if(ddd)printf("histo_process - start processing data\n");
  if(exp_mode == 1) {
    hel_offset = 0; /* no separate P and M histograms */
    type_offset = 0; 
  } else {
    type_offset = 2;
    hel_offset = 0; /* HEL_DOWN always for POL - helicity not used */
  }



  /* take care of Scal B histo's  */
  /* fill first of ScalerB's histos (ref channel) offset = 0 */
  j=0; /* histo counter */
  {
    for (i=0 ; i < n_his_bins ; i++)
    {
      histo[j].ph[i] += scaler[0].ps[i];
    }
  }

  j=1; /* next of ScalerB's (Pol XXX) */
  {
    for (i=0 ; i < n_his_bins ; i++)
    {
          histo[j].ph[i] += scaler[POL_XXX].ps[i]; 
    }
  }
  return 1;
}

/*-- Setup SIS ---------------------------------------------------------*/
INT sis_setup(DWORD nchan, float dwell)
/*------------------------------------------------------------------------*/
/*
  - Initialize the SIS3801 based on the rm struct and /settings
  - Enable channels 
  - Check and set dwelling time
  Inputs:  nchan        no. of channels (= # of hardware scalers)
	   dwell        dwell time

  NOTE - SIS module can enable up to 24 channels individually. Anything larger
  enables all 32

  
  WARNING - changing this subroutine often causes the sis3801 to cease
  giving interrupts i.e. no events are produced for no apparent reason.
  I have not been able to successfully remove nchan and dwell from the input parameter
  list (they are globals so should not need to be passed). It results in the
  cessation of interrupts.
        
*/
{
  DWORD idwell;
  DWORD   nchan_A,nchan_B; /* number of enabled REAL channels for SIS modules */
  
  nchan_A=MAX_CHAN_SIS3801A;

  if (ddd) printf ("sis_setup: starting with nchan = %d\n",nchan);
  /* check & set number of active channels */

  nchan_B = nchan;

  if (nchan_B >  MAX_CHAN_SIS3801B)
  {
    printf("sis_setup: Number of channels for SIS3801 module B (%d) must be less than %d\n",nchan_B,MAX_CHAN_SIS3801B);
    return FE_ERR_HW;
  }


  /* note: if this line is removed we get no interrupts or no data in one or
     both modules  (in test or real mode)
     if it is replaced by
     sis3801_channel_enable(SIS3801_BASE_n,N_SCALER_REAL); or
     sis3801_channel_enable(SIS3801_BASE_n,32); or
     sis3801_channel_enable(SIS3801_BASE_n,MAX_CHAN_SIS3801A); 
        where n=A or B
  */
  sis3801_channel_enable(SIS3801_BASE_B,nchan_B); 

  if(dd) printf("sis_setup: SIS enabled channels : Module B %d\n", nchan_B);
 

  /* mode 6 : using an external pulse for dwell time */
  if(dd) printf("sis_setup: Using external next pulse\n");
  return FE_SUCCESS ;
}

/*------------------------------------------------------------------------*/
INT helicity_read(void)
/*------------------------------------------------------------------------*/
{
  /* Read the Epics ILE2:POLSW2:STATON using channel access routines 

  NOTE: both gbl_HEL (value read from EPICS) and gbl_ppg_hel (Value set by PPG)
          are globals

	  fills gbl_HEL and gbl_ppg_hel, returns STATUS
	  status = SUCCESS  successfully read HEL, values agree
	           FE_ERR_HW could not read HEL from epics
		   DB_TYPE_MISMATCH values from epics and from PPG don't agree
*/
  float value;
  INT i,status;

  /* POL */
  gbl_HEL=gbl_ppg_hel=HEL_DOWN;
  return SUCCESS;  /* helicity is always set to 0 */

}

/*------------------------------------------------------------------------*/
INT set_init_hel_state(BOOL flip, INT hel_state)
/*------------------------------------------------------------------------*/
{
  /* 
     Parameters: 
     flip = TRUE if helicity flipping is enabled
     hel_state = desired helicity state

     Call to helicity_read updates gbl_ppg_hel (and gbl_HEL in single channel mode only).

     gbl_HEL = readback from EPICS of helicitiy switch status
     gbl_ppg_hel = value requested by PPG (i.e. value set on output line - under VME control not pulseblaster)
     NOTE: The Epics helicity switch is edge driven.

     Dual Channel Mode: 
         Any change in helicity state now takes place only when a particular channel 
         has the beam, so gbl_HEL and gbl_ppg_hel may not agree.
	 Epics readback (gbl_HEL) is not useful for this mode; we need a latched signal from the 
	 switch itself.
   
	 We only need get the PPG output line in the correct state. Since the first cycle is
	 thrown away the actual helicity should be correct by the second cycle.

     Single Channel Mode:
         Since the Epics helicity switch is edge driven, we have to send an edge to change the
         helicity. So if readback is HEL_UP & set value & desired value are both HEL_DOWN, ppg
	 has to send HEL_UP followed by HEL_DOWN to get helicity to change.
	 This channel always has the beam, helicity readback should be correct.
      
  */

  INT status;

#ifndef EPICS_ACCESS  
  /* sets gbl_ppg_hel appropriately to satisfy checks on helicity */
  helicity_read(); /* reads present helicity (gbl_ppg_hel) & sets gbl_HEL=gbl_ppg_hel */ 
  if(gbl_ppg_hel != hel_state)
    gbl_ppg_hel=ppgPolzFlip(PPG_BASE); /* flip the helicity */
  if(dh)printf("set_init_hel_state: (no epics) after ppgPolzFlip, gbl_ppg_hel = %d hel_state = %d\n",gbl_ppg_hel,hel_state);
  return SUCCESS;
#endif


  /* DUMMY helicity */
  printf("Up/Down scan selected for POL experiment (helicity is NOT actually flipped)\n");
  gbl_ppg_hel = gbl_HEL = hel_state;



  return SUCCESS;
}



/* POL has more hot links to clear  */

/*-- Clear hot link ------------------------------------------------------*/
void hot_reference_clear(INT item)
/*------------------------------------------------------------------------*/
{
  INT size;
  /* POL version: item = 0 clear all
                         1 clear p+
                         2 clear laser
                         3 clear Fcup
  */
  switch (item)
    {
    case 0: 
      {
	hot_rereference = FALSE;
	size = sizeof(hot_rereference);
	db_set_value(hDB, hFS, "Hardware/re-Reference", &hot_rereference, size, 1, TID_BOOL);
	if(hot_reref1)
	  {
	    hot_reref1=FALSE;
	    db_set_value(hDB, hFS, "Hardware/re-ref_p+", &hot_rereference, size, 1, TID_BOOL);
	  }
	if(hot_reref2)
	  {
	    hot_reref2=FALSE;	   
	    db_set_value(hDB, hFS, "Hardware/re-ref_Laser", &hot_rereference, size, 1, TID_BOOL);
	  }
	if(hot_reref3)
	  {
	    hot_reref3=FALSE;
	    db_set_value(hDB, hFS, "Hardware/re-ref_Fcup", &hot_rereference, size, 1, TID_BOOL);
	  }	  
	if(d3)printf("hot_reference_clear: Cleared all hot rereferences in odb\n");
	  
      }
    case 1:
      {
	hot_reref1 = FALSE;
	size = sizeof(hot_reref1);
	db_set_value(hDB, hFS, "Hardware/re-ref_p+", &hot_reref1, size, 1, TID_BOOL);
	if(d3)printf("hot_reference_clear: Cleared hot reref_p+ in odb\n");
	break;
      }
    case 2:
      {
	hot_reref2 = FALSE;
	size = sizeof(hot_reref2);
	db_set_value(hDB, hFS, "Hardware/re-ref_Laser", &hot_reref2, size, 1, TID_BOOL);
	if(d3)printf("hot_reference_clear: Cleared hot reref_Laser in odb\n");
	break;
      }
    case 3:
      {
	hot_reref3 = FALSE;
	size = sizeof(hot_reref3);
	db_set_value(hDB, hFS, "Hardware/re-ref_Fcup", &hot_reref3, size, 1, TID_BOOL);
	if(d3)printf("hot_reference_clear: Cleared hot reref_Fcup in odb\n");
	break;
      }
    default:
      {
	printf("hot_reference_clear: error - called with illegal parameter (%d)\n",item);	
      }
    }
}


/*-- Clear scaler i ------------------------------------------------------*/
void scaler_clear(INT i)
/*------------------------------------------------------------------------*/
{
  /* scaler_clear is for REAL SCALERS only */
  INT j;
  
  for (j=0;j<scaler[i].nbins;j++)
    scaler[i].ps[j]=0;
  scaler[i].sum_cycle = 0.0;
}

/*-- Clear histo h ------------------------------------------------------*/
void histo_clear(INT h)
/*------------------------------------------------------------------------*/
{
  INT j;
  for (j=0;j<histo[h].nbins;j++)
    histo[h].ph[j]=0;
  histo[h].sum = 0.0;
  histo[h].bin_max = 0.0;
}

/*-- Check time file was changed last  -----------------------------------*/
/*------------------------------------------------------------------------*/
INT check_file_time(void)
/*------------------------------------------------------------------------*/
{
 /* checks whether a file bytecode.dat has been made recently by rf_config.
    If file not created recently, checks if rf_config is running.
       If rf_config is running, waits 5s and retries.
         
       note: can't find time of file's last modification directly (no stat libs under vxworks) 
             get the present binary time; the ascii time does not seem to be
             local time, but the binary time can be used
 */
#ifdef PPCxxx
  time_t timbuf;
  char timbuf_ascii[30];
  INT elapsed_time;
  INT status,size;
  char str[128];
  time(&timbuf);



  if(ddd)
    {
      strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) );          
      printf ("Present time:  %s or (binary) %d \n",timbuf_ascii,timbuf);   
      printf("According to odb, bytecode.dat's last change was at:  %s, binary=%d\n",
             fs.output.compiled_file_time, fs.output.compiled_file_time__binary_);
    }
  elapsed_time= (INT) ( timbuf - fs.output.compiled_file_time__binary_);
  if(ddd)
    printf("Time since file last updated: %d seconds \n",elapsed_time);
  
  if(elapsed_time > rftime+epicstime)
    {
      /* check whether rf_config is running */
      status = cm_exist ("rf_config",FALSE);
      if (status  != CM_SUCCESS)
	{
	  cm_msg(MERROR,"check_file_time","rf_config is not running");
	  return(status);
	}
      else  /* wait and retry to find bytecode.dat; sometimes rf_config is slow */
	{
	  printf("   Waiting another 10s  for bytecode.dat to be created (rf_config IS running)....\n"); 
	  ss_sleep(5000);
	  cm_yield (100); 
	  ss_sleep(5000); 
	  /* get the creation time from the odb this time */
	  size = sizeof( fs.output.compiled_file_time__binary_);
	  sprintf(str,"/Equipment/%s/sis mcs/Output/compiled file time (binary)",equipment[FIFO].name);
	  status = db_get_value(hDB, 0, str, &fs.output.compiled_file_time__binary_ , &size, 
				TID_DWORD, FALSE);
	  if(status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"check_file_time","cannot read compiled file time at %s (%d)",str,status);
	      return FE_ERR_ODB;
	    }
	  if(ddd)printf("Read binary time as %d from odb\n", fs.output.compiled_file_time__binary_);
	  
	  time(&timbuf);  /* get the present time again */
	  elapsed_time= (INT) ( timbuf - fs.output.compiled_file_time__binary_);
	  
	  if(ddd)printf("Time since file last updated: %d seconds \n",elapsed_time);
	  if(elapsed_time > rftime)
	    {
	      cm_msg(MERROR,"check_file_time",
		     "Rf_config has not created file bytecode.dat within %d seconds (elapsed time = %d)",
		     rftime,elapsed_time);
	      return DB_NO_ACCESS;  /* return a failure code */
	    }
	} /* end of wait and retry*/
    }  /* end of if elapsed time */
#else
  printf("I do not know how to check file time under Linux\n");
#endif
  return(CM_SUCCESS);
}

/*-- die ------------------------------------------------------------------*/
void die()
/*------------------------------------------------------------------------*/
{
  /* for now cancel the cycle */
  skip_cycle = TRUE;
  cyinfo.cancelled_cycle ++;
  /* Set this for  histo_read equipment; it will skip the cycle by testing skip_cycle */
  waiteqp[FIFO] = FALSE;
  logMsg ("DAQ dies at cycle %d \n",gbl_CYCLE_N,0,0,0,0,0); 

  return;
}

/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/fifo_acq/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag  "/equipment/fifo_acq/client flags/client alarm" to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in mdarc_subs.c for use of linux clients;  later can try to just have 
     one routine with ifdefs

  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status,size;
  if (d9)  
    printf("set_client_flag: starting\n");
 
  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",equipment[FIFO].name,client_name );
  /* if(d9) */
    printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 
  /* Set the client flag to my_value 
                 value =  TRUE (success)  or FALSE (failure) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }

  /* Set the alarm flag; TRUE - alarm should go off, FALSE alarm stays off  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",equipment[FIFO].name );
  if(d9)printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }
  return CM_SUCCESS ;
}

void iwait(INT sec)
{
	INT i;

	 for(i=0; i<sec; i++)
	 {
		ss_sleep(1000); /* sleep for 1 s */
		cm_yield(100); 
         }
	 return;
}


void update_fix_counter(INT num_increments)
{
  /* this subroutine called only if flip is true and
     we are at the end of a scan */

  if(gbl_CYCLE_N == 1 || !flip)
    { /* no action unless flip is true and not at begin of run */
      printf("update_fix_counter: no action, gbl_CYCLE_N=%d  flip=%d\n",gbl_CYCLE_N,flip);
      return;
    }

/* except at Begin of Run,
   for up/down scan, increment counter has to be fixed */

  /* printf("update_fix_counter: starting with num increments = %d\n",num_increments);*/
  if(gbl_fix_inc_cntr == 0)  /* global */ 
    gbl_fix_inc_cntr = num_increments -1;
  else
    gbl_fix_inc_cntr = 0;

   if(d8) 
    printf("\nupdate_fix_counter: end of scan, gbl_CYCLE_N=%d. flip=%d, gbl_fix_inc_cntr changed to %d\n",
	   gbl_CYCLE_N, flip,gbl_fix_inc_cntr);
  return;
}

void ppgStop(void)
{
  if(ddd)printf("ppgStop: setting Beam Off\n");
  ppgBeamOff(PPG_BASE);
  if(ddd)printf("ppgStop: stopping PPG's sequencer\n");
  ppgStopSequencer(PPG_BASE);
  ppgDisableExtTrig(PPG_BASE);
  ppgBeamCtlPPG(PPG_BASE); /* ppg controls the beam cf pol_init.c */
  ppgPolzSet(PPG_BASE,HEL_DOWN); /* set helicity to HEL_DOWN */    
  return;
}

INT print_hardware_flags(void)
{
  char str[128];
  sprintf(str,"none");


  sprintf(str,"POL");

  printf("This version of the frontend code is built for experiment %s\n",str);
  if(strncmp(str,BEAMLINE,strlen(str) != 0))
  { /* code is built for the wrong beamline */
    cm_msg(MERROR,"frontend","Mismatch - code was built for %s, but this Midas experiment is %s",
	   str,BEAMLINE);
    return FE_ERR_HW;
  }

  sprintf(str,"with these hardware flags defined: ");
#ifdef VMEIO
  strcat(str,"VMEIO, ");
#endif


#ifdef EPICS_ACCESS
 strcat(str,"EPICS ACCESS, ");
#endif

#ifdef SMALL_MEMORY
 strcat(str,"SMALL MEMORY PPC, ");
#endif

#ifdef TWO_SCALERS
  strcat(str,"and TWO SCALERS");
#else
  strcat(str,"and ONE SCALER ");
#endif

  printf("%s\n",str);
  return SUCCESS;

}

/*---------------------------------------------------------------*/
INT prepare_to_stop(void)
/*---------------------------------------------------------------*/
{
  INT status=0;
  /* called from frontend loop when we want to stop the run */

  if(gbl_waiting_for_run_stop)  /* flag has already been set */
    return SUCCESS;

  printf(" prepare_to_stop: calling set_client_flag to stop the run\n");
  status = set_client_flag("frontend",FAILURE); /* mdarc or user should stop the run 
                                                  (hot link on client flag). */

  if(status != SUCCESS)printf("prepare_to_stop: error from set_client_flag\n");

  loop_error_cntr++;
  gbl_waiting_for_run_stop = TRUE;
  ss_sleep(1000);/* let fe_runlog have a chance to stop this run */
  cm_yield(100);
 return status;
}
/*---------------------------------------------------------------*/
void setup_hotlinks(void)
/*---------------------------------------------------------------*/
{
  INT status;
  /* Called from begin_of_run to set up the hotlinks
     If a hotlink cannot be opened, a message is sent
  */
  char str[128];

  /* setup hot link on "re-reference" */
  if (hRR==0)
    {
      sprintf(str,"Hardware/Re-reference");
      status = db_find_key(hDB, hFS, str, &hRR);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hRR, &lhot_rereference
				  , sizeof(hot_rereference)
				  , MODE_READ, call_back, "rereference");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }


  
  if (hCT1==0)
    {
      /* setup hot link on "Cycle thr1" */
      sprintf(str,"Hardware/Cycle thr1");
      status = db_find_key(hDB, hFS, str, &hCT1);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hCT1, &fs.hardware.cycle_thr1
				  , sizeof(fs.hardware.cycle_thr1)
				  , MODE_READ, call_back, "cycle threshold1");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

  if (hCT2==0)
    {
      /* setup hot link on "Cycle thr2" */
      sprintf(str,"Hardware/Cycle thr2");
      status = db_find_key(hDB, hFS, str, &hCT2);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hCT2, &fs.hardware.cycle_thr2
				  , sizeof(fs.hardware.cycle_thr2)
				  , MODE_READ, call_back, "cycle threshold2");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","hot link on \"%s\" failed (%d)",str,status);
    }

  if (hFT==0)
    {

      /* setup hot link on Cycle thr3 for POL */
      sprintf(str,"Hardware/Cycle thr3");
      status = db_find_key(hDB, hFS, str, &hFT);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hFT, &fs.hardware.cycle_thr3
				  , sizeof(fs.hardware.cycle_thr3)
				  , MODE_READ, call_back, "cycle threshold3");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }
  
  if (hDC==0)
    {
      /* setup hot link on "Diagnostic channel #" */
      sprintf(str,"Hardware/Diagnostic channel num");
      status = db_find_key(hDB, hFS,str, &hDC);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hDC, &fs.hardware.diagnostic_channel_num
				  , sizeof(fs.hardware.diagnostic_channel_num)
				  , MODE_READ, call_back, "diagnostic channel number");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    }

  if (hSK==0)
    {
      /* setup hot link on "skip ncycles out-of-tol" */
      sprintf(str,"Hardware/skip ncycles out-of-tol");
      status = db_find_key(hDB, hFS, str, &hSK);
      if (status == DB_SUCCESS)    
	{
	  status = db_open_record(hDB, hSK, &fs.hardware.skip_ncycles_out_of_tol
				  , sizeof(fs.hardware.skip_ncycles_out_of_tol)
				  , MODE_READ, call_back, "skip ncycles out-of-tol");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }



  if (hFF==0)
    {
      /* setup hot link on "flags/hold" */
    sprintf(str,"flags/hold");
      status = db_find_key(hDB, hFS, str, &hFF);
      if (status == DB_SUCCESS)  
	{  
	status = db_open_record(hDB, hFF, &fs.flags.hold, sizeof(fs.flags.hold)
				, MODE_READ, call_back, "hold flag");
	if (status != DB_SUCCESS) 
	  cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    }




  if (hPd==0)
    {
      /* setup hot link on "flag_bad_scan"  */
      sprintf(str, "Hardware/flag_bad_scan");
      status = db_find_key(hDB, hFS,str, &hPd);
      if (status == DB_SUCCESS)  
	{ 
	  status = db_open_record(hDB, hPd, &fs.hardware.flag_bad_scan
				  , sizeof(fs.hardware.flag_bad_scan)
				  , MODE_READ,  hot_bad_scan, "flag_bad_scan");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\"  failed (%d)",str,status);
    } 


  if (hR1==0)
    {
      sprintf(str, "Hardware/Re-ref_P+");
      status = db_find_key(hDB, hFS,str, &hR1);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hR1, &lhot_reref1
				  , sizeof(hot_reref1)
				  , MODE_READ, call_back, "reref P+");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

  if (hR2==0)
    {
     sprintf(str, "Hardware/Re-ref_Laser");
      status = db_find_key(hDB, hFS,str, &hR2);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hR2, &lhot_reref2
				  , sizeof(hot_reref2)
				  , MODE_READ, call_back, "reref Laser");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }

  if (hR3==0)
    {
      sprintf(str, "Hardware/Re-ref_Fcup");
      status = db_find_key(hDB, hFS, str, &hR3);
      if (status == DB_SUCCESS)
	{
	  status = db_open_record(hDB, hR3, &lhot_reref3
				  , sizeof(hot_reref3)
				  , MODE_READ, call_back, "reref Fcup");
	  if (status != DB_SUCCESS) 
	    cm_msg(MINFO,"setup_hotlinks","open record on \"%s\"  failed (%d)",str,status);
	}
      else
	cm_msg(MINFO,"setup_hotlinks","find key on \"%s\" failed (%d)",str,status);
    }
  
  return;
}


/* ----------------------------------------------------- */
void clear_scan_flag(void)
/* ----------------------------------------------------- */
{
  INT status;
  
  /* clear pol's bad scan flag 
     ( may be set by "bad scan" button) at beginning of each scan 

     bad scan flag must be cleared at begin-of-scan */ 
  bad_scan_flag = 0;
  status = db_set_value(hDB, hBsf, "flag bad scan", &bad_scan_flag, size_bsf, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    cm_msg(MERROR, "clear_scan_flag", "cannot clear \"hardware/flag bad scan\" at begin of cycle (%d)",status);		  return;
}






#ifdef EPICS_ACCESS

/*-----------------------------------------------------------------------*/
INT set_epics_incr(void)
/*-----------------------------------------------------------------------*/
{
  /* set an epics value
     retry on failure
     stop the run after n retries
  */
  INT status;
  INT num_retries=10;
  BOOL watchdog_flag;
  DWORD watchdog_timeout;

  epics_params.Epics_bad_flag = 0;
  /* retrying can take some time so set watchdog for 5 min */
  /*  cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  /* 5 min for reconnect 5*60*1000 */
  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */
  status = EpicsIncr( &epics_params );
  if(status == SUCCESS)
    {
      restore_watchdog(); /* restore watchdog timeout */
      /*      cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
      epics_params.Epics_bad_flag = 0;
      return (status);
    }
  else
    {
      if(status == -1 )
	{
	  /* serious error from reading/writing Epics device 
	     try to reconnect  */
	  status = EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      gbl_epics_live=FALSE; /* cannot maintain Epics live */
	      printf("\n  *** set_epics_incr: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     fs.output.e1n_epics_device);
	      printf("set_epics_incr: calling set_client_flag with FAILURE\n");
	      set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	      restore_watchdog(); /* restore watchdog timeout */
	      /* cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      return(FE_ERR_HW);
	    }
	}
      epics_params.Epics_bad_flag++;
    }
  printf("set_epics_incr: Waiting 0.5s ..... then rechecking Epics voltage \n");
  ss_sleep(500);
  cm_yield(100);
  while(epics_params.Epics_bad_flag < num_retries) 
    {
      status = EpicsCheck( &epics_params);  /* Resends the set point */
      if(status==SUCCESS)
	{
	  if(d5)printf("set_epics_incr: success from  EpicsCheck after %d failures\n",epics_params.Epics_bad_flag);
	  epics_params.Epics_bad_flag = 0;
	  restore_watchdog(); /* restore watchdog timeout */
	  /* cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return SUCCESS;
	}	
      else if (status == -1)
	{
	  cm_msg(MERROR, "set_epics_incr","Error accessing Epics device");
	  printf("set_epics_incr: calling set_client_flag with FAILURE\n");
	  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	  return FE_ERR_HW;
	}
      else if (status == -2)
	{ /* this should be picked up in earlier checks */
	  cm_msg(MERROR, "set_epics_incr","Invalid set value outside min/max range ");
	  printf("set_epics_incr: calling set_client_flag with FAILURE\n");
	  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	     restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return FE_ERR_HW;
	}

      epics_params.Epics_bad_flag++; /*   increment epics_params.Epics_bad_flag */
      printf("set_epics_incr: waiting 3s...... then retrying (attempt %d)\n",
	     epics_params.Epics_bad_flag);
      iwait(3);
    } /* while ends */
  
  /* cannot set Epics voltage */
     restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  printf("\n  *** set_epics_incr: Given up trying to set %s. Stop & restart run after checking Epics device *** \n",
	 fs.output.e1n_epics_device);

  printf("set_epics_incr: calling set_client_flag with FAILURE\n");
  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
  return(FE_ERR_HW);
}

/*-----------------------------------------------------------------------*/
INT set_epics_val(void)
/*-----------------------------------------------------------------------*/
{
  INT status;
  BOOL watchdog_flag;
  DWORD watchdog_timeout;
  INT ncounts;

  /* Set the Epics Scan device to the value  epics_params.Epics_val
     Usually called at the beginning of a Scan  */

  epics_params.Epics_bad_flag = 0;
     
  /* EpicsNewScan involves  waiting so set watchdog for 3 min */
  /*  cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  /* 5 min for reconnect 5*60*1000 */
  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */

  ncounts=0;
  while (ncounts < 10)
    {
      /* Note: EpicsNewScan sets Epics device, reads it back and checks correct value is set.

	   - it is called EpicsNewScan because it is called at the beginning of a scan to set a value (unless
           helicity is flipped -> scan direction reversed).
           At the beginning of a scan Epics device  will be set to 1 increment different from start value
           to give device time to settle (there may be big change in value). Then EpicsIncr will be called to set
           device to correct value.

      */

      status = EpicsNewScan( &epics_params ); /* set Epics value to  epics_params.Epics_val */
					      
      if(status == SUCCESS) 
	{
	  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return status;
	}
      if(status == -1)
	{
	  /* hardware error */ 
	  cm_msg(MERROR,"set_epics_val","trying to reconnect to Epics \n");
	  status=EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      gbl_epics_live=FALSE; /* cannot maintain Epics live */
	      printf("\n  *** set_epics_val: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     fs.output.e1n_epics_device);
	      restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      printf("set_epics_val: calling set_client_flag with FAILURE\n");
	      set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	      return(FE_ERR_HW);
	    }
	  
	}
      else if (status == -2)
	{
	  cm_msg(MERROR,"set_epics_val","Epics step parameters are outside max or min value allowed");
	  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  printf("set_epics_val: calling set_client_flag with FAILURE\n");
	  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	  return (FE_ERR_HW);
	}
      else if (status == DB_NO_ACCESS)
	{
	  cm_msg(MERROR,"set_epics_val","no access to scan device. Check device is available and switched on");
	  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  printf("set_epics_val: calling set_client_flag with FAILURE\n");
	  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	  return(FE_ERR_HW);
	}
      
      printf("set_epics_val:  waiting 10s then retrying.... \n");
      iwait(10); /* wait 10s */
      ncounts++;
    }

  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  printf("set_epics_val:  Epics Hardware is not responding after %d retries ... make sure device is switched on \n",ncounts);
  printf("set_epics_val: calling set_client_flag with FAILURE\n");
  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
  return (FE_ERR_HW);
}

/*-------------------------------------------------------------------------*/
INT read_epics_val(float *pval )
/*-------------------------------------------------------------------------*/
{
  INT status;
  BOOL watchdog_flag;
  DWORD watchdog_timeout;
  INT ncounts;

  /* Read the Epics Scan device into the value  epics_params.Epics_read */

  epics_params.Epics_bad_flag = 0;

  /* EpicsRead may involve waiting if we have to reconnect so set watchdog for 5 min */
  /*  cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  /* 5 min for reconnect 5*60*1000 */
  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */
  ncounts=0;

  while (ncounts < 10)
    {
      status = EpicsRead( pval, &epics_params ); /* read value from Epics */
      if(status == SUCCESS) 
	{
	  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	  return status;
	}
      
  
      if(status == -1)
	{
	  /* no connection or timeout */ 
	  cm_msg(MERROR,"read_epics_val","trying to reconnect to Epics \n");
	  status=EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      gbl_epics_live=FALSE; /* cannot maintain Epics live */
	      printf("\n  *** read_epics_val: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     fs.output.e1n_epics_device);
	      restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
	      printf("read_epics_val: calling set_client_flag with FAILURE\n");
	      set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
	      return(FE_ERR_HW);
	    }
	  
	}
      
      printf("read_epics_val:  waiting 10s then retrying.... \n");
      iwait(10); /* wait 10s */
      ncounts++;
    }

  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  printf("read_epics_val: Epics Hardware is not responding after %d retries ... make sure device is switched on \n",ncounts);
  printf("read_epics_val: calling set_client_flag with FAILURE\n");
  set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
  return (FE_ERR_HW);
}


/*----------------------------------------------------------------------------*/
INT epics_reconnect(BOOL hel)
/*----------------------------------------------------------------------------*/
{ /* Input parameter hel
       hel = FALSE reconnects the Epics scan devices 
             TRUE  reconnects Epics helicity read  

   set long watchdog before calling EpicsReconnect */
  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */
  INT status;
  
  printf("epics_reconnect starting with parameter=%d (TRUE for helicity)\n",hel);
  /* EpicsReconnect may involve waiting so set watchdog for 5 min */
  /*  cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);  /* 5 min for reconnect 5*60*1000 */
  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */
  if(hel)
    { /* reconnect helicity (or bias for POL ) */
      printf("calling ChannelReconnect to %s\n",Rname);
      Rchid=-1; /* clear old chid */
      Rchid = ChannelReconnect(Rname); /* reconnect */
     if(Rchid != -1)
       status = SUCCESS;
    }
  else
    {
      printf("calling EpicsReconnect\n");
      status=EpicsReconnect(&epics_params); /* reconnect the scan device */
      printf("after Reconntect, status = %d\n",status);
    }
  restore_watchdog(); /* restore watchdog timeout */
  /*cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  if(status != SUCCESS)
   {
     if(hel)
       cm_msg(MERROR,"epics_reconnect","cannot connect to Epics device %s",Rname);
     else
       cm_msg(MERROR,"epics_reconnect", "cannot connect to Epics device %s",
	      fs.output.e1n_epics_device);
     
     printf("epics_reconnect: calling set_client_flag with FAILURE\n");
     set_client_flag("frontend",FAILURE); /* mdarc should stop the run (hot link on client flag).*/
     return(FE_ERR_HW);
   }
  else    
    return SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT  epics_watchdog(EPICS_PARAMS *p_epics_params, BOOL hel)
/*----------------------------------------------------------------------------*/
{
  /* Set epics watchdog to a long time, then call EpicsWatchdog (if scan) 
           otherwise ChannelWatchdog 
  */
  
  BOOL watchdog_flag;
  DWORD watchdog_timeout;
  INT status;
  
   
  /* EpicsWatchdog will try to reconnect if channels have disconnected */
 
  if (hel)
    { /* helicity (Bias for POL) */
      if(Rchid == -1)
	{
	  printf("epics_watchdog: Epics channel %s has disconnected\n",Rname);
	  return -1;
	}
    }
  else
    {
      if(epics_params.XxWchid == -1 || epics_params.XxRchid == -1)
	{
	  printf("epics_watchdog: Epics channel(s) have disconnected\n");
	  return -1;
	}
    }
 
  /* Either read the Epics Scan device into the value  epics_params.Epics_read
     or read the helicity (bias for POL) if hel is true.
     
     May involve waiting if we have to reconnect so set watchdog for 5 min */

  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */
  if(hel)
    status = ChannelWatchdog(Rname, &Rchid); /* helicity (Bias for POL) */
  else
    status =  EpicsWatchdog( p_epics_params );

  if(status != SUCCESS)printf("Error detected from watchdog (%d)\n",status);

  restore_watchdog(); /* restore watchdog timeout */
  return status;
 
}
#endif /* Epics */




/*--------------------------------------------------*/
INT get_int_version(char *p, int len)
/*--------------------------------------------------*/
{
  int i;
  char *q;
  char digits[6]="";

  q=&digits[0];
  for (i=0; i<len; i++)
    {
      if( isdigit(*p))
	{
	  strncpy(q,p,1);
	  q++;
	}
      p++;
    }
  printf("%s\n",digits);
  return(atoi(digits));
}


INT clear_alarm(INT alarm_num)
{
  INT status;
  /* al_reset_alarm returns with status
     AL_INVALID_NAME         Alarm name not defined
     AL_RESET                Alarm was triggered and reset
     AL_SUCCESS              Successful completion
  */
   
  switch (alarm_num)
    {
    case 1:
      {
	status = al_reset_alarm("thr1 trip");
	break;
      }
    case 2:
      {	status = al_reset_alarm("thr2 trip");
      break;
      }
    case 3:
      {	status = al_reset_alarm("thr3 trip");
      break;
      }
    case 4:
      { 	status = al_reset_alarm("RF trip");
      break;
      }
    case 11:
      { /* shut off all thresh alarms (at end-of-run) */
	status = al_reset_alarm("thr1 trip");
	status = al_reset_alarm("thr2 trip");
	status = al_reset_alarm("thr3 trip");
      }
    default:
      return SUCCESS;
    }
  if(status != SUCCESS)
    printf("clear_alarm: status = %d returned by al_reset_alarm \n",status);
  return status;
}

#include "scan_jump.c"

INT pol_find_keys(void)
{
  INT status;

  status = db_find_key(hDB, 0, "/Equipment/DVM/Variables/Measured", &hVar);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_find_keys","failure finding key for /Equipment/DVM/Variables/Measured (%d)",status);
      return status;
    }

  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC1/Handshake", &hHand);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_find_keys","failure finding key for /Equipment/DVM/Settings/Slot2DAC1/Handshake (%d)",status);
      return status;
    }

  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC1/Demand", &hReq);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_find_keys","failure finding key for /Equipment/DVM/Settings/Slot2DAC1/Demand(%d)",status);
      return status;
    }

  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC2/Handshake", &hHand2);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_find_keys","failure finding key for /Equipment/DVM/Settings/Slot2DAC2/Handshake (%d)",status);
      return status;
    }
  status = db_find_key(hDB, 0, "/Equipment/DVM/Settings/Slot2DAC2/Demand", &hReq2);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_find_keys","failure finding key for /Equipment/DVM/Settings/Slot2DAC2/Demand (%d)",status);
      return status;
    }

  status = db_find_key(hDB, 0, "/Equipment/Wavemeter/Variables/Measured", &hRmon);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_find_keys","failure finding key for /Equipment/Wavemeter/Variables/Measured (%d)",status);
      return status;
    }
  return status;
}

/*-- Monitor Event  ---------------------------------------------------------*/
INT monitor_event(char *pevent, INT off)
{
  /*  periodic equipment reading monitor values for POL
      generates bank  MONI (double)  
      Word 1  bad scan flag (1=bad scan 0= good scan)
      Word 2  HV bias  (-99 = error)
      Word 3  Wavemeter (-99 = error)
*/
  INT    i,h, status, nhb, ihand,  size;
  double *pdata;
  char   bank_name[4];
  float value;

  if (!pol_Rmon_flag)  /* set at end-of-scan */
    return 0;
  
  /* Time to send a monitor event */
  bk_init(pevent);
  bk_create(pevent, "MONI", TID_DOUBLE, &pdata);
  *pdata++ = (double)bad_scan_flag; /* add to event (may be set by experimenter) */

#ifdef EPICS_ACCESS 
  if (fs.hardware.disable_epics_checks)
    {  /* EPICS is enabled, but this particular read is disabled */
      *pdata++ = (double) -99; /* -99 means error;  add to event */
    }
  else
    {
      
      /* Read the EPICS Bias value */
      
      Hel_last_time = ss_time(); /* we are about to read Bias... reset watchdog to keep Epics live  */
      value  = -1;
      i=0;
      if(dh)printf("monitor_event: starting with Rchid=%d \n",Rchid);
      set_long_watchdog(300000); /* 5 minutes */
      while(i < 2)
	{
	  status = read_Epics_chan(Rchid,  &value);
	  if(dh)printf("monitor_event: read_Epics_chan returns with status=%d\n",status);
	  if(status == SUCCESS) break;
	  
	  if(dh)printf("monitor_event: calling epics_reconnect for Bias \n");
	  if(!epics_reconnect(TRUE))  /* TRUE for bias. 
					 epics_reconnect sets the Midas watchdog flag.. stops the run on failure */
	    {
	      /* Cannot reconnect to Epics */
	      break; /* status not SUCCESS */
	    }
	  i++;
	}
      restore_watchdog();
      if(status != SUCCESS)
	{ 
	  cm_msg(MERROR,"monitor_event","error reading Bias from Epics");
	  value=-99; /* indicates error */
	  /*return FE_ERR_HW;*/
	} 
      if(dh)printf("monitor_event: after read_Epics_chan bias value = %f, status = %d\n",value,status);
      *pdata++ = (double)value; /* add to event */
    } /* end of epics bias read enabled */

#else /* no EPICS_ACCESS */
  *pdata++ = (double)-99;
#endif

  /* now read the Monitor value  (Wavemeter) */  
  size = sizeof(RmonVal); /* a float */
  status = db_get_data_index(hDB,hRmon,&RmonVal,&size,0,TID_FLOAT);
  if(status != SUCCESS)
    {
      printf("cycle_start: Error getting RmonVal using db_get_data_index (%d) \n",status);
      RmonVal=99.0; /* put some unlikely value to indicate error */
    }

    if(!d11)printf("\nMonitor_event: read Bias = %f; RmonVal (wavemeter) = %f \n",value, RmonVal);

  /* fill the buffer */
    *pdata++ = (double)RmonVal; /* add to event */

    bk_close(pevent, pdata);
    pol_Rmon_flag=FALSE; /* clear flag */
    return bk_size(pevent);
}


void dbug(void)
{
  printf("\nSet the following to 1 to turn on specific debugging:\n");
  printf("dd     debugs are in sis_setup and poll_event\n");
  printf("ddd    frontend_loop, begin_run, debug scalers, histo banks\n");
  printf("dddd   indicates if running in cycle in frontend loop, vmeio\n");
  printf("d3     reference threshold\n");
  printf("d5     Epics scan (higher level ... bnmr_epics.c)\n");
  printf("d6     Epics scan (lower level... EpicsStep.c)\n");
  printf("d7     Epics watchdog,\n");
  printf("d8     display: for debugging, don't overwrite  scan values in cycle_start \n");
  printf("d9     client flag; automatic stop; delayed transition if available\n");
  printf("d10    frequency scan  \n");
  printf("dc     Camp scan (1c and 1j) \n");
  printf("d11    Silent mode (d11=1 for POL); do not display anything \n");
  printf("d12    debug POL's dac scan\n");
  printf("dpsm   Debug PSM; also set pdd=1 for trPSM (BNQR only)\n");
  printf("ddac   Set to 1 if no feDVM client available or hardware off \n");
  printf("             (bypasses check on handshake... POL DAC only)\n");
  printf("dq     Set to 1 for manual ppg start\n");  
  printf("dran   Randomizing frequency values (can also use dr=1 for lower level)\n");
  printf("dh     Helicity (direct Epics channel access)\n");
  printf("dhw     Set to 1 to ignore failed helicity flip & disable helicity warnings(for testing)\n");
  printf("debug_2e debug e2e \n");
}

/*-----------------------------------------------------*/
INT set_long_watchdog(DWORD my_watchdog_timeout)
/*-----------------------------------------------------*/
{
 /* Set a long midas watchdog timer 
    Parameter  my_watchdog_timeout in ms 
  */
  DWORD k9;




  if(gbl_dachshund_flag)
    {  /* check flag */
      cm_get_watchdog_params(&gbl_watchdog_flag, &k9);
      printf("set_long_watchdog: long watchdog is set already, currently watchdog is set to %d\n",k9);
      return SUCCESS;
    }

  cm_set_watchdog_params(gbl_watchdog_flag, my_watchdog_timeout );
   if(dh || d7 ) 
    printf("set_long_watchdog:  gbl_watchdog_timeout=%d; set timeout to %d; \n",
	   gbl_watchdog_timeout, my_watchdog_timeout);
  
  gbl_dachshund_flag = TRUE;
  return SUCCESS;
}
/*-----------------------------------------------------*/
INT restore_watchdog(void)
/*-----------------------------------------------------*/
{
  /* restore the midas watchdog to the original value 
   */
  DWORD my_timeout;

  if(!gbl_dachshund_flag)
    {  /* check flag */
      printf("restore_watchdog: set_long_watchdog has not been called previously\n");
      return SUCCESS;
    }
  /* temp check... */
  cm_get_watchdog_params(&gbl_watchdog_flag, &my_timeout);
    if(dh || d7) 
    printf("restore_watchdog: watchdog value is presently %d; restoring it to %d; gbl_watchdog_flag=%d\n",
	 my_timeout, gbl_watchdog_timeout, gbl_watchdog_flag);
  cm_set_watchdog_params(gbl_watchdog_flag, gbl_watchdog_timeout);  /* e.g. 5 min for reconnect = 5*60*1000ms */
  gbl_dachshund_flag = FALSE;
  return SUCCESS;
}




