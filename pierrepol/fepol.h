/*   febnmr.h
   Common defines for BNMR and POLB types 1 and 2

   $Log: febnmr.h,v $
   Revision 1.12  2006/06/20 22:31:36  suz
   move hInfo key here from bnmr_init.c - i.e. make it global

   Revision 1.11  2006/06/19 17:16:22  suz
   change names to simplify broken input fix for bnqr

   Revision 1.10  2005/07/19 21:28:35  suz
   POL uses its own scalers now; BNQR scalers not sent out

   Revision 1.9  2005/05/17 23:52:34  suz
   add userbit1 histo to one scaler case

   Revision 1.8  2005/03/16 18:45:28  suz
   define PSM_BASE for bnmr as 0xC00000

   Revision 1.7  2005/02/28 21:47:20  suz
   add function prototype

   Revision 1.6  2005/02/16 20:48:12  suz
   add beamline

   Revision 1.5  2005/01/26 17:57:48  suz
   ppgTrigReg added (PPG mod for dual channel mode)

   Revision 1.4  2004/10/25 18:34:02  suz
   add support for PSM

   Revision 1.3  2004/09/09 21:26:05  suz
   add a handle used by POL

   Revision 1.1  2003/05/01 21:18:14  suz
   new version supports all expt types

   Revision 1.1  2003/01/13 18:45:08  suz
   original

*/

/* Function prototypes */
BOOL wait_end_cycle(int transition, BOOL first);
INT  bnmr_init(char  *p_name_fifo, char *p_name_info, char *p_name_scalers);
INT  FIFO_acq(char *pevent, INT off);
INT  diag_read(char * pevent, INT off);
INT  info_odb(char *pevent, INT off);
INT  histo_read(char *pevent, INT off);
INT  histo_process(void);
INT  scalers_cycle_read(char *pevent, INT off);
INT  display_scalers_read(char *pevent, INT off);
INT  scaler_increment(const DWORD nwords,  DWORD * pfifo, INT scaler_offset, 
                      INT maximum_channel,INT *gbl_bin, DWORD *userbit); 
INT  cycle_start(void);
INT  sis_setup(DWORD nchan, float dwell);
INT  helicity_read(void);
void hot_reference_clear(INT i);
void histo_clear(INT h);
void scaler_clear(INT h);
INT check_file_time(void);
void show_scaler_pointers(void);
void die();
void iwait(INT sec);
void ppgStop(void);
INT print_hardware_flags(void);
void update_fix_counter(INT num_increments);
INT get_int_version(char *p, int len);
void setup_hotlinks(void);
INT prepare_to_stop(void);
INT post_end_run(INT run_number, char *error); /* post-stop transition */


INT find_jump(float start, float stop, float inc, DWORD ninc, 
	      float jstart, float jstop, BOOL debug, DWORD *pjinc1, DWORD *pjinc2);

void clear_scan_flag(void);
INT pol_find_keys(void);
INT monitor_event(char *pevent, INT off);

/* febnmr Equipment indexes (constants) */
#define FIFO                     0
#define HISTO                    1
#define CYCLE                    2
#define DIAG                     3
#define INFO                     4
/********************************************
       SIS 3801 MultiChannel SCALERS 
/********************************************

  Note : SIS3801A : It is assumed ALL 32 inputs are ENABLED on Scaler A when
                       Scaler B is present.

         Important: if more than 24 channels are enabled on the SIS3801 all 32
         will be enabled (hardware feature). If MAX_CHAN_SIS3801B is between 24
         and 32 the software (scaler_increment) will not work without modification. 

   Remove the following defines in Makefile for dasdevpc test station: 
	 TWO_SCALERS   is defined in the Makefile for TWO SIS3801 SCALER MODULES 
	 For Type 1 only :
	 EPICS_ACCESS  is defined in the Makefile if direct access to NaCell or LASER is available 
	 undefine if no access and no epics software running on ppc (e.g. if not using bnmrhmvw) 
	 CAMP_ACCESS   is defined in the Makefile for direct access to Camp; (VXWORKS must also be defined in Makefile) 
*/ 

 /* 1 Scaler only -> Scaler B */
 #define MAX_CHAN_SIS3801A  0

 /* POL */
/* POL ... 1 scaler, POL's Real scalers start at channel MAX_CHAN_SIS3801B (12) but we have to read 12 + NUM_POL_CHAN (5)=17 channels */
#define POL_OFFSET 14  /* this equals MAX_CHAN_SIS3801B for BNMR/BNQR + 2 because input 14? is broken 
			 POL channels are fluor,P+ beam, Laser Power, FC15, LFB */
#define NUM_POL_CHAN 6   /* number of POL-specific real channels defined */
#define MAX_CHAN_SIS3801B (POL_OFFSET + NUM_POL_CHAN)   /* number of REAL scaler inputs used by SIS3801B for POL */
/* Note: Scaler Inputs start at 1, offsets go from 0 ) */
#define FLUOR_CHAN2  POL_OFFSET     /* Pol's fluorescence monitor 14  (input 15)  */
#define POL_P_PLUS (POL_OFFSET + 1) /* P+ beam counter 15 (input 16) */
#define POL_LASER (POL_OFFSET + 2)  /* Laser power 16 (input 17) */
#define POL_FC15 (POL_OFFSET + 3)   /* Faraday Cup 17 (input 18) */
#define POL_LFB (POL_OFFSET + 4)    /* Extra channel requested to replace DVM[2] locking feedback (input 19) */
#define POL_XXX (POL_OFFSET + 5)    /* one more channel for POL */

/********************************************
  Define histograms with time bin structure
/********************************************/


/* One scaler only... Scaler B  */


#define N1_HISTO_MAXB             2   /* Type 1 histograms for  B channels  SIS REF, chan XXX for POL */
#define N2_HISTO_MAXB             2   /* Type 2 histograms for Scaler B   */
#define N_HISTO_MAXB              2   /* must be the largest of N1_HISTO_MAXB and N2_HISTO_MAXB  */ 

/***********************************************************************
  Define scalers (totals for one cycle or cumulative since start of run
  ***********************************************************************/

#define N_SCALER_REAL MAX_CHAN_SIS3801B
/*          N_SCALER_REAL scalers listed above */

/*--------------------------------*/
/*    Calculated cycle scalers    */
/*--------------------------------*/


/* none */

#define NB_CYCLE_SCALER  0  /* No. calculated cycle scalers listed above */

#define N1_SCALER_CYC     0 /* Tot. Number of cycle scalers defined above */

#define N2_SCALER_CYC    0  /* Tot. Number of cycle scalers defined above */

  
/*--------------------------------*/
/*  Calculated cumulative scalers */
/*    clear only at BOR           */
/* Only scaler A values are used  */
/*--------------------------------*/


/* Possibly cumulative scalers from ScalerB will be added later */
#define NB_SCALER_CUM             0
#define NA_SCALER_CUM             0
#define N1_SCALER_CUM             0  /*  TYPE 1 - no cumulative scalers */
#define N2_SCALER_CUM             0  /*  TYPE 2 - */

/*--------------------------------*/
/*  Overall number of scalers     */
/*--------------------------------*/

#define N1_SCALER_TOTAL (N_SCALER_REAL + N1_SCALER_CYC + N1_SCALER_CUM)    /* Maximum scalers for scaler bank,  real + (any) calculated */
#define N2_SCALER_TOTAL (N_SCALER_REAL + N2_SCALER_CYC + N2_SCALER_CUM)    /* Maximum scalers for scaler bank,  real + (any) calculated */
#define N_SCALER_MAX    N2_SCALER_TOTAL  /* Must be the larger of the two */

#define N_HISTO_MAX   N_HISTO_MAXB
#define N_BINS_MAX           32000

/*--------------------------------*/
/* Scalers passed to EPICS        */
/*--------------------------------*/

 /* 1 scaler */
#define N_EPICS_VAR_A  0


/***************************************
  Other quantities and variables 
  **************************************/

#define HEL_DOWN                0         /* helicity down */
#define HEL_UP                  1         /* helicity up */

/* PPG */
#define PPG_BASE            0x8000




/* SIS3801 */

#define SIS3801_BASE_B      0x000000  /* Module B Base Address 0     */


#define IRQ_VECTOR_CIP        0x70
#define IRQ_LEVEL                5
#define DATA_MASK         0xffffff

/* VMEIO */
#define VMEIO_BASE        0x780000
/* outputs */
#define BOB_PULSE              0x1         /* Begin of beam */
#define EOB_PULSE              0x2         /* End of beam */
#define BOC_PULSE              0x4         /* Begin of cycle */
#define EOC_PULSE              0x8         /* End of cycle */
#define BEAM_ON               0x10         /* Beam latch */
#define CYCLE_ON              0x20         /* Cycle latch */
#define DEBUG1_PULSE          0x40      
#define DEBUG2_PULSE          0x80
/* inputs */
#define RF_TRIPPED             0x1         
#define IN_BEAM               0x10         /* input */
#define IN_CYCLE              0x20         /* input */
/* pattern */
#define VMEIO_PULSE_BITS      BOB_PULSE | EOB_PULSE | BOC_PULSE | EOC_PULSE | DEBUG1_PULSE | DEBUG2_PULSE 

/* init ODB structures and strings */
/* pick up as much as possible from experiment.h */


FIFO_ACQ_SIS_MCS fs;
CYCLE_SCALERS_SETTINGS cs;
INFO_ODB_EVENT cyinfo;



#ifdef EPICS_ACCESS
static EPICS_PARAMS epics_params ; /* structure defined in bnmr_epics.h */
#endif

/* these defined in bnmr_init */
BOOL  gbl_IN_CYCLE;
INT  gbl_BIN_A, gbl_BIN_B;  /* current time bin for each module */
INT     gbl_HEL;            /* current helicity state */

DWORD *pfifo_A, *pfifo_B; /* FIFO local buffers */
DWORD   csrdataA,csrdataB;
DWORD   gbl_bin_count;   /* internal cycle # */
INT     exp_mode;        /* =1 for all TYPE1 ; = 2 for others */
char    expt_name[32];
HNDLE hDB, hFS, hEPD, hEPM, hBsf, hInfo; /* these found in bnmr_init */
#ifndef POL
INT   ppgTrigReg; /* read state of PPG External Trigger register (BNMR.BNQR dual channel mode) */
#endif
char beamline[40],BEAMLINE[5]; /* bnmr/bnqr/pol ("experiment_name" is used for ppg_mode) */
/* end of bnmr_init defines */

/* must keep a NAMES structure for type 1 and type 2 of BNMR */

#define CYCLE_SCALERS_TYPE1_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[60] :",\
"[32] Back%BSeg00",\
"[32] Back%BSeg01",\
"[32] Back%BSeg02",\
"[32] Back%BSeg03",\
"[32] Back%BSeg04",\
"[32] Back%BSeg05",\
"[32] Back%BSeg06",\
"[32] Back%BSeg07",\
"[32] Back%BSeg08",\
"[32] Back%BSeg09",\
"[32] Back%BSeg10",\
"[32] Back%BSeg11",\
"[32] Back%BSeg12",\
"[32] Back%BSeg13",\
"[32] Back%BSeg14",\
"[32] Back%BSeg15",\
"[32] Front%FSeg00",\
"[32] Front%FSeg01",\
"[32] Front%FSeg02",\
"[32] Front%FSeg03",\
"[32] Front%FSeg04",\
"[32] Front%FSeg05",\
"[32] Front%FSeg06",\
"[32] Front%FSeg07",\
"[32] Front%FSeg08",\
"[32] Front%FSeg09",\
"[32] Front%FSeg10",\
"[32] Front%FSeg11",\
"[32] Front%FSeg12",\
"[32] Front%FSeg13",\
"[32] Front%FSeg14",\
"[32] Front%FSeg15",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Userbit=0",\
"[32] General%Back Userbit=1",\
"[32] General%Back Userbit=2",\
"[32] General%Back Userbit=3",\
"[32] General%Front Userbit=0",\
"[32] General%Front Userbit=1",\
"[32] General%Front Userbit=2",\
"[32] General%Front Userbit=3",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"",\
NULL }

#define CYCLE_SCALERS_TYPE2_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[60] :",\
"[32] Back%BSeg00",\
"[32] Back%BSeg01",\
"[32] Back%BSeg02",\
"[32] Back%BSeg03",\
"[32] Back%BSeg04",\
"[32] Back%BSeg05",\
"[32] Back%BSeg06",\
"[32] Back%BSeg07",\
"[32] Back%BSeg08",\
"[32] Back%BSeg09",\
"[32] Back%BSeg10",\
"[32] Back%BSeg11",\
"[32] Back%BSeg12",\
"[32] Back%BSeg13",\
"[32] Back%BSeg14",\
"[32] Back%BSeg15",\
"[32] Front%FSeg00",\
"[32] Front%FSeg01",\
"[32] Front%FSeg02",\
"[32] Front%FSeg03",\
"[32] Front%FSeg04",\
"[32] Front%FSeg05",\
"[32] Front%FSeg06",\
"[32] Front%FSeg07",\
"[32] Front%FSeg08",\
"[32] Front%FSeg09",\
"[32] Front%FSeg10",\
"[32] Front%FSeg11",\
"[32] Front%FSeg12",\
"[32] Front%FSeg13",\
"[32] Front%FSeg14",\
"[32] Front%FSeg15",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon 2",\
"[32] Scaler_B%Polariz Left",\
"[32] Scaler_B%Polariz Right",\
"[32] Scaler_B%Neutral Beam B1",\
"[32] Scaler_B%Neutral Beam B2",\
"[32] Scaler_B%Neutral Beam B3",\
"[32] Scaler_B%Neutral Beam B4",\
"[32] Scaler_B%Neutral Beam F1",\
"[32] Scaler_B%Neutral Beam F2",\
"[32] Scaler_B%Neutral Beam F3",\
"[32] Scaler_B%Neutral Beam F4",\
"[32] General%Back Cycle Sum",\
"[32] General%Front Cycle Sum",\
"[32] General%B/F Cycle",\
"[32] General%Asym Cycle",\
"[32] General%Pol Cycle Sum",\
"[32] General%Pol Cycle Asym",\
"[32] General%NeutBm Cycle Sum",\
"[32] General%NeutBm Cycle Asym",\
"[32] General%Back Cumul +",\
"[32] General%Front Cumul +",\
"[32] General%B/F Cumul +",\
"[32] General%Asym Cumul +",\
"[32] General%Back Cumul -",\
"[32] General%Front Cumul -",\
"[32] General%B/F Cumul -",\
"[32] General%Asym Cumul -",\
"",\
NULL }




#ifdef POL
#define CYCLE_SCALERS_POL_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[6] :",\
"[32] Scaler_B%SIS Ref pulse",\
"[32] Scaler_B%Fluor. mon",\
"[32] Scaler_B%P+ beam",\
"[32] Scaler_B%Laser power",\
"[32] Scaler_B%Faraday Cup 15",\
"[32] Scaler_B%Locking Feedback",\
"",\
NULL }
#endif
