/* pol_init

CVS log information:
$Log: pol_init.c,v $
Revision 1.14  2006/06/20 22:30:20  suz
get current settings of cyinfo, and move hInfo key to febnmr.h

Revision 1.13  2005/05/17 23:51:46  suz
remove a test

Revision 1.12  2005/02/28 21:48:49  suz
change str to str_set so POL works

Revision 1.11  2005/02/16 20:59:57  suz
get the Beamline from odb; opening deferred transition code moved here (not working on isdaq01 so enclosed in ifdef DEFERRED)

Revision 1.10  2005/02/04 22:19:55  suz
allow to continue with no PPG (for testing)

Revision 1.9  2005/02/03 22:44:06  suz
initialize ppg to have VME control helicity, disable ext trig

Revision 1.8  2005/01/26 21:53:31  suz
detect missing PPG to avoid annoyance

Revision 1.7  2005/01/26 17:54:41  suz
ppgDisable renamed ppgStopSequencer (PPG mod for dual channel)

Revision 1.6  2004/10/25 19:19:17  suz
add error return

Revision 1.5  2004/09/09 21:28:45  suz
add db_find_key for pol

Revision 1.4  2004/05/20 18:43:39  suz
add ifdef BNMR for POL

Revision 1.3  2003/12/01 18:58:47  suz
fix bug & remove message: cannot stop run

Revision 1.2  2003/07/29 19:12:04  suz
add call to set_client_flag on failure

Revision 1.1  2003/05/01 21:17:00  suz
new version supports all expt types

Revision 1.1  2003/01/13 18:44:36  suz
original

Revision 1.2  2003/01/08 18:52:57  suz
sis ref ch1 now for scaler A or B


*/
/* Renee P.  The following was defined when bnmr_init.c was compiled
             as a standalone routine.  I do not think this is working
             properly. For debugging, I am going to include this file
             from febnmr.c itself. Then I do not need the following definitions\
*/
#include <stdio.h>
#include <math.h>


#include "midas.h"
#include "sis3801.h"
#include "vmeio.h"
#include "experim.h"
#include "../ppg/trPPG.h"

#include "bnmr_epics.h"
#include "fepol.h"

INT     ddi=1;                     /* debug (definition of scaler channels) */

/* prototype */
void isr_CIP(void);
void show_scaler_pointers(void);
INT pol_init (char *p_name_fifo, char *p_name_info, char *p_name_scalers);


INT pol_init(char  *p_name_fifo, char *p_name_info, char *p_name_scalers )
{

FIFO_ACQ_SIS_MCS_STR(fifo_acq_sis_mcs_str);
CYCLE_SCALERS_SETTINGS_STR(cycle_scalers_settings_str);
INFO_ODB_EVENT_STR(info_odb_event_str);

 INT    status, rstate, size;
 char   str_set[256];
 INT  i,len;
 DWORD dchan;


  gbl_IN_CYCLE = FALSE;
  gbl_BIN_A = 0;
  gbl_BIN_B = 0;
  gbl_HEL = HEL_DOWN;


  printf("\n");

  /* get the experiment name */
  size = sizeof(expt_name);
  status = db_get_value(hDB, 0, "/experiment/Name", &expt_name, &size, TID_STRING, FALSE);

  printf("Front End code for experiment %s now running ... \n",expt_name);
  
  if(ddi)printf("pol_init starting...\n");
  
  /* get basic handle for experiment ODB */
  status=cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS)
    {
    cm_msg(MERROR,"pol_init","Not connected to experiment");
    return CM_UNDEF_EXP;
  }
 
  /* register for deferred transition */ 
  /* this technique was tried in summer 2000 and failed; it is not
     used for the moment */
  /*-PAA-
    status = cm_register_deferred_transition(TR_STOP, wait_end_cycle);
    if(status != CM_SUCCESS)
    cm_msg(MERROR,"pol_init","can't register deferred transition on TR_STOP");
    status = cm_register_deferred_transition(TR_PAUSE, wait_end_cycle);
    if(status != CM_SUCCESS)
    cm_msg(MERROR,"pol_init","can't register deferred transition on TR_PAUSE");
  */

  /* tried again Jan 2005 with Midas 1.9.5. Works on dasdevpc, not on isdaq01 */
#ifdef DEFERRED
  sprintf(str_set,"%s",cm_get_version());
  if(get_int_version(str_set, strlen(str_set)) >= 195)
    {
      /* deferred stop not working except on dasdevpc (new Midas) */
      printf("pol_init: Midas Version %s, registering to a deferred transition \n",str_set);
      status= cm_register_deferred_transition(TR_STOP,  wait_end_cycle);
      if (status == CM_SUCCESS)
	printf("pol_init: successfully registered to deferred transition at TR_STOP\n");
      else
	{
	  cm_msg(MERROR,"pol_init","failure registering to deferred transition (%d)",status);
	  return status;
	}
    }
  else
    printf("pol_init: Midas Version %s, NOT registering to a deferred transition \n",str_set);
#endif  




  /* check for current run state; if not stopped, stop it */
  size = sizeof(rstate);
  status = db_get_value(hDB, 0, "/runinfo/State", &rstate, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"pol_init","cannot GET /runinfo/State");
    return FE_ERR_ODB;
  }
  if (rstate != STATE_STOPPED)
  {
    /* see if we can get runlog to stop the run via the hotlink to frontend client flag */
    status = set_client_flag("frontend",0); /* set client flag to failure; mdarc if running should detect it */

    cm_msg(MINFO,"pol_init",
	   "!!! You must stop the run immediately (if mdarc has not stopped it)");
    cm_msg(MINFO,"pol_init",
	   "!!! You must shutdown the frontend (ppc) program");
    cm_msg(MINFO,"pol_init",
	   "!!! You must then reboot the frontend (ppc)");
    printf("\n !!! Run In Progress: Stop the run, shutdown and reboot frontend !!!\n");
    return (FE_ERR_HW);
}

  /* create record /Equipment/INFO ODB/Variables to make sure it exists  */
  sprintf(str_set,"/Equipment/%s/Variables",p_name_info);
  status = db_find_key(hDB, 0, str_set, &hInfo);
  if (status != DB_SUCCESS)  
    {
      printf( "pol_init: Key %s not found\n",str_set);
      status = db_create_record(hDB, 0, str_set, strcomb(info_odb_event_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"pol_init","db_create_record fails %s status(%d)",str_set,status);
	  return status;
	}
      /* now get the key  */
      status = db_find_key(hDB, 0, str_set, &hInfo);
      if (status != DB_SUCCESS)  
	{
	  cm_msg(MERROR, "pol_init", "Key %s not found",str_set);
	  return status;
	}
    }
  else
    {    /* got the key - check the record size */
      status = db_get_record_size(hDB, hInfo, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "pol_init", "error during get_record_size (%d)",status);
	  return status;
	}
      printf("Size of info saved structure: %d, size of info record: %d\n", sizeof(INFO_ODB_EVENT) ,size);
      if (sizeof(INFO_ODB_EVENT) != size) 
	{
	  status = db_create_record(hDB, 0, str_set, strcomb(info_odb_event_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"pol_init","db_create_record fails %s status(%d)",str_set,status);
	      return FE_ERR_ODB;
	    }
	  else
	    cm_msg(MERROR,"pol_init","INFO ODB had to be recreated!");
	}
    }
    /* Get current  settings */
  size = sizeof(cyinfo);
  status = db_get_record(hDB, hInfo, &cyinfo, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "pol_init", "cannot retrieve %s record (size of cyinfo=%d)", str_set,size);
    return DB_NO_ACCESS;
  }


  
  /* create record "/Equipment/FIFO_acq/sis mcs" to make sure it exists  */
  sprintf(str_set,"/Equipment/%s/sis mcs",p_name_fifo); 
  
  /* find the key for sis mcs */
  status = db_find_key(hDB, 0, str_set, &hFS);
  if (status != DB_SUCCESS)
    {
      printf( "pol_init: Cannot find key %s ",str_set);
      /* So try to create record */
      
      status = db_create_record(hDB, 0, str_set, strcomb(fifo_acq_sis_mcs_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"pol_init","db_create_record fails %s (status %d)",str_set,status);
	  return status;
	}
      /* Find the key  */
      status = db_find_key(hDB, 0, str_set, &hFS);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "pol_init", "Cannot find key %s ",str_set);
	  return DB_NO_KEY;
	}

    }
  else /* got a key */    
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hFS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "pol_init", "error during get_record_size (%d)",status);
	  return status;
	}
      printf("Size of sis saved structure: %d, size of sis record: %d\n", sizeof(FIFO_ACQ_SIS_MCS) ,size);
      if (sizeof(FIFO_ACQ_SIS_MCS) != size) 
	{
	  /* create record */
	  cm_msg(MINFO,"pol_init","creating record (sis mcs); mismatch between size of structure (%d) & record size (%d)", sizeof(FIFO_ACQ_SIS_MCS) ,size); 
	  status = db_create_record(hDB, 0, str_set , strcomb(fifo_acq_sis_mcs_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"pol_init","Could not create sis mcs record (%d)",status);
	      return status;
	    }
	  else
	    printf("Success from create record for %s\n",str_set);
	}
    }


  /* Get current  settings */
  size = sizeof(fs);
  status = db_get_record(hDB, hFS, &fs, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "pol_init", "cannot retrieve %s record (size of fs=%d)", str_set,size);
    return DB_NO_ACCESS;
  }
  /* Setup names in cycle_scaler/setting equipment */
  sprintf(str_set,"/Equipment/%s/Settings",p_name_scalers);
  
  /* create record /Equipment/cycle_scalers to make sure it exists  */
   status = db_create_record(hDB, 0, str_set, strcomb(cycle_scalers_settings_str));
   if (status != DB_SUCCESS)
     printf("pol_init: db_create_record fails %s with status(%d) \n",str_set,status);

  
  /* allocate SIS FIFO memory */
  pfifo_B = malloc(SIS_FIFO_SIZE); /* SIS hardware parameter 64kbytes */
  if (pfifo_B == NULL)

  {
    cm_msg(MERROR,"pol_init","FIFO memory allocation failed");
    return SS_NO_MEMORY;
  }
  else 
  {
     printf("malloc SIS readout buffers: pfifo_A ptr %p, pfifo_B %p\n",pfifo_A,pfifo_B);
  }

 /* Get EPICS odb keys for fe_epics */
  status = db_find_key(hDB, 0, "/equipment/epics/variables/demand", &hEPD);
  if (status != DB_SUCCESS) hEPD = 0;
  status = db_find_key(hDB, 0, "/equipment/epics/variables/measured", &hEPM);
  if (status != DB_SUCCESS) hEPM = 0;
  
  sprintf(str_set,"/Equipment/%s/Pol params/",p_name_fifo); 
  status = db_find_key(hDB, 0, str_set,  &hBsf);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "pol_init", "Cannot find key %s (%d)",str_set,status);
      return DB_NO_KEY;
    }


#ifdef VMEIO_USE
  /* Init VMEIO */
  vmeio_pulse_set(VMEIO_BASE, VMEIO_PULSE_BITS);    

  /* Reset external hardware to cycle off */ 
  vmeio_pulse_write(VMEIO_BASE, EOC_PULSE); /* end of cycle */
  vmeio_latch_write(VMEIO_BASE, 0);
#endif

#ifdef VXWORKS   
  /* Init SIS3801 - this will check that the module is there
     Select the particular setting with the mode parameter */

  sis3801_module_reset(SIS3801_BASE_B);
  sis3801_channel_enable(SIS3801_BASE_B,MAX_CHAN_SIS3801B); /* # channels enabled */

  printf("Number of enabled channels:  Module A = %d, Module B = %d\n",MAX_CHAN_SIS3801A,MAX_CHAN_SIS3801B );
  


  if( fs.hardware.enable_sis_ref_ch1_scaler_b)
    {
      printf("Ref ch1 is enabled for Scaler B\n");
      sis3801_ref1(SIS3801_BASE_B, ENABLE_REF_CH1);
    }
  else
    {
      printf("Ref ch1 is disabled for Scaler B\n");
      sis3801_ref1(SIS3801_BASE_B, DISABLE_REF_CH1);
    }
  

  if(fs.sis_test_mode.test_mode)
  {
    
    printf("Selecting SIS hardware TEST mode \n");


    sis3801_input_mode(SIS3801_BASE_B, 0);            /* SIS input mode 0 */
    sis3801_dwell_time(SIS3801_BASE_B, 1000000);      /* 1s */
    sis3801_CSR_write(SIS3801_BASE_B, DISABLE_EXTERN_NEXT);
    sis3801_CSR_write(SIS3801_BASE_B, DISABLE_EXTERN_DISABLE);
    sis3801_CSR_write(SIS3801_BASE_B, ENABLE_102LNE);
    sis3801_CSR_write(SIS3801_BASE_B, ENABLE_LNE);
    sis3801_CSR_write(SIS3801_BASE_B, ENABLE_TEST);  /* enable test bit */
    sis3801_CSR_write(SIS3801_BASE_B, ENABLE_25MHZ); /* enable 25MHZ test pulser */
  }  
  else
  {
    printf("Selecting SIS hardware REAL mode \n");


    sis3801_input_mode(SIS3801_BASE_B, 2);            /* SIS input mode 2 */
    sis3801_dwell_time(SIS3801_BASE_B, 1000000);      /* 1s */
    sis3801_CSR_write(SIS3801_BASE_B, ENABLE_EXTERN_NEXT);
    sis3801_CSR_write(SIS3801_BASE_B,  ENABLE_EXTERN_DISABLE);
    sis3801_CSR_write(SIS3801_BASE_B, DISABLE_102LNE);
    sis3801_CSR_write(SIS3801_BASE_B, DISABLE_LNE);
    sis3801_CSR_write(SIS3801_BASE_B, DISABLE_TEST);  /* disable test bit */
    sis3801_CSR_write(SIS3801_BASE_B, DISABLE_25MHZ); /* disable 25MHZ test pulser */
  }

  printf("\nSIS3801 module B :               Base Address = 0x%x\n",SIS3801_BASE_B);
  SIS3801_CSR_read(SIS3801_BASE_B);

  if (ddi) printf("# of active ch.  : %d\n",N_SCALER_REAL);

  /* Setup SIS3801 IRQ for CIP source (0) */
  /* one scaler only : interrupt from Scaler B */
  sis3801_int_attach (SIS3801_BASE_B, IRQ_VECTOR_CIP, IRQ_LEVEL, isr_CIP);


  /* Check the PPG is in the crate */
  status =  ppgBeamCtlRegRead(PPG_BASE);
  if (status == 0xFF)  /* reads 0xFF if no module */
    {
      cm_msg(MERROR,"pol_init","There is no PPG in the crate (or PPG is not responding). Cannot run");
      printf("\n Exit from routine pol_init. PPC is DEFINITELY NOT READY to run \n");
      return FE_ERR_HW;
    }

  /*Disable the PPG module just in case */
  ppgStopSequencer(PPG_BASE);
  ppgBeamCtlPPG(PPG_BASE); /* PPG controls the beam */
  ppgPolzCtlVME(PPG_BASE); /* VME controls Helicity (DRV POL) */
  ppgDisableExtTrig(PPG_BASE); /* default.. external trigger input disabled */
#endif
  if(ddi)show_scaler_pointers();


  /* Get the experiment name from odb  */
  size = sizeof(beamline); 
  sprintf(str_set,"/Experiment/Name"); 
  status = db_get_value(hDB, 0, str_set, beamline, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"pol_init","cannot get beamline from \"%s\" (%d)",str_set,status);
      return status;
    }
  len=strlen(beamline);
  strncpy(BEAMLINE,beamline,len);
  /*BEAMLINE[4]=beamline[4]='\0'; /* terminate */
  for  (i=0; i< len ; i++)
    { 
      BEAMLINE[i] = toupper (BEAMLINE[i]); 
      beamline[i] = tolower (beamline[i]);
    }




  printf("Success from pol_init for beamline %s\n",beamline);
  
  return FE_SUCCESS;
}

/* -------------------------------------------------------------------*/

void isr_CIP(void)

/* -------------------------------------------------------------------*/

/*
  Interrupt service routine for CIP (Copy In Progress)
  Takes about 4us +2us jitter.

  Note: CIP signal is used to count the bins (decrements)

*/
{
#ifdef VXWORKS
  sysIntDisable(IRQ_LEVEL);
  csrdataB = sis3801_CSR_read(SIS3801_BASE_B, CSR_FULL);
  sis3801_int_source_disable(SIS3801_BASE_B, SOURCE_CIP);
#endif


  if (gbl_bin_count <= 0)
  {
    /* end of cycle */  

    sis3801_next_logic(SIS3801_BASE_B, DISABLE_NEXT_CLK); /* disable SIS */ 

    /*    vmeio_pulse_write(VMEIO_BASE, EOC_PULSE); *//* send end-of-cycle pulse */
    /* vmeio_latch_write(VMEIO_BASE, 0); */
    /* indirectly by disabling the PPG */
#ifdef VXWORKS
    /*  previously here we did a test between type1 or type2 
	for single/dual channel mode, PPG is no longer free-running for Type 2 
	if(exp_mode == 1) */
    ppgStopSequencer(PPG_BASE); 

#endif
    gbl_IN_CYCLE = FALSE;
  }
  else
  {
    gbl_bin_count--; /* decrement bin count */
    sis3801_int_source_enable(SIS3801_BASE_B, SOURCE_CIP);

#ifdef VXWORKS
    sysIntEnable(IRQ_LEVEL);
#endif
  }

}

/*-----------------------------------------------------------------------------------------------------*/

void show_scaler_pointers()

/* ----------------------------------------------------------------------------------------------------*/
{
  /* show the pointers for the scalers (debug usually) */

  printf("\nREAL scalers indexes  for experiment %s:\n",expt_name);
  printf("Number of real inputs for scaler(s) : %d\n",N_SCALER_REAL);
}





















