#!/usr/bin/perl
#
#  $Log: kill_console.pl,v $
#  Revision 1.1  2004/07/02 22:39:40  suz
#  original: kill ppc/mvme162 windows
#
#
# called by kill-all to kill the MVME162 or PPC console windows
#
use strict;
# input parameter:
#  beamline  bnqr/pol  -> polcon1 
#              bnmr      -> bnmrcon1
#
# e.g.  kill_console.pl bnmr
#
my ($beamline ) = @ARGV;# input parameters
my $process_id;
my $debug=0;
my $name="blank";
my $username="none";
my $killed=0;

if ( $beamline =~ /bnmr/i ) { $name = "bnmrcon1"; }
else { $name = "polcon1"; }
print "Looking for \"$name\" to kill console windows for $beamline...\n";

open PS, "ps -efw |";
while (<PS>) 
{
#    print "$_";
    if(/([a-z]+)[\s]+([\d]+) [\s].*$name.*/) # look for character string (i.e. username) followed by
#                                              spaces, then a space +1 or more digits
#                                              first set of brackets  ->backreference $1 (username)
#                                              second set of brackets ->backreference $2 (pid)
    {
	$username = $1;
	$process_id = $2;
#	print "$_";
	print "found $name running  with PID $process_id and username $username\n" ;
	if( $username eq $beamline)
	{
	    print "killing $process_id\n";
	    open KI," kill $process_id |";
	    while (<KI>) 
	    {
		print "$_";
	    }
	    close (KI);
	    $killed=1;
	}
	else
	{	    
	    # print "Warning: username $username not same as $beamline\n";
	    print "\n *** ERROR  Another user ($username) is using your console ... \n";
	    die " *** that user must kill process $process_id before $beamline can start the console. \n\n";
	}
    }
}
close(PS) ;
unless ($killed)
{
    print "$name not found; console window(s) are not running for $beamline\n";
}
exit;


