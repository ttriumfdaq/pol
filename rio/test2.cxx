
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>

#include "TGalilRIO.h"

// Little test program that sets and reads the analog 
// output and input on Galil 47120. 
int main(int argc, const char * argv[]){

  TGalilRIO *rio = new TGalilRIO("192.168.1.100");
  char REPLY[100];
  char COMMAND[100];
  //  float start= -9.5;
  //  float stop = +9.5;
  // float inc  = 1.0;
  float start,stop,inc;
  float max_DAC;
  float min_DAC;
  float prev_set=0, prev_read=0, diff_set, diff_read;
  int ninc,nloops,dtest;
  float min_diff=25, max_diff=0;
  int diff_cntr=0;
  float max_offset=0;

  float dac_set;
  float value0=0;
  float av,diff;
  char str[80];

  int i,k=1;

  float rio_av;
  float window[4];
  float array[4];

  int range;
 float fval[2];
  char QUERY_A[]="MG _AQ0,_AQ1;";  // Message command
  char QUERY_D[]="MG _DQ0,_DQ1;";  // Message command





  
 



  printf("\n DAC range codes:  1=0-5V 2=0-10V 3=+/-5V 4=+/-10V (default=4)\n");
  printf("Enter range code for DAC chan 0  :");
  scanf("%d",&range);
  sprintf (COMMAND,"DQ 0,%d;",range);
  printf (" RIO set range command sent was \"%s\" \n", COMMAND);
  rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );  
  rio->Command(QUERY_D,&REPLY[0],100 );
    
  printf (" RIO query command was \"%s\" reply: \"%s\"\n",QUERY_D,REPLY);
  sscanf(REPLY, "%f %f ",&fval[0],&fval[1]);

       
      
  printf(" DAC channels 0 and 1 range codes  are %f,%f  \n",fval[0],fval[1]);


	   
  // determine max and min voltage for DAC      
  switch(range)
    {
    case 1:  // 0-5 Volts
      max_DAC=5;
      min_DAC=0;
      break;
    
    case 2:  // 0-10 Volts
      max_DAC=10;
      min_DAC=0;
      break;
    
    case 3:  // -5 to +5 Volts
      max_DAC=5;
      min_DAC=-5;
      break;
    
     default:  // case 4  -10 to +10 Volts
      max_DAC=10;
      min_DAC=-10;
      break;
    }





  //char QUERY[]="MG W[0],W[1],W[2],W[3];";
  char WQUERY[]="MG W[0];";
  // for (k=0; k<4; k++)
  k=0;
    {
      printf("Enter filtering window (volts) for index %d [e.g. 0.1] :",k);
      scanf("%f",&window[k]);
      sprintf(COMMAND,"W[%d]=%f;", k, window[k]);
      printf("Sending COMMAND:%s\n",COMMAND);
      rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );
      printf ("RIO command was: %s\n",COMMAND);
    }

  rio->Command(WQUERY,&REPLY[0], sizeof(REPLY) );
  printf ("RIO command was: %s; reply: %s\n",WQUERY,REPLY);
  //sscanf(REPLY, "%f %f %f %f",&array[0],&array[1],&array[2],&array[3]);
  sscanf(REPLY, "%f",array);
  //for (i=0;i<4; i++)
    printf("read w[%d]=%f\n",k,array[k]);


  char FQUERY[]="MG F[0];";
  // for (k=0; k<4; k++)
  k=0;
    {
      printf("Enter filter for index %d [e.g. 30] :",k);
      scanf("%f",&window[k]);
      sprintf(COMMAND,"F[%d]=%f;", k, window[k]);
      printf("Sending COMMAND:%s\n",COMMAND);
      rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );
      printf ("RIO command was: %s\n",COMMAND);
    }

  rio->Command(FQUERY,&REPLY[0], sizeof(REPLY) );
  printf ("RIO command was: %s; reply: %s\n",FQUERY,REPLY);
  //sscanf(REPLY, "%f %f %f %f",&array[0],&array[1],&array[2],&array[3]);
  sscanf(REPLY, "%f", array);
  //for (i=0;i<4; i++)
    printf("read w[%d]=%f\n",k,array[k]);


  k=1;
  while (k)
    {
      printf("Enter start voltage : ");
      scanf("%f",&start);
      if( (start <= max_DAC) && (start >= min_DAC))
	break;

      printf("Start value must be between %f and %f Volts\n",max_DAC,min_DAC);
    }

  while (k)
    {
      printf("Enter stop voltage : ");
      scanf("%f",&stop);
      if( (stop <=max_DAC) && (stop >= min_DAC))
	break;

      printf("Stop value must be between %f and %f Volts\n",max_DAC,min_DAC);
    }

 while (k)
    {
      printf("Enter Increment in volts : ");
      scanf("%f",&inc);
      if(inc != 0)
	{
	  ninc =  (int) fabs((start-stop)/inc);
	  if(ninc > 1) 
	    break;
	  else
	    printf("There must be at least 1 increment\n");
	}
      else
	printf("Increment cannot be 0 Volts\n");
    }


#ifdef GONE
 while (k)
    {
      printf("Enter D for read/write Step Difference test or A for Average test ");
      scanf("%s",str);
      
      if((strncmp(str,"d",1)==0) || (strncmp(str,"D",1)==0))
	{
	  printf("Enter max offset between read and write differences in volts (e.g. .03) : ");
	  scanf("%f",&max_offset);
	  dtest=1;
	  nloops=1;
	  break;
	}
      else if((strncmp(str,"a",1)==0) || (strncmp(str,"A",1)==0))
	{
	  dtest=0;
	  nloops=5;
	  break;
	}
      else
	  printf("Illegal value %s\n",str);
    }
#endif
 // Average test only for now
 dtest=0;
 nloops=5;


 printf("Parameters:\n Start at %f V \n Stop at %f V\n Increment is %f V\n Number of increments is %d\n",start,stop,inc,ninc);
	  
 if(start < stop)
   {
     if (inc < 0)
       inc*=-1;
   }
 else
   {
     if (inc > 0)
       inc*=-1;
   }


 sprintf (COMMAND,"%s","MG AV[0];");
  // Do tests of setting output and reading input

  for(int i = 0; i <= ninc; i++){
    
    usleep(1000);
    dac_set = start + i * inc;
    printf("\nStep %d set value %f\n",i,dac_set);
  
    if(  (dac_set > 10.0 ) || (dac_set < -10.0))
      {
	printf("Illegal dac set value (%f) \n",dac_set);
	return -1;
      }

    rio->SetAnalogOutput(0,dac_set);
    
    
    av=0;
    for (int j=0; j<nloops; j++)
      {
	value0 = rio->GetAnalogInput(0);
	printf("     Index %d  Read %.3f \n", j,value0);
	if(dtest==0)
	  {
	    diff = (dac_set-value0);  // difference between read and write values
	    if( fabs(diff) > max_diff)
	      max_diff = fabs(diff);
	    if(fabs(diff) < min_diff)
	      min_diff = fabs(diff);
	
	    av+=value0;
	  }
	usleep(50);
      }
    if(dtest==0) // average test
      {
	av=av/5;
	rio->Command(COMMAND,&REPLY[0], sizeof(REPLY) );
        sscanf(REPLY,"%f",&rio_av);
	printf("     Average value: calc %f, from filter pgm=%f \n",rio_av,av);
      }

    else if(dtest==1) // check differences between read and write steps   
      {
	if(i>0)
	  {
	    diff_set = fabs(dac_set - prev_set);
	    diff_read= fabs(value0 - prev_read);
	    diff = fabs(diff_set - diff_read);
	    printf("      Difference in set values =%f   Difference in read values = %f  fabs(difference)=%f\n", diff_set, diff_read,diff);
	  
	    if( diff > max_offset)
	      {
		printf(" ***  Voltage didn't appear to change at set value %f    ** \n",dac_set);
		diff_cntr++;
	      }
	    if( diff > max_diff)
	      max_diff = fabs(diff);
	    if(diff < min_diff)
	      min_diff = fabs(diff);

	  }
      
	prev_set=dac_set;
	prev_read=value0; // use last value read
      }

  }
  printf("\nOver the whole scan, max difference was %f and min difference was %f (absolute values) \n",max_diff,min_diff);
  if(dtest==1)
    {
      printf("Voltage change test used %f as the maximum difference in comparing read and write steps\n",max_offset);
      printf("Number of steps that failed this test: %d\n",diff_cntr);
    }
  return 0;

}


