
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "TGalilRIO.h"

// Little test program to accept a string for
// the Galil 47120. 

void check_string(char * name);

int main(int argc, const char * argv[]){

  TGalilRIO *rio = new TGalilRIO("192.168.1.100");

  char COMMAND[101];
  char REPLY[200];
  int len;

  printf ("main: starting\n");
  

  sprintf(COMMAND,"test"); 
  
    
  while ( isalpha(COMMAND[0]) )
    {
      printf("\nEnter command (A-Y) Digit or cntrl C to exit?  ");
      fgets(COMMAND, 100, stdin); 
      check_string(COMMAND);
      //    printf("Now COMMAND is \"%s\"\n",COMMAND);
     
    
   std::cout << "Sending Command...  " <<  std::endl;
   rio->Command(COMMAND,&REPLY[0] );
   std::cout <<"reply: " <<REPLY << std::endl;
    }  
}


void check_string(char * name)
{
  char *p,*q,*end;
  int l,len;
  // remove trailing spaces and \n if present
  // check for ; at end of string. If not there, add it
  len=strlen(name);
  //printf("name=\"%s\"  len=%d\n",name,len);

  q=strrchr(name,'\n');
  if(q!=NULL)
    {
      //printf("removing cr\n");
      *q='\0';
    }
  len=strlen(name);
  //printf("now name=\"%s\"  len=%d\n",name,len);
   

  // Trim trailing space
  end = name + strlen(name) - 1;
  
  while(end > name && isspace(*end)) end--;
  end++;
  *end='\0';
 len=strlen(name);
 //printf("after trim, name=\"%s\" len=%d\n",name,len);
  
 

  len=strlen(name);
  p=strrchr(name,';');

  //printf("p=%p\n");
  if(p==NULL)
    strncat(name,";",1);
 
  //printf("returning name=\"%s\"\n",name);
  return;
}
