#include <string.h>
#include <stdio.h>

int test(char *cmd);

int main(void)
{

  char cmd[]="B;";
  int len;

  printf("cmd=%s\n",cmd);
  len=strlen(cmd);
  printf("test: cmd=%s and len=%d\n",cmd,len);
  test(cmd);

}

int test(char *cmd)
{
  int i,len,c;
  char *p;
  char command[300];
  FILE *fin;
  len=strlen(cmd);
  printf("test: cmd=%s and len=%d\n",cmd,len);


  strncpy(command, cmd,len-1);
  printf("command=\"%s\" length=%d\n",command,strlen(command));

  printf("Now opening file POL.dmc\n");
  fin = fopen("POL.dmc","r");
  if(fin!=NULL)
    {
      while((c=getc(fin))!=EOF)
	putc(c,stdout);
    }
  else
    printf("Could not open file POL.dmc\n");


  return 1;


}
