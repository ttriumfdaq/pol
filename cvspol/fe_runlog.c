/*******************************************************************\

  Name:         fe_runlog.c
  Created by:   R. Poutissou 
                based on fedaq.c from TWIST, written by Peter Green

  Contents:     Fill runlog file for logging purposes
		This one only responds to Start/Stop run commands.

  Modified by:  S. Daviel  Jul 04
                Add client flags

\********************************************************************
*  
* $Log: fe_runlog.c,v $
* Revision 1.3  2006/07/21 18:34:42  suz
* start-daq-tasks
*
* Revision 1.2  2005/02/21 23:28:03  suz
* change cm_register_transition for Midas 1.9.5
*
* Revision 1.1  2004/08/10 19:54:26  suz
* original for POL; write runlog, stop run on error
*
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "midas.h"
#include "mcstd.h"

#include "experim.h"
#include "client_flags.h"  // prototypes

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fe_runlog";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
//INT display_period = 3000;
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 1000;
INT max_event_size_frag = 5*1024; /* unused */

/* buffer size to hold events */
INT event_buffer_size = 10*1000;

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

#define MAXBUFF 256
/* These are really positions within the buffer where things start
 */
#define TIMELEN 42
#define OPERATORLEN 32
#define COMMENTLEN 88
#define PURPOSELEN 80
#define ENABLELEN 8
#define EVCOUNTLEN 20
#define NCYCLELEN 25
#define TRIGGERLEN 24
#define MASSLEN 8
#define DCOFFLEN 8

static BOOL write_data;

HNDLE hDB;
INT gbl_run_number;

/* Root of Equipment/Settings tree */
//HNDLE hSettingRoot; not used

/* Root of Equipment/Variables tree */
// HNDLE hVarRoot; not used

/* Root of /Experiment/Edit on start */
HNDLE hEdit;

INT debug_cf=1; // debug client flags routines
INT debug_rl =0; // debug writing runlog

// needed by client flags
char    eqp_name[]="fifo_acq";
FIFO_ACQ_CLIENT_FLAGS fcf;
char client_str[132];
INT status,size;
HNDLE hCF;
BOOL tr_flag;
INT run_state;
INT run_number;
INT start_time;
BOOL first=TRUE;
BOOL gbl_stopping_flag;

  // function declarations
INT my_start(INT rn, char *error);
INT my_stop(INT rn, char *error);
INT my_prestart(INT rn, char *error);
INT my_post_stop(INT run_number, char *error);
void write_message1(INT status, char *name);
FILE* open_runlog (void);


/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {
  { "" }
};


#ifdef __cplusplus
}
#endif

#include "client_flags.c"  // code for client_flags handling
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int status;

  status = get_run_number(&gbl_run_number);
  status = cm_register_transition (TR_STOP, my_post_stop,600);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","could not register for (post) stop transition (%d)",status);
      return status;
    }
  
  status = cm_register_transition (TR_START, my_prestart,450); /* frontend start is 500 */
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","could not register for (pre) start transition (%d)",status);
      return (status);
    }
  
  status = cm_register_transition (TR_STOP,  my_stop,500); /* regular stop is 500 */
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","could not register for stop transition (%d)",status);
      return(status);
    }
  status = db_find_key(hDB,0,"/Experiment/Edit on start",&hEdit);
  if (status != SUCCESS) {
    cm_msg (MERROR, "frontend_init", "can't find Key for \"/Experiment/Edit on start\" ");
    return (status);
  }

  status = setup_clientflags();
  if(status != SUCCESS)
    {
      printf("frontend_init: failure attempting to set up client flags (%d)\n",status);
      return (status);
    }
  return (SUCCESS);
}


/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  return SUCCESS;
}

FILE *open_runlog (void)
{
  int status;
  char buff [256];
  int size;
  FILE *fp;

  if(debug_rl)
    printf("open_runlog: starting\n");
  size = sizeof(buff);
  status = db_get_value (hDB, 0, "/Logger/Data dir", buff, &size, TID_STRING, FALSE);
  if (status != SUCCESS) 
    {
      cm_msg (MERROR, frontend_name, "Can't get data for \"/Logger/Data dir\" - runlog cannot be updated");
      return NULL;
    }
  size = strlen (buff);
  if (buff [size - 1] != '/') {
    strcat (buff, "/");
  }
  strcat (buff, "runlog.txt");
  if(debug_rl)
    printf("Opening file: %s \n",buff);

  fp = fopen (buff, "a");
  if(!fp)
    cm_msg(MERROR,"open_runlog","failure opening runlog file \"%s\" ",buff);
  else
    {
      if(first)printf("Opened runlog file \"%s\" \n",buff);
      first=FALSE;
    }
  return (fp);
}

/*-- Begin of Run --------------------------------------------------*/



int padblanks (char *s, int n)
{
    /* Pad a string with n blanks at the end
     */

    s += strlen (s);
    while (n--) *s++ = ' ';
    *s = '\0';
    return (0);
}

INT begin_of_run(INT run_number, char *error)
{
  int status;
  char buff1 [MAXBUFF], buff2 [MAXBUFF];
  INT runnum;
  INT data_int;
  int size;
  FILE *fp;
  time_t now;
  char *s;
  char tmpstring32[32];

  

  if(debug_rl)
    printf("begin_of_run: starting run %d \n",run_number);
  gbl_stopping_flag = FALSE;
  start_time =  ss_time(); // set a timer when run was started
  if(debug_cf)
    printf("begin_run: starting, setting start_time to %d\n",start_time);
  tr_flag = FALSE; // and a flag for check_client_flags


  /* put here clear scalers etc. */
  gbl_run_number = run_number;

  /* open runlog. If it can't be opened, no point in getting
     all the values */

  fp = open_runlog ();
  if (fp == NULL) 
    {
      printf("Cannot open runlog file; no runlog can be written for run %d\n",run_number);
      return (SUCCESS); // let the run start
    }
   

  /* Set Running status
   */
  //  set_run_status (ST_RUNNING);
  time (&now);

/* Generate message for runlog
 * Note: On errors here I just give up - but still return success.
 * I still want the run to start.
 */
  s = buff1;
  /* can we not use run_number supplied as a parameter ? */
  status = get_run_number(&runnum);
  if (status != SUCCESS)
    {
       printf("begin_of_run: failure after get_value for \"/Runinfo/Run Number\"  (%d)\n",status);
       printf("          using supplied run number = %d\n",run_number);
       runnum=run_number;
    }
  else
    {
      if(runnum != run_number)
	printf("begin_of_run: WARNING... strange error in run numbers (read %d; expect %d)\n,",
	       runnum, run_number);
    }
  sprintf (s, "Start Run %5d %s", runnum, ctime (&now));
  printf("Start Run %5d %s\n", runnum, ctime (&now));

/* ctime puts a c/r on the end of things
 * We'll fix that!
 */
  size = strlen (s);
  s [size-1] = '\0';
  padblanks (s, TIMELEN);


  s += TIMELEN;
  sprintf(s,"E:");
  padblanks (s, 2);
  s += 2;
 
  status = my_find_and_load (hEdit, "Experimenter", NULL, s, OPERATORLEN, TID_STRING);
  if (status != SUCCESS)
    {
      printf("begin_of_run: failure after my_find_and_load for Experimenter  (%d)\n",status);
      return (SUCCESS); // let the run start
    }

  if(debug_rl)
    printf("after my_find_and_load for experimenter (E:), s=%s\n",s);
  padblanks (s, OPERATORLEN);
  s += OPERATORLEN;

  sprintf(s,"T:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hEdit, "run_title", NULL, s, COMMENTLEN, TID_STRING);
  if (status != SUCCESS)
    {
      printf("begin_of_run: failure after my_find_and_load for run_title (%d)\n",status);
      return (SUCCESS);
    }
  if(debug_rl)
    printf("after my_find_and_load for title (T:), s=%s\n",s);
  /* Just in case
   */
  padblanks (s,COMMENTLEN );
  s += COMMENTLEN;
  *s = '\0';
  
  /* Do it again for other info and Logger status
   */
  strncpy (buff2, buff1, TIMELEN);
  s = buff2;
  s += TIMELEN;
  
  sprintf(s,"E:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hEdit, "Element", NULL, s, TRIGGERLEN, TID_STRING);
  if (status != SUCCESS)
    {
      printf("begin_of_run: failure after my_find_and_load for Element (%d)\n",status);
      return (SUCCESS);
    }
  padblanks (s, TRIGGERLEN);
  s += TRIGGERLEN;

  sprintf(s,"M:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hEdit, "Mass", NULL, &data_int, 1, TID_INT);
  if (status != SUCCESS)
    return (SUCCESS);
  sprintf (s," %i6 ",data_int);
  padblanks (s, MASSLEN);
  s += MASSLEN;


  sprintf(s,"D:");
  padblanks (s, 2);
  s += 2;
  status = my_find_and_load (hEdit, "DC offset(V)", NULL, &data_int, 1, TID_INT);
  if (status != SUCCESS)
    {
      printf("begin_of_run: failure after my_find_and_load for DC offset (%d)\n",status);
      return (SUCCESS);
    }
  sprintf (s," %i6 ",data_int);
  padblanks (s, DCOFFLEN);
  s += DCOFFLEN;

  sprintf(s,"L:");
  padblanks (s, 2);
  s += 2;

  status = my_find_and_load (hEdit, "Write Data", NULL, &write_data, 1, TID_BOOL);
  if (status != SUCCESS)
    {
      printf("begin_of_run: failure after my_find_and_load for Write Data (%d)\n",status);
      return (SUCCESS);
    }

  if(debug_rl)printf("about to read write_data... \n");
  if (write_data)
    sprintf (s, "Enabled ");
  else
    sprintf (s, "Disabled");
  

  if(debug_rl)
    printf("begin_of_run: write_data=%d \n",write_data);

  padblanks (s, ENABLELEN);
  s += ENABLELEN;

  *s = '\0';

  if(debug_rl)
    {
      printf ("buff1: %s\n", buff1);
      printf ("buff2: %s\n", buff2);
      printf("begin_of_run: writing to runlog ...\n");
    }

  fprintf (fp, "%s\n", buff1);
  fprintf (fp, "%s\n", buff2);
  fclose (fp);

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  /* Set Stopped status
   */
  //  set_run_status (ST_STOPPED);
  return SUCCESS;
}

INT my_post_stop(INT run_number, char *error)
{
  int status;
  char buff [256];
  INT runnum;
  int size;
  FILE *fp;
  DWORD ncycle;
  double evcount;
  time_t now;
  char *s;

  time (&now);
  
  if(debug_rl)
    printf("my_post_stop: starting with rn=%d\n",run_number);

  /* open runlog file */
  fp = open_runlog ();
  if (fp == NULL)
    {
      cm_msg(MERROR,"my_post_stop","cannot open runlog to write endrun values for run %d\n",run_number);
      return (SUCCESS);
    }

  /* why not use parameter run_number ? */
  status = get_run_number(&runnum);
  if (status != SUCCESS)
    {
      printf("my_post_stop: error getting value of run number (%d)\n",status);
      /* well, just use parameter run_number...  */
      runnum = run_number;
    }

  if(debug_rl)printf("run number is %d\n",runnum);

  s = buff;
  sprintf (s, "End   Run %5d %s", runnum, ctime (&now));

/* ctime puts a c/r on the end of things
 */
  size = strlen (s);
  s [size-1] = '\0';
  strcat (s, "                                ");

  /* Modified to put out statistics 
   */
  s += TIMELEN;
  status = my_find_and_load (0, "/Equipment/Info ODB/Variables/current cycle", NULL,
			     &ncycle, 1, TID_DWORD);
  if (status != SUCCESS)
    {
      printf("my_post_stop: error after my_find_and_load for current cycle (%d)\n",status);
      return (SUCCESS);
    }

  sprintf (s, "Cycles:  %10i,", ncycle);
  strcat (buff, "               ");
  s += NCYCLELEN;
  
  if(debug_rl)printf("my_post_stop: write_data=%d\n",write_data);
  if (write_data) 
    {      
      size=sizeof(evcount);
      status = db_get_value(hDB, 0, "/Logger/Channels/0/Statistics/Events written", &evcount,&size,
			    TID_DOUBLE,FALSE);
      if (status != SUCCESS)
	{
	  printf("my_post_stop: error getting value for \"/Logger/Channels/0/Statistics/Events written\" (%d)\n",status);
	  return (SUCCESS);
	}
      sprintf (s, "Logger %10.0f ", evcount);
    } 
  else 
    {
      sprintf (s, "Logger Disabled");
    }

  strcat (buff, "               ");
  s += EVCOUNTLEN;
  *s = '\0';

  if(debug_rl)
    printf("my_post_stop: printing to runlog ...\n");

  printf ( "%s\n", buff);

  fprintf (fp, "%s\n", buff);
  fclose (fp);

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  /* Set Paused status
   */
  //  DB0PRINT("Pause run %d\n",gbl_run_number);
  //  set_run_status (ST_PAUSED);
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  /* Set Running status
   */
  //DB0PRINT("Resume run %d\n",gbl_run_number);
  //  set_run_status (ST_RUNNING);
  return SUCCESS;
}

INT my_prestart(INT rn, char *error)
{

  BOOL enable_client_check;
  char str[256];

  if(debug_rl)
    printf("my_prestart: Run %d starting\n",rn);

  start_time = 0;  /* don't start checking client flags until begin_of_run
		      in case of prestart run failure (important for mlogger's data file) */

 //  DO NOT CLEAR CLIENT FLAGS HERE ( in case any clients also use a prestart)
  //  they are cleared after a stop ready for next time.

  // get the latest value
  size = sizeof(fcf.enable_client_check);
  sprintf(str,"%s/enable client check",client_str);
  status = db_get_value(hDB, 0, str, &fcf.enable_client_check, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"tr_prestart","could not get value for \"%s\"  (%d)",str,status);
      return(status);
    }
 
  if(!fcf.enable_client_check)
    printf("my_prestart: Info - check on client flags is disabled\n");

  if(debug_cf)
    printf("my_prestart: Calling setup_hot_clients\n");

  status =  setup_hot_clients();
  if (status != DB_SUCCESS)
      return(status); // failure from hot links
    
  /* set fe_runlog's client flag to SUCCESS now (actually borrowing mdarc's) */
  if(debug_cf)
    printf("my_prestart: setting mdarc client flag TRUE\n");
  fcf.mdarc=TRUE;
  size = sizeof(fcf.mdarc);
  status = db_set_value(hDB, hCF, "mdarc", &fcf.mdarc, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "my_prestart", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
      return status;
    } 
  return status;
}


INT my_stop(INT rn, char *error)
{
  /* remove hot link on client flags while the run is off */
  if(debug_cf)printf("my_stop: starting\n");
  status = close_client_hotlinks();
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"my_stop","error closing client hotlinks (%d)",status);
    

  start_time = 0; // make sure check_client_flags is not called if run stopped soon after starting.  
  /* clear client success flags ready for next time */
  status = clear_client_flags();
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"my_stop","error clearing client success flags (%d)",status);

  return status;
}


INT setup_clientflags(void)
{
  /* called from frontend_init */
  run_number = gbl_run_number; // "run_number" needed by client flag routines
  status = create_ClFlgs_record(); // create the record for the client flags
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_clientflags","error return from create_ClFlgs_record (%d)",status);
      return(status);
    }

  // initialize a few things for client_flags
  start_time = 0; // dummy value


  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_clientflags","key not found /Runinfo/State (%d)",status);
    return(status);
  }

  if(run_state == STATE_STOPPED)
    {
      if(debug_cf)
	printf("setup_clientflags: run is STOPPED, clearing client flags\n");
      status = clear_client_flags();
      if(status != DB_SUCCESS)
	return status;
    }
  else if (run_state == STATE_RUNNING)
    {  /* run is already in progress; setup hotlinks */
      if(debug_cf)printf("setup_clientflags: running already so calling setup_hot_clients\n");
      status =  setup_hot_clients();
      if (status != DB_SUCCESS)
      {
	return(status); // failure from hot links
      }
    }
  
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */

  /* start_time=0  skips client flag check  */

  if(start_time > 0) // run has recently started 
    {
      if(ss_time() - start_time > 20) // allow 20s for everything to start
	{
	  if(debug_cf)
	    printf("frontend_loop: ***  about to call check_client_flag, start_time=%d, ss_time=%d, tr_flag=%d\n",
		   start_time,ss_time(),tr_flag);
	  /* if !fcf.enable_client_check, then check_client_flag will issue a warning message
	     but will not stop the run */
	  status = check_client_flags(&tr_flag); // may stop the run
	  if(status == DB_SUCCESS)
	    {
	      if(debug_cf)printf("frontend_loop: check_client_flags returns status=%d and tr_flag=%d\n",status,tr_flag);
	      if(tr_flag) // run is in transition; leave the flag set for next time
		printf("frontend_loop:run is still in transition... waiting to recheck client flags\n");
	      else
		{
		  if(debug_cf)printf("frontend_loop:resetting start_time to 0 to prevent check_client_flags being called again\n");
		  start_time = 0; // reset so we don't come here again
		}
	    }	    
	}
    } // start_time > 0
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\
  
  Readout routines for different events

\********************************************************************/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  if (test) {
    ss_sleep (count);
  }
  return (0);
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch(cmd)
    {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_ATTACH:
      break;
    case CMD_INTERRUPT_DETACH:
      break;
    }
  return SUCCESS;
}

INT my_find_and_load (HNDLE hRoot, const char *name, HNDLE *hKey, void *data,
		   int numvalues, int type)
{
  /* function to locate the Key for data and load it from the odb
   * hKey may be null if the key is not required
   * Returns SUCCESS if successful
   */
  HNDLE hTemp;
  int size;
  int status;
  char my_name[128];

  if(debug_rl)
    printf("my_find_and_load starting with name=%s \n",name);

  sprintf(my_name,"%s",name);
  status = db_find_key (hDB, hRoot, my_name, &hTemp);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't find Key for %s", my_name);
    return (status);
  }
  if (type == TID_BYTE || type == TID_SBYTE || type == TID_CHAR || type == TID_STRING)
    size = numvalues;
  else if (type == TID_WORD || type == TID_SHORT)
    size = numvalues * sizeof (short);
  else if (type == TID_DWORD || type == TID_INT
	   || type == TID_BOOL  || type == TID_FLOAT)
    size = numvalues * sizeof (DWORD);
  else if (type == TID_DOUBLE)
    size = numvalues * sizeof (double);
  else {
    cm_msg (MERROR, frontend_name, "unrecognized type %d", type);
    return (FE_ERR_ODB);
  }
  status = db_get_data (hDB, hTemp, data, &size, type);
  if (status != SUCCESS) {
    cm_msg (MERROR, frontend_name, "can't get value for %s", my_name);
    return (status);
  }
 
  if (hKey != NULL)
    *hKey = hTemp;
  return (SUCCESS);
}

INT get_run_number(INT *rn)
{
  INT runnum=0;
  size=sizeof(runnum);
  status = db_get_value(hDB, 0,"/Runinfo/Run Number",&runnum, &size, TID_INT, FALSE);
  if (status != SUCCESS)
      cm_msg(MERROR,"get_run_number","error getting value of run number (%d)",status);
  *rn = runnum;
  return status;
}  
  

void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}


