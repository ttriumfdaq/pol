/**
 * Function prototypes for GPIB device driver
 *
 * @author David Morris
 *
 * $Log: dd_gpib.h,v $
 * Revision 1.1  2007/10/23 19:40:13  suz
 * new to cvs
 *
 * Revision 1.2  2004/05/27 23:55:34  cadfael
 * Have demands working to DAC, and scan list build running
 *
 * Revision 1.1.1.1  2004/05/25 18:26:26  cadfael
 * Started CVS
 *
 */

/* Extra define for GPIB commands */

#define CMD_ADDRESS 100
#define CMD_EOS			101
#define CMD_EOT			102
#define CMD_SPOLL		103

#define GPIB_EOS_MODE_NONE	0x00
#define GPIB_EOS_MODE_EOS		0x04
#define GPIB_EOS_MODE_EOI		0x08
#define GPIB_EOS_MODE_8BIT	0x10

/*--------------------------------------------------------------------*/
/*   Function Declarations																						*/

/*
INT gpib_init(HNDLE hKeyRoot, INT index, INT channels);
INT gpib_exit(INT index);
INT gpib_address(INT iAddress);
INT gpib_eos(INT iAddress, char cMode, char cEOS);
INT gpib_eot(INT iAddress, INT iValue);
INT gpib_spoll(INT address, char * result);
INT gpib_set(INT iAddress, char * szString, INT iLength);
INT gpib_get(INT address, char * szString, INT iLength);
*/
INT gpib(INT cmd, ...);
