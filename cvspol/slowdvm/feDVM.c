/**
 * @file
 * This module contains the Midas DVM Front End code for the E920 DAQ system.
 *
 *
 * @author David Morris - TRIUMF, Created by:   Stefan Ritt
 */
/*   
 * $Log: feDVM.c,v $
 * Revision 1.1  2007/10/23 19:40:03  suz
 * new to cvs
 *
 * Revision 1.2  2004/06/10 00:01:28  cadfael
 * Many changes getting the front end working.
 * Completed timing tests of Agilent DVM
 * Added BOR EOR commands in feE920
 *
 * Revision 1.1.1.1  2004/05/25 18:26:26  cadfael
 * Started CVS
 *
 *
 */
#include <stdio.h>
#include "midas.h"

#include "Agilent34970A.h"
//#include "BurleighWavemeter.h"
#include "dd_gpib.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif


extern run_state;

/*-- Frontend globals ------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "feDVM";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms    */
//INT display_period = 5000;
  INT display_period = 0;  /* Disable disply */

/* maximum event size produced by this frontend */
INT max_event_size = 32768;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5*1024*1024;
		
/* buffer size to hold events */
INT event_buffer_size = 1400000; /* prior to 2.0.0 DEFAULT_EVENT_BUFFER_SIZE */

#define Debug cm_msg

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
INT dvm_read(char *pevent, INT off);

/*-- Global variables ------------------------------------------------*/
HNDLE hDB;

/*-- Equipment list ------------------------------------------------*/

/*--------------------------------------------------------------------*/
/*   Name: <device>_driver[]                                                                                    */
/*                                                                                                                                      */
/*   Purpose: This structure defines all the instances of a device      */
/*   for the driver functions. The array built for each device is           */
/*   terminated with an entry that has a NULL string for the name.      */
/*   This is used to test for the end of the array later. The second    */
/*   entry is the device driver for the specific equipment                */

DEVICE_DRIVER Agilent34970A_driver[] = {
  { "DVM",  gpib, 1},
  { "" }
};



/*--------------------------------------------------------------------*/
/*   Name: equipment[]                                                                                              */
/*                                                                                                                                      */
/*   Purpose: This structure defines several parameters for the             */
/*   device from an equipment point of view. Several parameters for     */
/*   running, data formats and collection rates followed by the             */
/*   names of the driver functions for reading data and the main            */
/*   action routine. The array built for each device is terminated      */
/*   with an entry that has a NULL string for the name. This is used    */
/*   to test for the end of the array later.                                                    */

/* DBM

 This Trigger seems to be needed to get the logger to create the proper
keys so it will work. It was disabled. Unsure if this should be enabled or not */

EQUIPMENT equipment[] = {
  {"DVM",      /* equipment name */
    {
     6, 0x0,        /* event ID, trigger mask */
     "",             /* event buffer */
     EQ_PERIODIC,              /* equipment type */
     0,                    /* event source */
     "MIDAS",              /* format */
     TRUE,                 /* enabled */
     RO_ALWAYS |
     RO_ODB,               /* update ODB and history */
     60000,                /* read every N sec */
     0,                    /* stop run after this event limit */
     0,                    /* number of sub event */
     60,                   /* log history */
     "","","", },
   cd_Agilent34970A_read,       /* readout routine in slow control mode */
   cd_Agilent34970A,            /* class driver main routine */
   Agilent34970A_driver,     /* device driver list */
   NULL,                    /* init string */
  },


  { "" }
};

#ifdef __cplusplus
}
#endif

/*-- Dummy routines ------------------------------------------------*/
INT  interrupt_configure(INT cmd, INT source[], PTYPE adr) {return 1;};
/*--- Call back ---------------------------------------------------*/

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init() {

  cm_get_experiment_database(&hDB, NULL);
  cd_Agilent34970A(CMD_INIT, (EQUIPMENT *) &equipment);
    //  atexit ((void *)cm_disconnect_experiment);

  return CM_SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit() {
  cd_Agilent34970A(CMD_EXIT, (EQUIPMENT *) &equipment);
  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop() {
  return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error) {
  
  //  Agilent34970A_readChannel((EQUIPMENT *) &equipment, 101);
  
  return CM_SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error) {

  //  Agilent34970A_readChannel((EQUIPMENT *) &equipment, 101);

  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error) {
  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error) {
  return CM_SUCCESS;
}

/*------------------------------------------------------------------*/
/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test) {
  int   i;
DWORD dummy;

  dummy = 0; 
  for (i=0 ; i<count ; i++) {
    if (dummy == 1) {
      if (!test) {
        return 1;
      }
    }
  }
  return FALSE;
}
