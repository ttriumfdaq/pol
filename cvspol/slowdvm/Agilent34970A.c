/*
 * This modules contains a simple set of routines to use an 
 * Agilent 34970A Data Acquisition/Switch Unit.
 * It is a simplification of the class driver written by Dave Morris
 * see below
 * @author Renee Poutissou June 2004
 *
 *** This module contains the implementation of a class driver for an
 *** Agilent 34970A Data Acquisition/Switch Unit. The class driver can use either a GPIB
 *** or an RS-232 driver to communicate with the instrument.
 *** 
 *** David Morris - TRIUMF 2004
 */
#include "midas.h"
#include "msystem.h"

#include "Agilent34970A.h"
#include "experim.h"
#include "dd_gpib.h"


/**
 *   Name: AGILENT_INFO
 *
 *   Purpose: This structure is the basic parameter definition for
 *   the device to be handled. The name of the structure is device
 *   specific. It is local to this module, but probably wise to keep
 *   things unique.
 *
 *   The order is not relevant. Each paradvm will be allocated at
 *   run time (note the pointers) and freed when no longer needed.
 *   The layout is purely for ease of use; the actual location in
 *   the equipment record is determined by code in the <device>_init
 *   function.
 */
typedef struct {
  /* ODB keys */
  HNDLE hKeyRoot;
  HNDLE hKeyMeasured;
  HNDLE hKeyDac1;
  HNDLE hKeyDac2;
  /* globals */
  INT    num_channels;
  INT   num_scanned;
  INT    format;
  INT    last_channel;
  DWORD  last_update;

  /* items in /Settings directory */
  INT address;
  char scanString[256];  char scanList[64];
  INT scanCount;
  INT * modules;
  char * names;
  char * config;
  /* Module 34907 specifics */
  float fDac1Demand;
  INT fDac1Address;
  INT fDac1Handshake;
  float fDac2Demand;  /* add these for Phil's expt; */ 
  INT fDac2Address;   /* read 3rd DVM channel on demand */
  INT fDac2Handshake;
  
  /* items in /Variables directory */
  float  *fMeasured;

  /* channel information */
  void  **driver;
  INT    *channel_offset;
  void  **dd_info;

} AGILENT_INFO;

INT scanStale = TRUE;
HNDLE hDB;
INT dd1 =0; /* Set dd1 = 1 for debugging */

/* Function prototypes */


#ifndef abs
#define abs(a) (((a) < 0)   ? -(a) : (a))
#endif


/**
 *   free_mem
 *
 *   Purpose: This static function will free all allocated memory
 *   for the device at the end of its lifetime. It is crucial that
 *   all entries in the device structure are included here
 *
 *   Inputs: <device>_info - pointer to structure for the specific
 *   device type.
 *
 *   Precond: None
 *
 *   Outputs: None
 *
 *   Postcond: All allocated memory for the device instance will be
 *   freed.
 */
static void free_mem(AGILENT_INFO *pAgilentInfo) {
  free(pAgilentInfo->names);

  free(pAgilentInfo->fMeasured);

  free(pAgilentInfo->driver);
  free(pAgilentInfo->dd_info);
  free(pAgilentInfo->channel_offset);

  free(pAgilentInfo);
}

/**
 *   Name: Agilent34970A_read
 *
 *   Purpose: This routine collects the 
 */
void Agilent34970A_read(EQUIPMENT *pequipment, int channel) {
  INT         i, status;
  AGILENT_INFO  *pAgilentInfo;
  char         szData[DVM_STR_LENGTH + 1];
  INT iCount;
  char spoll;
  char cmdGpib[64];
  DWORD start;

  if (dd1) printf("Agilent34970A_read\n");

  start = ss_millitime();
  
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;

  if (channel == -1) {
    for (i = 0; i < pAgilentInfo->num_channels; i++) {
      Agilent34970A_read(pequipment, i);
    }
  } else {
//    sprintf(cmdGpib, "*CLS;ESR?;*ESE 1;ROUT:SCAN (@%d);", 101 + channel);
    
//printf("gpib cmd channel %d %s\n", channel, cmdGpib);

//    status = DRIVER(channel)(CMD_SET, pAgilentInfo->address, cmdGpib, strlen(cmdGpib));
    
    scanStale = TRUE;
    
    sprintf(cmdGpib, "*CLS;READ?;*OPC;");
    
    if (dd1) printf("gpib cmd channel %d %s\n", channel, cmdGpib);
    
    status =  gpib(CMD_SET, pAgilentInfo->address, cmdGpib, strlen(cmdGpib));

    iCount = 0;
    do {  // wait for operation complete bit set
      status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
      iCount ++;
    } while ((spoll & 0x20) == 0);

    iCount = gpib(CMD_GET, pAgilentInfo->address, szData, DVM_STR_LENGTH);

    if (dd1) printf("gpib result %s\n", szData);

    if (status != FE_SUCCESS) {
      cm_msg(MERROR, "Agilent34970A_read", "Error in device driver - CMD_GET");
    }

    sscanf(szData, "%e", &pAgilentInfo->fMeasured[channel]);
  }


  db_set_data(hDB, pAgilentInfo->hKeyMeasured, pAgilentInfo->fMeasured,
	      pAgilentInfo->num_channels * sizeof(float),
	      pAgilentInfo->num_channels, TID_FLOAT);

  pequipment->odb_out++;
  
  if(dd1) printf("Acq time %ld ms\n\n", ss_millitime() - start);
}

/**
 * Reads data for requested channel and then restores scan list
 */
INT Agilent34970A_readChannel(EQUIPMENT *pequipment, INT channel) {
  INT       status;
  AGILENT_INFO *pAgilentInfo;
  char command[128];
  char spoll;
  char szData[32];

  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;

  if ((channel < 101) || (channel > 120)) {
    cm_msg(MERROR, "Agilent34970A_readChannel", "Channel number out of range");
    return FE_ERR_HW;
  }
  
  sprintf(command, "ROUT:SCAN (@%d);", channel);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  sprintf(command, "READ?;*OPC;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
  do {  // wait for operation complete bit set
    status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
  } while ((spoll & 0x20) == 0);

  //status = gpib(CMD_GET, pAgilentInfo->address, szData, strlen(szData));
  status = gpib(CMD_GET, pAgilentInfo->address, szData, 32);
        
  sprintf(command, "ROUT:SCAN (@%s);", pAgilentInfo->scanList);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  if(dd1)printf("readChannel data %s\n", szData);
  
  sscanf(szData, "%e", &pAgilentInfo->fMeasured[channel - 101]);
  if(dd1)printf("Measured data %f\n", pAgilentInfo->fMeasured[channel - 101]);

  return 0;
}

/**
 * Reads data for requested channel and then restores scan list
 * and returns the value read
 */
float Agilent34970A_read_returnChannel(EQUIPMENT *pequipment, INT channel) {
  INT        status;
  AGILENT_INFO *pAgilentInfo;
  char command[128];
  char spoll;
  char szData[32];
  char* sptr;
  float rdata;
  INT loop;

  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;

  if ((channel < 101) || (channel > 120)) {
    cm_msg(MERROR, "Agilent34970A_readChannel", "Channel number out of range");
    return FE_ERR_HW;
  }
  
  sprintf(command, "ROUT:SCAN (@%d);", channel);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  sprintf(command, "READ?;*OPC;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
  loop = 0;
  do {  // wait for operation complete bit set
    // spoll = 0x20;
    status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
    loop += 1;
  } while ((spoll & 0x20) == 0);
  //if(dd1)printf("spoll loop count %d\n",loop);

  //  status = gpib(CMD_GET, pAgilentInfo->address, szData, strlen(szData));
  status = gpib(CMD_GET, pAgilentInfo->address, szData, 32);
  if(dd1)printf("status return from gpib_get %d\n",status);
   
  sprintf(command, "ROUT:SCAN (@%s);", pAgilentInfo->scanList);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  sptr = szData;
  rdata = strtod(sptr,&sptr);
  //printf("value %f, sptr [%s]\n",rdata,sptr);
  if(dd1)printf("Measured data %f\n", rdata);
  return (rdata);
}



/**
 * Agilent 34901 Multiplexer Module demand. Called when a SlotxConfig entry is modified.
 */
void Agilent34901_demand(INT hDB, INT hKey, void *info) {
  AGILENT_INFO *pAgilentInfo;
  EQUIPMENT *pequipment;

  if(dd1) printf("Entering routine Agilent34901_demand\n");
  pequipment = (EQUIPMENT *) info;
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  Agilent34970A_config(pequipment);
  pequipment->odb_in++;   // increment ODB->FE counter
}

/**
 * Agilent 34907 Multifunction Module demand. Called when a SlotxConfig entry is modified.
 */
void Agilent34907_demand_DAC1(INT hDB, INT hKey, void *info) {
  INT       status;
  AGILENT_INFO *pAgilentInfo;
  EQUIPMENT *pequipment;
  char command[64];
  DWORD start;
  float rdata;
  char spoll;
  char szData[32];
  char* sptr;
  INT loop,channel;
  INT ihand;

  if(dd1) printf("Entering routine Agilent34907_demand_DAC1\n");
  start = ss_millitime();

  pequipment = (EQUIPMENT *) info;
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  
  /* set DAC to requested value */ 
  sprintf(command, "SOURCE:VOLTAGE %f,(@%d)", pAgilentInfo->fDac1Demand, pAgilentInfo->fDac1Address);

  //if(dd1)printf("write %s to DAC\n", command);

  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
  
  //if(dd1)cm_msg(MINFO, "Agilent34907_demand", "DAC set time %ld ms\n", ss_millitime() - start);

  //ss_sleep(10);
  
  //cd_Agilent34970A_read((char *) &pequipment, 0);
  //RP  rdata = Agilent34970A_read_returnChannel(pequipment, 101); 
  // Try different ways //
  channel = 101;
  sprintf(command, "ROUT:SCAN (@%d);", channel);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  //  sprintf(command, "READ?;*OPC;", pAgilentInfo->scanString);
  status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
  sprintf(command, "INIT;FETCH?;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
  loop = 0;
  do {  // wait for operation complete bit set
    status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
    loop += 1;
  } while ((spoll & 0x20) == 0);
  //if(dd1)printf("spoll loop count %d\n",loop);

  status = gpib(CMD_GET, pAgilentInfo->address, szData, 64);
  //if(dd1)printf("status return from gpib_get %d\n",status);
   
  sptr = szData;
  rdata = strtod(sptr,&sptr);
  //if(dd1)printf("rdata is %f\n",rdata);
  db_set_data_index(hDB,pAgilentInfo->hKeyMeasured, &rdata, sizeof(float),0,TID_FLOAT);
  /* Set the handshake */
  ihand = 1;
  //  db_find_key(hDB,pAgilentInfo->hKeyDac1,"/Handshake",&hkey);
  //db_set_data(hDB,hKey,&ihand,sizeof(ihand),TID_INT);
  db_set_value(hDB,pAgilentInfo->hKeyDac1,"/Handshake",&ihand,sizeof(ihand),1,TID_INT);
  //pequipment->odb_in++;   // increment ODB->FE counter
  if(dd1)cm_msg(MINFO, "Agilent34907_demand_DAC1", "DAC set + read : total time %ld ms\n", ss_millitime() - start);
  sprintf(command, "ROUT:SCAN (@%s);", pAgilentInfo->scanList);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
}



/**
 * Agilent 34907 Multifunction Module demand
     for Phil's POL expt (NaCell scan)
     reads the third channel of the DVM upon request via hotlink
 */
void Agilent34907_demand_DAC2(INT hDB, INT hKey, void *info) {
  INT       status;
  AGILENT_INFO *pAgilentInfo;
  EQUIPMENT *pequipment;
  char command[64];
  DWORD start;
  float rdata;
  char spoll;
  char szData[32];
  char* sptr;
  INT loop,channel;
  INT ihand;

  if(dd1) printf("Entering routine Agilent34907_demand_DAC2\n");
  start = ss_millitime();

  pequipment = (EQUIPMENT *) info;
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  

  channel = 103;  // third channel of the DVM
  sprintf(command, "ROUT:SCAN (@%d);", channel);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  //  sprintf(command, "READ?;*OPC;", pAgilentInfo->scanString);
  status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
  sprintf(command, "INIT;FETCH?;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
  loop = 0;
  do {  // wait for operation complete bit set
    status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
    loop += 1;
  } while ((spoll & 0x20) == 0);
  //if(dd1)printf("spoll loop count %d\n",loop);

  status = gpib(CMD_GET, pAgilentInfo->address, szData, 64);
  //if(dd1)printf("status return from gpib_get %d\n",status);
   
  sptr = szData;
  rdata = strtod(sptr,&sptr);
  //if(dd1)printf("rdata is %f\n",rdata);
  db_set_data_index(hDB,pAgilentInfo->hKeyMeasured, &rdata, sizeof(float),2,TID_FLOAT); // index=2
  /* Set the handshake */
  ihand = 1;
  //  db_find_key(hDB,pAgilentInfo->hKeyDac2,"/Handshake",&hkey);
  //db_set_data(hDB,hKey,&ihand,sizeof(ihand),TID_INT);
  db_set_value(hDB,pAgilentInfo->hKeyDac2,"/Handshake",&ihand,sizeof(ihand),1,TID_INT);
  //pequipment->odb_in++;   // increment ODB->FE counter
  if(dd1)cm_msg(MINFO, "Agilent34907_demand_DAC2", "DVM chan 3 read : total time %ld ms\n", ss_millitime() - start);
  sprintf(command, "ROUT:SCAN (@%s);", pAgilentInfo->scanList);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

}

/**
 * Agilent 34970 Scan List demand. Called when the ScanList entry is modified.
 * The scan list is parsed to count the number of scanned channels. A ROUTE command
 * is sent to the Agilent 34970A to update the scan list.
 */
void Agilent34970A_demandScan(INT hDB, INT hKey, void *info) {
  INT status;
  AGILENT_INFO *pAgilentInfo;
  EQUIPMENT *pequipment;
  char command[32];
  char * pStart;
  char * pEnd;

  if(dd1) printf("Entering routine Agilent34907a_demandScan\n");
  pequipment = (EQUIPMENT *) info;
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;

  pStart = pAgilentInfo->scanList;  
  pAgilentInfo->scanCount = 1;
  while (pStart != NULL) {
    pEnd = strchr(pStart, ',');
    if (pEnd == NULL) break;
    pAgilentInfo->scanCount++;
    pStart = pEnd + 1;
  }
  
  sprintf(command, "*ESE 1;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  sprintf(command, "ROUT:SCAN (@%s);", pAgilentInfo->scanList);
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
  
  pequipment->odb_in++;   // increment ODB->FE counter
}

/**
 * Writes the configuration strings to the DVM to build the scan list
 */
void Agilent34970A_config(EQUIPMENT *pequipment) {
  AGILENT_INFO *pAgilentInfo;
  HNDLE hKey, hConfig;
  int i, j, size, status;
  char config[64];
  char str[64];
  char command[64];
  
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  pAgilentInfo->num_scanned = 0;

  if(dd1)printf("Agilent34970A_config\n");
    
  // clear the instrument before sending any commands
    
  /* Use the Modules entries to identify the SlotxConfig type */
  if (db_find_key(hDB, pAgilentInfo->hKeyRoot, "Settings/Modules", &hKey) == DB_SUCCESS) {
    for (i = 0; i < 3; i++) {
      INT module;
      size = sizeof(module);
      status = db_get_data_index(hDB, hKey, &module, &size, i, TID_INT);
      if (module != 0) {  // we have a module in this slot
        // check that hardware matches ODB entry for module type
        char mod[16];
        sprintf(mod, "%d", module);
        sprintf(command, "SYST:CTYP? %d", (i + 1) * 100);
        status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));
        status = gpib(CMD_GET, pAgilentInfo->address, str, 64);
	//        status = gpib(CMD_GET, pAgilentInfo->address, str, strlen(str));
        cm_msg(MINFO, "Agilent34970A_config", "Module in slot %d is %s", (i + 1) * 100,str);
        
        if (strstr(str, mod) == NULL) {
          cm_msg(MERROR, "Agilent34970A_config", "Mismatch for ODB module type and hardware");
          return;
        }
        
        sprintf(str, "Settings/Slot%dConfig", i + 1);
        size = 64;
        if (db_find_key(hDB, pAgilentInfo->hKeyRoot, str, &hConfig) == DB_SUCCESS) {
          switch (module) {
            case 34901:
              strcpy(pAgilentInfo->scanString, "");
              for (j = 0; j < 20; j++) {  // 20 channels in this module
                status = db_get_data_index(hDB, hConfig, config, &size, j, TID_STRING);
                if (strlen(config) > 0) {  // parse and send GPIB commands
                  char * pConfig = config;
                  char * pSep;
                  char num[16];
                  pAgilentInfo->num_scanned ++;
                  sprintf(num, "%d", 101 + j);
                  strcat(pAgilentInfo->scanString, num);
                  strcat(pAgilentInfo->scanString, ",");
                  while (1) {
                    pSep = strchr(pConfig, ';');
                    if (pSep != NULL) *pSep = 0x0;
                    sprintf(command, "%s,(@%d)", pConfig, (i + 1) * 100 + j + 1);
		    if(dd1)printf("%s\n", command);
                    status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

                    if (pSep == NULL) break;
                    pConfig = pSep + 1;
                  }
                }
              }
              break;
            case 34907:
	      if(dd1) printf("No config action for multi\n");              
              break;
            default:
              cm_msg(MERROR, "Agilent34970A_config", "Unrecognized number in Settings/Modules %d", module);
              break;
          }
        }
      }
    }
  }
  
  sprintf(command, "DISPLAY OFF;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  sprintf(command, "SENS:ZERO:AUTO OFF;");
  status = gpib(CMD_SET, pAgilentInfo->address, command, strlen(command));

  if(dd1)printf("Agilent34970A_config done %s\n",pAgilentInfo->scanString);
}

/**
 * Configure hotlink demands for 34907 module. There are two digital ports, counter and 2 DACs.
 * DACs must always be demand type so create them and ignore rest for now.
 */
void Agilent34970A_multi(EQUIPMENT *pequipment) {
  AGILENT_INFO *pAgilentInfo;
  HNDLE  hKey,hDac,hDac2;
  int i,  size, status;
  char str[64];
  
  if(dd1) printf("Agilent34970A_multi\n");
  
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  
  /* Use the Modules entries to identify the module type */
  if (db_find_key(hDB, pAgilentInfo->hKeyRoot, "Settings/Modules", &hKey) == DB_SUCCESS) {
    for (i = 0; i < 3; i++) {
      INT module = 0;
      size = sizeof(module);
      db_get_data_index(hDB, hKey, &module, &size, i, TID_INT);
      if (module == 34907) {  // we have a multifunction module in this slot
        sprintf(str, "Settings/Slot%dDAC1", i + 1);
	status = db_find_key(hDB, pAgilentInfo->hKeyRoot, str, &pAgilentInfo->hKeyDac1);
        status = db_find_key(hDB, pAgilentInfo->hKeyDac1, "/Demand", &hDac);
        status = db_open_record(hDB, hDac, & pAgilentInfo->fDac1Demand, sizeof(float),
				MODE_READ, Agilent34907_demand_DAC1, pequipment); 
	pAgilentInfo->fDac1Address = (INT)( (i+1) * 100 + 4);
	printf ("Setting address for DAC1 to %d \n",pAgilentInfo->fDac1Address );
	// DAC2
	sprintf(str, "Settings/Slot%dDAC2", i + 1);
	status = db_find_key(hDB, pAgilentInfo->hKeyRoot, str, &pAgilentInfo->hKeyDac2);
        status = db_find_key(hDB, pAgilentInfo->hKeyDac2, "/Demand", &hDac2);
        status = db_open_record(hDB, hDac2, & pAgilentInfo->fDac2Demand, sizeof(float),
				MODE_READ, Agilent34907_demand_DAC2, pequipment); 
	pAgilentInfo->fDac2Address = (INT)( (i+1) * 100 + 5); // make this 5 
        printf ("Setting address for DAC2 to %d \n",pAgilentInfo->fDac2Address );
      }
    }
  }
}

/**
 *   Name: Agilent34970A_init
 *
 *   Purpose: This routine is used to initialize the instance of the
 *   equipment. The connection to the ODB is established, space is
 *   allocated to store all the paradvms for the devices in the
 *   equipment. Values are initialized. The device driver is
 *   initialized, and a first collection of data is made.
 *
 *   Inputs: pequipment - EQUIPMENT pointer to the euipment
 *   instance.
 *
 *   Precond:
 *
 *   Outputs:
 *
 *   Postcond:
 */
INT Agilent34970A_init(EQUIPMENT *pequipment) {
  int   status, size, i, j, index, ch_offset, struct_size;
  char  str[256];
  char  conf[256];
  HNDLE hKey;
  AGILENT_INFO *pAgilentInfo;
  DVM_SETTINGS_STR(dvm_settings_str);
  INT configCount = 0;
  
  if(dd1) printf("Entering routine Agilent34907A_init\n");
  /* allocate private data */
  pequipment->cd_info = calloc(1, sizeof(AGILENT_INFO));
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  
  /* get class driver root key */
  cm_get_experiment_database(&hDB, NULL);
  sprintf(str, "/Equipment/%s", pequipment->name);
  db_create_key(hDB, 0, str, TID_KEY);
  db_find_key(hDB, 0, str, &pAgilentInfo->hKeyRoot);

  /* save event format */
  size = sizeof(str);
  db_get_value(hDB, pAgilentInfo->hKeyRoot, "Common/Format", str, &size, TID_STRING, TRUE);

  if (equal_ustring(str, "Fixed")) {
    pAgilentInfo->format = FORMAT_FIXED;
  } else if (equal_ustring(str, "MIDAS")) {
    pAgilentInfo->format = FORMAT_MIDAS;
  } else if (equal_ustring(str, "YBOS")) {
    pAgilentInfo->format = FORMAT_YBOS;
  }

  /* If Settings not created use default from include file. */
  if (db_find_key(hDB, pAgilentInfo->hKeyRoot, "Settings", &hKey) != DB_SUCCESS)  
    {
      printf("Agilent34970A_init: creating record for /Equipment/DVM/Settings\n");
      status = db_create_record(hDB, pAgilentInfo->hKeyRoot, "Settings", strcomb(dvm_settings_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"Agilent34970A_init","Could not create /Equipment/DVM/Settings record (%d)\n",status);
	  
	  cm_msg(MINFO,"Agilent34970A_init","If running, may be due to open records. Stop the run and try again");
	  return status;
	}  //    db_create_record(hDB, pAgilentInfo->hKeyRoot, "Variables", strcomb(agilent_variables_str));
    }
  else  /* key has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hKey, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_hotlink", "error during get_record_size (%d) for /Equipment/DVM/Settings record",status);
	  return status;
	}
      struct_size =  sizeof(DVM_SETTINGS);
      
      printf("Agilent34970A_init:Info - size of /Equipment/DVM/Settings saved structure: %d, size of Settings record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
	{
	  cm_msg(MINFO,"Agilent34970A_init","creating record (/Equipment/DVM/Settings); mismatch between size of structure (%d) & record size (%d)",
		 struct_size ,size);
	  /* create record */
	  status = db_create_record(hDB, pAgilentInfo->hKeyRoot, "Settings", strcomb(dvm_settings_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"Agilent34970A_init","Could not create /Equipment/DVM/Settings record (%d)\n",status);
	      //  if (run_state == STATE_RUNNING )
	      cm_msg(MINFO,"Agilent34970A_init","If running, may be due to open records. Stop the run and try again");
	      return status;
	    }
	  else
	    {
	    status = db_get_record_size(hDB, hKey, 0, &size);
	    if(status != SUCCESS)
	      printf("Failure from db_get_record_size (%d)\n",status);
	    else
	      printf("Success from create record for /Equipment/DVM/Settings, record size is now %d\n",size);


	    }
	}
    } 
  
  /* Use the Modules entries to identify the SlotxConfig sizes and count the channels */
  if (db_find_key(hDB, pAgilentInfo->hKeyRoot, "Settings/Modules", &hKey) == DB_SUCCESS) {
    HNDLE hName, hConfig;    
    pAgilentInfo->num_channels = 0;
    pAgilentInfo->names = (char *) calloc(60, NAME_LENGTH); // max possible size of names
    for (i = 0; i < 3; i++) {
      INT module;
      size = sizeof(module);
      db_get_data_index(hDB, hKey, &module, &size, i, TID_INT);
      if (module != 0) {  // we have a module in this slot
       sprintf(str, "Settings/Slot%dNames", i + 1);
        size = NAME_LENGTH;
        if (db_find_key(hDB, pAgilentInfo->hKeyRoot, str, &hName) == DB_SUCCESS) {
          switch (module) {
            case 34901:
              configCount += 20;
              
              // Allocate the space to hold the config strings and create hotlinks
              
              pAgilentInfo->config = (char *)  calloc(20, 64);
              sprintf(conf, "Settings/Slot%dConfig", i + 1);
              status = db_merge_data(hDB, pAgilentInfo->hKeyRoot, conf,
                pAgilentInfo->config, 20 * 64,
                20, TID_STRING);
	      sleep(2);              
              status = db_find_key(hDB, pAgilentInfo->hKeyRoot, conf, &hConfig);
	      sleep(2);              
              status = db_open_record(hDB, hConfig, pAgilentInfo->config, 20 * 64,
                MODE_READ, Agilent34901_demand, pequipment);
	      sleep(2);              
              for (j = 0; j < 20; j++) {  // 20 channels in this module
                db_get_data_index(hDB, hName, str, &size, j, TID_STRING);
                if (strlen(str) > 0) {
                  strcpy(pAgilentInfo->names + pAgilentInfo->num_channels * NAME_LENGTH, str);
                  pAgilentInfo->num_channels++;
                }
              }
              break;
            case 34907:
	      /* At the moment we are only interested in the DACs in this module. */
              
	      //              for (j = 0; j < 5; j++) {  // 5 channels in this module
              //  db_get_data_index(hDB, hName, str, &size, j, TID_STRING);
              //  if (strlen(str) > 0) {
              //    strcpy(pAgilentInfo->names + pAgilentInfo->num_channels * NAME_LENGTH, str);
              //    pAgilentInfo->num_channels++;
              //  }
              // }
              break;
            default:
              cm_msg(MERROR, "Agilent34970A_init", "Unrecognized number in Settings/Modules %d", module);
              break;
          }
       }        
      }
    }
    pAgilentInfo->names = realloc(pAgilentInfo->names, pAgilentInfo->num_channels * NAME_LENGTH);
  }

#ifdef ODB_PRIORITY
  /* count total number of channels */
  db_create_key(hDB, pAgilentInfo->hKeyRoot, "Settings/Channels", TID_KEY);
  db_find_key(hDB, pAgilentInfo->hKeyRoot, "Settings/Channels", &hKey);

  for (i = pAgilentInfo->num_channels = 0; pequipment->driver[i].name[0]; i++) {
    /* ODB value has priority over driver list */
    size = sizeof(INT);
    db_get_value(hDB, hKey, pequipment->driver[i].name,
      &pequipment->driver[i].channels, &size, TID_INT, TRUE);

    if (pequipment->driver[i].channels == 0) {
      pequipment->driver[i].channels = 1;
    }
    pAgilentInfo->num_channels += pequipment->driver[i].channels;
  }
#endif

  if (pAgilentInfo->num_channels == 0) {
    cm_msg(MERROR, "Agilent34970A_init", "No channels found in device driver list");
    return FE_ERR_ODB;
  }
  if(dd1)printf("Agilent34970A_init: Number of channels found in device driver list=%d\n",pAgilentInfo->num_channels);

  /* Allocate memory for buffers */
/*  pAgilentInfo->names            = (char *)  calloc(pAgilentInfo->num_channels, NAME_LENGTH);*/
  pAgilentInfo->modules  = (INT *)   calloc(3, sizeof(INT));

  pAgilentInfo->fMeasured        = (float *) calloc(pAgilentInfo->num_channels, sizeof(float));
  pAgilentInfo->dd_info          = (void *)  calloc(pAgilentInfo->num_channels, sizeof(void*));
  pAgilentInfo->channel_offset  = (INT *)   calloc(pAgilentInfo->num_channels, sizeof(INT));
  pAgilentInfo->driver          = (void *)  calloc(pAgilentInfo->num_channels, sizeof(void*));

  if (!pAgilentInfo->driver) {
    cm_msg(MERROR, "Agilent34970A_init", "Not enough memory");
    return FE_ERR_ODB;
  }

  /*---- Create/Read Settings ----*/

  db_merge_data(hDB, pAgilentInfo->hKeyRoot, "Settings/Address",
    &pAgilentInfo->address, sizeof(INT), 1, TID_INT);
  
  /*  db_merge_data(hDB, pAgilentInfo->hKeyRoot, "Settings/Names",
    pAgilentInfo->names, pAgilentInfo->num_channels * NAME_LENGTH, 
    pAgilentInfo->num_channels, TID_STRING); */

  db_merge_data(hDB, pAgilentInfo->hKeyRoot, "Settings/ScanList",
    pAgilentInfo->scanList, 64, 1, TID_STRING);
  db_find_key(hDB, pAgilentInfo->hKeyRoot, "Settings/ScanList", &hKey);
  db_open_record(hDB, hKey, pAgilentInfo->scanList, 64,
    MODE_READ, Agilent34970A_demandScan, pequipment);

  /* Measured */
  
  for (i = 0; i < pAgilentInfo->num_channels; i++) {
    pAgilentInfo->fMeasured[i] = 0.0f;
  }
  db_merge_data(hDB, pAgilentInfo->hKeyRoot, "Variables/Measured",
		pAgilentInfo->fMeasured, pAgilentInfo->num_channels * sizeof(float),
		pAgilentInfo->num_channels, TID_FLOAT); 
  db_find_key(hDB, pAgilentInfo->hKeyRoot, "Variables/Measured", &pAgilentInfo->hKeyMeasured);

  /*---- Initialize device drivers ----*/

  /* call init method */
  for (i = 0; pequipment->driver[i].name[0]; i++) {
    sprintf(str, "Settings/Devices/%s", pequipment->driver[i].name);
    status = db_find_key(hDB, pAgilentInfo->hKeyRoot, str, &hKey);
    if (status != DB_SUCCESS) {
      db_create_key(hDB, pAgilentInfo->hKeyRoot, str, TID_KEY);
      status = db_find_key(hDB, pAgilentInfo->hKeyRoot, str, &hKey);
      if (status != DB_SUCCESS) {
        cm_msg(MERROR, "Agilent34970A_init", "Cannot create %s entry in online database", str);
        free_mem(pAgilentInfo);
        return FE_ERR_ODB;
      }
    }

    status = gpib(CMD_INIT, hKey, &pequipment->driver[i].dd_info, pequipment->driver[i].channels);
    if (status != FE_SUCCESS) {
      free_mem(pAgilentInfo);
      return status;
    }
   status = gpib(CMD_ADDRESS, pAgilentInfo->address);
  
  }

  /* compose device driver channel assignment */
  for (i=0,j=0,index=0,ch_offset=0 ; i<pAgilentInfo->num_channels ; i++,j++) {
    while (j >= pequipment->driver[index].channels &&
      pequipment->driver[index].name[0]) {
      ch_offset += j;
      index++;
      j = 0;
    }

    pAgilentInfo->driver[i] = pequipment->driver[index].dd;
    pAgilentInfo->dd_info[i] = pequipment->driver[index].dd_info;
    pAgilentInfo->channel_offset[i] = ch_offset;
  }

  Agilent34970A_config(pequipment);
  Agilent34970A_multi(pequipment);
  Agilent34970A_demandScan(0, 0, pequipment);
  
  return FE_SUCCESS;
}


/**
 *   Name: <device>_exit
 *
 *   Purpose: This function frees all memory associated with the
 *   device and calls the exit function associated with the
 *   equipment device.
 *
 *   Inputs: pequipment - EQUIPMENT pointer
 *
 *   Precond: None
 *
 *   Outputs: INT - result code
 *
 *   Postcond: Device is deleted and parent exit code is run
 */
INT Agilent34970A_exit(EQUIPMENT *pequipment) {
  AGILENT_INFO *pAgilentInfo;
  INT i;
  int status;
  char cmdGpib[16];

  if(dd1) printf("\nEntering routine Agilent_exit\n");
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;

  sprintf(cmdGpib, "DISPLAY ON");
  status = gpib(CMD_SET, pAgilentInfo->address, cmdGpib, strlen(cmdGpib));

  free_mem(pAgilentInfo);

  /* call exit method of device drivers */
  for (i=0 ; pequipment->driver[i].dd != NULL ; i++) {
//    gpib(CMD_EXIT, pequipment->driver[i].dd_info);
  }
  if(dd1) printf("\nLeaving routine Agilent_exit\n");
  return FE_SUCCESS;
}

/**
 *   Name: <device>_idle
 *
 *   Purpose: This function is the idle task called on a regular
 *   interval by the front end to keep things going. Tasks that may
 *   need to be run that take a appreciable amount of time, such as
 *   ramping outputs or monitoring for change can be done here. They
 *   must be designed to not spend much time in this function, but
 *   allow regular polling to react to changes. The other option is
 *   to use Threads or Tasks.
 *
 *   Inputs: pequipment - EQUIPMENT pointer to the equipment record
 *
 *   Precond:
 *
 *   Outputs: INT - result code
 *
 *   Postcond: This would depend on the functions called in the
 *   routine. Very device specific
 */
INT Agilent34970A_idle(EQUIPMENT *pequipment) {
  AGILENT_INFO    *pAgilentInfo;
  static int iFirstTime = 0;
  char szString[DVM_STR_LENGTH];
  static DWORD timeLast = 0;
  DWORD timeNow;
  
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;

  if (iFirstTime == 0) { /* On first pass run test/reset */
    int status;
    
    strcpy(szString, "*IDN?");
    status = gpib(CMD_SET, pAgilentInfo->address, szString, strlen(szString));
    ss_sleep(500);
    status = gpib(CMD_GET, pAgilentInfo->address, szString, DVM_STR_LENGTH);
    printf("Agilent 34970A *IDN response %s\n", szString);
    iFirstTime ++;
  } else { /* read value from dvm */
/*  
    timeNow = ss_millitime();
   
    if ((timeNow - timeLast) > pAgilentInfo->scanPeriod) {    
      do {
        pAgilentInfo->last_channel = (pAgilentInfo->last_channel + 1) % pAgilentInfo->num_channels;
        
      } while (strlen(pAgilentInfo->slot1 + pAgilentInfo->last_channel * DVM_STR_LENGTH) == 0);

      printf("last chan %d\n", pAgilentInfo->last_channel);
            
      Agilent34970A_read(pequipment, pAgilentInfo->last_channel);
      timeLast = timeNow;
    }  
*/    
  }
  return FE_SUCCESS;
}

/**
 *   Name: cd_<device>_read
 *
 *   Purpose: This sends the equipment to the data stream. Enabled
 *   by equipment parameter RO items
 *
 *   Inputs: pevent - char pointer to equipment
 *
 *   Precond: None
 *
 *   Outputs: INT - size of the added data in the data stream
 *
 *   Postcond: None
 */
INT cd_Agilent34970A_read(char *pevent, INT off) {
  INT i, size, status;
  char szData[128];
  float *pdata;
  DWORD *pdw;
  AGILENT_INFO *pAgilentInfo;
  EQUIPMENT *pequipment;
  char * pStart;
  char * pEnd;
  INT iSize;
  INT channel = 0;
  char spoll;
  char * endptr;
  INT iCount;
  DWORD start, finish, sent;
  char cmdGpib[128];
  static int first = 1;
  
  if(dd1) printf("\nEntering routine cd_Agilent34970A_read\n");
    
  pequipment = *((EQUIPMENT **) pevent);
  pAgilentInfo = (AGILENT_INFO *) pequipment->cd_info;
  //  cm_get_experiment_database(&hDB, NULL);

  start = ss_millitime();

  status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);

  sprintf(cmdGpib, "*ESR?");
  status = gpib(CMD_SET, pAgilentInfo->address, cmdGpib, strlen(cmdGpib));
  status = gpib(CMD_GET, pAgilentInfo->address, szData, DVM_STR_LENGTH);

//  sprintf(cmdGpib, "*CLS;");
//  status = DRIVER(channel)(CMD_SET, pAgilentInfo->address, cmdGpib, strlen(cmdGpib));
//  status = DRIVER(channel)(CMD_SPOLL, pAgilentInfo->address, &spoll);
//printf("ESR spoll 0x%X\n", spoll);

  sprintf(cmdGpib, "READ?;*OPC;");
  status = gpib(CMD_SET, pAgilentInfo->address, cmdGpib, strlen(cmdGpib));

  iCount = 0;
  do {  // wait for operation complete bit set
    status = gpib(CMD_SPOLL, pAgilentInfo->address, &spoll);
    iCount ++;
  } while ((spoll & 0x20) == 0);
  //if(dd1)printf("spoll loop count %d\n",iCount);
  pAgilentInfo->fMeasured[4] = (float) iCount;
  
  //  iCount = gpib(CMD_GET, pAgilentInfo->address, szData, strlen(szData));
  iCount = gpib(CMD_GET, pAgilentInfo->address, szData, 128);

  if (status != FE_SUCCESS) {
    cm_msg(MERROR, "Agilent34970A_read", "Error in device driver - CMD_GET");
  }

  if(dd1)printf("GET %s\n", szData);
    
  pStart = szData;  
  for (i = 0; i < pAgilentInfo->scanCount; i++) {
    pEnd = strchr(pStart, ',');
    if (pEnd != NULL) *pEnd = 0x0;
    sscanf(pStart, "%e", &pAgilentInfo->fMeasured[i]);
    pStart = pEnd + 1;
  }

  if(dd1)printf("Acq time %ld ms\n", ss_millitime() - start);
  
  pAgilentInfo->fMeasured[5] = (float) (ss_millitime() - start);

        
  db_set_data(hDB, pAgilentInfo->hKeyMeasured, pAgilentInfo->fMeasured,
    pAgilentInfo->num_channels * sizeof(float),
    pAgilentInfo->num_channels, TID_FLOAT);

  pequipment->odb_out++;

  return 0;
}   
 

/**
 *   Name: cd_<device>
 *
 *   Purpose: This function is the class driver dispatch table for
 *   the standard class function calls such as Init, Exit etc. The
 *   called functions are described above.
 *
 *   Inputs: cmd - INT command descriptor
 *   pequipment - PEQUIPMENT pointer to EQUIPMENT structure
 *
 *   Precond: The equipment instance must be valid
 *
 *   Outputs: INT result code
 *
 *   Postcond: This would depend on the function requested.
 *--------------------------------------------------------------------*/
INT cd_Agilent34970A(INT cmd, EQUIPMENT *pequipment) {
  INT  status;


  switch (cmd) {
  case CMD_INIT:
    status = Agilent34970A_init(pequipment);
    break;

  case CMD_EXIT:
    status = Agilent34970A_exit(pequipment);
    break;

  case CMD_IDLE:
    status = Agilent34970A_idle(pequipment);
    break;

  default:
    cm_msg(MERROR, "dvm class driver", "Received unknown command %d", cmd);
    status = FE_ERR_DRIVER;
    break;
  }

  return status;
}

