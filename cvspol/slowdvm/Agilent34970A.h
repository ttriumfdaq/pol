/**
 *
 * $Log: Agilent34970A.h,v $
 * Revision 1.1  2007/10/23 19:40:13  suz
 * new to cvs
 *
 * Revision 1.5  2004/06/10 00:01:28  cadfael
 * Many changes getting the front end working.
 * Completed timing tests of Agilent DVM
 * Added BOR EOR commands in feE920
 *
 * Revision 1.4  2004/05/28 19:00:30  cadfael
 * small changes to ODB names
 *
 * Revision 1.3  2004/05/27 23:55:34  cadfael
 * Have demands working to DAC, and scan list build running
 *
 * Revision 1.2  2004/05/25 21:14:51  cadfael
 * Getting ODB entries defined and starting to build SENS strings
 *
 * Revision 1.1.1.1  2004/05/25 18:26:26  cadfael
 * Started CVS
 *
 *
 */

/* Length of strings for configuring dvms */

#define DVM_STR_LENGTH 128
#define DVM_EOS_LENGTH 2


/* Configuration declarations */

#define DVM_ADDRESS		1
#define DVM_CONFIG		2
#define DVM_TEST			3
#define DVM_FORMAT		4
#define DVM_EOS				5
#define DVM_NAME			6

/* function prototypes */

INT cd_Agilent34970A(INT cmd, PEQUIPMENT pequipment);
INT cd_Agilent34970A_read(char *pevent, INT off);
INT Agilent34970A_readChannel(PEQUIPMENT pequipment, INT channel);
float Agilent34970A_read_returnChannel(PEQUIPMENT pequipment, INT channel);
void Agilent34970A_config(EQUIPMENT *pequipment);
