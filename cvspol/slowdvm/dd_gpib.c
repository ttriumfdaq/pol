/**
 *   This function contains the device driver functions
 *   between the equipment record and a National Instruments GPIB
 *   interface.
 *
 *   The driver handles all the GPIB devices in the system. Each
 *   class driver must call init to register it's address with the
 *   driver, then setup the instrument in whatever way is needed.
 *   The driver provides write and read functions only via character
 *   strings. The calling class driver must decode the data.
 *
 *   @author: David Morris
 *
 *   $Log: dd_gpib.c,v $
 *   Revision 1.1  2007/10/23 19:40:03  suz
 *   new to cvs
 *
 *   Revision 1.3  2004/06/10 00:01:28  cadfael
 *   Many changes getting the front end working.
 *   Completed timing tests of Agilent DVM
 *   Added BOR EOR commands in feE920
 *
 *   Revision 1.2  2004/05/27 23:55:34  cadfael
 *   Have demands working to DAC, and scan list build running
 *
 *   Revision 1.1.1.1  2004/05/25 18:26:26  cadfael
 *   Started CVS
 *
 */
#include "midas.h"
#include "msystem.h"
#ifdef OS_WINDOWS
#include "decl-32.h"    /* GPIB include file for DLL */
#endif
#ifdef OS_LINUX
#include "gpib_user.h"    /* GPIB include file for DLL */
#include "ib.h"

#endif

#include "dd_gpib.h"
int dd_gpib = 0; /* dd_gpib = 0 (quiet), dd_gpib=1 (debug printf) */

/* this array used to hold each address descriptor */
#define NUM_ADDRESS 32
static INT iDevice[NUM_ADDRESS];

/**
 *   This function initializes the hardware device using
 *   device specific actions. All setup of the physical equipment
 *   should be done here, along with any specific initialization
 *   functions. Care should be taken that the initialization here
 *   does not affect other equipment using the same hardware.
 *
 *   Inputs:
 *
 *   hKeyRoot - HNDLE handle to the root key of the database
 *
 *   index - INT value for an index value. May not be used by the
 *   routine.
 *
 *   channels - INT value identifying the number of channels for
 *   this device. May not be used by routine.
 *
 *   Precond: None
 *
 *   Outputs: INT result code
 *
 *   Postcond: The hardware device should be initialized, and any
 *   global identifiers set up. ie All CAMAC cdreg calls could be
 *   done here for later use.
 */
INT gpib_init(HNDLE hKeyRoot, INT index, INT channels) {
  HNDLE hDB;
  INT   iIndex;
  static BOOL bFirstTime = TRUE;

  cm_get_experiment_database(&hDB, NULL);

  /* Make sure device array is NULL before assigning devices to GPIB addresses */

  if (bFirstTime == 0) {
    for (iIndex = 0; iIndex < NUM_ADDRESS; iIndex ++) {
      iDevice[iIndex] = 0;
    }
    bFirstTime = FALSE;
  }

  return FE_SUCCESS;
}


/**
 *   Name: <device>_exit
 *
 *   Purpose: This function will handle any cleanup of the the
 *   device driver when equipment is deleted.
 *
 *   Inputs: index - INT value
 *
 *   Precond: None
 *
 *   Outputs: INT - result code
 *
 *   Postcond: All aspects of the driver should be removed from the
 *   system
 */
INT gpib_exit(INT index) {
  INT iAddress;
  char command[64];

  /* Take off-line */

  for (iAddress = 0; iAddress < NUM_ADDRESS; iAddress ++) {
    if (iDevice[iAddress] != 0) {
      ibonl(iDevice[iAddress], 0);
    }
  }
  return FE_SUCCESS;
}

/**
 *   Name: gpib_address
 *
 *   Purpose: This function tries to register the passed address for
 *   private use
 */
INT gpib_address(INT iAddress) {
  static INT iFirstTime = 0;
  char command[64];


  /* Valid GPIB address? */

  if ((iAddress < 1) || (iAddress > 30)) {
    cm_msg(MERROR, "gpib_init", "Invalid GPIB address %d", iAddress);
    return FE_ERR_ODB;
  }

  /* Already assigned? */

  if (iDevice[iAddress] != 0) {
    cm_msg(MERROR, "gpib_init", "GPIB address already in use %d", iAddress);
    return FE_ERR_ODB;
  }

  /* Call GPIB driver and get device handle */

  if ((iDevice[iAddress] = ibdev(0, iAddress, 0, T1s, 1, 0)) < 0) {
    cm_msg(MERROR, "gpib_init", "Cannot open GPIB address %d", iAddress);
    return FE_ERR_ODB;
  }

  /* set timeout for device */

  ibtmo(iDevice[iAddress], T3s);
  if (ibsta & ERR) {
    cm_msg(MERROR, "gpib device driver", "GPIB ibtmo addr %d error code 0x%x", iAddress, ibsta);
    return FE_ERR_ODB;
  }

  return FE_SUCCESS;
}


/**
 *   Name: gpib_eos
 *
 *   Purpose: Set the end-of-string character
 */
INT gpib_eos(INT iAddress, char cMode, char cEOS) {
  /* Make sure GPIB address has been initialized */

  if (iDevice[iAddress] == 0) {
    cm_msg(MERROR, "gpib device driver", "Not an initialized address %d", iAddress);
    return FE_ERR_ODB;
  }

  /* write string to GPIB device */

  ibeos(iDevice[iAddress], cMode << 8 | cEOS);
  if (ibsta & ERR) {
    cm_msg(MERROR, "gpib device driver", "GPIB ibeos addr %d error code 0x%x", iAddress, ibsta);
    return FE_ERR_ODB;
  }

  return FE_SUCCESS;
}

/**
 *   Name: gpib_eoi
 *
 *   Purpose: Enable/disable EOI transmission
 */
INT gpib_eot(INT iAddress, INT iValue) {
  /* Make sure GPIB address has been initialized */

  if (iDevice[iAddress] == 0) {
    cm_msg(MERROR, "gpib device driver", "Not an initialized address %d", iAddress);
    return FE_ERR_ODB;
  }

  /* write string to GPIB device */

  ibeot(iDevice[iAddress], iValue);
  if (ibsta & ERR) {
    cm_msg(MERROR, "gpib device driver", "GPIB ibeos addr %d error code 0x%x", iAddress, ibsta);
    return FE_ERR_ODB;
  }

  return FE_SUCCESS;
}

/**
 * Execute a Serial Poll on the instrument and return the value of the register
 */
INT gpib_spoll(INT iAddress, char * result) {
  static INT bReportError = TRUE;
  
  /* Make sure GPIB address has been initialized */

  if (iDevice[iAddress] == 0) {
    cm_msg(MERROR, "gpib device driver", "Not an initialized address %d", iAddress);
    return -1;
  }

  ibrsp(iDevice[iAddress], result);
  
  if (ibsta & ERR) {
    if (bReportError == TRUE) {
      bReportError = FALSE;
      cm_msg(MERROR, "gpib device driver", "GPIB ibrsp addr %d error code 0x%x", iAddress, ibsta);
      return -1;
    }
  } else {
    bReportError = TRUE;
  }

  return FE_SUCCESS;
}


/**
 *   Name: <device>_set
 *
 *   Purpose: This function contains the actions needed to set a
 *   value in a device. This could be a combination of many tasks,
 *   such as set a bit, adjust a DAC and return a status. It also
 *   may be a simple bit change. It is fairly device specific.
 *
 *   Inputs: Some of these may be used
 *   index - INT value for a device array
 *   channel - INT value for a channel in the unit
 *   fvalue - float value for a new setting
 *
 *   Precond: The <device>_init function must be called
 *   successfully.
 *
 *   Outputs: INT - result code
 *
 *   Postcond: This would depend on the requested action of the
 *   hardware


 *   Name: gpib_set
 *
 *   Purpose: This function is a general function used to both turn
 *   on the supply and to set the DAC for adjusting the magnet
 *   current. To set the gpib Magnet Supply, the DAC is written.
 *   The supply ramps up at a preset rate, and ramps down at another
 *   preset rate. These are manually adjusted on the fron of the
 *   power supply. Make sure the unit is in remote mode before
 *   setting the DAC.
 *
 *   Inputs:
 *
 *   index - not used
 *
 *   channel - INT value indicating entry in module table
 *
 *   fvalue - float of bit value. It is converted previously
 *   from engineering units.
 */
INT gpib_set(INT iAddress, char * szString, INT iLength) {
  /* Make sure GPIB address has been initialized */

  if (iDevice[iAddress] == 0) {
    cm_msg(MERROR, "gpib device driver", "Not an initialized address %d", iAddress);
    return FE_ERR_ODB;
  }

  if(dd_gpib)printf("GPIB set: %s\n", szString);

  /* write string to GPIB device */

  ibwrt(iDevice[iAddress], szString, iLength);
  if (ibsta & ERR) {
    cm_msg(MERROR, "gpib device driver", "GPIB ibwrt addr %d cmd %s error code 0x%x", iAddress, szString, ibsta);
    return FE_ERR_ODB;
  }

  return FE_SUCCESS;
}


/**
 *   Name: <device>_get
 *
 *   Purpose: This function is used to get data from the hardware
 *   associated with a particular device. Any complex actions needed
 *   to get the data would be done here. It may be a simple bit
 *   read.
 *
 *   Inputs: Some of these may be used
 *   index - INT value for a device array
 *   channel - INT value for a channel in the unit
 *   fvalue - float pointer to location to save the returned data
 *
 *   Precond: The <device>_init function must be called
 *   successfully.
 *
 *   Outputs: INT - result code
 *
 *   Postcond: None

 *   Name: gpib_get
 *
 *   Purpose: This function returns the value of an input device.
 *
 *   Inputs:
 *   index - None use
 *   channel - INT value identifying which module to read
 *   fvalue - float pointer of location for return data.
 */
INT gpib_get(INT iAddress, char * szString, INT iLength) {
  static INT bReportError = TRUE;
  DWORD start;

  /* Make sure GPIB address has been initialized */

  if (iDevice[iAddress] == 0) {
    cm_msg(MERROR, "gpib device driver", "Not an initialized address %d", iAddress);
    return -1;
  }

  /* Read string from GPIB device */
  start = ss_millitime();
  ibrd(iDevice[iAddress], szString, iLength);
  if(dd_gpib)printf("gpib_get took %ld ms\n",ss_millitime() - start);
  if (ibsta & ERR) {
    if (bReportError == TRUE) {
      bReportError = FALSE;
      cm_msg(MERROR, "gpib device driver", "GPIB ibrd addr %d cmd %s error code 0x%x", iAddress, szString, ibsta);
      return -1;
    }
  } else {
    bReportError = TRUE;
  }

  /* Make sure string is NULL terminated */

  szString[ibcntl] = 0x0;
  if(dd_gpib)printf("GPIB get: %s\n", szString);

  return ibcntl;
}


/**
 *   Name: <device>
 *
 *   Purpose: This function is the device driver entry for all
 *   functions associated with this particular device. This function
 *   dispatches requests to the specific functions in this module.
 *
 *   The variable length parameter string is handled by this routine
 *   for the specific function.
 *
 *   Inputs: cmd - INT value for the command designator
 *
 *   ... - a variable length parameter string
 *
 *   Precond: None
 *
 *   Outputs: INT - result code
 *
 *   Postcond: This will depend on the functiuon called.
 */
INT gpib(INT cmd, ...) {
  va_list argptr;
  HNDLE   hKey;
  INT     channel, status, index;
  INT     address;
  char *  szString;
  INT     iLength;
  char    cMode;
  char    cEOS;
  char *   spoll;
  
  va_start(argptr, cmd);
  status = FE_SUCCESS;

  switch (cmd) {
  case CMD_INIT:
    hKey = va_arg(argptr, HNDLE);
    index = va_arg(argptr, INT);
    channel = va_arg(argptr, INT);
    status = gpib_init(hKey, index, channel);
    break;

  case CMD_EXIT:
    index = va_arg(argptr, INT);
    status = gpib_exit(index);
    break;

  case CMD_ADDRESS:
    address = va_arg(argptr, INT);
    status = gpib_address(address);
    break;

  case CMD_EOS:
    address  = va_arg(argptr, INT);
    cMode  = va_arg(argptr, INT);
    cEOS  = va_arg(argptr, INT);
    status = gpib_eos(address, cMode, cEOS);
    break;

  case CMD_EOT:
    address  = va_arg(argptr, INT);
    index = va_arg(argptr, INT);
    status = gpib_eot(address, index);
    break;

  case CMD_SPOLL:
    address  = va_arg(argptr, INT);
    spoll  = va_arg(argptr, char *);
    status = gpib_spoll(address, spoll);
    break;

  case CMD_SET:
    address  = va_arg(argptr, INT);
    szString  = va_arg(argptr, char *);
    iLength  = va_arg(argptr, INT);
    status = gpib_set(address, szString, iLength);
    break;

  case CMD_GET:
    address  = va_arg(argptr, INT);
    szString  = va_arg(argptr, char *);
    iLength  = va_arg(argptr, INT);
    status = gpib_get(address, szString, iLength);
    break;

  default:
    cm_msg(MERROR, "gpib device driver", "Received unknown command %d", cmd);
    status = FE_ERR_DRIVER;
    break;
  }
  va_end(argptr);

  return status;
}
