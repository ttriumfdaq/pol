/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "scaler event" which are filled with
                CAMAC or random data. The trigger event is filled
                with two banks (ADC0 and TDC0), the scaler event
                with one bank (SCLR).

  $Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "mcstd.h"
#include "experim.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  extern INT run_state;

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fedeferred";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;

// For transition
BOOL transition_PS_requested = FALSE;
BOOL  end_of_mcs_cycle = FALSE;
int lcounter;
/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

INT poll_event(INT source, INT count, BOOL test);
INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

  INT poststart(INT run_number, char *error);
  INT poststop(INT run_number, char *error);
  INT prestart(INT run_number, char *error);

/*-- Equipment list ------------------------------------------------*/

EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,            /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING |            /* read only when running */
     RO_ODB,                 /* and update ODB */
     10,                     /* poll for deferred test use 10ms in periodic */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    },

   {"Scaler",                /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,            /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_scaler_event,       /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/


//
//-- Deferred transition callback
BOOL wait_end_cycle(int transition, BOOL first)
{
  if (first) {
    printf("PS requested\n");
    transition_PS_requested = TRUE;
    return FALSE;
  }
  
  if (end_of_mcs_cycle) {
    printf("Transition being performed\n");
    lcounter = 0;
    transition_PS_requested = FALSE;
    end_of_mcs_cycle = FALSE;
    return TRUE;
  } else {
    printf("PS postponed for now...\n");
    return FALSE;
  }
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
   // Register deferred transition
   cm_register_deferred_transition(TR_STOP, wait_end_cycle);
   cm_register_deferred_transition(TR_PAUSE, wait_end_cycle);

   cm_register_transition(TR_START, prestart, 350);
   cm_register_transition(TR_START, poststart, 600); 
   cm_register_transition(TR_STOP, poststop, 600); 

   /* print message and return FE_ERR_HW if frontend should not be started */

   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   /* put here clear scalers etc. */
  printf("begin_of_run: run %d starting\n",run_number);

   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  printf("end_of_run: end of run %d\n",run_number);
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
   WORD *pdata, a;

   /* init bank structure */
   bk_init(pevent);

   /* create structured ADC0 bank */
   bk_create(pevent, "ADC0", TID_WORD, &pdata);

   /* Use following code to "simulate" data */
   for (a = 0; a < 8; a++)
      *pdata++ = rand() % 1024;

   bk_close(pevent, pdata);

   if (transition_PS_requested) {
     printf("in trigger but transition postponed evt#:%d (lc:%d)\n", SERIAL_NUMBER(pevent), lcounter);
     if (lcounter++ > 200) {
       end_of_mcs_cycle = TRUE;
     }
   }
   
   ss_sleep(10);

   return bk_size(pevent);
}

/*-- Scaler event --------------------------------------------------*/

INT read_scaler_event(char *pevent, INT off)
{
  return 0;
}


INT prestart(INT run_number, char *error) 
{
  INT test;
  ss_sleep(500); // run a check program
  test = (int) (rand() % 256);
  printf("prestart: rand is %d\n", test) ;
  if(test < 150)
    {
      printf("prestart: check has failed (test < 150 ), run should not start\n");
      return FE_ERR_HW;
    }
  return SUCCESS;
 
}

INT poststart(INT run_number, char *error) 
{
  ss_sleep(500); // run another check on some hardware
  if(run_state == STATE_RUNNING) 
      printf("poststart: run started successfully\n") ;
  else
     printf("poststart: not running\n") ;
  return SUCCESS;
 
}

INT poststop(INT run_number, char *error) 
{
  ss_sleep(200); // clear some mallocs
  if(run_state == STATE_RUNNING) 
      printf("poststop: run stoppped\n") ;
  else
     printf("poststop: stopped\n") ;
  return SUCCESS;
 
}
